package com.anphuocthai.staff;

import com.google.android.gms.maps.model.PolylineOptions;

/**
 * Created by OpenYourEyes on 7/14/2020
 */
public interface DirectionPointListener {
    void onPath(PolylineOptions polylineOptions);
}
