package com.anphuocthai.staff.data.db.network;

import com.anphuocthai.staff.api.ApiURL;
import com.anphuocthai.staff.baserequest.APIResponse;
import com.anphuocthai.staff.data.db.network.model.request.LoginRequest;
import com.anphuocthai.staff.data.db.network.model.response.LoginResponse;
import com.anphuocthai.staff.model.ChiTietCongTacResponse;
import com.anphuocthai.staff.model.DanhSachCongTacResponse;
import com.anphuocthai.staff.model.GiaoHangParam;
import com.anphuocthai.staff.model.GiaoHangResponse;
import com.anphuocthai.staff.model.InsertCongTacBody;
import com.anphuocthai.staff.model.InsertCongTacResponse;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;
import com.anphuocthai.staff.utils.App;
import com.rx2androidnetworking.Rx2AndroidNetworking;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.Observable;

@javax.inject.Singleton
public class AppApiHelper implements ApiHelper {


    private ApiHeader mApiHeader;

    @Inject
    public AppApiHelper(ApiHeader apiHeader) {
        mApiHeader = apiHeader;
    }

    @Override
    public ApiHeader getApiHeader() {
        return mApiHeader;
    }

    @Override
    public Flowable<List<OrderTran>> getAllDebtApiCall(JSONObject object) {
        try {
            object.put("BoPhanID", App.getInstance().getDepartmentId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return Rx2AndroidNetworking.post(ApiURL.GET_ORDER_URL)
                .addHeaders(ApiURL.getBaseHeader())
                .addHeaders(ApiURL.getTokenHeader())
                .addJSONObjectBody(object)
                .build()
                .getObjectListFlowable(OrderTran.class);

    }

    @Override
    public Observable<LoginResponse> doServerLoginApiCall(LoginRequest request) {
        return Rx2AndroidNetworking.post(ApiURL.LOGIN_URL)
                .addHeaders(ApiURL.getBaseHeader())
                .addBodyParameter(request)
                .build()
                .getObjectObservable(LoginResponse.class);
    }

    @Override
    public Observable<APIResponse> banGiaoChungTu(GiaoHangParam param) {
        return Rx2AndroidNetworking.post(ApiURL.banGiaoChungTu)
                .addHeaders(ApiURL.getBaseHeader())
                .addApplicationJsonBody(param)
                .build()
                .getObjectObservable(APIResponse.class);
    }

    @Override
    public Observable<GiaoHangResponse> layDanhSachDonDat(int userId, int page, int size) {

        return Rx2AndroidNetworking.post(ApiURL.getDanhSachChuaBanGiao)
                .addHeaders(ApiURL.getBaseHeader())
                .addBodyParameter("Page", String.valueOf(page))
                .addBodyParameter("Size", String.valueOf(size))
                .addBodyParameter("isGetHangHoa", String.valueOf(true))
                .addBodyParameter("KinhDoanhID", String.valueOf(userId))
                .build()
                .getObjectObservable(GiaoHangResponse.class);
    }


    @Override
    public Observable<DanhSachCongTacResponse> laydanhSachCongTac(String fromDate, String toDate) {
        return Rx2AndroidNetworking.get(ApiURL.danhSachBaoCao)
                .addHeaders(ApiURL.getBaseHeader())
                .addHeaders(ApiURL.getTokenHeader())
                .addQueryParameter("fromDate", fromDate)
                .addQueryParameter("toDate", toDate)
                .build()
                .getObjectObservable(DanhSachCongTacResponse.class);
    }

    @Override
    public Observable<ChiTietCongTacResponse> chiTietCongTac(int id) {
        return Rx2AndroidNetworking.get(ApiURL.chiTietCongTac)
                .addHeaders(ApiURL.getBaseHeader())
                .addHeaders(ApiURL.getTokenHeader())
                .addQueryParameter("DuLieuChamCongId", String.valueOf(id))
                .build()
                .getObjectObservable(ChiTietCongTacResponse.class);
    }

    @Override
    public Observable<InsertCongTacResponse> inserCongTac(InsertCongTacBody body) {
        return Rx2AndroidNetworking.post(ApiURL.insertBaoCao)
                .addHeaders(ApiURL.getBaseHeader())
                .addHeaders(ApiURL.getTokenHeader())
                .addApplicationJsonBody(body)
                .build()
                .getObjectObservable(InsertCongTacResponse.class);
    }
}
