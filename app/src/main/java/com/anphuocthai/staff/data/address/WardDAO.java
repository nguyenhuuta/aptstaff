package com.anphuocthai.staff.data.address;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.anphuocthai.staff.entities.Wards;

import java.util.List;

/**
 * Created by DaiKySy on 8/19/19.
 */
@Dao
public interface WardDAO {
    @Insert
    void insert(List<Wards> wards);

    @Query("SELECT * FROM Wards WHERE quanHuyenID = :quanHuyenID")
    List<Wards> getWards(int quanHuyenID);

    @Query("SELECT * FROM Wards")
    List<Wards> getWards();

    @Update
    void update(Wards product);

    @Delete
    void delete(Wards product);
}
