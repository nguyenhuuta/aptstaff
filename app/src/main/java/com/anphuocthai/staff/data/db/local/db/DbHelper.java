package com.anphuocthai.staff.data.db.local.db;

import com.anphuocthai.staff.data.db.model.OrderProduct;
import com.anphuocthai.staff.model.CongTacModel;
import com.anphuocthai.staff.model.customer.Customer;

import java.util.List;

import io.reactivex.Observable;

public interface DbHelper {
    Observable<List<OrderProduct>> getAllOrderProducts();

    Observable<Boolean> insertOrderProduct(final OrderProduct orderProduct);

    Observable<Boolean> insertOrderProduct(final List<OrderProduct> orderProducts);

    Observable<Boolean> updateOrderProduct(final OrderProduct orderProduct);

    Observable<Boolean> deleteOrderProduct(final OrderProduct orderProduct);

    Observable<Boolean> deleteAllOrderProduct();

    // current customer
    Observable<List<Customer>> getAllCurrentCustomers();

    Observable<Boolean> insertCurrentCustomer(final Customer customer);

    Observable<Boolean> updateCurrentCustomer(final Customer customer);

    Observable<Boolean> deleteCurrentCustomer(final Customer customer);

    Observable<Boolean> deleteAllCurrentCustomer();

    Observable<List<CongTacModel>> queryCongTacByMonth(int month);

    void themCongTacs(List<CongTacModel> list);
}
