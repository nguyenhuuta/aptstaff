package com.anphuocthai.staff.data.db.local.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.anphuocthai.staff.data.db.local.db.dao.CurrentCustomerDao;
import com.anphuocthai.staff.data.db.local.db.dao.OrderProductDao;
import com.anphuocthai.staff.data.db.model.OrderProduct;
import com.anphuocthai.staff.model.customer.Customer;

@Database(entities = {OrderProduct.class, Customer.class}, version = 2)
public abstract class AppDatabase extends RoomDatabase{
    public abstract OrderProductDao orderProductDao();
    public abstract CurrentCustomerDao currentCustomerDao();

}
