package com.anphuocthai.staff.data.db.local.db.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.anphuocthai.staff.data.db.model.OrderProduct;

import java.util.List;

@Dao
public interface OrderProductDao {

    @Query("SELECT * FROM orderproduct")
    List<OrderProduct> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(OrderProduct orderProduct);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<OrderProduct> orderProducts);

    @Update
    void update(OrderProduct orderProduct);

    @Delete
    void delete(OrderProduct orderProduct);

    @Query("DELETE FROM orderproduct")
    void deleteAll();

}
