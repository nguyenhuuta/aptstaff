package com.anphuocthai.staff.data.product;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.anphuocthai.staff.entities.Product;

import java.util.List;

/**
 * Created by DaiKySy on 8/19/19.
 */
@Dao
public interface ProductDAO {
    @Insert
    void insert(Product product);

    @Query("SELECT * FROM Product WHERE catId = :catId")
    List<Product> getProducts(int catId);

    @Query("SELECT * FROM Product WHERE catId = :catId AND productId = :productId")
    Product getProducts(int catId, int productId);

    @Update
    void update(Product product);

    @Delete
    void delete(Product product);
}
