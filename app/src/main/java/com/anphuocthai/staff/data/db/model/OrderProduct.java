package com.anphuocthai.staff.data.db.model;


import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.anphuocthai.staff.data.TypeProduct;
import com.anphuocthai.staff.ui.delivery.model.GiaChinh;
import com.anphuocthai.staff.ui.delivery.model.Thuoctinhs;
import com.anphuocthai.staff.ui.product.OrderLogic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Entity(tableName = "orderproduct")
public class OrderProduct {

//    @PrimaryKey(autoGenerate = true)
//    private int id;

    @ColumnInfo(name = "customer_guid")
    private String customerGuid;

    @ColumnInfo(name = "customer_name")
    private String customerName;

    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "product_id")
    private String productId;

    @ColumnInfo(name = "product_quantity")
    private float quantity;

    @ColumnInfo(name = "product_code")
    private String productCode;

    @ColumnInfo(name = "product_name")
    private String productName;

    @ColumnInfo(name = "description")
    private String description;

    @ColumnInfo(name = "detail")
    private String detail;

    @ColumnInfo(name = "data")
    private String data;

    @Embedded
    private Thuoctinhs property;

    @Embedded
    private GiaChinh mainPrice;

    @ColumnInfo(name = "company_name")
    private String companyName;

    @ColumnInfo(name = "type_product")
    private String typeName;

    @ColumnInfo(name = "source")
    private String source;

    @ColumnInfo(name = "trade_mark")
    private String tradeMark;

    @ColumnInfo(name = "product_category_name")
    private String productCategoryName;

    @ColumnInfo(name = "product_category_id")
    private int productCategoryId;

    @ColumnInfo(name = "short_description")
    private String shortDescription;

    @ColumnInfo(name = "thumb_id")
    private int thumbId;

    @Embedded
    private OrderLogic orderLogic;

    @ColumnInfo(name = "product_note")
    private String productNote; //

    @Ignore
    private boolean isCheck;

    @Ignore
    private boolean isCheckChietKhau;
    @Ignore
    private Map<Integer, Integer> listChietKhauIds = new HashMap<>();

    @Ignore
    private TypeProduct typeProduct;

    @Ignore
    public OrderProduct() {

    }

    public OrderProduct(String customerGuid, String customerName, String productId, float quantity, String productCode, String productName, String description, String detail, String data, Thuoctinhs property, GiaChinh mainPrice, String companyName, String typeName, String source, String tradeMark, String productCategoryName, String shortDescription, int thumbId, OrderLogic orderLogic, String productNote, int productCategoryId) {
        this.customerGuid = customerGuid;
        this.customerName = customerName;
        this.productId = productId;
        this.quantity = quantity;
        this.productCode = productCode;
        this.productName = productName;
        this.description = description;
        this.detail = detail;
        this.data = data;
        this.property = property;
        this.mainPrice = mainPrice;
        this.companyName = companyName;
        this.typeName = typeName;
        this.source = source;
        this.tradeMark = tradeMark;
        this.productCategoryName = productCategoryName;
        this.shortDescription = shortDescription;
        this.thumbId = thumbId;
        this.orderLogic = orderLogic;
        this.productNote = productNote;
        this.productCategoryId = productCategoryId;
    }


    public Map<Integer, Integer> getListChietKhauIds() {
        return listChietKhauIds;
    }

    public int totalCV() {
        List<Integer> valueList = new ArrayList<>(listChietKhauIds.values());
        int total = 0;
        for (Integer number : valueList) {
            total += number;
        }
        return total;
    }

    public int getTongChietKhau(float quantity) {
        return totalCV() * (int) quantity;
    }

    public String getValueChietKhau(int id) {
        Integer value = listChietKhauIds.get(id);
        return value != null ? value.toString() : null;
    }

    public boolean addChietKhauId(int id, int value, int maxValue) {
        listChietKhauIds.remove(id);
        int total = totalCV();
        boolean isValid = total + value <= maxValue;
        if (isValid) {
            listChietKhauIds.put(id, value);
        }
        return isValid;
    }

    public boolean removeChietKhauId(Integer id) {
        Integer value = listChietKhauIds.remove(id);
        return value != null;
    }

    public void resetChietKhau() {
        listChietKhauIds.clear();
    }


    public boolean isCheckChietKhau() {
        return isCheckChietKhau;
    }

    public void setCheckChietKhau(boolean checkChietKhau) {
        if (!checkChietKhau) {
            resetChietKhau();
        }
        isCheckChietKhau = checkChietKhau;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean check) {
        isCheck = check;
        if (check && typeProduct != null) {
            orderLogic.setApplyCV(typeProduct.isCV());
            orderLogic.setApplyDoanhSo(typeProduct.isDoanhSo());
        } else {
            orderLogic.setApplyCV(true);
            orderLogic.setApplyDoanhSo(true);
        }
    }

    public TypeProduct getTypeProduct() {
        return typeProduct;
    }

    public void setTypeProduct(TypeProduct typeProduct) {
        this.typeProduct = typeProduct;
        if (isCheck && typeProduct != null) {
            orderLogic.setApplyCV(typeProduct.isCV());
            orderLogic.setApplyDoanhSo(typeProduct.isDoanhSo());
        } else {
            orderLogic.setApplyCV(true);
            orderLogic.setApplyDoanhSo(true);
        }
    }

    public String getCustomerGuid() {
        return this.customerGuid;
    }

    public void setCustomerGuid(String customerGuid) {
        this.customerGuid = customerGuid;
    }

    public String getCustomerName() {
        return this.customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getProductId() {
        return this.productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public float getQuantity() {
        return quantity;
    }

    public void setQuantity(float quantity) {
        this.quantity = quantity;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Thuoctinhs getProperty() {
        return property;
    }

    public void setProperty(Thuoctinhs property) {
        this.property = property;
    }

    public GiaChinh getMainPrice() {
        return mainPrice;
    }

    public void setMainPrice(GiaChinh mainPrice) {
        this.mainPrice = mainPrice;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTradeMark() {
        return tradeMark;
    }

    public void setTradeMark(String tradeMark) {
        this.tradeMark = tradeMark;
    }

    public String getProductCategoryName() {
        return productCategoryName;
    }

    public void setProductCategoryName(String productCategoryName) {
        this.productCategoryName = productCategoryName;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public int getThumbId() {
        return thumbId;
    }

    public void setThumbId(int thumbId) {
        this.thumbId = thumbId;
    }

    public OrderLogic getOrderLogic() {
        return orderLogic;
    }

    public void setOrderLogic(OrderLogic orderLogic) {
        this.orderLogic = orderLogic;
    }

    public String getProductNote() {
        return productNote;
    }

    public void setProductNote(String productNote) {
        this.productNote = productNote;
    }

    public int getProductCategoryId() {
        return productCategoryId;
    }

    public void setProductCategoryId(int productCategoryId) {
        this.productCategoryId = productCategoryId;
    }
}
