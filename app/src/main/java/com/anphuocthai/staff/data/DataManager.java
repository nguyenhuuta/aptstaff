/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.anphuocthai.staff.data;

import com.anphuocthai.staff.data.db.local.db.DbHelper;
import com.anphuocthai.staff.data.db.local.prefs.PreferencesHelper;
import com.anphuocthai.staff.data.db.network.ApiHelper;
import com.anphuocthai.staff.data.db.network.model.response.UserInfo;
import com.anphuocthai.staff.entities.DepartmentModel;

import java.util.List;

import io.reactivex.subjects.PublishSubject;

/**
 * Created by janisharali on 27/01/17.
 */

public interface DataManager extends DbHelper, PreferencesHelper, ApiHelper {
    void setUserInfo(UserInfo userInfo);

    UserInfo getUserInfo();

    void setDepartment(List<DepartmentModel> list);

    List<DepartmentModel> getListDepartment();

    PublishSubject<Boolean> triggerShowNotification();

}
