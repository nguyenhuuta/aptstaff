package com.anphuocthai.staff.data;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

/**
 * Created by OpenYourEyes on 3/7/2020
 */

// "id": 1,
//         "ten": "Hàng chưng bày",
//         "cv": true,
//         "doanhSo": true,
//         "xoa": false,
//         "createDate": "2020-03-10 21:23:06",
//         "createBy": 0,
//         "updateDate": "2020-03-18 12:04:39",
//         "updateBy": 0
public class TypeProduct {
    @SerializedName("id")
    private int id;
    @SerializedName("ten")
    private String name;

    @SerializedName("cv")
    private boolean isCV;

    @SerializedName("doanhSo")
    private boolean isDoanhSo;

    @SerializedName("xoa")
    private boolean xoa;


    public TypeProduct(int id, String name) {
        this.id = id;
        this.name = name;
    }


    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }


    public boolean isCV() {
        return isCV;
    }

    public void setCV(boolean CV) {
        isCV = CV;
    }

    public boolean isDoanhSo() {
        return isDoanhSo;
    }

    public void setDoanhSo(boolean doanhSo) {
        isDoanhSo = doanhSo;
    }

    public boolean isXoa() {
        return xoa;
    }

    @NonNull
    @Override
    public String toString() {
        return name;
    }
}
