
package com.anphuocthai.staff.data.db.network.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginResponse {

    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("idRecord")
    @Expose
    private String idRecord;
    @SerializedName("objectInfo")
    @Expose
    private UserInfo objectInfo;
    @SerializedName("isSuccess")
    @Expose
    private Boolean isSuccess;



    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(String idRecord) {
        this.idRecord = idRecord;
    }

    public UserInfo getObjectInfo() {
        return objectInfo;
    }

    public void setObjectInfo(UserInfo objectInfo) {
        this.objectInfo = objectInfo;
    }

    public Boolean getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(Boolean isSuccess) {
        this.isSuccess = isSuccess;
    }


}
