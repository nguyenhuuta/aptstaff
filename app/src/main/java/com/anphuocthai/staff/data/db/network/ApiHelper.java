package com.anphuocthai.staff.data.db.network;

import com.anphuocthai.staff.baserequest.APIResponse;
import com.anphuocthai.staff.data.db.network.model.request.LoginRequest;
import com.anphuocthai.staff.data.db.network.model.response.LoginResponse;
import com.anphuocthai.staff.model.ChiTietCongTacResponse;
import com.anphuocthai.staff.model.DanhSachCongTacResponse;
import com.anphuocthai.staff.model.GiaoHangParam;
import com.anphuocthai.staff.model.GiaoHangResponse;
import com.anphuocthai.staff.model.InsertCongTacBody;
import com.anphuocthai.staff.model.InsertCongTacResponse;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;

import org.json.JSONObject;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Observable;

public interface ApiHelper {
    ApiHeader getApiHeader();

    Flowable<List<OrderTran>> getAllDebtApiCall(JSONObject object);

    Observable<LoginResponse> doServerLoginApiCall(LoginRequest request);


    /**
     * Request API
     */

    Observable<APIResponse> banGiaoChungTu(GiaoHangParam param);

    Observable<GiaoHangResponse> layDanhSachDonDat(int userId, int page, int size);

    Observable<DanhSachCongTacResponse> laydanhSachCongTac(String fromDate, String toDate);

    Observable<ChiTietCongTacResponse> chiTietCongTac(int id);

    Observable<InsertCongTacResponse> inserCongTac(InsertCongTacBody body);


}
