package com.anphuocthai.staff.data.address;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.anphuocthai.staff.entities.District;

import java.util.List;

/**
 * Created by DaiKySy on 8/19/19.
 */
@Dao
public interface DistrictDAO {
    @Insert
    void insert(List<District> product);

    @Query("SELECT * FROM District")
    List<District> getDistricts();

    @Query("SELECT * FROM District WHERE tinh_ThanhPho_ID = :cityId")
    List<District> getDistrictsByCity(int cityId);

    @Update
    void update(District product);

    @Delete
    void delete(District product);
}
