package com.anphuocthai.staff.data;

/**
 * Created by DaiKySy on 8/19/19.
 */
public enum ActionRoom {
    INSERT, UPDATE, DELETE
}
