/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.anphuocthai.staff.data.db.local.prefs;

import android.content.Context;

import com.andreacioccarelli.cryptoprefs.CryptoPrefs;
import com.anphuocthai.staff.di.PreferenceInfo;

import javax.inject.Inject;

import static com.anphuocthai.staff.utils.Constants.PREF_ENCODE_CODE;

/**
 * Created by amitshekhar on 07/07/17.
 */

public class AppPreferencesHelper implements PreferencesHelper {

    private static final String PREF_KEY_CURRENT_CUSTOMER_NAME = "customer_name";
    private static final String PREF_KEY_CURRENT_CUSTOMER_GUID = "customer_guid";
    private static final String PREF_KEY_IS_SHOW_PRODUCT_GRID = "is_grid";

    private static final String PREF_KEY_USER_NAME = "user_name";
    private static final String PREF_KEY_PASS_WORD = "pass_word";

    public static final String PREF_KEY_LOGIN_TOKEN = "login_token";
    private static final String PREF_KEY_AUTO_LOGIN = "auto_login";

    private static final String PREF_KEY_PAY_METHOD = "pay_method";

    private static final String PREF_KEY_USER_INFO_ID = "user_info_id";

    public static final String PREF_KEY_USER_GUID = "user_guid";

    private final CryptoPrefs mPrefs;

    @Inject
    public AppPreferencesHelper(Context context, @PreferenceInfo String prefFileName) {
        //mPrefs = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
        mPrefs = new CryptoPrefs(context, prefFileName, PREF_ENCODE_CODE, true);
    }

    @Override
    public void setCurrentCustomerName(String name) {
        if (name == null)
            name = "";
        //mPrefs.edit().putString(PREF_KEY_CURRENT_CUSTOMER_NAME, name).apply();
        mPrefs.put(PREF_KEY_CURRENT_CUSTOMER_NAME, name);
    }

    @Override
    public void setCurrentCustomerGuid(String guid) {
        //mPrefs.edit().putString(PREF_KEY_CURRENT_CUSTOMER_GUID, guid).apply();
        if(guid == null)
            guid = "";
        mPrefs.put(PREF_KEY_CURRENT_CUSTOMER_GUID, guid);

    }

    @Override
    public String getCurrentCustomerName() {
        return mPrefs.getString(PREF_KEY_CURRENT_CUSTOMER_NAME, "");
    }

    @Override
    public String getCurrentCustomerGuid() {
        return mPrefs.getString(PREF_KEY_CURRENT_CUSTOMER_GUID, "");
    }

    @Override
    public void setIsShowGrid(boolean isShowGrid) {
       // mPrefs.edit().putBoolean(PREF_KEY_IS_SHOW_PRODUCT_GRID, isShowGrid).apply();
        mPrefs.put(PREF_KEY_IS_SHOW_PRODUCT_GRID, isShowGrid);
    }

    @Override
    public boolean getIsShowGrid() {
        return mPrefs.getBoolean(PREF_KEY_IS_SHOW_PRODUCT_GRID, true);
    }

    @Override
    public void setUserName(String username) {
        mPrefs.put(PREF_KEY_USER_NAME, username);
    }

    @Override
    public void setPassword(String password) {
        mPrefs.put(PREF_KEY_PASS_WORD, password);
    }

    @Override
    public String getUserName() {
        return mPrefs.getString(PREF_KEY_USER_NAME, "");
    }

    @Override
    public String getPassword() {
        return mPrefs.getString(PREF_KEY_PASS_WORD, "");
    }

    @Override
    public void setToken(String token) {
        mPrefs.put(PREF_KEY_LOGIN_TOKEN, token);
    }

    @Override
    public String getToken() {
        return mPrefs.getString(PREF_KEY_LOGIN_TOKEN, "");
    }

    @Override
    public void setIsAutoLogin(boolean isAutoLogin) {
        mPrefs.put(PREF_KEY_AUTO_LOGIN, isAutoLogin);

    }


    @Override
    public boolean getIsAutoLogin() {
        return  mPrefs.getBoolean(PREF_KEY_AUTO_LOGIN, false);
    }

    @Override
    public void setPayMethod(int payMethod) {
        mPrefs.put(PREF_KEY_PAY_METHOD, payMethod);
    }

    @Override
    public int getPayMethod() {
        return mPrefs.getInt(PREF_KEY_PAY_METHOD, -1);
    }

    @Override
    public void setUserInfoId(int userId) {
        mPrefs.put(PREF_KEY_USER_INFO_ID, userId);
    }

    @Override
    public int getUserInfoId() {
        return mPrefs.getInt(PREF_KEY_USER_INFO_ID, -1);
    }

    @Override
    public void setUserGuid(String guid) {
        mPrefs.put(PREF_KEY_USER_GUID, guid);
    }

    @Override
    public String getUserGuid() {
        return mPrefs.getString(PREF_KEY_USER_GUID, "");
    }


}
