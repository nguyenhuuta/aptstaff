package com.anphuocthai.staff.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.anphuocthai.staff.model.CongTacModel

/**
 * Created by OpenYourEyes on 1/5/2021
 */
@Dao
interface CongTacDAO {
    @Insert
    fun themCongTacs(list: MutableList<CongTacModel>)

    @Query("SELECT * FROM CongTacModel WHERE month = :month")
    fun layCongTacs(month: Int): List<CongTacModel>

    @Query("UPDATE CongTacModel SET totalYeuCau = totalYeuCau + 1 WHERE id = :id")
    fun updateNumber(id: Int)

    @Update
    fun capNhatCongTacs(congTacModel: CongTacModel)
}