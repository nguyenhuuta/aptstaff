package com.anphuocthai.staff.data.db.local.db;

import com.anphuocthai.staff.data.address.DatabaseManager;
import com.anphuocthai.staff.data.db.model.OrderProduct;
import com.anphuocthai.staff.model.CongTacModel;
import com.anphuocthai.staff.model.customer.Customer;

import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class AppDbHelper implements DbHelper {

    private final AppDatabase mAppDatabase;

    @Inject
    public AppDbHelper(AppDatabase appDatabase) {
        this.mAppDatabase = appDatabase;
    }

    @Override
    public Observable<List<OrderProduct>> getAllOrderProducts() {
        return Observable.fromCallable(new Callable<List<OrderProduct>>() {
            @Override
            public List<OrderProduct> call() throws Exception {
                return mAppDatabase.orderProductDao().getAll();
            }
        });
    }

    @Override
    public Observable<Boolean> insertOrderProduct(OrderProduct orderProduct) {
        return Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                mAppDatabase.orderProductDao().insert(orderProduct);
                return true;
            }
        });
    }

    @Override
    public Observable<Boolean> insertOrderProduct(List<OrderProduct> orderProducts) {
        return Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                mAppDatabase.orderProductDao().insert(orderProducts);
                return true;
            }
        });
    }

    @Override
    public Observable<Boolean> updateOrderProduct(OrderProduct orderProduct) {
        return Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                mAppDatabase.orderProductDao().update(orderProduct);
                return true;
            }
        });
    }

    @Override
    public Observable<Boolean> deleteOrderProduct(OrderProduct orderProduct) {
        return Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                mAppDatabase.orderProductDao().delete(orderProduct);
                return true;
            }
        });
    }

    @Override
    public Observable<Boolean> deleteAllOrderProduct() {
        return Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                mAppDatabase.orderProductDao().deleteAll();
                return true;
            }
        });
    }

    @Override
    public Observable<List<Customer>> getAllCurrentCustomers() {
        return Observable.fromCallable(new Callable<List<Customer>>() {
            @Override
            public List<Customer> call() throws Exception {
                return mAppDatabase.currentCustomerDao().getAll();
            }
        });
    }

    @Override
    public Observable<Boolean> insertCurrentCustomer(Customer customer) {
        return Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                mAppDatabase.currentCustomerDao().insert(customer);
                return true;
            }
        });
    }

    @Override
    public Observable<Boolean> updateCurrentCustomer(Customer customer) {
        return Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                mAppDatabase.currentCustomerDao().update(customer);
                return true;
            }
        });
    }

    @Override
    public Observable<Boolean> deleteCurrentCustomer(Customer customer) {
        return Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                mAppDatabase.currentCustomerDao().delete(customer);
                return true;
            }
        });
    }

    @Override
    public Observable<Boolean> deleteAllCurrentCustomer() {
        return Observable.fromCallable(() -> {
            mAppDatabase.currentCustomerDao().deleteAll();
            return true;
        });
    }

    @Override
    public Observable<List<CongTacModel>> queryCongTacByMonth(int month) {
        return Observable.fromCallable(() -> DatabaseManager.getInstance().CongTacDAO().layCongTacs(month));
    }

    @Override
    public void themCongTacs(List<CongTacModel> list) {
        DatabaseManager.getInstance().CongTacDAO().themCongTacs(list);
    }
}
