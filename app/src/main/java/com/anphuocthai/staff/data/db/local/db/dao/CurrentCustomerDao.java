package com.anphuocthai.staff.data.db.local.db.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.anphuocthai.staff.model.customer.Customer;

import java.util.List;

@Dao
public interface CurrentCustomerDao {
    @Query("SELECT * FROM currentsearchcustomer")
    List<Customer> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Customer customer);

    @Update
    void update(Customer customer);

    @Delete
    void delete(Customer customer);

    @Query("DELETE FROM currentsearchcustomer")
    void deleteAll();

}
