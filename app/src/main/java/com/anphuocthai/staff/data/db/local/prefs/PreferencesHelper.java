/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.anphuocthai.staff.data.db.local.prefs;


/**
 * Created by amitshekhar on 07/07/17.
 */

public interface PreferencesHelper {

    void setCurrentCustomerName(String name);
    void setCurrentCustomerGuid(String guid);

    String getCurrentCustomerName();
    String getCurrentCustomerGuid();

    void setIsShowGrid(boolean isShowGrid);
    boolean getIsShowGrid();

    void setUserName(String username);
    void setPassword(String password);

    String getUserName();
    String getPassword();


    void setToken(String token);
    String getToken();

    void setIsAutoLogin(boolean isAutoLogin);
    boolean getIsAutoLogin();

    void setPayMethod(int payMethod);

    int getPayMethod();

    void setUserInfoId(int userId);
    int getUserInfoId();

    void setUserGuid(String guid);
    String getUserGuid();

}
