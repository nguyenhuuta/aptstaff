package com.anphuocthai.staff.data.category;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.anphuocthai.staff.entities.Category;

import java.util.List;

/**
 * Created by DaiKySy on 8/19/19.
 */
@Dao
public interface CategoryDAO {
    @Insert
    void insert(Category product);

    @Query("SELECT * FROM Category")
    List<Category> getCategorys();

    @Query("SELECT * FROM Category WHERE catId = :catId")
    Category getCategoryProducts(int catId);

    @Update
    void update(Category product);

    @Delete
    void delete(Category product);
}
