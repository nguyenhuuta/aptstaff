package com.anphuocthai.staff.data.category;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.anphuocthai.staff.data.ActionRoom;
import com.anphuocthai.staff.entities.Category;
import com.anphuocthai.staff.utils.App;

import java.util.List;

/**
 * Created by DaiKySy on 8/19/19.
 */
@Database(entities = Category.class, version = 1)
public abstract class CategoryDatabase extends RoomDatabase {
    private static final String DATABASE_NAME = "category-database";
    private static CategoryDatabase mInstance;

    public abstract CategoryDAO queryDatabase();


    public static CategoryDatabase getInstance() {
        if (mInstance == null) {
            mInstance = Room.databaseBuilder(App.getInstance().getApplicationContext(), CategoryDatabase.class, DATABASE_NAME)
                    .fallbackToDestructiveMigration()
                    .allowMainThreadQueries()
                    .build();
        }
        return mInstance;
    }

    public void onAction(ActionRoom action, Category product) {
        switch (action) {
            case INSERT:
                if (getCategoryById(product.getCatId()) == null) {
                    queryDatabase().insert(product);
                }
                break;
            case UPDATE:
                queryDatabase().update(product);
                break;
            case DELETE:
                queryDatabase().delete(product);
                break;
        }
    }

    public Category getCategoryById(int catId) {
        return queryDatabase().getCategoryProducts(catId);
    }

    public List<Category> getCategories() {
        return queryDatabase().getCategorys();
    }
}
