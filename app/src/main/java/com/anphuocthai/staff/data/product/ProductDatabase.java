package com.anphuocthai.staff.data.product;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.anphuocthai.staff.data.ActionRoom;
import com.anphuocthai.staff.entities.Product;
import com.anphuocthai.staff.utils.App;

import java.util.List;

/**
 * Created by DaiKySy on 8/19/19.
 */
@Database(entities = Product.class, version = 3)
public abstract class ProductDatabase extends RoomDatabase {
    private static final String DATABASE_NAME = "product-database";
    private static ProductDatabase mInstance;

    abstract ProductDAO queryDatabase();


    public static ProductDatabase getInstance() {
        if (mInstance == null) {
            mInstance = Room.databaseBuilder(App.getInstance().getApplicationContext(), ProductDatabase.class, DATABASE_NAME)
                    .fallbackToDestructiveMigration()
                    .allowMainThreadQueries()
                    .build();
        }
        return mInstance;
    }

    public void onAction(ActionRoom action, Product product) {
        switch (action) {
            case INSERT:
                if (getProducts(product.getCatId(), product.getProductId()) == null) {
                    queryDatabase().insert(product);
                }
                break;
            case UPDATE:
                queryDatabase().update(product);
                break;
            case DELETE:
                queryDatabase().delete(product);
                break;
        }
    }

    public Product getProducts(int catId, int productId) {
        return queryDatabase().getProducts(catId, productId);
    }

    public List<Product> getProducts(int catId) {
        return queryDatabase().getProducts(catId);
    }
}
