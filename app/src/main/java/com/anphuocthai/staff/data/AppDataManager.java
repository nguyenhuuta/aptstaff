/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.anphuocthai.staff.data;


import android.content.Context;

import com.anphuocthai.staff.baserequest.APIResponse;
import com.anphuocthai.staff.data.db.local.db.DbHelper;
import com.anphuocthai.staff.data.db.local.prefs.PreferencesHelper;
import com.anphuocthai.staff.data.db.model.OrderProduct;
import com.anphuocthai.staff.data.db.network.ApiHeader;
import com.anphuocthai.staff.data.db.network.ApiHelper;
import com.anphuocthai.staff.data.db.network.model.request.LoginRequest;
import com.anphuocthai.staff.data.db.network.model.response.LoginResponse;
import com.anphuocthai.staff.data.db.network.model.response.UserInfo;
import com.anphuocthai.staff.di.ApplicationContext;
import com.anphuocthai.staff.entities.DepartmentModel;
import com.anphuocthai.staff.model.ChiTietCongTacResponse;
import com.anphuocthai.staff.model.CongTacModel;
import com.anphuocthai.staff.model.DanhSachCongTacResponse;
import com.anphuocthai.staff.model.GiaoHangParam;
import com.anphuocthai.staff.model.GiaoHangResponse;
import com.anphuocthai.staff.model.InsertCongTacBody;
import com.anphuocthai.staff.model.InsertCongTacResponse;
import com.anphuocthai.staff.model.customer.Customer;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;

import org.json.JSONObject;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by janisharali on 27/01/17.
 */

@Singleton
public class AppDataManager implements DataManager {

    private static final String TAG = AppDataManager.class.getSimpleName();
    private UserInfo userInfo;
    private List<DepartmentModel> listDepartment;
    private final Context mContext;

    private final DbHelper mDbHelper;

    private final PreferencesHelper mPreferencesHelper;

    private final ApiHelper mApiHelper;

    private PublishSubject<Boolean> triggerShowNotification;

    @Inject
    public AppDataManager(@ApplicationContext Context context,
                          DbHelper dbHelper,
                          PreferencesHelper preferencesHelper,
                          ApiHelper apiHelper) {
        mContext = context;
        mDbHelper = dbHelper;
        mPreferencesHelper = preferencesHelper;
        mApiHelper = apiHelper;

    }

    @Override
    public PublishSubject<Boolean> triggerShowNotification() {
        if (triggerShowNotification == null) {
            triggerShowNotification = PublishSubject.create();
        }
        return triggerShowNotification;
    }

    @Override
    public Observable<List<OrderProduct>> getAllOrderProducts() {
        return mDbHelper.getAllOrderProducts();
    }

    @Override
    public Observable<Boolean> insertOrderProduct(OrderProduct orderProduct) {
        return mDbHelper.insertOrderProduct(orderProduct);
    }

    @Override
    public Observable<Boolean> insertOrderProduct(List<OrderProduct> orderProducts) {
        return mDbHelper.insertOrderProduct(orderProducts);
    }

    @Override
    public Observable<Boolean> updateOrderProduct(OrderProduct orderProduct) {
        return mDbHelper.updateOrderProduct(orderProduct);
    }

    @Override
    public Observable<Boolean> deleteOrderProduct(OrderProduct orderProduct) {
        return mDbHelper.deleteOrderProduct(orderProduct);
    }

    @Override
    public Observable<Boolean> deleteAllOrderProduct() {
        setCurrentCustomerGuid(null);
        setCurrentCustomerName(null);
        setPayMethod(-1);
        return mDbHelper.deleteAllOrderProduct();
    }

    @Override
    public Observable<List<Customer>> getAllCurrentCustomers() {
        return mDbHelper.getAllCurrentCustomers();
    }

    @Override
    public Observable<Boolean> insertCurrentCustomer(Customer customer) {
        return mDbHelper.insertCurrentCustomer(customer);
    }

    @Override
    public Observable<Boolean> updateCurrentCustomer(Customer customer) {
        return mDbHelper.updateCurrentCustomer(customer);
    }

    @Override
    public Observable<Boolean> deleteCurrentCustomer(Customer customer) {
        return mDbHelper.deleteCurrentCustomer(customer);
    }

    @Override
    public Observable<Boolean> deleteAllCurrentCustomer() {
        return mDbHelper.deleteAllCurrentCustomer();
    }


    @Override
    public void setCurrentCustomerName(String name) {
        mPreferencesHelper.setCurrentCustomerName(name);
    }

    @Override
    public void setCurrentCustomerGuid(String guid) {
        mPreferencesHelper.setCurrentCustomerGuid(guid);
    }

    @Override
    public String getCurrentCustomerName() {
        return mPreferencesHelper.getCurrentCustomerName();
    }

    @Override
    public String getCurrentCustomerGuid() {
        return mPreferencesHelper.getCurrentCustomerGuid();
    }

    @Override
    public void setIsShowGrid(boolean isShowGrid) {
        mPreferencesHelper.setIsShowGrid(isShowGrid);
    }

    @Override
    public boolean getIsShowGrid() {
        return mPreferencesHelper.getIsShowGrid();
    }

    @Override
    public void setUserName(String username) {
        mPreferencesHelper.setUserName(username);
    }

    @Override
    public void setPassword(String password) {
        mPreferencesHelper.setPassword(password);
    }

    @Override
    public String getUserName() {
        return mPreferencesHelper.getUserName();
    }

    @Override
    public String getPassword() {
        return mPreferencesHelper.getPassword();
    }

    @Override
    public void setToken(String token) {
        mPreferencesHelper.setToken(token);
    }

    @Override
    public String getToken() {
        return mPreferencesHelper.getToken();
    }

    @Override
    public void setIsAutoLogin(boolean isAutoLogin) {
        mPreferencesHelper.setIsAutoLogin(isAutoLogin);
    }

    @Override
    public boolean getIsAutoLogin() {
        return mPreferencesHelper.getIsAutoLogin();
    }

    @Override
    public void setPayMethod(int payMethod) {
        mPreferencesHelper.setPayMethod(payMethod);
    }

    @Override
    public int getPayMethod() {
        return mPreferencesHelper.getPayMethod();
    }

    @Override
    public void setUserInfoId(int userId) {
        mPreferencesHelper.setUserInfoId(userId);
    }

    @Override
    public int getUserInfoId() {
        return mPreferencesHelper.getUserInfoId();
    }

    @Override
    public void setUserGuid(String guid) {
        mPreferencesHelper.setUserGuid(guid);
    }

    @Override
    public String getUserGuid() {
        return mPreferencesHelper.getUserGuid();
    }

    @Override
    public ApiHeader getApiHeader() {
        return null;
    }

    @Override
    public Flowable<List<OrderTran>> getAllDebtApiCall(JSONObject object) {
        return mApiHelper.getAllDebtApiCall(object);
    }

    @Override
    public Observable<LoginResponse> doServerLoginApiCall(LoginRequest request) {
        return mApiHelper.doServerLoginApiCall(request);
    }

    @Override
    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    @Override
    public UserInfo getUserInfo() {
        return userInfo;
    }

    @Override
    public void setDepartment(List<DepartmentModel> list) {
        listDepartment = list;
    }

    @Override
    public List<DepartmentModel> getListDepartment() {
        return listDepartment;
    }


    @Override
    public Observable<APIResponse> banGiaoChungTu(GiaoHangParam param) {
        return mApiHelper.banGiaoChungTu(param);
    }

    @Override
    public Observable<GiaoHangResponse> layDanhSachDonDat(int userId, int page, int size) {
        return mApiHelper.layDanhSachDonDat(userId, page, size);
    }

    @Override
    public Observable<DanhSachCongTacResponse> laydanhSachCongTac(String fromDate, String toDate) {
        return mApiHelper.laydanhSachCongTac(fromDate, toDate);
    }

    @Override
    public Observable<ChiTietCongTacResponse> chiTietCongTac(int id) {
        return mApiHelper.chiTietCongTac(id);
    }


    @Override
    public Observable<InsertCongTacResponse> inserCongTac(InsertCongTacBody body) {
        return mApiHelper.inserCongTac(body);
    }

    @Override
    public Observable<List<CongTacModel>> queryCongTacByMonth(int month) {
        return mDbHelper.queryCongTacByMonth(month);
    }

    @Override
    public void themCongTacs(List<CongTacModel> list) {
        mDbHelper.themCongTacs(list);
    }
}
