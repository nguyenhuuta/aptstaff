package com.anphuocthai.staff.data.address;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.anphuocthai.staff.entities.Cities;

import java.util.List;

/**
 * Created by DaiKySy on 8/19/19.
 */
@Dao
public interface CityDAO {
    @Insert
    void insertCities(List<Cities> cities);

    @Query("SELECT * FROM Cities")
    List<Cities> getCities();

    @Update
    void updateCities(Cities cities);

    @Delete
    void deleteCities(Cities cities);
}
