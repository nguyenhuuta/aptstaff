package com.anphuocthai.staff.data

import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import io.reactivex.functions.Function
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import java.util.concurrent.Callable


/**
 * Created by OpenYourEyes on 9/4/2020
 */
class RxProgressBar() : Observable<Boolean>() {

    private val variable = BehaviorSubject.create<Int>()

    private var loading: Observable<Boolean>

    init {
        loading = variable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturnItem(0)
                .map<Boolean> { x -> x > 0 }
                .share()
    }

    fun <T> trackActivity(source: Observable<T>): Observable<T> {
        val resourceFactory: Callable<Unit> = Callable {
            increment()
            Unit.default
        }

        val observableFactory = Function<Unit, Observable<T>> { source }
        val disposer = Consumer<Unit> { decrement() }
        return using(resourceFactory, observableFactory, disposer)
    }

    override fun subscribeActual(observer: Observer<in Boolean>) {
        loading.subscribe(observer)
    }

    private fun increment() {
        variable.onNext((variable.value ?: 0) + 1)
    }

    private fun decrement() {
        variable.onNext((variable.value ?: 0) - 1)
    }
}

fun <T> Observable<T>.trackProgressBar(trackIndicator: RxProgressBar): Observable<T> {
    return trackIndicator.trackActivity(this)
}


class Unit : Comparable<Unit> {

    companion object {
        val default = Unit()
    }

    override fun compareTo(other: Unit): Int {
        return 0
    }

    override fun equals(other: Any?): Boolean {
        return other is Unit
    }

    override fun hashCode(): Int {
        return 0
    }
}
