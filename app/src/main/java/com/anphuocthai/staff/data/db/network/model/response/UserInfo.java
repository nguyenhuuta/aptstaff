
package com.anphuocthai.staff.data.db.network.model.response;

import com.anphuocthai.staff.model.CauHinhModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserInfo {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("guid")
    @Expose
    private String guid;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("fullname")
    @Expose
    private String fullname;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("isAdmin")
    @Expose
    private Boolean isAdmin;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("appName")
    @Expose
    private String appName;
    @SerializedName("ipTruyCap")
    @Expose
    private String ipTruyCap;
    @SerializedName("ngayTruyCap")
    @Expose
    private String ngayTruyCap;
    @SerializedName("anhDaiDienId")
    @Expose
    private Integer anhDaiDienId;

    @SerializedName("roleName")
    @Expose
    private String roleName;

    @SerializedName("cauHinh")
    CauHinhModel cauHinhModel;

    public CauHinhModel getCauHinhModel() {
        return cauHinhModel;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Boolean getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(Boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getIpTruyCap() {
        return ipTruyCap;
    }

    public void setIpTruyCap(String ipTruyCap) {
        this.ipTruyCap = ipTruyCap;
    }

    public String getNgayTruyCap() {
        return ngayTruyCap;
    }

    public void setNgayTruyCap(String ngayTruyCap) {
        this.ngayTruyCap = ngayTruyCap;
    }

    public Integer getAnhDaiDienId() {
        return anhDaiDienId;
    }

    public void setAnhDaiDienId(Integer anhDaiDienId) {
        this.anhDaiDienId = anhDaiDienId;
    }


    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    @Override
    public String toString() {
        return "UserInfo{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", guid='" + guid + '\'' +
                ", password='" + password + '\'' +
                ", fullname='" + fullname + '\'' +
                ", email='" + email + '\'' +
                ", mobile='" + mobile + '\'' +
                ", status=" + status +
                ", isAdmin=" + isAdmin +
                ", token='" + token + '\'' +
                ", appName='" + appName + '\'' +
                ", ipTruyCap='" + ipTruyCap + '\'' +
                ", ngayTruyCap='" + ngayTruyCap + '\'' +
                ", anhDaiDienId=" + anhDaiDienId +
                ", roleName='" + roleName + '\'' +
                '}';
    }
}
