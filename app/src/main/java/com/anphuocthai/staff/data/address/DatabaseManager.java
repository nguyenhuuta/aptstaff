package com.anphuocthai.staff.data.address;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.anphuocthai.staff.data.CongTacDAO;
import com.anphuocthai.staff.entities.Cities;
import com.anphuocthai.staff.entities.District;
import com.anphuocthai.staff.entities.Wards;
import com.anphuocthai.staff.model.CongTacModel;
import com.anphuocthai.staff.utils.App;

import java.util.List;

/**
 * Created by DaiKySy on 8/19/19.
 */
@Database(entities = {Cities.class, District.class, Wards.class, CongTacModel.class}, version = 2)
public abstract class DatabaseManager extends RoomDatabase {
    private static final String DATABASE_NAME = "apt-database";
    private static DatabaseManager mInstance;

    abstract CityDAO cityDAO();

    abstract DistrictDAO districtDAO();

    abstract WardDAO wardDAO();

    public abstract CongTacDAO CongTacDAO();


    public static DatabaseManager getInstance() {
        if (mInstance == null) {
            mInstance = Room.databaseBuilder(App.getInstance().getApplicationContext(), DatabaseManager.class, DATABASE_NAME)
                    .fallbackToDestructiveMigration()
                    .allowMainThreadQueries()
                    .build();
        }
        return mInstance;
    }

    /*
     * City
     * */
    public void insertCity(List<Cities> list) {
        cityDAO().insertCities(list);
    }

    public List<Cities> getAllCities() {
        return cityDAO().getCities();
    }

    /*
     * District
     * */
    public void inserDistrict(List<District> districts) {
        districtDAO().insert(districts);
    }

    public List<District> getDistrict() {
        return districtDAO().getDistricts();
    }

    public List<District> getDistrict(int cityId) {
        return districtDAO().getDistrictsByCity(cityId);
    }

    /*
     * Wards
     * */

    public void insertWard(List<Wards> wardsList) {
        wardDAO().insert(wardsList);

    }

    public List<Wards> getWards() {
        return wardDAO().getWards();
    }

    public List<Wards> getWardsByDistrictId(int districtID) {
        return wardDAO().getWards(districtID);
    }
}
