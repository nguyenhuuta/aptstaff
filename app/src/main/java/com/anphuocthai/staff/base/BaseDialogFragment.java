package com.anphuocthai.staff.base;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.anphuocthai.staff.R;

/**
 * Created by DaiKySy on 8/29/19.
 */
public abstract class BaseDialogFragment extends DialogFragment {
    private View rootView;
    public TextView mTitleDialog, mButtonOK;
    public ImageView mButtonCancel;
    private ProgressBar mProgress;

    @Override
    public void onStart() {
        super.onStart();
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        getDialog().getWindow().setLayout((6 * width) / 7, LinearLayout.LayoutParams.WRAP_CONTENT);//(4 * height) / 5
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.base_dialog, container, false);
        mTitleDialog = rootView.findViewById(R.id.titleDialog);
        mButtonCancel = rootView.findViewById(R.id.buttonCancel);
        mButtonOK = rootView.findViewById(R.id.buttonOK);
        mProgress = rootView.findViewById(R.id.progressBar);

        mTitleDialog.setText(getTitleDialog());
        mButtonOK.setText(getTextButtonOK());

        FrameLayout relativeLayout = rootView.findViewById(R.id.rootView);
        View view = inflater.inflate(getContentView(), container, false);
        relativeLayout.addView(view);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setCancelable(dismissClickOutside());
        initView(rootView);
        initAction();
        initData();

        mButtonOK.setOnClickListener(view1 -> onButtonOKClick());
        mButtonCancel.setOnClickListener(view1 -> onButtonCancelClick());
        return rootView;
    }


    public abstract int getContentView();

    public abstract void initView(View view);

    public abstract void initAction();

    public abstract void initData();

    public abstract void onButtonOKClick();

    public void onButtonCancelClick() {
        hide();
    }


    public boolean dismissClickOutside() {
        return true;
    }


    public String getTitleDialog() {
        return "";
    }

    public String getTextButtonOK() {
        return getString(R.string.button_ok);
    }

    public void setTitleDialog(String text) {
        mTitleDialog.setText(text);
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        FragmentTransaction ft = manager.beginTransaction();
        ft.add(this, tag);
        ft.commitAllowingStateLoss();
    }

    public void hide() {
        dismiss();
    }

    public IBaseCallback mCallbackDialog;

    public void setCallbackDialog(IBaseCallback mCallbackDialog) {
        this.mCallbackDialog = mCallbackDialog;
    }

    public void showLoading() {
        mProgress.setVisibility(View.VISIBLE);
        mButtonOK.setEnabled(false);
    }

    public void hideLoading() {
        mProgress.setVisibility(View.GONE);
        mButtonOK.setEnabled(true);
    }
}
