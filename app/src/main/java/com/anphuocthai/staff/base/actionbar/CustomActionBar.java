package com.anphuocthai.staff.base.actionbar;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.DrawableRes;
import androidx.core.content.ContextCompat;

import com.anphuocthai.staff.R;

/**
 * Created by DaiKySy on 8/27/19.
 */
public class CustomActionBar extends LinearLayout {
    private ImageView mButtonLeft, mButtonRight;
    private TextView mTitleActionBar;
    private ICustomActionBar customActionBar;

    public CustomActionBar(Context context) {
        super(context);
    }


    public CustomActionBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate(context, R.layout.custom_action_bar, this);
        initView();
        initAction();
    }

    public CustomActionBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    private void initView() {
        mButtonLeft = findViewById(R.id.buttonLeft);
        mTitleActionBar = findViewById(R.id.titleActionBar);
        mButtonRight = findViewById(R.id.buttonRight);
    }

    private void initAction() {
        mButtonRight.setOnClickListener(v -> customActionBar.onButtonRightClick());
        mButtonLeft.setOnClickListener(v -> customActionBar.onButtonLeftClick());
    }


    public void setTitle(String title) {
        mTitleActionBar.setText(title);
    }


    public void setCustomActionBar(ICustomActionBar customActionBar) {
        this.customActionBar = customActionBar;
    }

    public void showLeftRightButton(boolean isShowLeft, boolean isShowRight) {
        mButtonLeft.setVisibility(isShowLeft ? VISIBLE : INVISIBLE);
        mButtonRight.setVisibility(isShowRight ? VISIBLE : INVISIBLE);
    }

    public void setImageRight(@DrawableRes int drawableRes) {
        mButtonRight.setVisibility(VISIBLE);
        mButtonRight.setImageDrawable(ContextCompat.getDrawable(getContext(), drawableRes));
    }


    public interface ICustomActionBar {
        void onButtonLeftClick();

        default void onButtonRightClick() {

        }
    }


}
