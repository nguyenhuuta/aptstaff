package com.anphuocthai.staff.api;

import android.util.Log;
import android.widget.ImageView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.anphuocthai.staff.R;
import com.anphuocthai.staff.interfaces.IAPICallback;
import com.anphuocthai.staff.interfaces.IAPIInterface;
import com.rx2androidnetworking.Rx2AndroidNetworking;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

import io.reactivex.Observable;

import static com.anphuocthai.staff.api.ApiURL.IMAGE_VIEW_URL;

public class NetworkManager implements IAPIInterface {

    private static final String TAG = NetworkManager.class.getSimpleName();

    private static NetworkManager mInstance = new NetworkManager();

    public static NetworkManager getInstance() {
        if (mInstance == null) {
            mInstance = new NetworkManager();
        }
        return mInstance;
    }

    @Override
    public void sendGetRequestNotAuthen(String url, HashMap hashMap, boolean isResponseObject, IAPICallback iapiCallback) {
        if (isResponseObject) {
            AndroidNetworking.get(url)
                    .addHeaders(ApiURL.getBaseHeader())
                    .addQueryParameter(hashMap)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            iapiCallback.onResponseSuccess(response);
                        }

                        @Override
                        public void onError(ANError anError) {
                            iapiCallback.onResponseError(anError);
                        }
                    });
        } else {
            AndroidNetworking.get(url)
                    .addHeaders(ApiURL.getBaseHeader())
                    .addQueryParameter(hashMap)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONArray(new JSONArrayRequestListener() {
                        @Override
                        public void onResponse(JSONArray response) {
                            iapiCallback.onResponseSuccess(response);
                        }

                        @Override
                        public void onError(ANError anError) {
                            iapiCallback.onResponseError(anError);
                        }
                    });
        }

    }


    @Override
    public void sendPostRequest(String url, JSONObject jsonObject, IAPICallback iapiCallback) {
        AndroidNetworking.post(url)
                .addHeaders(ApiURL.getBaseHeader())
                .addHeaders(ApiURL.getTokenHeader())
                .addJSONObjectBody(jsonObject)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        iapiCallback.onResponseSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        iapiCallback.onResponseError(anError);
                    }
                });
    }

    @Override
    public void sendPostJsonRequest(String url, Object jsonObject, IAPICallback iapiCallback) {
        AndroidNetworking.post(url)
                .addHeaders(ApiURL.getBaseHeader())
                .addHeaders(ApiURL.getTokenHeader())
                .addBodyParameter(jsonObject)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        iapiCallback.onResponseSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        iapiCallback.onResponseError(anError);
                    }
                });
    }

    @Override
    public void sendPostRequestWithArrayResponse(String url, JSONObject jsonObject, IAPICallback iapiCallback) {
        AndroidNetworking.post(url)
                .addHeaders(ApiURL.getBaseHeader())
                //.addHeaders("Authorization", "Bearer " + Singleton.getInstance().login.getObjectInfo().getToken())
                .addHeaders(ApiURL.getTokenHeader())
                .addJSONObjectBody(jsonObject)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        //Log.d(TAG, response.toString());
                        iapiCallback.onResponseSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        iapiCallback.onResponseError(anError);
                    }
                });
    }

    @Override
    public void sendPostRequestWithQueryParam(String url, HashMap hashMap, IAPICallback iapiCallback) {
        AndroidNetworking.post(url)
                .addHeaders(ApiURL.getBaseHeader())
                //.addHeaders("Authorization", "Bearer " + Singleton.getInstance().login.getObjectInfo().getToken())
                .addHeaders(ApiURL.getTokenHeader())
                .addQueryParameter(hashMap)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        iapiCallback.onResponseSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        iapiCallback.onResponseError(anError);
                    }
                });

    }

    @Override
    public void sendPostRequestNotAuthen(String url, JSONObject jsonObject, boolean isObjectResponse, IAPICallback iapiCallback) {
        if (isObjectResponse) {
            AndroidNetworking.post(url)
                    .addHeaders(ApiURL.getBaseHeader())
                    .addJSONObjectBody(jsonObject)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            iapiCallback.onResponseSuccess(response);
                        }

                        @Override
                        public void onError(ANError anError) {
                            iapiCallback.onResponseError(anError);
                        }
                    });

        } else {
            AndroidNetworking.post(url)
                    .addHeaders(ApiURL.getBaseHeader())
                    .addJSONObjectBody(jsonObject)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONArray(new JSONArrayRequestListener() {
                        @Override
                        public void onResponse(JSONArray response) {
                            iapiCallback.onResponseSuccess(response);
                        }

                        @Override
                        public void onError(ANError anError) {
                            iapiCallback.onResponseError(anError);
                        }
                    });
        }

    }

    @Override
    public void sendGetRequest(String url, HashMap hashMap, IAPICallback iapiCallback) {
        AndroidNetworking.get(url)
                .addHeaders(ApiURL.getBaseHeader())
                .addHeaders(ApiURL.getTokenHeader())
                .addQueryParameter(hashMap)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        iapiCallback.onResponseSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        iapiCallback.onResponseError(anError);
                    }
                });
    }

    @Override
    public void loadImage(int imgId, ImageView imageView) {
        Picasso.get()
                .load(IMAGE_VIEW_URL + imgId)
                .placeholder(R.drawable.ic_no_image)
                .fit()
                .centerCrop()
                //.networkPolicy(NetworkPolicy.OFFLINE)
                .into(imageView);
    }

    @Override
    public void sendGetRequestObjectResponse(String url, HashMap hashMap, IAPICallback iapiCallback) {
        AndroidNetworking.get(url)
                .addHeaders(ApiURL.getBaseHeader())
                //.addHeaders("Authorization", "Bearer " + Singleton.getInstance().login.getObjectInfo().getToken())
                .addHeaders(ApiURL.getTokenHeader())
                .addQueryParameter(hashMap)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        iapiCallback.onResponseSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        iapiCallback.onResponseError(anError);
                    }
                });
    }

    @Override
    public <T> Observable<T> postRequest(String url, Object object, Class<T> response) {
        return Rx2AndroidNetworking.post(url)
                .addHeaders(ApiURL.getHeader())
                .addApplicationJsonBody(object)
                .build()
                .getObjectObservable(response);

    }


    @Override
    public void sendPostRequestWithObject(String url, JSONArray jsonArray, IAPICallback iapiCallback) {
        AndroidNetworking.post(url)
                .addHeaders(ApiURL.getBaseHeader())
                .addHeaders(ApiURL.getTokenHeader())
                .addJSONArrayBody(jsonArray)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        iapiCallback.onResponseSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {
                        iapiCallback.onResponseError(anError);
                    }
                });
    }


}
