package com.anphuocthai.staff.api;

import com.anphuocthai.staff.BuildConfig;
import com.anphuocthai.staff.data.DataManager;
import com.anphuocthai.staff.utils.App;

import java.util.HashMap;

import javax.inject.Inject;

public class ApiURL {

    @Inject
    DataManager dataManager;


    public static final String BASE_URL = BuildConfig.HOST;

    public static final String LOGIN_URL = BASE_URL + "AuthenSvc/Login";

    // change password..
    public static final String CHANGE_PASS_URL = BASE_URL + "AdminSvc/changePassword";

    // user Hồ sơ
    public static final String USER_HO_SO_URL = BASE_URL + "AdminSvc/getUserHoSo";

    // user Hợp đồng
    public static final String USER_HOP_DONG_URL = BASE_URL + "AdminSvc/getUserHopDong";

    // tài khoản ngân hàng
    public static final String TAI_KHOAN_NGAN_HANG_URL = BASE_URL + "AdminSvc/getTaiKhoanNganHang";


    // location
    public static final String UPDATE_LOCATION_URL = BASE_URL + "CommonSvc/updLocation";
    public static final String GET_LOCATION_URL = BASE_URL + "CommonSvc/getLocation";

    /*
     *
     * Danh sách khách hàng đối tác
     *
     * */

    // Tạo mới thông tin khách hàng


    public static final String CREATE_CUSTOMER_URL = BASE_URL + "MemberSvc/regMember";

    public static final String CHANGE_CUSTOMER_TYPE = BASE_URL + "MemberSvc/updInfoPhanLoaiKhachHang";

    public static final String UPDATE_CUSTOMER_URL = BASE_URL + "MemberSvc/UpdatedThanhVien";
    public static final String GET_CUSTOMER_BY_GUID_URL = BASE_URL + "MemberSvc/getMemberByGuid";
    public static final String GET_CUSTOMER_BY_CATEGORY_URL = BASE_URL + "MemberSvc/searchMember";
    public static final String GET_ADDRESS_BY_MEMBER_URL = BASE_URL + "MemberSvc/getAddressByMember";
    public static final String UPDATE_ADDRESS_URL = BASE_URL + "MemberSvc/updAddress";
    public static final String GET_NOTE_CUSTOMER = BASE_URL + "MemberSvc/getLogs";
    public static final String UPDATE_CONFIG_INFO_URL = BASE_URL + "MemberSvc/" + "updConfigInfo";

    /*
     *
     * Lịch làm việc với khách hàng
     * */

    public static final String CREATE_OR_UPDATE_WORK_SCHEDULE_URL = BASE_URL + "AdminSvc/updLichLamViec";
    public static final String SEARCH_WORK_SCHEDULE_URL = BASE_URL + "AdminSvc/searchLichLamViec";
    public static final String GET_WORK_SCHEDULE_URL = BASE_URL + "AdminSvc/getLichLamViec";
    public static final String DELETE_WORK_SCHEDULE_URL = BASE_URL + "AdminSvc/delLichLamViec";
    public static final String UPLOAD_FILE_WORK_SCHEDULE_URL = BASE_URL + "AdminSvc/uploadFile";
    public static final String GET_FILES_WORK_SCHEDULE_URL = BASE_URL + "AdminSvc/getFiles";
    public static final String DELETE_FILE_WORK_SCHEDULE_URL = BASE_URL + "AdminSvc/delFile";


    /**
     * image view
     */

    public static final String IMAGE_VIEW_URL = "http://admin.anphuocthai.vn/Common/ImageFile.aspx?id=";
    public static final String IMAGE_VIEW_URL2 = "http://res.anphuocthai.vn/images/HangHoa/";

    /**
     * Order
     */

    public static final String GET_ALL_CATEGORY_COMMODITY_URL = BASE_URL + "CommonSvc/getAllNhomHangHoa";
    public static final String GET_ALL_COMMODITY_IN_CATEGORY_URL = BASE_URL + "CommonSvc/searchHangHoa";
    public static final String GET_COMMODITY_BY_ID = BASE_URL + "CommonSvc/getHangHoa";
    public static final String CREATE_ORDER_URL = BASE_URL + "OrderSvc/createDonDat";
    public static final String GET_ORDER_URL = BASE_URL + "OrderSvc/searchDonDat";

    public static final String DEL_ORDER_URL = BASE_URL + "OrderSvc/delDonDat";


    public static final String SET_TRANSPORT_URL = BASE_URL + "OrderSvc/setVanChuyen";
    public static final String START_TRANSPORT_URL = BASE_URL + "OrderSvc/startVanChuyen";
    public static final String STOP_TRANSPORT_URL = BASE_URL + "OrderSvc/stopVanChuyen";
    public static final String RETURN_TRANSPORT_URL = BASE_URL + "OrderSvc/returnVanChuyen";


    /**
     * debt
     */
    public static final String SET_DEBT_TRANSPORT_URL = BASE_URL + "OrderSvc/setDebtVanChuyen";
    public static final String STOP_DEBT_TRANSPORT_URL = BASE_URL + "OrderSvc/stopDebtVanChuyen";


    /**
     * Home page statistics
     */

    public static final String STATISTIC_URL = BASE_URL + "AdminSvc/getTongHop";
    public static final String STATISTIC_ORDER_BY_DAY_URL_DYNAMIC = BASE_URL + "AdminSvc/getThongKeDonHangTheoNgayV1";
    public static final String STATISTIC_ORDER_BY_TOP_CUSTOMER_DYNAMIC = BASE_URL + "AdminSvc/getThongKeDonHangThanhVienV1";
    public static final String STATISTIC_REPORT_SELL = BASE_URL + "AdminSvc/getBaoCaoBanHang";

    /**
     * Sync contacts
     *
     * @return
     */
    public static final String SYNC_MULTI_CONTACTS = BASE_URL + "AdminSvc/syncContacts";


    /**
     * Notifications
     *
     * @return
     */

    public static final String NOTIFICATION_SVC = BASE_URL + "NotificationSvc/";

    public static final String GET_ALL_NOTIFICATION = NOTIFICATION_SVC + "getNotifications";


    public static final String GET_UNREAD_NOTIFICATION = NOTIFICATION_SVC + "getUnRead";

    public static final String GET_DYNAMIC_SUMARY_HOME = BASE_URL + "AdminSvc/getWidgetInfo";


    public static final String GET_REPORT = BASE_URL + "AdminSvc/getBaoCao";
    public static final String GET_REPORT_JSON = BASE_URL + "AdminSvc/getBaoCaoList";

    public static final String GET_DETAIL_ORDER_URL = BASE_URL + "OrderSvc/getDonDatWidget";


    public static final String ChamCongAPI = BASE_URL + "BackendSvc/Users_DuLieuChamCongOnline_Insert";
    public static final String HistoryTimeKeepingAPI = BASE_URL + "BackendSvc/ChiTietChamCongApp";
    public static final String BANNER_API = "CommonSvc/GetSlideImageForApp";
    public static final String DetailBanner = "CommonSvc/GetBaiVietBySlide";
    public static final String QuyCheLuong = "CommonSvc/QuyCheLuong";
    public static final String TonghopLuong = "BackendSvc/GetTongHopLuong_TheoThang_Ext";
    public static final String ChiTietPhuCap = "BackendSvc/NS_LuongCoBan_GetListPhuCapByUser";
    public static final String QuyCheHanhChinhNhanSu = "AdminSvc/QuyCheHanhChinhNhanSu";


    /*Công nợ*/
    public static final String allCongNo = BASE_URL + "CongNo/getAllCongNo";
    public static final String danhSachCongNo = BASE_URL + "CongNo/getDanhSachQLCongNo";
    public static final String ThuCongNo = BASE_URL + "CongNo/ThietLapTienThuCongNo";
    public static final String traCongNo = BASE_URL + "CongNo/TraChungTuCongNoThuCongNo ";
    public static final String ChuyenCongNoSangChoTra = BASE_URL + "CongNo/UpdateChuyenThuTien";
    public static final String yeuCauMuonCongNo = BASE_URL + "CongNo/ThietLapCongNo";


    public static final String danhMucDungThu = BASE_URL + "DanhMuc/getAllDanhMucHangHoaDungThu";
    public static final String listChietKhau = BASE_URL + "DanhMuc/getAllDM_ChietKhau";


    public static final String banGiaoChungTu = BASE_URL + "BackendSvc/DD_DonDat_BanGiaoChungTu";
    public static final String getDanhSachChuaBanGiao = BASE_URL + "BackendSvc/GetDanhSachDonDaDiChuaBanGiao";

    public static final String tongHopCong = "BackendSvc/GetTongHopCong_TheoThang_Ext";
    public static final String thongTinChamgCong = "BackendSvc/GetThongTinChamCongTrongThang_Ext";

    public static final String danhSachBaoCao = BASE_URL + "AdminSvc/Users_DuLieuChamCong_ChuaXacNhan";
    public static final String chiTietCongTac = BASE_URL + "AdminSvc/ChamCongLyDoDiCongTac_GetById";
    public static final String insertBaoCao = BASE_URL + "AdminSvc/ChamCongLyDoDiCongTac_Add";


    public static HashMap<String, String> getBaseHeader() {
        HashMap<String, String> header = new HashMap<>();
        header.put("Content-Type", "application/x-www-form-urlencoded");
        return header;
    }

    public static HashMap<String, String> getTokenHeader() {
        HashMap<String, String> header = new HashMap<>();
        try {
            header.put("Authorization", "Bearer " + App.getUserInfo().getToken());
        } catch (Exception e) {
            header.put("Authorization", "Bearer " + App.getComponent().getDataManager().getToken());
            e.printStackTrace();
            return header;
        }

        return header;
    }


    public static HashMap<String, String> getHeader() {
        HashMap<String, String> header = new HashMap<>();
        header.put("Content-Type", "application/json");
        try {
            header.put("Authorization", "Bearer " + App.getUserInfo().getToken());
        } catch (Exception e) {
            header.put("Authorization", "Bearer " + App.getComponent().getDataManager().getToken());
            e.printStackTrace();
            return header;
        }

        return header;
    }
}
