package com.anphuocthai.staff.api;


import com.anphuocthai.staff.interfaces.login.ILoginParam;

import org.json.JSONException;
import org.json.JSONObject;

public class ParamFactory implements ILoginParam{

    @Override
    public JSONObject makeLoginParam(String username, String password, String app) {
        JSONObject loginObject = new JSONObject();
        try {
            loginObject.put("username", username);
            loginObject.put("password", password);
            loginObject.put("app", app);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return loginObject;
    }


}
