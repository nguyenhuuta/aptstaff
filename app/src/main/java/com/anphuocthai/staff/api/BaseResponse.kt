package com.anphuocthai.staff.api

import com.rx2androidnetworking.Rx2ANRequest
import com.rx2androidnetworking.Rx2AndroidNetworking
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * Created by OpenYourEyes on 7/21/2020
 */
open class BaseResponse<T> {
    var code: String? = ""
    var message: String? = ""
    var idRecord: String? = ""
    var objectInfo: T? = null
    var isSuccess: Boolean? = false

    fun isSuccess(): Boolean {
        if (true == isSuccess) {
            return true
        }
        return false
    }
}


data class ErrorMessage(private val message: String?, val code: Int = 0) {
    fun message() = message ?: "Có lỗi xảy ra \n Vui lòng thử lại sau"
}


fun <T> Observable<T>.runOnMain(): Observable<T> {
    return subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
}

fun Disposable.addTo(compositeDisposable: CompositeDisposable) {
    compositeDisposable.add(this)
}

fun String.postRequest(): Rx2ANRequest.PostRequestBuilder {
    return Rx2AndroidNetworking.post(ApiURL.BASE_URL + this)
            .addHeaders(ApiURL.getBaseHeader())
            .addHeaders(ApiURL.getTokenHeader())

}


fun <T> Rx2ANRequest.PostRequestBuilder.get(clazz: Class<T>): Observable<T> {
    return this.build()
            .getObjectObservable(clazz)
}


fun String.getRequest(): Rx2ANRequest.GetRequestBuilder {
    return Rx2AndroidNetworking.get(ApiURL.BASE_URL + this)
            .addHeaders(ApiURL.getBaseHeader())
            .addHeaders(ApiURL.getTokenHeader())
}

fun <T> Rx2ANRequest.GetRequestBuilder.get(clazz: Class<T>): Observable<T> {
    return this.build()
            .getObjectObservable(clazz)
}





