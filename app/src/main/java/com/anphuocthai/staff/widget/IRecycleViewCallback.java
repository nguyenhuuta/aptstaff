package com.anphuocthai.staff.widget;

/**
 * Created by DaiKySy on 9/6/19.
 */
public interface IRecycleViewCallback<Data> {

    void onItemClick(int position, Data data);
}
