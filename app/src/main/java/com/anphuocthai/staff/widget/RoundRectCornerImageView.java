package com.anphuocthai.staff.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.anphuocthai.staff.utils.Utils;

/**
 * Created by OpenYourEyes on 6/11/2020
 */
public class RoundRectCornerImageView extends ImageView {

    private float radius = Utils.convertDpToPixel(getContext(), 4.0f);
    private Path path;
    private RectF rect;

    public void setRadius(float radius) {
        this.radius = Utils.convertDpToPixel(getContext(), radius);
    }

    public RoundRectCornerImageView(Context context) {
        super(context);
        init();
    }

    public RoundRectCornerImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RoundRectCornerImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        path = new Path();

    }

    @Override
    protected void onDraw(Canvas canvas) {
        rect = new RectF(0, 0, this.getWidth(), this.getHeight());
        path.addRoundRect(rect, radius, radius, Path.Direction.CW);
        canvas.clipPath(path);
        super.onDraw(canvas);
    }
}