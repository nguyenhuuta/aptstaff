package com.anphuocthai.staff.widget

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.RecyclerView
import com.anphuocthai.staff.utils.printLog
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject

/**
 * Created by DaiKySy on 8/27/19.
 */
interface OnLoadMore {
    fun onLoadMore()
}


interface ILoadMore {
    var triggerLoadMore: PublishSubject<Unit>
    var triggerRefresh: BehaviorSubject<Unit>
}

class LoadMoreRecycleView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null, defStyle: Int = 0
) : RecyclerView(context, attrs, defStyle) {

    private lateinit var scrollListener: RecyclerViewLoadMoreScroll

    fun setPauseLoadMore(isPause: Boolean) {
        printLog("setPauseLoadMore $isPause")
        if (::scrollListener.isInitialized) {
            scrollListener.setPauseLoadMore(isPause)
        }
    }

    fun setLoading(isLoading: Boolean = false) {
        printLog("setLoading $isLoading")
        if (::scrollListener.isInitialized) {
            scrollListener.setLoading(isLoading)
        }
    }

    fun setEndOfPage(isEndOfPage: Boolean = true) {
        printLog("setEndOfPage $isEndOfPage")
        if (::scrollListener.isInitialized) {
            scrollListener.setEndOfPage(isEndOfPage)
        }
    }

    fun addLoadMore(onLoadMore: () -> Unit) {
        scrollListener = RecyclerViewLoadMoreScroll(layoutManager!!, onLoadMore)
        addOnScrollListener(scrollListener)
    }

}