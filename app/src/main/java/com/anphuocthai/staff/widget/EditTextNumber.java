package com.anphuocthai.staff.widget;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatEditText;

import com.anphuocthai.staff.utils.Logger;

import java.text.DecimalFormat;
import java.text.ParseException;

/**
 * Created by OpenYourEyes on 11/19/2020
 */
public class EditTextNumber extends AppCompatEditText implements TextWatcher {
    private String number = "";
    private DecimalFormat formatter;

    public EditTextNumber(Context context) {
        this(context, null);
    }

    public EditTextNumber(Context context, AttributeSet attrs) {
        this(context, attrs, android.R.attr.editTextStyle);
    }

    public EditTextNumber(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        formatter = new DecimalFormat("###,###,###");
        addTextChangedListener(this);
        setOnFocusChangeListener((view, focus) -> {
            String value = getText() == null ? "" : getText().toString();
            Logger.d("EditTextNumber", "setOnFocusChangeListener: " + value);
            if (!focus && value.isEmpty()) {
                setText("0");
            }
        });
    }


    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    @Override
    public void afterTextChanged(Editable s) {
        Logger.d("EditTextNumber", "afterTextChanged: " + s.toString());
        removeTextChangedListener(this);
        try {
            number = s.toString().replace(String.valueOf(formatter.getDecimalFormatSymbols().getGroupingSeparator()), "");
            Number n = formatter.parse(number);
            String value = formatter.format(n);
            setText(value);
            setSelection(value.length());
        } catch (NumberFormatException | ParseException nfe) {
            // do nothing?
        }
        addTextChangedListener(this);
    }

    public float getNumber() {
        try {
            return Float.parseFloat(number);
        } catch (Exception ignore) {
            return 0;
        }
    }

}
