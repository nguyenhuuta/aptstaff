package com.anphuocthai.staff.widget

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.RecyclerView
import com.anphuocthai.staff.utils.printLog

/**
 * Created by OpenYourEyes on 11/24/2020
 */
class LoadMoreRecyclerView2 @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        def: Int = 0
) : RecyclerView(context, attrs, def) {
    private var isLoading = false
    var isEndOfPage = false

    fun setLoaded() {
        this.isLoading = false
    }

    fun addListenerLoadMore(triggerLoadMore: () -> Unit) {
        addOnScrollListener(object : OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val isBottom = !recyclerView.canScrollVertically(1)
                println("LoadMoreRecyclerView $isBottom $isLoading $isEndOfPage")
                if (isBottom && !isLoading && !isEndOfPage) {
                    printLog("LoadMoreRecyclerView triggerLoadMore")
                    isLoading = true
                    triggerLoadMore()
                }
            }
        })
    }
}