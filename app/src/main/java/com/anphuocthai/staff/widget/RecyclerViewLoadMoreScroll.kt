package com.anphuocthai.staff.widget


import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager

/**
 * Created by OpenYourEyes on 10/1/2020
 */

open class RecyclerViewLoadMoreScroll(
        private val mLayoutManager: RecyclerView.LayoutManager,
        private val triggerLoadMore: () -> Unit
) : RecyclerView.OnScrollListener() {

    private var visibleThreshold = 5
    private var isLoading: Boolean = false
    private var isEndOfPage = false
    private var isPause = false

    private var lastVisibleItem: Int = 0
    private var totalItemCount: Int = 0


    fun setPauseLoadMore(isPause: Boolean) {
        this.isPause = isPause
    }

    fun setLoading(isLoading: Boolean = false) {
        this.isLoading = isLoading
    }

    fun setEndOfPage(isEndOfPage: Boolean = true) {
        this.isEndOfPage = isEndOfPage
    }

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        if (dy <= 0 || isEndOfPage || isLoading || isPause) return
        totalItemCount = mLayoutManager.itemCount

        when (mLayoutManager) {
            is StaggeredGridLayoutManager -> {
                val lastVisibleItemPositions =
                        mLayoutManager.findLastVisibleItemPositions(null)
                lastVisibleItem = getLastVisibleItem(lastVisibleItemPositions)
            }
            is GridLayoutManager -> {
                lastVisibleItem =
                        mLayoutManager.findLastVisibleItemPosition()
            }
            is LinearLayoutManager -> {
                lastVisibleItem =
                        mLayoutManager.findLastVisibleItemPosition()
            }
        }
        val isBottom = !recyclerView.canScrollVertically(1)
        if (!isLoading && isBottom) {
            triggerLoadMore()
            isLoading = true
        }

    }

    private fun getLastVisibleItem(lastVisibleItemPositions: IntArray): Int {
        var maxSize = 0
        for (i in lastVisibleItemPositions.indices) {
            if (i == 0) {
                maxSize = lastVisibleItemPositions[i]
            } else if (lastVisibleItemPositions[i] > maxSize) {
                maxSize = lastVisibleItemPositions[i]
            }
        }
        return maxSize
    }
}