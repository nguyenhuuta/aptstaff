package com.anphuocthai.staff.utils;

import android.content.Context;

import androidx.multidex.MultiDexApplication;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.interceptors.HttpLoggingInterceptor;
import com.anphuocthai.staff.baserequest.APIService;
import com.anphuocthai.staff.data.DataManager;
import com.anphuocthai.staff.data.db.network.model.response.UserInfo;
import com.anphuocthai.staff.di.component.AppComponent;
import com.anphuocthai.staff.di.component.DaggerAppComponent;
import com.anphuocthai.staff.di.module.AppModule;
import com.anphuocthai.staff.ui.home.notifications.model.Notification;
import com.anphuocthai.staff.ui.home.statistics.viewholder.generalstatistic.APTIndex;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import okhttp3.OkHttpClient;


public class App extends MultiDexApplication {
    private static final String TAG = App.class.getSimpleName();
    private static Context context;
    private static App sInstance;
    private static AppComponent component;

    private static UserInfo userInfo;

    private static ArrayList<APTIndex> aptIndexArrayList;

    private static Notification lastNotification;


    public static AppComponent getComponent() {
        return component;
    }


    private int departmentId = 0;


    @Inject
    DataManager dataManager;

    public static App getInstance() {
        return sInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        // create timeout
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .build();

        AndroidNetworking.initialize(getApplicationContext(), okHttpClient);
        AndroidNetworking.enableLogging(HttpLoggingInterceptor.Level.HEADERS);
        component = buildComponent();
        component.inject(this);
        sInstance = this;

    }

    protected AppComponent buildComponent() {
        return DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public DataManager getDataManager() {
        return dataManager;
    }

    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }

    public int getDepartmentId() {
        return departmentId;
    }

    public static Context getAppContext() {
        return App.context;
    }

    public static void setContext(Context mContext) {
        App.context = mContext;
    }


    public static UserInfo getUserInfo() {
        return App.userInfo;
    }

    public static void setUserInfo(UserInfo userInfo) {
        if (userInfo != null) {
            APIService.getInstance().addAuthorizationHeader(userInfo.getToken());
        }
        App.userInfo = userInfo;
    }

    public static String getToken() {
        return component.getDataManager().getToken();
    }

    public static ArrayList<APTIndex> getStatistics() {
        return aptIndexArrayList;
    }

    public static void setStatistics(ArrayList<APTIndex> statistics) {
        App.aptIndexArrayList = statistics;
    }

    public static Notification getLastNotification() {
        return App.lastNotification;
    }

    public static void setLastNotification(Notification notification) {
        App.lastNotification = notification;
    }

}