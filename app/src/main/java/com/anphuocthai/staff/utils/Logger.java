package com.anphuocthai.staff.utils;

import android.text.TextUtils;
import android.util.Log;

import com.anphuocthai.staff.BuildConfig;

/**
 * Created by DaiKySy on 8/15/19.
 */
public class Logger {
    private static final int LOG_LEVEL = BuildConfig.DEBUG ? Log.VERBOSE : Log.INFO;

    public static void v(String tag, String message) {
        if (TextUtils.isEmpty(tag) || TextUtils.isEmpty(message)) return;
        if (Log.VERBOSE >= LOG_LEVEL) {
            Log.v(tag, message);
        }
    }

    public static void d(String tag, String message) {
        if (TextUtils.isEmpty(tag) || TextUtils.isEmpty(message)) return;
        if (Log.DEBUG >= LOG_LEVEL) {
            Log.d(tag, message);
        }
    }

    public static void i(String tag, String message) {
        if (TextUtils.isEmpty(tag) || TextUtils.isEmpty(message)) return;
        if (Log.INFO >= LOG_LEVEL) {
            Log.i(tag, message);
        }
    }

    public static void w(String tag, String message) {
        if (TextUtils.isEmpty(tag) || TextUtils.isEmpty(message)) return;
        if (Log.WARN >= LOG_LEVEL) {
            Log.w(tag, message);
        }
    }

    public static void e(String tag, String message) {
        if (TextUtils.isEmpty(tag) || TextUtils.isEmpty(message)) return;
        if (Log.ERROR >= LOG_LEVEL) {
            Log.e(tag, message);
        }
    }

    private Logger() {
    }

    public static void printLog(String message) {
        StringBuilder builder = new StringBuilder();
        Thread thread = Thread.currentThread();
        StackTraceElement stackTraceElement = thread.getStackTrace()[4];
        Thread.currentThread().getStackTrace();
        builder.append("DKS ");
        builder.append("[ ");
        builder.append(thread.getName());
        builder.append("-> ");
        builder.append(stackTraceElement.getFileName());
        builder.append(": ");
        builder.append(stackTraceElement.getMethodName() + "() ");
        builder.append(" ]");
        Log.d(builder.toString(), message);
    }
}
