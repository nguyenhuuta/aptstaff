package com.anphuocthai.staff.utils

import android.view.View
import com.anphuocthai.staff.BuildConfig
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by OpenYourEyes on 7/1/2020
 */

fun printLog(message: String) {
    if (!BuildConfig.DEBUG) return
    val stackTrace = Thread.currentThread().stackTrace[4]
//    Logger.d("[${stackTrace.fileName} - ${stackTrace.methodName} - DKS]", message)
    Logger.d("[${stackTrace.fileName} - [${Thread.currentThread().name}] - DKS]", message)
}


fun printLog(clazz: Any, message: String) {
    if (BuildConfig.DEBUG) {
        println("$clazz - DKS: $message")
//        Logger.d("$clazz - DKS", message)
    }
}


enum class DateType(val type: String) {
    FULL("dd/MM/yyyy HH:mm"),
    ONLY_DATE("dd/MM/yyyy"),
    ONLY_TIME("HH:mm"),
    ONLY_TIME2("HH:mm:ss"),
    MONTH_YEAR("MM/yyyy"),
    MONTH("'Tháng' MM"),
    FUll2("dd-MM-yyyy HH:mm:ss"),
    FUll3("yyyy-MM-dd HH:mm:ss"),
    ONLY_DATE2("yyyy-MM-dd"),
    ONLY_DATE3("dd-MM-yyyy");
}

fun String.convertDate(from: DateType, to: DateType): String {
    val df: DateFormat = SimpleDateFormat(from.type, Locale.getDefault())
    val startDate: Date
    try {
        startDate = df.parse(this)
        val newDate = SimpleDateFormat(to.type, Locale.getDefault())
        return newDate.format(startDate)
    } catch (e: ParseException) {
        e.printStackTrace()
    }
    return ""
}

fun Calendar.convertDate(format: DateType): String {
    return try {
        val simple: DateFormat = SimpleDateFormat(format.type, Locale.getDefault())
        val result = Date(timeInMillis)
        simple.format(result)
    } catch (ignore: Exception) {
        ""
    }
}


fun Boolean.visibility(): Int {
    return if (this) View.VISIBLE else View.GONE
}

