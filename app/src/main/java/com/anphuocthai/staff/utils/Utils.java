package com.anphuocthai.staff.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.Html;
import android.text.Spanned;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.api.ApiURL;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.io.ByteArrayOutputStream;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Utils {

    public static int convertDpToPixel(Context context, float dp) {
        if (context != null && context.getResources() != null) {
            Resources resources = context.getResources();
            DisplayMetrics metrics = resources.getDisplayMetrics();
            return (int) (dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT));
        }
        return (int) dp;
    }

    public static int getWidthDevice(Activity activity) {
        try {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
//        int height = displayMetrics.heightPixels;
            return displayMetrics.widthPixels;
        } catch (Exception ignore) {
            return 0;
        }


    }

    public static ProgressDialog showLoadingDialog(Context context) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.circle_progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }


    public static void hideKeyboard(Activity activity) {
        // Check if no view has focus:
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }


    public static String formatDate(String dateString, boolean isViewHM) {
        if (dateString == null) {
            return "";
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date convertedDate;
        try {
            convertedDate = dateFormat.parse(dateString);
            SimpleDateFormat sdfnewformat;
            if (isViewHM) {
                sdfnewformat = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.getDefault());
            } else {
                sdfnewformat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
            }

            return sdfnewformat.format(convertedDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";

    }

    public static String formatDateWithFormat(String format, Date date) {
        String finalDateString = new String();
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        try {
            finalDateString = dateFormat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return finalDateString;

    }

    public static String formatDateStringWithFormat(String oldFormat, String newFormat, String dateString) {
        String finalDateString = new String();
        SimpleDateFormat oldDateFormat = new SimpleDateFormat(oldFormat);
        SimpleDateFormat newDateFormat = new SimpleDateFormat(newFormat);

        try {
            Date date = oldDateFormat.parse(dateString);
            finalDateString = newDateFormat.format(date);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return finalDateString;

    }

    public static String bitmapToBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    public static Bitmap decodeBase64(String input) {
        byte[] decodedByte = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }


    public static String formatValue(double value, @Enum.FieldValueType.ValueType int valueType) {
        String result = new String();
        DecimalFormat formatter;
        Locale currentLocale = Locale.getDefault();
        formatter = new DecimalFormat("###,###,###.##");
        switch (valueType) {
            case Enum.FieldValueType.CURRENCY:
                formatter = new DecimalFormat("###,###,###");
                result = formatter.format(value) + " VNĐ";
                break;
            case Enum.FieldValueType.NORMAL:
                DecimalFormatSymbols dotSymbols = new DecimalFormatSymbols(currentLocale);
                dotSymbols.setGroupingSeparator('.');
                formatter = new DecimalFormat("###,###,###", dotSymbols);
                result = formatter.format(value);
                break;

            case Enum.FieldValueType.WEIGHT:
                DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(currentLocale);
                otherSymbols.setDecimalSeparator('.');
                otherSymbols.setGroupingSeparator('.');
                formatter = new DecimalFormat("###.#", otherSymbols);
                result = formatter.format(value) + "Kg";
                break;
            case Enum.FieldValueType.FLOAT_NORMAL:
                DecimalFormatSymbols floatNormalSymbol = new DecimalFormatSymbols(currentLocale);
                floatNormalSymbol.setDecimalSeparator('.');
                floatNormalSymbol.setGroupingSeparator('.');
                formatter = new DecimalFormat("###.##", floatNormalSymbol);
                result = formatter.format(value);
                break;
            case Enum.FieldValueType.CURRENCY_NORMAL:
                formatter = new DecimalFormat("###,###,###.##");
                result = formatter.format(value);
                break;

        }

        return result;
    }


    public static long getDateInMillis(String srcDate) {
        SimpleDateFormat desiredFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        long dateInMillis = 0;
        try {
            Date date = desiredFormat.parse(srcDate);
            dateInMillis = date.getTime();
            return dateInMillis;
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }

        return 0;
    }


    public static boolean isNull(Object o) {
        if (o == null) return true;
        if (o instanceof List) {
            return ((List) o).size() == 0;
        } else if (o instanceof String) {
            return ((String) o).isEmpty();
        }
        return true;
    }

    public static void loadImage(ImageView imageView, int imageId) {
        Glide.with(imageView.getContext())
                .load(ApiURL.IMAGE_VIEW_URL + imageId)
                .placeholder(R.drawable.ic_no_image)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(imageView);
    }

    public static Spanned currency(int value) {
        DecimalFormat formatter = new DecimalFormat("###,###,###.##");
        String price = formatter.format(value) + " <u>đ</u>";
        return Html.fromHtml(price);
    }


}
