package com.anphuocthai.staff.utils;

import com.anphuocthai.staff.model.ChangePass;

public class Singleton {
    private static  Singleton ourInstance = new Singleton();

    public static Singleton getInstance() {
        if (ourInstance == null) {
            ourInstance = new Singleton();
        }
        return ourInstance;
    }

    private Singleton() {
    }

    //public Login login;
    //public Context context;
    public ChangePass changePass;
}
