package com.anphuocthai.staff.utils.extension

import java.text.DecimalFormat

/**
 * Created by OpenYourEyes on 9/8/2020
 */
fun Float.toCurrency(isVND: Boolean = true): String {
    val formatter = DecimalFormat("###,###,###")
    var result = formatter.format(this.toDouble());
    if (isVND) {
        result += " VNĐ"
    }
    return result
}