package com.anphuocthai.staff.utils

/**
 * Created by OpenYourEyes on 7/10/21
 */
data class CustomerFragmentReload(val customerId: Int)