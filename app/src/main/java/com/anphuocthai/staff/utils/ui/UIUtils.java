package com.anphuocthai.staff.utils.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.interfaces.IYNDialogCallback;


public class UIUtils {
    public static void showYesNoDialog(Context context,String title, String message, IYNDialogCallback callback) {
        new AlertDialog.Builder(context, R.style.MyDialogTheme)
                .setTitle(title)
                .setMessage(message)

                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        callback.accept();
                    }})
                .setNegativeButton(android.R.string.no, null).show();
    }

    public static void showProgressbar(){
//        ProgressBar mprogressBar = (ProgressBar) findViewById(R.id.circular_progress_bar);
//        ObjectAnimator anim = ObjectAnimator.ofInt(mprogressBar, "progress", 0, 100);
//        anim.setDuration(15000);
//        anim.setInterpolator(new DecelerateInterpolator());
//        anim.start();
    }
}
