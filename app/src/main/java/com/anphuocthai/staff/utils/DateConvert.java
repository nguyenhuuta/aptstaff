package com.anphuocthai.staff.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by DaiKySy on 9/26/19.
 */
public class DateConvert {

    public static String convertDate(long millisecond, TypeConvert typeConvert) {
        try {
            DateFormat simple = new SimpleDateFormat(typeConvert.format, Locale.getDefault());
            Date result = new Date(millisecond);
            return simple.format(result);
        } catch (Exception ignore) {
            return "";
        }
    }

    public static String formatDate(String dateString, TypeConvert fromType, TypeConvert toType) {
        if (dateString == null) {
            return "";
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat(fromType.format, Locale.getDefault());
        Date convertedDate;
        try {
            convertedDate = dateFormat.parse(dateString);
            SimpleDateFormat sdfnewformat = new SimpleDateFormat(toType.format, Locale.getDefault());
            return sdfnewformat.format(convertedDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";

    }

    public enum TypeConvert {
        FULL("dd/MM/yyyy HH:mm"),
        ONLY_DATE("dd/MM/yyyy"),
        ONLY_TIME("HH:mm"),
        MONTH_YEAR("MM/yyyy"),
        MONTH("'Tháng' MM"),
        FUll2("dd-MM-yyyy HH:mm:ss"),
        FUll3("yyyy-MM-dd HH:mm:ss"),
        ONLY_DATE2("yyyy-MM-dd"),
        ONLY_DATE3("dd-MM-yyyy");

        private String format;

        TypeConvert(String format) {
            this.format = format;
        }
    }
}
