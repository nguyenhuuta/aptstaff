package com.anphuocthai.staff.utils.extension

import androidx.lifecycle.MutableLiveData

/**
 * Created by OpenYourEyes on 3/15/21
 */
class ListLiveData<T> : MutableLiveData<MutableList<T>>()