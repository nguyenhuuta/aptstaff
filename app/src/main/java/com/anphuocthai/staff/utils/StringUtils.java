package com.anphuocthai.staff.utils;

import android.content.Context;

import com.anphuocthai.staff.R;
import com.google.i18n.phonenumbers.PhoneNumberUtil;

import java.util.AbstractMap;

public class StringUtils {

    public static String toLowerCase(String text){
        String firstLetter = text.substring(0,1).toUpperCase();
        String restLetters = text.substring(1).toLowerCase();
        return firstLetter + restLetters;
    }

    public static boolean phoneCompare(String firstPhone, String secondPhone) {

        PhoneNumberUtil pnu = PhoneNumberUtil.getInstance();
        PhoneNumberUtil.MatchType mt = pnu.isNumberMatch(firstPhone, secondPhone);
        if( mt == PhoneNumberUtil.MatchType.NSN_MATCH || mt == PhoneNumberUtil.MatchType.EXACT_MATCH ) {
            return true;
        }

        return false;
    }

    // phone in customer activity
    public static String phoneFormProcess(String phoneInput, Context context) {
        String phoneResult = "";

        //entries = new ArrayList<>();
        // step 1: Cat theo dau ;
        String[] semicolon = phoneInput.split(";");

        if (semicolon.length <= 1) {
            //semicolon[0] = context.getString(R.string.main_phone_number) + ":" + semicolon[0].trim();
            phoneResult = semicolon[0].trim();
        }

        for (String entrys: semicolon) {
            String[] entry = entrys.split(":");

            if (entry.length >= 2) {
                AbstractMap.SimpleEntry<String, String> resultEntry
                        = new AbstractMap.SimpleEntry<>(entry[0], entry[1]);
                if (entry[0].trim().equals(context.getString(R.string.main_phone_number))) {
                    phoneResult = entry[1];
                }
               // entries.add(resultEntry);
            }
        }
        //setupPhoneNumbers();

        return phoneResult;
    }

    public static String breakLinePhoneProcess(String phoneInput, Context context) {
        String phoneResult = "";

        //entries = new ArrayList<>();
        // step 1: Cat theo dau ;
        String[] semicolon = phoneInput.split(";");

        if (semicolon.length <= 1) {
            semicolon[0] = context.getString(R.string.main_phone_number) + ":" + semicolon[0].trim();
        }

        for (int i = 0; i < semicolon.length; i++) {
          phoneResult += semicolon[i];
          if (i < semicolon.length - 1){
              phoneResult += "\n\n";
          }
        }


        return phoneResult;
    }
}
