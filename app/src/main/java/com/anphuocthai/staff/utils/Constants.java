package com.anphuocthai.staff.utils;


import com.google.android.gms.maps.model.LatLng;

public final class Constants {

    public static final String START_TIME = "timer_start_time";
    public static final String ORIGINAL_TIME = "timer_original_time";
    public static final String DATA_ITEM_PATH = "/timer";

    public static final int NOTIFICATION_TIMER_COUNTDOWN = 1;
    public static final int NOTIFICATION_TIMER_EXPIRED = 2;

    public static final String ACTION_SHOW_ALARM
            = "com.android.example.clockwork.timer.ACTION_SHOW";
    public static final String ACTION_DELETE_ALARM
            = "com.android.example.clockwork.timer.ACTION_DELETE";
    public static final String ACTION_RESTART_ALARM
            = "com.android.example.clockwork.timer.ACTION_RESTART";


    /**
     * API CODE RETURN
     */
    public static final String NOT_FOUND_CODE = "04";
    public static final String ERROR_CODE = "01";
    public static final String SUCCESS_CODE = "00";

    public static final String EMPTY = "";

    public static final int API_STATUS_CODE_LOCAL_ERROR = 0;


    /**
     * USER SETTING SHAREPREFERENCE
     */
    public static final String USER_SETTING_FILE = "user_setting";
    public static final String AUTO_LOGIN_KEY = "autologin";
    public static final String USER_NAME_KEY = "username";
    public static final String PASS_WORD_KEY = "password";

    public static final String PREF_ENCODE_CODE = "qwerty!@#%^&*()";


    /**
     * INTENT REQUEST CODE
     */

    public static final int REQUEST_LAT_LONG_CREATE_CUSTOMER_CODE = 0;
    public static final int REQUEST_LAT_LONG_UPDATE_CUSTOMER_CODE = 1;
    public static final int REQUEST_LAT_LONG_CREATE_NEW_ADDRESS_CODE = 2;
    public static final int REQUEST_PHONE_CALL = 3;
    public static final int REQUEST_CODE_SEARCH_CUSTOMER = 4;
    public static final int RESULT_CODE_SEARCH_CUSTOMER = 5;
    public static final int RESULT_CODE_TAKE_PICTURE = 6;

    /**
     * APT
     */
    public static final LatLng APT_LATLNG = new LatLng(20.999119, 105.822204);

    /**
     * Database order name
     */
    public static final String DB_NAME = "apt_mvp.db";

    // statistic in home
    public static final int NUMBER_FIX_REPORT = 5;

    public static final int OFF_SCREEN_PAGE_LIMIT = 10;

    //
    public static final int NUMBER_ROW_LOAD_MORE = 30;

    public static final int NUMBER_OF_DAILY_WORK_IN_HOME = 3;

    public static final int FIRST_PAGE = 1;
    public static final int LIMIT = 20;

}
