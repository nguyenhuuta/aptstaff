package com.anphuocthai.staff.utils;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class Enum {

    public static class FieldValueType {
        public static final int NORMAL = 0;
        public static final int CURRENCY = 1;
        public static final int WEIGHT = 2;
        public static final int CURRENCY_NORMAL = 3;
        public static final int FLOAT_NORMAL = 4;

        public FieldValueType(@ValueType int valueType) {
            System.out.println("Value Type :" + valueType);
        }

        @IntDef({NORMAL, CURRENCY, WEIGHT, CURRENCY_NORMAL, FLOAT_NORMAL})
        @Retention(RetentionPolicy.SOURCE)
        public @interface ValueType {
        }
    }

    public static class WorkScheduleState {
        public static final int NEW = 1;
        public static final int FINISH = 2;
        public static final int CANCEL = 4;

        public WorkScheduleState(@WorkScheduleType int workScheduleType) {
            System.out.println("WorkScheduleType :" + workScheduleType);
        }

        @IntDef({NEW, FINISH, CANCEL})
        @Retention(RetentionPolicy.SOURCE)
        public @interface WorkScheduleType {
        }

    }

    public static class PayMethod {
        public static final int CASH = 1;
        public static final int TRANSFER = 5;
        public static final int DEBT = 6;
        public static final int PAY_AT_DELIVERED = 9;
    }


    public static class OrderState { // trạng thái đơn đặt
        public static final int NEW = 1;
        public static final int PROCESSING = 2;
        public static final int DENY = 21;
        public static final int WAIT_OUTPUT = 3;
        public static final int PACK = 31;
        public static final int BE_TRANSPORT = 5;
        public static final int DELIVERED = 6;
        public static final int RETURNS = 61;
        public static final int WAIT_INPUT = 62;
        public static final int FINISH = 7;
        public static final int CANCEL = 4;
    }

    public static class OpenCustomerListType {
        public static final int SCHEDULE = 1;
        public static final int VIEW = 2;
        public static final int QUICK_CHECK_IN = 3;
    }

}
