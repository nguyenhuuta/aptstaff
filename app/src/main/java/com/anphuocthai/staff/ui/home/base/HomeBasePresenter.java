package com.anphuocthai.staff.ui.home.base;

import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.data.DataManager;
import com.anphuocthai.staff.ui.base.BasePresenter;
import com.anphuocthai.staff.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class HomeBasePresenter<V extends HomeBaseMvpView> extends BasePresenter<V> implements HomeBaseMvpPresenter<V> {

    @Inject
    public HomeBasePresenter(NetworkManager networkManager, DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(networkManager, dataManager, schedulerProvider, compositeDisposable);
    }
}
