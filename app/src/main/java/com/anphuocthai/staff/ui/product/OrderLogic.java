package com.anphuocthai.staff.ui.product;

import androidx.room.Ignore;

import java.io.Serializable;

public class OrderLogic implements Serializable {

    private static final String TAG = OrderLogic.class.getSimpleName();

    public static final int DEFAULT_QUANTITY = 5;

    private float quantityThreshold;

    private float lowerThreshold;

    private float quantity;

    private float priceRetail;

    private float priceWholeSale;

    private float currentPrice;

    private int cv;

    private float totalFee;

    private int fixCV;

    private int customerType;

    private int numberBox = 0;

    // isSkip = true tinh CV, false khong tinh
    @Ignore
    private boolean isApplyCV = true;

    // isSkip = true tinh doanh so
    @Ignore
    private boolean isApplyDoanhSo = true;

    public boolean isApplyCV() {
        return isApplyCV;
    }

    public void setApplyCV(boolean applyCV) {
        isApplyCV = applyCV;
    }

    public boolean isApplyDoanhSo() {
        return isApplyDoanhSo;
    }

    public void setApplyDoanhSo(boolean applyDoanhSo) {
        isApplyDoanhSo = applyDoanhSo;
    }

    public OrderLogic() {
        setQuantity(DEFAULT_QUANTITY);
    }


    public float getQuantity() {
        return quantity;
    }

    public void setQuantity(float quantity) {
        setupCurrentPrice(quantity);
        this.quantity = quantity;
    }

    public void increaseQuantity(float increaseValue) {
        if (this.quantity < getQuantityThreshold()) {
            this.quantity = getQuantityThreshold();
        } else {
            this.quantity += increaseValue;
        }

        setupCurrentPrice(this.quantity);
    }

    public void decreaseQuantity(float decreaseValue) {
        if (this.quantity < getQuantityThreshold()) {
            this.quantity = 0;
        } else {
            this.quantity -= decreaseValue;
            if (this.quantity <= 0) {
                this.quantity = 0;
            }
        }
        setupCurrentPrice(this.quantity);
    }


    public int getDefaultQuantity() {
        return DEFAULT_QUANTITY;
    }

    public float getQuantityThreshold() {
        return quantityThreshold;
    }

    public void setQuantityThreshold(float quantityThreshold) {
        this.quantityThreshold = quantityThreshold;
    }

    public float getPriceRetail() {
        return priceRetail;
    }

    public void setPriceRetail(float priceRetail) {
        this.priceRetail = priceRetail;
        setupCurrentPrice(this.quantity);
    }

    public float getPriceWholeSale() {
        return priceWholeSale;
    }

    public void setPriceWholeSale(float priceWholeSale) {
        this.priceWholeSale = priceWholeSale;
        setupCurrentPrice(this.quantity);

    }

    public float getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(float currentPrice) {
//        Logger.d("DKS", "setCurrentPrice: " + this.currentPrice + "/" + currentPrice);
        this.currentPrice = currentPrice;
    }

    public int getMaxCV(int percent) {
        return (int) (cv - Math.round(fixCV * percent * 1.0 / 100));
    }

    private void setupCurrentPrice(float quantity) {
        if (quantity >= quantityThreshold) {
            setCurrentPrice(getPriceWholeSale());
        } else {
            setCurrentPrice(getPriceRetail());
        }
    }

    public float getLowerThreshold() {
        return lowerThreshold;
    }

    public void setLowerThreshold(float lowerThreshold) {
        this.lowerThreshold = lowerThreshold;
        setupCurrentPrice(this.quantity);
    }

    public int getCv() {
        return cv;
    }

    public int getCvWithQuantity() {
        return Math.round(cv * quantity);
    }

    public void setCv(int cv) {
        this.cv = cv;
    }

    public float getTotalFee() {
        return this.quantity * this.currentPrice;
    }

    public void setTotalFee(float totalFee) {
        this.totalFee = totalFee;
    }

    public int getFixCV() {
        return fixCV;
    }

    public void setFixCV(int fixCV) {
        this.fixCV = fixCV;
    }

    public int getCustomerType() {
        return customerType;
    }

    public void setCustomerType(int customerType) {
        this.customerType = customerType;
    }

    public int getNumberBox() {
        return numberBox;
    }

    public void setNumberBox(int numberBox) {
        this.numberBox = numberBox;
    }
}
