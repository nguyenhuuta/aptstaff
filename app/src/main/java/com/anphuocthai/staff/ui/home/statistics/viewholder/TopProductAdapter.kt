package com.anphuocthai.staff.ui.home.statistics.viewholder

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityOptionsCompat
import androidx.recyclerview.widget.RecyclerView
import com.anphuocthai.staff.R
import com.anphuocthai.staff.model.order.Product
import com.anphuocthai.staff.ui.home.MainActivity
import com.anphuocthai.staff.ui.product.DetailProductActivity
import com.anphuocthai.staff.utils.Utils
import com.anphuocthai.staff.utils.extension.loadImage
import kotlinx.android.synthetic.main.item_product_top_sell.view.*


/**
 * Created by OpenYourEyes on 6/18/2020
 */
class TopProductAdapter(val list: MutableList<Product>) : RecyclerView.Adapter<TopProductAdapter.ViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_product_top_sell, viewGroup, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.bindData(list[position])
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bindData(product: Product) {
            itemView.image.loadImage(product.thumbId)
            itemView.productName.text = product.name
            itemView.priceProduct.text = Utils.currency(product.mainPrice?.kDBanBuonGia ?: 0)

            itemView.setOnClickListener { view ->
                itemView.context?.let {
                    if (it is MainActivity) {
                        val intent = Intent(it, DetailProductActivity::class.java)
                        val bundle = Bundle()
                        bundle.putInt("productid", product.id)
                        bundle.putInt("imageProduct", product.thumbId)
                        intent.putExtra("productExtra", bundle)
                        val options: ActivityOptionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(it, view, "imageProduct")
                        it.startActivity(intent, options.toBundle())
                    }
                }

            }

        }
    }

}