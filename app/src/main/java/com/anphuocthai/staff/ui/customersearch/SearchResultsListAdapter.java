package com.anphuocthai.staff.ui.customersearch;

/**
 * Copyright (C) 2015 Ari C.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.interfaces.ItemClickListener;
import com.anphuocthai.staff.model.customer.SearchCustomer;
import com.arlib.floatingsearchview.util.Util;
import com.l4digital.fastscroll.FastScroller;

import java.util.ArrayList;
import java.util.List;

public class SearchResultsListAdapter extends RecyclerView.Adapter<SearchResultsListAdapter.ViewHolder> implements FastScroller.SectionIndexer, Filterable {

    private List<SearchCustomer> mDataSet = new ArrayList<>();

    private List<SearchCustomer> customerList;

    private List<SearchCustomer> customerListOriginal;

    CSearchMvpPresenter<CSearchMvpView> mPresenter;

    public void setmPresenter(CSearchMvpPresenter<CSearchMvpView> mPresenter) {
        this.mPresenter = mPresenter;
    }

    public void setCustomerListOriginal(List<SearchCustomer> customerListOriginal) {
        this.customerListOriginal = customerListOriginal;
    }

    public List<SearchCustomer> getCustomerListOriginal() {
        return customerListOriginal;
    }

    private String currentFilterQuery;

    private int mLastAnimatedItemPosition = -1;

    @Override
    public Filter getFilter() {
        return new Filter() {
            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mDataSet = (List<SearchCustomer>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                List<SearchCustomer> filteredResults = null;
                if (constraint.length() == 0) {
                    filteredResults = customerListOriginal;
                    currentFilterQuery = null;
                } else {
                    filteredResults = getFilteredResults(constraint.toString().toLowerCase());
                }
                FilterResults results = new FilterResults();
                results.values = filteredResults;
                return results;
            }
        };
    }

    private List<SearchCustomer> getFilteredResults(String constraint) {
        List<SearchCustomer> results = new ArrayList<>();
        for (SearchCustomer item : customerListOriginal) {
            if (item.getTenDayDu().toLowerCase().contains(constraint)) {
                results.add(item);
            }
        }
        return results;
    }


    @Override
    public String getSectionText(int position) {
        try {
            return String.valueOf(customerList.get(position).getTenDayDu().charAt(0));
        } catch (NullPointerException | IndexOutOfBoundsException ex){
            ex.printStackTrace();
            return "";
        }
    }

    public void filterOnText(String query){
        this.currentFilterQuery = query;
        getFilter().filter(currentFilterQuery);
    }

    public interface OnItemClickListener{
        void onClick(SearchCustomer colorWrapper);
    }

    private OnItemClickListener mItemsOnClickListener;

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        public  TextView txtCustomerSearchName;
        public ImageView imgCustomer;

        private ItemClickListener itemClickListener;

        public ViewHolder(View view) {
            super(view);
            txtCustomerSearchName =  view.findViewById(R.id.txt_customer_search_name);
            imgCustomer = view.findViewById(R.id.img_customer_search);
            view.setOnClickListener(this);
            view.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            itemClickListener.onClick(v, getAdapterPosition(),false);
        }
        @Override
        public boolean onLongClick(View v) {
            itemClickListener.onClick(v,getAdapterPosition(),true); // Gọi interface , true là vì đây là onLongClick
            return true;
        }
        public void setItemClickListener(ItemClickListener itemClickListener)
        {
            this.itemClickListener = itemClickListener;
        }
    }

    public void swapData(List<SearchCustomer> mNewDataSet) {
        mDataSet = mNewDataSet;
        notifyDataSetChanged();
    }

    public void swapDataLoadMore(List<SearchCustomer> customerList) {
        mDataSet.addAll(customerList);
        notifyDataSetChanged();
    }

    public void setItemsOnClickListener(OnItemClickListener onClickListener){
        this.mItemsOnClickListener = onClickListener;
    }

    @Override
    public SearchResultsListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_search_customer, parent, false);
        ViewHolder vh = new ViewHolder(view);
        vh.setItemClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position, boolean isLongClick) {
                if (!isLongClick) {
                    mPresenter.onCustomerSelected(mDataSet.get(position));
                }
            }
        });
        return vh;
    }

    @Override
    public void onBindViewHolder(SearchResultsListAdapter.ViewHolder holder, final int position) {

        if (mDataSet.size() <= 0) {
            return;
        }
        holder.txtCustomerSearchName.setText(mDataSet.get(position).getTenDayDu());

//        ColorWrapper colorSuggestion = mDataSet.get(position);
//        holder.mColorName.setText(colorSuggestion.getName());
//        holder.mColorValue.setText(colorSuggestion.getHex());
//
//        int color = Color.parseColor(colorSuggestion.getHex());
//        holder.mColorName.setTextColor(color);
//        holder.mColorValue.setTextColor(color);
//
//        if(mLastAnimatedItemPosition < position){
//            animateItem(holder.itemView);
//            mLastAnimatedItemPosition = position;
//        }
//
//        if(mItemsOnClickListener != null){
//            holder.itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    mItemsOnClickListener.onClick(mDataSet.get(position));
//                }
//            });
//        }
    }

    @Override
    public int getItemCount() {

        if (mDataSet != null && mDataSet.size() > 0) {
            return (mDataSet.size()); // thêm nút cập nhật danh bạ
        } else {
            return 1;
        }
    }

    private void animateItem(View view) {
        view.setTranslationY(Util.getScreenHeight((Activity) view.getContext()));
        view.animate()
                .translationY(0)
                .setInterpolator(new DecelerateInterpolator(3.f))
                .setDuration(700)
                .start();
    }
}
