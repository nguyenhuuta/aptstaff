package com.anphuocthai.staff.ui.customer.detail.note;

import android.util.Log;

import com.androidnetworking.error.ANError;
import com.anphuocthai.staff.api.ApiURL;
import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.data.DataManager;
import com.anphuocthai.staff.interfaces.IAPICallback;
import com.anphuocthai.staff.model.customer.Note;
import com.anphuocthai.staff.ui.base.BasePresenter;
import com.anphuocthai.staff.utils.rx.SchedulerProvider;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class CustomerNotePresenter<V extends CustomerNoteMvpView> extends BasePresenter<V> implements CustomerNoteMvpPresenter<V> {

    private static final String TAG = CustomerNotePresenter.class.getSimpleName();
    @Inject
    public CustomerNotePresenter(NetworkManager networkManager,
                                 DataManager dataManager,
                                 SchedulerProvider schedulerProvider,
                                 CompositeDisposable compositeDisposable) {
        super(networkManager,
                dataManager,
                schedulerProvider,
                compositeDisposable);
    }

    @Override
    public void getNotes(String guid) {
        HashMap hashMap = new HashMap();
        hashMap.put("guid", guid);

        getmNetworkManager().sendGetRequest(ApiURL.GET_NOTE_CUSTOMER, hashMap, new IAPICallback() {
            @Override
            public void onResponseSuccess(JSONObject response) {

            }

            @Override
            public void onResponseSuccess(JSONArray response) {

                if (response != null) {
                    Log.d(TAG, response.toString());
                    Gson gson = new Gson();
                    Type type = new TypeToken<ArrayList<Note>>() {
                    }.getType();
                    ArrayList<Note> noteArrayList = gson.fromJson(response.toString(), type);
                    if (noteArrayList.size() > 0) {
                        getmMvpView().updateNote(noteArrayList);
                    }
                }

            }

            @Override
            public void onResponseError(ANError anError) {

            }
        });
    }
}
