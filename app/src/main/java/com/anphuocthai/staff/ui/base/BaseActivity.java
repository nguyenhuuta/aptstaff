package com.anphuocthai.staff.ui.base;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.data.DataManager;
import com.anphuocthai.staff.di.component.ActivityComponent;
import com.anphuocthai.staff.di.component.DaggerActivityComponent;
import com.anphuocthai.staff.di.module.ActivityModule;
import com.anphuocthai.staff.fragments.dialogmessage.DialogMessage;
import com.anphuocthai.staff.utils.App;
import com.anphuocthai.staff.utils.Logger;
import com.anphuocthai.staff.utils.NetworkUtils;
import com.anphuocthai.staff.utils.Utils;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.analytics.FirebaseAnalytics;

public class BaseActivity extends AppCompatActivity implements MvpView, BaseFragment.Callback {
    private ProgressDialog mProgressDialog;
    private Toolbar toolbar;

    private ActivityComponent mActivityComponent;

    public ActivityComponent getmActivityComponent() {
        return mActivityComponent;
    }

    DataManager dataManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dataManager = App.getInstance().getDataManager();
        App.setContext(this);
        FirebaseAnalytics.getInstance(this);
        mActivityComponent = DaggerActivityComponent.builder()
                .activityModule(new ActivityModule(this))
                .appComponent(((App) getApplication()).getComponent())
                .build();
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void requestPermissionsSafely(String[] permissions, int requestCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, requestCode);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean hasPermission(String permission) {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
                checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void showLoading() {
//        hideLoading();
        if (mProgressDialog == null) {
            mProgressDialog = Utils.showLoadingDialog(this);
        }
        print("showLoading isShow = " + mProgressDialog.isShowing());
        if (!mProgressDialog.isShowing() && !isDestroyed()) {
            mProgressDialog.show();
        }
    }

    @Override
    public void hideLoading() {
        if (mProgressDialog == null) return;
        print("hideLoading ");
        if (mProgressDialog.isShowing()) {
            mProgressDialog.cancel();
        }
    }

    @Override
    public void openActivityOnTokenExpire() {

    }

    @Override
    public void onError(int resId) {
        onError(getString(resId));
    }

    @Override
    public void onError(String message) {
        if (message != null) {
            showSnackBar(message);
        } else {
            showSnackBar(getString(R.string.have_error));
        }
    }

    @Override
    public void showMessage(String message) {
        if (message != null) {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void showSnackBar(String message) {
//        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),
//                message, Snackbar.LENGTH_SHORT);
//        View sbView = snackbar.getView();
//        TextView textView = (TextView) sbView
//                .findViewById(android.support.design.R.id.snackbar_text);
//        textView.setTextColor(ContextCompat.getColor(this, R.color.white));
//        snackbar.show();
    }


    @Override
    public void showMessage(@StringRes int resId) {
        showMessage(getString(resId));
    }

    @Override
    public boolean isNetworkConnected() {
        return NetworkUtils.isNetworkConnected(getApplicationContext());
    }

    @Override
    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void setupToolbar(String title) {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(title);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_white_24dp);
        toolbar.setNavigationOnClickListener(view -> {
            Runnable runnable = onButtonBackClick();
            if (runnable != null) {
                runnable.run();
                return;
            }
            finish();
        });
    }


    @Override
    public void setupToobarWithTab(String title, ViewPager viewPager, boolean isFixed) {
        setupToolbar(title);
        TabLayout tabLayout = findViewById(R.id.tabLayout);
        tabLayout.setVisibility(View.VISIBLE);
        tabLayout.setupWithViewPager(viewPager);
        if (isFixed) {
            tabLayout.setTabMode(TabLayout.MODE_FIXED);
        } else {
            tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        }
    }

    @Override
    public void onFragmentAttached() {

    }

    @Override
    public void onFragmentDetached(String tag) {
    }

    public Runnable onButtonBackClick() {
        return null;
    }


    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
        final ViewGroup viewGroup = (ViewGroup) ((ViewGroup) this
                .findViewById(android.R.id.content)).getChildAt(0);
        viewGroup.setFitsSystemWindows(true);

    }

    public Toolbar getToolbar() {
        return this.toolbar;
    }

    public void print(String message) {
        Logger.printLog(message);
    }

    @Override
    public void showDialogConfirm(String content, DialogMessage.IOnClickListener callback) {
        DialogMessage dialogMessage = DialogMessage.newInstance(content, getString(R.string.OK), getString(R.string.cancel));
        dialogMessage.setCallback(callback);
        dialogMessage.show(getSupportFragmentManager(), null);
    }

    @Override
    public void showDialog(String content, DialogMessage.IOnClickListener callback) {
        DialogMessage dialogMessage = DialogMessage.newInstance(content, getString(R.string.OK));
        dialogMessage.setCallback(callback);
        dialogMessage.show(getSupportFragmentManager(), null);
    }

}
