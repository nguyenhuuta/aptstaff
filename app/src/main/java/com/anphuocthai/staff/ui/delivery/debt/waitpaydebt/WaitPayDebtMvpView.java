package com.anphuocthai.staff.ui.delivery.debt.waitpaydebt;

import com.anphuocthai.staff.model.CongNoModel;
import com.anphuocthai.staff.ui.base.MvpView;

import java.util.List;

public interface WaitPayDebtMvpView extends MvpView {
    void updateOrderTran(List<CongNoModel> orderTranArrayList);
}
