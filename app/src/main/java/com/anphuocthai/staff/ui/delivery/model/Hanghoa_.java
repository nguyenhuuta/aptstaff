
package com.anphuocthai.staff.ui.delivery.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Hanghoa_ implements Serializable {

    @SerializedName("dD_HangHoaID")
    @Expose
    private Integer dDHangHoaID;
    @SerializedName("maHangHoa")
    @Expose
    private String maHangHoa;
    @SerializedName("tenHangHoa")
    @Expose
    private String tenHangHoa;
    @SerializedName("moTa")
    @Expose
    private String moTa;
    @SerializedName("chiTiet")
    @Expose
    private String chiTiet;
    @SerializedName("dangSanPhamId")
    @Expose
    private Integer dangSanPhamId;
    @SerializedName("congTyId")
    @Expose
    private Object congTyId;
    @SerializedName("isInUse")
    @Expose
    private Boolean isInUse;
    @SerializedName("isApp")
    @Expose
    private Boolean isApp;
    @SerializedName("isWeb")
    @Expose
    private Boolean isWeb;
    @SerializedName("xuatXuId")
    @Expose
    private Integer xuatXuId;
    @SerializedName("thuongHieuId")
    @Expose
    private Integer thuongHieuId;
    @SerializedName("duLieu")
    @Expose
    private String duLieu;
    @SerializedName("thuoctinhs")
    @Expose
    private Thuoctinhs thuoctinhs;
    @SerializedName("giaChinh")
    @Expose
    private GiaChinh giaChinh;
    @SerializedName("giakhacs")
    @Expose
    private Object giakhacs;
    @SerializedName("tenCongTy")
    @Expose
    private String tenCongTy;
    @SerializedName("tenDangSanPham")
    @Expose
    private String tenDangSanPham;
    @SerializedName("xuatXu")
    @Expose
    private String xuatXu;
    @SerializedName("thuongHieu")
    @Expose
    private String thuongHieu;
    @SerializedName("nhomSanPhamId")
    @Expose
    private Integer nhomSanPhamId;
    @SerializedName("tenNhomSanPham")
    @Expose
    private String tenNhomSanPham;
    @SerializedName("anhDaiDienId")
    @Expose
    private Integer anhDaiDienId;
    @SerializedName("tonKho")
    @Expose
    private float tonKho;
    @SerializedName("moTaNgan")
    @Expose
    private String moTaNgan;

    public Integer getDDHangHoaID() {
        return dDHangHoaID;
    }

    public void setDDHangHoaID(Integer dDHangHoaID) {
        this.dDHangHoaID = dDHangHoaID;
    }

    public String getMaHangHoa() {
        return maHangHoa;
    }

    public void setMaHangHoa(String maHangHoa) {
        this.maHangHoa = maHangHoa;
    }

    public String getTenHangHoa() {
        return tenHangHoa;
    }

    public void setTenHangHoa(String tenHangHoa) {
        this.tenHangHoa = tenHangHoa;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public String getChiTiet() {
        return chiTiet;
    }

    public void setChiTiet(String chiTiet) {
        this.chiTiet = chiTiet;
    }

    public Integer getDangSanPhamId() {
        return dangSanPhamId;
    }

    public void setDangSanPhamId(Integer dangSanPhamId) {
        this.dangSanPhamId = dangSanPhamId;
    }

    public Object getCongTyId() {
        return congTyId;
    }

    public void setCongTyId(Object congTyId) {
        this.congTyId = congTyId;
    }

    public Boolean getIsInUse() {
        return isInUse;
    }

    public void setIsInUse(Boolean isInUse) {
        this.isInUse = isInUse;
    }

    public Boolean getIsApp() {
        return isApp;
    }

    public void setIsApp(Boolean isApp) {
        this.isApp = isApp;
    }

    public Boolean getIsWeb() {
        return isWeb;
    }

    public void setIsWeb(Boolean isWeb) {
        this.isWeb = isWeb;
    }

    public Integer getXuatXuId() {
        return xuatXuId;
    }

    public void setXuatXuId(Integer xuatXuId) {
        this.xuatXuId = xuatXuId;
    }

    public Integer getThuongHieuId() {
        return thuongHieuId;
    }

    public void setThuongHieuId(Integer thuongHieuId) {
        this.thuongHieuId = thuongHieuId;
    }

    public String getDuLieu() {
        return duLieu;
    }

    public void setDuLieu(String duLieu) {
        this.duLieu = duLieu;
    }

    public Thuoctinhs getThuoctinhs() {
        return thuoctinhs;
    }

    public void setThuoctinhs(Thuoctinhs thuoctinhs) {
        this.thuoctinhs = thuoctinhs;
    }

    public GiaChinh getGiaChinh() {
        return giaChinh;
    }

    public void setGiaChinh(GiaChinh giaChinh) {
        this.giaChinh = giaChinh;
    }

    public Object getGiakhacs() {
        return giakhacs;
    }

    public void setGiakhacs(Object giakhacs) {
        this.giakhacs = giakhacs;
    }

    public String getTenCongTy() {
        return tenCongTy;
    }

    public void setTenCongTy(String tenCongTy) {
        this.tenCongTy = tenCongTy;
    }

    public String getTenDangSanPham() {
        return tenDangSanPham;
    }

    public void setTenDangSanPham(String tenDangSanPham) {
        this.tenDangSanPham = tenDangSanPham;
    }

    public String getXuatXu() {
        return xuatXu;
    }

    public void setXuatXu(String xuatXu) {
        this.xuatXu = xuatXu;
    }

    public String getThuongHieu() {
        return thuongHieu;
    }

    public void setThuongHieu(String thuongHieu) {
        this.thuongHieu = thuongHieu;
    }

    public Integer getNhomSanPhamId() {
        return nhomSanPhamId;
    }

    public void setNhomSanPhamId(Integer nhomSanPhamId) {
        this.nhomSanPhamId = nhomSanPhamId;
    }

    public String getTenNhomSanPham() {
        return tenNhomSanPham;
    }

    public void setTenNhomSanPham(String tenNhomSanPham) {
        this.tenNhomSanPham = tenNhomSanPham;
    }

    public Integer getAnhDaiDienId() {
        return anhDaiDienId;
    }

    public void setAnhDaiDienId(Integer anhDaiDienId) {
        this.anhDaiDienId = anhDaiDienId;
    }

    public float getTonKho() {
        return tonKho;
    }

    public void setTonKho(Integer tonKho) {
        this.tonKho = tonKho;
    }

    public String getMoTaNgan() {
        return moTaNgan;
    }

    public void setMoTaNgan(String moTaNgan) {
        this.moTaNgan = moTaNgan;
    }

}
