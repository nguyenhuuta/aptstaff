package com.anphuocthai.staff.ui.delivery.debt.requestdebt;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.model.CongNoModel;
import com.anphuocthai.staff.ui.base.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RequestDebtAdapter extends RecyclerView.Adapter<BaseViewHolder> {
    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private List<CongNoModel> orderTranArrayList;

    public RequestDebtAdapter(ArrayList<CongNoModel> orderTranArrayList) {
        this.orderTranArrayList = orderTranArrayList;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new ViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cho_duyet, parent, false));
            case VIEW_TYPE_EMPTY:
            default:
                return new EmptyViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_view, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);

    }

    @Override
    public int getItemCount() {
        if (orderTranArrayList != null && orderTranArrayList.size() > 0) {
            return orderTranArrayList.size();
        }
        return 1;

    }

    @Override
    public int getItemViewType(int position) {
        if (orderTranArrayList != null && orderTranArrayList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    public interface Callback {

    }

    public void addItems(List<CongNoModel> orderTrans) {
        orderTranArrayList.clear();
        orderTranArrayList.addAll(orderTrans);
        notifyDataSetChanged();
    }


    public class ViewHolder extends BaseViewHolder {
        TextView txtOrderCode;
        TextView txtCustomerName, tvTenKhachHang;
        TextView txtSumaryMoney;
        TextView tienDaThu;
        TextView txtRemain;


        public ViewHolder(View itemView) {
            super(itemView);
            initView(itemView);
        }

        private void initView(View itemView) {
            txtOrderCode = itemView.findViewById(R.id.all_debt_txt_order_code);
            txtCustomerName = itemView.findViewById(R.id.all_debt_txt_order_customer_name);
            tvTenKhachHang = itemView.findViewById(R.id.tenKhachHang);
            txtSumaryMoney = itemView.findViewById(R.id.debt_txt_sumary_money);
            tienDaThu = itemView.findViewById(R.id.tienDaThu);
            txtRemain = itemView.findViewById(R.id.debt_txt_remain);
        }

        protected void clear() {
        }

        public void onBind(int position) {
            super.onBind(position);
            CongNoModel orderTran = orderTranArrayList.get(position);
            txtOrderCode.setText(orderTran.orderCode());
            txtCustomerName.setText(orderTran.getContentDetail());
            tvTenKhachHang.setText(orderTran.getTenThanhVien());
            txtSumaryMoney.setText(orderTran.getCongNoDonHangString());
            tienDaThu.setText(orderTran.getTongTienDaThuString());
            txtRemain.setText(orderTran.getSoTienConLaiString());

        }
    }

    public class EmptyViewHolder extends BaseViewHolder {

        @BindView(R.id.btn_retry)
        Button retryButton;

        @BindView(R.id.tv_message)
        TextView messageTextView;

        @BindView(R.id.txt_retry)
        TextView txtRetry;

        public EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            retryButton.setVisibility(View.GONE);
            txtRetry.setVisibility(View.GONE);
        }

        @Override
        protected void clear() {

        }

        @OnClick(R.id.btn_retry)
        void onRetryClick() {
        }
    }
}
