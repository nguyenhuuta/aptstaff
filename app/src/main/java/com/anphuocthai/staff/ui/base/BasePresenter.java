package com.anphuocthai.staff.ui.base;

import android.util.Log;

import com.androidnetworking.common.ANConstants;
import com.androidnetworking.error.ANError;
import com.anphuocthai.staff.R;
import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.baserequest.APIService;
import com.anphuocthai.staff.baserequest.IAppAPI;
import com.anphuocthai.staff.data.DataManager;
import com.anphuocthai.staff.data.db.network.model.ApiError;
import com.anphuocthai.staff.fragments.dialogmessage.DialogMessage;
import com.anphuocthai.staff.utils.Constants;
import com.anphuocthai.staff.utils.rx.SchedulerProvider;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

import javax.inject.Inject;
import javax.net.ssl.HttpsURLConnection;

import io.reactivex.disposables.CompositeDisposable;

public class BasePresenter<V extends MvpView> implements MvpPresenter<V> {

    private static final String TAG = BasePresenter.class.getSimpleName();

    private final NetworkManager mNetworkManager;

    private final DataManager mDataManager;

    private final SchedulerProvider mSchedulerProvider;

    private final CompositeDisposable mCompositeDisposable;

    private V mMvpView;

    private APIService apiService = APIService.getInstance();

    @Inject
    public BasePresenter(NetworkManager networkManager,
                         DataManager dataManager,
                         SchedulerProvider schedulerProvider,
                         CompositeDisposable compositeDisposable) {
        this.mNetworkManager = networkManager;
        this.mDataManager = dataManager;
        this.mSchedulerProvider = schedulerProvider;
        this.mCompositeDisposable = compositeDisposable;
    }

    @Override
    public void onAttach(V mvpView) {
        this.mMvpView = mvpView;
    }

    @Override
    public void onDetach() {
        mCompositeDisposable.dispose();
        mMvpView = null;
    }

    @Override
    public void handleApiError(ANError error) {
        if (error == null || error.getErrorBody() == null) {
            getmMvpView().onError(R.string.api_default_error);
            return;
        }

        if (error.getErrorCode() == Constants.API_STATUS_CODE_LOCAL_ERROR
                && error.getErrorDetail().equals(ANConstants.CONNECTION_ERROR)) {
            getmMvpView().onError(R.string.connection_error);
            return;
        }


        if (error.getErrorCode() == Constants.API_STATUS_CODE_LOCAL_ERROR
                && error.getErrorDetail().equals(ANConstants.REQUEST_CANCELLED_ERROR)) {
            getmMvpView().onError(R.string.api_retry_error);
            return;
        }

        final GsonBuilder builder = new GsonBuilder().excludeFieldsWithoutExposeAnnotation();
        final Gson gson = builder.create();

        try {
            ApiError apiError = gson.fromJson(error.getErrorBody(), ApiError.class);

            if (apiError == null || apiError.getMessage() == null) {
                getmMvpView().onError(R.string.api_default_error);
                return;
            }

            switch (error.getErrorCode()) {
                case HttpsURLConnection.HTTP_UNAUTHORIZED:
                case HttpsURLConnection.HTTP_FORBIDDEN:
                    //setUserAsLoggedOut();
                    getmMvpView().openActivityOnTokenExpire();
                case HttpsURLConnection.HTTP_INTERNAL_ERROR:
                case HttpsURLConnection.HTTP_NOT_FOUND:
                default:
                    getmMvpView().onError(apiError.getMessage());
            }
        } catch (JsonSyntaxException | NullPointerException e) {
            Log.e(TAG, "handleApiError", e);
            getmMvpView().onError(R.string.api_default_error);
        }
    }

    @Override
    public String getToken() {
        return getDataManager().getToken();
    }

    public boolean isViewAttached() {
        return mMvpView != null;
    }

    public V getmMvpView() {
        return this.mMvpView;
    }

    public NetworkManager getmNetworkManager() {
        return mNetworkManager;
    }

    public DataManager getDataManager() {
        return mDataManager;
    }

    public SchedulerProvider getSchedulerProvider() {
        return mSchedulerProvider;
    }

    public CompositeDisposable getCompositeDisposable() {
        return mCompositeDisposable;
    }

    public IAppAPI request() {
        if (apiService == null) {
            apiService = APIService.getInstance();
        }
        return apiService.getAppAPI();
    }

    @Override
    public void showDialog(String message, DialogMessage.IOnClickListener callback) {
        mMvpView.showDialog(message, callback);
    }

    @Override
    public void showDialogConfirm(String message, DialogMessage.IOnClickListener callback) {

    }
}
