package com.anphuocthai.staff.ui.ordercart;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.widget.ListPopupWindow;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.data.TypeProduct;
import com.anphuocthai.staff.data.db.model.OrderProduct;
import com.anphuocthai.staff.model.ChietKhauModel;
import com.anphuocthai.staff.model.customer.Customer;
import com.anphuocthai.staff.ui.base.BaseActivity;
import com.anphuocthai.staff.ui.customersearch.SearchActivity;
import com.anphuocthai.staff.ui.delivery.model.Hanghoa;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;
import com.anphuocthai.staff.ui.home.MainActivity;
import com.anphuocthai.staff.ui.ordercart.payconfig.OrderConfigDialog;
import com.anphuocthai.staff.ui.ordercart.sale.SaleDialog;
import com.anphuocthai.staff.utils.Enum;
import com.anphuocthai.staff.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import static com.anphuocthai.staff.utils.Constants.REQUEST_CODE_SEARCH_CUSTOMER;
import static com.anphuocthai.staff.utils.Constants.RESULT_CODE_SEARCH_CUSTOMER;

public class OrderCartActivity extends BaseActivity implements OrderCartMvpView {

    private static final String TAG = OrderCartActivity.class.getSimpleName();

    private RecyclerView recyclerView;

    private TextView txtSumFee;

    private TextView txtSumCV;

    private ConstraintLayout constraintLayout;

    private ConstraintLayout customerDebtLayout;

    private TextView txtNumberDebt;

    private TextView txtSumDebt;

    private Button btnStartOrder;

    public static final String ARG_CLEAR_ALL = "reload_list";

    @Inject
    OrderCartMvpPresenter<OrderCartMvpView> mPresenter;

    @Inject
    LinearLayoutManager mLayoutManager;

    @Inject
    OrderCartAdapter mOrderCartAdapter;
    private TypeProduct mCurrentTypeProduct;

    //  = true trong trường hợp sửa đơn hàng
    private boolean isOpenToEdit = false;

    List<Hanghoa> hanghoaList = new ArrayList<>();

    private OrderTran orderTran;

    private boolean hasRemoveData = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handleIntent();
        setContentView(R.layout.activity_order_cart);
        getmActivityComponent().inject(this);
        mOrderCartAdapter.setmPresenter(mPresenter);
        mOrderCartAdapter.setBaseActivity(this);
        mOrderCartAdapter.setPercenChietKhau(mPresenter.percenChietKhau());
        mOrderCartAdapter.setCallback(new OrderCartAdapter.Callback() {
            @Override
            public void onDeleteItem(OrderProduct product, OrderCartAdapter.CallbackDelete callbackDelete) {
                hasRemoveData = true;
                showDialogConfirm(getString(R.string.order_cart_delete_product), () -> {
                    mPresenter.onDeleteOrderCart(product);
                    callbackDelete.confirmDelete();
                });
            }

            @Override
            public void onCheckBoxClick(boolean isCheck) {
                if (mOrderCartAdapter != null) {
                    checkBox.setOnCheckedChangeListener(null);
                    checkBox.setChecked(mOrderCartAdapter.checkAllSelected());
                    checkBox.setOnCheckedChangeListener(onCheckAll);
                }
            }

            @Override
            public void showToast() {
                showDialog("Số CV vượt cho phép", null);
            }
        });
        mPresenter.onAttach(this);
        initToolBar();
        initView();
    }


    private void initToolBar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_white_24dp);
        toolbar.setNavigationOnClickListener(view -> {
            finish();
        });
    }

    private void showPopupMenu(List<TypeProduct> list, List<ChietKhauModel> listChietKhau) {
        if (list.size() == 0) return;

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView anchor = toolbar.findViewById(R.id.textSpinner);

        ListPopupWindow listPopupWindow = new ListPopupWindow(
                this);
        ArrayAdapter<TypeProduct> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, list);
        listPopupWindow.setAdapter(adapter);
        listPopupWindow.setAnchorView(anchor);

        listPopupWindow.setModal(true);
        listPopupWindow.setOnItemClickListener((adapterView, view, position, id) -> {
            Object item = adapterView.getItemAtPosition(position);
            if (item instanceof TypeProduct) {
                mCurrentTypeProduct = (TypeProduct) item;
                anchor.setText(mCurrentTypeProduct.getName());
            }
            mOrderCartAdapter.changeTypeProduct(mCurrentTypeProduct);
            listPopupWindow.dismiss();

        });
        mCurrentTypeProduct = list.get(0);
        anchor.setText(mCurrentTypeProduct.getName());
        anchor.setOnClickListener(v -> listPopupWindow.show());
        mOrderCartAdapter.addListTypeProduct(list, listChietKhau);
    }


    @Override
    public Runnable onButtonBackClick() {
        return () -> {
            if (hasRemoveData) {
                sendActionReloadOrderScreen(false);
            }
            finish();
        };
    }

    private void handleIntent() {
        Bundle bundle = getIntent().getBundleExtra("OrderCartExtra");
        isOpenToEdit = bundle.getBoolean("isEditOrder");
        if (isOpenToEdit) {
            orderTran = (OrderTran) bundle.getSerializable("OrderProduct");
            hanghoaList = orderTran.getHanghoas();
        }
    }

    private void initView() {

        customerDebtLayout = findViewById(R.id.customer_debt_layout);
        customerDebtLayout.setVisibility(View.GONE);

        txtNumberDebt = findViewById(R.id.txt_number_debt);
        txtSumDebt = findViewById(R.id.txtSumDebt);

        txtSumFee = findViewById(R.id.order_cart_sum_fee_all);
        txtSumCV = findViewById(R.id.order_cart_sum_cv_all);
        btnStartOrder = findViewById(R.id.btn_confirm_transport);

        btnStartOrder.setOnClickListener(view -> {
            recyclerView.scrollToPosition(0);
            if (isOpenToEdit) {
                if (orderTran != null) {
                    if (mOrderCartAdapter.checkCustomerAndPayMethodCondition()) {
                        mPresenter.cancelOrderTran(orderTran);
                    }
                }
            } else {
                startOrder();
            }
        });

        constraintLayout = findViewById(R.id.order_cart_container_sum);
        recyclerView = findViewById(R.id.order_recycler_order_cart_list);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        // set adapter here
        recyclerView.setAdapter(mOrderCartAdapter);

        if (!isOpenToEdit) {
            if (mPresenter.isHaveCustomerSave()) {
                if (!mPresenter.getGuidCustomerSave().equals("")) {
                    mPresenter.getCustomerByGuid(mPresenter.getGuidCustomerSave());
                }
            } else {
                mPresenter.onGetAllOrderProduct();
            }
        } else {
            mPresenter.getCustomerByGuid(orderTran.getGuidThanhVien());
        }
    }


    public static Intent getOrderCartActivityIntent(Context context) {
        Intent intent = new Intent(context, OrderCartActivity.class);
        return intent;
    }

    @Override
    public void onBackButtonClick() {
    }

    @Override
    public void onUpdateOrderProducts(List<OrderProduct> orderProducts) {
        mOrderCartAdapter.addItems(orderProducts);
        mPresenter.getListTypeProduct();
        if (orderProducts.size() <= 0) {
            constraintLayout.setVisibility(View.INVISIBLE);
            btnStartOrder.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onUpdateSumaryFeeAndCV(float sumfee, float sumCV, int tongChietKhau) {
        String cv;
        if (tongChietKhau > 0) {
            cv = Utils.formatValue(sumCV - tongChietKhau, Enum.FieldValueType.NORMAL);
        } else {
            cv = Utils.formatValue(sumCV, Enum.FieldValueType.NORMAL);
        }
        txtSumCV.setText(cv);
        String fee;
        if (tongChietKhau > 0) {
            fee = Utils.formatValue(sumfee - tongChietKhau * 1000, Enum.FieldValueType.CURRENCY);
        } else {
            fee = Utils.formatValue(sumfee, Enum.FieldValueType.CURRENCY);
        }
        txtSumFee.setText(fee);
    }

    @Override
    public void onOpenSaleDialog(OrderProduct orderProduct, OrderCartAdapter.ViewHolder viewHolder) {
        SaleDialog.newInstance().show(getSupportFragmentManager(), orderProduct, viewHolder);
    }

    @Override
    public void openSearchActivity() {
        Intent i = new Intent(this, SearchActivity.class);
        this.startActivityForResult(i, REQUEST_CODE_SEARCH_CUSTOMER);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_SEARCH_CUSTOMER && resultCode == RESULT_CODE_SEARCH_CUSTOMER) {
            Customer customer = (Customer) data.getSerializableExtra("customer_extra");
            mOrderCartAdapter.setCustomer(customer);
            if (customer.getGuid() != null && !customer.getGuid().isEmpty()) {
                mPresenter.getAllOrderDeptCustomer(customer.getGuid());
            }
        }
    }

    @Override
    public void onCreateOrderSuccess() {
        showMessage(R.string.order_created_success);
        sendActionReloadOrderScreen(true);
        finish();
    }

    @Override
    public void startOrder() {
        mOrderCartAdapter.onShowConfigDialogOrder();
    }

    @Override
    public void onUpdateCustomerDebt(ArrayList<OrderTran> orderTrans) {
        if (orderTrans != null && orderTrans.size() > 0) {
            customerDebtLayout.setVisibility(View.VISIBLE);
        } else {
            customerDebtLayout.setVisibility(View.GONE);
        }
        txtNumberDebt.setText(orderTrans.size() + " đơn");

        int sum = 0;

        for (int i = 0; i < orderTrans.size(); i++) {
            sum += orderTrans.get(i).getPhaiThanhToan();
        }

        txtSumDebt.setText(Utils.formatValue(sum, Enum.FieldValueType.CURRENCY));

    }


    @Override
    public void onShowOrderConfigDialog(Customer customer, OrderCartAdapter adapter) {
        OrderConfigDialog.newInstance().show(getSupportFragmentManager(), customer, adapter);
    }

    @Override
    public void onGetCustomerByGuidSuccess(Customer customer) {
        mOrderCartAdapter.setCustomer(customer);
        mOrderCartAdapter.notifyItemChanged(0);
        mPresenter.onGetAllOrderProduct();
    }


    @Override
    public void showListTypeProduct(Pair<List<TypeProduct>, List<ChietKhauModel>> pair) {
        showPopupMenu(pair.first, pair.second);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    hideKeyboard();
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

    private CheckBox checkBox;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_order_carts, menu);


        MenuItem menuItem = menu.findItem(R.id.menuCheckBox);
        checkBox = (CheckBox) MenuItemCompat.getActionView(menuItem);
        checkBox.setOnCheckedChangeListener(onCheckAll);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menu_remove_all_cart) {
            showDialogConfirm(getString(R.string.order_delete_all_carts), () -> {
                mPresenter.onDeleteAllOrderCart();
                sendActionReloadOrderScreen(true);
            });
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    CompoundButton.OnCheckedChangeListener onCheckAll = (buttonView, isChecked) -> {
        if (mOrderCartAdapter != null && mOrderCartAdapter.getItemCount() > 0) {
            mOrderCartAdapter.checkAll(isChecked, mCurrentTypeProduct);
        }

    };

    private void sendActionReloadOrderScreen(boolean isClearAll) {
        Intent intent = new Intent();
        intent.putExtra(ARG_CLEAR_ALL, isClearAll);
        intent.setAction(MainActivity.BADGE_CHANGE);
        sendBroadcast(intent);
    }

}
