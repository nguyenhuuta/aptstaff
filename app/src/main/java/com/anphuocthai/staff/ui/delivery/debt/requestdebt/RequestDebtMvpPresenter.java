package com.anphuocthai.staff.ui.delivery.debt.requestdebt;

import com.anphuocthai.staff.ui.base.MvpPresenter;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;

import org.json.JSONObject;

import java.util.List;

import io.reactivex.Flowable;

public interface RequestDebtMvpPresenter<V extends RequestDebtMvpView> extends MvpPresenter<V> {
    void onViewPrepared(int page);

    Flowable<List<OrderTran>> getAllDebtApiCall(JSONObject object);
}
