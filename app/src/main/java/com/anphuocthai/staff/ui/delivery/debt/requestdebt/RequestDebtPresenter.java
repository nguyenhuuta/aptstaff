package com.anphuocthai.staff.ui.delivery.debt.requestdebt;

import com.anphuocthai.staff.api.ApiURL;
import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.data.DataManager;
import com.anphuocthai.staff.model.CongNoParams;
import com.anphuocthai.staff.model.CongNoResponse;
import com.anphuocthai.staff.ui.base.BasePresenter;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;
import com.anphuocthai.staff.utils.App;
import com.anphuocthai.staff.utils.Logger;
import com.anphuocthai.staff.utils.rx.SchedulerProvider;
import com.rx2androidnetworking.Rx2AndroidNetworking;

import org.json.JSONObject;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class RequestDebtPresenter<V extends RequestDebtMvpView> extends BasePresenter<V> implements RequestDebtMvpPresenter<V> {

    private static final String TAG = RequestDebtPresenter.class.getSimpleName();

    @Inject
    public RequestDebtPresenter(NetworkManager networkManager, DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(networkManager, dataManager, schedulerProvider, compositeDisposable);
    }

    private JSONObject createJSONObject(final int page) {
        JSONObject notTranObject = new JSONObject();
        try {
            notTranObject.put("phanLoai", 5);
            notTranObject.put("guidThanhVien", "");
            notTranObject.put("sessionID", "");
            notTranObject.put("tuNgay", "");
            notTranObject.put("denNgay", "");
            notTranObject.put("trangThaiIds", "");
            notTranObject.put("pageIndex", page);
            notTranObject.put("pageSize", 10);
            notTranObject.put("BoPhanID", App.getInstance().getDepartmentId());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return notTranObject;
    }

    @Override
    public Flowable<List<OrderTran>> getAllDebtApiCall(JSONObject object) {
        return Rx2AndroidNetworking.post(ApiURL.GET_ORDER_URL)
                .addHeaders(ApiURL.getBaseHeader())
                .addHeaders(ApiURL.getTokenHeader())
                .addJSONObjectBody(object)
                .build()
                .getObjectListFlowable(OrderTran.class);

    }


    @Override
    public void onViewPrepared(int page) {
        if (getmMvpView() != null) {
            getmMvpView().showLoading();
        }
        CongNoParams congNoParams = new CongNoParams(getDataManager().getUserInfoId(), page, CongNoParams.TypeChoDuyet);
        Disposable disposable = getmNetworkManager().postRequest(ApiURL.danhSachCongNo, congNoParams, CongNoResponse.class)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(congNoResponse -> {
                    if (getmMvpView() != null) {
                        getmMvpView().hideLoading();
                    }
                    if (congNoResponse != null && congNoResponse.getData() != null) {
                        getmMvpView().updateOrderTran(congNoResponse.getData());
                    }
                }, throwable -> {
                    if (getmMvpView() != null) {
                        getmMvpView().hideLoading();
                    }
                    Logger.d("InventoryDebtPresenter", throwable.getLocalizedMessage());
                    getmMvpView().onError(throwable.getLocalizedMessage());
                });
        getCompositeDisposable().add(disposable);
    }
}
