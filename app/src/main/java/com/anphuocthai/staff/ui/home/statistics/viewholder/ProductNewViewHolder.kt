package com.anphuocthai.staff.ui.home.statistics.viewholder

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.app.ActivityOptionsCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.anphuocthai.staff.R
import com.anphuocthai.staff.activities.listtopproduct.ListTopProductActivity
import com.anphuocthai.staff.baserequest.APICallback
import com.anphuocthai.staff.baserequest.APIService
import com.anphuocthai.staff.entities.TopSellBody
import com.anphuocthai.staff.model.order.Product
import com.anphuocthai.staff.ui.base.BaseViewHolder
import com.anphuocthai.staff.ui.home.MainActivity
import com.anphuocthai.staff.ui.product.DetailProductActivity
import com.anphuocthai.staff.utils.Utils
import com.anphuocthai.staff.utils.extension.loadImage
import kotlinx.android.synthetic.main.item_product_new.view.*
import kotlinx.android.synthetic.main.sub_item_product_new.view.*
import java.util.*

/**
 * Created by OpenYourEyes on 6/11/2020
 */
class ProductNewViewHolder(private val view: View) : BaseViewHolder(view) {
    lateinit var layoutManager: LinearLayoutManager
    val title = "Sản phẩm mới"

    override fun clear() {
    }

    override fun onBind(position: Int) {
        super.onBind(position)
        view.titleRecyclerView.text = title
        getData()
    }

    private fun getData() {
        view.post {
            view.shimmerView.startShimmerAnimation()
        }
        val cal = Calendar.getInstance()
        cal.time = Date()
        val day = cal[Calendar.DAY_OF_MONTH]
        val month = cal[Calendar.MONTH] + 1
        val year = cal[Calendar.YEAR]
        val topSellBody = TopSellBody(day, month, year, 2, true)
        APIService.getInstance().appAPI.getProductNew(topSellBody).getAsyncResponse(object : APICallback<List<Product>> {
            override fun onSuccess(topSellProductModels: List<Product>) {
                fillData(topSellProductModels.toMutableList())
            }

            override fun onFailure(errorMessage: String) {
                view.shimmerView.stopShimmerAnimation()
            }
        })
    }

    private fun fillData(products: MutableList<Product>) {
        view.shimmerView.stopShimmerAnimation()
        with(view) {
            seeMore.visibility = View.VISIBLE
            shimmerView.visibility = View.GONE
            rowOne.visibility = View.VISIBLE
            rowTwo.visibility = View.VISIBLE
        }
        fillItem(itemView.item1, products.firstOrNull())
        fillItem(itemView.item2, products.getOrNull(1))
        fillItem(itemView.item3, products.getOrNull(2))
        fillItem(itemView.item4, products.getOrNull(3))
        view.seeMore.setOnClickListener {
            val context = it.context
            val intent = Intent(context, ListTopProductActivity::class.java)
            intent.putExtra(ListTopProductActivity.ARG_PRODUCT, java.util.ArrayList(products))
            intent.putExtra(ListTopProductActivity.ARG_TITLE, title)
            context.startActivity(intent)
        }
    }

    private fun fillItem(itemProduct: View, product: Product?) {
        product?.let {
            itemProduct.imageProduct.loadImage(product.thumbId)
            itemProduct.productName.text = product.name
            itemProduct.priceProduct.text = Utils.currency(product.mainPrice?.kDBanBuonGia ?: 0)

            itemProduct.setOnClickListener {
                itemView.context?.let {
                    if (it is MainActivity) {
                        val intent = Intent(it, DetailProductActivity::class.java)
                        val bundle = Bundle()
                        bundle.putInt("productid", product.id)
                        bundle.putInt("imageProduct", product.thumbId)
                        intent.putExtra("productExtra", bundle)
                        val options: ActivityOptionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(it, itemProduct.imageProduct, "imageProduct")
                        it.startActivity(intent, options.toBundle())
                    }
                }
            }
        } ?: kotlin.run {
            itemProduct.imageProduct.setImageResource(R.drawable.ic_no_image)
            itemView.productName.text = ""
            itemView.priceProduct.text = ""
        }

    }

}