package com.anphuocthai.staff.ui.customer.detail.debtdetail;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.di.component.ActivityComponent;
import com.anphuocthai.staff.ui.base.BaseFragment;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;
import com.anphuocthai.staff.utils.App;

import java.util.List;

import javax.inject.Inject;

public class DebtDetailFragment extends BaseFragment implements DebtDetailMvpView {

    private static final String TAG = DebtDetailFragment.class.getSimpleName();

    private RecyclerView recyclerView;

    private RecyclerView.LayoutManager aLayout;

    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Inject
    DebtDetailAdapter mAdapter;

    private Context context;

    private String guid;

    @Inject
    DebtDetailMvpPresenter<DebtDetailMvpView> mPresenter;

    @Inject
    LinearLayoutManager linearLayoutManager;


    public DebtDetailFragment() {
        // Required empty public constructor
        this.context = App.getAppContext();
    }

    public static DebtDetailFragment newInstance(String guid, Context context) {
        DebtDetailFragment fragment = new DebtDetailFragment();
        fragment.guid = guid;
        fragment.context = context;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getActivityComponent().inject(this);
//        mPresenter.onAttach(this);
//        mPresenter.getHistoryTrans(guid);
    }

    @Override
    public void updateOrderTran(List<OrderTran> orderTranArrayList) {
        mAdapter.addItems(orderTranArrayList);
    }


    @Override
    protected void setUp(View view) {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.history_tran_fragment, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            mPresenter.onAttach(this);
            mPresenter.getHistoryTrans(guid);
        }

        recyclerView = view.findViewById(R.id.history_tran_recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(mAdapter);
        return view;
    }
}
