
package com.anphuocthai.staff.ui.home.statistics.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DailyCV {

    @SerializedName("nhan")
    @Expose
    private String nhan;
    @SerializedName("nhan1")
    @Expose
    private String nhan1;
    @SerializedName("giaTri")
    @Expose
    private Integer giaTri;
    @SerializedName("giaTri1")
    @Expose
    private Integer giaTri1;
    @SerializedName("giaTri2")
    @Expose
    private Integer giaTri2;

    public String getNhan() {
        return nhan;
    }

    public void setNhan(String nhan) {
        this.nhan = nhan;
    }

    public String getNhan1() {
        return nhan1;
    }

    public void setNhan1(String nhan1) {
        this.nhan1 = nhan1;
    }

    public Integer getGiaTri() {
        return giaTri;
    }

    public void setGiaTri(Integer giaTri) {
        this.giaTri = giaTri;
    }

    public Integer getGiaTri1() {
        return giaTri1;
    }

    public void setGiaTri1(Integer giaTri1) {
        this.giaTri1 = giaTri1;
    }

    public Integer getGiaTri2() {
        return giaTri2;
    }

    public void setGiaTri2(Integer giaTri2) {
        this.giaTri2 = giaTri2;
    }

}
