package com.anphuocthai.staff.ui.delivery.nottransportyet.trachungtu;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.model.ThongTinDonHangModel;
import com.anphuocthai.staff.ui.base.BaseViewHolder;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;
import com.anphuocthai.staff.utils.Enum;
import com.anphuocthai.staff.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by OpenYourEyes on 11/23/2020
 */
public class TraChungTuAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private static final String TAG = com.anphuocthai.staff.ui.delivery.nottransportyet.NotTranAdapter.class.getSimpleName();

    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private List<OrderTran> mOrderTrans;


    TraChungTuAdapter() {
        mOrderTrans = new ArrayList<>();
    }

    void updateList(List<OrderTran> orderTrans, boolean isRefresh) {
        if (isRefresh) {
            mOrderTrans.clear();
        }
        mOrderTrans.addAll(orderTrans);
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new TraChungTuAdapter.ViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_tra_chung_tu, parent, false));
            case VIEW_TYPE_EMPTY:
            default:
                return new TraChungTuAdapter.EmptyViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_view, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mOrderTrans != null && mOrderTrans.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }


    }

    @Override
    public int getItemCount() {
        if (mOrderTrans != null && mOrderTrans.size() > 0) {
            return mOrderTrans.size();
        } else {
            return 1;
        }

    }

    public void addItems(List<OrderTran> orderTrans) {
        mOrderTrans.clear();
        mOrderTrans.addAll(orderTrans);
        notifyDataSetChanged();
    }

    public interface Callback {
        void onBlogEmptyViewRetryClick();
    }

    public class ViewHolder extends BaseViewHolder {
        TextView donHangId, nhanVienId, tenKhachHang;
        TextView address, thongTinDonHang, tongCanVaCV;
        TextView nguoiVanChuyen, countdownTime;
        TextView tongTienDaThu;


        public ViewHolder(View itemView) {
            super(itemView);
            initView(itemView);
        }

        private void initView(View itemView) {
            donHangId = itemView.findViewById(R.id.donHangId);
            nhanVienId = itemView.findViewById(R.id.nhanVienId);
            tenKhachHang = itemView.findViewById(R.id.tenKhachHang);
            address = itemView.findViewById(R.id.address);
            thongTinDonHang = itemView.findViewById(R.id.thongTinDonHang);
            tongCanVaCV = itemView.findViewById(R.id.tongCanVaCV);
            nguoiVanChuyen = itemView.findViewById(R.id.nguoiVanChuyen);
            countdownTime = itemView.findViewById(R.id.countdownTime);
            tongTienDaThu = itemView.findViewById(R.id.tongTienDaThu);
        }

        protected void clear() {
        }

        public void onBind(int position) {
            super.onBind(position);
            OrderTran orderTran = mOrderTrans.get(position);
            String donDat = "ĐH: " + orderTran.getMaDonDat();
            donHangId.setText(donDat);
            String tenNhanVien = "Tên NV: " + orderTran.getTenNhanVien();
            nhanVienId.setText(tenNhanVien);
            String khachHang = "Khách: " + orderTran.getTenThanhVien();
            tenKhachHang.setText(khachHang);
            String diaChi = "Địa chỉ: " + orderTran.getDiaChiNguoiMua();
            address.setText(diaChi);
            ThongTinDonHangModel thongTinDonHangModel = orderTran.getThongTinDonHang();
            thongTinDonHang.setText(thongTinDonHangModel.getTenCacMatHang());
            String tongCan = "Tổng cân: " + Utils.formatValue(thongTinDonHangModel.getTongSoLuong(), Enum.FieldValueType.WEIGHT);
            tongCan = tongCan + "       CV: " + thongTinDonHangModel.getTongCV();
            tongCanVaCV.setText(tongCan);
            String nguoiVC = "Người VC: " + orderTran.getTenNhanVien();
            nguoiVanChuyen.setText(nguoiVC);

            String startEnd = orderTran.getCountDown();
            if (startEnd.isEmpty()) {
                countdownTime.setVisibility(View.GONE);
            } else {
                countdownTime.setVisibility(View.VISIBLE);
                countdownTime.setText(startEnd);
            }


            if (orderTran.isBanGiaoChungTu) {
                tongTienDaThu.setVisibility(View.GONE);
            } else {
                tongTienDaThu.setVisibility(View.VISIBLE);
                String money = "Tiền đã thu: " + Utils.formatValue(orderTran.tienVanChuyenThu, Enum.FieldValueType.CURRENCY);
                tongTienDaThu.setText(money);
            }
        }
    }

    public class EmptyViewHolder extends BaseViewHolder {

        @BindView(R.id.btn_retry)
        Button retryButton;

        @BindView(R.id.tv_message)
        TextView messageTextView;

        @BindView(R.id.txt_retry)
        TextView txtRetry;

        public EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            retryButton.setVisibility(View.GONE);
            txtRetry.setVisibility(View.GONE);
        }

        @Override
        protected void clear() {
        }

        @OnClick(R.id.btn_retry)
        void onRetryClick() {
        }
    }
}