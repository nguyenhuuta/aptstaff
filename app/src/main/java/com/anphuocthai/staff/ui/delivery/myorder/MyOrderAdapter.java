package com.anphuocthai.staff.ui.delivery.myorder;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.interfaces.IYNDialogCallback;
import com.anphuocthai.staff.ui.base.BaseActivity;
import com.anphuocthai.staff.ui.base.BaseViewHolder;
import com.anphuocthai.staff.ui.delivery.debtlocation.DebtLocationActivity;
import com.anphuocthai.staff.ui.delivery.model.Hanghoa;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;
import com.anphuocthai.staff.ui.delivery.myorder.detailorder.DetailMyOrderActivity;
import com.anphuocthai.staff.ui.delivery.nottransportyet.NotTranAdapter;
import com.anphuocthai.staff.utils.Enum;
import com.anphuocthai.staff.utils.Utils;
import com.anphuocthai.staff.utils.ui.UIUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.anphuocthai.staff.utils.Enum.FieldValueType.CURRENCY;
import static com.anphuocthai.staff.utils.Enum.FieldValueType.FLOAT_NORMAL;
import static com.anphuocthai.staff.utils.Enum.FieldValueType.NORMAL;

public class MyOrderAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private static final String TAG = NotTranAdapter.class.getSimpleName();

    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private Callback mCallback;
    private List<OrderTran> mOrderTrans;

    MyOrderMvpPresenter<MyOrderMvpView> mPresenter;

    BaseActivity baseActivity;


    public void setmPresenter(MyOrderMvpPresenter<MyOrderMvpView> mPresenter) {
        this.mPresenter = mPresenter;
    }

    public void setBaseActivity(BaseActivity baseActivity) {
        this.baseActivity = baseActivity;
    }

    public MyOrderAdapter(List<OrderTran> mOrderTrans) {
        this.mOrderTrans = mOrderTrans;
    }

    public void setCallback(Callback callback) {
        mCallback = callback;
    }


    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new ViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_don_hang_cua_toi, parent, false));
            case VIEW_TYPE_EMPTY:
            default:
                return new EmptyViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_view, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mOrderTrans != null && mOrderTrans.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }


    }

    @Override
    public int getItemCount() {
        if (mOrderTrans != null && mOrderTrans.size() > 0) {
            return mOrderTrans.size();
        } else {
            return 1;
        }

    }

    public void addItems(List<OrderTran> orderTrans) {
        mOrderTrans.clear();
        mOrderTrans.addAll(orderTrans);
        notifyDataSetChanged();
    }

    public void addItemLoadMore(List<OrderTran> orderTrans) {
        mOrderTrans.addAll(orderTrans);
        notifyDataSetChanged();
    }

    public void resetData(OrderTran orderTran) {
        this.mOrderTrans.remove(orderTran);
        notifyDataSetChanged();
    }

    public interface Callback {
        void onBlogEmptyViewRetryClick();
    }

    public class ViewHolder extends BaseViewHolder {
        TextView txtOrderCode;
        TextView txtCustomerAddress;
        TextView txtCustomerName;
        TextView txtProductInfo;
        TextView btnSubmitTran;

        //
        TextView labelSumaryMoney;
        TextView txtSumaryMoney;

        TextView labelMustPay;
        TextView txtMustPay;

        TextView labelRemain;
        TextView txtRemain;

        TextView labelAlreadyCollect;
        TextView txtAlreadyCollect;

        TextView labelCerti;
        TextView txtCerti;
        TextView tvKiemDuyet;
        TextView tienDaThu;
        TextView countdownTime;

        private TextView txtStateOrder;

        private TextView txtEditOrder;

        private TextView txtCancelOrder;


        private TextView debtLocation;

        public ViewHolder(View itemView) {
            super(itemView);
            initView(itemView);
        }

        private void initView(View itemView) {
            txtOrderCode = itemView.findViewById(R.id.all_debt_txt_order_code);
            txtCustomerAddress = itemView.findViewById(R.id.all_debt_txt_order_customer_address);
            txtCustomerName = itemView.findViewById(R.id.all_debt_txt_order_customer_name);
            txtProductInfo = itemView.findViewById(R.id.all_debt_txt_product_info);
            btnSubmitTran = itemView.findViewById(R.id.all_debt_btn_submit);
            btnSubmitTran.setVisibility(View.INVISIBLE);
            txtEditOrder = itemView.findViewById(R.id.btn_edit_order);
            txtCancelOrder = itemView.findViewById(R.id.btn_cancel_order);


            labelSumaryMoney = itemView.findViewById(R.id.debt_label_sumary_money);
            txtSumaryMoney = itemView.findViewById(R.id.debt_txt_sumary_money);
            labelMustPay = itemView.findViewById(R.id.debt_label_must_pay);
            txtMustPay = itemView.findViewById(R.id.debt_txt_must_pay);
            labelRemain = itemView.findViewById(R.id.debt_label_remain);
            txtRemain = itemView.findViewById(R.id.debt_txt_remain);
            labelAlreadyCollect = itemView.findViewById(R.id.debt_label_already_collect);
            txtAlreadyCollect = itemView.findViewById(R.id.debt_txt_already_collect);
            labelCerti = itemView.findViewById(R.id.debt_label_certi);

            txtCerti = itemView.findViewById(R.id.debt_txt_certi);
            txtStateOrder = itemView.findViewById(R.id.txt_state_order);
            tvKiemDuyet = itemView.findViewById(R.id.tvKiemDuyet);

            debtLocation = itemView.findViewById(R.id.txt_debt_location);
            debtLocation.setVisibility(View.GONE);
            tienDaThu = itemView.findViewById(R.id.tienDaThu);
            countdownTime = itemView.findViewById(R.id.countdownTime);


        }

        protected void clear() {
        }

        public void onBind(int position) {
            super.onBind(position);
            final OrderTran orderTran = mOrderTrans.get(position);
            if (orderTran.getMaDonDat() != null) {
                String date = orderTran.getNgayKetThuc() != null ? orderTran.getNgayKetThuc() : orderTran.getNgayBatDau();
                String time = orderTran.getMaDonDat() + " - " + Utils.formatDate(date, true);
                txtOrderCode.setText(time);
            }
            tvKiemDuyet.setVisibility(orderTran.isDuyetCapCao() ? View.VISIBLE : View.GONE);
            if (orderTran.getTrangThaiText() != null) {
                txtStateOrder.setVisibility(View.VISIBLE);
                String trangThaiText = " - " + orderTran.getTrangThaiText();
                txtStateOrder.setText(trangThaiText);
                txtEditOrder.setVisibility(View.GONE);
                txtCancelOrder.setVisibility(View.GONE);
                if (orderTran.getDDTrangThaiID() == Enum.OrderState.NEW) {
                    txtEditOrder.setVisibility(View.VISIBLE);
                    txtCancelOrder.setVisibility(View.VISIBLE);
                    txtEditOrder.setOnClickListener((View v) -> {


                        UIUtils.showYesNoDialog(baseActivity, "", String.format(baseActivity.getString(R.string.alert_cancel_to_edit_order), orderTran.getMaDonDat().toString()), new IYNDialogCallback() {
                            @Override
                            public void accept() {
                                mPresenter.editOrderStepCancelOrder(orderTran);
                            }

                            @Override
                            public void cancel() {

                            }
                        });

                    });
                    txtCancelOrder.setOnClickListener((View v) -> {
                        UIUtils.showYesNoDialog(baseActivity, "", baseActivity.getString(R.string.cancel_order), new IYNDialogCallback() {
                            @Override
                            public void accept() {
                                mPresenter.cancelOrderTran(orderTran);
                            }

                            @Override
                            public void cancel() {
                            }
                        });
                    });
                }
            }


            if (orderTran.getTenThanhVien() != null) {
                txtCustomerName.setText(orderTran.getTenThanhVien());
            }

            txtCustomerAddress.setVisibility(View.GONE);
            txtCustomerAddress.setText("");
            if (orderTran.getVanchuyens() != null) {
                int size = orderTran.getVanchuyens().size();
                if (size > 0) {
                    String addressList = new String();
                    if (orderTran.getVanchuyens().get(0).getDiaChiNhanHang() != null) {
                        addressList += orderTran.getVanchuyens().get(0).getDiaChiNhanHang();
                    }
                    txtCustomerAddress.setText(addressList);
                }
                if (txtCustomerAddress.getText().toString().trim().isEmpty()) {
                    txtCustomerAddress.setVisibility(View.GONE);
                }
            }

            txtProductInfo.setVisibility(View.VISIBLE);
            txtProductInfo.setText("");
            if (orderTran.getHanghoas() != null) {
                int size = orderTran.getHanghoas().size();
                if (size > 0) {
                    String goodsNames = "";
                    for (int i = 0; i < size; i++) {
                        if (orderTran.getHanghoas().get(i).getHanghoa().getTenHangHoa() != null) {
                            Hanghoa hanghoa = orderTran.getHanghoas().get(i);
                            if ((i == 0 && (size == 1)) || i == size - 1) {
                                goodsNames += hanghoa.getHanghoa().getTenHangHoa() + "(" + Utils.formatValue(hanghoa.getSoLuong(), FLOAT_NORMAL) + hanghoa.getDonVi() + ")";
                            } else {
                                goodsNames += hanghoa.getHanghoa().getTenHangHoa() + "(" + Utils.formatValue(hanghoa.getSoLuong(), FLOAT_NORMAL) + hanghoa.getDonVi() + ")" + "\n";
                            }

                        }
                    }

                    txtProductInfo.setText(goodsNames);
                }
            }


            txtSumaryMoney.setText("");
            if (orderTran.getTongGiaTri() != null) {
                txtSumaryMoney.setText(Utils.formatValue(orderTran.getTongGiaTri(), CURRENCY));
            }

            txtMustPay.setText("");
            if (orderTran.getTongThanhToan() != null) {
                txtMustPay.setText(Utils.formatValue(orderTran.getTongThanhToan(), CURRENCY));
            }

            txtRemain.setText("");
            if (orderTran.getPhaiThanhToan() != null) {
                txtRemain.setText(Utils.formatValue(orderTran.getPhaiThanhToan(), CURRENCY));
            }

            labelAlreadyCollect.setVisibility(View.VISIBLE);
            txtAlreadyCollect.setVisibility(View.VISIBLE);

            labelAlreadyCollect.setText(R.string.order_home_cv);

            if (orderTran.getTongCV() != null) {
                txtAlreadyCollect.setText(Utils.formatValue(orderTran.getTongCV(), NORMAL));
            }

            txtCerti.setText(R.string.debt_info_default);

            if (orderTran.getVanchuyens() != null && orderTran.getVanchuyens().size() > 0) {
                if (orderTran.getVanchuyens().get(0).getTenNguoiChuyen() != null && !orderTran.getVanchuyens().get(0).getTenNguoiChuyen().isEmpty()) {
                    if (orderTran.getVanchuyens().get(0).getTrangThaiID() != 3) {
                        txtCerti.setText(orderTran.getVanchuyens().get(0).getTenNguoiChuyen());
                    }
                } else {
                    txtCerti.setText(baseActivity.getString(R.string.identifi_undefine));
                }
            }

            if (orderTran.getVanchuyens() != null && orderTran.getVanchuyens().size() > 0) {
                if (orderTran.getVanchuyens().get(0).getTenNguoiChuyen() != null && !orderTran.getVanchuyens().get(0).getTenNguoiChuyen().isEmpty()) {
                    if (orderTran.getVanchuyens().get(0).getTrangThaiID() != 3) {
                        txtCerti.setText(orderTran.getVanchuyens().get(0).getTenNguoiChuyen());
                    }
                } else {
                    txtCerti.setText(baseActivity.getString(R.string.identifi_undefine));
                }
            }

            labelCerti.setText(R.string.transport_person);

            if (orderTran.getDDTrangThaiID() != null) {
                if (orderTran.getDDTrangThaiID() == Enum.OrderState.BE_TRANSPORT ||
                        orderTran.getDDTrangThaiID() == Enum.OrderState.DELIVERED ||
                        orderTran.getDDTrangThaiID() == Enum.OrderState.RETURNS) {
                    debtLocation.setVisibility(View.VISIBLE);


                    debtLocation.setOnClickListener((View v) -> {
                        Intent i = DebtLocationActivity.getDebtLocationIntent(baseActivity);

                        if (orderTran.getVanchuyens() != null && orderTran.getVanchuyens().size() > 0) {
                            Bundle bundle = new Bundle();
                            bundle.putDouble("latitude", orderTran.getVanchuyens().get(0).getLatitude());
                            bundle.putDouble("longitude", orderTran.getVanchuyens().get(0).getLongitude());
                            bundle.putString("debtname", orderTran.getVanchuyens().get(0).getTenNguoiChuyen());
                            i.putExtra("LatLong", bundle);
                        }


                        baseActivity.startActivity(i);
                    });
                } else {
                    debtLocation.setVisibility(View.GONE);
                }
            }
            if (orderTran.isBanGiaoChungTu) {
                tienDaThu.setVisibility(View.GONE);
            } else {
                tienDaThu.setVisibility(View.VISIBLE);
                String money = "Tiền đã thu: " + Utils.formatValue(orderTran.tienVanChuyenThu, Enum.FieldValueType.CURRENCY);
                tienDaThu.setText(money);
            }
            itemView.setOnClickListener((View v) -> {
                Intent i = DetailMyOrderActivity.getDetailMyOrderIntent(baseActivity);
                if (orderTran.getSessionID() != null) {
                    Bundle bundle = new Bundle();
                    bundle.putString("sessionID", orderTran.getSessionID());
                    i.putExtra("session", bundle);
                }
                baseActivity.startActivity(i);
            });
            String startEnd = orderTran.getCountDown();
            if (startEnd.isEmpty()) {
                countdownTime.setVisibility(View.GONE);
            } else {
                countdownTime.setVisibility(View.VISIBLE);
                countdownTime.setText(startEnd);
            }
        }
    }

    public class EmptyViewHolder extends BaseViewHolder {

        @BindView(R.id.btn_retry)
        Button retryButton;

        @BindView(R.id.tv_message)
        TextView messageTextView;

        @BindView(R.id.txt_retry)
        TextView txtRetry;

        public EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            retryButton.setVisibility(View.GONE);
            txtRetry.setVisibility(View.GONE);
        }

        @Override
        protected void clear() {

        }

        @OnClick(R.id.btn_retry)
        void onRetryClick() {
            if (mCallback != null)
                mCallback.onBlogEmptyViewRetryClick();
        }
    }
}
