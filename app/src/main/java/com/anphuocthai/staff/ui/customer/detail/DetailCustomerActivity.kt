package com.anphuocthai.staff.ui.customer.detail

import android.content.Intent
import android.os.Bundle
import androidx.viewpager.widget.ViewPager
import com.anphuocthai.staff.R
import com.anphuocthai.staff.activities.customer.AddNewCustomerActivity
import com.anphuocthai.staff.model.customer.SearchCustomer
import com.anphuocthai.staff.ui.base.BaseActivity
import com.anphuocthai.staff.ui.base.ViewPagerAdapter
import com.anphuocthai.staff.ui.customer.detail.config.ConfigCustomerFM
import com.anphuocthai.staff.ui.customer.detail.debtdetail.DebtDetailFragment
import com.anphuocthai.staff.ui.customer.detail.historytransactions.HistoryTranFragment
import com.anphuocthai.staff.ui.customer.detail.infomation.GeneralCustomerInfoFragment
import com.anphuocthai.staff.ui.customer.detail.note.CustomerNoteFM
import com.anphuocthai.staff.utils.App
import com.anphuocthai.staff.utils.printLog
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


/**
 * This class show customer detail when you click on a customer
 */


class DetailCustomerActivity : BaseActivity() {
    private lateinit var viewPager: ViewPager
    private lateinit var searchCustomer: SearchCustomer
    private lateinit var viewPagerAdapter: ViewPagerAdapter
    private var guid: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        printLog("onCreate")
        EventBus.getDefault().register(this)
        setContentView(R.layout.activity_detail_customer)
        val bundle = intent.getBundleExtra("detailcustomeractivity")
        if (intent != null && bundle != null) {
            guid = bundle.getString("customerguid")
            val customerName = bundle.getString("customername")
            App.setContext(this)
            searchCustomer = bundle.getParcelable(ARG_CUSTOMER)!!
            setupViewPager()
            setupToobarWithTab(customerName, viewPager, false)
        }
    }


    private fun setupViewPager() {
        viewPager = findViewById(R.id.viewpagerDetailCustomer)
        viewPagerAdapter = ViewPagerAdapter(supportFragmentManager)
        viewPagerAdapter.addFragment(
            GeneralCustomerInfoFragment.newInstance(searchCustomer),
            getString(R.string.ci_detail_customer_gerneral_info)
        )
        viewPagerAdapter.addFragment(
            HistoryTranFragment.newInstance(guid, this),
            getString(R.string.detail_customer_history_tran)
        )
        viewPagerAdapter.addFragment(
            CustomerNoteFM.newInstance(guid, this),
            getString(R.string.detail_customer_note)
        )
        viewPagerAdapter.addFragment(
            DebtDetailFragment.newInstance(guid, this),
            getString(R.string.detail_customer_debt)
        )
        viewPagerAdapter.addFragment(
            ConfigCustomerFM.newInstance(guid, this),
            getString(R.string.detail_customer_config)
        )
        viewPager.adapter = viewPagerAdapter
    }


    fun editCustomer() {
        val i = Intent(this, AddNewCustomerActivity::class.java)
        val bundle = Bundle()
        bundle.putParcelable(AddNewCustomerActivity.ARG_USER, searchCustomer)
        i.putExtras(bundle)
        startActivity(i)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: SearchCustomer) {
        printLog(this, "onMessageEvent $event")
        if (!isDestroyed) {
            val fragment = viewPagerAdapter.getFragmentAt(viewPager.currentItem)
            if (fragment is GeneralCustomerInfoFragment) {
                showDialog("Dữ liệu bị thay đổi, vui lòng quay lại để load dữ liệu mới") {
                    finish()
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        printLog(this, "onDestroy")
        EventBus.getDefault().unregister(this);
    }

    companion object {
        const val ARG_CUSTOMER = "customer"
    }
}
