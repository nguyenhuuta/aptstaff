package com.anphuocthai.staff.ui.customer.detail.infomation.editinformation.phonetypedialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.fragment.app.FragmentManager;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.di.component.ActivityComponent;
import com.anphuocthai.staff.model.DanhMucLoaiKhachHangModel;
import com.anphuocthai.staff.ui.base.BaseDialog;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class PhoneTypeTypeDialog extends BaseDialog implements PhoneTypeMvpView {

    private static final String TAG = PhoneTypeTypeDialog.class.getSimpleName();

    private PhoneTypeCallBack callBack;
    @Inject
    PhoneTypeMvpPresenter<PhoneTypeMvpView> mPresenter;

    private ArrayList<String> radioButtons;


    public interface PhoneTypeCallBack {
        void onSetPhoneType(int position);

        void onSetOtherText(String otherText);
    }

    @Override
    protected void setUp(View view) {
    }

    public static PhoneTypeTypeDialog newInstance() {
        PhoneTypeTypeDialog fragment = new PhoneTypeTypeDialog();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.phone_type_dialog_layout, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            initEvents(view);
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    public void hideKeyboard() {
        super.hideKeyboard();
    }

    private void initEvents(View view) {


        //Defining 5 buttons.
        //int buttons = 5;

        AppCompatRadioButton[] rb = new AppCompatRadioButton[radioButtons.size()];

        RadioGroup rgp = view.findViewById(R.id.phone_type_radio_group);
        rgp.setOrientation(LinearLayout.VERTICAL);

        for (int i = 0; i < radioButtons.size(); i++) {
            RadioButton rbn = new RadioButton(getBaseActivity());
            rbn.setId(i + 1000);
            rbn.setText(radioButtons.get(i));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            params.setMargins(20, 20, 20, 20);
            rbn.setLayoutParams(params);
            rgp.addView(rbn);

            int finalI = i;
            rbn.setOnClickListener((View v) -> {

//                if (finalI == radioButtons.size() - 1) {
//                    LayoutInflater layoutInflaterAndroid = LayoutInflater.from(getBaseActivity());
//                    View mView = layoutInflaterAndroid.inflate(R.layout.user_input_dialog_box, null);
//                    AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(getBaseActivity(), R.style.MyDialogTheme);
//                    alertDialogBuilderUserInput.setView(mView);
//
//                    final EditText userInputDialogEditText = (EditText) mView.findViewById(R.id.userInputDialog);
//                    TextView txtTitle = mView.findViewById(R.id.dialogTitle);
//
//                    txtTitle.setText(R.string.common_input_data);
//                    userInputDialogEditText.setHint(R.string.common_input_infor);
//                    alertDialogBuilderUserInput
//                            .setCancelable(false)
//                            .setPositiveButton("Đồng ý", (dialogBox, id) -> {
//                                // ToDo get user input here
//                                callBack.onSetOtherText(userInputDialogEditText.getText().toString());
//                                dismissDialog();
//                            })
//
//                            .setNegativeButton("Hủy",
//                                    (dialogBox, id) -> {
//                                        dialogBox.cancel();
//                                        dismissDialog();
//                                    });
//
//                    AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
//                    alertDialogAndroid.show();
//                } else {
//                    callBack.onSetPhoneType(finalI);
//                    dismissDialog();
//                }

                callBack.onSetPhoneType(finalI);
                dismissDialog();
            });
        }


    }

    /**
     * From edit my profile
     *
     * @param fragmentManager
     * @param radioButtons
     * @param callBack
     */
    public void show(FragmentManager fragmentManager,
                     ArrayList<String> radioButtons,
                     PhoneTypeCallBack callBack) {
        super.show(fragmentManager, TAG);
        this.callBack = callBack;
        this.radioButtons = radioButtons;
    }

    /**
     * From add new customer scren
     *
     * @param fragmentManager
     * @param danhMucLoaiKhachHangs
     * @param callBack
     */
    public void show(FragmentManager fragmentManager,
                     List<DanhMucLoaiKhachHangModel> danhMucLoaiKhachHangs,
                     PhoneTypeCallBack callBack) {
        super.show(fragmentManager, TAG);
        this.callBack = callBack;
        ArrayList<String> list = new ArrayList<>();
        for (DanhMucLoaiKhachHangModel model : danhMucLoaiKhachHangs) {
            list.add(model.getTen());
        }
        this.radioButtons = list;
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void dismissDialog() {
        super.dismissDialog(TAG);
    }

    @Override
    public void onStopDebtSuccess() {
        dismissDialog();
    }
}
