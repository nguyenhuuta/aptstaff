package com.anphuocthai.staff.ui.searchsimpledata;

import android.content.Intent;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.ui.base.BaseActivity;
import com.anphuocthai.staff.utils.Utils;
import com.arlib.floatingsearchview.FloatingSearchView;

import java.util.ArrayList;
import java.util.List;

public class SearchSimpleActivity extends BaseActivity implements SearchSimpleAdapter.OnEmployeeSelectedListener {

    private static final String TAG = SearchSimpleActivity.class.getSimpleName();

    public static final int REQUEST_CODE_SELECT_EMP_TAB1 = 101;
    public static final int REQUEST_CODE_SELECT_EMP_TAB2 = 102;

    public static final int REQUEST_CODE_SELECT_CUS_TAB1 = 111;
    public static final int REQUEST_CODE_SELECT_CUS_TAB2 = 112;
    public static final int REQUEST_CODE_SELECT_CUS_TAB3 = 113;
    public static final int REQUEST_CODE_SELECT_CUS_TAB4 = 114;
    public static final int REQUEST_CODE_SELECT_CUS_TAB5 = 115;
    public static final int REQUEST_CODE_SELECT_CUS_TAB6 = 116;

    public static final int REQUEST_CODE_SELECT_ORDER_STATE = 121;

    public static final String NAME_PARCELABLE_ARRAYLIST = "NAME_PARCELABLE_ARRAYLIST";
    public static final String KEY_ID = "KEY_ID";
    public static final int ID_NULL = -1;

    private FloatingSearchView floatingSearchView;

    private List<SearchSimpleItem> mSearchSimpleItems;

    private RecyclerView mSearchResultsList;

    private SearchSimpleAdapter mSearchSimpleAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_search);
        addControls();
        addEvents();
    }

    private void addControls() {
        floatingSearchView = findViewById(R.id.floating_search_view);
        mSearchResultsList = findViewById(R.id.search_results_list);
        mSearchResultsList.setLayoutManager(new LinearLayoutManager(this));

        mSearchSimpleItems = getIntent().getExtras().getParcelableArrayList(NAME_PARCELABLE_ARRAYLIST);
        mSearchSimpleAdapter = new SearchSimpleAdapter(mSearchSimpleItems, this);
        mSearchResultsList.setAdapter(mSearchSimpleAdapter);
    }

    private void addEvents() {
        floatingSearchView.setOnQueryChangeListener((oldQuery, newQuery) -> {
            if (newQuery != null && !newQuery.isEmpty()) {
                ArrayList<SearchSimpleItem> searchSimpleItems = new ArrayList<>();
                for (SearchSimpleItem se : mSearchSimpleItems) {
                    if (se.getBody().toLowerCase().contains(newQuery.toLowerCase())) {
                        searchSimpleItems.add(se);
                    }
                }
                mSearchSimpleAdapter.updateSearchEmployees(searchSimpleItems);
            } else {
                mSearchSimpleAdapter.updateSearchEmployees(mSearchSimpleItems);
            }
        });

        floatingSearchView.setOnHomeActionClickListener(() -> {
            setResult(RESULT_CANCELED);
            finish();
        });
    }

    @Override
    public void onEmployeeSelected(Integer id) {
        Utils.hideKeyboard(SearchSimpleActivity.this);
        Intent intent = new Intent();
        intent.putExtra(KEY_ID, id);
        setResult(RESULT_OK, intent);
        finish();
    }
}
