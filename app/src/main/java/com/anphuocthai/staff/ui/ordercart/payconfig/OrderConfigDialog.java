package com.anphuocthai.staff.ui.ordercart.payconfig;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentManager;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.di.component.ActivityComponent;
import com.anphuocthai.staff.interfaces.IYNDialogCallback;
import com.anphuocthai.staff.model.customer.Customer;
import com.anphuocthai.staff.ui.base.BaseDialog;
import com.anphuocthai.staff.ui.delivery.debt.holdingdebt.HoldingDebtFragment;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;
import com.anphuocthai.staff.ui.ordercart.OrderCartAdapter;
import com.anphuocthai.staff.utils.Enum;
import com.anphuocthai.staff.utils.Utils;
import com.anphuocthai.staff.utils.ui.UIUtils;

import javax.inject.Inject;

import static com.anphuocthai.staff.utils.Enum.PayMethod.CASH;
import static com.anphuocthai.staff.utils.Enum.PayMethod.DEBT;
import static com.anphuocthai.staff.utils.Enum.PayMethod.PAY_AT_DELIVERED;
import static com.anphuocthai.staff.utils.Enum.PayMethod.TRANSFER;

public class OrderConfigDialog extends BaseDialog implements OrderConfigMvpView, AdapterView.OnItemSelectedListener {

    private static final String TAG = OrderConfigDialog.class.getSimpleName();

    private TextView txtPersonOrder;

    private TextView txtCustomer;

    private Button btnCancel;

    private Button btnOK;

    private Spinner spinner;

    private EditText edtConfigAddress;

    private CheckBox chkAttachDebt;

    private TextView txtDeptType;

    private EditText edtConfigNote;

    private int payType = -1;

    private Customer customer;

    private OrderCartAdapter adapter;

    private ConstraintLayout constraintLayout;

    private HoldingDebtFragment fragment;

    private TextView txtPreviousNote;

    private TextView lblPreviousNote;

    @Inject
    OrderConfigMvpPresenter<OrderConfigMvpView> mPresenter;

    private OrderTran orderTran;

    @Override
    protected void setUp(View view) {
    }

    public static OrderConfigDialog newInstance() {
        OrderConfigDialog fragment = new OrderConfigDialog();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.pay_config_dialog_layout, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            setupView(view);
            initEvents();
            mPresenter.onAttach(this);
        }

        mPresenter.getCustomerByGuid(customer.getGuid());
        return view;
    }

    @Override
    public void hideKeyboard() {
        super.hideKeyboard();
    }

    private void setupView(View view) {

        txtCustomer = view.findViewById(R.id.txt_customer_order_config);

        spinner = view.findViewById(R.id.spinner_order_config);

        txtDeptType = view.findViewById(R.id.txt_debt_type_customer);

        txtPersonOrder = view.findViewById(R.id.txt_person_order);

        chkAttachDebt = view.findViewById(R.id.check_box_attach_debt);

        edtConfigAddress = view.findViewById(R.id.edt_order_config_address);

        btnCancel = view.findViewById(R.id.order_config_btn_cancel);

        btnOK = view.findViewById(R.id.order_config_btn_ok);

        edtConfigNote = view.findViewById(R.id.edt_config_note);

        txtPreviousNote = view.findViewById(R.id.txtPreviousNote);
        lblPreviousNote = view.findViewById(R.id.lblPreviousNote);
        txtPreviousNote.setVisibility(View.GONE);
        lblPreviousNote.setVisibility(View.GONE);

        setupData();

        setupSpinner();
    }

    private void setupData() {
        edtConfigNote.clearFocus();

        if (customer == null) {
            return;
        }

        if (customer.getGhiChuDonHang() != null && !customer.getGhiChuDonHang().trim().equals("")) {
            txtPreviousNote.setVisibility(View.VISIBLE);
            lblPreviousNote.setVisibility(View.VISIBLE);
            txtPreviousNote.setText(customer.getGhiChuDonHang());
            txtPreviousNote.setOnClickListener((View v) -> {
                edtConfigNote.setText(customer.getGhiChuDonHang());
            });
        }

        if (customer.getTenDayDu() != null) {
            txtCustomer.setText(customer.getTenDayDu());
        }

        if (customer.getDiaChi() != null) {
            edtConfigAddress.setText(customer.getDiaChi());
            edtConfigAddress.clearFocus();
        }

        txtPersonOrder.setText(R.string.common_no_data);
        if (customer.getCustomerData() != null) {
            if (customer.getCustomerData().getNguoiDatDon() != null) {
                txtPersonOrder.setText(customer.getCustomerData().getNguoiDatDon());
            }
        }

        txtDeptType.setText("");
        txtDeptType.setVisibility(View.GONE);

        if (customer.getCongNo() == null) {
            return;
        }

        if (customer.getCongNo() <= 0) {
            chkAttachDebt.setVisibility(View.GONE);
        } else {
            //Log.d(TAG, chkAttachDebt.getText() + " (" + Utils.formatValue(customer.getCongNo(), Enum.FieldValueType.CURRENCY) + ")");
            chkAttachDebt.setText(R.string.attach_debt);
            chkAttachDebt.setText(chkAttachDebt.getText() + " (" + Utils.formatValue(customer.getCongNo(), Enum.FieldValueType.CURRENCY) + ")");
        }
    }

    private void setupSpinner() {

        spinner.setOnItemSelectedListener(this);

        String[] categories = {"",
                getString(R.string.order_cart_cash),
                getString(R.string.order_cart_bank_transfer),
                getString(R.string.order_cart_debt),
                getString(R.string.order_cart_bank_pay_at_position)
        };

        // Creating adapter for spinner
        MyArrayAdapter dataAdapter = new MyArrayAdapter(getBaseActivity(), R.layout.spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
        spinner.setSelection(1);
        if (mPresenter.isHaveCustomerSave()) {
            if (mPresenter.getCustomerPayType() != -1) {
                switch (mPresenter.getCustomerPayType()) {
                    case CASH:
                      spinner.setSelection(1);
                        break;
                    case TRANSFER:
                       spinner.setSelection(2);
                        break;
                    case DEBT:
                        spinner.setSelection(3);
                        break;
                    case PAY_AT_DELIVERED:
                        spinner.setSelection(4);
                        break;
                    default:
                        spinner.setSelection(0);
                        break;
                }
            }
        }
    }

    private void initEvents() {
        btnCancel.setOnClickListener((View v) -> {
            dismissDialog();
        });

        btnOK.setOnClickListener((View v) -> {
            if (payType == -1) {
                showMessage(R.string.order_type_pay_validate);
                return;
            }
            UIUtils.showYesNoDialog(getBaseActivity(), "", getString(R.string.order_cart_start_order_confirm), new IYNDialogCallback() {
                @Override
                public void accept() {
                    String note = new String();
                    note = edtConfigNote.getText().toString();
                    if (chkAttachDebt.isChecked()) {
                        note = note + " " + "\n" + " (" + getString(R.string.attach_debt) + ")";
                    }
                    adapter.startOrderFromDialog(payType, note, edtConfigAddress.getText().toString());
                    dismissDialog();
                }

                @Override
                public void cancel() {
                }
            });
        });

//        constraintLayout.setOnClickListener((View v) -> {
//            InputMethodManager imm = (InputMethodManager) getBaseActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
//            edtRealReceive.clearFocus();
//        });


    }

    public void show(FragmentManager fragmentManager, Customer customer, OrderCartAdapter adapter) {
        super.show(fragmentManager, TAG);
        //this.orderTran = orderTran;
        this.fragment = fragment;
        this.customer = customer;
        this.adapter = adapter;

    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void dismissDialog() {
        super.dismissDialog(TAG);
    }

    @Override
    public void onStopDebtSuccess() {
        dismissDialog();
        fragment.onRefresh();
    }

    @Override
    public void getCustomerByGuidSuccess(Customer customer) {
        this.customer = customer;
        setupData();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
        switch (position) {
            case 0:
                payType = -1;
                txtDeptType.setVisibility(View.GONE);
                break;
            case 1:
                payType = CASH;
                txtDeptType.setVisibility(View.GONE);
                break;
            case 2:
                payType = TRANSFER;
                txtDeptType.setVisibility(View.GONE);
                break;
            case 3:
                payType = DEBT;
                debtSpinnerSelectSetup();
                break;
            case 4:
                payType = PAY_AT_DELIVERED;
                txtDeptType.setVisibility(View.GONE);
                break;
            default:
                payType = -1;
                txtDeptType.setVisibility(View.GONE);
                break;
        }
    }

    private void debtSpinnerSelectSetup() {
        if (customer.getCustomerData() != null && customer.getCustomerData().getThanhToanCongNo() != null) {
            txtDeptType.setVisibility(View.VISIBLE);
            txtDeptType.setText(customer.getCustomerData().getThanhToanCongNo());
        } else {
            txtDeptType.setVisibility(View.VISIBLE);
            txtDeptType.setText(R.string.must_setup_debt_config);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private class MyArrayAdapter extends ArrayAdapter<String> {

        private int textViewResourceId;

        public MyArrayAdapter(Context context, int textViewResourceId, String[] objects) {
            super(context, textViewResourceId, objects);
            this.textViewResourceId = textViewResourceId;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            View view;
            TextView text;

            if (convertView == null) {
                view = getLayoutInflater().inflate(textViewResourceId, parent, false);
            } else {
                view = convertView;
            }
            text = (TextView) view;
            String item;

            item = getItem(position);


            text.setText((CharSequence) item);
            if (position == 0) {
                ViewGroup.LayoutParams l = text.getLayoutParams();
                l.height = 1;
                text.setLayoutParams(l);
            } else {
                ViewGroup.LayoutParams l = text.getLayoutParams();
                //you can change height value to yours
                l.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                text.setLayoutParams(l);
            }

            return view;
        }
    }

}
