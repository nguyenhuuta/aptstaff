package com.anphuocthai.staff.ui.delivery.debt.inventorydebt;

import com.anphuocthai.staff.ui.base.MvpView;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;

import java.util.List;

public interface InventoryDebtMvpView extends MvpView {
    void updateOrderTran(List<OrderTran> orderTranArrayList);

    void yeuCauMuonThanhCong();
}
