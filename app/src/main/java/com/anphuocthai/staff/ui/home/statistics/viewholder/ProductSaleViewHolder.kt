package com.anphuocthai.staff.ui.home.statistics.viewholder

import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.app.ActivityOptionsCompat
import androidx.core.content.ContextCompat
import com.anphuocthai.staff.R
import com.anphuocthai.staff.activities.listtopproduct.ListTopProductActivity
import com.anphuocthai.staff.baserequest.APICallback
import com.anphuocthai.staff.baserequest.APIService
import com.anphuocthai.staff.entities.TopSellBody
import com.anphuocthai.staff.model.order.Product
import com.anphuocthai.staff.model.order.ProductSaleModel
import com.anphuocthai.staff.model.order.ProductSaleResponse
import com.anphuocthai.staff.ui.base.BaseViewHolder
import com.anphuocthai.staff.ui.home.MainActivity
import com.anphuocthai.staff.ui.product.DetailProductActivity
import com.anphuocthai.staff.utils.Utils
import com.anphuocthai.staff.utils.extension.loadImage
import kotlinx.android.synthetic.main.item_product_sale.view.*
import java.util.*

/**
 * Created by OpenYourEyes on 6/11/2020
 */
class ProductSaleViewHolder(private val rootView: View) : BaseViewHolder(rootView) {
    val title = "Sản phẩm giảm giá"

    init {
        getData()
    }

    fun bindData(products: List<ProductSaleModel>) {
        rootView.titleRecyclerView.text = title
        rootView.seeMore.setOnClickListener {
            val context = it.context
            val intent = Intent(context, ListTopProductActivity::class.java)
            intent.putExtra(ListTopProductActivity.ARG_PRODUCT, ArrayList(products))
            intent.putExtra(ListTopProductActivity.ARG_TITLE, title)
            intent.putExtra(ListTopProductActivity.ARG_SALE, true)
            context.startActivity(intent)
        }

        fun onItemClick(view: View, imageView: ImageView, product: Product) {
            view.setOnClickListener {
                itemView.context?.let {
                    if (it is MainActivity) {
                        val intent = Intent(it, DetailProductActivity::class.java)
                        val bundle = Bundle()
                        bundle.putInt("productid", product.id)
                        bundle.putInt("imageProduct", product.thumbId)
                        intent.putExtra("productExtra", bundle)
                        val options: ActivityOptionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(it, imageView, "imageProduct")
                        it.startActivity(intent, options.toBundle())
                    }
                }
            }
        }

        if (products.isEmpty()) return

        fun fillData(
                product: ProductSaleModel?,
                imageProduct: ImageView,
                productName: TextView,
                priceProduct1: TextView,
                priceProduct2: TextView,
                imageStatusPrice: ImageView,
                rootView: View
        ) {
            if (product == null) return

            val isPriceUp = product.isPriceUp()


            val colorRed = ContextCompat.getColor(rootView.context, R.color.red)
            val colorBlack = ContextCompat.getColor(rootView.context, R.color.black)
            if (isPriceUp) {
                imageStatusPrice.setImageResource(android.R.drawable.stat_sys_upload)
                imageStatusPrice.setColorFilter(ContextCompat.getColor(rootView.context, R.color.red))
                priceProduct1.setTextColor(colorRed)
                priceProduct2.paintFlags = priceProduct2.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                priceProduct2.setTextColor(colorBlack)

                priceProduct1.text = Utils.currency(product.giaKM ?: 0)
                priceProduct2.text = Utils.currency(product.giaGoc ?: 0)

            } else {
                imageStatusPrice.setImageResource(android.R.drawable.stat_sys_download)
                imageStatusPrice.setColorFilter(ContextCompat.getColor(rootView.context, R.color.colorPrimary))
                priceProduct2.setTextColor(colorRed)
                priceProduct1.paintFlags = priceProduct1.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                priceProduct1.setTextColor(colorBlack)

                priceProduct1.text = Utils.currency(product.giaGoc ?: 0)
                priceProduct2.text = Utils.currency(product.giaKM ?: 0)
            }
            imageProduct.loadImage(product.thumbId)
            productName.text = product.name
            onItemClick(rootView, imageProduct, product)

        }
        fillData(products.getOrNull(0), rootView.image1, rootView.productName1, rootView.oldPrice1, rootView.priceProduct1, rootView.imageStatusPrice1, rootView.layout1)
        fillData(products.getOrNull(1), rootView.image2, rootView.productName2, rootView.oldPrice2, rootView.priceProduct2, rootView.imageStatusPrice2, rootView.layout2)
        fillData(products.getOrNull(2), rootView.image3, rootView.productName3, rootView.oldPrice3, rootView.priceProduct3, rootView.imageStatusPrice3, rootView.layout3)
        fillData(products.getOrNull(3), rootView.image4, rootView.productName4, rootView.oldPrice4, rootView.priceProduct4, rootView.imageStatusPrice4, rootView.layout4)
        fillData(products.getOrNull(4), rootView.image5, rootView.productName5, rootView.oldPrice5, rootView.priceProduct5, rootView.imageStatusPrice5, rootView.layout5)
    }

    override fun clear() {
    }

    private fun getData() {
        val cal = Calendar.getInstance()
        cal.time = Date()
        val day = cal[Calendar.DAY_OF_MONTH]
        val month = cal[Calendar.MONTH] + 1
        val year = cal[Calendar.YEAR]
        val topSellBody = TopSellBody(day, month, year, 2, true)

        APIService.getInstance().appAPI.getProductSale(topSellBody).getAsyncResponse(object : APICallback<ProductSaleResponse> {
            override fun onSuccess(topSellProductModels: ProductSaleResponse) {
                topSellProductModels.mergerList()
                bindData(topSellProductModels.list)
            }

            override fun onFailure(errorMessage: String) {
            }
        })
    }

}