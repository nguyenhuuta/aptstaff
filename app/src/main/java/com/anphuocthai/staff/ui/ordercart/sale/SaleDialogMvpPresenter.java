package com.anphuocthai.staff.ui.ordercart.sale;

import com.anphuocthai.staff.ui.base.MvpPresenter;

public interface SaleDialogMvpPresenter<V extends SaleDialogMvpView> extends MvpPresenter<V> {

    void onRatingSubmitted(float rating, String message);

    void onCancelClicked();

    void onLaterClicked();

    void onPlayStoreRatingClicked();
}

