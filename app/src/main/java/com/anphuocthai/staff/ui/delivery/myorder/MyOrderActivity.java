package com.anphuocthai.staff.ui.delivery.myorder;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.ui.base.BaseActivity;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;
import com.anphuocthai.staff.ui.delivery.myorder.adjust.AdjustOrderDialog;
import com.anphuocthai.staff.ui.ordercart.OrderCartActivity;
import com.anphuocthai.staff.ui.searchsimpledata.SearchSimpleActivity;
import com.anphuocthai.staff.ui.searchsimpledata.SearchSimpleItem;

import org.reactivestreams.Publisher;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.processors.PublishProcessor;
import io.reactivex.schedulers.Schedulers;

public class MyOrderActivity extends BaseActivity implements MyOrderMvpView, SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = MyOrderActivity.class.getSimpleName();
    private static final int NOT_FILTER = -2;

    @Inject
    MyOrderMvpPresenter<MyOrderMvpView> mPresenter;

    public MyOrderMvpPresenter<MyOrderMvpView> getmPresenter() {
        return mPresenter;
    }

    @Inject
    MyOrderAdapter mAdapter;

    @Inject
    LinearLayoutManager mLayoutManager;


    private RecyclerView recyclerView;

    private ProgressBar progressBar;

    private SwipeRefreshLayout mSwipeRefreshLayout;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private PublishProcessor<Integer> paginator = PublishProcessor.create();

    private boolean loading = false;
    private int pageNumber = 0;
    private final int VISIBLE_THRESHOLD = 1;
    private int lastVisibleItem, totalItemCount;

    private ConstraintLayout layoutSumary;

    private TextView txtNumberOrder;
    private TextView txtSumaryMoney;

    private List<OrderTran> mOrderTrans = new ArrayList<>();

    private int filterId = NOT_FILTER;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_order);
        setupToolbar(getString(R.string.ci_my_order));
        getmActivityComponent().inject(this);
        mPresenter.onAttach(this);
        mAdapter.setmPresenter(mPresenter);
        mAdapter.setBaseActivity(this);
        initView();
        setupPullToRefresh();

    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    private void initView() {
        txtNumberOrder = findViewById(R.id.txt_number_order);
        txtSumaryMoney = findViewById(R.id.txt_sumary_cv);
        layoutSumary = findViewById(R.id.layout_sumary);
        layoutSumary.setVisibility(View.GONE);
        progressBar = findViewById(R.id.my_order_progressbar);
        recyclerView = findViewById(R.id.delivery_my_order_recycler_view);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        mPresenter.hideRecyclerView(true);
        setUpLoadMoreListener();
        subscribeForData();
    }


    @Override
    public void resetLoadMoreData(OrderTran orderTran) {
        mOrderTrans.remove(orderTran);
        mAdapter.resetData(orderTran);
    }


    /**
     * setting listener to get callback for load more
     */
    private void setUpLoadMoreListener() {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView,
                                   int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = mLayoutManager.getItemCount();
                lastVisibleItem = mLayoutManager
                        .findLastVisibleItemPosition();
                if (!loading
                        && totalItemCount <= (lastVisibleItem + VISIBLE_THRESHOLD)) {
                    pageNumber++;
                    paginator.onNext(pageNumber);
                    loading = true;
                }
            }
        });
    }


    /**
     * subscribing for data
     */
    private void subscribeForData() {
        Disposable disposable = paginator
                .onBackpressureDrop()
                .concatMap((Function<Integer, Publisher<List<OrderTran>>>) page -> {
                    loading = true;
                    progressBar.setVisibility(View.VISIBLE);
                    return mPresenter.rxJavaOnGetAllMyOrder(page);
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(orderTrans -> {
                    mPresenter.hideRecyclerView(false);
                    mOrderTrans.addAll(orderTrans);
                    showFilterList();
                    loading = false;
                    progressBar.setVisibility(View.GONE);
                }, throwable -> {
                    loading = false;
                    progressBar.setVisibility(View.GONE);
                    showDialog(throwable.getMessage(), null);
                });

        compositeDisposable.add(disposable);
        paginator.onNext(pageNumber);

    }

    public static Intent getMyOrderIntent(Context context) {
        return new Intent(context, MyOrderActivity.class);
    }

    @Override
    public void displayOrderTrans(ArrayList<OrderTran> orderTrans) {
        filterId = NOT_FILTER;
        mOrderTrans.clear();
        mOrderTrans.addAll(orderTrans);
        mAdapter.addItems(mOrderTrans);
        mPresenter.hideRecyclerView(false);
        mSwipeRefreshLayout.setRefreshing(false);
        layoutSumary.setVisibility(View.VISIBLE);
        txtNumberOrder.setText(orderTrans.size() + " đơn");
        int sumCV = 0;
        for (int i = 0; i < orderTrans.size(); i++) {
            sumCV += orderTrans.get(i).getTongCV();
        }
        txtSumaryMoney.setText(sumCV + " CV");
    }

    @Override
    public void hideRecyclerView(boolean isHide) {
        if (isHide) {
            recyclerView.setVisibility(View.INVISIBLE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        //mPresenter.onGetAllMyOrder();
    }

    @Override
    public void showAdjustDialog(OrderTran orderTran) {
        AdjustOrderDialog.newInstance().show(getSupportFragmentManager(), orderTran, this);
    }

    @Override
    public void onCancelToEditOrderSuccess(String message, OrderTran orderTran) {
        showMessage(message + "\n" + getString(R.string.move_product_to_cart));

        // cancel success -> DELETE all products in cart if cart have product
        mPresenter.onDeleteAllOrderCart(orderTran);
    }

    @Override
    public void onDeleteCartSuccess(OrderTran orderTran) {
        // after remove all, add new product to order cart
        mPresenter.insertProductToCart(orderTran);
    }

    @Override
    public void onCreateProductSuccess(OrderTran orderTran) {
        Intent intent = OrderCartActivity.getOrderCartActivityIntent(this);
        Bundle bundle = new Bundle();
        bundle.putBoolean("isEditOrder", true);
        bundle.putSerializable("OrderProduct", (Serializable) orderTran);
        intent.putExtra("OrderCartExtra", bundle);
        startActivity(intent);
    }

    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.setRefreshing(false);
        // Fetching data from server
        // mPresenter.onGetAllMyOrder();
    }

    private void setupPullToRefresh() {
        // SwipeRefreshLayout
        mSwipeRefreshLayout = findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);
        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.filter_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menu_filter) {
            showFilterScreen();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showFilterScreen() {
        if (mOrderTrans != null && !mOrderTrans.isEmpty()) {
            Set<Integer> searchEmployeeIds = new HashSet<>();
            ArrayList<SearchSimpleItem> searchSimpleItems = new ArrayList<>();
            for (OrderTran orderTran : mOrderTrans) {
                boolean added = searchEmployeeIds.add(orderTran.getTrangthai());
                if (added) {
                    Integer id = orderTran.getTrangthai();
                    String name = orderTran.getTrangThaiText();
                    if (id == null) {
                        id = SearchSimpleActivity.ID_NULL;
                    }
                    searchSimpleItems.add(new SearchSimpleItem(id, name));
                }
            }

            if (!searchSimpleItems.isEmpty()) {
                Intent intent = new Intent(MyOrderActivity.this, SearchSimpleActivity.class);
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList(SearchSimpleActivity.NAME_PARCELABLE_ARRAYLIST, searchSimpleItems);
                intent.putExtras(bundle);
                startActivityForResult(intent, SearchSimpleActivity.REQUEST_CODE_SELECT_ORDER_STATE);
            } else {
                Toast.makeText(MyOrderActivity.this, "Danh sách lọc bị trống!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(MyOrderActivity.this, "Danh sách lọc bị trống!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SearchSimpleActivity.REQUEST_CODE_SELECT_ORDER_STATE) {
            if (resultCode == Activity.RESULT_OK) {
                filterId = data.getIntExtra(SearchSimpleActivity.KEY_ID, SearchSimpleActivity.ID_NULL);
                showFilterList();
            } else if (resultCode == Activity.RESULT_CANCELED) {
                mAdapter.addItems(mOrderTrans);
            }
        }
    }

    private void showFilterList() {
        if (filterId == NOT_FILTER) {
            Log.e(TAG, "NOT_FILTER, show all order trans");
            mAdapter.addItems(mOrderTrans);
            return;
        }

        ArrayList<OrderTran> searchOrderTran = new ArrayList<>();
        for (OrderTran orderTran : mOrderTrans) {
            if (SearchSimpleActivity.ID_NULL == filterId) {
                if (orderTran.getTrangthai() == null) {
                    searchOrderTran.add(orderTran);
                }
            } else if (orderTran.getTrangthai() != null && orderTran.getTrangthai() == filterId) {
                searchOrderTran.add(orderTran);
            }
        }
        mAdapter.addItems(searchOrderTran);
    }
}

