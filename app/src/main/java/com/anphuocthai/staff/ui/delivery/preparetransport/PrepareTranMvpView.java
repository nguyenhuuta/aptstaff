package com.anphuocthai.staff.ui.delivery.preparetransport;

import com.anphuocthai.staff.ui.base.MvpView;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;

import java.util.ArrayList;

public interface PrepareTranMvpView extends MvpView{
    void displayOrderTrans(ArrayList<OrderTran> orderTrans);
}
