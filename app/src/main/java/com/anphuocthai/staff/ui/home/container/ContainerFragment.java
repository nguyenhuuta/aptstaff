package com.anphuocthai.staff.ui.home.container;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.error.ANError;
import com.anphuocthai.staff.BuildConfig;
import com.anphuocthai.staff.R;
import com.anphuocthai.staff.activities.danhsachcongtac.DanhSachCongTacActivity;
import com.anphuocthai.staff.activities.quycheluong.QuyCheLuongActivity;
import com.anphuocthai.staff.activities.tonghopcong.TongHopCongActivity;
import com.anphuocthai.staff.activities.tonghopluong.TongHopLuongActivity;
import com.anphuocthai.staff.adapters.checkin.CheckInAdapter;
import com.anphuocthai.staff.adapters.checkin.InDayWSAdapter;
import com.anphuocthai.staff.adapters.common.DemoAdapter;
import com.anphuocthai.staff.adapters.more.GeneralAdapter;
import com.anphuocthai.staff.api.ApiURL;
import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.di.component.ActivityComponent;
import com.anphuocthai.staff.entities.SubModel;
import com.anphuocthai.staff.entities.SubUserModel;
import com.anphuocthai.staff.interfaces.IAPICallback;
import com.anphuocthai.staff.model.Statistics;
import com.anphuocthai.staff.model.customer.Schedule;
import com.anphuocthai.staff.ui.base.BaseFragment;
import com.anphuocthai.staff.ui.home.order.OrderMainFragment;
import com.anphuocthai.staff.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

/**
 *
 */
public class ContainerFragment extends BaseFragment implements ContainerMvpView {

    private static final String TAG = ContainerFragment.class.getSimpleName();

    @Inject
    ContainerMvpPresenter<ContainerMvpView> mPresenter;

    private OrderMainFragment orderMainFragment;
    public FrameLayout fragmentContainer;
    private ConstraintLayout fragmentConstraintLayoutContainer;
    public RecyclerView recyclerView;

    private RecyclerView checkInRecyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private Context context;
    private RecyclerView newWSTabRecyclerView;
    private TextView emptyView;

    private ArrayList<Statistics> statisticsArrayList = new ArrayList<>();
    private int index;

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
//        if (orderMainFragment != null) {
//            orderMainFragment.clearAllCheckProduct();
//        }
    }


    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();

    }

    /**
     * Create a new instance of the fragment
     */
    public static ContainerFragment newInstance(int index, Context context) {
        ContainerFragment fragment = new ContainerFragment();
        Bundle b = new Bundle();
        b.putInt("index", index);
        fragment.setArguments(b);
        fragment.context = context;
        return fragment;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view;

        ActivityComponent activityComponent = getActivityComponent();
        if (activityComponent != null) {
            activityComponent.inject(this);
            mPresenter.onAttach(this);
        }
        index = getArguments().getInt("index", 0);
        if (index == 0) { // chart statistic
            view = inflater.inflate(R.layout.fragment_new_wschedule_tab, container, false);
            initFragmentNewWS(view);
        } else if (index == 1) { // order
            view = inflater.inflate(R.layout.order_main_layout, container, false);
            orderMainFragment = new OrderMainFragment();
            orderMainFragment.initOrderView(view, this);
        } else if (index == 2) {
            view = inflater.inflate(R.layout.fragment_container_check_in, container, false);
            initCheckInFragment(view);
        } else if (index == 4) {
            view = inflater.inflate(R.layout.fragment_demo_list, container, false);
            initMoreView(view);
        } else {
            view = inflater.inflate(R.layout.fragment_demo_list, container, false);
            initDemoList(view);
        }

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public ContainerMvpPresenter<ContainerMvpView> getPresenter() {
        return mPresenter;
    }

    public OrderMainFragment getOrderMainFragment() {
        return orderMainFragment;
    }


    private void initFragmentNewWS(View view) {
        newWSTabRecyclerView = view.findViewById(R.id.ci_new_work_schedule_tab_recyclerview);
        newWSTabRecyclerView.setHasFixedSize(true);
        view.findViewById(R.id.ci_ws_txt_next_day).setVisibility(View.GONE);
        TextView emptyText = view.findViewById(R.id.ci_new_wschedule_other_empty_view);
        emptyText.setVisibility(View.VISIBLE);
        emptyText.setText("Báo cáo thống kê");
        view.findViewById(R.id.ci_new_work_schedule_tab_other_recyclerview).setVisibility(View.GONE);
        RecyclerView.LayoutManager aLayout = new LinearLayoutManager(context);
        newWSTabRecyclerView.setLayoutManager(aLayout);
        emptyView = view.findViewById(R.id.ci_new_wschedule_empty_view);
        getIndayWorkSchedule();
    }

    private void getIndayWorkSchedule() {
        JSONObject workScheduleObject = new JSONObject();
        try {
            // lúc hiển thị danh sách không cần guid
            workScheduleObject.put("guidThanhVien", "");
            workScheduleObject.put("tuNgay", Utils.formatDateWithFormat("yyyy-MM-dd", new Date()));
            workScheduleObject.put("denNgay", Utils.formatDateWithFormat("yyyy-MM-dd", new Date()));
            workScheduleObject.put("trangThaiId", 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        NetworkManager.getInstance().sendPostRequestWithArrayResponse(ApiURL.SEARCH_WORK_SCHEDULE_URL, workScheduleObject, new IAPICallback() {
            @Override
            public void onResponseSuccess(JSONObject response) {

            }

            @Override
            public void onResponseSuccess(JSONArray response) {

                if (response != null) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<ArrayList<Schedule>>() {
                    }.getType();
                    ArrayList<Schedule> result = gson.fromJson(response.toString(), type);

                    InDayWSAdapter adapter = new InDayWSAdapter(result, context, true, true, true);
                    newWSTabRecyclerView.setAdapter(adapter);
                    if (result.isEmpty()) {
                        newWSTabRecyclerView.setVisibility(View.GONE);
                        emptyView.setVisibility(View.VISIBLE);
                    } else {
                        newWSTabRecyclerView.setVisibility(View.VISIBLE);
                        emptyView.setVisibility(View.GONE);
                    }

                }
            }

            @Override
            public void onResponseError(ANError anError) {

            }
        });
    }


    /**
     * Init the fragment
     */
    private void initDemoList(View view) {
        fragmentContainer = view.findViewById(R.id.fragment_container);
        recyclerView = view.findViewById(R.id.fragment_demo_recycler_view);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        ArrayList<String> itemsData = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            itemsData.add("Fragment " + getArguments().getInt("index", -1) + " / Item " + i);
        }

        DemoAdapter adapter = new DemoAdapter(itemsData);
        recyclerView.setAdapter(adapter);
    }

    private void initCheckInFragment(View view) {
        fragmentConstraintLayoutContainer = view.findViewById(R.id.ci_fragment_container_check_in);
        checkInRecyclerView = view.findViewById(R.id.ci_fragment_container_recycler_view);
        checkInRecyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        checkInRecyclerView.setLayoutManager(layoutManager);

        try {
            ArrayList<String> itemsData = new ArrayList<>();
            itemsData.add(getBaseActivity().getString(R.string.ci_adap_schedule_customer_care));
            itemsData.add(getBaseActivity().getString(R.string.ci_my_order));
            itemsData.add(getBaseActivity().getString(R.string.transport_managerment));
            itemsData.add(getBaseActivity().getString(R.string.order_tran_near_tran_title));
            itemsData.add(getBaseActivity().getString(R.string.order_tran_already_tran_title));
            itemsData.add(getBaseActivity().getString(R.string.manager_debt_title));
            CheckInAdapter adapter = new CheckInAdapter(itemsData, context);
            checkInRecyclerView.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        registerBroadCast();
    }

    private BroadcastReceiver receiver;

    private void registerBroadCast() {
        if (context != null) {
            receiver = new ItemNumberChangeReceiver();
            IntentFilter filter = new IntentFilter("ITEM_NUMBER_CHANGE");
            context.registerReceiver(receiver, filter);
        }
    }

    class ItemNumberChangeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            ArrayList<String> list = intent.getStringArrayListExtra("list");
            if (checkInRecyclerView != null) {
                CheckInAdapter adapter = (CheckInAdapter) checkInRecyclerView.getAdapter();
                if (adapter != null) {
                    adapter.setArrayItemNumber(list);
                }
            }
        }
    }


    private void initMoreView(View view) {
        fragmentContainer = view.findViewById(R.id.fragment_container);
        recyclerView = view.findViewById(R.id.fragment_demo_recycler_view);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        ArrayList<SubUserModel> itemsData = new ArrayList<>();
        itemsData.add(new SubUserModel(getString(R.string.ci_adap_customer_list), null));
        List<SubModel> subTongHopCong = new ArrayList<SubModel>() {
            {
                add(new SubModel("Tổng hợp công", TongHopCongActivity.class));
                add(new SubModel("Danh sách công tác", DanhSachCongTacActivity.class));
            }
        };

        itemsData.add(new SubUserModel(getString(R.string.timekeeping_more), subTongHopCong));
        itemsData.add(new SubUserModel(getString(R.string.regulation_more), null));
        List<SubModel> subLuong = new ArrayList<SubModel>() {
            {
                add(new SubModel("Quy chế lương", QuyCheLuongActivity.class));
                add(new SubModel("Tổng hợp lương", TongHopLuongActivity.class));
            }
        };
        itemsData.add(new SubUserModel(getString(R.string.salary_policy), subLuong));
        itemsData.add(new SubUserModel("Ưu đãi", null));
        itemsData.add(new SubUserModel("Xin nghỉ phép", null));
        itemsData.add(new SubUserModel("KPI hiện tại", null));
        itemsData.add(new SubUserModel("Phiên bản " + BuildConfig.VERSION_NAME, null));
        GeneralAdapter adapter = new GeneralAdapter(itemsData, getContext());
        recyclerView.setAdapter(adapter);
    }


    /**
     * Refresh
     */
    public void refresh() {
        if (getArguments().getInt("index", 0) > 0 && recyclerView != null) {
            recyclerView.smoothScrollToPosition(0);
        }
    }

    /**
     * Called when a fragment will be displayed
     */
    public void willBeDisplayed() {
        // Do what you want here, for example animate the content
        if (fragmentContainer != null) {
            Animation fadeIn = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
            fragmentContainer.startAnimation(fadeIn);
        }
    }

    /**
     * Called when a fragment will be hidden
     */
    public void willBeHidden() {
        if (fragmentContainer != null) {
            Animation fadeOut = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out);
            fragmentContainer.startAnimation(fadeOut);
        }
    }

    @Override
    protected void setUp(View view) {
        view.setOnClickListener((View v) -> {

        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (receiver != null) {
            context.unregisterReceiver(receiver);
        }
    }
}
