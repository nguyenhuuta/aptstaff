package com.anphuocthai.staff.ui.delivery.myorder.detailorder.detailtab;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.di.component.ActivityComponent;
import com.anphuocthai.staff.ui.base.BaseFragment;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;
import com.anphuocthai.staff.ui.delivery.myorder.detailorder.DetailOrder;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class DebtTabFragment extends BaseFragment implements DebtTabMvpView, DebtTabAdapter.Callback, SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = DebtTabFragment.class.getSimpleName();
    @Inject
    DebtTabMvpPresenter<DebtTabMvpView> mPresenter;

    @Inject
    DebtTabAdapter mInventoryDeptAdapter;

    @Inject
    LinearLayoutManager mLayoutManager;


    private SwipeRefreshLayout mSwipeRefreshLayout;

    private List<OrderTran> orderTranArrayList = new ArrayList<>();

    private WebView webView;

    private View emptyView;

    private DetailOrder detailOrder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.customer_debt_layout, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            mPresenter.onAttach(this);
            mInventoryDeptAdapter.setCallback(this);
            mInventoryDeptAdapter.setContext(getActivity());
            mInventoryDeptAdapter.setmPresenter(mPresenter);
        }
        return view;
    }

    @Override
    protected void setUp(View view) {

        webView = view.findViewById(R.id.web_view_customer_debt);

        emptyView = view.findViewById(R.id.layout_empty_view);

        emptyView.setVisibility(View.VISIBLE);


        setupPullToRefresh(view);


        try {
            this.detailOrder = (DetailOrder) getArguments().getSerializable("detail_order");
            displayTabOrderDetail(this.detailOrder);
        } catch (Exception e) {
            showMessage(e.toString());
        }


    }

    public static DebtTabFragment newInstance(DetailOrder detailOrder) {
        Bundle args = new Bundle();
        args.putSerializable("detail_order", detailOrder);
        DebtTabFragment fragment = new DebtTabFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void updateOrderTran(List<OrderTran> orderTranArrayList) {
    }

    @Override
    public void onGetCustomerDebtSuccess(String report) {
        mSwipeRefreshLayout.setRefreshing(false);
        if (report != null) {
            webView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
            webView.loadData(report, "text/html", null);
        } else {
            webView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        }

    }


    public void displayTabOrderDetail(DetailOrder detailOrder) {
        mSwipeRefreshLayout.setRefreshing(false);
        String html = detailOrder.getBody();
        if (html != null) {
            webView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
            webView.loadDataWithBaseURL(null, html, "text/html; charset=UTF-8", null, null);
        } else {
            webView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void setRefresh(boolean isRefresh) {
        if (mSwipeRefreshLayout != null) {
            if (isRefresh) {
                mSwipeRefreshLayout.setRefreshing(true);
            } else {
                mSwipeRefreshLayout.setRefreshing(false);
            }
        }
    }

    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.setRefreshing(false);
        // mPresenter.onViewPrepared();
        // mPresenter.getCustomerDebt();
    }

    private void setupPullToRefresh(View view) {
        // SwipeRefreshLayout
        mSwipeRefreshLayout = view.findViewById(R.id.swipe_container_customer_debt);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);
        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
        mSwipeRefreshLayout.post(() -> {
            if (orderTranArrayList.size() <= 0) {

            } else {
                updateOrderTran(orderTranArrayList);
            }
        });
    }
}
