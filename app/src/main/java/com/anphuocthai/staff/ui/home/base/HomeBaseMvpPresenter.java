package com.anphuocthai.staff.ui.home.base;

import com.anphuocthai.staff.ui.base.MvpPresenter;

public interface HomeBaseMvpPresenter<V extends HomeBaseMvpView> extends MvpPresenter<V> {
}
