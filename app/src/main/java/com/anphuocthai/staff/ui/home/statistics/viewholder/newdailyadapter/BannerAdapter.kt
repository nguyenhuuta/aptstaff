package com.anphuocthai.staff.ui.home.statistics.viewholder.newdailyadapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import com.anphuocthai.staff.R
import com.anphuocthai.staff.activities.webview.WebViewActivity
import com.anphuocthai.staff.activities.webview.WebViewType
import com.anphuocthai.staff.model.BannerModel
import com.anphuocthai.staff.utils.Utils


/**
 * Created by OpenYourEyes on 7/30/20
 */
class BannerAdapter(private val mContext: Context) : PagerAdapter() {
    private val mList: MutableList<BannerModel> = ArrayList()
    fun updateList(list: List<BannerModel>?) {
        mList.clear()
        mList.addAll(list!!)
        notifyDataSetChanged()
    }

    override fun getCount(): Int {
        return mList.size
    }

    override fun isViewFromObject(view: View, o: Any): Boolean {
        return o === view
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val layout = LayoutInflater.from(mContext).inflate(R.layout.layout_image, container, false)
        val imageView = layout.findViewById<ImageView>(R.id.image)
        val bannerModel = mList[position]
        Utils.loadImage(imageView, bannerModel.url?.toInt() ?: 0)
        imageView.setOnClickListener {
            val intent = Intent(mContext, WebViewActivity::class.java)
            intent.putExtra(WebViewActivity.WEB_VIEW_TYPE, WebViewType.Banner)
            intent.putExtra(WebViewActivity.URL_ARG, bannerModel)
            mContext.startActivity(intent)
        }
        container.addView(layout)
        return layout
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

}
