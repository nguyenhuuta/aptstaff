package com.anphuocthai.staff.ui.home.order;

import com.anphuocthai.staff.data.db.model.OrderProduct;
import com.anphuocthai.staff.ui.base.MvpView;

import java.util.List;

public interface OrderMainMvpView extends MvpView {

    void getAllProductLocal(List<OrderProduct> orderProducts);

    void getAllProductLocalWhenOrder(List<OrderProduct> orderProducts);

}
