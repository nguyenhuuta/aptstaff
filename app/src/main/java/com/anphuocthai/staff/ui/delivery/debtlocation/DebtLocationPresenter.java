package com.anphuocthai.staff.ui.delivery.debtlocation;

import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.data.DataManager;
import com.anphuocthai.staff.ui.base.BasePresenter;
import com.anphuocthai.staff.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class DebtLocationPresenter<V extends DebtLocationMvpView> extends BasePresenter<V> implements DebtLocationMvpPresenter<V>{

    @Inject
    public DebtLocationPresenter(NetworkManager networkManager, DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(networkManager, dataManager, schedulerProvider, compositeDisposable);
    }
}
