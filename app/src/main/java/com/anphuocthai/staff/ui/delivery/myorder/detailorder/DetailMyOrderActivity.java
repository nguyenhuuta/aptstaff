package com.anphuocthai.staff.ui.delivery.myorder.detailorder;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.viewpager.widget.ViewPager;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.ui.base.BaseActivity;
import com.anphuocthai.staff.ui.base.ViewPagerAdapter;
import com.anphuocthai.staff.ui.delivery.myorder.detailorder.detailtab.DebtTabFragment;

import java.util.ArrayList;

import javax.inject.Inject;

public class DetailMyOrderActivity extends BaseActivity implements DetailMyOrderMvpView {


    @Inject
    DetailMyOrderMvpPresenter< DetailMyOrderMvpView> mPresenter;

    private String sessionID;

    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_my_order);
        getmActivityComponent().inject(this);
        mPresenter.onAttach(this);
        handleIntent();

    }


    private void setupViewPager(ArrayList<DetailOrder> detailOrders) {
        viewPager = findViewById(R.id.detail_debt_view_pager);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        for (int i = 0; i < detailOrders.size(); i++) {
            adapter.addFragment(DebtTabFragment.newInstance(detailOrders.get(i)), detailOrders.get(i).getTitle());
        }
        viewPager.setAdapter(adapter);
    }


    private void handleIntent(){
        Bundle b = getIntent().getBundleExtra("session");
        sessionID = b.getString("sessionID");
        mPresenter.getDetailOrder(sessionID);

    }

    public static Intent getDetailMyOrderIntent(Context context) {
        Intent intent = new Intent(context, DetailMyOrderActivity.class);
        return intent;
    }

    @Override
    public void createAllTab(ArrayList<DetailOrder> detailOrders) {
        setupViewPager(detailOrders);
        setupToobarWithTab(getString(R.string.detail_order_title), viewPager, false);
    }
}
