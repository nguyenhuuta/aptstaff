package com.anphuocthai.staff.ui.home.statistics.viewholder;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.error.ANError;
import com.anphuocthai.staff.R;
import com.anphuocthai.staff.activities.detailtable.DetailTableActivity;
import com.anphuocthai.staff.api.ApiURL;
import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.entities.DynamicReportType;
import com.anphuocthai.staff.interfaces.IAPICallback;
import com.anphuocthai.staff.ui.base.BaseViewHolder;
import com.anphuocthai.staff.ui.home.statistics.StatisticAdapter;
import com.anphuocthai.staff.ui.home.statistics.model.DynamicReport;
import com.anphuocthai.staff.ui.home.statistics.model.updatedynamic.Item;
import com.anphuocthai.staff.ui.home.statistics.model.updatedynamic.PerformStatistic;
import com.anphuocthai.staff.utils.ColorUtils;
import com.anphuocthai.staff.utils.DateConvert;
import com.anphuocthai.staff.utils.Utils;
import com.anphuocthai.staff.widget.IRecycleViewCallback;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class TableViewHolder extends BaseViewHolder implements IRecycleViewCallback<Item> {

    private TextView mTitle;
    private RecyclerView mRecycleView;

    private CardView mButtonPrev, mButtonNext;
    private RelativeLayout loadingView;

    private TopCustomerTableAdapter mTopCustomerAdapter;
    private TopDynamicReportAdapter mTopDynamicReportAdapter;
    private Context mContext;

    private int mColor;
    private int mType;
    private Calendar mCalendar;

    private DynamicReport mDynamicReport;
    private String title;

    /**
     * for ViewType TopCustomer
     *
     * @param context
     * @param parent
     * @param type
     */
    public TableViewHolder(Context context, ViewGroup parent, int type) {
        super(LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_table, parent, false));
        this.mContext = context;
        this.mType = type;
        initView();
        initAction();
        initData();
    }

    /**
     * @param context
     * @param parent
     * @param type
     * @param dynamicReport
     */

    public TableViewHolder(Context context, ViewGroup parent, int type, DynamicReport dynamicReport) {
        super(LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_table, parent, false));
        this.mContext = context;
        this.mType = type;
        this.mDynamicReport = dynamicReport;
        initView();
        initAction();
        initData();
    }

    @Override
    public void onBind(int position) {
        super.onBind(position);
    }

    @Override
    protected void clear() {
    }


    private void initView() {
        mTitle = itemView.findViewById(R.id.layout_item_demo_title);
        mRecycleView = itemView.findViewById(R.id.table_recycler_view);
        mRecycleView.setHasFixedSize(true);
        mRecycleView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        mButtonPrev = itemView.findViewById(R.id.buttonPrev);
        mButtonNext = itemView.findViewById(R.id.buttonNext);
        loadingView = itemView.findViewById(R.id.loadingView);

    }

    private void initAction() {
        mButtonPrev.setOnClickListener(v -> onClickDate(-1));
        mButtonNext.setOnClickListener(v -> onClickDate(1));
    }

    /*
     * amount = -1
     * amount = 1
     * */
    private void onClickDate(int amount) {
        String time;
        if (mType == StatisticAdapter.VIEW_TOP_CUSTOMER) {
            mCalendar.add(Calendar.MONTH, amount);
            time = DateConvert.convertDate(mCalendar.getTimeInMillis(), DateConvert.TypeConvert.MONTH_YEAR);
            setTitleTopCustomer();
            getTopCustomer(time);
        } else {
            if (mDynamicReport == null) return;

            DateConvert.TypeConvert typeConvert = mDynamicReport.dateConvert;
            if (typeConvert == DateConvert.TypeConvert.ONLY_DATE) {
                mCalendar.add(Calendar.DAY_OF_MONTH, amount);
            } else if (typeConvert == DateConvert.TypeConvert.MONTH_YEAR) {
                mCalendar.add(Calendar.MONTH, amount);
            }
            time = DateConvert.convertDate(mCalendar.getTimeInMillis(), typeConvert);
            String fullTitle = title + " " + time;
            mTitle.setText(fullTitle);

            HashMap<String, String> query = new HashMap<>();
            query.put("type", String.valueOf(mDynamicReport.getType()));
            query.put("NgayThang", time);
            loadingView.setVisibility(View.VISIBLE);
            NetworkManager.getInstance().sendGetRequest(ApiURL.STATISTIC_REPORT_SELL, query, new IAPICallback() {
                @Override
                public void onResponseSuccess(JSONObject response) {
                    loadingView.setVisibility(View.GONE);
                }

                @Override
                public void onResponseSuccess(JSONArray response) {
                    loadingView.setVisibility(View.GONE);
                    if (response != null) {
                        Gson gson = new Gson();
                        Type type = new TypeToken<ArrayList<DynamicReport>>() {
                        }.getType();
                        ArrayList<DynamicReport> dynamicReports = gson.fromJson(response.toString(), type);
                        if (dynamicReports != null && dynamicReports.size() > 0) {
                            mTopDynamicReportAdapter.addItems(dynamicReports.get(0).getItems());
                        }
                    }
                }

                @Override
                public void onResponseError(ANError anError) {
                    loadingView.setVisibility(View.GONE);
                }
            });


        }
        if (amount == 1) {
            String time1 = DateConvert.convertDate(mCalendar.getTimeInMillis(), DateConvert.TypeConvert.MONTH_YEAR);
            String time2 = DateConvert.convertDate(Calendar.getInstance().getTimeInMillis(), DateConvert.TypeConvert.MONTH_YEAR);
            if (time1.equals(time2)) {
                mButtonNext.setVisibility(View.GONE);
            }
        } else {
            mButtonNext.setVisibility(View.VISIBLE);
        }
    }


    private void initData() {
        mCalendar = Calendar.getInstance();
        if (mType == StatisticAdapter.VIEW_TOP_CUSTOMER) {
            if (mColor == 0) {
                mColor = ColorUtils.getRandomMaterialColor();
            }
            mTitle.setBackgroundColor(mColor);
            title = mContext.getString(R.string.top_customer);
            setTitleTopCustomer();
            mTopCustomerAdapter = new TopCustomerTableAdapter(mColor, this);
            mTopCustomerAdapter.setContext(mContext);
            mRecycleView.setAdapter(mTopCustomerAdapter);
            getTopCustomer("");
        } else {
            updateDynamicTable();
        }
    }

    private void setTitleTopCustomer() {
        String monthYear = DateConvert.convertDate(mCalendar.getTimeInMillis(), DateConvert.TypeConvert.MONTH_YEAR);
        String tile = String.format(title, monthYear);
        mTitle.setText(tile);
    }


    /*
     *
     * TOP_CUSTOMER
     *
     * */
    private int typeTopCustomer = 0;

    private void getTopCustomer(String time) {
        HashMap<String, String> hashMap = null;
        if (typeTopCustomer != 0) {
            hashMap = new HashMap<>();
            hashMap.put("type", String.valueOf(typeTopCustomer));
            hashMap.put("NgayThang", time);
        }
        loadingView.setVisibility(View.VISIBLE);
        NetworkManager.getInstance().sendGetRequestObjectResponse(ApiURL.STATISTIC_ORDER_BY_TOP_CUSTOMER_DYNAMIC, hashMap, new IAPICallback() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                loadingView.setVisibility(View.GONE);
                if (response != null) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<PerformStatistic>() {
                    }.getType();
                    PerformStatistic performStatistic = gson.fromJson(response.toString(), type);
                    if (performStatistic == null) return;
                    typeTopCustomer = performStatistic.getType();
                    updateTopCustomer(performStatistic);
                }
            }

            @Override
            public void onResponseSuccess(JSONArray response) {
                loadingView.setVisibility(View.GONE);
            }

            @Override
            public void onResponseError(ANError anError) {
                loadingView.setVisibility(View.GONE);
            }
        });
    }

    private void updateTopCustomer(PerformStatistic performStatistic) {
        if (performStatistic == null) return;
        if (!performStatistic.getHienThi()) {
            return;
        }
        List<Item> list = performStatistic.getItems();
        if (!Utils.isNull(list)) {
            mTopCustomerAdapter.addItems(list);
        }
    }


    /*
     * TOP_SELL_IN_DAY
     * TOP_SELL_IN_MONTH
     * GPS
     * */


    private void updateDynamicTable() {
        if (mDynamicReport != null) {
            if (mColor == 0) {
                mColor = ColorUtils.getRandomMaterialColor();
            }
            mTitle.setBackgroundColor(mColor);
            title = mDynamicReport.getTenBaoCao();
            String date = "";
            if (mDynamicReport.getTypeReport() == DynamicReportType.GPS) {
                mButtonPrev.setVisibility(View.GONE);
                mButtonNext.setVisibility(View.GONE);
            } else {
                date = DateConvert.convertDate(mCalendar.getTimeInMillis(), mDynamicReport.dateConvert);
            }
            mTitle.setText(title + " " + date);

            List<com.anphuocthai.staff.ui.home.statistics.model.Item> list = mDynamicReport.getItems();
            mTopDynamicReportAdapter = new TopDynamicReportAdapter(mContext, mColor, list, (position, item) -> {
                gotoDetailTable(item.getFullname());
            });
            mRecycleView.setAdapter(mTopDynamicReportAdapter);
        } else {
            mTitle.setText("Chưa có dữ liệu");
        }

    }

    private void gotoDetailTable(String customName) {
        Intent intent = new Intent(mContext, DetailTableActivity.class);
        intent.putExtra(DetailTableActivity.ARG_NAME, customName);
        mContext.startActivity(intent);
    }

    @Override
    public void onItemClick(int position, Item item) {

    }
}
