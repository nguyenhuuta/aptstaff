package com.anphuocthai.staff.ui.home.statistics;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Build;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.activities.map.MapsActivity;
import com.anphuocthai.staff.ui.base.BaseViewHolder;
import com.anphuocthai.staff.ui.home.statistics.model.DynamicReport;
import com.anphuocthai.staff.ui.home.statistics.viewholder.BannerViewHolder;
import com.anphuocthai.staff.ui.home.statistics.viewholder.DailyCVViewHolder;
import com.anphuocthai.staff.ui.home.statistics.viewholder.ProductNewViewHolder;
import com.anphuocthai.staff.ui.home.statistics.viewholder.ProductSaleViewHolder;
import com.anphuocthai.staff.ui.home.statistics.viewholder.TableViewHolder;
import com.anphuocthai.staff.ui.home.statistics.viewholder.TopSellViewHolder;
import com.anphuocthai.staff.ui.home.statistics.viewholder.generalstatistic.GeneralStatisticViewHolder;
import com.anphuocthai.staff.utils.ColorUtils;
import com.anphuocthai.staff.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class StatisticAdapter extends RecyclerView.Adapter<BaseViewHolder> {
    private static final int VIEW_TYPE_BANNER = 0;

    private static final int VIEW_TYPE_GENERAL = 1;

    private static final int VIEW_TYPE_SHORT_CUT = 2;

    private static final int VIEW_TYPE_TOP_SELL = 3;

    private static final int VIEW_TYPE_PRODUCT_NEW = 4;

    private static final int VIEW_TYPE_PRODUCT_SALE = 5;

    private static final int VIEW_DAILY_CHART = 6;

    public static final int VIEW_TOP_CUSTOMER = 7;

    public static final int FIX_NUMBER = VIEW_TOP_CUSTOMER + 1;

    private StatisticMvpPresenter<StatisticMvpView> mPresenter;

    private Context context;

    private ArrayList<DynamicReport> dynamicReports;


    public StatisticAdapter(Context context) {
        this.context = context;
        dynamicReports = new ArrayList<>();
    }

    public void setDynamicReports(ArrayList<DynamicReport> dynamicReports) {
        if (dynamicReports != null) {
            this.dynamicReports.clear();
            this.dynamicReports.addAll(dynamicReports);
            notifyDataSetChanged();
        }
    }

    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_BANNER:
                return new BannerViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_banner, parent, false));
            case VIEW_TYPE_GENERAL:
                return new GeneralStatisticViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.general_statistic_layout, parent, false), context);
            case VIEW_TYPE_SHORT_CUT:
                return new ShortCutViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.statistic_short_cut_layout, parent, false));
            case VIEW_TYPE_TOP_SELL:
                return new TopSellViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_top_sell, parent, false));
            case VIEW_TYPE_PRODUCT_NEW:
                return new ProductNewViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product_new, parent, false));
            case VIEW_TYPE_PRODUCT_SALE:
                return new ProductSaleViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product_sale, parent, false));
            case VIEW_DAILY_CHART:
                return new DailyCVViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_daily_cv, parent, false), this);
            case VIEW_TOP_CUSTOMER:
                return new TableViewHolder(context, parent, viewType);
            default:
                DynamicReport dynamicReport = null;
                int position = viewType - FIX_NUMBER;
                if (!Utils.isNull(dynamicReports) && position > -1) {
                    dynamicReport = dynamicReports.get(position);
                    dynamicReport.initData();
                }
                return new TableViewHolder(context, parent, viewType, dynamicReport);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemCount() {
        return FIX_NUMBER + dynamicReports.size();
    }


    @Override
    public int getItemViewType(int position) {
        return position;
//        switch (position) {
//            case VIEW_TYPE_BANNER:
//                return VIEW_TYPE_BANNER;
//            case VIEW_TYPE_GENERAL:
//                return VIEW_TYPE_GENERAL;
//            case VIEW_TYPE_SHORT_CUT:
//                return VIEW_TYPE_SHORT_CUT;
//            case VIEW_TYPE_TOP_SELL:
//                return VIEW_TYPE_TOP_SELL;
//            case VIEW_TYPE_PRODUCT_NEW:
//                return VIEW_TYPE_PRODUCT_NEW;
//            case VIEW_TYPE_PRODUCT_SALE:
//                return VIEW_TYPE_PRODUCT_SALE;
//            case VIEW_DAILY_CHART:
//                return VIEW_DAILY_CHART;
//            case VIEW_TOP_CUSTOMER:
//                return VIEW_TOP_CUSTOMER;
//            default:
//                System.out.println("getItemViewType " + position);
//                return VIEW_TABLE;
//        }
    }


    public class ShortCutViewHolder extends BaseViewHolder {

        private LinearLayout itemChamCong, itemOrder, itemDebt;
        private LinearLayout itemCustomer, itemTransport, itemWork;
        private CircleImageView ivChamCong;

        private CircleImageView txtMyOrderShortcut;

        private CircleImageView txtDebtManagement;

        private CircleImageView txtCustomerShortcut;

        private CircleImageView txtTransportShortcut;

        private CircleImageView mDailyWork;


        public ShortCutViewHolder(View itemView) {
            super(itemView);
            itemChamCong = itemView.findViewById(R.id.chamCong);
            itemOrder = itemView.findViewById(R.id.order);
            itemDebt = itemView.findViewById(R.id.debt);
            itemCustomer = itemView.findViewById(R.id.customer);
            itemTransport = itemView.findViewById(R.id.transport);
            itemWork = itemView.findViewById(R.id.work);


            ivChamCong = itemView.findViewById(R.id.imageChamCong);
            txtMyOrderShortcut = itemView.findViewById(R.id.my_order_short_cut);
            txtDebtManagement = itemView.findViewById(R.id.debt_management);
            txtCustomerShortcut = itemView.findViewById(R.id.customer_short_cut);
            txtTransportShortcut = itemView.findViewById(R.id.txt_transport_short_cut);
            mDailyWork = itemView.findViewById(R.id.dailyWork);
            changeBackground();
        }

        void changeSize(Pair<Integer, Integer> size, View... views) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(size.first, size.second);
            for (View view : views) {
                view.setLayoutParams(params);
            }
        }

        private void changeBackground() {
            int widthDevice = Utils.getWidthDevice((Activity) context);
            int widthItem = (int) ((widthDevice - Utils.convertDpToPixel(context, 12 + 6) * 2) * 1.0 / 5);
            Pair<Integer, Integer> pairItem = new Pair<>(widthItem, LinearLayout.LayoutParams.WRAP_CONTENT);
            changeSize(pairItem, itemChamCong, itemCustomer, itemDebt, itemOrder, itemTransport, itemWork);

            int size = widthItem - Utils.convertDpToPixel(context, 15);
            Pair<Integer, Integer> pairImage = new Pair<>(size, size);
            changeSize(pairImage, txtMyOrderShortcut, txtDebtManagement, txtCustomerShortcut, txtTransportShortcut, mDailyWork, ivChamCong);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                List<Integer> listColor = ColorUtils.getRandomColor(6);
                txtMyOrderShortcut.setBackgroundTintList(ColorStateList.valueOf(listColor.get(0)));
                txtDebtManagement.setBackgroundTintList(ColorStateList.valueOf(listColor.get(1)));
                txtCustomerShortcut.setBackgroundTintList(ColorStateList.valueOf(listColor.get(2)));
                txtTransportShortcut.setBackgroundTintList(ColorStateList.valueOf(listColor.get(3)));
                mDailyWork.setBackgroundTintList(ColorStateList.valueOf(listColor.get(4)));
                ivChamCong.setBackgroundTintList(ColorStateList.valueOf(listColor.get(5)));
            }

        }

        @Override
        protected void clear() {
        }

        @Override
        public void onBind(int position) {
            super.onBind(position);
            txtMyOrderShortcut.setOnClickListener((View v) -> {
                mPresenter.openMyOrderShortcut();
            });
            txtDebtManagement.setOnClickListener((View v) -> {
                mPresenter.openDebtManagement();
            });
            txtCustomerShortcut.setOnClickListener((View v) -> {
                mPresenter.openCustomerShortcut();
            });
            txtTransportShortcut.setOnClickListener((View v) -> {
                mPresenter.openTransportShortcut();
            });
            mDailyWork.setOnClickListener(view -> mPresenter.openDailyWork());
            ivChamCong.setOnClickListener(view -> {
                Intent intent = new Intent(context, MapsActivity.class);
                context.startActivity(intent);
            });
        }
    }

    public StatisticMvpPresenter<StatisticMvpView> getmPresenter() {
        return mPresenter;
    }

    public void setmPresenter(StatisticMvpPresenter<StatisticMvpView> mPresenter) {
        this.mPresenter = mPresenter;
    }


    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
