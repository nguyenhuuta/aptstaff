package com.anphuocthai.staff.ui.home;

import com.anphuocthai.staff.entities.DepartmentModel;
import com.anphuocthai.staff.ui.base.MvpPresenter;

import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.subjects.PublishSubject;

public interface MainMvpPresenter<V extends MainMvpView> extends MvpPresenter<V> {
    void getAllOrderProduct();

    void deleteAllOrderProduct();

    boolean getIsShowGrid();

    void setShowGrid(boolean isShowGrid);

    void getAllStatistics();

    List<DepartmentModel> getListDepartment();

    PublishSubject<Boolean> triggerShowNotification();

    CompositeDisposable getDisposable();
}
