package com.anphuocthai.staff.ui.base;

import com.anphuocthai.staff.data.db.local.prefs.PreferencesHelper;
import com.anphuocthai.staff.data.db.network.ApiHelper;

public interface MvpInteractor {

    ApiHelper getApiHelper();

    PreferencesHelper getPreferencesHelper();

}
