package com.anphuocthai.staff.ui.delivery.myorder.adjust;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.interfaces.IYNDialogCallback;
import com.anphuocthai.staff.ui.base.BaseViewHolder;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;
import com.anphuocthai.staff.ui.delivery.myorder.MyOrderActivity;
import com.anphuocthai.staff.ui.ordercart.OrderCartActivity;
import com.anphuocthai.staff.utils.ui.UIUtils;

import java.io.Serializable;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AdjustOrderAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private static final String TAG = AdjustOrderAdapter.class.getSimpleName();

    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private Callback mCallback;

    private List<String> stringList;

    MyOrderActivity baseActivity;

    private OrderTran orderTran;

    AdjustOrderMvpPresenter<AdjustOrderMvpView> mPresenter;

    public void setmPresenter(AdjustOrderMvpPresenter<AdjustOrderMvpView> mPresenter) {
        this.mPresenter = mPresenter;
    }

    public void setOrderTran(OrderTran orderTran) {
        this.orderTran = orderTran;
    }

    public void setBaseActivity(MyOrderActivity baseActivity) {
        this.baseActivity = baseActivity;
    }

    public AdjustOrderAdapter(List<String> stringList) {
        this.stringList = stringList;
    }

    public void setCallback(Callback callback) {
        mCallback = callback;
    }


    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new ViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_adjust_order_dialog, parent, false));
            case VIEW_TYPE_EMPTY:
            default:
                return new EmptyViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_view, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (stringList != null && stringList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }


    }

    @Override
    public int getItemCount() {
        if (stringList != null && stringList.size() > 0) {
            return stringList.size();
        } else {
            return 1;
        }

    }

    public void addItems(List<String> strings) {
        stringList.clear();
        stringList.addAll(strings);
        notifyDataSetChanged();
    }


    public interface Callback {
        void onBlogEmptyViewRetryClick();
    }

    public class ViewHolder extends BaseViewHolder {

        private TextView txtContent;

        public ViewHolder(View itemView) {
            super(itemView);
            initView(itemView);
        }

        private void initView(View itemView) {

            txtContent = itemView.findViewById(R.id.txt_adjust_content);

        }

        protected void clear() {
        }

        public void onBind(int position) {
            super.onBind(position);
            txtContent.setText(stringList.get(position));
            itemView.setOnClickListener((View v) -> {
                mPresenter.dismissDialog();
                switch (position) {
                    case 0:
                        Intent intent = OrderCartActivity.getOrderCartActivityIntent(baseActivity);
                        Bundle bundle = new Bundle();
                        bundle.putBoolean("isEditOrder", true);
                        bundle.putSerializable("OrderProduct", (Serializable) orderTran);
                        intent.putExtra("OrderCartExtra", bundle);
                        baseActivity.startActivity(intent);
                        break;
                    case 1:

                        UIUtils.showYesNoDialog(baseActivity, "", baseActivity.getString(R.string.cancel_order), new IYNDialogCallback() {
                            @Override
                            public void accept() {
                                baseActivity.getmPresenter().cancelOrderTran(orderTran);
                            }
                            @Override
                            public void cancel() {}
                        });

                        break;
                }
            });

        }
    }


    public class EmptyViewHolder extends BaseViewHolder {

        @BindView(R.id.btn_retry)
        Button retryButton;

        @BindView(R.id.tv_message)
        TextView messageTextView;

        @BindView(R.id.txt_retry)
        TextView txtRetry;

        public EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            retryButton.setVisibility(View.GONE);
            txtRetry.setVisibility(View.GONE);
        }

        @Override
        protected void clear() {

        }

        @OnClick(R.id.btn_retry)
        void onRetryClick() {
            if (mCallback != null)
                mCallback.onBlogEmptyViewRetryClick();
        }
    }
}

