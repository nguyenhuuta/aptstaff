package com.anphuocthai.staff.ui.customer.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CustomerData implements Serializable {

    @SerializedName("nguoiDatDon")
    @Expose
    private String nguoiDatDon;
    @SerializedName("thanhToanCongNoId")
    @Expose
    private Integer thanhToanCongNoId;
    @SerializedName("thanhToanCongNo")
    @Expose
    private String thanhToanCongNo;

    public String getNguoiDatDon() {
        return nguoiDatDon;
    }

    public void setNguoiDatDon(String nguoiDatDon) {
        this.nguoiDatDon = nguoiDatDon;
    }

    public Integer getThanhToanCongNoId() {
        return thanhToanCongNoId;
    }

    public void setThanhToanCongNoId(Integer thanhToanCongNoId) {
        this.thanhToanCongNoId = thanhToanCongNoId;
    }

    public String getThanhToanCongNo() {
        return thanhToanCongNo;
    }

    public void setThanhToanCongNo(String thanhToanCongNo) {
        this.thanhToanCongNo = thanhToanCongNo;
    }
}
