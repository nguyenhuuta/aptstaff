package com.anphuocthai.staff.ui.ordercart.payconfig;

import com.anphuocthai.staff.ui.base.MvpPresenter;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;

public interface OrderConfigMvpPresenter<V extends OrderConfigMvpView> extends MvpPresenter<V> {
   void onStopCollectDebt(OrderTran orderTran, int realReceive);
   void getCustomerByGuid(String guid);

   boolean isHaveCustomerSave();
   int getCustomerPayType();
}
