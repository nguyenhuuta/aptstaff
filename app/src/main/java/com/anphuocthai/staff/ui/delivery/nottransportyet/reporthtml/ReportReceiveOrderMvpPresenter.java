package com.anphuocthai.staff.ui.delivery.nottransportyet.reporthtml;

import com.anphuocthai.staff.ui.base.MvpPresenter;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;

import org.json.JSONObject;

import java.util.List;

import io.reactivex.Flowable;

public interface ReportReceiveOrderMvpPresenter<V extends ReportReceiveOrderMvpView> extends MvpPresenter<V> {
    void onViewPrepared();
    Flowable<List<OrderTran>> getAllDebtApiCall(JSONObject object);
    void onSetStartCollectDebt(OrderTran orderTran);


    void getCustomerDebt();
}
