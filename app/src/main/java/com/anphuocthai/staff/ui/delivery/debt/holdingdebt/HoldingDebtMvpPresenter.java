package com.anphuocthai.staff.ui.delivery.debt.holdingdebt;

import com.anphuocthai.staff.model.CongNoModel;
import com.anphuocthai.staff.ui.base.MvpPresenter;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;

import org.json.JSONObject;

import java.util.List;

import io.reactivex.Flowable;

public interface HoldingDebtMvpPresenter<V extends HoldingDebtMvpView> extends MvpPresenter<V> {
    void onViewPrepared(int page);

    Flowable<List<OrderTran>> getAllDebtApiCall(JSONObject object);

    void daThuTienn(CongNoModel congNoModel, int sotien);

    void onReturnOrder(CongNoModel congNoModel);

    void chuyenDaMuonSangChoTra(String ids);
}
