package com.anphuocthai.staff.ui.home.order;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.view.GestureDetectorCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.data.db.model.OrderProduct;
import com.anphuocthai.staff.interfaces.ItemClickListener;
import com.anphuocthai.staff.model.order.Product;
import com.anphuocthai.staff.ui.home.MainActivity;
import com.anphuocthai.staff.ui.product.DetailProductActivity;
import com.anphuocthai.staff.utils.Enum;
import com.anphuocthai.staff.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static com.anphuocthai.staff.api.ApiURL.IMAGE_VIEW_URL;
import static com.anphuocthai.staff.utils.Enum.FieldValueType.CURRENCY;


public class ProductOrderAdapter extends RecyclerView.Adapter<ProductOrderAdapter.ItemProductViewHolder> {

    private ArrayList<Product> arrayList;
    private Context context;
    private boolean isShowGrid;
    private IProductOrderCallback mCallback;
    private int mImageSize;

    ProductOrderAdapter(ArrayList<Product> arrayList, Context context, boolean isShowGrid, int size, IProductOrderCallback callback) {
        this.setHasStableIds(true);
        this.arrayList = arrayList;
        this.context = context;
        this.isShowGrid = isShowGrid;
        this.mCallback = callback;
        this.mImageSize = size;
    }

    @Override
    public ItemProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (isShowGrid) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product_layout, parent, false);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product_layout_horizontal, parent, false);
        }

        ItemProductViewHolder vh = new ItemProductViewHolder(view);
        vh.setItemClickListener((view1, position, isLongClick) -> {
            if (isLongClick) {
                Intent intent = new Intent(context, DetailProductActivity.class);
                Bundle bundle = new Bundle();
                Product product = arrayList.get(position);
                bundle.putInt("productid", product.getId());
                bundle.putInt("imageProduct", product.getThumbId());
                intent.putExtra("productExtra", bundle);
                context.startActivity(intent);
            } else {
                try {
                    Product product = arrayList.get(position);
                    if (product.isSelected()) {
                        mCallback.onDeleteOrderProduct(product.toOrderProduct("", ""));
                    } else {
                        mCallback.onSaveOrderProduct(product.toOrderProduct("", ""));
                    }
                    product.setSelected(!product.isSelected());
                    vh.txtCheck.setVisibility(product.isSelected() ? View.VISIBLE : View.GONE);
                    Intent intent = new Intent();
                    intent.setAction(MainActivity.BADGE_CHANGE);
                    context.sendBroadcast(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        return vh;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(ItemProductViewHolder holder, int position) {
        Product product = arrayList.get(position);
        holder.txtProductName.setText(product.getName());
        if (!isShowGrid) {
            holder.txtPrice.setText(Utils.formatValue(product.getMainPrice().getKDBanBuonGia(), CURRENCY) + "/" +
                    Utils.formatValue(product.getMainPrice().getKDBanLeGia(), CURRENCY));
        } else {
            holder.txtPrice.setText(Utils.formatValue(product.getMainPrice().getKDBanBuonGia(), CURRENCY));
        }
        holder.txtCheck.setVisibility(product.isSelected() ? View.VISIBLE : View.GONE);

        Picasso.get()
                .load(IMAGE_VIEW_URL + product.getThumbId())
                .placeholder(R.drawable.ic_no_image)
                .fit()
                .centerCrop()
                .into(holder.imgProduct);

        if (isShowGrid) {
            holder.txtCV.setText(Utils.formatValue(product.getMainPrice().getKDCV(), Enum.FieldValueType.NORMAL));
        } else {
            holder.txtCV.setText(Utils.formatValue(product.getMainPrice().getKDCV(), Enum.FieldValueType.NORMAL) + " CV");
            holder.txtCVCustomer.setText(Utils.formatValue(product.getMainPrice().getKHCV(), Enum.FieldValueType.NORMAL) + " CV");
            holder.txtPriceRetail.setText(Utils.formatValue(product.getMainPrice().getKHBanLeGia(), CURRENCY));
        }
        holder.mNumberCoin.setText(String.valueOf(product.getMainPrice().getCoint()));

        if (product.isHetHang()) {
            holder.iconNew.setVisibility(View.VISIBLE);
            holder.iconNew.setText("Hết hàng");
        } else {
            if (!Utils.isNull(product.getWhatNew())) {
                holder.iconNew.setVisibility(View.VISIBLE);
                holder.iconNew.setText(product.getWhatNew());
                if (mCallback != null) {
                    mCallback.onUpdateItemToOld(product.getId());
                }
            } else {
                holder.iconNew.setVisibility(View.GONE);
            }
        }

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class ItemProductViewHolder extends RecyclerView.ViewHolder implements GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener {

        private TextView iconNew;
        private ImageView imgProduct;
        private TextView txtProductName;
        private TextView txtPrice;
        private TextView txtCV, mNumberCoin;
        private TextView txtCheck;

        private TextView txtPriceRetail;
        private TextView txtCVCustomer;
        private GestureDetectorCompat mDetector;
        private ItemClickListener itemClickListener;

        ItemProductViewHolder(View itemView) {
            super(itemView);
            iconNew = itemView.findViewById(R.id.iconNew);
            imgProduct = itemView.findViewById(R.id.order_img_product);
            if (isShowGrid && mImageSize > 0) {
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(mImageSize, mImageSize);
                params.addRule(RelativeLayout.CENTER_HORIZONTAL);
                params.setMargins(0, Utils.convertDpToPixel(itemView.getContext(), 10), 0, 0);
                imgProduct.setLayoutParams(params);
            }
            txtProductName = itemView.findViewById(R.id.order_txt_product_name);
            txtCV = itemView.findViewById(R.id.order_home_txt_cv);
            txtPrice = itemView.findViewById(R.id.order_home_txt_price);
            txtCheck = itemView.findViewById(R.id.order_txt_check);

            txtPriceRetail = itemView.findViewById(R.id.order_home_txt_price_retail);
            txtCVCustomer = itemView.findViewById(R.id.order_home_txt_cv_customer);
            mNumberCoin = itemView.findViewById(R.id.numberCoin);
            mDetector = new GestureDetectorCompat(context, this);
            mDetector.setOnDoubleTapListener(this);

            itemView.setOnTouchListener((view, motionEvent) -> {
                if (mDetector.onTouchEvent(motionEvent)) {
                    return true;
                }
                return mDetector.onTouchEvent(motionEvent);
            });
        }

        public void setItemClickListener(ItemClickListener itemClickListener) {
            this.itemClickListener = itemClickListener;
        }

        @Override
        public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
            int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                itemClickListener.onClick(null, position, false);
            }
            return true;
        }

        @Override
        public boolean onDoubleTap(MotionEvent motionEvent) {
            int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                itemClickListener.onClick(null, position, true);
            }
            return true;
        }

        @Override
        public boolean onDoubleTapEvent(MotionEvent motionEvent) {
            return false;
        }

        @Override
        public boolean onDown(MotionEvent motionEvent) {
            return false;
        }

        @Override
        public void onShowPress(MotionEvent motionEvent) {

        }

        @Override
        public boolean onSingleTapUp(MotionEvent motionEvent) {
            return false;
        }

        @Override
        public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
            return false;
        }

        @Override
        public void onLongPress(MotionEvent motionEvent) {

        }

        @Override
        public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
            return false;
        }


    }

    public void setShowGrid(boolean showGrid) {
        isShowGrid = showGrid;
    }

    public interface IProductOrderCallback {
        void onUpdateItemToOld(int productId);

        void onSaveOrderProduct(OrderProduct orderProduct);

        void onDeleteOrderProduct(OrderProduct orderProduct);
    }

}
