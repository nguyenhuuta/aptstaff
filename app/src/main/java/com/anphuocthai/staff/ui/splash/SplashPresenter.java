/*
 * Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://mindorks.com/license/apache-v2
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.anphuocthai.staff.ui.splash;


import com.androidnetworking.error.ANError;
import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.baserequest.APICallback;
import com.anphuocthai.staff.baserequest.APIService;
import com.anphuocthai.staff.data.DataManager;
import com.anphuocthai.staff.data.db.network.model.request.LoginRequest;
import com.anphuocthai.staff.entities.DepartmentModel;
import com.anphuocthai.staff.ui.base.BasePresenter;
import com.anphuocthai.staff.utils.App;
import com.anphuocthai.staff.utils.Logger;
import com.anphuocthai.staff.utils.rx.SchedulerProvider;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

import static com.anphuocthai.staff.utils.Constants.ERROR_CODE;
import static com.anphuocthai.staff.utils.Constants.NOT_FOUND_CODE;

/**
 * Created by janisharali on 27/01/17.
 */

public class SplashPresenter<V extends SplashMvpView> extends BasePresenter<V> implements SplashMvpPresenter<V> {

    @Inject
    public SplashPresenter(NetworkManager networkManager,
                           DataManager dataManager,
                           SchedulerProvider schedulerProvider,
                           CompositeDisposable compositeDisposable) {
        super(networkManager, dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onAttach(V mvpView) {
        super.onAttach(mvpView);

    }

    @Override
    public void checkLogin() {
        Logger.d("Splash", "autoLogin = " + getDataManager().getIsAutoLogin());
        if (getDataManager().getIsAutoLogin()) {
            autoLogin();
        } else {
            getmMvpView().openLoginActivity();
        }
    }

    private void autoLogin() {
        LoginRequest request = new LoginRequest(getDataManager().getUserName(), getDataManager().getPassword(), "ADMIN");
        getCompositeDisposable().add(getDataManager()
                .doServerLoginApiCall(request)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(loginResponse -> {
                    App.setUserInfo(loginResponse.getObjectInfo());
                    getDataManager().setUserInfo(loginResponse.getObjectInfo());
                    if (!isViewAttached()) {
                        return;
                    }
                    if (loginResponse.getCode().equals(NOT_FOUND_CODE)) {
                        getmMvpView().showDialogLoginFailure(loginResponse.getMessage());
                    } else if (loginResponse.getCode().equals(ERROR_CODE)) {
                        getmMvpView().showDialogLoginFailure(loginResponse.getMessage());
                    } else {
                        saveUserInfo(loginResponse.getObjectInfo().getUsername(),
                                request.getPassword(),
                                loginResponse.getObjectInfo().getToken(),
                                loginResponse.getObjectInfo().getId(),
                                loginResponse.getObjectInfo().getGuid());
                        getDepartment(loginResponse.getObjectInfo().getId());
                    }
                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }
                    getmMvpView().hideLoading();
                    // handle the login error here
                    if (throwable instanceof ANError) {
                        ANError anError = (ANError) throwable;
                        handleApiError(anError);
                        getmMvpView().showDialogLoginFailure(anError.getMessage());
                    }

                })
        );


    }

    private void saveUserInfo(String name, String pass, String token, int id, String guid) {
        getDataManager().setUserName(name);
        getDataManager().setPassword(pass);
        getDataManager().setToken(token);
        getDataManager().setUserInfoId(id);
        getDataManager().setUserGuid(guid);
    }

    private void getDepartment(int userId) {
        APIService.getInstance().getAppAPI()
                .getDepartment(userId)
                .getAsyncResponse(new APICallback<List<DepartmentModel>>() {
                    @Override
                    public void onSuccess(List<DepartmentModel> list) {

                        if (list != null && list.size() > 0) {
                            DepartmentModel firstModel = list.get(0);
                            if (firstModel != null && firstModel.getId() != null) {
                                App.getInstance().setDepartmentId(firstModel.getId());
                            }
                        } else {
                            list = new ArrayList<>();
                            DepartmentModel departmentModel = new DepartmentModel(0, "", "Tât cả", false, 0, "", 0, 0, 0);
                            list.add(departmentModel);
                        }
                        getDataManager().setDepartment(list);
                        getmMvpView().hideLoading();
                        getmMvpView().openMainActivity();

                    }

                    @Override
                    public void onFailure(String errorMessage) {
                        getmMvpView().hideLoading();
                        getmMvpView().openMainActivity();
                    }
                });
    }

}
