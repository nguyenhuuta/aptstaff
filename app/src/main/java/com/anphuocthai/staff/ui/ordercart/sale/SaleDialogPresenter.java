package com.anphuocthai.staff.ui.ordercart.sale;


import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.data.DataManager;
import com.anphuocthai.staff.ui.base.BasePresenter;
import com.anphuocthai.staff.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class SaleDialogPresenter<V extends SaleDialogMvpView> extends BasePresenter<V>
        implements SaleDialogMvpPresenter<V> {

    public static final String TAG = "SaleDialogPresenter";

    private boolean isRatingSecondaryActionShown = false;

    @Inject
    public SaleDialogPresenter(NetworkManager networkManager,
                               DataManager dataManager,
                               SchedulerProvider schedulerProvider,
                               CompositeDisposable compositeDisposable) {
        super(networkManager,dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onRatingSubmitted(final float rating, String message) {

        if (rating == 0) {
            //getmMvpView().showMessage(R.string.rating_not_provided_error);
            return;
        }

        if (!isRatingSecondaryActionShown) {
            if (rating == 5) {
                getmMvpView().showPlayStoreRatingView();
                getmMvpView().hideSubmitButton();
                getmMvpView().disableRatingStars();
            } else {
                getmMvpView().showRatingMessageView();
            }
            isRatingSecondaryActionShown = true;
            return;
        }

        getmMvpView().showLoading();

        //for demo
        getmMvpView().hideLoading();
        //getmMvpView().showMessage(R.string.rating_thanks);
        getmMvpView().dismissDialog();

    }

    private void sendRatingDataToServerInBackground(float rating) {

    }

    @Override
    public void onCancelClicked() {
        getmMvpView().dismissDialog();
    }

    @Override
    public void onLaterClicked() {
        getmMvpView().dismissDialog();
    }

    @Override
    public void onPlayStoreRatingClicked() {
        getmMvpView().openPlayStoreForRating();
        sendRatingDataToServerInBackground(5);
        getmMvpView().dismissDialog();
    }
}
