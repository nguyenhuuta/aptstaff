package com.anphuocthai.staff.ui.customersearch;

import com.androidnetworking.error.ANError;
import com.anphuocthai.staff.api.ApiURL;
import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.data.DataManager;
import com.anphuocthai.staff.fragments.customer.CustomerType;
import com.anphuocthai.staff.interfaces.IAPICallback;
import com.anphuocthai.staff.model.customer.SearchCustomer;
import com.anphuocthai.staff.ui.base.BasePresenter;
import com.anphuocthai.staff.utils.rx.SchedulerProvider;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.rx2androidnetworking.Rx2AndroidNetworking;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.anphuocthai.staff.utils.Constants.NUMBER_ROW_LOAD_MORE;

public class CustomerSearchPresenter<V extends CSearchMvpView> extends BasePresenter<V> implements CSearchMvpPresenter<V> {

    private static final String TAG = CustomerSearchPresenter.class.getSimpleName();

    @Inject
    public CustomerSearchPresenter(NetworkManager networkManager,
                                   DataManager dataManager,
                                   SchedulerProvider schedulerProvider,
                                   CompositeDisposable compositeDisposable) {
        super(networkManager, dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onGetRecentCustomer() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("tenDayDu", "");
            jsonObject.put("doiTuongId", "");
            jsonObject.put("pageNumber", 0);
            jsonObject.put("pageSize", 5);
            jsonObject.put("phanLoai", CustomerType.RECENT.getId());
        } catch (Exception e) {

        }
        getmNetworkManager().sendPostRequestWithArrayResponse(ApiURL.GET_CUSTOMER_BY_CATEGORY_URL, jsonObject, new IAPICallback() {
            @Override
            public void onResponseSuccess(JSONObject response) {
            }

            @Override
            public void onResponseSuccess(JSONArray response) {
                if (response != null) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<List<SearchCustomer>>() {
                    }.getType();
                    List<SearchCustomer> result = gson.fromJson(response.toString(), type);
                    getmMvpView().onShowRecentCustomer(result);
                }
            }

            @Override
            public void onResponseError(ANError anError) {

            }
        });
    }

    private JSONObject createJSONObject(final int page) {
        JSONObject object = new JSONObject();
        try {
            object.put("tenDayDu", "");
            object.put("doiTuongId", 0);
            object.put("pageNumber", page);
            object.put("pageSize", NUMBER_ROW_LOAD_MORE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return object;
    }

    @Override
    public void onCustomerSelected(SearchCustomer customer) {
        getmMvpView().onCustomerSelected(customer);
    }

    @Override
    public Flowable<List<SearchCustomer>> rxJavaOnGetAllCustomer(int page) {
        return Rx2AndroidNetworking.post(ApiURL.GET_CUSTOMER_BY_CATEGORY_URL)
                .addHeaders(ApiURL.getBaseHeader())
                .addHeaders(ApiURL.getTokenHeader())
                .addJSONObjectBody(createJSONObject(page))
                .build()
                .getObjectListFlowable(SearchCustomer.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
