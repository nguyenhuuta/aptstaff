package com.anphuocthai.staff.ui.delivery.debt.holdingdebt.receivemoney;

import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.data.DataManager;
import com.anphuocthai.staff.ui.base.BasePresenter;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;
import com.anphuocthai.staff.utils.App;
import com.anphuocthai.staff.utils.rx.SchedulerProvider;

import org.json.JSONObject;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class ReceiveMoneyPresenter<V extends ReceiveMoneyMvpView> extends BasePresenter<V> implements ReceiveMoneyMvpPresenter<V> {

    @Inject
    public ReceiveMoneyPresenter(NetworkManager networkManager, DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(networkManager, dataManager, schedulerProvider, compositeDisposable);
    }


    @Override
    public void onStopCollectDebt(OrderTran orderTran, int realReceive) {

        JSONObject setDebtObject = new JSONObject();
        try {
            setDebtObject.put("id", orderTran.getVanchuyens().get(0).getId());
            setDebtObject.put("sessionID", orderTran.getSessionID());
            setDebtObject.put("SoTien", realReceive);
            setDebtObject.put("BoPhanID", App.getInstance().getDepartmentId());
        } catch (Exception e) {
            e.printStackTrace();
        }

//        Rx2AndroidNetworking.post(ApiURL.STOP_DEBT_TRANSPORT_URL)
//                .addHeaders(ApiURL.getBaseHeader())
//                .addHeaders(ApiURL.getTokenHeader())
//                .addJSONObjectBody(setDebtObject)
//                .build()
//                .getObjectObservable(ResponseMessage.class)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Observer<ResponseMessage>() {
//                    @Override
//                    public void onSubscribe(Disposable d) {
//                    }
//
//                    @Override
//                    public void onNext(ResponseMessage responseMessage) {
//                        if (responseMessage.getCode().equals(SUCCESS_CODE)) {
//                            getmMvpView().showMessage(R.string.stop_debt_success);
//                            getmMvpView().onStopDebtSuccess("");
//                        } else {
//                            String message = responseMessage.getMessage();
//                            if (message == null || message.isEmpty()) {
//                                message = "Có lỗi xảy ra, vui lòng thử lại sau";
//                            }
//                            getmMvpView().onStopDebtSuccess(message);
//                        }
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        Logger.d("STOP_DEBT_TRANSPORT_URL", e.getLocalizedMessage());
//                        String message = e.getMessage();
//                        if (message == null || message.isEmpty()) {
//                            message = "Có lỗi xảy ra, vui lòng thử lại sau";
//                        }
//                        getmMvpView().onStopDebtSuccess(message);
//
//                    }
//
//                    @Override
//                    public void onComplete() {
//                        Logger.d("STOP_DEBT_TRANSPORT_URL", "onComplete");
//                    }
//                });
    }
}
