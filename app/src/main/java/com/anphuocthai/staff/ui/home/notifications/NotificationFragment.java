package com.anphuocthai.staff.ui.home.notifications;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.activities.detailpush.DetailPushActivity;
import com.anphuocthai.staff.base.IBaseCallback;
import com.anphuocthai.staff.di.component.ActivityComponent;
import com.anphuocthai.staff.ui.home.container.ContainerFragment;
import com.anphuocthai.staff.ui.home.notifications.model.Notification;
import com.anphuocthai.staff.utils.App;

import org.reactivestreams.Publisher;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.processors.PublishProcessor;
import io.reactivex.schedulers.Schedulers;

public class NotificationFragment extends ContainerFragment implements NotificationMvpView, SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = NotificationFragment.class.getSimpleName();

    private Context context;


    @Inject
    NotificationMvpPresenter<NotificationMvpView> mPresenter;

    WrapContentLinearLayoutManager layoutManager;

    private NotificationAdapter adapter;

    private SwipeRefreshLayout mSwipeRefreshLayout;

    private RecyclerView recyclerView;


    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private PublishProcessor<Integer> paginator = PublishProcessor.create();

    private boolean loading = false;
    private int pageNumber = 0;
    private int maxId = 0;
    private final int VISIBLE_THRESHOLD = 1;
    private int lastVisibleItem, totalItemCount;


    public class WrapContentLinearLayoutManager extends LinearLayoutManager {


        public WrapContentLinearLayoutManager(Context context, int orientation, boolean reverseLayout) {
            super(context, orientation, reverseLayout);
        }

        //... constructor
        @Override
        public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
            try {
                super.onLayoutChildren(recycler, state);
            } catch (IndexOutOfBoundsException e) {
                Log.e("probe", "meet a IOOBE in RecyclerView");
            }
        }
    }

    /**
     * Create a new instance of the fragment
     */
    public static NotificationFragment newInstance(int index, Context context) {
        NotificationFragment fragment = new NotificationFragment();
        Bundle b = new Bundle();
        b.putInt("index", index);
        fragment.setArguments(b);
        fragment.context = context;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view;
        ActivityComponent activityComponent = getActivityComponent();
        if (activityComponent != null) {
            activityComponent.inject(this);
            mPresenter.onAttach(this);
        }

        view = inflater.inflate(R.layout.notification_fragment_layout, container, false);
        initView(view);
        setupPullToRefresh(view);
        return view;
    }

    private void initView(View view) {
        fragmentContainer = view.findViewById(R.id.fragment_container);
        recyclerView = view.findViewById(R.id.notification_recycler_view);
        recyclerView.setHasFixedSize(true);
        layoutManager = new WrapContentLinearLayoutManager(getBaseActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    /**
     * Refresh
     */
    public void refresh() {
        if (getArguments().getInt("index", 0) > 0 && recyclerView != null) {
            recyclerView.smoothScrollToPosition(0);
        }
        resetLoadMoreData();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (adapter == null) {
            return;
        }
    }


    /**
     * Called when a fragment will be displayed
     */
    public void willBeDisplayed() {
        // Do what you want here, for example animate the content
        if (fragmentContainer != null) {
            Animation fadeIn = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
            fragmentContainer.startAnimation(fadeIn);
        }
    }

    /**
     * Called when a fragment will be hidden
     */
    public void willBeHidden() {
        if (fragmentContainer != null) {
            Animation fadeOut = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out);
            fragmentContainer.startAnimation(fadeOut);
        }
    }

    @Override
    protected void setUp(View view) {
        view.setOnClickListener((View v) -> {

        });
    }


    public void setupLoadMores() {
        setUpLoadMoreListener();
        subscribeForData();
    }

    public void setupLoadMoreDelay() {
        adapter = new NotificationAdapter(new IBaseCallback<Notification>() {
            @Override
            public void onCallback(Notification notification) {
                if (notification == null || context == null) return;
                Intent intent = new Intent(context, DetailPushActivity.class);
                intent.putExtra(DetailPushActivity.ARG_PUSH,notification);
                context.startActivity(intent);
            }
        });
        recyclerView.setAdapter(adapter);
        setupLoadMores();
    }

    private void resetLoadMoreData() {
        loading = false;
        pageNumber = 0;
        maxId = 0;
        lastVisibleItem = 0;
        totalItemCount = 0;
        if (adapter != null) {
            adapter.resetData(); // this line is reason crash
        }
        paginator.onNext(pageNumber);
        loading = true;
    }


    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.setRefreshing(true);
        resetLoadMoreData();

    }

    private void setupPullToRefresh(View view) {
        // SwipeRefreshLayout
        mSwipeRefreshLayout = view.findViewById(R.id.swipe_container_notification);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);
    }


    @Override
    public void onGetAllNotificationSuccess(ArrayList<Notification> notifications) {
        recyclerView.getRecycledViewPool().clear();
        adapter.addItems(notifications);

        if (notifications.size() > 0) {
            App.setLastNotification(notifications.get(notifications.size() - 1));
            maxId = notifications.get(notifications.size() - 1).getId();
        }
    }

    @Override
    public void onCloseOnRefreshIcon() {
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onUpdateNotificationNumberSuccess(String number) {
        try {
            Intent intent = new Intent();
            intent.putExtra("number", number);
            intent.setAction("NOTIFICATION_CHANGE");
            getBaseActivity().sendBroadcast(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    /**
     * setting listener to get callback for load more
     */
    private void setUpLoadMoreListener() {

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView,
                                   int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = layoutManager.getItemCount();
                lastVisibleItem = layoutManager
                        .findLastVisibleItemPosition();
                if (!loading
                        && totalItemCount <= (lastVisibleItem + VISIBLE_THRESHOLD)) {
                    pageNumber++;
                    paginator.onNext(pageNumber);
                    loading = true;
                }
            }
        });
    }


    /**
     * subscribing for data
     */
    private void subscribeForData() {
        Disposable disposable = paginator
                .onBackpressureDrop()
                .concatMap((Function<Integer, Publisher<List<Notification>>>) page -> {
                    loading = true;
                    return mPresenter.rxJavaOnGetAllNotification(maxId);
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(items -> {
                    recyclerView.getRecycledViewPool().clear();
                    adapter.addItems(items);
                    if (items.size() > 0) {
                        // for load more
                        maxId = items.get(items.size() - 1).getId();
                    }

                    mSwipeRefreshLayout.setRefreshing(false);
                    loading = false;
                    mPresenter.getUnReadNotification();
                }, throwable -> {

                });

        compositeDisposable.add(disposable);
        paginator.onNext(pageNumber);

    }

}
