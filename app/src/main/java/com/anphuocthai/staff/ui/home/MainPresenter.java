package com.anphuocthai.staff.ui.home;

import com.androidnetworking.error.ANError;
import com.anphuocthai.staff.api.ApiURL;
import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.data.DataManager;
import com.anphuocthai.staff.entities.DepartmentModel;
import com.anphuocthai.staff.interfaces.IAPICallback;
import com.anphuocthai.staff.model.Statistics;
import com.anphuocthai.staff.ui.base.BasePresenter;
import com.anphuocthai.staff.utils.rx.SchedulerProvider;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.subjects.PublishSubject;

public class MainPresenter<V extends MainMvpView> extends BasePresenter<V> implements MainMvpPresenter<V> {

    @Inject
    public MainPresenter(NetworkManager networkManager, DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(networkManager, dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void getAllOrderProduct() {
        getCompositeDisposable().add(getDataManager()
                .getAllOrderProducts()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(orderProducts -> getmMvpView().onUpdateShoppingCart(orderProducts), throwable -> {
                    System.out.println(throwable.getMessage());
                }));
    }

    @Override
    public CompositeDisposable getDisposable() {
        return getCompositeDisposable();
    }

    @Override
    public void deleteAllOrderProduct() {
        getCompositeDisposable().add(getDataManager()
                .deleteAllOrderProduct()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<Boolean>() {
                    @Override
                    public void accept(Boolean aBoolean) throws Exception {
                        getDataManager().setCurrentCustomerGuid(null);
                        getDataManager().setCurrentCustomerName(null);
                        getmMvpView().onDeleteAllComplete();
                        getAllOrderProduct();
                    }
                }, throwable -> {

                }));
    }

    @Override
    public boolean getIsShowGrid() {
        return getDataManager().getIsShowGrid();
    }

    @Override
    public void setShowGrid(boolean isShowGrid) {
        getDataManager().setIsShowGrid(isShowGrid);
    }

    @Override
    public void getAllStatistics() {
        getmNetworkManager().sendGetRequest(ApiURL.STATISTIC_URL, null, new IAPICallback() {
            @Override
            public void onResponseSuccess(JSONObject response) {

            }

            @Override
            public void onResponseSuccess(JSONArray response) {

                if (response != null) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<ArrayList<Statistics>>() {
                    }.getType();
                    ArrayList<Statistics> statistics = gson.fromJson(response.toString(), type);
                    getmMvpView().updateStatistics(statistics);
                }
            }

            @Override
            public void onResponseError(ANError anError) {
            }
        });
    }


    @Override
    public List<DepartmentModel> getListDepartment() {
        return getDataManager().getListDepartment();
    }

    @Override
    public PublishSubject<Boolean> triggerShowNotification() {
        return getDataManager().triggerShowNotification();
    }
}
