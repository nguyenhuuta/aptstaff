package com.anphuocthai.staff.ui.delivery.shipped.finishtransport;

import com.anphuocthai.staff.ui.base.DialogMvpView;

public interface FinishTransportMvpView extends DialogMvpView {

    void dismissDialog();
}
