
package com.anphuocthai.staff.ui.home.statistics.model.updatedynamic;

import com.anphuocthai.staff.model.SeeMore;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Item {

    @SerializedName("nhan")
    @Expose
    private String nhan;
    @SerializedName("nhan1")
    @Expose
    private String nhan1;
    @SerializedName("giaTri")
    @Expose
    private Long giaTri;
    @SerializedName("giaTri1")
    @Expose
    private Long giaTri1;
    @SerializedName("giaTri2")
    @Expose
    private Long giaTri2;

    public SeeMore seeMore = null;

    public String getNhan() {
        return nhan;
    }

    public void setNhan(String nhan) {
        this.nhan = nhan;
    }

    public String getNhan1() {
        return nhan1;
    }

    public void setNhan1(String nhan1) {
        this.nhan1 = nhan1;
    }

    public Long getGiaTri() {
        return giaTri;
    }

    public void setGiaTri(Long giaTri) {
        this.giaTri = giaTri;
    }

    public Long getGiaTri1() {
        return giaTri1;
    }

    public void setGiaTri1(Long giaTri1) {
        this.giaTri1 = giaTri1;
    }

    public Long getGiaTri2() {
        return giaTri2;
    }

    public void setGiaTri2(Long giaTri2) {
        this.giaTri2 = giaTri2;
    }

}
