package com.anphuocthai.staff.ui.home;

import android.Manifest;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.MenuItemCompat;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.adapters.common.MainViewPagerAdapter;
import com.anphuocthai.staff.data.db.model.OrderProduct;
import com.anphuocthai.staff.entities.DepartmentModel;
import com.anphuocthai.staff.model.Statistics;
import com.anphuocthai.staff.services.LocationService;
import com.anphuocthai.staff.ui.base.BaseActivity;
import com.anphuocthai.staff.ui.home.container.ContainerFragment;
import com.anphuocthai.staff.ui.home.notifications.NotificationFragment;
import com.anphuocthai.staff.ui.ordercart.OrderCartActivity;
import com.anphuocthai.staff.utils.App;
import com.anphuocthai.staff.utils.Logger;
import com.anphuocthai.staff.utils.Util;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationAdapter;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationViewPager;
import com.aurelhubert.ahbottomnavigation.notification.AHNotification;
import com.google.android.material.appbar.AppBarLayout;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

import static com.anphuocthai.staff.activities.map.MapsActivity.MY_PERMISSIONS_REQUEST_LOCATION;

public class MainActivity extends BaseActivity implements MainMvpView {
    /**
     * all variables
     */
    private static final String TAG = MainActivity.class.getSimpleName();
    private ArrayList<AHBottomNavigationItem> bottomNavigationItems = new ArrayList<>();

    public static final String BADGE_CHANGE = "BADGE_CHANGE";
    private int[] tabColors;
    private Handler handler = new Handler();
    private int mCartItemCount = 0;
    private boolean isShowGrid = true;
    private BadgeChangeReceiver badgeChangeReceiver;
    private NotificationNumberChangeReceiver notificationNumberChangeReceiver;
    private int currentTabPosition;
    public CompositeDisposable compositeDisposable = new CompositeDisposable();
    private boolean isFirstLoadNotification = false;
    /**
     * all UI
     */
    private AHBottomNavigationViewPager viewPager;
    private AHBottomNavigation bottomNavigation;
    private MainViewPagerAdapter adapter;
    private AHBottomNavigationAdapter navigationAdapter;
    private ContainerFragment currentFragment;
    private AppBarLayout appBarLayoutMain;
    private Toolbar toolbar;

    private TextView textCartItemCount;

    private List<DepartmentModel> departmentModels;

    @Inject
    MainMvpPresenter<MainMvpView> mPresenter;

    public static Intent getStartIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        changeColorStatusBar();
        registerBroadCast();
        setTheme(R.style.HideActionBar);
        setContentView(R.layout.activity_main);
        getmActivityComponent().inject(this);
        mPresenter.onAttach(this);

        setupMainToolbar();
        initUI();
        isShowGrid = mPresenter.getIsShowGrid();
        departmentModels = mPresenter.getListDepartment();
//        initLocationService();
        requestPermissionLocation();


        Disposable disposable = mPresenter.triggerShowNotification()
                .subscribe(a -> {
                    showNotification();
                }, throwable -> {

                });
        mPresenter.getDisposable().add(disposable);


        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        Log.w(TAG, "Fetching FCM registration token failed", task.getException());
                        return;
                    }
                    // Get new FCM registration token
                    String token = task.getResult();

                    // Log and toast
                    Log.d(TAG, "token " + token);
                });
    }

    LocationService mLocationService;
    Intent mServiceIntent;

    private void initLocationService() {
        if (!Util.INSTANCE.isLocationEnabledOrNot(this)) {
            Log.d(TAG, "LocationService is running");
            showDialog("Định vị đang bị tắt, vui lòng bật lên và thử lại", null);
            return;
        }
        requestPermissionLocation();
    }


    private void showNotification() {
        Intent intent = new Intent(this, NotiService.class);
        intent.setAction(NotiService.ACTION_START_FOREGROUND_SERVICE);
        startService(intent);
    }


    private void startLocation() {
//        mLocationService = new LocationService();
//        mServiceIntent = new Intent(this, LocationService.class);
//        if (!Util.INSTANCE.isMyServiceRunning(LocationService.class, this)) {
//            startService(mServiceIntent);
//            Log.d(TAG, "running service success");
//        } else {
//            Log.d(TAG, "service is running");
//        }
    }

    private void requestPermissionLocation() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                checkLocationPermission();
                return;
            }
        }
        startLocation();
    }

    private void checkLocationPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_LOCATION);
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION:
                if (grantResults.length > 0 &&
                        grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        startLocation();
                    }
                } else {
                    Logger.d(TAG, "MY_PERMISSIONS_REQUEST_LOCATION denied");
                }
        }
    }


    public void changeColorStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(badgeChangeReceiver);
        unregisterReceiver(notificationNumberChangeReceiver);
        handler.removeCallbacksAndMessages(null);
        compositeDisposable.dispose();
        if (mServiceIntent != null) {
            stopService(mServiceIntent);
        }
    }


    class BadgeChangeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null && intent.getExtras() != null) {
                boolean isRemoveAll = intent.getExtras().getBoolean(OrderCartActivity.ARG_CLEAR_ALL, false);
                if (currentFragment != null && currentFragment.getOrderMainFragment() != null) {
                    if (isRemoveAll) {
                        currentFragment.getOrderMainFragment().clearAllCheckProduct();
                    } else {
                        currentFragment.getOrderMainFragment().updateCheckProductWhenOrder();
                    }
                }

            }
            mPresenter.getAllOrderProduct();
        }
    }

    class NotificationNumberChangeReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            setUpNotificationNumber(intent.getStringExtra("number"));
        }

    }

    private void registerBroadCast() {
        badgeChangeReceiver = new BadgeChangeReceiver();
        final IntentFilter filter = new IntentFilter(BADGE_CHANGE);
        registerReceiver(badgeChangeReceiver, filter);


        notificationNumberChangeReceiver = new NotificationNumberChangeReceiver();
        final IntentFilter notificationFilter = new IntentFilter("NOTIFICATION_CHANGE");
        registerReceiver(notificationNumberChangeReceiver, notificationFilter);
    }

    /**
     *
     */
    private void setupMainToolbar() {
        appBarLayoutMain = findViewById(R.id.appBarLayoutMain);
        toolbar = findViewById(R.id.toolbarMain);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.app_name));
        toolbar.setNavigationIcon(R.drawable.ic_tool_icon);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        try {
            if ("OPEN_NOTIFICATION_TAB".equals(intent.getAction())) {
                Log.d(TAG, "OPEN_NOTIFICATION_TAB");
                if (viewPager != null) {
                    viewPager.setCurrentItem(3);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "OPEN_NOTIFICATION_TAB_ERROR");
        }
    }

    /**
     * Init UI
     */
    private void initUI() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        }
        bottomNavigation = findViewById(R.id.bottom_navigation);
        viewPager = findViewById(R.id.view_pager);

        tabColors = getApplicationContext().getResources().getIntArray(R.array.tab_colors);
        navigationAdapter = new AHBottomNavigationAdapter(this, R.menu.bottom_navigation_menu_5);
        navigationAdapter.setupWithBottomNavigation(bottomNavigation, tabColors);
        bottomNavigation.setTranslucentNavigationEnabled(true);
        bottomNavigation.setTitleTextSize(getResources().getDimension(R.dimen.font_small), getResources().getDimension(R.dimen.font_small));
        bottomNavigation.setAccentColor(Color.parseColor("#059546"));
        bottomNavigation.setBehaviorTranslationEnabled(false); // prevent hide bottom bar
        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                if (position == 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        appBarLayoutMain.setElevation(0);
                        appBarLayoutMain.setStateListAnimator(null);
                    }
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        appBarLayoutMain.setElevation(20f);
                    }
                }

                currentTabPosition = position;
                if (currentFragment == null) {
                    currentFragment = adapter.getCurrentFragment();
                }

                if (wasSelected) {
                    currentFragment.refresh();
                    return true;
                }

                if (currentFragment != null) {
                    currentFragment.willBeHidden();
                }
                viewPager.setCurrentItem(position, false);
                if (position == 1) {
                    if (mCartItemCount <= 0) {
                        toolbar.getMenu().getItem(0).setVisible(false);
                    } else {
                        toolbar.getMenu().getItem(0).setVisible(true);
                    }
                    setIsShowGridIcon();
                    toolbar.getMenu().getItem(1).setVisible(true);
                    toolbar.getMenu().getItem(3).setVisible(true);

                } else {
                    toolbar.getMenu().getItem(0).setVisible(false);
                    toolbar.getMenu().getItem(1).setVisible(false);
                    toolbar.getMenu().getItem(3).setVisible(false);
                }


                if (currentFragment == null) {
                    return true;
                }
                currentFragment = adapter.getCurrentFragment();
                currentFragment.willBeDisplayed();

                if (position == 2) {
                    mPresenter.getAllStatistics();
                }

                if (position == 3) {
                    if (currentFragment instanceof NotificationFragment) {

                        if (!isFirstLoadNotification) {
                            ((NotificationFragment) currentFragment).setupLoadMoreDelay();

                        } else {
                            ((NotificationFragment) currentFragment).refresh();
                        }
                        isFirstLoadNotification = true;
                    }
                }

                if (position == 1) {
                    bottomNavigation.setNotification("", 1);

                }

                return true;
            }
        });

        viewPager.setOffscreenPageLimit(4);
        adapter = new MainViewPagerAdapter(getSupportFragmentManager(), this);
        viewPager.setAdapter(adapter);
        currentFragment = adapter.getCurrentFragment();
    }


    private void setUpNotificationNumber(String number) {

        if (Integer.parseInt(number) == 0) {
            number = "";
        }
        // Setting custom colors for notification
        AHNotification notification = new AHNotification.Builder()
                .setText(number)
                .setBackgroundColor(ContextCompat.getColor(MainActivity.this, R.color.color_tab_5))
                .setTextColor(ContextCompat.getColor(MainActivity.this, R.color.white))
                .build();

        bottomNavigation.setNotification(notification, 3);
    }

    private void setupBadge() {
        if (textCartItemCount != null) {
            if (mCartItemCount == 0) {
                if (textCartItemCount.getVisibility() != View.GONE) {
                    textCartItemCount.setVisibility(View.GONE);
                }
            } else {
                textCartItemCount.setText(String.valueOf(Math.min(mCartItemCount, 99)));
                if (textCartItemCount.getVisibility() != View.VISIBLE) {
                    textCartItemCount.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cart_order, menu);
        final MenuItem menuItem = menu.findItem(R.id.menu_shopping_cart);
        View actionView = (View) MenuItemCompat.getActionView(menuItem);
        textCartItemCount = (TextView) actionView.findViewById(R.id.txt_number_item);
        setupBadge();
        actionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menuItem);
            }
        });

        toolbar.getMenu().getItem(0).setVisible(false);
        toolbar.getMenu().getItem(1).setVisible(false);
        createMenuDepartment();
        return true;
    }


    private void createMenuDepartment() {
        if (departmentModels == null) return;
        MenuItem menuItemDepartment = toolbar.getMenu().findItem(R.id.menuDepartment);
        SubMenu subMenu = menuItemDepartment.getSubMenu();
        subMenu.setGroupCheckable(R.id.menuDepartment, true, true);
        for (DepartmentModel departmentModel : departmentModels) {
            subMenu.add(0, departmentModel.getId(), departmentModel.getId(), departmentModel.getTen());
        }

        menuItemClick = subMenu.getItem(0);
        menuItemClick.setChecked(true);
        menuItemClick.setCheckable(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.menu_shopping_cart:
                Intent intent = OrderCartActivity.getOrderCartActivityIntent(this);
                Bundle bundle = new Bundle();
                bundle.putBoolean("isEditOrder", false);
                intent.putExtra("OrderCartExtra", bundle);
                startActivity(intent);
                return true;
            case R.id.menu_grid_or_list:
                if (isShowGrid) {
                    isShowGrid = false;
                    toolbar.getMenu().getItem(1).setIcon(R.drawable.ic_list_menu);
                } else {
                    isShowGrid = true;
                    toolbar.getMenu().getItem(1).setIcon(R.drawable.ic_grid_menu);
                }
                mPresenter.setShowGrid(isShowGrid);
                currentFragment.getOrderMainFragment().setShowGrid(isShowGrid);
                return true;
            case R.id.menu_clear_all:
                showDialogConfirm("Huỷ chọn tất cả?", () -> mPresenter.deleteAllOrderProduct());
                return true;
        }
        if (departmentModels != null && menuItemClick != item) {
            for (DepartmentModel departmentModel : departmentModels) {
                if (departmentModel != null && departmentModel.getId() != null) {
                    if (id == departmentModel.getId()) {
                        App.getInstance().setDepartmentId(id);
                        item.setCheckable(true);
                        item.setChecked(true);
                        if (menuItemClick != null) {
                            menuItemClick.setChecked(false);
                            menuItemClick.setChecked(false);
                        }
                        currentFragment.getOrderMainFragment().changeIdDepartment(id);
                        menuItemClick = item;
                    }
                }
            }
        }

        return super.onOptionsItemSelected(item);
    }

    private void setIsShowGridIcon() {
        if (isShowGrid) {
            toolbar.getMenu().getItem(1).setIcon(R.drawable.ic_grid_menu);
        } else {
            toolbar.getMenu().getItem(1).setIcon(R.drawable.ic_list_menu);
        }
    }

    private MenuItem menuItemClick;


    @Override
    public void onUpdateShoppingCart(List<OrderProduct> orderProducts) {
        mCartItemCount = orderProducts.size();
        if (currentTabPosition == 1) {
            if (mCartItemCount <= 0) {
                toolbar.getMenu().getItem(0).setVisible(false);
            } else {
                toolbar.getMenu().getItem(0).setVisible(true);
            }
        }
        setupBadge();
    }

    @Override
    public void onDeleteAllComplete() {
        if (currentFragment != null) {
            currentFragment.getOrderMainFragment().clearAllCheckProduct();
        }
    }

    @Override
    public void updateStatistics(ArrayList<Statistics> statistics) {
        Intent intent = new Intent();
        intent.setAction("ITEM_NUMBER_CHANGE");

        ArrayList<String> itemNumber = new ArrayList<>();
        for (int i = 0; i < statistics.size(); i++) {
            if (statistics.get(i).getMa().equals("DON_HANG_CUA_TOI")) {
                itemNumber.add(statistics.get(i).getGiaTri().toString());
            }
        }
        for (int i = 0; i < statistics.size(); i++) {
            if (statistics.get(i).getMa().equals("DON_HANG_CHUA_VAN_CHUYEN")) {
                itemNumber.add(statistics.get(i).getGiaTri().toString());
            }
        }

        for (int i = 0; i < statistics.size(); i++) {
            if (statistics.get(i).getMa().equals("DON_HANG_CHO_VAN_CHUYEN")) {
                itemNumber.add(statistics.get(i).getGiaTri().toString());
            }
        }

        for (int i = 0; i < statistics.size(); i++) {
            if (statistics.get(i).getMa().equals("DON_HANG_DANG_VAN_CHUYEN")) {
                itemNumber.add(statistics.get(i).getGiaTri().toString());
            }
        }

        for (int i = 0; i < statistics.size(); i++) {
            if (statistics.get(i).getMa().equals("CONG_NO_CUA_TOI_TAT_CA")) {
                itemNumber.add(statistics.get(i).getGiaTri().toString());
            }
        }
        for (int i = 0; i < statistics.size(); i++) {
            if (statistics.get(i).getMa().equals("THONG_BAO_CHUA_DOC_CUA_TOI")) {
                setUpNotificationNumber(statistics.get(i).getGiaTri().toString());
            }
        }

        intent.putStringArrayListExtra("list", itemNumber);
        Bundle bundle = new Bundle();
        bundle.putSerializable("statistic_change", statistics);
        intent.putExtra("statistic_extra", bundle);
        sendBroadcast(intent);
    }


    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.getAllOrderProduct();
        mPresenter.getAllStatistics();
    }

    private boolean isFinishActivity;

    @Override
    public void onBackPressed() {

        ActivityManager mngr = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> taskList = mngr.getRunningTasks(10);
        if (taskList.get(0).numActivities == 1 &&
                taskList.get(0).topActivity.getClassName().equals(this.getClass().getName())) {
            if (!isFinishActivity) {
                isFinishActivity = true;
                Toast.makeText(this, "Nhấn lần nữa để thoát", Toast.LENGTH_LONG).show();
                new Handler().postDelayed(() -> isFinishActivity = false, 2000);
                return;
            }
            finish();
        } else {
            super.onBackPressed();
        }
    }
}
