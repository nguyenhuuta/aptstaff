package com.anphuocthai.staff.ui.delivery.debt.waitpaydebt;

import com.anphuocthai.staff.api.ApiURL;
import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.data.DataManager;
import com.anphuocthai.staff.model.CongNoParams;
import com.anphuocthai.staff.model.CongNoResponse;
import com.anphuocthai.staff.ui.base.BasePresenter;
import com.anphuocthai.staff.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class WaitPayDebtPresenter<V extends WaitPayDebtMvpView> extends BasePresenter<V> implements WaitPayDebtMvpPresenter<V> {

    @Inject
    public WaitPayDebtPresenter(NetworkManager networkManager, DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(networkManager, dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onViewPrepared(int page) {
        getmMvpView().showLoading();
        CongNoParams congNoParams = new CongNoParams(getDataManager().getUserInfoId(), page, CongNoParams.TypeChoTraVaThanhToan);
        Disposable disposable = getmNetworkManager().postRequest(ApiURL.danhSachCongNo, congNoParams, CongNoResponse.class)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(congNoResponse -> {
                    if (getmMvpView() == null) {
                        return;
                    }
                    getmMvpView().hideLoading();
                    if (congNoResponse != null && congNoResponse.getData() != null) {
                        getmMvpView().updateOrderTran(congNoResponse.getData());
                    }
                }, throwable -> {
                    if (getmMvpView() == null) {
                        return;
                    }
                    getmMvpView().hideLoading();
                    getmMvpView().onError(throwable.getLocalizedMessage());
                });
        getCompositeDisposable().add(disposable);
    }
}
