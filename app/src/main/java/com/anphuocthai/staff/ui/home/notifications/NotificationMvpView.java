package com.anphuocthai.staff.ui.home.notifications;

import com.anphuocthai.staff.ui.base.MvpView;
import com.anphuocthai.staff.ui.home.notifications.model.Notification;

import java.util.ArrayList;

public interface NotificationMvpView extends MvpView {
//    void onDailyCVStatisticSuccess(ArrayList<DailyCV> dailyCVS, DailyCVViewHolder viewHolder);
//
//    void onTopCustomerStatisticSuccess(ArrayList<TopCustomer> topCustomers, TableViewHolder viewHolder);
//
//    void onTopCustomerStatisticSuccess(ArrayList<TopCustomer> topCustomers, TopCustomerVHTable viewHolder);
//
//    void onGetDynamicReportSuccess(ArrayList<DynamicReport> dynamicReports);
//
//    void openMyOrderShortcut();
//    void openDebtManagement();
//    void openCustomerShortcut();
//
//    void openTransportShortcut();
//
//    void onGetDynamicReportError();

    void onGetAllNotificationSuccess(ArrayList<Notification> notifications);

    void onCloseOnRefreshIcon();

    void onUpdateNotificationNumberSuccess(String number);
}
