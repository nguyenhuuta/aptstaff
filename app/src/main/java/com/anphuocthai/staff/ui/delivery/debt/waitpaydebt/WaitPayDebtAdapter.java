package com.anphuocthai.staff.ui.delivery.debt.waitpaydebt;

import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.model.CongNoModel;
import com.anphuocthai.staff.ui.base.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.anphuocthai.staff.utils.Enum.PayMethod.CASH;
import static com.anphuocthai.staff.utils.Enum.PayMethod.DEBT;

public class WaitPayDebtAdapter extends RecyclerView.Adapter<BaseViewHolder> {
    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private List<CongNoModel> orderTranArrayList;

    public WaitPayDebtAdapter(ArrayList<CongNoModel> orderTranArrayList) {
        this.orderTranArrayList = orderTranArrayList;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new ViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_congno_chotra_thanhtoan, parent, false));
            case VIEW_TYPE_EMPTY:
            default:
                return new EmptyViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_view, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);

    }

    @Override
    public int getItemCount() {
        if (orderTranArrayList != null && orderTranArrayList.size() > 0) {
            return orderTranArrayList.size();
        } else {
            return 1;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (orderTranArrayList != null && orderTranArrayList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    public void addItems(List<CongNoModel> orderTrans) {
        orderTranArrayList.clear();
        orderTranArrayList.addAll(orderTrans);
        notifyDataSetChanged();
    }


    public class ViewHolder extends BaseViewHolder {
        TextView txtOrderCode;
        TextView txtCustomerName;
        TextView labelSumaryMoney;
        TextView txtSumaryMoney;
        TextView labelMustPay;
        TextView txtMustPay;
        TextView labelRemain;
        TextView txtRemain;

        TextView txtTimeRemain;
        TextView waitType;
        CountDownTimer countDownTimer;

        public ViewHolder(View itemView) {
            super(itemView);
            initView(itemView);
        }

        private void initView(View itemView) {
            txtOrderCode = itemView.findViewById(R.id.all_debt_txt_order_code);
            txtCustomerName = itemView.findViewById(R.id.all_debt_txt_order_customer_name);
            labelSumaryMoney = itemView.findViewById(R.id.debt_label_sumary_money);
            txtSumaryMoney = itemView.findViewById(R.id.debt_txt_sumary_money);
            labelMustPay = itemView.findViewById(R.id.debt_label_must_pay);
            txtMustPay = itemView.findViewById(R.id.debt_txt_must_pay);
            labelRemain = itemView.findViewById(R.id.debt_label_remain);
            txtRemain = itemView.findViewById(R.id.debt_txt_remain);


            txtTimeRemain = itemView.findViewById(R.id.txt_time_remain);
            waitType = itemView.findViewById(R.id.waitType);
        }

        protected void clear() {
        }

        public void onBind(int position) {
            super.onBind(position);
            CongNoModel orderTran = orderTranArrayList.get(position);
            waitType.setText("");
            if (orderTran.getThanhToanId() == CASH) {
                waitType.setText(R.string.wait_to_pay);
            }
            if (orderTran.getThanhToanId() == DEBT) {
                waitType.setText(R.string.return_order_dept);
            }
            txtOrderCode.setText(orderTran.orderCode());
            txtCustomerName.setText(orderTran.getContentDetail());

            txtSumaryMoney.setText(orderTran.getCongNoDonHangString());
            txtMustPay.setText(orderTran.getTongTienDaThuString());
            txtRemain.setText(orderTran.getSoTienConLaiString());


            txtTimeRemain.setText("");
            int timeRemain = orderTran.getTimeRemain();
            if (timeRemain == 0) {
                txtTimeRemain.setText(R.string.common_end_time);
            } else {
                tiktok(timeRemain);
            }
        }

        private void tiktok(int time) {
            if (countDownTimer != null) {
                countDownTimer.cancel();
            }
            countDownTimer = new CountDownTimer(time * 1000, 1000) { // adjust the milli seconds here
                public void onTick(long millisUntilFinished) {
                    txtTimeRemain.setText("Còn: " + String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millisUntilFinished),
                            TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                            TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
                }

                public void onFinish() {
                    txtTimeRemain.setText(R.string.common_end_time);
                }
            }.start();
        }
    }


    public class EmptyViewHolder extends BaseViewHolder {

        @BindView(R.id.btn_retry)
        Button retryButton;

        @BindView(R.id.tv_message)
        TextView messageTextView;

        @BindView(R.id.txt_retry)
        TextView txtRetry;

        public EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            retryButton.setVisibility(View.GONE);
            txtRetry.setVisibility(View.GONE);
        }

        @Override
        protected void clear() {

        }

        @OnClick(R.id.btn_retry)
        void onRetryClick() {
//            if (mCallback != null)
//                mCallback.onBlogEmptyViewRetryClick();
        }
    }
}
