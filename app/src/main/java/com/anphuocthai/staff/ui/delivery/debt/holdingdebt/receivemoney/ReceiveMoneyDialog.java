package com.anphuocthai.staff.ui.delivery.debt.holdingdebt.receivemoney;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentManager;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.di.component.ActivityComponent;
import com.anphuocthai.staff.model.CongNoModel;
import com.anphuocthai.staff.ui.base.BaseDialog;
import com.anphuocthai.staff.utils.Enum;
import com.anphuocthai.staff.utils.NumberTextWatcher;
import com.anphuocthai.staff.utils.Utils;

import javax.inject.Inject;

public class ReceiveMoneyDialog extends BaseDialog implements ReceiveMoneyMvpView {

    private static final String TAG = ReceiveMoneyDialog.class.getSimpleName();

    private TextView txtDialogTitle;

    private TextView txtMoneyMustPay;

    private Button btnCancel;

    private Button btnOK;

    private EditText edtRealReceive;

    private int currentPrice = 0;
    private double maxPrice = 0;

    private ConstraintLayout constraintLayout;

    ReceiveMoneyDialogCallback callback;
    @Inject
    ReceiveMoneyMvpPresenter<ReceiveMoneyMvpView> mPresenter;

    private CongNoModel orderTran;

    @Override
    protected void setUp(View view) {
    }

    public static ReceiveMoneyDialog newInstance() {
        ReceiveMoneyDialog fragment = new ReceiveMoneyDialog();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.receive_money_dialog_layout, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            txtDialogTitle = view.findViewById(R.id.receive_money_dialog_title);
            txtMoneyMustPay = view.findViewById(R.id.receive_money_txt_pay);
            btnCancel = view.findViewById(R.id.receive_btn_cancel);
            btnOK = view.findViewById(R.id.order_config_btn_ok);
            edtRealReceive = view.findViewById(R.id.receive_dialog_edt);
            constraintLayout = view.findViewById(R.id.receive_dialog_container);
            initEvents();
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    public void hideKeyboard() {
        super.hideKeyboard();
    }

    private void initEvents() {
        String maDonDat = getString(R.string.order_tran_order_code) + " " + orderTran.getMaDonDat();
        txtDialogTitle.setText(maDonDat);
        maxPrice = orderTran.getCongNoDonHang();
        String soTienConLai = "Số nợ: " + Utils.formatValue(maxPrice, Enum.FieldValueType.CURRENCY);
        txtMoneyMustPay.setText(soTienConLai);

        edtRealReceive.setHint("Nhập số tiền thu được");
        btnCancel.setOnClickListener((View v) -> {
            dismissDialog();
        });
        btnOK.setOnClickListener((View v) -> {
            if (edtRealReceive.getText().toString().isEmpty()) {
                showMessage(R.string.stop_debt_validate_real_receive);
            } else if (maxPrice < currentPrice) {
                showMessage(R.string.gia_tri_vuot_muc);
            } else if (currentPrice == 0) {
                showMessage(R.string.current_price_equal_0);
            } else {
                dismissDialog();
                callback.thuTien(orderTran, currentPrice);
            }
        });

        constraintLayout.setOnClickListener((View v) -> {
            InputMethodManager imm = (InputMethodManager) getBaseActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            edtRealReceive.clearFocus();
        });

        edtRealReceive.setOnFocusChangeListener((view, b) -> {
            if (!b) {
                try {
                    if (edtRealReceive.getText().toString().isEmpty()) {
                        edtRealReceive.setText(Utils.formatValue(orderTran.getCongNoDonHang(), Enum.FieldValueType.CURRENCY_NORMAL));
                    } else {
                        String numberString = edtRealReceive.getText().toString().replaceAll("[^\\d]", "");
                        edtRealReceive.setText(Utils.formatValue(Integer.parseInt(numberString), Enum.FieldValueType.CURRENCY_NORMAL));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        edtRealReceive.addTextChangedListener(new NumberTextWatcher(edtRealReceive));
        edtRealReceive.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    String myString = editable.toString().replaceAll("[^\\d]", "");
                    if (!myString.isEmpty()) {
                        currentPrice = Integer.parseInt(myString);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public void show(FragmentManager fragmentManager, CongNoModel orderTran, ReceiveMoneyDialogCallback callback) {
        super.show(fragmentManager, TAG);
        this.orderTran = orderTran;
        this.callback = callback;
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void dismissDialog() {
        super.dismissDialog(TAG);
    }

}
