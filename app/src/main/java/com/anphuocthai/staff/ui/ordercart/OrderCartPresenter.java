package com.anphuocthai.staff.ui.ordercart;

import android.util.Pair;

import com.androidnetworking.error.ANError;
import com.anphuocthai.staff.api.ApiURL;
import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.data.DataManager;
import com.anphuocthai.staff.data.TypeProduct;
import com.anphuocthai.staff.data.db.model.OrderProduct;
import com.anphuocthai.staff.interfaces.IAPICallback;
import com.anphuocthai.staff.model.ChietKhauModel;
import com.anphuocthai.staff.model.ChietKhauResponse;
import com.anphuocthai.staff.model.DanhMucDungThuResponse;
import com.anphuocthai.staff.model.ResponseMessage;
import com.anphuocthai.staff.model.customer.Customer;
import com.anphuocthai.staff.ui.base.BasePresenter;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;
import com.anphuocthai.staff.utils.App;
import com.anphuocthai.staff.utils.rx.SchedulerProvider;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class OrderCartPresenter<V extends OrderCartMvpView> extends BasePresenter<V> implements OrderCartMvpPresenter<V> {

    public static final String TAG = OrderCartPresenter.class.getSimpleName();

    @Inject
    public OrderCartPresenter(NetworkManager networkManager,
                              DataManager dataManager,
                              SchedulerProvider schedulerProvider,
                              CompositeDisposable compositeDisposable) {
        super(networkManager, dataManager, schedulerProvider, compositeDisposable);
    }


    private JSONObject createJSONObject(String guid) {
        JSONObject notTranObject = new JSONObject();
        try {
            notTranObject.put("phanLoai", 3);
            notTranObject.put("guidThanhVien", guid);
            notTranObject.put("sessionID", "");
            notTranObject.put("tuNgay", "");
            notTranObject.put("denNgay", "");
            notTranObject.put("trangThaiIds", "");
            notTranObject.put("pageIndex", 1);
            notTranObject.put("pageSize", 10);
            notTranObject.put("BoPhanID", App.getInstance().getDepartmentId());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return notTranObject;
    }


    @Override
    public void onGetAllOrderProduct() {
        getCompositeDisposable().add(getDataManager()
                .getAllOrderProducts()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(orderProducts -> {
                    getmMvpView().onUpdateOrderProducts(orderProducts);
                }, throwable -> {
                    if (!isViewAttached()) {
                        return;
                    }
                }));
    }

    @Override
    public void onUpdateOrderCart(OrderProduct orderProduct, boolean isInsert) {
        Observable<Boolean> queryDataBase = isInsert ? getDataManager().insertOrderProduct(orderProduct) : getDataManager().updateOrderProduct(orderProduct);
        getCompositeDisposable().add(queryDataBase
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(aBoolean -> {

                }, throwable -> {

                }));
    }

    @Override
    public void onDeleteOrderCart(OrderProduct orderProduct) {
        getCompositeDisposable().add(getDataManager()
                .deleteOrderProduct(orderProduct)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(aBoolean -> {
//                    onGetAllOrderProduct();
                }, throwable -> {

                }));
    }

    @Override
    public void onDeleteAllOrderCart() {
        getCompositeDisposable().add(getDataManager()
                .deleteAllOrderProduct()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(aBoolean -> {
                    // DELETE customer
                    getDataManager().setCurrentCustomerGuid(null);
                    getDataManager().setCurrentCustomerName(null);
                    onGetAllOrderProduct();
                }, throwable -> {

                }));
    }

    @Override
    public void onUpdateSumaryFeeAndCV(float sumfee, float sumCV, int tongChietKhau) {
        getmMvpView().onUpdateSumaryFeeAndCV(sumfee, sumCV, tongChietKhau);
    }

    @Override
    public void onStartOrder(JSONObject jsonObject) {
        getmMvpView().showLoading();
        getmNetworkManager().sendPostRequest(ApiURL.CREATE_ORDER_URL, jsonObject, new IAPICallback() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                onDeleteAllOrderCart();
                if (!isViewAttached()) {
                    return;
                }
                // delele preference

                getDataManager().setPayMethod(-1);
                getDataManager().setCurrentCustomerName(null);
                getDataManager().setCurrentCustomerGuid(null);

                getmMvpView().onCreateOrderSuccess();
                getmMvpView().hideLoading();

            }

            @Override
            public void onResponseSuccess(JSONArray response) {
            }

            @Override
            public void onResponseError(ANError anError) {
                getmMvpView().hideLoading();
            }
        });

    }

    @Override
    public void onOpenSaleDialog(OrderProduct orderProduct, OrderCartAdapter.ViewHolder viewHolder) {
        getmMvpView().onOpenSaleDialog(orderProduct, viewHolder);
    }

    @Override
    public void showMessage(int restId) {
        getmMvpView().showMessage(restId);
    }

    @Override
    public void openSearchActivity() {
        getmMvpView().openSearchActivity();
    }

    @Override
    public void cancelOrderTran(OrderTran orderTran) {

        JSONObject cancelOrder = new JSONObject();
        try {
            cancelOrder.put("sessionID", orderTran.getSessionID());
            cancelOrder.put("ghiChu", "Test");
        } catch (Exception e) {
            e.printStackTrace();
        }

        getmNetworkManager().sendPostRequest(ApiURL.DEL_ORDER_URL, cancelOrder, new IAPICallback() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                if (response != null) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<ResponseMessage>() {
                    }.getType();
                    ResponseMessage message = gson.fromJson(response.toString(), type);
                    getmMvpView().showMessage(message.getMessage());
                    getmMvpView().startOrder();
                }
            }

            @Override
            public void onResponseSuccess(JSONArray response) {

            }

            @Override
            public void onResponseError(ANError anError) {

            }
        });


    }

    @Override
    public void getAllOrderDeptCustomer(String guid) {
        getmNetworkManager().sendPostRequestWithArrayResponse(ApiURL.GET_ORDER_URL, createJSONObject(guid), new IAPICallback() {
            @Override
            public void onResponseSuccess(JSONObject response) {
            }

            @Override
            public void onResponseSuccess(JSONArray response) {
                if (response != null) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<ArrayList<OrderTran>>() {
                    }.getType();
                    ArrayList<OrderTran> orderTrans = gson.fromJson(response.toString(), type);
                    getmMvpView().onUpdateCustomerDebt(orderTrans);
                }
            }

            @Override
            public void onResponseError(ANError anError) {

            }
        });
    }

    @Override
    public void onShowOrderConfigDialog(Customer customer, OrderCartAdapter adapter) {
        getmMvpView().onShowOrderConfigDialog(customer, adapter);
    }

    @Override
    public void getCustomerByGuid(String guid) {

        getmMvpView().showLoading();

        HashMap hashMap = new HashMap();
        hashMap.put("guid", guid);

        getmNetworkManager().sendGetRequestObjectResponse(ApiURL.GET_CUSTOMER_BY_GUID_URL, hashMap, new IAPICallback() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                if (response != null) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<Customer>() {
                    }.getType();
                    Customer result = new Customer();
                    result = gson.fromJson(response.toString(), type);

                    if (!isViewAttached()) {
                        return;
                    }

                    getmMvpView().onGetCustomerByGuidSuccess(result);
                    getmMvpView().hideLoading();
                }
            }

            @Override
            public void onResponseSuccess(JSONArray response) {

            }

            @Override
            public void onResponseError(ANError anError) {
                handleApiError(anError);
                getmMvpView().hideLoading();
            }
        });
    }

    @Override
    public boolean isHaveCustomerSave() {
        if (getDataManager().getCurrentCustomerName().equals("") || getDataManager().getCurrentCustomerGuid().equals("")
                || getDataManager().getPayMethod() == -1) {
            return false;
        }
        return true;
    }

    @Override
    public String getGuidCustomerSave() {
        return getDataManager().getCurrentCustomerGuid();
    }


    @Override
    public void getListTypeProduct() {

        Observable<List<TypeProduct>> request1 = getmNetworkManager().postRequest(ApiURL.danhMucDungThu, null, DanhMucDungThuResponse.class)
                .subscribeOn(getSchedulerProvider().io()).observeOn(getSchedulerProvider().ui())
                .map(danhMucDungThuResponse -> {
                    if (danhMucDungThuResponse.isSuccess() && danhMucDungThuResponse.getData() != null) {
                        return danhMucDungThuResponse.getData();
                    }
                    return new ArrayList<>();
                });

        Observable<List<ChietKhauModel>> request2 = getmNetworkManager().postRequest(ApiURL.listChietKhau, new Object(), ChietKhauResponse.class)
                .subscribeOn(getSchedulerProvider().io()).observeOn(getSchedulerProvider().ui())
                .map(chietKhauResponse -> {
                    if (chietKhauResponse.isSuccess() && chietKhauResponse.getData() != null) {
                        return chietKhauResponse.getData();
                    }
                    return new ArrayList<>();
                });
        getmMvpView().showLoading();
        Disposable disposable = Observable.combineLatest(request1, request2, Pair::new)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(pair -> {
                    getmMvpView().hideLoading();
                    getmMvpView().showListTypeProduct(pair);
                }, throwable -> {
                    getmMvpView().hideLoading();
                    getmMvpView().showMessage(throwable.getMessage());
                });
        getCompositeDisposable().add(disposable);

    }

    @Override
    public int percenChietKhau() {
        try {
            return getDataManager().getUserInfo().getCauHinhModel().getChietKhauToiDa();
        } catch (Exception e) {

        }

        return 0;
    }
}
