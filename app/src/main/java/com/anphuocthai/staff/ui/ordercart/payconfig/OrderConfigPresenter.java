package com.anphuocthai.staff.ui.ordercart.payconfig;

import com.androidnetworking.error.ANError;
import com.anphuocthai.staff.R;
import com.anphuocthai.staff.api.ApiURL;
import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.data.DataManager;
import com.anphuocthai.staff.interfaces.IAPICallback;
import com.anphuocthai.staff.model.ResponseMessage;
import com.anphuocthai.staff.model.customer.Customer;
import com.anphuocthai.staff.ui.base.BasePresenter;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;
import com.anphuocthai.staff.utils.App;
import com.anphuocthai.staff.utils.rx.SchedulerProvider;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.rx2androidnetworking.Rx2AndroidNetworking;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.anphuocthai.staff.utils.Constants.ERROR_CODE;
import static com.anphuocthai.staff.utils.Constants.NOT_FOUND_CODE;
import static com.anphuocthai.staff.utils.Constants.SUCCESS_CODE;

public class OrderConfigPresenter<V extends OrderConfigMvpView> extends BasePresenter<V> implements OrderConfigMvpPresenter<V> {

    @Inject
    public OrderConfigPresenter(NetworkManager networkManager, DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(networkManager, dataManager, schedulerProvider, compositeDisposable);
    }


    @Override
    public void onStopCollectDebt(OrderTran orderTran, int realReceive) {

        JSONObject setDebtObject = new JSONObject();
        try {
            setDebtObject.put("id", orderTran.getVanchuyens().get(0).getId());
            setDebtObject.put("sessionID", orderTran.getSessionID());
            setDebtObject.put("SoTien", realReceive);
            setDebtObject.put("BoPhanID", App.getInstance().getDepartmentId());
        } catch (Exception e) {
            e.printStackTrace();
        }

        Rx2AndroidNetworking.post(ApiURL.STOP_DEBT_TRANSPORT_URL)
                .addHeaders(ApiURL.getBaseHeader())
                .addHeaders(ApiURL.getTokenHeader())
                .addJSONObjectBody(setDebtObject)
                .build()
                .getObjectObservable(ResponseMessage.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ResponseMessage>() {
                    @Override
                    public void onSubscribe(Disposable d) {}

                    @Override
                    public void onNext(ResponseMessage responseMessage) {
                        if (responseMessage.getCode().equals(SUCCESS_CODE)) {
                            getmMvpView().showMessage(R.string.stop_debt_success);
                            getmMvpView().onStopDebtSuccess();
                        } else if (responseMessage.getCode().equals(NOT_FOUND_CODE)) {

                        } else if (responseMessage.getCode().equals(ERROR_CODE)) {

                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void getCustomerByGuid(String guid) {
        HashMap hashMap = new HashMap();
        hashMap.put("guid", guid);

        getmNetworkManager().sendGetRequestObjectResponse(ApiURL.GET_CUSTOMER_BY_GUID_URL, hashMap, new IAPICallback() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                if (response != null) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<Customer>() {
                    }.getType();
                    Customer result = new Customer();
                    result = gson.fromJson(response.toString(), type);
                    //getmMvpView().onGetCustomerByGuidSuccess(result);
                    if (!isViewAttached()) {
                        return;
                    }
                    getmMvpView().getCustomerByGuidSuccess(result);
                }
            }

            @Override
            public void onResponseSuccess(JSONArray response) {

            }

            @Override
            public void onResponseError(ANError anError) {

            }
        });
    }

    @Override
    public boolean isHaveCustomerSave() {
        if (getDataManager().getCurrentCustomerName().equals("") || getDataManager().getCurrentCustomerGuid().equals("")
                || getDataManager().getPayMethod() == -1) {
            return false;
        }
        return true;
    }

    @Override
    public int getCustomerPayType() {
        return getDataManager().getPayMethod();
    }

}
