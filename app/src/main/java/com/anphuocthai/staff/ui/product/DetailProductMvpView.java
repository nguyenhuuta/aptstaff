package com.anphuocthai.staff.ui.product;

import com.anphuocthai.staff.data.db.model.OrderProduct;
import com.anphuocthai.staff.model.order.Product;
import com.anphuocthai.staff.ui.base.MvpView;

import java.util.List;

public interface DetailProductMvpView extends MvpView {
    void openOrderCartActivity();

    void displayProduct(Product product);

    void setPriceFeeAndCV(float price, float sumfee, int cv);

    void onUpdateShoppingCart(List<OrderProduct> orderProducts);

    void onCreateOrderSuccess();

}
