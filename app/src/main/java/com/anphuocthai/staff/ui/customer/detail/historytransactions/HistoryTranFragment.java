package com.anphuocthai.staff.ui.customer.detail.historytransactions;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.di.component.ActivityComponent;
import com.anphuocthai.staff.ui.base.BaseFragment;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;
import com.anphuocthai.staff.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class HistoryTranFragment extends BaseFragment implements HistoryTranMvpView, SwipeRefreshLayout.OnRefreshListener {


    private RecyclerView recyclerView;

    private SwipeRefreshLayout mSwipeRefreshLayout;

    private TextView mEmptyView;

    HistoryTranAdapter mAdapter;

    private String guid;

    @Inject
    HistoryTranMvpPresenter<HistoryTranMvpView> mPresenter;

    @Inject
    LinearLayoutManager linearLayoutManager;
    List<OrderTran> orderTranList;


    public static HistoryTranFragment newInstance(String guid, Context context) {
        HistoryTranFragment fragment = new HistoryTranFragment();
        fragment.guid = guid;
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.history_tran_fragment, container, false);
        mEmptyView = view.findViewById(R.id.emptyView);
        ActivityComponent component = getActivityComponent();
        mSwipeRefreshLayout = view.findViewById(R.id.swipeRefresh);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        recyclerView = view.findViewById(R.id.history_tran_recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        if (orderTranList == null) {
            orderTranList = new ArrayList<>();
        }
        mAdapter = new HistoryTranAdapter(orderTranList);
        recyclerView.setAdapter(mAdapter);

        if (component != null) {
            component.inject(this);
            mPresenter.onAttach(this);
        }
        if (orderTranList.isEmpty()) {
            mSwipeRefreshLayout.setRefreshing(true);
            mPresenter.getHistoryTrans(guid);
        }
        return view;
    }

    @Override
    public void onRefresh() {
        mPresenter.getHistoryTrans(guid);
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void updateOrderTran(List<OrderTran> orderTranArrayList) {
        mSwipeRefreshLayout.setRefreshing(false);
        orderTranList.clear();
        orderTranList.addAll(orderTranArrayList);
        if (Utils.isNull(orderTranArrayList)) {
            mEmptyView.setVisibility(View.VISIBLE);
            return;
        }
        mEmptyView.setVisibility(View.GONE);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    protected void setUp(View view) {

    }
}
