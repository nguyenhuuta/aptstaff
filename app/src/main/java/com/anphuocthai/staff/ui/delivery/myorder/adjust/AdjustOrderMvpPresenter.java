package com.anphuocthai.staff.ui.delivery.myorder.adjust;

import com.anphuocthai.staff.ui.base.MvpPresenter;

public interface AdjustOrderMvpPresenter<V extends AdjustOrderMvpView> extends MvpPresenter<V> {
    void dismissDialog();
}
