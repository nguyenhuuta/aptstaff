package com.anphuocthai.staff.ui.customer.detail.infomation.editinformation.phonetypedialog;

import com.anphuocthai.staff.ui.base.MvpPresenter;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;

public interface PhoneTypeMvpPresenter<V extends PhoneTypeMvpView> extends MvpPresenter<V> {
   void onStopCollectDebt(OrderTran orderTran, int realReceive);
}
