package com.anphuocthai.staff.ui.delivery.nottransportyet.trachungtu;

import com.anphuocthai.staff.ui.base.MvpPresenter;

/**
 * Created by OpenYourEyes on 11/23/2020
 */
public interface ITraChungTuPresenter extends MvpPresenter<ITraChungTuView> {
    void layDanhSachDonDat(int page, int size);
}
