package com.anphuocthai.staff.ui.delivery.debtlocation;

import com.anphuocthai.staff.ui.base.MvpPresenter;

public interface DebtLocationMvpPresenter<V extends DebtLocationMvpView> extends MvpPresenter<V> {
}
