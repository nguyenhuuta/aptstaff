package com.anphuocthai.staff.ui.ordercart;

import com.anphuocthai.staff.data.db.model.OrderProduct;
import com.anphuocthai.staff.model.customer.Customer;
import com.anphuocthai.staff.ui.base.MvpPresenter;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;

import org.json.JSONObject;

public interface OrderCartMvpPresenter<V extends OrderCartMvpView> extends MvpPresenter<V> {

    void onGetAllOrderProduct();

    void onUpdateOrderCart(OrderProduct orderProduct,boolean isInsert);

    void onDeleteOrderCart(OrderProduct orderProduct);

    void onDeleteAllOrderCart();

    void onUpdateSumaryFeeAndCV(float sumfee, float sumCV, int tongChietKhau);

    void onStartOrder(JSONObject jsonObject);


    void onOpenSaleDialog(OrderProduct orderProduct, OrderCartAdapter.ViewHolder viewHolder);

    void showMessage(int restId);

    void openSearchActivity();

    void cancelOrderTran(OrderTran orderTran);

    void getAllOrderDeptCustomer(String guid);

    void onShowOrderConfigDialog(Customer customer, OrderCartAdapter adapter);

    void getCustomerByGuid(String guid);

    boolean isHaveCustomerSave();

    String getGuidCustomerSave();

    void getListTypeProduct();

    int percenChietKhau();

}
