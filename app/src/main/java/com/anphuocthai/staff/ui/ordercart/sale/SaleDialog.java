package com.anphuocthai.staff.ui.ordercart.sale;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.fragment.app.FragmentManager;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.data.db.model.OrderProduct;
import com.anphuocthai.staff.di.component.ActivityComponent;
import com.anphuocthai.staff.ui.base.BaseDialog;
import com.anphuocthai.staff.ui.ordercart.OrderCartAdapter;
import com.anphuocthai.staff.ui.product.OrderLogic;
import com.anphuocthai.staff.utils.Enum;
import com.anphuocthai.staff.utils.Utils;

import javax.inject.Inject;

import butterknife.OnClick;

public class SaleDialog extends BaseDialog implements SaleDialogMvpView {

    private static final String TAG = SaleDialog.class.getSimpleName();

    private static final int STEP_UNIT = 1000;

    @Inject
    SaleDialogMvpPresenter<SaleDialogMvpView> mPresenter;

    //@BindView(R.id.rating_bar_feedback)
    RatingBar mRatingBar;

    //@BindView(R.id.et_message)
    EditText mMessage;

    //@BindView(R.id.view_rating_message)
    View mRatingMessageView;

    //@BindView(R.id.view_play_store_rating)
    View mPlayStoreRatingView;

    //@BindView(R.id.btn_submit)
    Button mSubmitButton;

    Button btnLater;

    private float minFee;
    private float maxFee;

    ImageView imgSub;
    ImageView imgIncrease;
    EditText txtPrice;
    TextView txtCV;
    TextView txtAlert;

    OrderLogic orderLogic;
    OrderProduct orderProduct;
    OrderCartAdapter.ViewHolder viewHolder;

    public static SaleDialog newInstance() {
        SaleDialog fragment = new SaleDialog();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_sale_product, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            mRatingBar = view.findViewById(R.id.rating_bar_feedback);
            mMessage = view.findViewById(R.id.et_message);
            mRatingMessageView = view.findViewById(R.id.view_rating_message);
            mPlayStoreRatingView = view.findViewById(R.id.view_play_store_rating);
            mSubmitButton = view.findViewById(R.id.btn_submit);
            btnLater = view.findViewById(R.id.btn_later);
            imgSub = view.findViewById(R.id.sale_dialog_img_sub);
            imgIncrease = view.findViewById(R.id.sale_dialog_img_increase);
            txtPrice = view.findViewById(R.id.sale_dialog_txt_price);
            txtCV = view.findViewById(R.id.sale_dialog_txt_cv);
            txtAlert = view.findViewById(R.id.sale_dialog_txt_alert);
            mPresenter.onAttach(this);
        }

        return view;
    }

    public void show(FragmentManager fragmentManager, OrderProduct orderProduct, OrderCartAdapter.ViewHolder viewHolder) {
        super.show(fragmentManager, TAG);
        this.orderProduct = orderProduct;
        this.orderLogic = orderProduct.getOrderLogic();
        this.viewHolder = viewHolder;
        minFee = orderLogic.getCurrentPrice() - calculateMaxSalePrice();
        maxFee = orderLogic.getCurrentPrice();
    }


    @Override
    public void openPlayStoreForRating() {
        //AppUtils.openPlayStoreForApp(getContext());
    }

    @Override
    public void showPlayStoreRatingView() {
        mPlayStoreRatingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showRatingMessageView() {
        mRatingMessageView.setVisibility(View.VISIBLE);
    }

    @Override
    protected void setUp(View view) {

        mSubmitButton.setOnClickListener(v -> handleSubmitClick());

        btnLater.setOnClickListener(view1 -> mPresenter.onLaterClicked());

        mMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        imgSub.setOnClickListener(view12 -> subPrice());

        imgIncrease.setOnClickListener(view13 -> increasePrice());

        txtPrice.setText(Utils.formatValue(orderLogic.getCurrentPrice(), Enum.FieldValueType.CURRENCY));
        txtCV.setText(Utils.formatValue(orderLogic.getCv(), Enum.FieldValueType.NORMAL));
        clearAlert();

    }

    private void increasePrice() {
        clearAlert();
        float newPrice = orderLogic.getCurrentPrice() + STEP_UNIT;
        orderLogic.setCurrentPrice(newPrice);
        updatePriceDialogSale();
        calculateCV();
    }

    private void subPrice() {
        clearAlert();
        float newPrice = orderLogic.getCurrentPrice() - STEP_UNIT;
        if (newPrice < minFee) {
            newPrice = minFee;
            showAlert(getString(R.string.order_cart_sale_max_price) + ": " + Utils.formatValue(calculateMaxSalePrice(), Enum.FieldValueType.CURRENCY));
        }
        orderLogic.setCurrentPrice(newPrice);
        updatePriceDialogSale();
        calculateCV();
    }


    private void calculateCV() {

        float distance = maxFee - orderLogic.getCurrentPrice();

        int newCV = orderLogic.getFixCV() - (int) distance / 1000;
        orderLogic.setCv(newCV);
        txtCV.setText(Utils.formatValue(newCV, Enum.FieldValueType.NORMAL));
    }

    private void clearAlert() {
        txtAlert.setText("");
        txtAlert.setVisibility(View.GONE);
    }


    private void updatePriceDialogSale() {
        txtPrice.setText(Utils.formatValue(orderLogic.getCurrentPrice(), Enum.FieldValueType.CURRENCY));

    }

    private void showAlert(String message) {
        txtAlert.setVisibility(View.VISIBLE);
        txtAlert.setText(message);
    }

    private void handleSubmitClick() {
        viewHolder.onUpdatePrice(false);
//        viewHolder.logicChietKhau(orderProduct);
        dismissDialog();

    }

    private float calculateMaxSalePrice() {
        int maxSubCV = (int) orderLogic.getFixCV() / 2;
        return maxSubCV * 1000;
    }

    @OnClick(R.id.btn_later)
    void onLaterClick() {
        mPresenter.onLaterClicked();
    }

    @OnClick(R.id.btn_rate_on_play_store)
    void onPlayStoreRateClick() {
        mPresenter.onPlayStoreRatingClicked();
    }

    @Override
    public void disableRatingStars() {
        mRatingBar.setIsIndicator(true);
    }

    @Override
    public void hideSubmitButton() {
        mSubmitButton.setVisibility(View.GONE);
    }

    @Override
    public void dismissDialog() {
        super.dismissDialog(TAG);
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }


}