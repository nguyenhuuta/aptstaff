package com.anphuocthai.staff.ui.login;

import com.anphuocthai.staff.data.db.network.model.request.LoginRequest;
import com.anphuocthai.staff.ui.base.MvpPresenter;

public interface LoginMvpPresenter<V extends LoginMvpView> extends MvpPresenter<V> {

    void onLogin(LoginRequest request);

    void onAutoLoginChange(boolean isAutoLogin);

    void saveUserInfo(String name, String pass, String token, int userId, String guid);


}
