package com.anphuocthai.staff.ui.customer.detail.address;


import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.error.ANError;
import com.anphuocthai.staff.R;
import com.anphuocthai.staff.adapters.checkin.CustomerAddressDetailAdapter;
import com.anphuocthai.staff.api.ApiURL;
import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.base.IBaseCallback;
import com.anphuocthai.staff.interfaces.IAPICallback;
import com.anphuocthai.staff.model.customer.CustomerAddress;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;


public class AddressCustomerInfoFragment extends Fragment {

    private static final String TAG = AddressCustomerInfoFragment.class.getSimpleName();
    private Context context;
    private String guid;
    private RecyclerView detailCustomerAdressRecyclerView;
    private RecyclerView.LayoutManager aLayout;
    ArrayList<CustomerAddress> arrayCustomer = new ArrayList<>();
    CustomerAddressDetailAdapter mAdapter;

    public AddressCustomerInfoFragment() {
        // Required empty public constructor
    }

    public static AddressCustomerInfoFragment newInstance(String guid, Context context) {
        AddressCustomerInfoFragment addressCustomerInfoFragment = new AddressCustomerInfoFragment();
        addressCustomerInfoFragment.context = context;
        addressCustomerInfoFragment.guid = guid;
        return addressCustomerInfoFragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_address_customer_info, container, false);
        detailCustomerAdressRecyclerView = view.findViewById(R.id.ci_update_customer_address_recycler_view);
        detailCustomerAdressRecyclerView.setHasFixedSize(true);
        aLayout = new LinearLayoutManager(context);
        detailCustomerAdressRecyclerView.setLayoutManager(aLayout);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAdapter = new CustomerAddressDetailAdapter(arrayCustomer, context, new IBaseCallback<Integer>() {
            @Override
            public void onCallback(Integer position) {
                CustomerAddress customerAddress = arrayCustomer.get(position);
                int idThanhVien = customerAddress.getiD_ThanhVien();
                DialogEditAddress dialogEditAddress = DialogEditAddress.newInstance(idThanhVien);
                dialogEditAddress.setCallbackDialog(new IBaseCallback() {
                    @Override
                    public void onCallback(com.anphuocthai.staff.base.Type type, Object o) {
                        getAddressByGuidOfCustomer();
                    }
                });
                dialogEditAddress.show(getChildFragmentManager(), null);
            }
        });
        detailCustomerAdressRecyclerView.setAdapter(mAdapter);
        getAddressByGuidOfCustomer();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void getAddressByGuidOfCustomer() {
        HashMap param = new HashMap();
        param.put("guid", this.guid);
        NetworkManager.getInstance().sendGetRequest(ApiURL.GET_ADDRESS_BY_MEMBER_URL, param, new IAPICallback() {
            @Override
            public void onResponseSuccess(JSONObject response) {

            }

            @Override
            public void onResponseSuccess(JSONArray response) {

                if (response != null) {
                    Log.d(TAG, response.toString());
                    Gson gson = new Gson();
                    Type type = new TypeToken<ArrayList<CustomerAddress>>() {
                    }.getType();
                    arrayCustomer.clear();
                    arrayCustomer.addAll(gson.fromJson(response.toString(), type));
                    if (arrayCustomer.size() > 0) {
                        mAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onResponseError(ANError anError) {

            }
        });


    }

}