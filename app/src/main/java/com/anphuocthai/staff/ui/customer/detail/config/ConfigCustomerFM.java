package com.anphuocthai.staff.ui.customer.detail.config;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.di.component.ActivityComponent;
import com.anphuocthai.staff.model.customer.Customer;
import com.anphuocthai.staff.ui.base.BaseFragment;
import com.anphuocthai.staff.ui.login.LoginActivity;
import com.anphuocthai.staff.utils.App;

import javax.inject.Inject;

public class ConfigCustomerFM extends BaseFragment implements ConfigCustomerMvpView, AdapterView.OnItemSelectedListener {

    private static final String TAG = ConfigCustomerFM.class.getSimpleName();


    private String guid;

    private Context context;

    private EditText edtPersonOrder;

    private TextView txtPayDept;

    private Spinner spinner;

    private EditText edtDebtName;

    private Customer customer;

    private Button btnUpdateConfig;

    private ConstraintLayout editOtherDebtContainer;
    @Inject
    ConfigCustomerMvpPresenter<ConfigCustomerMvpView> mPresenter;

    @Inject
    LinearLayoutManager linearLayoutManager;

    @Inject
    ConfigCustomerAdapter mAdapter;


    public ConfigCustomerFM() {
        // Required empty public constructor
        this.context = App.getAppContext();
    }

    public static ConfigCustomerFM newInstance(String guid, Context context) {
        ConfigCustomerFM fragment = new ConfigCustomerFM();
        fragment.guid = guid;
        fragment.context = context;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void setUp(View view) {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.config_customer_fragment, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            mPresenter.onAttach(this);
            mPresenter.getCustomerByGuid(guid);
        }

        editOtherDebtContainer = view.findViewById(R.id.edit_other_debt_container);

        editOtherDebtContainer.setVisibility(View.GONE);

        edtPersonOrder = view.findViewById(R.id.edt_person_order);

        txtPayDept = view.findViewById(R.id.txt_pay_dept);

        spinner = view.findViewById(R.id.spinner_pay_dept_id);

        edtDebtName = view.findViewById(R.id.edt_label_debt_name);

        btnUpdateConfig = view.findViewById(R.id.btn_update_config);

        btnUpdateConfig.setOnClickListener((View v) -> {
            if (edtDebtName.getText().toString().trim().equals("") || edtDebtName.getText().toString().trim().equals("")) {
                showMessage(R.string.alert_empty_name_or_debt);
                return;
            }
            mPresenter.updateCustomer(customer,
                    edtPersonOrder.getText().toString(),
                    edtDebtName.getText().toString());
        });



        return view;
    }

    private void setupSpinner(){
        spinner.setOnItemSelectedListener(this);
        String[] categories;
        if (customer != null && customer.getCustomerData() != null && customer.getCustomerData().getThanhToanCongNo() != null){
             categories = new String[]{getString(R.string.common_debt_type),
                     getString(R.string.debt_month),
                     getString(R.string.debt_half_month),
                     getString(R.string.debt_week),
                     customer.getCustomerData().getThanhToanCongNo()
             };
        }else {
            categories = new String[]{getString(R.string.common_debt_type),
                    getString(R.string.debt_month),
                    getString(R.string.debt_half_month),
                    getString(R.string.debt_week),
                    getString(R.string.debt_other)
            };
        }

        // Creating adapter for spinner
        MyArrayAdapter dataAdapter = new MyArrayAdapter(getBaseActivity(), R.layout.spinner_item, categories);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
    }

    @Override
    public void onUpdateView(Customer customer) {
        if (customer == null) {
            return;
        }
        this.customer = customer;
        setupSpinner();
        setupSpinnerSelected();
        edtPersonOrder.setText(customer.getTenDayDu());
        if (customer.getCustomerData().getNguoiDatDon() != null) {
            edtPersonOrder.setText(customer.getCustomerData().getNguoiDatDon());
        }
        txtPayDept.setText(R.string.empty_screen);
        if (customer.getCustomerData().getThanhToanCongNo() != null) {
            txtPayDept.setText(customer.getCustomerData().getThanhToanCongNo());
        }
        edtDebtName.setText(R.string.empty_screen);
        if (customer.getCustomerData().getThanhToanCongNo() != null) {
            edtDebtName.setText(customer.getCustomerData().getThanhToanCongNo());
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        try {
            switch (i)
            {
                case 0:
                    txtPayDept.setText(getString(R.string.common_no_data));
                    edtDebtName.setText(getString(R.string.common_no_data));
                    if (customer.getCustomerData() != null) {
                        customer.getCustomerData().setNguoiDatDon(customer.getTenDayDu());
                        customer.getCustomerData().setThanhToanCongNoId(0);
                    }

                    btnUpdateConfig.setVisibility(View.GONE);
                    editOtherDebtContainer.setVisibility(View.GONE);
                    break;
                case 1:
                    txtPayDept.setText(getString(R.string.debt_month));
                    edtDebtName.setText(getString(R.string.debt_month));
                    customer.getCustomerData().setNguoiDatDon(customer.getTenDayDu());
                    customer.getCustomerData().setThanhToanCongNoId(1);
                    customer.getCustomerData().setThanhToanCongNo(getString(R.string.debt_month));
                    btnUpdateConfig.setVisibility(View.VISIBLE);
                    editOtherDebtContainer.setVisibility(View.GONE);
                    break;
                case 2:
                    txtPayDept.setText(getString(R.string.debt_half_month));
                    edtDebtName.setText(getString(R.string.debt_half_month));
                    customer.getCustomerData().setNguoiDatDon(customer.getTenDayDu());
                    customer.getCustomerData().setThanhToanCongNoId(2);
                    customer.getCustomerData().setThanhToanCongNo(getString(R.string.debt_half_month));
                    btnUpdateConfig.setVisibility(View.VISIBLE);
                    editOtherDebtContainer.setVisibility(View.GONE);
                    break;
                case 3:
                    txtPayDept.setText(getString(R.string.debt_week));
                    edtDebtName.setText(getString(R.string.debt_week));
                    customer.getCustomerData().setNguoiDatDon(customer.getTenDayDu());
                    customer.getCustomerData().setThanhToanCongNoId(3);
                    customer.getCustomerData().setThanhToanCongNo(getString(R.string.debt_week));
                    btnUpdateConfig.setVisibility(View.VISIBLE);
                    editOtherDebtContainer.setVisibility(View.GONE);

                    break;
                case 4:
                    txtPayDept.setText(getString(R.string.debt_other));
                    edtDebtName.setText(customer.getCustomerData().getThanhToanCongNo());
                    customer.getCustomerData().setNguoiDatDon(customer.getTenDayDu());
                    customer.getCustomerData().setThanhToanCongNoId(4);
                    customer.getCustomerData().setThanhToanCongNo(getString(R.string.debt_other));
                    btnUpdateConfig.setVisibility(View.VISIBLE);
                    editOtherDebtContainer.setVisibility(View.VISIBLE);
                    break;
            }
        }catch (Exception e) {
            showMessage(R.string.have_error);
            Intent intent = LoginActivity.getStartIntent(context);
            startActivity(intent);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private void setupSpinnerSelected(){
        switch(customer.getCustomerData().getThanhToanCongNoId()) {
            case 0:
                spinner.setSelection(0);
                break;
            case 1:
                spinner.setSelection(1);
                break;
            case 2:
                spinner.setSelection(2);
                break;
            case 3:
                spinner.setSelection(3);
                break;
            case 4:
                spinner.setSelection(4);
                break;
        }
    }


    private class MyArrayAdapter extends ArrayAdapter<String> {

        private int textViewResourceId;

        public MyArrayAdapter(Context context, int textViewResourceId, String[] objects) {
            super(context, textViewResourceId, objects);
            this.textViewResourceId = textViewResourceId;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            View view;
            TextView text;

            if (convertView == null) {
                view = getLayoutInflater().inflate(textViewResourceId, parent, false);
            } else {
                view = convertView;
            }


            text = (TextView) view;
            String item;

            item = getItem(position);

            text.setText((CharSequence) item);
            if (position == 0) {
                ViewGroup.LayoutParams l = text.getLayoutParams();
                l.height = 1;
                text.setLayoutParams(l);
            } else {
                ViewGroup.LayoutParams l = text.getLayoutParams();
                //you can change height value to yours
                l.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                text.setLayoutParams(l);
            }

            return view;
        }
    }
}
