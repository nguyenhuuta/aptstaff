package com.anphuocthai.staff.ui.delivery.nottransportyet.transportorder;

import com.anphuocthai.staff.ui.base.MvpPresenter;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;

import java.util.List;

import io.reactivex.Flowable;

public interface TransportOrderMvpPresenter<V extends TransportOrderMvpView> extends MvpPresenter<V> {
    void onGetAllNotTranOrder();
    Flowable<List<OrderTran>> rxJavaOnGetAllNotTranOrder(final int page);
    void hideRecyclerView(boolean isHide);

    int getUserId();
}
