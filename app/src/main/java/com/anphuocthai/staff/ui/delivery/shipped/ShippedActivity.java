package com.anphuocthai.staff.ui.delivery.shipped;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ahamed.multiviewadapter.SimpleRecyclerAdapter;
import com.anphuocthai.staff.R;
import com.anphuocthai.staff.ui.base.BaseActivity;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;
import com.anphuocthai.staff.ui.delivery.shipped.finishtransport.FinishTransportDialog;

import java.util.ArrayList;

import javax.inject.Inject;

public class ShippedActivity extends BaseActivity implements ShippedMvpView {

    @Inject
    ShippedMvpPresenter<ShippedMvpView> mPresenter;

    private RecyclerView recyclerView;
    SimpleRecyclerAdapter<OrderTran, ShippedAdapter> adapter;

    private View emptyView;

    private TextView txtRetry;

    private Button btnRetry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shipped);
        getmActivityComponent().inject(this);
        mPresenter.onAttach(this);
        setupToolbar(getString(R.string.order_shipped_toolbar_title));
        initView();
        setUpAdapter();
    }

    private void initView() {
        recyclerView = findViewById(R.id.shipped_recycler_view);
        emptyView = findViewById(R.id.empty_view_shipped);
        txtRetry = emptyView.findViewById(R.id.txt_retry);
        btnRetry = emptyView.findViewById(R.id.btn_retry);
        txtRetry.setVisibility(View.GONE);
        btnRetry.setVisibility(View.GONE);
    }

    protected void setUpAdapter() {
        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        recyclerView.addItemDecoration(
                new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL));

        adapter = new SimpleRecyclerAdapter<>(new ShippedAdapter(this, mPresenter));

        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(adapter);
        getAllShipped();
    }

    public void getAllShipped() {
        mPresenter.getAllShippedOrderTrans();
    }

    @Override
    public void displayOrderTrans(ArrayList<OrderTran> orderTrans) {
        if (orderTrans.size() <= 0) {
            emptyView.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        } else {
            emptyView.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
        adapter.setData(orderTrans);
    }

    @Override
    public void onShowFinishShipDialog(OrderTran item) {
        FinishTransportDialog.newInstance().show(getSupportFragmentManager(), item, this);
    }

    public static Intent getShippedIntent(Context context) {
        return new Intent(context, ShippedActivity.class);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
