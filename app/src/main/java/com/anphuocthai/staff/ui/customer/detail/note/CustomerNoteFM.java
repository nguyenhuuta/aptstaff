package com.anphuocthai.staff.ui.customer.detail.note;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.di.component.ActivityComponent;
import com.anphuocthai.staff.model.customer.Note;
import com.anphuocthai.staff.ui.base.BaseFragment;
import com.anphuocthai.staff.utils.App;

import java.util.ArrayList;

import javax.inject.Inject;

public class CustomerNoteFM extends BaseFragment implements CustomerNoteMvpView {
    private static final String TAG = CustomerNoteFM.class.getSimpleName();

    private RecyclerView recyclerView;

    private RecyclerView.LayoutManager aLayout;

    private SwipeRefreshLayout mSwipeRefreshLayout;

    private String guid;

    private Context context;

    @Inject
    CustomerNoteMvpPresenter<CustomerNoteMvpView> mPresenter;

    @Inject
    LinearLayoutManager linearLayoutManager;

    @Inject
    CustomerNoteAdapter mAdapter;


    public CustomerNoteFM() {
        // Required empty public constructor
        this.context = App.getAppContext();
    }

    public static CustomerNoteFM newInstance(String guid, Context context) {
        CustomerNoteFM fragment = new CustomerNoteFM();
        fragment.guid = guid;
        fragment.context = context;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getActivityComponent().inject(this);
//        mPresenter.onAttach(this);
//        mPresenter.getNotes(this.guid);
    }

    @Override
    protected void setUp(View view) {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.customer_note_fragment, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            mPresenter.onAttach(this);
            mPresenter.getNotes(this.guid);
        }

        recyclerView = view.findViewById(R.id.customer_note_recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setAdapter(mAdapter);
        return view;
    }

    @Override
    public void updateNote(ArrayList<Note> noteArrayList) {
        mAdapter.addItems(noteArrayList);
    }
}
