package com.anphuocthai.staff.ui.login;

import com.anphuocthai.staff.data.db.network.model.request.LoginRequest;
import com.anphuocthai.staff.data.db.network.model.response.LoginResponse;
import com.anphuocthai.staff.ui.base.MvpInteractor;

import io.reactivex.Observable;

public interface LoginMvpInteractor extends MvpInteractor {
    Observable<LoginResponse> doServerLoginApiCall(LoginRequest request);
}
