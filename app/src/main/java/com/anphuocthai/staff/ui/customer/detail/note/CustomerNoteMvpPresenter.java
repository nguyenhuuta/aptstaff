package com.anphuocthai.staff.ui.customer.detail.note;

import com.anphuocthai.staff.ui.base.MvpPresenter;

public interface CustomerNoteMvpPresenter<V extends CustomerNoteMvpView> extends MvpPresenter<V> {
    void getNotes(String guid);
}
