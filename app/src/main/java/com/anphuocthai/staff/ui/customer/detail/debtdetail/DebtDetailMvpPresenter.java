package com.anphuocthai.staff.ui.customer.detail.debtdetail;

import com.anphuocthai.staff.ui.base.MvpPresenter;

public interface DebtDetailMvpPresenter<V extends DebtDetailMvpView> extends MvpPresenter<V> {

    void getHistoryTrans(String guid);
}
