
package com.anphuocthai.staff.ui.delivery.model;

import com.anphuocthai.staff.model.ThongTinDonHangModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class OrderTran implements Serializable {

    @SerializedName("dD_DonDatID")
    @Expose
    private Integer dDDonDatID;
    @SerializedName("sessionID")
    @Expose
    private String sessionID;
    @SerializedName("maDonDat")
    @Expose
    private String maDonDat;
    @SerializedName("tenDonDat")
    @Expose
    private String tenDonDat;
    @SerializedName("ngayBatDau")
    @Expose
    private String ngayBatDau;
    @SerializedName("ngayKetThuc")
    @Expose
    private String ngayKetThuc;
    @SerializedName("dD_TrangThaiID")
    @Expose
    private Integer dDTrangThaiID;
    @SerializedName("iD_ThanhVien")
    @Expose
    private Integer iDThanhVien;
    @SerializedName("dD_ThanhToanID")
    @Expose
    private Integer dDThanhToanID;
    @SerializedName("dD_KhoHangID")
    @Expose
    private Integer dDKhoHangID;
    @SerializedName("tongGiaTri")
    @Expose
    private Integer tongGiaTri;
    @SerializedName("tongCV")
    @Expose
    private Integer tongCV;

    //Số tiền đã thanh toán
    @SerializedName("tongThanhToan")
    @Expose
    private Integer tongThanhToan;
    @SerializedName("loaiDonDatID")
    @Expose
    private Integer loaiDonDatID;
    @SerializedName("maBarCode")
    @Expose
    private String maBarCode;
    @SerializedName("nguoiTaoID")
    @Expose
    private Integer nguoiTaoID;
    @SerializedName("dD_DonDatChaID")
    @Expose
    private Object dDDonDatChaID;
    @SerializedName("soHoaDon")
    @Expose
    private String soHoaDon;
    @SerializedName("ghiChu")
    @Expose
    private String ghiChu;
    @SerializedName("doiTuongID")
    @Expose
    private Integer doiTuongID;
    @SerializedName("nhanVienMuaID")
    @Expose
    private Object nhanVienMuaID;
    @SerializedName("nguoiTiepNhanID")
    @Expose
    private Integer nguoiTiepNhanID;
    @SerializedName("isNo")
    @Expose
    private Boolean isNo;
    @SerializedName("kinhDoanhID")
    @Expose
    private Integer kinhDoanhID;
    @SerializedName("trangThaiText")
    @Expose
    private String trangThaiText;
    @SerializedName("maKhoHang")
    @Expose
    private String maKhoHang;
    @SerializedName("tenKhoHang")
    @Expose
    private String tenKhoHang;
    @SerializedName("tenThanhVien")
    @Expose
    private String tenThanhVien;
    @SerializedName("guidThanhVien")
    @Expose
    private String guidThanhVien;
    @SerializedName("tenNhanVien")
    @Expose
    private String tenNhanVien;
    @SerializedName("nguoiThucHien")
    @Expose
    private String nguoiThucHien;
    @SerializedName("phuongThucThanhToan")
    @Expose
    private String phuongThucThanhToan;
    @SerializedName("hanghoas")
    @Expose
    private List<Hanghoa> hanghoas = null;
    @SerializedName("nhatkys")
    @Expose
    private Object nhatkys;
    @SerializedName("vanchuyens")
    @Expose
    private List<Vanchuyen> vanchuyens = null;
    @SerializedName("thanhtoans")
    @Expose
    private Object thanhtoans;
    @SerializedName("loaiDonDatText")
    @Expose
    private String loaiDonDatText;
    @SerializedName("loaidondat")
    @Expose
    private Integer loaidondat;
    @SerializedName("trangthai")
    @Expose
    private Integer trangthai;
    @SerializedName("doituong")
    @Expose
    private Integer doituong;
    @SerializedName("isEdit")
    @Expose
    private Boolean isEdit;

    @SerializedName("phaiThanhToan")
    @Expose
    private Integer phaiThanhToan;

    @SerializedName("daThu")
    @Expose
    private Integer daThu; // Chờ thanh toán

    @SerializedName("diaChiNguoiMua")
    private String diaChiNguoiMua;
    @SerializedName("isBanGiaoChungTu")
    public boolean isBanGiaoChungTu;
    @SerializedName("tienVanChuyenThu")
    public int tienVanChuyenThu;

    private String contentDetail;

    @SerializedName("isDuyetCapCao")
    private boolean isDuyetCapCao;

    @SerializedName("ngayThucHien")
    private String ngayThucHien;
    @SerializedName("ketThucThucHien")
    private String ketThucThucHien;

    public String getCountDown() {
        String time1 = ngayThucHien == null ? "" : ngayThucHien;
        String time2 = ketThucThucHien == null ? "" : ketThucThucHien;
        if (time1.isEmpty()) {
            return "";
        }
        return time1 + " - " + time2;
    }


    public boolean isDuyetCapCao() {
        return isDuyetCapCao;
    }

    public String getDiaChiNguoiMua() {
        return diaChiNguoiMua;
    }

    private boolean selected;

    public void setContentDetail(String contentDetail) {
        this.contentDetail = contentDetail;
    }

    public String getContentDetail() {
        return contentDetail;
    }

    public Integer getDDDonDatID() {
        return dDDonDatID;
    }

    public void setDDDonDatID(Integer dDDonDatID) {
        this.dDDonDatID = dDDonDatID;
    }

    public String getSessionID() {
        return sessionID;
    }

    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }

    public String getMaDonDat() {
        return maDonDat;
    }

    public void setMaDonDat(String maDonDat) {
        this.maDonDat = maDonDat;
    }

    public String getTenDonDat() {
        return tenDonDat;
    }

    public void setTenDonDat(String tenDonDat) {
        this.tenDonDat = tenDonDat;
    }

    public String getNgayBatDau() {
        return ngayBatDau;
    }

    public void setNgayBatDau(String ngayBatDau) {
        this.ngayBatDau = ngayBatDau;
    }

    public String getNgayKetThuc() {
        return ngayKetThuc;
    }

    public void setNgayKetThuc(String ngayKetThuc) {
        this.ngayKetThuc = ngayKetThuc;
    }

    public Integer getDDTrangThaiID() {
        return dDTrangThaiID;
    }

    public void setDDTrangThaiID(Integer dDTrangThaiID) {
        this.dDTrangThaiID = dDTrangThaiID;
    }

    public Integer getIDThanhVien() {
        return iDThanhVien;
    }

    public void setIDThanhVien(Integer iDThanhVien) {
        this.iDThanhVien = iDThanhVien;
    }

    public Integer getDDThanhToanID() {
        return dDThanhToanID;
    }

    public void setDDThanhToanID(Integer dDThanhToanID) {
        this.dDThanhToanID = dDThanhToanID;
    }

    public Integer getDDKhoHangID() {
        return dDKhoHangID;
    }

    public void setDDKhoHangID(Integer dDKhoHangID) {
        this.dDKhoHangID = dDKhoHangID;
    }

    public Integer getTongGiaTri() {
        return tongGiaTri;
    }

    public void setTongGiaTri(Integer tongGiaTri) {
        this.tongGiaTri = tongGiaTri;
    }

    public Integer getTongCV() {
        return tongCV;
    }

    public void setTongCV(Integer tongCV) {
        this.tongCV = tongCV;
    }


    //Số tiền đã thanh toán
    public Integer getTongThanhToan() {
        return tongThanhToan == null ? 0 : tongThanhToan;
    }

    public void setTongThanhToan(Integer tongThanhToan) {
        this.tongThanhToan = tongThanhToan;
    }

    public Integer getLoaiDonDatID() {
        return loaiDonDatID;
    }

    public void setLoaiDonDatID(Integer loaiDonDatID) {
        this.loaiDonDatID = loaiDonDatID;
    }

    public String getMaBarCode() {
        return maBarCode;
    }

    public void setMaBarCode(String maBarCode) {
        this.maBarCode = maBarCode;
    }

    public Integer getNguoiTaoID() {
        return nguoiTaoID;
    }

    public void setNguoiTaoID(Integer nguoiTaoID) {
        this.nguoiTaoID = nguoiTaoID;
    }

    public Object getDDDonDatChaID() {
        return dDDonDatChaID;
    }

    public void setDDDonDatChaID(Object dDDonDatChaID) {
        this.dDDonDatChaID = dDDonDatChaID;
    }

    public String getSoHoaDon() {
        return soHoaDon;
    }

    public void setSoHoaDon(String soHoaDon) {
        this.soHoaDon = soHoaDon;
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    public Integer getDoiTuongID() {
        return doiTuongID;
    }

    public void setDoiTuongID(Integer doiTuongID) {
        this.doiTuongID = doiTuongID;
    }

    public Object getNhanVienMuaID() {
        return nhanVienMuaID;
    }

    public void setNhanVienMuaID(Object nhanVienMuaID) {
        this.nhanVienMuaID = nhanVienMuaID;
    }

    public Integer getNguoiTiepNhanID() {
        return nguoiTiepNhanID;
    }

    public void setNguoiTiepNhanID(Integer nguoiTiepNhanID) {
        this.nguoiTiepNhanID = nguoiTiepNhanID;
    }

    public Boolean getIsNo() {
        return isNo;
    }

    public void setIsNo(Boolean isNo) {
        this.isNo = isNo;
    }

    public Integer getKinhDoanhID() {
        return kinhDoanhID;
    }

    public void setKinhDoanhID(Integer kinhDoanhID) {
        this.kinhDoanhID = kinhDoanhID;
    }

    public String getTrangThaiText() {
        return trangThaiText;
    }

    public void setTrangThaiText(String trangThaiText) {
        this.trangThaiText = trangThaiText;
    }

    public String getMaKhoHang() {
        return maKhoHang;
    }

    public void setMaKhoHang(String maKhoHang) {
        this.maKhoHang = maKhoHang;
    }

    public String getTenKhoHang() {
        return tenKhoHang;
    }

    public void setTenKhoHang(String tenKhoHang) {
        this.tenKhoHang = tenKhoHang;
    }

    public String getTenThanhVien() {
        return tenThanhVien;
    }

    public void setTenThanhVien(String tenThanhVien) {
        this.tenThanhVien = tenThanhVien;
    }

    public String getGuidThanhVien() {
        return guidThanhVien;
    }

    public void setGuidThanhVien(String guidThanhVien) {
        this.guidThanhVien = guidThanhVien;
    }

    public String getTenNhanVien() {
        return tenNhanVien;
    }

    public void setTenNhanVien(String tenNhanVien) {
        this.tenNhanVien = tenNhanVien;
    }

    public String getNguoiThucHien() {
        return nguoiThucHien;
    }

    public void setNguoiThucHien(String nguoiThucHien) {
        this.nguoiThucHien = nguoiThucHien;
    }

    public String getPhuongThucThanhToan() {
        return phuongThucThanhToan;
    }

    public void setPhuongThucThanhToan(String phuongThucThanhToan) {
        this.phuongThucThanhToan = phuongThucThanhToan;
    }

    public List<Hanghoa> getHanghoas() {
        return hanghoas;
    }


    public void setHanghoas(List<Hanghoa> hanghoas) {
        this.hanghoas = hanghoas;
    }

    public Object getNhatkys() {
        return nhatkys;
    }

    public void setNhatkys(Object nhatkys) {
        this.nhatkys = nhatkys;
    }

    public List<Vanchuyen> getVanchuyens() {
        return vanchuyens;
    }

    public String getDiaChiVanChuyen() {
        if (vanchuyens != null && vanchuyens.size() > 0) {
            String diaChi = vanchuyens.get(0).getDiaChiNhanHang();
            return diaChi == null ? "" : diaChi;
        }
        return "";
    }


    public void setVanchuyens(List<Vanchuyen> vanchuyens) {
        this.vanchuyens = vanchuyens;
    }

    public Object getThanhtoans() {
        return thanhtoans;
    }

    public void setThanhtoans(Object thanhtoans) {
        this.thanhtoans = thanhtoans;
    }

    public String getLoaiDonDatText() {
        return loaiDonDatText;
    }

    public void setLoaiDonDatText(String loaiDonDatText) {
        this.loaiDonDatText = loaiDonDatText;
    }

    public Integer getLoaidondat() {
        return loaidondat;
    }

    public void setLoaidondat(Integer loaidondat) {
        this.loaidondat = loaidondat;
    }

    public Integer getTrangthai() {
        return trangthai;
    }

    public void setTrangthai(Integer trangthai) {
        this.trangthai = trangthai;
    }

    public Integer getDoituong() {
        return doituong;
    }

    public void setDoituong(Integer doituong) {
        this.doituong = doituong;
    }

    public Boolean getIsEdit() {
        return isEdit;
    }

    public void setIsEdit(Boolean isEdit) {
        this.isEdit = isEdit;
    }


    public Integer getPhaiThanhToan() {
        return phaiThanhToan;
    }

    public void setPhaiThanhToan(Integer phaiThanhToan) {
        this.phaiThanhToan = phaiThanhToan;
    }

    public Integer getDaThu() {
        if (daThu != null) {
            return daThu;
        }
        return 0;
    }

    public void setDaThu(Integer daThu) {
        this.daThu = daThu;
    }

    public OrderTran clone() {
        OrderTran ot = new OrderTran();
        ot.dDDonDatID = dDDonDatID;
        ot.sessionID = sessionID;
        ot.maDonDat = maDonDat;
        ot.tenDonDat = tenDonDat;
        ot.ngayBatDau = ngayBatDau;
        ot.ngayKetThuc = ngayKetThuc;
        ot.dDTrangThaiID = dDTrangThaiID;
        ot.iDThanhVien = iDThanhVien;
        ot.dDThanhToanID = dDThanhToanID;
        ot.dDKhoHangID = dDKhoHangID;
        ot.tongGiaTri = tongGiaTri;
        ot.tongCV = tongCV;
        ot.tongThanhToan = tongThanhToan;
        ot.loaiDonDatID = loaiDonDatID;
        ot.maBarCode = maBarCode;
        ot.nguoiTaoID = nguoiTaoID;
        ot.dDDonDatChaID = dDDonDatChaID;
        ot.soHoaDon = soHoaDon;
        ot.ghiChu = ghiChu;
        ot.doiTuongID = doiTuongID;
        ot.nhanVienMuaID = nhanVienMuaID;
        ot.nguoiTiepNhanID = nguoiTiepNhanID;
        ot.isNo = isNo;
        ot.kinhDoanhID = kinhDoanhID;
        ot.trangThaiText = trangThaiText;
        ot.maKhoHang = maKhoHang;
        ot.tenKhoHang = tenKhoHang;
        ot.tenThanhVien = tenThanhVien;
        ot.guidThanhVien = guidThanhVien;
        ot.tenNhanVien = tenNhanVien;
        ot.nguoiThucHien = nguoiThucHien;
        ot.phuongThucThanhToan = phuongThucThanhToan;
        ot.hanghoas = hanghoas;
        ot.nhatkys = nhatkys;
        ot.vanchuyens = vanchuyens;
        ot.thanhtoans = thanhtoans;
        ot.loaiDonDatText = loaiDonDatText;
        ot.loaidondat = loaidondat;
        ot.trangthai = trangthai;
        ot.doituong = doituong;
        ot.isEdit = isEdit;
        ot.phaiThanhToan = phaiThanhToan;
        ot.daThu = daThu;
        return ot;
    }

    public boolean isTheSameVanChuyen(OrderTran ot) {
        if (ot == null) {
            return false;
        }

        if (ot.getVanchuyens() == null) {
            if (ot.getVanchuyens() == null || ot.getVanchuyens().isEmpty()) {
                return true;
            } else {
                return false;
            }
        }

        if (getVanchuyens().isEmpty()) {
            if (ot.getVanchuyens() == null || ot.getVanchuyens().isEmpty()) {
                return true;
            } else {
                return false;
            }
        }

        if (ot.getVanchuyens() == null || ot.getVanchuyens().isEmpty()) {
            return false;
        }

        if (getVanchuyens().get(0).getNguoiChuyenID() == ot.getVanchuyens().get(0).getNguoiChuyenID()) {
            return true;
        }

        return false;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }


    int remainPay = 0;

    public int remainPay() {
        if (remainPay == 0) {
            int tongGT = tongGiaTri != null ? tongGiaTri : 0;
            int tongTT = tongThanhToan != null ? tongThanhToan : 0;
            int daThu = this.daThu != null ? this.daThu : 0;
            remainPay = tongGT - tongTT - daThu;
            if (remainPay < 0) {
                remainPay = 0;
            }
        }
        return remainPay;
    }

    private ThongTinDonHangModel thongTinDonHangModel;

    public ThongTinDonHangModel getThongTinDonHang() {
        if (hanghoas == null) {
            thongTinDonHangModel = new ThongTinDonHangModel("", 0, 0);
        }
        if (thongTinDonHangModel != null) {
            return thongTinDonHangModel;
        }

        StringBuilder stringBuilder = new StringBuilder("Thông tin đơn hàng:");
        int soLuong = 0;
        for (Hanghoa hanghoa : hanghoas) {
            soLuong += hanghoa.getSoLuong();
            stringBuilder.append("\n");
            stringBuilder.append("     - ").append(hanghoa.getTenHangHoa()).append("(").append(hanghoa.getDonVi()).append(")");
        }
        thongTinDonHangModel = new ThongTinDonHangModel(stringBuilder.toString(), soLuong, tongCV);
        return thongTinDonHangModel;
    }
}
