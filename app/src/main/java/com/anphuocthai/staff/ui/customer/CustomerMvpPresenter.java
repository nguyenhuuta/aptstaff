package com.anphuocthai.staff.ui.customer;

import android.view.View;

import com.anphuocthai.staff.model.customer.SearchCustomer;
import com.anphuocthai.staff.ui.base.MvpPresenter;

import org.json.JSONArray;

import java.util.List;

import io.reactivex.Flowable;

public interface CustomerMvpPresenter<V extends CustomerMvpView> extends MvpPresenter<V> {
    void getAllCustomer();

    void onQuickActionShow(View view);

    void onShowBottomSheet(SearchCustomer customer);

    void onCustomerCall(SearchCustomer customer);

    void onCustomerSMS(SearchCustomer customer);

    void onCustomerDetail(SearchCustomer customer);

    void loadAllContacts();

    void hideLoading();

    void openAddNewWS(SearchCustomer customer);

    void openAddNewQWS(SearchCustomer customer);

    void updateContacts(JSONArray jsonArray);

    Flowable<List<SearchCustomer>> rxJavaOnGetAllCustomer(final int page);

    void onSearchCustomer(String customerName);
}
