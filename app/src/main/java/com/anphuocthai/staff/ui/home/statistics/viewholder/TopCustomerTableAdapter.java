package com.anphuocthai.staff.ui.home.statistics.viewholder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.model.SeeMore;
import com.anphuocthai.staff.ui.base.BaseViewHolder;
import com.anphuocthai.staff.ui.home.statistics.model.updatedynamic.Item;
import com.anphuocthai.staff.widget.IRecycleViewCallback;

import java.util.ArrayList;
import java.util.List;

public class TopCustomerTableAdapter extends RecyclerView.Adapter<BaseViewHolder> {
    private static final int VIEW_TYPE_HEADER = 0;
    private static final int VIEW_TYPE_NORMAL = 1;


    private static final int VIEW_LOAD_MORE = 3;

    private List<Item> mListItem;
    private List<Item> mListOrigin;
    private Context context;
    private int color;
    private IRecycleViewCallback<Item> mCallback;

    public TopCustomerTableAdapter(int color, IRecycleViewCallback<Item> callback) {
        this.color = color;
        this.mCallback = callback;
        mListItem = new ArrayList<>();
    }


    public void addItems(List<Item> topCustomers) {
        if (topCustomers == null) return;
        if (mListItem == null) {
            mListItem = new ArrayList<>();
        } else {
            mListItem.clear();
        }
        mListOrigin = topCustomers;
        mListItem = new ArrayList<>();
        if (mListOrigin.size() > 10) {
            mListItem.addAll(mListOrigin.subList(0, 10));
            Item item = new Item();
            item.seeMore = SeeMore.SEE_MORE;
            mListItem.add(item);
        } else {
            mListItem.addAll(topCustomers);
        }
        notifyDataSetChanged();
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new NormalViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.header_table_top_customer, parent, false));
            case VIEW_TYPE_HEADER:
                return new HeaderViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.header_table_top_customer, parent, false));
            case VIEW_LOAD_MORE:
                return new BottomViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bottom_load_more, parent, false));
            default:
                return new NormalViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_demo, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemCount() {
        return mListItem.size() + 1; // 1 for header
    }


    public class HeaderViewHolder extends BaseViewHolder {
        private TextView txtSTT;
        private TextView txtCustomerName;
        private TextView txtCustomerCV;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            txtSTT = itemView.findViewById(R.id.txt_table_stt);
            txtCustomerName = itemView.findViewById(R.id.txt_table_customer_name);
            txtCustomerCV = itemView.findViewById(R.id.txt_table_cv);
            randomColor(txtSTT, txtCustomerName, txtCustomerCV);
        }

        void randomColor(TextView... textViews) {
            for (TextView textView : textViews) {
                textView.setBackgroundColor(color);
            }
        }

        @Override
        protected void clear() {

        }

        @Override
        public void onBind(int position) {
            super.onBind(position);
        }
    }


    public class NormalViewHolder extends BaseViewHolder {

        private TextView txtSTT;
        private TextView txtCustomerName;
        private TextView txtCustomerCV;


        public NormalViewHolder(View itemView) {
            super(itemView);
            txtSTT = itemView.findViewById(R.id.txt_table_stt);
            txtCustomerName = itemView.findViewById(R.id.txt_table_customer_name);
            txtCustomerCV = itemView.findViewById(R.id.txt_table_cv);
            randomColor(txtSTT, txtCustomerName, txtCustomerCV);
        }

        void randomColor(TextView... textViews) {
            for (TextView textView : textViews) {
                textView.setBackgroundColor(getContext().getResources().getColor(R.color.opacity_blue));
                textView.setTextColor(getContext().getResources().getColor(R.color.graytext));
            }
        }


        @Override
        protected void clear() {

        }

        @Override
        public void onBind(int position) {
            super.onBind(position);
            if (position == 0) {
                return;
            }
            Item item = mListItem.get(position - 1);
            txtSTT.setText(String.valueOf(position));
            txtCustomerName.setText(item.getNhan());
            txtCustomerCV.setText(String.valueOf(item.getGiaTri1()));
            itemView.setOnClickListener(v -> mCallback.onItemClick(position, item));
        }
    }


    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return VIEW_TYPE_HEADER;
        } else {
            Item item = mListItem.get(position - 1);
            if (item.seeMore != null) {
                return VIEW_LOAD_MORE;
            } else {
                return VIEW_TYPE_NORMAL;
            }
        }
    }


    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public class BottomViewHolder extends BaseViewHolder {

        private TextView mSeeMore;


        BottomViewHolder(View itemView) {
            super(itemView);
            mSeeMore = itemView.findViewById(R.id.seeMore);
        }


        @Override
        protected void clear() {
        }

        @Override
        public void onBind(int position) {
            super.onBind(position);
            Item item = mListItem.get(position - 1);
            if (item.seeMore == null) return;

            if (item.seeMore == SeeMore.SEE_MORE) {
                mSeeMore.setText("Xem thêm");
            } else {
                mSeeMore.setText("Thu lại");
            }

            mSeeMore.setOnClickListener(view -> {
                if (item.seeMore == SeeMore.SEE_MORE) {
                    item.seeMore = SeeMore.COLLAPSE;
                    mListItem.addAll(10, mListOrigin.subList(10, mListOrigin.size()));
                    notifyDataSetChanged();
                } else {
                    item.seeMore = SeeMore.SEE_MORE;
                    List<Item> list = mListOrigin.subList(10, mListOrigin.size());
                    mListItem.removeAll(list);
                    notifyDataSetChanged();
                }

            });
        }
    }

}
