package com.anphuocthai.staff.ui.delivery.debt.customerdebt;

import com.anphuocthai.staff.entities.ListReportResponse;
import com.anphuocthai.staff.ui.base.MvpView;

public interface CustomerDebtMvpView extends MvpView {
    void onGetCustomerDebtSuccess(ListReportResponse reportResponse);
}
