package com.anphuocthai.staff.ui.customer.detail.config;

import com.anphuocthai.staff.model.customer.Customer;
import com.anphuocthai.staff.ui.base.MvpPresenter;

public interface ConfigCustomerMvpPresenter<V extends ConfigCustomerMvpView> extends MvpPresenter<V> {

    void getCustomerByGuid(String guid);

    void updateCustomer(Customer customer, String personOrder, String debtType);
}
