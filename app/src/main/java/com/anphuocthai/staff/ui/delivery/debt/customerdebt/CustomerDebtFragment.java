package com.anphuocthai.staff.ui.delivery.debt.customerdebt;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.activities.detaildebtactivity.DetailDebtActivity;
import com.anphuocthai.staff.di.component.ActivityComponent;
import com.anphuocthai.staff.entities.ListReportResponse;
import com.anphuocthai.staff.entities.Report;
import com.anphuocthai.staff.ui.base.BaseFragment;
import com.anphuocthai.staff.utils.Logger;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class CustomerDebtFragment extends BaseFragment implements CustomerDebtMvpView, SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = CustomerDebtFragment.class.getSimpleName();
    @Inject
    CustomerDebtMvpPresenter<CustomerDebtMvpView> mPresenter;

    @Inject
    LinearLayoutManager mLayoutManager;

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecycleView;
    private CustomerDebtAdapter mAdapter;
    private View emptyView;

    private ArrayList<Report> mListReport;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_report, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void setUp(View view) {
        mSwipeRefreshLayout = view.findViewById(R.id.swipe_container_customer_debt);
        mRecycleView = view.findViewById(R.id.recyclerView);
        emptyView = view.findViewById(R.id.emptyView);

        mSwipeRefreshLayout.setOnRefreshListener(this);
        mRecycleView.setLayoutManager(new LinearLayoutManager(getBaseActivity()));
        mListReport = new ArrayList<>();
        mAdapter = new CustomerDebtAdapter(mListReport);
        mAdapter.setOnItemClick(report -> {
            Intent intent = new Intent(this.getContext(), DetailDebtActivity.class);
            intent.putExtra(DetailDebtActivity.ARG_USER_ID, report.getiD_ThanhVien());
            intent.putExtra(DetailDebtActivity.ARG_USER_NAME, report.getTenThanhVien());
            startActivity(intent);
        });
        mRecycleView.setAdapter(mAdapter);
        mPresenter.getCustomerDebt(true);
    }

    @Override
    public void onGetCustomerDebtSuccess(ListReportResponse reportResponse) {
        if (reportResponse != null) {
            List<Report> list = reportResponse.getListReport();
            Report report = new Report();
            report.setTongGiaTri(reportResponse.getTongSoTien());
            report.setPhaiThanhToan(reportResponse.getSoTienConLai());
            report.setTongThanhToan(reportResponse.getTongSoTienDaThanhToan());
            list.add(report);
            mListReport.clear();
            mListReport.addAll(list);
            mAdapter.notifyDataSetChanged();
        }
        emptyView.setVisibility(mAdapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onResume() {
        super.onResume();
        Logger.d(TAG, "onResume");
    }

    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.setRefreshing(false);
        mPresenter.getCustomerDebt(true);
    }


    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void reloadData() {
        if (mPresenter != null && mListReport != null) {
            mPresenter.getCustomerDebt(false);
        }
    }
}
