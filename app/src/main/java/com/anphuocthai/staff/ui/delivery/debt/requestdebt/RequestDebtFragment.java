package com.anphuocthai.staff.ui.delivery.debt.requestdebt;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.di.component.ActivityComponent;
import com.anphuocthai.staff.model.CongNoModel;
import com.anphuocthai.staff.ui.base.BaseFragment;
import com.anphuocthai.staff.ui.searchsimpledata.SearchSimpleActivity;
import com.anphuocthai.staff.ui.searchsimpledata.SearchSimpleItem;
import com.anphuocthai.staff.utils.Constants;
import com.anphuocthai.staff.utils.Enum;
import com.anphuocthai.staff.utils.Utils;
import com.anphuocthai.staff.widget.LoadMoreRecycleView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

public class RequestDebtFragment extends BaseFragment implements RequestDebtMvpView, RequestDebtAdapter.Callback, SwipeRefreshLayout.OnRefreshListener {

    @Inject
    RequestDebtMvpPresenter<RequestDebtMvpView> mPresenter;

    @Inject
    RequestDebtAdapter mdapter;

    @Inject
    LinearLayoutManager mLayoutManager;

    private LoadMoreRecycleView recyclerView;
    private TextView txtSumaryMustCollect;
    private TextView txtSumaryOrder;
    private TextView txtSumaryAlreadyCollect;
    private View sumaryContainer;

    private SwipeRefreshLayout mSwipeRefreshLayout;

    private List<CongNoModel> mOrderTrans = new ArrayList<>();
    private int currentPage = Constants.FIRST_PAGE;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.request_debt_layout, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void setUp(View view) {

        sumaryContainer = view.findViewById(R.id.layout_sumary);
        txtSumaryMustCollect = view.findViewById(R.id.txt_must_collect);
        txtSumaryMustCollect.setVisibility(View.VISIBLE);
        txtSumaryAlreadyCollect = view.findViewById(R.id.txt_already_collect);
        txtSumaryAlreadyCollect.setVisibility(View.VISIBLE);
        txtSumaryOrder = view.findViewById(R.id.txt_number_order);
        sumaryContainer.setVisibility(View.GONE);
        recyclerView = view.findViewById(R.id.request_debt_recycler_view);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(mdapter);
        recyclerView.addLoadMore(() -> {
            currentPage++;
            mPresenter.onViewPrepared(currentPage);
            return null;
        });
        setupPullToRefresh(view);

    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void onError(String message) {
        showDialog(message, null);
        recyclerView.setLoading(false);
    }

    @Override
    public void updateOrderTran(List<CongNoModel> orderTrans) {
        recyclerView.setLoading(false);
        recyclerView.setVisibility(View.VISIBLE);
        if (currentPage == Constants.FIRST_PAGE) {
            mOrderTrans.clear();
        }
        mOrderTrans.addAll(orderTrans);
        if (orderTrans.size() < Constants.LIMIT) {
            recyclerView.setEndOfPage(true);
        }
        updateOrderTranUI(mOrderTrans);
    }

    @Override
    public void reloadData() {
        if (mPresenter != null && mOrderTrans.isEmpty()) {
            mPresenter.onViewPrepared(currentPage);
        }
    }

    private void updateOrderTranUI(List<CongNoModel> orderTrans) {
        mdapter.addItems(orderTrans);

        if (orderTrans.size() <= 0) {
            sumaryContainer.setVisibility(View.GONE);
            return;
        }
        sumaryContainer.setVisibility(View.VISIBLE);
        int sumMustCollect = 0;
        int sumAlreadyCollect = 0;
        for (CongNoModel congNoModel : orderTrans) {
            sumMustCollect += congNoModel.getCongNoDonHang();
            sumAlreadyCollect += congNoModel.getTongTienDaThu();
        }
        txtSumaryMustCollect.setText(Utils.formatValue(sumMustCollect, Enum.FieldValueType.CURRENCY));
        txtSumaryAlreadyCollect.setText(Utils.formatValue(sumAlreadyCollect, Enum.FieldValueType.CURRENCY));
        String summaryOrder = orderTrans.size() + " " + getString(R.string.common_order);
        txtSumaryOrder.setText(summaryOrder);
    }


    @Override
    public void onRefresh() {
        currentPage = Constants.FIRST_PAGE;
        recyclerView.setEndOfPage(false);
        mSwipeRefreshLayout.setRefreshing(false);
        mPresenter.onViewPrepared(currentPage);
    }

    private void setupPullToRefresh(View view) {
        // SwipeRefreshLayout
        mSwipeRefreshLayout = view.findViewById(R.id.swipe_container_request_debt);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.filter_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_filter) {
            showFilterScreen();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showFilterScreen() {
        if (mOrderTrans != null && !mOrderTrans.isEmpty()) {
            Set<Integer> searchEmployeeIds = new HashSet<>();
            ArrayList<SearchSimpleItem> searchSimpleItems = new ArrayList<>();
            for (CongNoModel orderTran : mOrderTrans) {
                boolean added = searchEmployeeIds.add(orderTran.getIDThanhVien());
                if (added) {
                    int id = orderTran.getIDThanhVien();
                    String name = orderTran.getTenThanhVien();
                    if (id == 0) {
                        id = SearchSimpleActivity.ID_NULL;
                    }
                    searchSimpleItems.add(new SearchSimpleItem(id, name));
                }
            }

            if (!searchSimpleItems.isEmpty()) {
                Intent intent = new Intent(getActivity(), SearchSimpleActivity.class);
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList(SearchSimpleActivity.NAME_PARCELABLE_ARRAYLIST, searchSimpleItems);
                intent.putExtras(bundle);
                startActivityForResult(intent, SearchSimpleActivity.REQUEST_CODE_SELECT_CUS_TAB3);
            } else {
                Toast.makeText(getActivity(), "Danh sách lọc bị trống!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getActivity(), "Danh sách lọc bị trống!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SearchSimpleActivity.REQUEST_CODE_SELECT_CUS_TAB3) {
            if (resultCode == Activity.RESULT_OK) {
                int id = data.getIntExtra(SearchSimpleActivity.KEY_ID, SearchSimpleActivity.ID_NULL);
                ArrayList<CongNoModel> searchOrderTran = new ArrayList<>();
                for (CongNoModel orderTran : mOrderTrans) {
                    if (SearchSimpleActivity.ID_NULL == id) {
                        if (orderTran.getIDThanhVien() == 0) {
                            searchOrderTran.add(orderTran);
                        }
                    } else if (orderTran.getIDThanhVien() == id) {
                        searchOrderTran.add(orderTran);
                    }
                }
                updateOrderTranUI(searchOrderTran);

            } else if (resultCode == Activity.RESULT_CANCELED) {
                updateOrderTranUI(mOrderTrans);
            }
        }
    }
}
