
package com.anphuocthai.staff.ui.home.statistics.model.updatedynamic;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PerformStatistic {

    @SerializedName("nhanDe")
    @Expose
    private String nhanDe;
    @SerializedName("hienThi")
    @Expose
    private Boolean hienThi;
    @SerializedName("colColor")
    @Expose
    private String colColor;
    @SerializedName("items")
    @Expose
    private List<Item> items = null;
    @SerializedName("colNames")
    @Expose
    private List<String> colNames = null;
    @SerializedName("type")
    private int type;

    public String getNhanDe() {
        return nhanDe;
    }

    public void setNhanDe(String nhanDe) {
        this.nhanDe = nhanDe;
    }

    public Boolean getHienThi() {
        return hienThi;
    }

    public void setHienThi(Boolean hienThi) {
        this.hienThi = hienThi;
    }

    public String getColColor() {
        return colColor;
    }

    public void setColColor(String colColor) {
        this.colColor = colColor;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public List<String> getColNames() {
        return colNames;
    }

    public void setColNames(List<String> colNames) {
        this.colNames = colNames;
    }

    public int getType() {
        return type;
    }
}
