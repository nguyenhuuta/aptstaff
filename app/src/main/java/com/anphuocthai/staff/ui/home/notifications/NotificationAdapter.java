package com.anphuocthai.staff.ui.home.notifications;

import android.graphics.Typeface;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.base.IBaseCallback;
import com.anphuocthai.staff.ui.base.BaseViewHolder;
import com.anphuocthai.staff.ui.home.notifications.model.Notification;
import com.anphuocthai.staff.utils.Utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.text.format.DateUtils.MINUTE_IN_MILLIS;


public class NotificationAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private static final String TAG = NotificationAdapter.class.getSimpleName();

    private static final int VIEW_TYPE_NORMAL = -1;

    private static final int VIEW_TYPE_EMPTY = 0;

    private ArrayList<Notification> notifications;

    private IBaseCallback<Notification> mCallback;

    public NotificationAdapter(IBaseCallback<Notification> callback) {
        this.notifications = new ArrayList<>();
        this.mCallback = callback;
    }

    public void addItems(List<Notification> notifications) {
        this.notifications.addAll(notifications);
        notifyDataSetChanged();
    }

    public void resetData() {
        this.notifications.clear();
        notifyDataSetChanged();
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_EMPTY:
                return new EmptyViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_view, parent, false));
            case VIEW_TYPE_NORMAL:
                return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_item_layout, parent, false));
            default:
                return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_demo, parent, false)) {
                    @Override
                    protected void clear() {
                    }
                };
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemCount() {
        if (notifications != null && notifications.size() > 0) {
            return notifications.size();
        }
        return 1;
    }


    @Override
    public int getItemViewType(int position) {
        if (notifications != null && notifications.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    public class ViewHolder extends BaseViewHolder {
        private TextView title;
        private TextView content;
        private TextView txtTime;

        public ViewHolder(View v) {
            super(v);
            title = itemView.findViewById(R.id.txt_noti_title);
            content = itemView.findViewById(R.id.txt_noti_content);
            txtTime = itemView.findViewById(R.id.txt_noti_time);
            v.setOnClickListener(view -> mCallback.onCallback(notifications.get(getCurrentPosition())));
        }

        @Override
        public void onBind(int position) {
            super.onBind(position);
            if (!notifications.get(position).getIsSeen()) {
                title.setTypeface(title.getTypeface(), Typeface.BOLD);
            }
            title.setText(notifications.get(position).getNhanDe());
            content.setText(notifications.get(position).getNoiDung());

            long time = Utils.getDateInMillis(notifications.get(position).getNgayTao());

            String timeAgo = DateUtils.getRelativeTimeSpanString(time, Calendar.getInstance().getTimeInMillis(), MINUTE_IN_MILLIS, 1).toString();
            txtTime.setText(timeAgo);


        }

        @Override
        protected void clear() {
        }
    }


    public class EmptyViewHolder extends BaseViewHolder {

        @BindView(R.id.btn_retry)
        Button retryButton;

        @BindView(R.id.tv_message)
        TextView messageTextView;

        @BindView(R.id.txt_retry)
        TextView txtRetry;

        public EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            retryButton.setVisibility(View.GONE);
            txtRetry.setVisibility(View.GONE);
        }

        @Override
        protected void clear() {

        }

        @OnClick(R.id.btn_retry)
        void onRetryClick() {
        }
    }
}
