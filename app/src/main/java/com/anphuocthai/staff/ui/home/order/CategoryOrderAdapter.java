package com.anphuocthai.staff.ui.home.order;

import android.content.Context;
import android.graphics.PorterDuff;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.interfaces.ItemClickListener;
import com.anphuocthai.staff.model.order.ProductCategory;
import com.anphuocthai.staff.utils.Constants;
import com.anphuocthai.staff.utils.StringUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static com.anphuocthai.staff.api.ApiURL.IMAGE_VIEW_URL;

public class CategoryOrderAdapter extends RecyclerView.Adapter<CategoryOrderAdapter.ItemCategoryViewHolder> {

    private ArrayList<ProductCategory> arrayList;
    private Context context;

    private ArrayList<Integer> numberProductInCategory;

    private ICategoryCallback mCallback;

    private int mLastSelected = 0;
    private int mItemSize;

    CategoryOrderAdapter(ArrayList<ProductCategory> arrayList, Context context, int itemSize, ICategoryCallback categoryCallback) {
        this.arrayList = arrayList;
        this.context = context;
        this.mCallback = categoryCallback;
        this.mItemSize = itemSize;
    }

    @Override
    public CategoryOrderAdapter.ItemCategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product_category, parent, false);
        CategoryOrderAdapter.ItemCategoryViewHolder vh = new CategoryOrderAdapter.ItemCategoryViewHolder(view);
        vh.setItemClickListener((view1, position, isLongClick) -> {
            if (mLastSelected == position) return;
            ProductCategory lastProduct = arrayList.get(mLastSelected);
            lastProduct.setSelected(false);
            ProductCategory productCategory = arrayList.get(position);
            productCategory.setSelected(true);
            mCallback.onItemClick(productCategory.getId());
            notifyItemChanged(mLastSelected);
            notifyItemChanged(position);
            mLastSelected = position;
        });
        return vh;
    }

    @Override
    public void onBindViewHolder(CategoryOrderAdapter.ItemCategoryViewHolder holder, int position) {
        ProductCategory category = arrayList.get(position);
        int numberNew = category.getNumberItemNew();
        if (category.isNewCategory() || numberNew > 0) {
            holder.mNumberNew.setVisibility(View.VISIBLE);
            if (category.getNumberItemNew() > 0) {
                holder.mNumberNew.setText(String.valueOf(numberNew));
            } else {
                holder.mNumberNew.setText(Constants.EMPTY);
            }
        } else {
            holder.mNumberNew.setVisibility(View.GONE);
        }
        if (category.isNewCategory()) {
            mCallback.onUpdateCategoryToOld(category.getId());
        }


        String categoryName = StringUtils.toLowerCase(category.getName());
        if (category.getNumberProductSelected() > 0) {
            categoryName += " (" + category.getNumberProductSelected() + ")";
        }
        holder.txtCategoryName.setText(categoryName);
//        try {
//            if (numberProductInCategory != null) {
//                if (numberProductInCategory.size() > 0) {
//                    if (numberProductInCategory.get(position) > 0) {
//                        holder.txtCategoryName.setText(StringUtils.toLowerCase(category.getName()) + "(" + numberProductInCategory.get(position) + ")");
//                    }
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


        Picasso.get()
                .load(IMAGE_VIEW_URL + category.getCategoryThumb())
                //.load(R.drawable.ic_cua)
                .placeholder(R.drawable.ic_no_image)
                .fit()
                .centerCrop()
                .into(holder.imgProductCategory);
        if (category.isSelected()) {
            holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.white));
            holder.imgProductCategory.setColorFilter(context.getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP);
            holder.txtCategoryName.setTextColor(context.getResources().getColor(R.color.colorAccent));
        } else {
            holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.lowgray));
            holder.imgProductCategory.setColorFilter(context.getResources().getColor(R.color.graytext), PorterDuff.Mode.SRC_ATOP);
            holder.txtCategoryName.setTextColor(context.getResources().getColor(R.color.graytext));
        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    class ItemCategoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        private ImageView imgProductCategory;
        private TextView txtCategoryName;
        private TextView mNumberNew;
        private ItemClickListener itemClickListener;

        public ItemCategoryViewHolder(View itemView) {
            super(itemView);
            imgProductCategory = itemView.findViewById(R.id.order_img_product_category);
            txtCategoryName = itemView.findViewById(R.id.order_txt_product_category);
            mNumberNew = itemView.findViewById(R.id.numberNew);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);

            ViewGroup.LayoutParams layoutParams = itemView.getLayoutParams();
            layoutParams.width = mItemSize;
            layoutParams.height = mItemSize;
            itemView.setLayoutParams(layoutParams);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                itemClickListener.onClick(v, getAdapterPosition(), false);
            }
        }

        @Override
        public boolean onLongClick(View v) {
            int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                itemClickListener.onClick(v, getAdapterPosition(), true); // Gọi interface , true là vì đây là onLongClick
            }
            return true;
        }

        public void setItemClickListener(ItemClickListener itemClickListener) {
            this.itemClickListener = itemClickListener;
        }
    }


    public interface ICategoryCallback {
        void onItemClick(int categoryId);

        void onUpdateCategoryToOld(int categoryId);

    }

    public void clearData() {
        mLastSelected = 0;
    }
}
