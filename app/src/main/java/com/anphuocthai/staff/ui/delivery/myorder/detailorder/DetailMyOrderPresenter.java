package com.anphuocthai.staff.ui.delivery.myorder.detailorder;

import com.androidnetworking.error.ANError;
import com.anphuocthai.staff.api.ApiURL;
import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.data.DataManager;
import com.anphuocthai.staff.interfaces.IAPICallback;
import com.anphuocthai.staff.ui.base.BasePresenter;
import com.anphuocthai.staff.utils.App;
import com.anphuocthai.staff.utils.rx.SchedulerProvider;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class DetailMyOrderPresenter<V extends DetailMyOrderMvpView> extends BasePresenter<V> implements DetailMyOrderMvpPresenter<V> {

    @Inject
    public DetailMyOrderPresenter(NetworkManager networkManager, DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(networkManager, dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void getDetailOrder(String sessionID) {
        getmMvpView().showLoading();
        HashMap<String, String> hashMap = new HashMap();
        hashMap.put("sessionID", sessionID);
        hashMap.put("BoPhanID", String.valueOf(App.getInstance().getDepartmentId()));

        getmNetworkManager().sendGetRequest(ApiURL.GET_DETAIL_ORDER_URL, hashMap, new IAPICallback() {
            @Override
            public void onResponseSuccess(JSONObject response) {

            }

            @Override
            public void onResponseSuccess(JSONArray response) {
                if (response != null) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<ArrayList<DetailOrder>>() {
                    }.getType();
                    ArrayList<DetailOrder> detailOrders = gson.fromJson(response.toString(), type);

                    if (!isViewAttached()) {
                        return;
                    }
                    getmMvpView().createAllTab(detailOrders);
                    getmMvpView().hideLoading();
                }
            }

            @Override
            public void onResponseError(ANError anError) {

                if (!isViewAttached()) {
                    return;
                }
                handleApiError(anError);
                getmMvpView().hideLoading();
            }
        });
    }
}
