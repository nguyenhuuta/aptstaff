package com.anphuocthai.staff.ui.delivery.debt.inventorydebt

/**
 * Created by OpenYourEyes on 10/21/2020
/ */
//class CongNoAdapter(val context: Context, val orderTranArrayList: MutableList<CongNoModel>) : RecyclerView.Adapter<BaseViewHolder>() {
//    private var mCallback: Callback? = null
//    private val mListOrigin: MutableList<CongNoModel> = mutableListOf()
//    fun setCallback(mCallback: Callback?) {
//        this.mCallback = mCallback
//    }
//
//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
//        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_cong_no_ton, parent, false))
//    }
//
//    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
//        holder.onBind(position)
//    }
//
//    override fun getItemCount(): Int {
//        return orderTranArrayList.size
//    }
//
//    interface Callback {
//        fun onItemSelected()
//    }
//
//    fun addItems(orderTrans: MutableList<CongNoModel>) {
//        orderTranArrayList.clear()
//        mListOrigin.clear()
//        orderTranArrayList.addAll(orderTrans)
//        mListOrigin.addAll(orderTrans)
//        notifyDataSetChanged()
//    }

//    fun onFilterMonth(month: String?) {
//        if (Utils.isNull(mListOrigin)) return
//        orderTranArrayList.clear()
//        if (month == null) {
//            orderTranArrayList.addAll(mListOrigin)
//        } else {
//            var time: String
//            for (orderTran in mListOrigin) {
//                if (orderTran.maDonDat != null) {
//                    time = orderTran.ngayKetThuc ?: run {
//                        orderTran.ngayBatDau ?: ""
//                    }
//                    if (time.contains(month)) {
//                        orderTranArrayList.add(orderTran)
//                    }
//                }
//            }
//        }
//        notifyDataSetChanged()
//    }
//
//    fun unSelectedAll() {
//        for (orderTran in orderTranArrayList) {
//            orderTran.selected = false
//        }
//        notifyDataSetChanged()
//    }
//
//    fun selectedAll() {
//        for (orderTran in orderTranArrayList) {
//            orderTran.selected = true
//        }
//        notifyDataSetChanged()
//    }
//
//    inner class ViewHolder(itemView: View) : BaseViewHolder(itemView) {
//        lateinit var mCheckBox: CheckBox
//        lateinit var txtOrderCode: TextView
//        lateinit var txtCustomerName: TextView
//        lateinit var labelSumaryMoney: TextView
//        lateinit var txtSumaryMoney: TextView
//        lateinit var labelMustPay: TextView
//        lateinit var txtMustPay: TextView
//        lateinit var labelRemain: TextView
//        lateinit var txtRemain: TextView
//        private fun initView(itemView: View) {
//            mCheckBox = itemView.findViewById(R.id.checkBox)
//            txtOrderCode = itemView.findViewById(R.id.all_debt_txt_order_code)
//            txtCustomerName = itemView.findViewById(R.id.all_debt_txt_order_customer_name)
//            labelSumaryMoney = itemView.findViewById(R.id.debt_label_sumary_money)
//            txtSumaryMoney = itemView.findViewById(R.id.debt_txt_sumary_money)
//            labelMustPay = itemView.findViewById(R.id.debt_label_must_pay)
//            txtMustPay = itemView.findViewById(R.id.debt_txt_must_pay)
//            labelRemain = itemView.findViewById(R.id.debt_label_remain)
//            txtRemain = itemView.findViewById(R.id.debt_txt_remain)
//        }
//
//        override fun clear() {}
//        override fun onBind(position: Int) {
//            super.onBind(position)
//            val congNoModel = orderTranArrayList[position]
//            mCheckBox.isChecked = congNoModel.selected
//            txtOrderCode.text = congNoModel.maDonDat?.let {
//                val date = (congNoModel.ngayKetThuc
//                        ?: congNoModel.ngayBatDau)?.convertDate(from = DateType.FUll3, to = DateType.ONLY_DATE)
//                "ĐH số : $it - ${date ?: ""}"
//            } ?: ""
//
//            txtCustomerName.text = congNoModel.getContentDetail()
//            txtSumaryMoney.text = ""
//            if (congNoModel.tongGiaTri != null) {
//                txtSumaryMoney.text = Utils.formatValue(congNoModel.tongGiaTri!!.toDouble(), FieldValueType.CURRENCY)
//            }
//            txtMustPay.text = ""
//            if (congNoModel.daThanhToan != null) {
//                txtMustPay.text = Utils.formatValue(congNoModel.daThanhToan!!.toDouble(), FieldValueType.CURRENCY)
//            }
//            txtRemain.text = ""
//            if (congNoModel.phaiThanhToan != null) {
//                txtRemain.text = Utils.formatValue(congNoModel.phaiThanhToan!!.toDouble(), FieldValueType.CURRENCY)
//            }
//        }
//
//        init {
//            initView(itemView)
//            itemView.setOnClickListener { v: View? ->
//                val orderTran = orderTranArrayList[currentPosition]
//                orderTran.selected = !orderTran.selected
//                mCheckBox.isChecked = orderTran.selected
//                mCallback?.onItemSelected()
//            }
//        }
//    }
//
//    inner class EmptyViewHolder(itemView: View?) : BaseViewHolder(itemView) {
//        @BindView(R.id.btn_retry)
//        var retryButton: Button? = null
//
//        @BindView(R.id.tv_message)
//        var messageTextView: TextView? = null
//
//        @BindView(R.id.txt_retry)
//        var txtRetry: TextView? = null
//        override fun clear() {}
//
//        @OnClick(R.id.btn_retry)
//        fun onRetryClick() {
////            if (mCallback != null)
////                mCallback.onBlogEmptyViewRetryClick();
//        }
//
//        init {
//            ButterKnife.bind(this, itemView!!)
//            retryButton!!.visibility = View.GONE
//            txtRetry!!.visibility = View.GONE
//        }
//    }
//}