package com.anphuocthai.staff.ui.home.statistics;

import com.anphuocthai.staff.ui.base.MvpPresenter;
import com.anphuocthai.staff.ui.home.statistics.viewholder.DailyCVViewHolder;

public interface StatisticMvpPresenter<V extends StatisticMvpView> extends MvpPresenter<V> {


    void getDynamicReport();

    void openMyOrderShortcut();

    void openDebtManagement();

    void openCustomerShortcut();

    void openTransportShortcut();

    void openDailyWork();

    void getStatisticPerform(DailyCVViewHolder viewHolder);


}
