package com.anphuocthai.staff.ui.delivery.debt.waitpaydebt;

import com.anphuocthai.staff.ui.base.MvpPresenter;

public interface WaitPayDebtMvpPresenter<V extends WaitPayDebtMvpView> extends MvpPresenter<V> {
    void onViewPrepared(int page);
}
