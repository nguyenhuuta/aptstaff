package com.anphuocthai.staff.ui.home.statistics.viewholder;

import android.graphics.Color;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.ui.base.BaseViewHolder;
import com.anphuocthai.staff.ui.home.statistics.StatisticAdapter;
import com.anphuocthai.staff.ui.home.statistics.model.DailyCV;
import com.anphuocthai.staff.ui.home.statistics.model.updatedynamic.PerformStatistic;
import com.anphuocthai.staff.utils.ColorUtils;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.util.ArrayList;
import java.util.List;

public class DailyCVViewHolder extends BaseViewHolder {

    private TextView mTextView;

    private BarChart barChart;

    private StatisticAdapter adapter;

    private int position;


    private View itemView;

    private PerformStatistic performStatistic;

    public DailyCVViewHolder(View itemView, StatisticAdapter adapter) {
        super(itemView);
        this.itemView = itemView;
        setVisibility(false);
        setAdapter(adapter);
        mTextView = itemView.findViewById(R.id.layout_item_demo_title);
        mTextView.setBackgroundColor(ColorUtils.getRandomMaterialColor());
        barChart = itemView.findViewById(R.id.barchart);
        adapter.getmPresenter().getStatisticPerform(this);
    }

    @Override
    public void onBind(int position) {
        super.onBind(position);
        this.position = position;
        mTextView.setText(R.string.statistic_cv_daily);

        createBarChart(this.performStatistic);


    }

    public void setVisibility(boolean isVisible) {
        RecyclerView.LayoutParams param = (RecyclerView.LayoutParams) itemView.getLayoutParams();
        if (isVisible) {
            param.height = LinearLayout.LayoutParams.WRAP_CONTENT;
            param.width = LinearLayout.LayoutParams.MATCH_PARENT;
            itemView.setVisibility(View.VISIBLE);
        } else {
            itemView.setVisibility(View.GONE);
            param.height = 0;
            param.width = 0;
        }
        itemView.setLayoutParams(param);
    }


    @Override
    protected void clear() {
    }


    public void setAdapter(StatisticAdapter adapter) {
        this.adapter = adapter;
    }

    public void update(ArrayList<DailyCV> dailyCVArrayList) {
        adapter.notifyItemChanged(position);
    }

    public void updatePerformStatistic(PerformStatistic performStatistic) {
        this.performStatistic = performStatistic;
        if (!performStatistic.getHienThi()) {
            setVisibility(false);
            return;
        }
        setVisibility(true);
        adapter.notifyItemChanged(position);
    }

    private void createBarChart(PerformStatistic performStatistic) {

        if (performStatistic == null || performStatistic.getItems() == null) {
            return;
        }

        if (performStatistic.getItems().size() <= 0) {
            return;
        }

        if (!performStatistic.getHienThi()) {

        }

        List<BarEntry> entries = new ArrayList<>();


        mTextView.setText(performStatistic.getNhanDe());


        for (int i = 0; i < performStatistic.getItems().size(); i++) {
            entries.add(new BarEntry(i, performStatistic.getItems().get(i).getGiaTri1()));
        }


        BarDataSet set = new BarDataSet(entries, "");
        set.setColor(Color.parseColor(performStatistic.getColColor()));
        BarData data = new BarData(set);
        data.setBarWidth(0.7f); // set custom bar width

        barChart.setData(data);
        barChart.setFitBars(true); // make the x-axis fit exactly all bars


        final ArrayList<String> xAxisLabel = new ArrayList<>();
        for (int j = 0; j < performStatistic.getItems().size(); j++) {
            xAxisLabel.add(performStatistic.getItems().get(j).getNhan());
        }

        XAxis xAxis = barChart.getXAxis();

        YAxis axisRight = barChart.getAxisRight();

        // hide right bar
        axisRight.setDrawLabels(false);
        //axisRight.setDrawAxisLine(false);

        xAxis.setLabelCount(performStatistic.getItems().size());
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return xAxisLabel.get((int) value);
            }
        });
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        Description description = new Description();
        //description.setText(adapter.getContext().getString(R.string.chart_cv_daily));
        description.setText("");
        barChart.setDescription(description);

        // hide label dataset
        barChart.getLegend().setEnabled(false);
        barChart.setTouchEnabled(false);

        barChart.getXAxis().setDrawGridLines(false); // disable grid lines for the XAxis
        //barChart.getAxisLeft().setDrawGridLines(false); // disable grid lines for the left YAxis
        barChart.getAxisRight().setDrawGridLines(false); // disable grid lines for the right YAxis

        barChart.invalidate(); // refresh


    }


}
