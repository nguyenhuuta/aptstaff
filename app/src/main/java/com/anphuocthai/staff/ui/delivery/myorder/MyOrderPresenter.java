package com.anphuocthai.staff.ui.delivery.myorder;

import com.androidnetworking.error.ANError;
import com.anphuocthai.staff.R;
import com.anphuocthai.staff.api.ApiURL;
import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.data.DataManager;
import com.anphuocthai.staff.data.db.model.OrderProduct;
import com.anphuocthai.staff.interfaces.IAPICallback;
import com.anphuocthai.staff.model.ResponseMessage;
import com.anphuocthai.staff.ui.base.BasePresenter;
import com.anphuocthai.staff.ui.delivery.model.Hanghoa;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;
import com.anphuocthai.staff.utils.App;
import com.anphuocthai.staff.utils.rx.SchedulerProvider;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.rx2androidnetworking.Rx2AndroidNetworking;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;


public class MyOrderPresenter<V extends MyOrderMvpView> extends BasePresenter<V> implements MyOrderMvpPresenter<V> {

    private static final String TAG = MyOrderPresenter.class.getSimpleName();


    private JSONObject createJSONObject(final int page) {
        JSONObject notTranObject = new JSONObject();
        try {
            notTranObject.put("phanLoai", 0);
            notTranObject.put("guidThanhVien", "");
            notTranObject.put("sessionID", "");
            notTranObject.put("tuNgay", "");
            notTranObject.put("denNgay", "");
            notTranObject.put("trangThaiIds", "");
            notTranObject.put("pageIndex", page);
            notTranObject.put("pageSize", 20);
            notTranObject.put("BoPhanID", App.getInstance().getDepartmentId());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return notTranObject;
    }

    @Inject
    public MyOrderPresenter(NetworkManager networkManager,
                            DataManager dataManager,
                            SchedulerProvider schedulerProvider,
                            CompositeDisposable compositeDisposable) {
        super(networkManager,
                dataManager,
                schedulerProvider,
                compositeDisposable);
    }

    @Override
    public void onGetAllMyOrder() {
        //getmMvpView().showLoading();
        JSONObject notTranObject = new JSONObject();
        try {
            notTranObject.put("phanLoai", 0);
            notTranObject.put("guidThanhVien", "");
            notTranObject.put("sessionID", "");
            notTranObject.put("tuNgay", "");
            notTranObject.put("denNgay", "");
            notTranObject.put("trangThaiIds", "");
            notTranObject.put("pageIndex", 1);
            notTranObject.put("pageSize", 100);
            notTranObject.put("BoPhanID", App.getInstance().getDepartmentId());

        } catch (Exception e) {
            e.printStackTrace();
        }

        getmNetworkManager().sendPostRequestWithArrayResponse(ApiURL.GET_ORDER_URL, notTranObject, new IAPICallback() {
            @Override
            public void onResponseSuccess(JSONObject response) {}
            @Override
            public void onResponseSuccess(JSONArray response) {
                if (response != null) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<ArrayList<OrderTran>>() {
                    }.getType();
                    try {
                        ArrayList<OrderTran> orderTrans = gson.fromJson(response.toString(), type);
                        if(!isViewAttached()) {
                            return;
                        }
                        getmMvpView().displayOrderTrans(orderTrans);
                    }catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onResponseError(ANError anError) {
                //getmMvpView().hideLoading();
            }
        });
    }



    @Override
    public void hideRecyclerView(boolean isHide) {
        getmMvpView().hideRecyclerView(isHide);
    }

    @Override
    public Flowable<List<OrderTran>> rxJavaOnGetAllMyOrder(final int page) {
        return Rx2AndroidNetworking.post(ApiURL.GET_ORDER_URL)
                .addHeaders(ApiURL.getBaseHeader())
                .addHeaders(ApiURL.getTokenHeader())
                .addJSONObjectBody(createJSONObject(page))
                .build()
                .getObjectListFlowable(OrderTran.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public void cancelOrderTran(OrderTran orderTran) {

        JSONObject cancelOrder = new JSONObject();
        try {
            cancelOrder.put("sessionID", orderTran.getSessionID());
            cancelOrder.put("ghiChu", "Test");
        }catch (Exception e) {
            e.printStackTrace();
        }

        getmNetworkManager().sendPostRequest(ApiURL.DEL_ORDER_URL, cancelOrder, new IAPICallback() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                if (response != null) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<ResponseMessage>() {
                    }.getType();
                    ResponseMessage message = gson.fromJson(response.toString(), type);

                    if (!isViewAttached()) {
                        return;
                    }
                    getmMvpView().showMessage(message.getMessage());
                    getmMvpView().resetLoadMoreData(orderTran);
                }
            }

            @Override
            public void onResponseSuccess(JSONArray response) {

            }

            @Override
            public void onResponseError(ANError anError) {

            }
        });


    }

    @Override
    public void showAdjustDialog(OrderTran orderTran) {
        getmMvpView().showAdjustDialog(orderTran);
    }

    @Override
    public void editOrderStepCancelOrder(OrderTran orderTran) {
        JSONObject cancelOrder = new JSONObject();
        try {
            cancelOrder.put("sessionID", orderTran.getSessionID());
            cancelOrder.put("ghiChu", "Test");
        }catch (Exception e) {
            e.printStackTrace();
        }

        getmNetworkManager().sendPostRequest(ApiURL.DEL_ORDER_URL, cancelOrder, new IAPICallback() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                if (response != null) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<ResponseMessage>() {
                    }.getType();

                    if (!isViewAttached()) {
                        return;
                    }
                    ResponseMessage message = gson.fromJson(response.toString(), type);
                    //getmMvpView().showMessage(message.getMessage());
                    onGetAllMyOrder();
                    getmMvpView().onCancelToEditOrderSuccess(message.getMessage(), orderTran);
                }
            }

            @Override
            public void onResponseSuccess(JSONArray response) {

            }

            @Override
            public void onResponseError(ANError anError) {

            }
        });
    }

    @Override
    public void onDeleteAllOrderCart(OrderTran orderTran) {
        getCompositeDisposable().add(getDataManager()
                .deleteAllOrderProduct()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<Boolean>() {
                    @Override
                    public void accept(Boolean aBoolean) throws Exception {
                        // DELETE customer
                        getDataManager().setCurrentCustomerGuid(null);
                        getDataManager().setCurrentCustomerName(null);

                        getmMvpView().onDeleteCartSuccess(orderTran);

                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {

                    }
                }));
    }

    @Override
    public void insertProductToCart(OrderTran orderTran) {
        List<Hanghoa> hanghoaList = new ArrayList<>();
        hanghoaList = orderTran.getHanghoas();
        List<OrderProduct> orderProducts = new ArrayList<>();
        for (Hanghoa hanghoa: hanghoaList) {
            orderProducts.add(hanghoa.toOrderProduct("", ""));
        }

        getCompositeDisposable().add(getDataManager()
                .insertOrderProduct(orderProducts)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<Boolean>() {
                    @Override
                    public void accept(Boolean aBoolean) throws Exception {
                        try {
                            getDataManager().setCurrentCustomerGuid(orderTran.getGuidThanhVien());
                            getDataManager().setCurrentCustomerName(orderTran.getTenThanhVien());
                            getDataManager().setPayMethod(orderTran.getDDThanhToanID());
                        }catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (!isViewAttached()) {
                            return;
                        }
                        getmMvpView().onCreateProductSuccess(orderTran);

                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        getmMvpView().onError(R.string.have_error);
                    }
                }));
    }
}
