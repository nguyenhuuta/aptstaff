package com.anphuocthai.staff.ui.delivery.nottransportyet;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.ui.base.BaseActivity;
import com.anphuocthai.staff.ui.base.ViewPagerAdapter;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;
import com.anphuocthai.staff.ui.delivery.nottransportyet.reporthtml.ReportReceiveOrderFragment;
import com.anphuocthai.staff.ui.delivery.nottransportyet.requestreceiverorder.ReceiverOrderFragment;
import com.anphuocthai.staff.ui.delivery.nottransportyet.trachungtu.TraChungTuThanhToanFragment;
import com.anphuocthai.staff.ui.delivery.nottransportyet.transportorder.TransportOrderFragment;

import org.reactivestreams.Publisher;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.processors.PublishProcessor;
import io.reactivex.schedulers.Schedulers;

public class NotTranActivity extends BaseActivity implements NotTranMvpView {

    private static final String TAG = NotTranActivity.class.getSimpleName();

    @Inject
    NotTranMvpPresenter<NotTranMvpView> mPresenter;

    @Inject
    NotTranAdapter mNotTranAdapter;

    @Inject
    LinearLayoutManager mLayoutManager;


    private ViewPager viewPager;


    private RecyclerView recyclerView;

    private ProgressBar progressBar;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private PublishProcessor<Integer> paginator = PublishProcessor.create();

    private boolean loading = false;
    private int pageNumber = 1;
    private final int VISIBLE_THRESHOLD = 1;
    private int lastVisibleItem, totalItemCount;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_not_tran);

        getmActivityComponent().inject(this);
        mPresenter.onAttach(this);
        mNotTranAdapter.setmPresenter(mPresenter);
        mNotTranAdapter.setBaseActivity(this);
        setupViewPager();
        initView();

    }

    protected void setupViewPager() {
        viewPager = findViewById(R.id.view_pager);
        setupToobarWithTab(getString(R.string.order_not_tran_toolbar_title), viewPager, false);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(ReceiverOrderFragment.newInstance(), getString(R.string.order_tran_not_tran_title));
        adapter.addFragment(TransportOrderFragment.newInstance(), getString(R.string.order_transport));
        adapter.addFragment(new TraChungTuThanhToanFragment(), getString(R.string.giao_tra_hang));
        adapter.addFragment(ReportReceiveOrderFragment.newInstance(), getString(R.string.debt_customer));
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(4);
    }

    @Override
    protected void onDestroy() {
        mPresenter.onDetach();
        super.onDestroy();
    }

    private void initView() {


    }

    /**
     * setting listener to get callback for load more
     */
    private void setUpLoadMoreListener() {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView,
                                   int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = mLayoutManager.getItemCount();
                lastVisibleItem = mLayoutManager
                        .findLastVisibleItemPosition();
                if (!loading
                        && totalItemCount <= (lastVisibleItem + VISIBLE_THRESHOLD)) {
                    pageNumber++;
                    paginator.onNext(pageNumber);
                    loading = true;
                }
            }
        });
    }


    /**
     * subscribing for data
     */
    private void subscribeForData() {
        Disposable disposable = paginator
                .onBackpressureDrop()
                .concatMap(new Function<Integer, Publisher<List<OrderTran>>>() {
                    @Override
                    public Publisher<List<OrderTran>> apply(@NonNull Integer page) throws Exception {
                        loading = true;
                        progressBar.setVisibility(View.VISIBLE);
                        return mPresenter.rxJavaOnGetAllNotTranOrder(page);
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(items -> {
                    mPresenter.hideRecyclerView(false);
                    mNotTranAdapter.addItems(items);
                    mNotTranAdapter.notifyDataSetChanged();
                    loading = false;
                    progressBar.setVisibility(View.GONE);
                }, throwable -> {

                });

        compositeDisposable.add(disposable);
        paginator.onNext(pageNumber);

    }

    public static Intent getNotTranIntent(Context context) {
        return new Intent(context, NotTranActivity.class);
    }

    @Override
    public void displayOrderTrans(ArrayList<OrderTran> orderTrans) {
        mNotTranAdapter.addItems(orderTrans);
        mPresenter.hideRecyclerView(false);
    }

    @Override
    public void hideRecyclerView(boolean isHide) {
        if (isHide) {
            recyclerView.setVisibility(View.INVISIBLE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

}
