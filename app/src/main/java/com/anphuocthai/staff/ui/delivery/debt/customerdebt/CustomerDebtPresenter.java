package com.anphuocthai.staff.ui.delivery.debt.customerdebt;

import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.baserequest.APICallback;
import com.anphuocthai.staff.baserequest.APIService;
import com.anphuocthai.staff.data.DataManager;
import com.anphuocthai.staff.entities.ListReportResponse;
import com.anphuocthai.staff.entities.Report;
import com.anphuocthai.staff.ui.base.BasePresenter;
import com.anphuocthai.staff.utils.Logger;
import com.anphuocthai.staff.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class CustomerDebtPresenter<V extends CustomerDebtMvpView> extends BasePresenter<V> implements CustomerDebtMvpPresenter<V> {

    @Inject
    public CustomerDebtPresenter(NetworkManager networkManager, DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(networkManager, dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void getCustomerDebt(boolean isFirst) {
        Logger.d("getCustomerDebt", "getCustomerDebt");
        if (isFirst && getmMvpView() != null) {
            getmMvpView().showLoading();
        }
        APIService.getInstance().getAppAPI().getListReport(new Report.ReportBody()).getAsyncResponse(new APICallback<ListReportResponse>() {
            @Override
            public void onSuccess(ListReportResponse listReportResponse) {
                if (getmMvpView() == null) return;
                getmMvpView().hideLoading();
                getmMvpView().onGetCustomerDebtSuccess(listReportResponse);
            }

            @Override
            public void onFailure(String errorMessage) {
                if (getmMvpView() == null) return;
                getmMvpView().hideLoading();
                getmMvpView().showMessage(errorMessage);
            }
        });

    }
}
