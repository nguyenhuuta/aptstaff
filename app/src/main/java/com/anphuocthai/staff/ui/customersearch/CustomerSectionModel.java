package com.anphuocthai.staff.ui.customersearch;

import com.anphuocthai.staff.model.customer.SearchCustomer;

import java.util.List;

public class CustomerSectionModel {

    private String sectionLabel;
    private List<SearchCustomer> customerList;

    public CustomerSectionModel(String sectionLabel,
                                List<SearchCustomer> customers) {
        this.sectionLabel = sectionLabel;
        this.customerList = customers;
    }

    public String getSectionLabel() {
        return sectionLabel;
    }

    public List<SearchCustomer> getCustomerList() {
        return customerList;
    }

    public void setSectionLabel(String sectionLabel) {
        this.sectionLabel = sectionLabel;
    }

    public void setCustomerList(List<SearchCustomer> customerList) {
        this.customerList = customerList;
    }
}
