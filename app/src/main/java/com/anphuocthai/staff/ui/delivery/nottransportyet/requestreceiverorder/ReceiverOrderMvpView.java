package com.anphuocthai.staff.ui.delivery.nottransportyet.requestreceiverorder;

import com.anphuocthai.staff.ui.base.MvpView;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;

import java.util.ArrayList;

public interface ReceiverOrderMvpView extends MvpView {
    void displayOrderTrans(ArrayList<OrderTran> orderTrans);
    void hideRecyclerView(boolean isHide);
}
