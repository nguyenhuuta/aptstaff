package com.anphuocthai.staff.ui.delivery.myorder.detailorder;

import com.anphuocthai.staff.ui.base.MvpView;

import java.util.ArrayList;

public interface DetailMyOrderMvpView extends MvpView {

    void createAllTab(ArrayList<DetailOrder> detailOrders);
}
