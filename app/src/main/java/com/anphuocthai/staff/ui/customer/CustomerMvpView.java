package com.anphuocthai.staff.ui.customer;

import android.view.View;

import com.anphuocthai.staff.model.customer.SearchCustomer;
import com.anphuocthai.staff.ui.base.MvpView;

import java.util.ArrayList;
import java.util.List;

public interface CustomerMvpView extends MvpView {
    void updateCustomer(ArrayList<SearchCustomer> customerList);

    void showQuickAction(View view);

    void showBottomSheet(SearchCustomer customer);

    void onCustomerCall(SearchCustomer customer);

    void onCustomerSMS (SearchCustomer customer);

    void onCustomerDetail(SearchCustomer customer);

    void openAddNewWS(SearchCustomer customer);

    void openAddNewQWS(SearchCustomer customer);

    void onSearchCustomerComplele(List<SearchCustomer> customerList);
}
