package com.anphuocthai.staff.ui.home.notifications;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.anphuocthai.staff.api.ApiURL;
import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.data.DataManager;
import com.anphuocthai.staff.interfaces.IAPICallback;
import com.anphuocthai.staff.ui.base.BasePresenter;
import com.anphuocthai.staff.ui.home.notifications.model.Notification;
import com.anphuocthai.staff.utils.rx.SchedulerProvider;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.rx2androidnetworking.Rx2AndroidNetworking;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class NotificationPresenter<V extends NotificationMvpView> extends BasePresenter<V> implements NotificationMvpPresenter<V> {

    private static final String TAG = NotificationPresenter.class.getSimpleName();
    @Inject
    public NotificationPresenter(NetworkManager networkManager, DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(networkManager, dataManager, schedulerProvider, compositeDisposable);
    }


    @Override
    public void onGetAllNotification() {
        HashMap hashMap = new HashMap();
        hashMap.put("maxId", "0");
        getmNetworkManager().sendGetRequest(ApiURL.GET_ALL_NOTIFICATION, hashMap, new IAPICallback() {
            @Override
            public void onResponseSuccess(JSONObject response) {

            }

            @Override
            public void onResponseSuccess(JSONArray response) {
                if (response != null) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<ArrayList<Notification>>() {
                    }.getType();
                    ArrayList<Notification> notifications = gson.fromJson(response.toString(), type);
                    getmMvpView().onGetAllNotificationSuccess(notifications);
                    getmMvpView().onCloseOnRefreshIcon();
              }
            }

            @Override
            public void onResponseError(ANError anError) {
                getmMvpView().onCloseOnRefreshIcon();
            }
        });
    }


    @Override
    public Flowable<List<Notification>> rxJavaOnGetAllNotification(int page) {
        HashMap hashMap = new HashMap();
        hashMap.put("maxId", String.valueOf(page));
        return Rx2AndroidNetworking.get(ApiURL.GET_ALL_NOTIFICATION)
                .addHeaders(ApiURL.getBaseHeader())
                .addHeaders(ApiURL.getTokenHeader())
                .addQueryParameter(hashMap)
                .build()
                .getObjectListFlowable(Notification.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public void getUnReadNotification() {
        AndroidNetworking.get(ApiURL.GET_UNREAD_NOTIFICATION)
                .addHeaders(ApiURL.getBaseHeader())
                .addHeaders(ApiURL.getTokenHeader())
                .addQueryParameter(null)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        if (!isViewAttached()) {
                            return;
                        }
                        getmMvpView().onUpdateNotificationNumberSuccess(response);
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }

}
