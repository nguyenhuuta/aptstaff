package com.anphuocthai.staff.ui.home.statistics.viewholder;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.activities.customer.DetailWorkScheduleActivity;
import com.anphuocthai.staff.activities.customer.ListImageAlreadyCheckInActivity;
import com.anphuocthai.staff.activities.customer.WorkScheduleActivity;
import com.anphuocthai.staff.interfaces.ItemClickListener;
import com.anphuocthai.staff.model.customer.Schedule;
import com.anphuocthai.staff.ui.base.BaseViewHolder;
import com.anphuocthai.staff.utils.ColorUtils;
import com.anphuocthai.staff.utils.Utils;

import java.util.ArrayList;

import static com.anphuocthai.staff.utils.Constants.NUMBER_OF_DAILY_WORK_IN_HOME;

public class DailyWorkAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private static final String TAG = DailyWorkAdapter.class.getSimpleName();

    private ArrayList<Schedule> mDataset = new ArrayList<>();
    private Context context;
    private boolean isCheckin;
    private boolean isShowInHome;

    private boolean isToday;

    private static final int VIEW_TYPE_NORMAL = 0;
    private static final int VIEW_TYPE_VIEW_MORE = 1;


    public DailyWorkAdapter(ArrayList<Schedule> dataset, Context context, boolean isCheckin, boolean isShowInHome, boolean isToday) {
        mDataset.clear();
        mDataset.addAll(dataset);
        this.context = context;
        this.isCheckin = isCheckin;
        this.isShowInHome = isShowInHome;
        this.isToday = isToday;
    }

    public void updateList(ArrayList<Schedule> dataset){
        mDataset.clear();
        mDataset.addAll(dataset);
        notifyDataSetChanged();
    }
    public class ViewHolder extends BaseViewHolder implements View.OnClickListener, View.OnLongClickListener {
        public TextView workScheduleName;
        public TextView workScheduleCustomerName;
        public TextView workScheduleTime;
        public TextView txtWorkScheduleCheckin;

        private TextView txtStateSchedule;
        //public ImageView imgIcon;
        private ItemClickListener itemClickListener;

        public ViewHolder(View v) {
            super(v);
            workScheduleName = (TextView) v.findViewById(R.id.ci_txt_work_schedule_name);
            workScheduleCustomerName = v.findViewById(R.id.ci_txt_work_schedule_customer_name);
            workScheduleTime = v.findViewById(R.id.ci_txt_work_schedule_time);
            txtWorkScheduleCheckin = v.findViewById(R.id.ci_txt_check_in_work_schedule);
            txtStateSchedule = v.findViewById(R.id.txtStateSchedule);
            v.setOnClickListener(this);
            v.setOnLongClickListener(this);
        }

        @Override
        protected void clear() {

        }

        @Override
        public void onBind(int position) {
            super.onBind(position);
            if (position >= mDataset.size()) {
                return;
            }
            workScheduleName.setText(mDataset.get(position).getTen());
            workScheduleCustomerName.setText(mDataset.get(position).getTenThanhVien());
            if (isToday) {
                workScheduleTime.setText(Utils.formatDateStringWithFormat("yyyy-MM-dd HH:mm:ss", "HH:mm", mDataset.get(position).getNgayBatDau()));
            } else {
                workScheduleTime.setText(Utils.formatDateStringWithFormat("yyyy-MM-dd HH:mm:ss", "dd-MM\nHH:mm", mDataset.get(position).getNgayBatDau()));
            }
            if (isCheckin) {
                txtWorkScheduleCheckin.setVisibility(View.VISIBLE);
            } else {
                txtWorkScheduleCheckin.setVisibility(View.INVISIBLE);
            }

            if (mDataset.get(position).getTrangThaiId() != 1) {
                txtWorkScheduleCheckin.setTextColor(context.getResources().getColor(R.color.tran_person_color));
                ColorUtils.setTextViewDrawableColor(txtWorkScheduleCheckin, R.color.tran_person_color);
                txtWorkScheduleCheckin.setText(R.string.ci_already_check_in);
            }else {
                txtWorkScheduleCheckin.setTextColor(context.getResources().getColor(R.color.pink));
                ColorUtils.setTextViewDrawableColor(txtWorkScheduleCheckin, R.color.pink);
                txtWorkScheduleCheckin.setText(R.string.ci_work_schedule_check_in);
            }

            if (mDataset.get(position).getTrangThaiText() != null){
               txtStateSchedule.setText(mDataset.get(position).getTrangThaiText());
            }

            txtWorkScheduleCheckin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(context, ListImageAlreadyCheckInActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("schedule", mDataset.get(position));
                    i.putExtra("scheduledata", bundle);
                    context.startActivity(i);
                }
            });

        }

        @Override
        public void onClick(View v) {
            itemClickListener.onClick(v, getAdapterPosition(), false);
        }

        @Override
        public boolean onLongClick(View v) {
            itemClickListener.onClick(v, getAdapterPosition(), true); // Gọi interface , true là vì đây là onLongClick
            return true;
        }

        public void setItemClickListener(ItemClickListener itemClickListener) {
            this.itemClickListener = itemClickListener;
        }
    }

    public class ViewMoreViewHolder extends BaseViewHolder implements View.OnClickListener, View.OnLongClickListener {

        private ItemClickListener itemClickListener;

        public ViewMoreViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);
            v.setOnLongClickListener(this);
        }

        @Override
        protected void clear() {

        }

        @Override
        public void onBind(int position) {
            super.onBind(position);
            if (position != NUMBER_OF_DAILY_WORK_IN_HOME) {
                return;
            }
        }

        @Override
        public void onClick(View v) {
            itemClickListener.onClick(v, getAdapterPosition(), false);
        }

        @Override
        public boolean onLongClick(View v) {
            itemClickListener.onClick(v, getAdapterPosition(), true); // Gọi interface , true là vì đây là onLongClick
            return true;
        }

        public void setItemClickListener(ItemClickListener itemClickListener) {
            this.itemClickListener = itemClickListener;
        }
    }




    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                ViewHolder vh = new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_work_schedule, parent, false));
                vh.setItemClickListener((view, position, isLongClick) -> {
                    if (isLongClick) {

                    } else {
                        Intent i = new Intent(context, DetailWorkScheduleActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("schedule", mDataset.get(position));
                        bundle.putBoolean("isHistory", false);
                        i.putExtra("extraschedule", bundle);
                        context.startActivity(i);
                    }
                });
                return vh;

            case VIEW_TYPE_VIEW_MORE:
                ViewMoreViewHolder viewMoreViewHolder = new ViewMoreViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_more_daily_work, parent, false));
                viewMoreViewHolder.setItemClickListener((View view, int position, boolean isLongClick) -> {
                    Intent i = new Intent(context, WorkScheduleActivity.class);
                    context.startActivity(i);
                });
                return viewMoreViewHolder;

            default:
                ViewHolder viewHolder = new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_work_schedule, parent, false));
                viewHolder.setItemClickListener((view, position, isLongClick) -> {
                    if (isLongClick) {

                    } else {
                        Intent i = new Intent(context, DetailWorkScheduleActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("schedule", mDataset.get(position));
                        bundle.putBoolean("isHistory", false);
                        i.putExtra("extraschedule", bundle);
                        context.startActivity(i);
                    }
                });
                return viewHolder;
        }

    }



    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder baseViewHolder, int i) {
        baseViewHolder.onBind(i);
    }

    @Override
    public int getItemCount() {
        if (mDataset.size() <= NUMBER_OF_DAILY_WORK_IN_HOME) {
            return mDataset.size();
        }else {
            return NUMBER_OF_DAILY_WORK_IN_HOME + 1;
        }

    }

    @Override
    public int getItemViewType(int position) {
        Log.d(TAG, "Position: " + position);
        if (position == NUMBER_OF_DAILY_WORK_IN_HOME) {
            return VIEW_TYPE_VIEW_MORE;
        }
        return VIEW_TYPE_NORMAL;
    }
}
