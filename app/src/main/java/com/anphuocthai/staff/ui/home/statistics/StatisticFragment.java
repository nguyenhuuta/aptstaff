package com.anphuocthai.staff.ui.home.statistics;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.activities.dailywork.DailyWorkActivity;
import com.anphuocthai.staff.activities.newcustomer.CustomerActivities;
import com.anphuocthai.staff.di.component.ActivityComponent;
import com.anphuocthai.staff.ui.delivery.debt.DebtManagerActivity;
import com.anphuocthai.staff.ui.delivery.myorder.MyOrderActivity;
import com.anphuocthai.staff.ui.delivery.nottransportyet.NotTranActivity;
import com.anphuocthai.staff.ui.home.container.ContainerFragment;
import com.anphuocthai.staff.ui.home.statistics.model.DynamicReport;
import com.anphuocthai.staff.ui.home.statistics.model.updatedynamic.PerformStatistic;
import com.anphuocthai.staff.ui.home.statistics.viewholder.DailyCVViewHolder;

import java.util.ArrayList;

import javax.inject.Inject;

public class StatisticFragment extends ContainerFragment implements StatisticMvpView, SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = StatisticFragment.class.getSimpleName();

    private Context context;


    @Inject
    StatisticMvpPresenter<StatisticMvpView> mPresenter;

    @Inject
    LinearLayoutManager layoutManager;


    private StatisticAdapter adapter;

    private SwipeRefreshLayout mSwipeRefreshLayout;

    /**
     * Create a new instance of the fragment
     */
    public static StatisticFragment newInstance(int index, Context context) {
        StatisticFragment fragment = new StatisticFragment();
        Bundle b = new Bundle();
        b.putInt("index", index);
        fragment.setArguments(b);
        fragment.context = context;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view;
        ActivityComponent activityComponent = getActivityComponent();
        if (activityComponent != null) {
            activityComponent.inject(this);
            mPresenter.onAttach(this);
        }

        view = inflater.inflate(R.layout.statistic_home_fragment, container, false);
        initView(view);
        setupPullToRefresh(view);
        return view;
    }

    private void initView(View view) {
        fragmentContainer = view.findViewById(R.id.fragment_container);
        recyclerView = view.findViewById(R.id.statistic_recycler_view);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemViewCacheSize(10);

    }


    private void setupRecyclerView(ArrayList<DynamicReport> dynamicReports) {
        if (adapter == null) {
            adapter = new StatisticAdapter(getBaseActivity());
            adapter.setmPresenter(mPresenter);
            adapter.setContext(getBaseActivity());
            recyclerView.setAdapter(adapter);
        }
        adapter.setDynamicReports(dynamicReports);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    /**
     * Refresh
     */
    public void refresh() {
        if (getArguments().getInt("index", 0) > 0 && recyclerView != null) {
            recyclerView.smoothScrollToPosition(0);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    /**
     * Called when a fragment will be displayed
     */
    public void willBeDisplayed() {
        if (fragmentContainer != null) {
            Animation fadeIn = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
            fragmentContainer.startAnimation(fadeIn);
        }
    }

    /**
     * Called when a fragment will be hidden
     */
    public void willBeHidden() {
        if (fragmentContainer != null) {
            Animation fadeOut = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out);
            fragmentContainer.startAnimation(fadeOut);
        }
    }

    @Override
    protected void setUp(View view) {
        view.setOnClickListener((View v) -> {

        });
    }



    @Override
    public void onGetDynamicReportSuccess(ArrayList<DynamicReport> dynamicReports) {
        mSwipeRefreshLayout.setRefreshing(false);
        setupRecyclerView(dynamicReports);
    }

    @Override
    public void openMyOrderShortcut() {
        Intent i = MyOrderActivity.getMyOrderIntent(getBaseActivity());
        getBaseActivity().startActivity(i);
    }

    @Override
    public void openDebtManagement() {
        Intent i = DebtManagerActivity.getDebtIntent(getBaseActivity());
        getBaseActivity().startActivity(i);
    }

    @Override
    public void openCustomerShortcut() {
        Intent intent = new Intent(getBaseActivity(), CustomerActivities.class);
        startActivity(intent);
    }

    @Override
    public void openTransportShortcut() {
        if(context == null)return;
        Intent i = NotTranActivity.getNotTranIntent(context);
        context.startActivity(i);
    }

    @Override
    public void openDailyWork() {
        if(context == null)return;
        Intent intent = new Intent(context, DailyWorkActivity.class);
        context.startActivity(intent);
    }

    @Override
    public void onGetDynamicReportError() {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onPerformStatisticSuccess(PerformStatistic performStatistic, DailyCVViewHolder viewHolder) {
        viewHolder.updatePerformStatistic(performStatistic);
    }

//    @Override
//    public void onTopCustomerStatisticDynamicSuccess(PerformStatistic performStatistic, TopCustomerVHTable viewHolder) {
//        viewHolder.updateDynamic(performStatistic);
//    }

    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.setRefreshing(true);
        mPresenter.getDynamicReport();
    }

    private void setupPullToRefresh(View view) {
        // SwipeRefreshLayout
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container_statistic);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);
        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(true);
                // Fetching data from server
                mPresenter.getDynamicReport();
            }
        });
    }
}
