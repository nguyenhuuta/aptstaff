package com.anphuocthai.staff.ui.customer.detail.config;

import com.androidnetworking.error.ANError;
import com.anphuocthai.staff.R;
import com.anphuocthai.staff.api.ApiURL;
import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.data.DataManager;
import com.anphuocthai.staff.interfaces.IAPICallback;
import com.anphuocthai.staff.model.ResponseMessage;
import com.anphuocthai.staff.model.customer.Customer;
import com.anphuocthai.staff.ui.base.BasePresenter;
import com.anphuocthai.staff.utils.rx.SchedulerProvider;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class ConfigCustomerPresenter<V extends ConfigCustomerMvpView> extends BasePresenter<V> implements ConfigCustomerMvpPresenter<V> {

    private static final String TAG = ConfigCustomerPresenter.class.getSimpleName();

    @Inject
    public ConfigCustomerPresenter(NetworkManager networkManager,
                                   DataManager dataManager,
                                   SchedulerProvider schedulerProvider,
                                   CompositeDisposable compositeDisposable) {
        super(networkManager,
                dataManager,
                schedulerProvider,
                compositeDisposable);
    }


    @Override
    public void getCustomerByGuid(String guid) {

        getmMvpView().showLoading();
        HashMap hashMap = new HashMap();
        hashMap.put("guid", guid);
        getmNetworkManager().sendGetRequestObjectResponse(ApiURL.GET_CUSTOMER_BY_GUID_URL, hashMap, new IAPICallback() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                if (response != null) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<Customer>() {
                    }.getType();
                    Customer result = new Customer();
                    result = gson.fromJson(response.toString(), type);


                    getmMvpView().onUpdateView(result);
                    getmMvpView().hideLoading();
                }
            }

            @Override
            public void onResponseSuccess(JSONArray response) {}

            @Override
            public void onResponseError(ANError anError) {
                getmMvpView().hideLoading();
            }
        });

    }

    @Override
    public void updateCustomer(Customer customer, String personOrder, String debtType) {

        JSONObject object = new JSONObject();
        try {
            object.put("guid", customer.getGuid());
            object.put("nguoDatDon", personOrder);
            object.put("thanhToanCongNoId", customer.getCustomerData().getThanhToanCongNoId());
            object.put("thanhToanCongNo", debtType);
        }catch (Exception e) {
            e.printStackTrace();
        }

        getmNetworkManager().sendPostRequest(ApiURL.UPDATE_CONFIG_INFO_URL, object, new IAPICallback() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                if (response != null) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<ResponseMessage>() {
                    }.getType();
                    ResponseMessage result = gson.fromJson(response.toString(), type);
                    if (result.getCode().equals("00")) {
                        getmMvpView().showMessage(R.string.common_update_success);
                        getCustomerByGuid(customer.getGuid());
                    }
                    else {}

                }
            }
            @Override
            public void onResponseSuccess(JSONArray response) {

            }

            @Override
            public void onResponseError(ANError anError) {

            }
        });


    }
}
