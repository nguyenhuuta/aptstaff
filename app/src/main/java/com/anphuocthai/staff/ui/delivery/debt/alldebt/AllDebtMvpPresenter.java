package com.anphuocthai.staff.ui.delivery.debt.alldebt;

import com.anphuocthai.staff.ui.base.MvpPresenter;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;

import org.json.JSONObject;

import java.util.List;

import io.reactivex.Flowable;

public interface AllDebtMvpPresenter<V extends AllDebtMvpView> extends MvpPresenter<V> {
    void onViewPrepared();
    Flowable<List<OrderTran>> getAllDebtApiCall(JSONObject object);
}
