package com.anphuocthai.staff.ui.delivery.myorder.adjust;

import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.data.DataManager;
import com.anphuocthai.staff.ui.base.BasePresenter;
import com.anphuocthai.staff.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class AdjustOrderPresenter<V extends AdjustOrderMvpView> extends BasePresenter<V> implements AdjustOrderMvpPresenter<V>{

    @Inject
    public AdjustOrderPresenter(NetworkManager networkManager, DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(networkManager, dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void dismissDialog() {
        getmMvpView().dismissDialog();
    }
}
