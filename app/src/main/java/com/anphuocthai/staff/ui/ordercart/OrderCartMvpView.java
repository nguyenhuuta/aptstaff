package com.anphuocthai.staff.ui.ordercart;

import android.util.Pair;

import com.anphuocthai.staff.data.TypeProduct;
import com.anphuocthai.staff.data.db.model.OrderProduct;
import com.anphuocthai.staff.model.ChietKhauModel;
import com.anphuocthai.staff.model.customer.Customer;
import com.anphuocthai.staff.ui.base.MvpView;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;

import java.util.ArrayList;
import java.util.List;

public interface OrderCartMvpView extends MvpView {
    void onBackButtonClick();

    void onUpdateOrderProducts(List<OrderProduct> orderProducts);

    void onUpdateSumaryFeeAndCV(float sumfee, float sumCV, int tongChietKhau);

    void onOpenSaleDialog(OrderProduct orderProduct, OrderCartAdapter.ViewHolder viewHolder);

    void openSearchActivity();

    void onCreateOrderSuccess();

    void startOrder();

    void onUpdateCustomerDebt(ArrayList<OrderTran> orderTrans);

    void onShowOrderConfigDialog(Customer customer, OrderCartAdapter adapter);

    void onGetCustomerByGuidSuccess(Customer customer);

    void showListTypeProduct(Pair<List<TypeProduct>, List<ChietKhauModel>> pair);
}
