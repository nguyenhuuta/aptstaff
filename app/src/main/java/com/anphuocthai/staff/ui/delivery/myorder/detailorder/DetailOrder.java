
package com.anphuocthai.staff.ui.delivery.myorder.detailorder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DetailOrder  implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("code")
    @Expose
    private Object code;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("val")
    @Expose
    private Object val;
    @SerializedName("body")
    @Expose
    private String body;
    @SerializedName("typeId")
    @Expose
    private Object typeId;
    @SerializedName("backColor")
    @Expose
    private Object backColor;
    @SerializedName("foreColor")
    @Expose
    private Object foreColor;
    @SerializedName("cssClass")
    @Expose
    private Object cssClass;
    @SerializedName("cssStyle")
    @Expose
    private Object cssStyle;
    @SerializedName("orderId")
    @Expose
    private Object orderId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Object getCode() {
        return code;
    }

    public void setCode(Object code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Object getVal() {
        return val;
    }

    public void setVal(Object val) {
        this.val = val;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Object getTypeId() {
        return typeId;
    }

    public void setTypeId(Object typeId) {
        this.typeId = typeId;
    }

    public Object getBackColor() {
        return backColor;
    }

    public void setBackColor(Object backColor) {
        this.backColor = backColor;
    }

    public Object getForeColor() {
        return foreColor;
    }

    public void setForeColor(Object foreColor) {
        this.foreColor = foreColor;
    }

    public Object getCssClass() {
        return cssClass;
    }

    public void setCssClass(Object cssClass) {
        this.cssClass = cssClass;
    }

    public Object getCssStyle() {
        return cssStyle;
    }

    public void setCssStyle(Object cssStyle) {
        this.cssStyle = cssStyle;
    }

    public Object getOrderId() {
        return orderId;
    }

    public void setOrderId(Object orderId) {
        this.orderId = orderId;
    }

}
