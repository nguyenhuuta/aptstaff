package com.anphuocthai.staff.ui.delivery.nottransportyet.transportorder;

import com.anphuocthai.staff.ui.base.MvpView;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;

import java.util.ArrayList;

public interface TransportOrderMvpView extends MvpView {
    void displayOrderTrans(ArrayList<OrderTran> orderTrans);
    void hideRecyclerView(boolean isHide);
}
