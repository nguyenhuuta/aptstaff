package com.anphuocthai.staff.ui.delivery.shipped.finishtransport;

import com.anphuocthai.staff.ui.base.MvpPresenter;

public interface FinishTransportMvpPresenter<V extends FinishTransportMvpView> extends MvpPresenter<V> {
    /**
     * Bàn giao số tiền giao hàng
     *
     * @param orderId : Id đơn hàng
     * @param money   : số tiền thu được
     * @param ghiChu  : ghi chú trong trường hợp có trả lại hàng (bắt buộc) còn không thì không cần.
     */
    void banGiaoChungTu(int orderId, float money, String ghiChu,int DD_TrangThaiID);

}
