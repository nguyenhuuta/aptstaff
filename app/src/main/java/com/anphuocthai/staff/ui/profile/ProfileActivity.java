package com.anphuocthai.staff.ui.profile;

import android.os.Bundle;

import androidx.viewpager.widget.ViewPager;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.fragments.profile.ChangePassFragment;
import com.anphuocthai.staff.fragments.profile.TaiKhoanNganHangFragment;
import com.anphuocthai.staff.fragments.profile.UserHoSoFragment;
import com.anphuocthai.staff.fragments.profile.UserHopDongFragment;
import com.anphuocthai.staff.ui.base.BaseActivity;
import com.anphuocthai.staff.ui.base.ViewPagerAdapter;
import com.anphuocthai.staff.ui.profile.personalinfo.ProfileFragment;
import com.anphuocthai.staff.utils.App;


public class ProfileActivity extends BaseActivity {
    private ViewPager viewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        App.setContext(this);
        viewPager =  findViewById(R.id.viewpager);
        setupToobarWithTab(getString(R.string.profile_name), viewPager, false);
        setupViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(ProfileFragment.newInstance(viewPager), getString(R.string.profile_personal_title_fragment));
        adapter.addFragment(new UserHoSoFragment(), getString(R.string.profile_infomation_title));
        adapter.addFragment(new UserHopDongFragment(), getString(R.string.profile_contract_title));
        adapter.addFragment(new TaiKhoanNganHangFragment(), getString(R.string.profile_bank_account));
        adapter.addFragment(new ChangePassFragment(), getString(R.string.proflie_change_pass_title));
        viewPager.setAdapter(adapter);
    }
}
