package com.anphuocthai.staff.ui.delivery.nottransportyet.trachungtu;

import com.anphuocthai.staff.ui.base.MvpView;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;

import java.util.List;

/**
 * Created by OpenYourEyes on 11/23/2020
 */
public interface ITraChungTuView extends MvpView {
    void getDanhSachDonDat(List<OrderTran> list);
}
