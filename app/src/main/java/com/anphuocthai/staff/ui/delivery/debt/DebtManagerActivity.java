package com.anphuocthai.staff.ui.delivery.debt;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.ui.base.BaseActivity;
import com.anphuocthai.staff.ui.base.BaseFragment;
import com.anphuocthai.staff.ui.base.ViewPagerAdapter;
import com.anphuocthai.staff.ui.delivery.debt.alldebt.AllDebtFragment;
import com.anphuocthai.staff.ui.delivery.debt.customerdebt.CustomerDebtFragment;
import com.anphuocthai.staff.ui.delivery.debt.holdingdebt.HoldingDebtFragment;
import com.anphuocthai.staff.ui.delivery.debt.inventorydebt.InventoryDebtFragment;
import com.anphuocthai.staff.ui.delivery.debt.requestdebt.RequestDebtFragment;
import com.anphuocthai.staff.ui.delivery.debt.waitpaydebt.WaitPayDebtFragment;
import com.anphuocthai.staff.utils.Logger;

import javax.inject.Inject;

public class DebtManagerActivity extends BaseActivity implements DebtMvpView {

    @Inject
    DebtMvpPresenter<DebtMvpView> mPresenter;

    private ViewPager viewPager;
    ViewPagerAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_debt_manager);
        getmActivityComponent().inject(this);
        mPresenter.onAttach(this);
        initViews();
        setupViewPager();
        setupToobarWithTab(getString(R.string.manager_debt_title), viewPager, false);
    }

    private void initViews() {
        viewPager = findViewById(R.id.debt_view_pager);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int position) {
                Logger.d("onPageSelected", position + "");
                Fragment fragment = mAdapter.getItem(position);
                if (fragment instanceof BaseFragment) {
                    BaseFragment baseFragment = (BaseFragment) fragment;
                    baseFragment.reloadData();
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    protected void setupViewPager() {
        mAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        mAdapter.addFragment(new CustomerDebtFragment(), getString(R.string.debt_customer));
        mAdapter.addFragment(new InventoryDebtFragment(), getString(R.string.inventory_debt_tab));
        mAdapter.addFragment(new RequestDebtFragment(), getString(R.string.request_debt_tab));
        mAdapter.addFragment(new HoldingDebtFragment(), getString(R.string.holding_debt_tab));
        mAdapter.addFragment(new WaitPayDebtFragment(), getString(R.string.wait_pay_debt_tab));
        mAdapter.addFragment(AllDebtFragment.newInstance(), getString(R.string.all_debt_tab));
        viewPager.setOffscreenPageLimit(6);
        viewPager.setAdapter(mAdapter);
    }

    public static Intent getDebtIntent(Context context) {
        return new Intent(context, DebtManagerActivity.class);
    }
}
