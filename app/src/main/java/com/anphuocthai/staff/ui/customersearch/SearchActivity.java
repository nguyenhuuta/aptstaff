package com.anphuocthai.staff.ui.customersearch;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.androidnetworking.error.ANError;
import com.anphuocthai.staff.R;
import com.anphuocthai.staff.activities.IActivityCallback;
import com.anphuocthai.staff.activities.customer.AddNewCustomerActivity;
import com.anphuocthai.staff.activities.newcustomer.ViewPageCustomerAdapter;
import com.anphuocthai.staff.api.ApiURL;
import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.data.DataManager;
import com.anphuocthai.staff.fragments.BaseFragment;
import com.anphuocthai.staff.fragments.FragmentId;
import com.anphuocthai.staff.interfaces.IAPICallback;
import com.anphuocthai.staff.model.customer.Customer;
import com.anphuocthai.staff.model.customer.SearchCustomer;
import com.anphuocthai.staff.ui.base.BaseActivity;
import com.anphuocthai.staff.utils.Utils;
import com.arlib.floatingsearchview.FloatingSearchView;
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import static com.anphuocthai.staff.utils.Constants.RESULT_CODE_SEARCH_CUSTOMER;

public class SearchActivity extends BaseActivity implements CSearchMvpView, IActivityCallback {

    public static final String TAG = "SearchActivity";

    private FloatingSearchView floatingSearchView;

    private RecyclerView mRecentCustomer;

    private FloatingActionButton floatAddCustomer;

    @Inject
    CSearchMvpPresenter<CSearchMvpView> mPresenter;

    @Inject
    DataManager dataManager;


    @Inject
    LinearLayoutManager mLayoutManager;

    private List<SearchCustomer> historyCustomers = new ArrayList<>();



    private TabLayout mTabLayout;
    private ViewPager mViewPageCustomer;
    private ViewPageCustomerAdapter mAdapter;

    @Inject
    LoadMoreCustomerAdapter loadMoreCustomerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        getmActivityComponent().inject(this);
        mPresenter.onAttach(this);
        addControls();
        addEvents();
        mPresenter.onGetRecentCustomer();

        initTabLayout();

    }

    private void addControls() {
        floatAddCustomer = findViewById(R.id.add_new_customer_fab);
        floatingSearchView = findViewById(R.id.floating_search_view);
        mRecentCustomer = findViewById(R.id.recentList);
        mRecentCustomer.setLayoutManager(mLayoutManager);

        loadMoreCustomerAdapter.setmPresenter(mPresenter);
    }

    private void initTabLayout() {
        mTabLayout = findViewById(R.id.tabLayout);
        mViewPageCustomer = findViewById(R.id.viewPage);
        mAdapter = new ViewPageCustomerAdapter(getSupportFragmentManager());
        mViewPageCustomer.setAdapter(mAdapter);
        mViewPageCustomer.setOffscreenPageLimit(2);
        mTabLayout.setupWithViewPager(mViewPageCustomer);
    }


    private void addEvents() {
        floatAddCustomer.setOnClickListener((View v) -> {
            Intent i = new Intent(this, AddNewCustomerActivity.class);
            startActivity(i);
        });

        floatingSearchView.setOnQueryChangeListener((oldQuery, newQuery) -> {
            if (!oldQuery.equals("") && newQuery.equals("")) {
                floatingSearchView.clearSuggestions();
            } else {
                searchCustomer(newQuery);
                floatingSearchView.clearSuggestions();
            }

        });

        floatingSearchView.setOnFocusChangeListener(new FloatingSearchView.OnFocusChangeListener() {
            @Override
            public void onFocus() {

            }

            @Override
            public void onFocusCleared() {

            }
        });

        floatingSearchView.setOnBindSuggestionCallback((suggestionView, leftIcon, textView, item, itemPosition) -> {

            SearchCustomer searchCustomer = (SearchCustomer) item;
            if (searchCustomer.isHistory()) {
                leftIcon.setImageDrawable(ResourcesCompat.getDrawable(getResources(),
                        R.drawable.ic_history, null));
            }
        });


        floatingSearchView.setOnHomeActionClickListener(() -> finish());


        floatingSearchView.setOnSearchListener(new FloatingSearchView.OnSearchListener() {
            @Override
            public void onSuggestionClicked(SearchSuggestion searchSuggestion) {
                floatingSearchView.clearSearchFocus();
                floatingSearchView.clearSuggestions();
                Utils.hideKeyboard(SearchActivity.this);
                SearchCustomer customer = (SearchCustomer) searchSuggestion;
                dataManager.setCurrentCustomerGuid(customer.getGuid());
                dataManager.setCurrentCustomerName(customer.getTenDayDu());
                Toast.makeText(SearchActivity.this, getString(R.string.order_already_selected_customer) + " " + customer.getTenDayDu(), Toast.LENGTH_SHORT).show();
                sendToOrderCartActivity(customer.toCustomer());
            }

            @Override
            public void onSearchAction(String currentQuery) {
            }
        });

        floatingSearchView.setOnSuggestionsListHeightChanged(new FloatingSearchView.OnSuggestionsListHeightChanged() {
            @Override
            public void onSuggestionsListHeightChanged(float newHeight) {
                mRecentCustomer.setTranslationY(newHeight);
            }
        });
    }


    private void sendToOrderCartActivity(Customer customer) {
        Intent i = new Intent();
        i.putExtra("customer_extra", customer);
        setResult(RESULT_CODE_SEARCH_CUSTOMER, i);
        finish();
    }

    private void searchCustomer(String customerName) {
        floatingSearchView.showProgress();
        if (customerName.isEmpty()) {
            return;
        }
        JSONObject customerObject = new JSONObject();
        try {
            customerObject.put("tenDayDu", customerName);
            customerObject.put("doiTuongId", "");
            customerObject.put("pageIndex", 1);
            customerObject.put("pageSize", 5);

        } catch (Exception e) {
            e.printStackTrace();
        }

        NetworkManager.getInstance().sendPostRequestWithArrayResponse(ApiURL.GET_CUSTOMER_BY_CATEGORY_URL, customerObject, new IAPICallback() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                if (response != null) {

                }
            }

            @Override
            public void onResponseSuccess(JSONArray response) {
                if (response != null) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<List<SearchCustomer>>() {
                    }.getType();
                    List<SearchCustomer> result = gson.fromJson(response.toString(), type);
                    floatingSearchView.clearSuggestions();
                    floatingSearchView.swapSuggestions(result);
                    floatingSearchView.hideProgress();
                }
            }

            @Override
            public void onResponseError(ANError anError) {
                Log.d(TAG, anError.toString());
            }
        });
    }

    @Override
    public void onShowRecentCustomer(List<SearchCustomer> customerList) {
        historyCustomers.clear();

        if (customerList.size() < 5 && customerList.size() >= 0) {
            for (int i = 0; i < customerList.size(); i++) {
                customerList.get(i).setHistory(true);
                historyCustomers.add(customerList.get(i));
            }
        } else {
            for (int i = 0; i < 5; i++) {
                customerList.get(i).setHistory(true);
                historyCustomers.add(customerList.get(i));
            }
        }
        loadMoreCustomerAdapter.addHistoryCustomers(historyCustomers);
        mRecentCustomer.setAdapter(loadMoreCustomerAdapter);
    }

    @Override
    public void onShowAllCustomer(List<SearchCustomer> customerList) {
        for (int i = 0; i < customerList.size(); i++) {
            customerList.get(i).setTenDayDu(Character.toUpperCase(customerList.get(i).getTenDayDu().charAt(0)) + customerList.get(i).getTenDayDu().substring(1));
        }

        Collections.sort(customerList, (contact, t1) -> contact.getTenDayDu().compareToIgnoreCase(t1.getTenDayDu()));
    }

    @Override
    public void onCustomerSelected(SearchCustomer customer) {
        floatingSearchView.clearSearchFocus();
        floatingSearchView.clearSuggestions();
        Utils.hideKeyboard(SearchActivity.this);
        dataManager.setCurrentCustomerGuid(customer.getGuid());
        dataManager.setCurrentCustomerName(customer.getTenDayDu());
        Toast.makeText(SearchActivity.this, getString(R.string.order_already_selected_customer) + " " + customer.getTenDayDu(), Toast.LENGTH_SHORT).show();
        sendToOrderCartActivity(customer.toCustomer());
    }

    @Override
    public void displayScreen(FragmentId screenId, boolean... addToBackStack) {

    }

    @Override
    public void onFragmentResumed(BaseFragment fragment) {

    }

    @Override
    public void setAppBarTitle(String title) {

    }

    @Override
    public void onBackPressed(Bundle bundle) {

    }
}
