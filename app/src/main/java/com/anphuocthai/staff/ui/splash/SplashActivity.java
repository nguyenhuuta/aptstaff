package com.anphuocthai.staff.ui.splash;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import androidx.core.content.ContextCompat;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.ui.base.BaseActivity;
import com.anphuocthai.staff.ui.home.MainActivity;
import com.anphuocthai.staff.ui.login.LoginActivity;

import javax.inject.Inject;

/**
 * Splash screen, run when app start
 */
public class SplashActivity extends BaseActivity implements SplashMvpView {
    public static final String TAG = "SplashActivity";
    @Inject
    SplashMvpPresenter<SplashMvpView> mPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch_screen);
        changeColorStatusBar();
        getmActivityComponent().inject(this);
        mPresenter.onAttach(this);
        mPresenter.checkLogin();
    }
    public void changeColorStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        }
    }


        @Override
    public void openLoginActivity() {
        print("openLoginActivity");
        Intent intent = LoginActivity.getStartIntent(SplashActivity.this);
        startActivity(intent);
        finish();
    }

    @Override
    public void openMainActivity() {
        print("openMainActivity");
        Intent intent = MainActivity.getStartIntent(SplashActivity.this);
        startActivity(intent);
        finish();
    }

    @Override
    public void showDialogLoginFailure(String message) {
        String message1 = message +"\n Bạn có muốn đăng nhập lại?";
        showDialogConfirm(message1, () -> openLoginActivity());
    }
}
