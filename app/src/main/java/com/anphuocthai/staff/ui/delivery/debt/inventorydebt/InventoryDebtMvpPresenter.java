package com.anphuocthai.staff.ui.delivery.debt.inventorydebt;

import com.anphuocthai.staff.ui.base.MvpPresenter;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;

import java.util.List;

public interface InventoryDebtMvpPresenter<V extends InventoryDebtMvpView> extends MvpPresenter<V> {
    void onViewPrepared(String keySearch, int page);

    void onSetStartCollectDebt(List<OrderTran> orderTran);
}
