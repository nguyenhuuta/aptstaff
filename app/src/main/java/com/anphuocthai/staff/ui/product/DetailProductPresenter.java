package com.anphuocthai.staff.ui.product;

import android.util.Log;

import com.androidnetworking.error.ANError;
import com.anphuocthai.staff.R;
import com.anphuocthai.staff.api.ApiURL;
import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.data.DataManager;
import com.anphuocthai.staff.data.db.model.OrderProduct;
import com.anphuocthai.staff.interfaces.IAPICallback;
import com.anphuocthai.staff.model.order.Product;
import com.anphuocthai.staff.ui.base.BasePresenter;
import com.anphuocthai.staff.utils.App;
import com.anphuocthai.staff.utils.rx.SchedulerProvider;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

public class DetailProductPresenter<V extends DetailProductMvpView> extends BasePresenter<V> implements DetailProductMvpPresenter<V> {

    private final String TAG = DetailProductPresenter.class.getSimpleName();

    @Inject
    public DetailProductPresenter(NetworkManager networkManager,
                                  DataManager dataManager,
                                  SchedulerProvider schedulerProvider,
                                  CompositeDisposable compositeDisposable) {
        super(networkManager, dataManager, schedulerProvider, compositeDisposable);

    }

    @Inject
    OrderLogic orderLogic;

    @Override
    public void onAddToCartButtonClick(OrderProduct orderProduct) {
        getCompositeDisposable().add(getDataManager()
                .insertOrderProduct(orderProduct)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(aBoolean -> {
                    getmMvpView().showMessage(R.string.order_already_add_to_cart_notifi);
                    getAllOrderProduct();
                }, throwable -> {

                }));
    }

    @Override
    public void onCartButtonClick() {
        getmMvpView().openOrderCartActivity();
    }

    @Override
    public void onGetProductById(int id) {
        getmMvpView().showLoading();
        HashMap hashMap = new HashMap();
        hashMap.put("id", String.valueOf(id));
        hashMap.put("ddhhimgs", "true");
        getmNetworkManager().sendGetRequestNotAuthen(ApiURL.GET_COMMODITY_BY_ID, hashMap, true, new IAPICallback() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                if (response != null) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<Product>() {
                    }.getType();
                    Product product = gson.fromJson(response.toString(), type);


                    // setting order logic
                    orderLogic.setCv(product.getMainPrice().getKDCV());
                    orderLogic.setLowerThreshold(product.getMainPrice().getKDBanBuonSoLuong2());
                    orderLogic.setQuantityThreshold(product.getMainPrice().getKDBanBuonSoLuong1());
                    orderLogic.setPriceRetail(product.getMainPrice().getKDBanLeGia());
                    orderLogic.setPriceWholeSale(product.getMainPrice().getKDBanBuonGia());
                    orderLogic.setFixCV(product.getMainPrice().getKDCV());

                    getmMvpView().displayProduct(product);
                    getmMvpView().hideLoading();
                }
            }

            @Override
            public void onResponseSuccess(JSONArray response) {

            }

            @Override
            public void onResponseError(ANError anError) {
                getmMvpView().hideLoading();
            }
        });
    }

    @Override
    public void onIncreaseQuantityButtonClick() {
        orderLogic.setQuantity(orderLogic.getQuantity() + 1);
        onUpdatePrice();
    }

    @Override
    public void onSubQuantityButtonClick() {
        float currentValue = orderLogic.getQuantity() - 1;
        if (currentValue < 0) {
            currentValue = 0;
        }
        orderLogic.setQuantity(currentValue);
        onUpdatePrice();
    }

    @Override
    public void onChangeQuantity(float quantity) {
        orderLogic.setQuantity(quantity);
        onUpdatePrice();
    }

    @Override
    public void onSetDefaultQuantity() {
    }

    @Override
    public void onUpdatePrice() {
        getmMvpView().setPriceFeeAndCV(orderLogic.getCurrentPrice(), orderLogic.getTotalFee(), orderLogic.getCvWithQuantity());
    }

    @Override
    public void onCreateOrder(Product product, String guid) {
        JSONObject mainObject = new JSONObject();
        try {
            JSONObject productObject = new JSONObject();
            productObject.put("dD_HangHoaID", product.getId());
            productObject.put("donViID", product.getMainPrice().getDMDonViID());
            productObject.put("soLuong", orderLogic.getQuantity());
            productObject.put("giaTien", orderLogic.getCurrentPrice());
            productObject.put("cV", product.getMainPrice().getKDCV());
            productObject.put("giaHangHoaID", 2);
            productObject.put("duLieu", "ghi chú hàng hóa");
            JSONArray products = new JSONArray();
            products.put(productObject);
            mainObject.put("dD_ThanhToanID", 1);
            mainObject.put("loaiDonDatID", 1);
            mainObject.put("ghiChu", "Ghi chú đơn hàng");
            mainObject.put("guidThanhVien", guid);
            mainObject.put("hanghoas", products);
            mainObject.put("BoPhanID", App.getInstance().getDepartmentId());

        } catch (Exception e) {
            e.printStackTrace();
        }

        getmNetworkManager().sendPostRequest(ApiURL.CREATE_ORDER_URL, mainObject, new IAPICallback() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                Log.d(TAG, response.toString());
                getmMvpView().showMessage(R.string.order_created_success);
                getmMvpView().onCreateOrderSuccess();
            }

            @Override
            public void onResponseSuccess(JSONArray response) {

            }

            @Override
            public void onResponseError(ANError anError) {

            }
        });
    }

    @Override
    public void getAllOrderProduct() {
        getCompositeDisposable().add(getDataManager()
                .getAllOrderProducts()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(new Consumer<List<OrderProduct>>() {
                    @Override
                    public void accept(List<OrderProduct> orderProducts) throws Exception {
                        getmMvpView().onUpdateShoppingCart(orderProducts);
                    }
                }, throwable -> {

                }));
    }


}
