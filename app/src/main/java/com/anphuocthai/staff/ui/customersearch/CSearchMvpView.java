package com.anphuocthai.staff.ui.customersearch;

import com.anphuocthai.staff.model.customer.SearchCustomer;
import com.anphuocthai.staff.ui.base.MvpView;

import java.util.List;

public interface CSearchMvpView extends MvpView {
    void onShowRecentCustomer(List<SearchCustomer> customerList);
    void onShowAllCustomer(List<SearchCustomer> customers);

    void onCustomerSelected(SearchCustomer customer);
}
