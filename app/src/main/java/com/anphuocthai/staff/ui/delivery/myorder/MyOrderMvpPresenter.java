package com.anphuocthai.staff.ui.delivery.myorder;

import com.anphuocthai.staff.ui.base.MvpPresenter;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;

import java.util.List;

import io.reactivex.Flowable;

public interface MyOrderMvpPresenter<V extends MyOrderMvpView> extends MvpPresenter<V> {
    void onGetAllMyOrder();
    void hideRecyclerView(boolean isHide);
    Flowable<List<OrderTran>> rxJavaOnGetAllMyOrder(final int page);

    void cancelOrderTran(OrderTran orderTran);

    void showAdjustDialog(OrderTran orderTran);

    void editOrderStepCancelOrder(OrderTran orderTran);

    void onDeleteAllOrderCart(OrderTran orderTran);

    void insertProductToCart(OrderTran orderTran);
}
