
package com.anphuocthai.staff.ui.delivery.model;

import com.anphuocthai.staff.data.db.model.OrderProduct;
import com.anphuocthai.staff.ui.product.OrderLogic;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Hanghoa implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("sessionID")
    @Expose
    private String sessionID;
    @SerializedName("dD_DonDatID")
    @Expose
    private Integer dDDonDatID;
    @SerializedName("dD_HangHoaID")
    @Expose
    private Integer dDHangHoaID;
    @SerializedName("giaHangHoaID")
    @Expose
    private Integer giaHangHoaID;
    @SerializedName("donViID")
    @Expose
    private Integer donViID;
    @SerializedName("soLuong")
    @Expose
    private Float soLuong;
    @SerializedName("giaTien")
    @Expose
    private Integer giaTien;
    @SerializedName("cv")
    @Expose
    private Integer cv;
    @SerializedName("duLieu")
    @Expose
    private String duLieu;
    @SerializedName("tiLeGiam")
    @Expose
    private Integer tiLeGiam;
    @SerializedName("traLai")
    @Expose
    private Boolean traLai;
    @SerializedName("khuyenMaiID")
    @Expose
    private Object khuyenMaiID;
    @SerializedName("loaiDonDatId")
    @Expose
    private Integer loaiDonDatId;
    @SerializedName("tenHangHoa")
    private String tenHangHoa;
    @SerializedName("donVi")
    @Expose
    private String donVi;
    @SerializedName("loaidondat")
    @Expose
    private Integer loaidondat;
    @SerializedName("ngayTao")
    @Expose
    private String ngayTao;
    @SerializedName("hanghoa")
    @Expose
    private Hanghoa_ hanghoa;
    @SerializedName("tongCV")
    @Expose
    private Integer tongCV;

    OrderLogic orderLogic = new OrderLogic();


    public OrderProduct toOrderProduct(String customerName, String customerGuid) {

        OrderProduct orderProduct = new OrderProduct();
        orderProduct.setCustomerName(customerName);
        orderProduct.setCustomerGuid(customerGuid);
        orderProduct.setProductId(String.valueOf(dDHangHoaID));
        orderProduct.setQuantity(getSoLuong());

        // additional infomation
        orderProduct.setProductCode(getHanghoa().getMaHangHoa());
        orderProduct.setProductName(getHanghoa().getTenHangHoa());
        orderProduct.setDescription(getHanghoa().getMoTa());
        orderProduct.setDetail(getHanghoa().getChiTiet());
        orderProduct.setData(getHanghoa().getDuLieu());
        orderProduct.setProperty(getHanghoa().getThuoctinhs());
        orderProduct.setMainPrice(getHanghoa().getGiaChinh());
        orderProduct.setCompanyName(getHanghoa().getTenCongTy());
        orderProduct.setTypeName(getHanghoa().getTenDangSanPham());
        orderProduct.setSource(getHanghoa().getXuatXu());
        orderProduct.setTradeMark(getHanghoa().getThuongHieu());
        orderProduct.setProductCategoryName(getHanghoa().getTenNhomSanPham());
        orderProduct.setProductCategoryId(getHanghoa().getNhomSanPhamId());
        orderProduct.setShortDescription(getHanghoa().getMoTaNgan());
        if (getHanghoa().getAnhDaiDienId() != null) {
            orderProduct.setThumbId(getHanghoa().getAnhDaiDienId());
        }


        // order logic setup
        orderLogic.setCv(this.getHanghoa().getGiaChinh().getKDCV());
        int t = this.getHanghoa().getGiaChinh().getKDCV();

        orderLogic.setLowerThreshold(this.getHanghoa().getGiaChinh().getKDBanBuonSoLuong2());
        orderLogic.setQuantityThreshold(this.getHanghoa().getGiaChinh().getKDBanBuonSoLuong1());
        orderLogic.setPriceRetail(this.getHanghoa().getGiaChinh().getKDBanLeGia());
        orderLogic.setPriceWholeSale(this.getHanghoa().getGiaChinh().getKDBanBuonGia());
        orderLogic.setFixCV(this.getHanghoa().getGiaChinh().getKDCV());


        orderLogic.setQuantity(getSoLuong());


        orderProduct.setOrderLogic(orderLogic);
        orderProduct.setProductNote("");
        return orderProduct;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSessionID() {
        return sessionID;
    }

    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }

    public Integer getDDDonDatID() {
        return dDDonDatID;
    }

    public void setDDDonDatID(Integer dDDonDatID) {
        this.dDDonDatID = dDDonDatID;
    }

    public Integer getDDHangHoaID() {
        return dDHangHoaID;
    }

    public void setDDHangHoaID(Integer dDHangHoaID) {
        this.dDHangHoaID = dDHangHoaID;
    }

    public Integer getGiaHangHoaID() {
        return giaHangHoaID;
    }

    public void setGiaHangHoaID(Integer giaHangHoaID) {
        this.giaHangHoaID = giaHangHoaID;
    }

    public Integer getDonViID() {
        return donViID;
    }

    public void setDonViID(Integer donViID) {
        this.donViID = donViID;
    }

    public Float getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(Float soLuong) {
        this.soLuong = soLuong;
    }

    public Integer getGiaTien() {
        return giaTien;
    }

    public void setGiaTien(Integer giaTien) {
        this.giaTien = giaTien;
    }

    public Integer getCv() {
        return cv;
    }

    public void setCv(Integer cv) {
        this.cv = cv;
    }

    public String getDuLieu() {
        return duLieu;
    }

    public void setDuLieu(String duLieu) {
        this.duLieu = duLieu;
    }

    public Integer getTiLeGiam() {
        return tiLeGiam;
    }

    public void setTiLeGiam(Integer tiLeGiam) {
        this.tiLeGiam = tiLeGiam;
    }

    public Boolean getTraLai() {
        return traLai;
    }

    public void setTraLai(Boolean traLai) {
        this.traLai = traLai;
    }

    public Object getKhuyenMaiID() {
        return khuyenMaiID;
    }

    public void setKhuyenMaiID(Object khuyenMaiID) {
        this.khuyenMaiID = khuyenMaiID;
    }

    public Integer getLoaiDonDatId() {
        return loaiDonDatId;
    }

    public void setLoaiDonDatId(Integer loaiDonDatId) {
        this.loaiDonDatId = loaiDonDatId;
    }

    public String getDonVi() {
        return donVi;
    }

    public void setDonVi(String donVi) {
        this.donVi = donVi;
    }

    public Integer getLoaidondat() {
        return loaidondat;
    }

    public void setLoaidondat(Integer loaidondat) {
        this.loaidondat = loaidondat;
    }

    public String getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(String ngayTao) {
        this.ngayTao = ngayTao;
    }

    public Hanghoa_ getHanghoa() {
        return hanghoa;
    }

    public void setHanghoa(Hanghoa_ hanghoa) {
        this.hanghoa = hanghoa;
    }

    public Integer getTongCV() {
        return tongCV;
    }

    public void setTongCV(Integer tongCV) {
        this.tongCV = tongCV;
    }

    public String getTenHangHoa() {
        return tenHangHoa;
    }
}
