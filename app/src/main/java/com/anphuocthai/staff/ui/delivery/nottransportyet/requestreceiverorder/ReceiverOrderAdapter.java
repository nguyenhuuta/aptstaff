package com.anphuocthai.staff.ui.delivery.nottransportyet.requestreceiverorder;

import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.interfaces.IYNDialogCallback;
import com.anphuocthai.staff.ui.base.BaseActivity;
import com.anphuocthai.staff.ui.base.BaseViewHolder;
import com.anphuocthai.staff.ui.delivery.model.Hanghoa;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;
import com.anphuocthai.staff.ui.delivery.model.Vanchuyen;
import com.anphuocthai.staff.utils.Enum;
import com.anphuocthai.staff.utils.Utils;
import com.anphuocthai.staff.utils.ui.UIUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ReceiverOrderAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private static final String TAG = com.anphuocthai.staff.ui.delivery.nottransportyet.NotTranAdapter.class.getSimpleName();

    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private Callback mCallback;
    private List<OrderTran> mOrderTrans;

    ReceiverOrderMvpPresenter<ReceiverOrderMvpView> mPresenter;

    BaseActivity baseActivity;


    public void setmPresenter(ReceiverOrderMvpPresenter<ReceiverOrderMvpView> mPresenter) {
        this.mPresenter = mPresenter;
    }

    public void setBaseActivity(BaseActivity baseActivity) {
        this.baseActivity = baseActivity;
    }

    public ReceiverOrderAdapter(List<OrderTran> mOrderTrans) {
        this.mOrderTrans = mOrderTrans;
    }

    public void setCallback(Callback callback) {
        mCallback = callback;
    }


    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new ReceiverOrderAdapter.ViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_not_tran, parent, false));
            case VIEW_TYPE_EMPTY:
            default:
                return new ReceiverOrderAdapter.EmptyViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_view, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mOrderTrans != null && mOrderTrans.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }


    }

    @Override
    public int getItemCount() {
        if (mOrderTrans != null && mOrderTrans.size() > 0) {
            return mOrderTrans.size();
        } else {
            return 1;
        }

    }

    public void addItems(List<OrderTran> orderTrans) {
        mOrderTrans.clear();
        mOrderTrans.addAll(orderTrans);
        notifyDataSetChanged();
    }

    public void hideEmptyView() {

    }

    public interface Callback {
        void onBlogEmptyViewRetryClick();
    }

    public class ViewHolder extends BaseViewHolder {
        TextView txtOrderCode;
        TextView txtCustomerAddress;
        TextView txtCustomerName;
        TextView txtProductInfo;
        TextView btnSubmitTran;

        private TextView lblPersonReceiver;
        private TextView txtPersonReceiver;

        private TextView lblSumaryWeight;
        private TextView txtSumaryWeight;

        private TextView lblCV;
        private TextView txtCV;

        private LinearLayout llNvkd;
        private TextView txtNvkd;

        private LinearLayout llGhiChu;
        private TextView txtGhiChu;

        private ImageView imgMap;

        public ViewHolder(View itemView) {
            super(itemView);
            initView(itemView);
        }

        private void initView(View itemView) {
            lblPersonReceiver = itemView.findViewById(R.id.lbl_person_receiver);
            txtPersonReceiver = itemView.findViewById(R.id.txt_person_receiver);

            lblPersonReceiver.setVisibility(View.GONE);
            txtPersonReceiver.setVisibility(View.GONE);

            txtSumaryWeight = itemView.findViewById(R.id.txtSumaryWeight);
            lblSumaryWeight = itemView.findViewById(R.id.lblSumaryWeight);

            txtCV = itemView.findViewById(R.id.txtCV);
            lblCV = itemView.findViewById(R.id.lblCV);

            llNvkd = itemView.findViewById(R.id.llNvkd);
            txtNvkd = itemView.findViewById(R.id.txtNvkd);

            txtGhiChu = itemView.findViewById(R.id.txtGhiChu);
            llGhiChu = itemView.findViewById(R.id.llGhiChu);

            txtOrderCode = itemView.findViewById(R.id.all_debt_txt_order_code);
            txtCustomerAddress = itemView.findViewById(R.id.all_debt_txt_order_customer_address);
            txtCustomerName = itemView.findViewById(R.id.all_debt_txt_order_customer_name);
            txtProductInfo = itemView.findViewById(R.id.order_tran_txt_product_info);
            btnSubmitTran = itemView.findViewById(R.id.all_debt_btn_submit);
            btnSubmitTran.setOnClickListener((View v) -> {
                UIUtils.showYesNoDialog(baseActivity, "", baseActivity.getString(R.string.order_tran_submit_tran_order), new IYNDialogCallback() {
                    @Override
                    public void accept() {
                        mPresenter.setTransport(mOrderTrans.get(getAdapterPosition()));
                    }

                    @Override
                    public void cancel() {

                    }
                });
            });
            imgMap = itemView.findViewById(R.id.ic_map);
        }

        protected void clear() {
        }

        public void onBind(int position) {
            super.onBind(position);
            final OrderTran orderTran = mOrderTrans.get(position);
            if (orderTran.getMaDonDat() != null) {
                txtOrderCode.setText(orderTran.getMaDonDat());
            }
            if (orderTran.getTenThanhVien() != null) {
                txtCustomerName.setText(orderTran.getTenThanhVien());
            }

            txtCustomerAddress.setText("");
            txtPersonReceiver.setText("");
            List<Vanchuyen> listVanChuyen = orderTran.getVanchuyens();
            if (!Utils.isNull(listVanChuyen)) {
                Vanchuyen firstVanChuyen = orderTran.getVanchuyens().get(0);
                String address = firstVanChuyen.getDiaChiNhanHang();
                txtCustomerAddress.setText(address);
                if (firstVanChuyen.getNguoiChuyenID() == null || firstVanChuyen.getNguoiChuyenID() <= 0) {
                    // hide receiver order
                    btnSubmitTran.setVisibility(View.VISIBLE);
                    lblPersonReceiver.setVisibility(View.GONE);
                    txtPersonReceiver.setVisibility(View.GONE);
                    btnSubmitTran.setText(R.string.order_tran_select_tran);
                    btnSubmitTran.setBackground(baseActivity.getResources().getDrawable(R.drawable.rounded_shape));
                    btnSubmitTran.setOnClickListener((View v) -> {
                        UIUtils.showYesNoDialog(baseActivity, "", baseActivity.getString(R.string.order_tran_submit_tran_order), new IYNDialogCallback() {
                            @Override
                            public void accept() {
                                mPresenter.setTransport(mOrderTrans.get(position));
                            }

                            @Override
                            public void cancel() {

                            }
                        });
                    });
                }

                if (firstVanChuyen.getNguoiChuyenID() != null && firstVanChuyen.getNguoiChuyenID() > 0) {
                    btnSubmitTran.setVisibility(View.VISIBLE);
                    btnSubmitTran.setBackgroundColor(baseActivity.getResources().getColor(R.color.tran_person_color));
                    txtPersonReceiver.setVisibility(View.GONE);
                    lblPersonReceiver.setVisibility(View.GONE);
                    btnSubmitTran.setText(firstVanChuyen.getTenNguoiChuyen());
                    btnSubmitTran.setOnClickListener(null);
                }
                if (txtCustomerAddress.getText().toString().trim().isEmpty()) {
                    txtCustomerAddress.setVisibility(View.GONE);
                }
            }


            txtProductInfo.setText("");
            int sumaryWeight = 0;
            if (orderTran.getHanghoas() != null) {
                int size = orderTran.getHanghoas().size();
                if (size > 0) {
                    String goodsNames = new String();
                    for (int i = 0; i < size; i++) {
                        if (orderTran.getHanghoas().get(i).getHanghoa().getTenHangHoa() != null) {
                            Hanghoa hanghoa = orderTran.getHanghoas().get(i);
                            sumaryWeight += hanghoa.getSoLuong();
                            if ((i == 0 && (size == 1)) || i == size - 1) {
                                goodsNames += hanghoa.getHanghoa().getTenHangHoa() + "(" + Utils.formatValue(hanghoa.getSoLuong(), Enum.FieldValueType.NORMAL) + hanghoa.getDonVi() + ")";
                            } else {
                                goodsNames += hanghoa.getHanghoa().getTenHangHoa() + "(" + Utils.formatValue(hanghoa.getSoLuong(), Enum.FieldValueType.NORMAL) + hanghoa.getDonVi() + ")" + "\n";
                            }

                        }
                    }

                    txtProductInfo.setText(goodsNames);
                }
            }

            String ghiChu = orderTran.getGhiChu();
            if (ghiChu != null && !ghiChu.trim().isEmpty()) {
                llGhiChu.setVisibility(View.VISIBLE);
                txtGhiChu.setText(ghiChu.trim());
            } else {
                llGhiChu.setVisibility(View.GONE);
            }

            txtSumaryWeight.setVisibility(View.VISIBLE);
            lblSumaryWeight.setVisibility(View.VISIBLE);
            txtSumaryWeight.setText(Utils.formatValue(sumaryWeight, Enum.FieldValueType.WEIGHT));

            Integer cv = orderTran.getTongCV();
            if (cv != null && cv > 0) {
                txtCV.setVisibility(View.VISIBLE);
                lblCV.setVisibility(View.VISIBLE);
                txtCV.setText(Utils.formatValue(cv, Enum.FieldValueType.NORMAL));
            }

            String nvkd = orderTran.getTenNhanVien();
            if (!TextUtils.isEmpty(nvkd)) {
                llNvkd.setVisibility(View.VISIBLE);
                txtNvkd.setText(nvkd);
            }
            imgMap.setOnClickListener(v -> {
                Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + txtCustomerAddress.getText());
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                baseActivity.startActivity(mapIntent);
            });
        }
    }

    public class EmptyViewHolder extends BaseViewHolder {

        @BindView(R.id.btn_retry)
        Button retryButton;

        @BindView(R.id.tv_message)
        TextView messageTextView;

        @BindView(R.id.txt_retry)
        TextView txtRetry;

        public EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            retryButton.setVisibility(View.GONE);
            txtRetry.setVisibility(View.GONE);
        }

        @Override
        protected void clear() {

        }

        @OnClick(R.id.btn_retry)
        void onRetryClick() {
            if (mCallback != null)
                mCallback.onBlogEmptyViewRetryClick();
        }
    }
}
