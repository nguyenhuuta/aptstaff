package com.anphuocthai.staff.ui.customer.detail.historytransactions;

import com.anphuocthai.staff.ui.base.MvpPresenter;

public interface HistoryTranMvpPresenter<V extends HistoryTranMvpView> extends MvpPresenter<V> {

    void getHistoryTrans(String guid);
}
