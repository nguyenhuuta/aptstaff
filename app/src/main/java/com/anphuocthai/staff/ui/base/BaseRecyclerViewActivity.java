package com.anphuocthai.staff.ui.base;


import android.os.Bundle;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;

import javax.inject.Inject;

public class BaseRecyclerViewActivity extends BaseActivity {

    private RecyclerView recyclerView;

    @Inject
    LinearLayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_recycler_view);
        getmActivityComponent().inject(this);
        initView();
    }

    @Override
    public void setupToolbar(String title) {
        super.setupToolbar(title);
    }

    private void initView() {
        recyclerView = findViewById(R.id.base_activity_recycler_view);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    public RecyclerView getRecyclerView() {
        return recyclerView;
    }
}
