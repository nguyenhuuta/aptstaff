package com.anphuocthai.staff.ui.delivery.nottransportyet.reporthtml;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.di.component.ActivityComponent;
import com.anphuocthai.staff.ui.base.BaseFragment;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class ReportReceiveOrderFragment extends BaseFragment implements ReportReceiveOrderMvpView,ReportReceiveOrderAdapter.Callback, SwipeRefreshLayout.OnRefreshListener{

    private static final String TAG = ReportReceiveOrderFragment.class.getSimpleName();
    @Inject
    ReportReceiveOrderMvpPresenter<ReportReceiveOrderMvpView> mPresenter;

    @Inject
    ReportReceiveOrderAdapter mInventoryDeptAdapter;

    @Inject
    LinearLayoutManager mLayoutManager;

//    private RecyclerView recyclerView;
//
//    private TextView labelSumaryCV;
//
//    private TextView txtSumaryCV;
//
//
//    private TextView labelSumaryMustCollect;
//
//    private TextView txtSumaryMustCollect;
//
//    private TextView labelSumaryAlreadyCollect;
//
//    private TextView txtSumaryOrder;
//
//    private TextView txtSumaryAlreadyCollect;
//
//    private View sumaryContainer;

    private SwipeRefreshLayout mSwipeRefreshLayout;

    private List<OrderTran> orderTranArrayList = new ArrayList<>();

    private WebView webView;

    private View emptyView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.customer_debt_layout, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            mPresenter.onAttach(this);
            mInventoryDeptAdapter.setCallback(this);
            mInventoryDeptAdapter.setContext(getActivity());
            mInventoryDeptAdapter.setmPresenter(mPresenter);
        }
        return view;
    }

    @Override
    protected void setUp(View view) {

        webView = view.findViewById(R.id.web_view_customer_debt);

        emptyView = view.findViewById(R.id.layout_empty_view);

        emptyView.setVisibility(View.VISIBLE);

//        sumaryContainer = view.findViewById(R.id.layout_sumary);
//        sumaryContainer.setVisibility(View.GONE);
//
//        labelSumaryCV = view.findViewById(R.id.label_sumary_cv);
//
//        txtSumaryCV = view.findViewById(R.id.txt_sumary_cv);
//
//        labelSumaryCV.setVisibility(View.GONE);
//        txtSumaryCV.setVisibility(View.GONE);
//
//        labelSumaryMustCollect= view.findViewById(R.id.label_must_collect);
//
//        txtSumaryMustCollect= view.findViewById(R.id.txt_must_collect);
//
//        labelSumaryMustCollect.setVisibility(View.VISIBLE);
//        txtSumaryMustCollect.setVisibility(View.VISIBLE);
//
//        labelSumaryAlreadyCollect= view.findViewById(R.id.label_already_collect);
//
//        txtSumaryAlreadyCollect= view.findViewById(R.id.txt_already_collect);
//
//        labelSumaryAlreadyCollect.setVisibility(View.VISIBLE);
//
//        txtSumaryAlreadyCollect.setVisibility(View.VISIBLE);
//
//        txtSumaryOrder = view.findViewById(R.id.txt_number_order);
//
//        recyclerView = view.findViewById(R.id.customer_debt_recycler_view);
//        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
//        recyclerView.setLayoutManager(mLayoutManager);
//        recyclerView.setItemAnimator(new DefaultItemAnimator());
//        recyclerView.setAdapter(mInventoryDeptAdapter);


        setupPullToRefresh(view);

    }

    public static ReportReceiveOrderFragment newInstance() {
        Bundle args = new Bundle();
        ReportReceiveOrderFragment fragment = new ReportReceiveOrderFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void updateOrderTran(List<OrderTran> orderTranArrayList) {
//        recyclerView.setVisibility(View.VISIBLE);
//        this.orderTranArrayList = orderTranArrayList;
//        mInventoryDeptAdapter.addItems(orderTranArrayList);
//
//        if (orderTranArrayList.size() <= 0 ){
//            sumaryContainer.setVisibility(View.GONE);
//            mSwipeRefreshLayout.setRefreshing(false);
//            return;
//        }else {
//            sumaryContainer.setVisibility(View.VISIBLE);
//        }
//
//        int sumMustCollect = 0;
//        int sumAlreadyCollect = 0;
//
//        for (int i = 0; i < orderTranArrayList.size(); i++) {
//            sumMustCollect += orderTranArrayList.get(i).getPhaiThanhToan();
//            sumAlreadyCollect += orderTranArrayList.get(i).getDaThu();
//        }
//
//        txtSumaryMustCollect.setText(Utils.formatValue(sumMustCollect, Enum.FieldValueType.CURRENCY));
//        txtSumaryAlreadyCollect.setText(Utils.formatValue(sumAlreadyCollect, Enum.FieldValueType.CURRENCY));
//        txtSumaryOrder.setText(String.valueOf(orderTranArrayList.size()) + " " +  getString(R.string.common_order));
//
//        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onGetCustomerDebtSuccess(String report) {
        mSwipeRefreshLayout.setRefreshing(false);
        if (report != null) {
            webView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
            webView.loadData(report, "text/html", null);
        }else {
            webView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void setRefresh(boolean isRefresh) {
        if (mSwipeRefreshLayout != null) {
            if (isRefresh) {
                mSwipeRefreshLayout.setRefreshing(true);
            }else {
                mSwipeRefreshLayout.setRefreshing(false);
            }
        }
    }

    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.setRefreshing(true);
       // mPresenter.onViewPrepared();
        mPresenter.getCustomerDebt();
    }

    private void setupPullToRefresh(View view) {
        // SwipeRefreshLayout
        mSwipeRefreshLayout =  view.findViewById(R.id.swipe_container_customer_debt);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);
        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
        mSwipeRefreshLayout.post(() -> {
            if (orderTranArrayList.size() <= 0) {
                mSwipeRefreshLayout.setRefreshing(true);
               // mPresenter.onViewPrepared();
                //recyclerView.setVisibility(View.GONE);
                webView.setVisibility(View.GONE);

                mPresenter.getCustomerDebt();
            }
            else {
                updateOrderTran(orderTranArrayList);
            }
        });
    }
}
