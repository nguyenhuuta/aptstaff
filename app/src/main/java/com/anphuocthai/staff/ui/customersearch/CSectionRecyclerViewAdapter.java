package com.anphuocthai.staff.ui.customersearch;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.adapters.checkin.RecyclerViewType;

import java.util.ArrayList;

public class CSectionRecyclerViewAdapter extends RecyclerView.Adapter<CSectionRecyclerViewAdapter.SectionViewHolder> {


    private RecyclerView allCustomerRecyclerView;

    private SearchActivity searchActivity;

    private LinearLayoutManager linearLayoutManager1;

    private SearchResultsListAdapter recentCustomerAdapter = new SearchResultsListAdapter();

    private SearchResultsListAdapter allCustomerAdapter = new SearchResultsListAdapter();


    public SearchResultsListAdapter getRecentCustomerAdapter() {
        return recentCustomerAdapter;
    }

    public SearchResultsListAdapter getAllCustomerAdapter() {
        return allCustomerAdapter;
    }

    class SectionViewHolder extends RecyclerView.ViewHolder {
        private TextView sectionLabel;
        private RecyclerView itemRecyclerView;

        public SectionViewHolder(View itemView) {
            super(itemView);
            sectionLabel = itemView.findViewById(R.id.section_label);
            itemRecyclerView =  itemView.findViewById(R.id.item_recycler_view);

        }
    }

    private Context context;
    private RecyclerViewType recyclerViewType;
    private ArrayList<CustomerSectionModel> sectionModelArrayList;

    public CSectionRecyclerViewAdapter(Context context,
                                       RecyclerViewType recyclerViewType,
                                       ArrayList<CustomerSectionModel> sectionModelArrayList,
                                       SearchActivity activity) {
        this.searchActivity = activity;
        this.context = context;
        this.recyclerViewType = recyclerViewType;
        this.sectionModelArrayList = sectionModelArrayList;
    }

    @Override
    public SectionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.customer_section_layout, parent, false);
        return new SectionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SectionViewHolder holder, int position) {
        final CustomerSectionModel sectionModel = sectionModelArrayList.get(position);
        holder.sectionLabel.setText(sectionModel.getSectionLabel());

        //recycler view for items
        holder.itemRecyclerView.setHasFixedSize(true);
        holder.itemRecyclerView.setNestedScrollingEnabled(false);

        /* set layout manager on basis of recyclerview enum type */
        switch (recyclerViewType) {
            case LINEAR_VERTICAL:
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
                if (position == 1) {
                    this.linearLayoutManager1 = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
                    holder.itemRecyclerView.setLayoutManager(this.linearLayoutManager1);
                }else  {
                    holder.itemRecyclerView.setLayoutManager(linearLayoutManager);
                }

                break;
            case LINEAR_HORIZONTAL:
                LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
                holder.itemRecyclerView.setLayoutManager(linearLayoutManager1);
                break;
            case GRID:
                GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 3);
                holder.itemRecyclerView.setLayoutManager(gridLayoutManager);
                break;
        }

        if (position == 0) {
            holder.itemRecyclerView.setAdapter(recentCustomerAdapter);
        }
        if (position == 1) {
//            allCustomerRecyclerView = holder.itemRecyclerView;
//            holder.itemRecyclerView.setAdapter(allCustomerAdapter);
//            searchActivity.setUpLoadMoreListener(linearLayoutManager1);
//            searchActivity.setupLoadMore();

        }

    }

    @Override
    public int getItemCount() {
        return sectionModelArrayList.size();
    }


    public RecyclerView getAllCustomerRecyclerView() {
        return allCustomerRecyclerView;
    }
}

