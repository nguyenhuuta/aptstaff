package com.anphuocthai.staff.ui.delivery.myorder.detailorder;

import com.anphuocthai.staff.ui.base.MvpPresenter;

public interface DetailMyOrderMvpPresenter<V extends DetailMyOrderMvpView> extends MvpPresenter<V> {

    void getDetailOrder(String sessionID);
}
