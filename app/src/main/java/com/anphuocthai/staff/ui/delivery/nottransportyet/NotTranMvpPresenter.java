package com.anphuocthai.staff.ui.delivery.nottransportyet;

import com.anphuocthai.staff.ui.base.MvpPresenter;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;

import java.util.List;

import io.reactivex.Flowable;

public interface NotTranMvpPresenter<V extends NotTranMvpView> extends MvpPresenter<V> {
    void onGetAllNotTranOrder();
    Flowable<List<OrderTran>> rxJavaOnGetAllNotTranOrder(final int page);
    void hideRecyclerView(boolean isHide);

    void setTransport(OrderTran orderTran);
}
