package com.anphuocthai.staff.ui.delivery.nottransportyet.transportorder;

import com.androidnetworking.error.ANError;
import com.anphuocthai.staff.api.ApiURL;
import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.data.DataManager;
import com.anphuocthai.staff.interfaces.IAPICallback;
import com.anphuocthai.staff.ui.base.BasePresenter;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;
import com.anphuocthai.staff.utils.App;
import com.anphuocthai.staff.utils.rx.SchedulerProvider;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.rx2androidnetworking.Rx2AndroidNetworking;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class TransportOrderPresenter<V extends TransportOrderMvpView> extends BasePresenter<V> implements TransportOrderMvpPresenter<V> {


    private static final String TAG = TransportOrderPresenter.class.getSimpleName();
    private JSONObject createJSONObject(final int page) {
        JSONObject notTranObject = new JSONObject();
        try {
            notTranObject.put("phanLoai", 1);
            notTranObject.put("guidThanhVien", "");
            notTranObject.put("sessionID", "");
            notTranObject.put("tuNgay", "");
            notTranObject.put("denNgay", "");
            notTranObject.put("trangThaiIds", "");
            notTranObject.put("pageIndex", page);
            notTranObject.put("pageSize", 10);
            notTranObject.put("BoPhanID", App.getInstance().getDepartmentId());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return notTranObject;
    }

    @Inject
    public TransportOrderPresenter(NetworkManager networkManager, DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(networkManager, dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void onGetAllNotTranOrder() {
        // getmMvpView().showLoading();
        JSONObject notTranObject = new JSONObject();
        try {
            notTranObject.put("phanLoai", 11);
            notTranObject.put("guidThanhVien", "");
            notTranObject.put("sessionID", "");
            notTranObject.put("tuNgay", "");
            notTranObject.put("denNgay", "");
            notTranObject.put("trangThaiIds", "");
            notTranObject.put("pageIndex", 1);
            notTranObject.put("pageSize", 10);
            notTranObject.put("BoPhanID", App.getInstance().getDepartmentId());
        } catch (Exception e) {
            e.printStackTrace();
        }

        getmNetworkManager().sendPostRequestWithArrayResponse(ApiURL.GET_ORDER_URL, notTranObject, new IAPICallback() {
            @Override
            public void onResponseSuccess(JSONObject response) {}
            @Override
            public void onResponseSuccess(JSONArray response) {
                //Log.d(TAG, response.toString());
                if (response != null) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<ArrayList<OrderTran>>() {
                    }.getType();
                    ArrayList<OrderTran> orderTrans = gson.fromJson(response.toString(), type);
                    if (!isViewAttached()) {
                        return;
                    }
                    getmMvpView().displayOrderTrans(orderTrans);
                    //getmMvpView().hideLoading();
                }
            }

            @Override
            public void onResponseError(ANError anError) {
                //  getmMvpView().hideLoading();
            }
        });
    }

    @Override
    public Flowable<List<OrderTran>> rxJavaOnGetAllNotTranOrder(final int page) {
        return Rx2AndroidNetworking.post(ApiURL.GET_ORDER_URL)
                .addHeaders(ApiURL.getBaseHeader())
                .addHeaders(ApiURL.getTokenHeader())
                .addJSONObjectBody(createJSONObject(page))
                .build()
                .getObjectListFlowable(OrderTran.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public void hideRecyclerView(boolean isHide) {
        getmMvpView().hideRecyclerView(isHide);
    }

    @Override
    public int getUserId() {
        return getDataManager().getUserInfoId();
    }


//    @Override
//    public void setTransport(OrderTran orderTran) {
//        JSONObject setTransportObject = new JSONObject();
//        try {
//            setTransportObject.put("id", orderTran.getVanchuyens().get(0).getId());
//            setTransportObject.put("sessionID", orderTran.getSessionID());
//            setTransportObject.put("SoTien", 0);
//        }catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        getmNetworkManager().sendPostRequest(ApiURL.SET_TRANSPORT_URL, setTransportObject, new IAPICallback() {
//            @Override
//            public void onResponseSuccess(JSONObject response) {
//                if (response != null) {
//                    Gson gson = new Gson();
//                    Type type = new TypeToken<ResponseMessage>() {
//                    }.getType();
//                    ResponseMessage result = gson.fromJson(response.toString(), type);
//
//                    if (result.getCode().equals(SUCCESS_CODE)) {
//                        getmMvpView().showMessage(result.getMessage());
//                        onGetAllNotTranOrder();
//                    }
//                    else if (result.getCode().equals(NOT_FOUND_CODE)){
//
//                    }
//                    else if (result.getCode().equals(ERROR_CODE)) {
//
//                    }
//
//                }
//            }
//
//            @Override
//            public void onResponseSuccess(JSONArray response) {
//
//            }
//
//            @Override
//            public void onResponseError(ANError anError) {
//
//            }
//        });
//    }
}
