
package com.anphuocthai.staff.ui.delivery.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Thuoctinhs implements Serializable {

    @SerializedName("dongGoi")
    @Expose
    private String dongGoi;
    @SerializedName("baoQuan")
    @Expose
    private String baoQuan;


//    public ProductProperties toProductProperties(){
//        ProductProperties productProperties = new ProductProperties();
//        productProperties.setPack(getDongGoi());
//        productProperties.setPreservation(getBaoQuan());
//        return productProperties;
//    }

    public String getDongGoi() {
        return dongGoi;
    }

    public void setDongGoi(String dongGoi) {
        this.dongGoi = dongGoi;
    }

    public String getBaoQuan() {
        return baoQuan;
    }

    public void setBaoQuan(String baoQuan) {
        this.baoQuan = baoQuan;
    }

}
