package com.anphuocthai.staff.ui.delivery.myorder.detailorder.detailtab;

import com.anphuocthai.staff.ui.base.MvpView;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;

import java.util.List;

public interface DebtTabMvpView extends MvpView {
    void updateOrderTran(List<OrderTran> orderTranArrayList);

    void onGetCustomerDebtSuccess(String report);

    void setRefresh(boolean isRefresh);
}
