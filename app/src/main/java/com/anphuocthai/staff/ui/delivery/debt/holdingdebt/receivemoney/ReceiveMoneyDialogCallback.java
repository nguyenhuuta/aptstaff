package com.anphuocthai.staff.ui.delivery.debt.holdingdebt.receivemoney;

import com.anphuocthai.staff.model.CongNoModel;

/**
 * Created by OpenYourEyes on 10/29/2020
 */
public interface ReceiveMoneyDialogCallback {
    void thuTien(CongNoModel congNoModel, int soTienDaThu);
}
