package com.anphuocthai.staff.ui.delivery.preparetransport;

import com.androidnetworking.error.ANError;
import com.anphuocthai.staff.R;
import com.anphuocthai.staff.api.ApiURL;
import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.data.DataManager;
import com.anphuocthai.staff.interfaces.IAPICallback;
import com.anphuocthai.staff.model.ResponseMessage;
import com.anphuocthai.staff.ui.base.BasePresenter;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;
import com.anphuocthai.staff.utils.App;
import com.anphuocthai.staff.utils.rx.SchedulerProvider;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

import static com.anphuocthai.staff.utils.Constants.ERROR_CODE;
import static com.anphuocthai.staff.utils.Constants.NOT_FOUND_CODE;
import static com.anphuocthai.staff.utils.Constants.SUCCESS_CODE;

public class PrepareTranPresenter<V extends PrepareTranMvpView> extends BasePresenter<V> implements PrepareTranMvpPresenter<V> {

    SingleSelectionBinder singleSelectionBinder;

    private int successCount = 0;

    public void setSingleSelectionBinder(SingleSelectionBinder singleSelectionBinder) {
        this.singleSelectionBinder = singleSelectionBinder;
    }

    @Inject
    public PrepareTranPresenter(NetworkManager networkManager, DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(networkManager, dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void getAllPrepareTranOrders() {
        getmMvpView().showLoading();
        JSONObject notTranObject = new JSONObject();
        try {
            notTranObject.put("phanLoai", 2);
            notTranObject.put("guidThanhVien", "");
            notTranObject.put("sessionID", "");
            notTranObject.put("tuNgay", "");
            notTranObject.put("denNgay", "");
            notTranObject.put("trangThaiIds", "");
            notTranObject.put("pageIndex", 1);
            notTranObject.put("pageSize", 10);
            notTranObject.put("BoPhanID", App.getInstance().getDepartmentId());
        }catch (Exception e) {
            e.printStackTrace();
        }

        getmNetworkManager().sendPostRequestWithArrayResponse(ApiURL.GET_ORDER_URL, notTranObject, new IAPICallback() {
            @Override
            public void onResponseSuccess(JSONObject response) {

            }

            @Override
            public void onResponseSuccess(JSONArray response) {
                //Log.d(TAG, response.toString());
                if (response != null) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<ArrayList<OrderTran>>() {
                    }.getType();
                    ArrayList<OrderTran> orderTrans = gson.fromJson(response.toString(), type);
                    if (!isViewAttached()) {
                        return;
                    }
                    getmMvpView().displayOrderTrans(orderTrans);
                    getmMvpView().hideLoading();
                }
            }

            @Override
            public void onResponseError(ANError anError) {
                getmMvpView().hideLoading();
            }
        });
    }


    private void sendConfirmOneByOne(OrderTran orderTran, int size) {
        JSONObject setTransportObject = new JSONObject();
        try {
            setTransportObject.put("id", orderTran.getVanchuyens().get(0).getId());
            setTransportObject.put("sessionID", orderTran.getSessionID());
            setTransportObject.put("SoTien", 0);
            setTransportObject.put("BoPhanID", App.getInstance().getDepartmentId());

        }catch (Exception e) {
            e.printStackTrace();
        }

        getmNetworkManager().sendPostRequest(ApiURL.START_TRANSPORT_URL, setTransportObject, new IAPICallback() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                if (response != null) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<ResponseMessage>() {
                    }.getType();
                    ResponseMessage result = gson.fromJson(response.toString(), type);

                    if (result.getCode().equals(SUCCESS_CODE)) {
                        successCount++;
                        if (successCount >= size) {
                            getmMvpView().showMessage(R.string.transport_already_confirm);
                            getAllPrepareTranOrders();
                        }
                    }
                    else if (result.getCode().equals(NOT_FOUND_CODE)){

                    }
                    else if (result.getCode().equals(ERROR_CODE)) {

                    }

                }
            }

            @Override
            public void onResponseSuccess(JSONArray response) {

            }

            @Override
            public void onResponseError(ANError anError) {

            }
        });
    }

    @Override
    public void confirmTransport(List<OrderTran> orderTranListSelected) {
        successCount = 0;
        getmMvpView().showLoading();
        for (int i = 0; i < orderTranListSelected.size(); i++) {
            sendConfirmOneByOne(orderTranListSelected.get(i), orderTranListSelected.size());
        }
    }
}
