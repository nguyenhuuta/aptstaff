package com.anphuocthai.staff.ui.delivery.debt.holdingdebt;

import com.anphuocthai.staff.api.ApiURL;
import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.baserequest.APIResponse;
import com.anphuocthai.staff.data.DataManager;
import com.anphuocthai.staff.model.CongNoModel;
import com.anphuocthai.staff.model.CongNoParams;
import com.anphuocthai.staff.model.CongNoResponse;
import com.anphuocthai.staff.ui.base.BasePresenter;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;
import com.anphuocthai.staff.utils.App;
import com.anphuocthai.staff.utils.Logger;
import com.anphuocthai.staff.utils.rx.SchedulerProvider;
import com.rx2androidnetworking.Rx2AndroidNetworking;

import org.json.JSONObject;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class HoldingDebtPresenter<V extends HoldingDebtMvpView> extends BasePresenter<V> implements HoldingDebtMvpPresenter<V> {


    private static final String TAG = HoldingDebtPresenter.class.getSimpleName();

    @Inject
    public HoldingDebtPresenter(NetworkManager networkManager, DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(networkManager, dataManager, schedulerProvider, compositeDisposable);
    }

    private JSONObject createJSONObject(final int page) {
        JSONObject notTranObject = new JSONObject();
        try {
            notTranObject.put("phanLoai", 6);
            notTranObject.put("guidThanhVien", "");
            notTranObject.put("sessionID", "");
            notTranObject.put("tuNgay", "");
            notTranObject.put("denNgay", "");
            notTranObject.put("trangThaiIds", "");
            notTranObject.put("pageIndex", page);
            notTranObject.put("pageSize", 10);
            notTranObject.put("BoPhanID", App.getInstance().getDepartmentId());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return notTranObject;
    }

    @Override
    public Flowable<List<OrderTran>> getAllDebtApiCall(JSONObject object) {
        return Rx2AndroidNetworking.post(ApiURL.GET_ORDER_URL)
                .addHeaders(ApiURL.getBaseHeader())
                .addHeaders(ApiURL.getTokenHeader())
                .addJSONObjectBody(object)
                .build()
                .getObjectListFlowable(OrderTran.class);

    }


    @Override
    public void onViewPrepared(int page) {
        if (getmMvpView() != null) {
            getmMvpView().showLoading();
        }
        CongNoParams congNoParams = new CongNoParams(getDataManager().getUserInfoId(), page, CongNoParams.TypeDaMuon);
        Disposable disposable = getmNetworkManager().postRequest(ApiURL.danhSachCongNo, congNoParams, CongNoResponse.class)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(congNoResponse -> {
                    if (getmMvpView() == null) {
                        return;
                    }
                    getmMvpView().hideLoading();
                    if (congNoResponse != null && congNoResponse.getData() != null) {
                        getmMvpView().updateOrderTran(congNoResponse.getData());
                    }
                }, throwable -> {
                    if (getmMvpView() == null) {
                        return;
                    }
                    Logger.d("InventoryDebtPresenter", throwable.getLocalizedMessage());
                    getmMvpView().hideLoading();
                    getmMvpView().onError(throwable.getLocalizedMessage());
                });
        getCompositeDisposable().add(disposable);
    }

    @Override
    public void daThuTienn(CongNoModel congNoModel, int sotien) {
        if (getmMvpView() != null) {
            getmMvpView().showLoading();
        }
        JSONObject setDebtObject = new JSONObject();
        try {
            setDebtObject.put("UserID", getDataManager().getUserInfoId());
            setDebtObject.put("NguoiThucHien", congNoModel.getTenNguoiMuon());
            setDebtObject.put("QLCongNoID", congNoModel.getQlCongNoID());
            setDebtObject.put("TongTienDaThu", sotien);
            setDebtObject.put("BoPhanID", App.getInstance().getDepartmentId());
        } catch (Exception e) {
            e.printStackTrace();
        }
        Disposable disposable = Rx2AndroidNetworking.post(ApiURL.ThuCongNo)
                .addHeaders(ApiURL.getBaseHeader())
                .addHeaders(ApiURL.getTokenHeader())
                .addJSONObjectBody(setDebtObject)
                .build()
                .getObjectObservable(APIResponse.class)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(responseMessage -> {
                    if (getmMvpView() == null) return;
                    if (getmMvpView() != null) {
                        getmMvpView().hideLoading();
                    }
                    if (responseMessage.isSuccess()) {
                        congNoModel.setTongTienDaThu(sotien);
                        getmMvpView().datThuTienThanhCong();
                    } else {
                        getmMvpView().showMessage(responseMessage.getMessage());
                    }
                }, throwable -> {
                    if (getmMvpView() != null) {
                        getmMvpView().hideLoading();
                        getmMvpView().showMessage(throwable.getMessage());
                    }
                });

        getCompositeDisposable().add(disposable);


    }

    @Override
    public void chuyenDaMuonSangChoTra(String ids) {
        if (getmMvpView() != null) {
            getmMvpView().showLoading();
        }
        JSONObject setDebtObject = new JSONObject();
        try {
            String fullName = getDataManager().getUserInfo().getFullname();
            setDebtObject.put("UserID", getDataManager().getUserInfoId());
            setDebtObject.put("NguoiThucHien", fullName);
            setDebtObject.put("QLCongNoIDs", ids);
            setDebtObject.put("BoPhanID", App.getInstance().getDepartmentId());
        } catch (Exception e) {
            e.printStackTrace();
        }
        Disposable disposable = Rx2AndroidNetworking.post(ApiURL.ChuyenCongNoSangChoTra)
                .addHeaders(ApiURL.getBaseHeader())
                .addHeaders(ApiURL.getTokenHeader())
                .addJSONObjectBody(setDebtObject)
                .build()
                .getObjectObservable(APIResponse.class)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(responseMessage -> {
                    if (getmMvpView() == null) return;
                    if (getmMvpView() != null) {
                        getmMvpView().hideLoading();
                    }
                    if (responseMessage.isSuccess()) {
                        getmMvpView().chuyenCongNoSangChoTraThanhCong();
                    } else {
                        getmMvpView().showMessage(responseMessage.getMessage());
                    }
                }, throwable -> {
                    if (getmMvpView() != null) {
                        getmMvpView().hideLoading();
                        getmMvpView().showMessage(throwable.getMessage());
                    }
                });

        getCompositeDisposable().add(disposable);
    }

    @Override
    public void onReturnOrder(CongNoModel orderTran) {
        if (getmMvpView() != null) {
            getmMvpView().showLoading();
        }
        JSONObject setDebtObject = new JSONObject();
        try {
            String fullName = getDataManager().getUserInfo().getFullname();
            setDebtObject.put("UserID", getDataManager().getUserInfoId());
            setDebtObject.put("NguoiThucHien", fullName);
            setDebtObject.put("QLCongNoIDs", orderTran.getQlCongNoID());
            setDebtObject.put("BoPhanID", App.getInstance().getDepartmentId());
        } catch (Exception e) {
            e.printStackTrace();
        }
        Disposable disposable = Rx2AndroidNetworking.post(ApiURL.traCongNo)
                .addHeaders(ApiURL.getBaseHeader())
                .addHeaders(ApiURL.getTokenHeader())
                .addJSONObjectBody(setDebtObject)
                .build()
                .getObjectObservable(APIResponse.class)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(responseMessage -> {
                    if (getmMvpView() == null) return;
                    if (getmMvpView() != null) {
                        getmMvpView().hideLoading();
                    }
                    if (responseMessage.isSuccess()) {
                        getmMvpView().traLaiHoaDonThanhCong(orderTran);
                    } else {
                        getmMvpView().showMessage(responseMessage.getMessage());
                    }
                }, throwable -> {
                    if (getmMvpView() != null) {
                        getmMvpView().hideLoading();
                        getmMvpView().showMessage(throwable.getMessage());
                    }
                });

        getCompositeDisposable().add(disposable);
//        JSONObject setDebtObject = new JSONObject();
//        try {
//            setDebtObject.put("id", orderTran.getVanchuyens().get(0).getId());
//            setDebtObject.put("sessionID", orderTran.getSessionID());
//            setDebtObject.put("SoTien", 0);
//            setDebtObject.put("BoPhanID", App.getInstance().getDepartmentId());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        Rx2AndroidNetworking.post(ApiURL.STOP_DEBT_TRANSPORT_URL)
//                .addHeaders(ApiURL.getBaseHeader())
//                .addHeaders(ApiURL.getTokenHeader())
//                .addJSONObjectBody(setDebtObject)
//                .build()
//                .getObjectObservable(ResponseMessage.class)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Observer<ResponseMessage>() {
//                    @Override
//                    public void onSubscribe(Disposable d) {
//                    }
//
//                    @Override
//                    public void onNext(ResponseMessage responseMessage) {
//                        if (responseMessage.getCode().equals(SUCCESS_CODE)) {
//                            if (getmMvpView() != null) {
//                                getmMvpView().showMessage(R.string.return_order_success);
//                                getmMvpView().onRefresh();
//                            }
//                        } else if (responseMessage.getCode().equals(NOT_FOUND_CODE)) {
//
//                        } else if (responseMessage.getCode().equals(ERROR_CODE)) {
//
//                        }
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//
//                    }
//
//                    @Override
//                    public void onComplete() {
//
//                    }
//                });
    }
}
