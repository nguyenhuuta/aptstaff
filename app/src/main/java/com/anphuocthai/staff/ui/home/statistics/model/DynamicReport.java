
package com.anphuocthai.staff.ui.home.statistics.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.anphuocthai.staff.entities.DynamicReportType;
import com.anphuocthai.staff.utils.DateConvert;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class DynamicReport implements Parcelable {

    @SerializedName("tenBaoCao")
    @Expose
    private String tenBaoCao;
    @SerializedName("items")
    @Expose
    private List<Item> items;

    @SerializedName("type")
    private int type;

    /**
     * type = 1 : Top bán hàng xuất sắc trong ngày
     * type = 2 : Top bán hàng năng động trong tháng
     * type = 3 : Top vận chuyển năng động trong tháng
     * type = 4 : Top xuất hàng năng động trong tháng
     * type = 5 : GPS
     */

    public void initData() {
        typeReport = DynamicReportType.Companion.compareValue(tenBaoCao);
        if (typeReport == DynamicReportType.TopSellInDay) {
            dateConvert = DateConvert.TypeConvert.ONLY_DATE;
        } else {
            dateConvert = DateConvert.TypeConvert.MONTH_YEAR;
        }
    }

    private DynamicReportType typeReport;
    public DateConvert.TypeConvert dateConvert = DateConvert.TypeConvert.ONLY_DATE;

    public DynamicReportType getTypeReport() {
        return typeReport;
    }

    protected DynamicReport(Parcel in) {
        tenBaoCao = in.readString();
        items = in.createTypedArrayList(Item.CREATOR);
        type = in.readInt();
    }

    public static final Creator<DynamicReport> CREATOR = new Creator<DynamicReport>() {
        @Override
        public DynamicReport createFromParcel(Parcel in) {
            return new DynamicReport(in);
        }

        @Override
        public DynamicReport[] newArray(int size) {
            return new DynamicReport[size];
        }
    };

    public String getTenBaoCao() {
        return tenBaoCao;
    }

    public void setTenBaoCao(String tenBaoCao) {
        this.tenBaoCao = tenBaoCao;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(tenBaoCao);
        dest.writeTypedList(items);
        dest.writeInt(type);
    }

    public int getType() {
        return type;
    }
}
