package com.anphuocthai.staff.ui.delivery.shipped;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ahamed.multiviewadapter.BaseViewHolder;
import com.ahamed.multiviewadapter.ItemBinder;
import com.anphuocthai.staff.R;
import com.anphuocthai.staff.ui.delivery.model.Hanghoa;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;
import com.anphuocthai.staff.utils.Enum;
import com.anphuocthai.staff.utils.Utils;

public class ShippedAdapter extends ItemBinder<OrderTran, ShippedAdapter.ItemViewHolder> {

    private Context context;
    ShippedMvpPresenter<ShippedMvpView> mPresenter;

    public ShippedAdapter(Context context, ShippedMvpPresenter<ShippedMvpView> mPresenter) {
        this.context = context;
        this.mPresenter = mPresenter;
    }

    @Override public ItemViewHolder create(LayoutInflater layoutInflater, ViewGroup parent) {
        return new ItemViewHolder(layoutInflater.inflate(R.layout.shipped_item_layout, parent, false));
    }

    @Override public boolean canBindData(Object item) {
        return item instanceof OrderTran;
    }

    @Override public void bind(ItemViewHolder holder, OrderTran item) {

        holder.btnShipped.setOnClickListener((View v) -> {
            mPresenter.onShowFinishShipDialog(item);
        });
        holder.txtOrderCode.setText(item.getMaDonDat() + " - " + item.getPhuongThucThanhToan());

        holder.txtCustomerName.setText(item.getTenThanhVien());
        holder.txtProductInfo.setText("");
        if (item.getHanghoas() != null) {
            int size = item.getHanghoas().size();
            if (size > 0) {
                String goodsNames = new String();
                for (int i = 0; i < size; i++){
                    if (item.getHanghoas().get(i).getHanghoa().getTenHangHoa() != null) {
                        Hanghoa hanghoa = item.getHanghoas().get(i);
                        if ((i == 0 && (size == 1)) || i == size - 1) {
                            goodsNames += hanghoa.getHanghoa().getTenHangHoa() + "(" + Utils.formatValue(hanghoa.getSoLuong(), Enum.FieldValueType.NORMAL) +  hanghoa.getDonVi() + ")";
                        }else {
                            goodsNames += hanghoa.getHanghoa().getTenHangHoa() + "(" + Utils.formatValue(hanghoa.getSoLuong(), Enum.FieldValueType.NORMAL) +  hanghoa.getDonVi() + ")"   +"\n";
                        }

                    }
                }
                holder.txtProductInfo.setText(goodsNames);
            }
        }

    }

    static class ItemViewHolder extends BaseViewHolder<OrderTran> {

        private TextView txtOrderCode;
        private TextView txtCustomerName;
        private TextView txtProductInfo;
        private TextView btnShipped;


        ItemViewHolder(View itemView) {
            super(itemView);
            txtOrderCode = (TextView) itemView.findViewById(R.id.all_debt_txt_order_code);
            txtCustomerName = (TextView) itemView.findViewById(R.id.all_debt_txt_order_customer_name);
            txtProductInfo = (TextView) itemView.findViewById(R.id.shipped_txt_product_info);
            btnShipped = itemView.findViewById(R.id.all_debt_btn_submit);
        }
    }
}
