package com.anphuocthai.staff.ui.customersearch;

import com.anphuocthai.staff.utils.Utils;
import com.arlib.floatingsearchview.FloatingSearchView;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by DaiKySy on 9/20/19.
 */
public class RxSearchObservable {
    public static Observable<String> fromView(FloatingSearchView searchView) {
        final PublishSubject<String> subject = PublishSubject.create();
        searchView.setOnQueryChangeListener((oldQuery, newQuery) -> {
            if (Utils.isNull(newQuery)) {
                subject.onComplete();
            } else {
                subject.onNext(newQuery);
            }
        });
        return subject;
    }
}
