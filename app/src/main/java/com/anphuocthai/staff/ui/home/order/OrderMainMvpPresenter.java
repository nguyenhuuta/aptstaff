package com.anphuocthai.staff.ui.home.order;

import com.anphuocthai.staff.data.db.model.OrderProduct;
import com.anphuocthai.staff.ui.base.MvpPresenter;

public interface OrderMainMvpPresenter<V extends OrderMainMvpView> extends MvpPresenter<V> {

    void getAllOrderProduct();

    void getAllProductLocalWhenOrder();

    void onDeleteOrderProduct(OrderProduct orderProduct);

    void onSaveOrderProduct(OrderProduct orderProduct);

    boolean getIsShowGrid();
}
