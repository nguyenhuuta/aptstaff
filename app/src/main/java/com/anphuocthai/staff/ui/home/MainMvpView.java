package com.anphuocthai.staff.ui.home;

import com.anphuocthai.staff.data.db.model.OrderProduct;
import com.anphuocthai.staff.model.Statistics;
import com.anphuocthai.staff.ui.base.MvpView;

import java.util.ArrayList;
import java.util.List;

public interface MainMvpView extends MvpView {
    void onUpdateShoppingCart(List<OrderProduct> orderProducts);
    void onDeleteAllComplete();
    void updateStatistics(ArrayList<Statistics> statistics);
}
