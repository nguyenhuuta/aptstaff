package com.anphuocthai.staff.ui.product;

import com.anphuocthai.staff.data.db.model.OrderProduct;
import com.anphuocthai.staff.di.PerActivity;
import com.anphuocthai.staff.model.order.Product;
import com.anphuocthai.staff.ui.base.MvpPresenter;

@PerActivity
public interface DetailProductMvpPresenter<V extends DetailProductMvpView> extends MvpPresenter<V> {
    void onAddToCartButtonClick(OrderProduct orderProduct);
    void onCartButtonClick();
    void onGetProductById(int id);
    void onIncreaseQuantityButtonClick();
    void onSubQuantityButtonClick();
    void onChangeQuantity(float quantity);
    void onSetDefaultQuantity();
    void onUpdatePrice();
    void onCreateOrder(Product product, String guid);
    // database
    void getAllOrderProduct();
}
