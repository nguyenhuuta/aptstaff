package com.anphuocthai.staff.ui.delivery.myorder.adjust;

import com.anphuocthai.staff.ui.base.MvpView;

public interface AdjustOrderMvpView extends MvpView {

    void dismissDialog();
}
