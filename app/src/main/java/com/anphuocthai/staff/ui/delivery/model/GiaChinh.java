
package com.anphuocthai.staff.ui.delivery.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GiaChinh implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("dD_HangHoaID")
    @Expose
    private Integer dDHangHoaID;
    @SerializedName("ngayTao")
    @Expose
    private String ngayTao;
    @SerializedName("nguoiTao")
    @Expose
    private String nguoiTao;
    @SerializedName("ngayKetThuc")
    @Expose
    private String ngayKetThuc;
    @SerializedName("isInUse")
    @Expose
    private Boolean isInUse;
    @SerializedName("dM_DonViID")
    @Expose
    private Integer dMDonViID;
    @SerializedName("donVi")
    @Expose
    private String donVi;
    @SerializedName("giaNhap")
    @Expose
    private Integer giaNhap;
    @SerializedName("kD_BanBuonGia")
    @Expose
    public Integer kDBanBuonGia;
    @SerializedName("kD_BanBuonDieuKien")
    @Expose
    private String kDBanBuonDieuKien;
    @SerializedName("loaidieukien")
    @Expose
    private Integer loaidieukien;
    @SerializedName("kD_BanBuonSoLuong1")
    @Expose
    private Float kDBanBuonSoLuong1;
    @SerializedName("kD_BanBuonSoLuong2")
    @Expose
    private Integer kDBanBuonSoLuong2;
    @SerializedName("kD_BanBuonDieuKienMoTa")
    @Expose
    private String kDBanBuonDieuKienMoTa;
    @SerializedName("kD_BanLeGia")
    @Expose
    private Integer kDBanLeGia;
    @SerializedName("kD_CV")
    @Expose
    private Integer kDCV;
    @SerializedName("kH_BanLeGia")
    @Expose
    private Integer kHBanLeGia;
    @SerializedName("kH_CV")
    @Expose
    private Integer kHCV;
    @SerializedName("kD_BanBuonLN")
    @Expose
    private Integer kDBanBuonLN;
    @SerializedName("kD_BanBuonTiLe")
    @Expose
    private Double kDBanBuonTiLe;
    @SerializedName("kD_BanLeLN")
    @Expose
    private Integer kDBanLeLN;
    @SerializedName("kD_BanLeTiLe")
    @Expose
    private Double kDBanLeTiLe;
    @SerializedName("kH_BanLeLN")
    @Expose
    private Integer kHBanLeLN;
    @SerializedName("kH_BanLeTiLe")
    @Expose
    private Double kHBanLeTiLe;

    @SerializedName("kH_DiemThuong")
    private int coint;

    public int getCoint() {
        return coint;
    }

    public void setCoint(int coint) {
        this.coint = coint;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDDHangHoaID() {
        return dDHangHoaID;
    }

    public void setDDHangHoaID(Integer dDHangHoaID) {
        this.dDHangHoaID = dDHangHoaID;
    }

    public String getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(String ngayTao) {
        this.ngayTao = ngayTao;
    }

    public String getNguoiTao() {
        return nguoiTao;
    }

    public void setNguoiTao(String nguoiTao) {
        this.nguoiTao = nguoiTao;
    }

    public String getNgayKetThuc() {
        return ngayKetThuc;
    }

    public void setNgayKetThuc(String ngayKetThuc) {
        this.ngayKetThuc = ngayKetThuc;
    }

    public Boolean getIsInUse() {
        return isInUse;
    }

    public void setIsInUse(Boolean isInUse) {
        this.isInUse = isInUse;
    }

    public Integer getDMDonViID() {
        return dMDonViID;
    }

    public void setDMDonViID(Integer dMDonViID) {
        this.dMDonViID = dMDonViID;
    }

    public String getDonVi() {
        return donVi;
    }

    public void setDonVi(String donVi) {
        this.donVi = donVi;
    }

    public Integer getGiaNhap() {
        return giaNhap;
    }

    public void setGiaNhap(Integer giaNhap) {
        this.giaNhap = giaNhap;
    }

    public Integer getKDBanBuonGia() {
        return kDBanBuonGia;
    }

    public void setKDBanBuonGia(Integer kDBanBuonGia) {
        this.kDBanBuonGia = kDBanBuonGia;
    }

    public String getKDBanBuonDieuKien() {
        return kDBanBuonDieuKien;
    }

    public void setKDBanBuonDieuKien(String kDBanBuonDieuKien) {
        this.kDBanBuonDieuKien = kDBanBuonDieuKien;
    }

    public Integer getLoaidieukien() {
        return loaidieukien;
    }

    public void setLoaidieukien(Integer loaidieukien) {
        this.loaidieukien = loaidieukien;
    }

    public Float getKDBanBuonSoLuong1() {
        return kDBanBuonSoLuong1;
    }

    public void setKDBanBuonSoLuong1(Float kDBanBuonSoLuong1) {
        this.kDBanBuonSoLuong1 = kDBanBuonSoLuong1;
    }

    public Integer getKDBanBuonSoLuong2() {
        return kDBanBuonSoLuong2;
    }

    public void setKDBanBuonSoLuong2(Integer kDBanBuonSoLuong2) {
        this.kDBanBuonSoLuong2 = kDBanBuonSoLuong2;
    }

    public String getKDBanBuonDieuKienMoTa() {
        return kDBanBuonDieuKienMoTa;
    }

    public void setKDBanBuonDieuKienMoTa(String kDBanBuonDieuKienMoTa) {
        this.kDBanBuonDieuKienMoTa = kDBanBuonDieuKienMoTa;
    }

    public Integer getKDBanLeGia() {
        return kDBanLeGia;
    }

    public void setKDBanLeGia(Integer kDBanLeGia) {
        this.kDBanLeGia = kDBanLeGia;
    }

    public Integer getKDCV() {
        return kDCV;
    }

    public void setKDCV(Integer kDCV) {
        this.kDCV = kDCV;
    }

    public Integer getKHBanLeGia() {
        return kHBanLeGia;
    }

    public void setKHBanLeGia(Integer kHBanLeGia) {
        this.kHBanLeGia = kHBanLeGia;
    }

    public Integer getKHCV() {
        return kHCV;
    }

    public void setKHCV(Integer kHCV) {
        this.kHCV = kHCV;
    }

    public Integer getKDBanBuonLN() {
        return kDBanBuonLN;
    }

    public void setKDBanBuonLN(Integer kDBanBuonLN) {
        this.kDBanBuonLN = kDBanBuonLN;
    }

    public Double getKDBanBuonTiLe() {
        return kDBanBuonTiLe;
    }

    public void setKDBanBuonTiLe(Double kDBanBuonTiLe) {
        this.kDBanBuonTiLe = kDBanBuonTiLe;
    }

    public Integer getKDBanLeLN() {
        return kDBanLeLN;
    }

    public void setKDBanLeLN(Integer kDBanLeLN) {
        this.kDBanLeLN = kDBanLeLN;
    }

    public Double getKDBanLeTiLe() {
        return kDBanLeTiLe;
    }

    public void setKDBanLeTiLe(Double kDBanLeTiLe) {
        this.kDBanLeTiLe = kDBanLeTiLe;
    }

    public Integer getKHBanLeLN() {
        return kHBanLeLN;
    }

    public void setKHBanLeLN(Integer kHBanLeLN) {
        this.kHBanLeLN = kHBanLeLN;
    }

    public Double getKHBanLeTiLe() {
        return kHBanLeTiLe;
    }

    public void setKHBanLeTiLe(Double kHBanLeTiLe) {
        this.kHBanLeTiLe = kHBanLeTiLe;
    }

}
