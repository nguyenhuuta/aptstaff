package com.anphuocthai.staff.ui.delivery.myorder;

import com.anphuocthai.staff.ui.base.MvpView;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;

import java.util.ArrayList;

public interface MyOrderMvpView extends MvpView {

    void displayOrderTrans(ArrayList<OrderTran> orderTrans);

    void hideRecyclerView(boolean isHide);

    void showAdjustDialog(OrderTran orderTran);

    void onCancelToEditOrderSuccess(String message, OrderTran orderTran);

    void onDeleteCartSuccess(OrderTran orderTran);

    void onCreateProductSuccess(OrderTran orderTran);

    void resetLoadMoreData(OrderTran orderTran);

}