
package com.anphuocthai.staff.ui.home.statistics.viewholder.generalstatistic;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class APTIndex {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("val")
    @Expose
    private Float val;
    @SerializedName("body")
    @Expose
    private String body;
    @SerializedName("typeId")
    @Expose
    private Integer typeId;
    @SerializedName("backColor")
    @Expose
    private String backColor;
    @SerializedName("foreColor")
    @Expose
    private String foreColor;
    @SerializedName("cssClass")
    @Expose
    private String cssClass;
    @SerializedName("cssStyle")
    @Expose
    private String cssStyle;
    @SerializedName("orderId")
    @Expose
    private Integer orderId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Float getVal() {
        return val;
    }

    public void setVal(Float val) {
        this.val = val;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public String getBackColor() {
        return backColor;
    }

    public void setBackColor(String backColor) {
        this.backColor = backColor;
    }

    public String getForeColor() {
        return foreColor;
    }

    public void setForeColor(String foreColor) {
        this.foreColor = foreColor;
    }

    public String getCssClass() {
        return cssClass;
    }

    public void setCssClass(String cssClass) {
        this.cssClass = cssClass;
    }

    public String getCssStyle() {
        return cssStyle;
    }

    public void setCssStyle(String cssStyle) {
        this.cssStyle = cssStyle;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public boolean isTongPhat() {
        return "Tổng phạt".equals(title);
    }
}
