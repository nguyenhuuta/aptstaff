package com.anphuocthai.staff.ui.home.statistics.viewholder.generalstatistic;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.View;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.error.ANError;
import com.anphuocthai.staff.R;
import com.anphuocthai.staff.api.ApiURL;
import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.interfaces.IAPICallback;
import com.anphuocthai.staff.ui.base.BaseViewHolder;
import com.anphuocthai.staff.utils.App;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class GeneralStatisticViewHolder extends BaseViewHolder {

    private RecyclerView recyclerView;

    private Context context;

    public static int convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return (int) (dp * (metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public GeneralStatisticViewHolder(View itemView, Context context) {
        super(itemView);
        this.context = context;
        recyclerView = itemView.findViewById(R.id.general_statistic_recycler_view);
        recyclerView.setNestedScrollingEnabled(false);

        if (App.getStatistics() != null) {
            setupAdapter(App.getStatistics());
        } else {
            getDynamicStatistic();
        }
    }


    private void getDynamicStatistic() {
        NetworkManager.getInstance().sendGetRequest(ApiURL.GET_DYNAMIC_SUMARY_HOME, null, new IAPICallback() {
            @Override
            public void onResponseSuccess(JSONObject response) {

            }

            @Override
            public void onResponseSuccess(JSONArray response) {
                if (response != null) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<ArrayList<APTIndex>>() {
                    }.getType();
                    ArrayList<APTIndex> aptIndexArrayList = gson.fromJson(response.toString(), type);
                    setupAdapter(aptIndexArrayList);
                }
            }

            @Override
            public void onResponseError(ANError anError) {

            }
        });
    }

    private void setupAdapter(ArrayList<APTIndex> statistics) {

        GridLayoutManager glm = new GridLayoutManager(context, 3);
        GridAdapter adapter = new GridAdapter(convertDpToPixel(4, context), statistics);
        adapter.setSpanCount(3);
        recyclerView.addItemDecoration(adapter.getItemDecorationManager());
        glm.setSpanSizeLookup(adapter.getSpanSizeLookup());
        recyclerView.setLayoutManager(glm);
        recyclerView.setAdapter(adapter);

        List<BaseModel> data = new ArrayList<>();


        for (int i = 0; i < statistics.size(); i++) {
            data.add(GridItem.generateGridItem(i, statistics.get(i).getBackColor()));
        }
        adapter.addData(data);
    }

    @Override
    protected void clear() {
    }

    @Override
    public void onBind(int position) {
        super.onBind(position);
    }
}
