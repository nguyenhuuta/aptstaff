package com.anphuocthai.staff.ui.profile.personalinfo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.adapters.profile.ProfileAdapter;
import com.anphuocthai.staff.di.component.ActivityComponent;
import com.anphuocthai.staff.interfaces.IYNDialogCallback;
import com.anphuocthai.staff.ui.base.BaseFragment;
import com.anphuocthai.staff.ui.login.LoginActivity;
import com.anphuocthai.staff.utils.App;
import com.anphuocthai.staff.utils.ui.UIUtils;

import javax.inject.Inject;


public class ProfileFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener, ProfileMvpView {

    private static final String TAG = ProfileFragment.class.getSimpleName();

    private RecyclerView profileRecyclerView;

    private RecyclerView.LayoutManager aLayout;

    private SwipeRefreshLayout mSwipeRefreshLayout;

    private Button btnRedirectToChangePass;

    private Button btnLogout;

    public ViewPager viewPager;

    private Context context;

    @Inject
    ProfileMvpPresenter<ProfileMvpView> mvpPresenter;


    public ProfileFragment() {
        // Required empty public constructor
        this.context = App.getAppContext();
    }

    public static ProfileFragment newInstance(ViewPager viewPager) {
        ProfileFragment fragment = new ProfileFragment();
        fragment.viewPager = viewPager;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getActivityComponent().inject(this);
//        mvpPresenter.onAttach(this);
    }

    @Override
    protected void setUp(View view) {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            mvpPresenter.onAttach(this);
        }

        // Inflate the layout for this fragment
        profileRecyclerView = view.findViewById(R.id.recycler_profile);
        btnRedirectToChangePass = view.findViewById(R.id.btn_redirect_change_pass);
        btnLogout = view.findViewById(R.id.btn_logout);


        btnRedirectToChangePass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPager.setCurrentItem(5, true);
            }
        });

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                UIUtils.showYesNoDialog(getBaseActivity(), "", getActivity().getString(R.string.confirm_logout), new IYNDialogCallback() {
                    @Override
                    public void accept() {
                        mvpPresenter.onChangePass();
                        Intent i = new Intent(context, LoginActivity.class);
                        startActivity(i);
                    }

                    @Override
                    public void cancel() {

                    }
                });

            }
        });

        profileRecyclerView.setHasFixedSize(true);
        aLayout = new LinearLayoutManager(context);
        profileRecyclerView.setLayoutManager(aLayout);
        setupPullToRefresh(view);
        fetchUserProfile();
        return view;
    }

    private void fetchUserProfile() {
        mSwipeRefreshLayout.setRefreshing(true);
        ProfileAdapter adapter = new ProfileAdapter(App.getUserInfo(), context);
        profileRecyclerView.setAdapter(adapter);
        mSwipeRefreshLayout.setRefreshing(false);
    }


    /**
     * This method is called when swipe refresh is pulled down
     */
    @Override
    public void onRefresh() {
        // Fetching data from server
        fetchUserProfile();
    }

    private void setupPullToRefresh(View view) {
        // SwipeRefreshLayout
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container_profile);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);
        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(true);
                // Fetching data from server
                fetchUserProfile();
            }
        });
    }



}