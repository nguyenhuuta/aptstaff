package com.anphuocthai.staff.ui.product;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.view.MenuItemCompat;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.data.DataManager;
import com.anphuocthai.staff.data.db.model.OrderProduct;
import com.anphuocthai.staff.model.order.ImageProductModel;
import com.anphuocthai.staff.model.order.Product;
import com.anphuocthai.staff.ui.base.BaseActivity;
import com.anphuocthai.staff.ui.ordercart.OrderCartActivity;
import com.anphuocthai.staff.utils.Enum;
import com.anphuocthai.staff.utils.Utils;
import com.google.android.material.appbar.AppBarLayout;
import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;

import static com.anphuocthai.staff.api.ApiURL.IMAGE_VIEW_URL;
import static com.anphuocthai.staff.utils.Constants.USER_SETTING_FILE;

/**
 * Note: Lưu lại các chi tiết nhỏ của sản phẩm đã đặt, chứ không lưu hết
 * các chi tiết khác sẽ được load lại từ server
 */

public class DetailProductActivity extends BaseActivity implements DetailProductMvpView {

    private static final String TAG = DetailProductActivity.class.getSimpleName();

    @Inject
    DetailProductMvpPresenter<DetailProductMvpView> mPresenter;

    @Inject
    DataManager dataManager;


    private Toolbar toolbar;
    private TextView btnAddToCart;
    private TextView txtProductName;
    private TextView txtProductCode;
    private ImageView imgProduct;
    private TextView txtPrice;
    private TextView txtDesciptionProduct;
    private TextView textCartItemCount;
    private int mCartItemCount = 0;
    private Product product;
    private int productId;
    private LinearLayout containerImage;
    private TextView txtWholeSaleCV;

    private TextView txtRetailCV;

    private TextView txtRetailPrice;

    private TextView txtDetail;


    private SharedPreferences mSharedPreferences;
    private AppBarLayout mAppBarLayout;

    public void setStatusBarColor(String color) {
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.parseColor(color));
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarColor("#0022FF00");
        setContentView(R.layout.activity_detail_product);
        mSharedPreferences = getSharedPreferences(USER_SETTING_FILE, Context.MODE_PRIVATE);
        getmActivityComponent().inject(this);
        mPresenter.onAttach(this);
        initUI();
        handleIntent();
        initEvent();
        getProductById();


    }

    private void handleIntent() {
        Bundle b = getIntent().getBundleExtra("productExtra");
        productId = b.getInt("productid", 0);
        int thumbnailId = b.getInt("imageProduct", 0);
        supportPostponeEnterTransition();
        imgProduct.getViewTreeObserver().addOnPreDrawListener(
                new ViewTreeObserver.OnPreDrawListener() {
                    @Override
                    public boolean onPreDraw() {
                        imgProduct.getViewTreeObserver().removeOnPreDrawListener(this);
                        supportStartPostponedEnterTransition();
                        return true;
                    }
                }
        );
        Picasso.get()
                .load(IMAGE_VIEW_URL + thumbnailId)
                .fit()
                .centerCrop()
                .into(imgProduct);

    }

    private void initUI() {
        toolbar = findViewById(R.id.toolbarDetailProduct);
        mAppBarLayout = findViewById(R.id.appbar);
        View statusBar = findViewById(R.id.statusBar);
//        mAppBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
//            @Override
//            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
//                float alpha = (float) (Math.abs(verticalOffset) * 1.0 / appBarLayout.getTotalScrollRange());
//                statusBar.setAlpha(alpha);
//            }
//        });


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setTitle(getString(R.string.order_product_detail_title));

        toolbar.setNavigationIcon(R.drawable.ic_arrow_white_24dp);
        toolbar.setNavigationOnClickListener(view -> ActivityCompat.finishAfterTransition(this));

        txtRetailCV = findViewById(R.id.txtRetailCV);
        txtRetailPrice = findViewById(R.id.txtRetailPrice);
        txtWholeSaleCV = findViewById(R.id.txtWholeCV);
        txtDetail = findViewById(R.id.txt_detail);


        btnAddToCart = findViewById(R.id.btn_add_to_cart);
        txtProductName = findViewById(R.id.order_txt_product_name);
        txtProductCode = findViewById(R.id.order_txt_product_code);
        imgProduct = findViewById(R.id.order_img_product);
        txtPrice = findViewById(R.id.order_txt_price_product);
        txtDesciptionProduct = findViewById(R.id.order_txt_description_product);
        containerImage = findViewById(R.id.containerImage);
        mPresenter.onSetDefaultQuantity();
    }

    private void initEvent() {
        btnAddToCart.setOnClickListener(view -> {
            mPresenter.onAddToCartButtonClick(product.toOrderProduct(mSharedPreferences.getString("customer_name", getString(R.string.order_cart_no_customer)),
                    mSharedPreferences.getString("customer_guid", "")));
            supportFinishAfterTransition();
        });

    }

    private void getProductById() {
        mPresenter.onGetProductById(productId);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.getAllOrderProduct();
    }

    @Override
    public void openOrderCartActivity() {
        Intent intent = OrderCartActivity.getOrderCartActivityIntent(this);
        Bundle bundle = new Bundle();
        bundle.putBoolean("isEditOrder", false);
        intent.putExtra("OrderCartExtra", bundle);
        startActivity(intent);
    }

    @Override
    public void displayProduct(Product product) {
        this.product = product;
        txtProductName.setText(product.getName());
        txtProductCode.setText(getString(R.string.order_product_code) + ": " + product.getCommodityCode());

        txtWholeSaleCV.setText(product.getMainPrice().getKDCV() + " CV");
        txtRetailCV.setText(product.getMainPrice().getKHCV() + " CV");
        txtRetailPrice.setText(Utils.formatValue(product.getMainPrice().getKHBanLeGia(), Enum.FieldValueType.CURRENCY));
        txtPrice.setText(Utils.formatValue(product.getMainPrice().getKDBanBuonGia(), Enum.FieldValueType.CURRENCY) + "/"
                + Utils.formatValue(product.getMainPrice().getKDBanLeGia(), Enum.FieldValueType.CURRENCY));
        displayDetailProduct(product);

        mPresenter.onUpdatePrice();
        txtDesciptionProduct.setText(product.getMainPrice().getKDBanBuonDieuKienMoTa());
        int width = Utils.convertDpToPixel(this, 80f);
        int space = Utils.convertDpToPixel(this, 10f);
        List<ImageProductModel> list = product.getImages();
        for (ImageProductModel image : list) {
            ImageView imageView = new ImageView(this);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(width, width);
            lp.setMargins(0, 0, space, 0);
            imageView.setLayoutParams(lp);
            imageView.setTag(image.getUrl());
            Picasso.get()
                    .load(image.getUrl())
                    .placeholder(R.drawable.ic_no_image)
                    .fit()
                    .centerCrop()
                    .into(imageView);
            imageView.setOnClickListener(view -> {
                String tag = (String) view.getTag();
                try {
                    Bitmap bitmap = ((BitmapDrawable) ((ImageView) view).getDrawable()).getBitmap();
                    imgProduct.setImageBitmap(bitmap);
                } catch (Exception e) {
                    Picasso.get()
                            .load(IMAGE_VIEW_URL + tag)
                            .placeholder(R.drawable.ic_no_image)
                            .fit()
                            .centerCrop()
                            .into(imgProduct);
                }


            });
            containerImage.addView(imageView);
        }
    }

    private void displayDetailProduct(Product product) {

        String format = "Đóng gói: %s \nBảo quản: %s\nXuất xứ: %s\nThương hiệu: %s\nTồn kho: %s";
        txtDetail.setText(String.format(format,
                product.getProperties().getDongGoi(),
                product.getProperties().getBaoQuan(),
                product.getSource(),
                product.getTradeMark(),
                String.valueOf(product.getTonKho())));
    }

    @Override
    public void setPriceFeeAndCV(float price, float sumfee, int cv) {
        txtPrice.setText(Utils.formatValue(product.getMainPrice().getKDBanBuonGia(), Enum.FieldValueType.CURRENCY) + "/"
                + Utils.formatValue(product.getMainPrice().getKDBanLeGia(), Enum.FieldValueType.CURRENCY));
    }

    @Override
    public void onUpdateShoppingCart(List<OrderProduct> orderProducts) {
        mCartItemCount = orderProducts.size();
        setupBadge();
    }

    @Override
    public void onCreateOrderSuccess() {
        finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cart_order, menu);

        final MenuItem menuItem = menu.findItem(R.id.menu_shopping_cart);
        menu.findItem(R.id.menu_grid_or_list).setVisible(false);
        menu.findItem(R.id.menu_clear_all).setVisible(false);
        View actionView = MenuItemCompat.getActionView(menuItem);
        textCartItemCount = actionView.findViewById(R.id.txt_number_item);
        setupBadge();

        actionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menuItem);
            }
        });


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.menu_shopping_cart) {
            mPresenter.onCartButtonClick();
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setupBadge() {

        if (textCartItemCount != null) {
            if (mCartItemCount == 0) {
                if (textCartItemCount.getVisibility() != View.GONE) {
                    textCartItemCount.setVisibility(View.GONE);
                }
            } else {
                textCartItemCount.setText(String.valueOf(Math.min(mCartItemCount, 99)));
                if (textCartItemCount.getVisibility() != View.VISIBLE) {
                    textCartItemCount.setVisibility(View.VISIBLE);
                }
            }
        }
    }


}
