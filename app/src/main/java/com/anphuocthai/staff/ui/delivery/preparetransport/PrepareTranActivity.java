package com.anphuocthai.staff.ui.delivery.preparetransport;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ahamed.multiviewadapter.DataListManager;
import com.ahamed.multiviewadapter.SelectableAdapter;
import com.ahamed.multiviewadapter.listener.MultiSelectionChangedListener;
import com.ahamed.multiviewadapter.util.ItemDecorator;
import com.ahamed.multiviewadapter.util.SimpleDividerDecoration;
import com.anphuocthai.staff.R;
import com.anphuocthai.staff.interfaces.IYNDialogCallback;
import com.anphuocthai.staff.ui.base.BaseActivity;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;
import com.anphuocthai.staff.utils.ui.UIUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;

public class PrepareTranActivity extends BaseActivity implements PrepareTranMvpView {

    @Inject
    PrepareTranMvpPresenter<PrepareTranMvpView> mPresenter;

    @Inject
    PrepareTranAdapter mPrepareTranAdapter;

    @Inject
    LinearLayoutManager mLayoutManager;

    SingleSelectionBinder singleSelectionBinder;

    List<OrderTran> orderTransSelected = new ArrayList<>();

    List<OrderTran> tempOrderTrans = new ArrayList<>();

    private boolean isSelectAll = false;

    private View emptyView;
    private TextView txtRetry;
    private Button btnRetry;

    private RecyclerView recyclerView;
    private Button btnConfirmTransport;
    private TextView emptyList;

    DataListManager<OrderTran> selectableItemsManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prepare_tran);
        getmActivityComponent().inject(this);
        mPresenter.onAttach(this);
        setupToolbar(getString(R.string.order_prepare_tran_toolbar_title));
        initView();
        setUpAdapter();
    }

    private void initView() {
        emptyView = findViewById(R.id.empty_view_prepare);
        txtRetry = emptyView.findViewById(R.id.txt_retry);
        btnRetry = emptyView.findViewById(R.id.btn_retry);
        txtRetry.setVisibility(View.GONE);
        btnRetry.setVisibility(View.GONE);

        recyclerView = findViewById(R.id.transport_prepare_recycler_view);
        emptyList = findViewById(R.id.prepare_tran_txt_empty);
        btnConfirmTransport = findViewById(R.id.btn_confirm_transport);
        btnConfirmTransport.setOnClickListener((View v)-> {
            if (orderTransSelected != null && !orderTransSelected.isEmpty()) {
                UIUtils.showYesNoDialog(this, "", getString(R.string.transport_confirm_orders), new IYNDialogCallback() {
                    @Override
                    public void accept() {
                        mPresenter.confirmTransport(orderTransSelected);
                    }

                    @Override
                    public void cancel() {
                    }
                });
            } else {
                showMessage(getString(R.string.transport_not_select));
            }
        });

    }

    @SuppressLint("RestrictedApi")
    protected void setUpAdapter() {
        ItemDecorator itemDecorator =
                new SimpleDividerDecoration(this, SimpleDividerDecoration.VERTICAL);
        SelectableAdapter adapter = new SelectableAdapter();
        selectableItemsManager = new DataListManager<>(adapter);

        selectableItemsManager.setMultiSelectionChangedListener(new MultiSelectionChangedListener<OrderTran>() {
            @Override
            public void onMultiSelectionChangedListener(List<OrderTran> selectedItems) {
                orderTransSelected.clear();
                orderTransSelected.addAll(selectedItems);
            }
        });

        adapter.addDataManager(selectableItemsManager);
        singleSelectionBinder = new SingleSelectionBinder(itemDecorator, mPresenter);
        adapter.registerBinder(singleSelectionBinder);
        mPresenter.setSingleSelectionBinder(singleSelectionBinder);
        adapter.setSelectionMode(SelectableAdapter.SELECTION_MODE_MULTIPLE);

        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        recyclerView.addItemDecoration(adapter.getItemDecorationManager());
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(adapter);


        // hide view when start

        //getToolbar().getMenu().getItem(0).setVisible(false);

        recyclerView.setVisibility(View.GONE);
        btnConfirmTransport.setVisibility(View.GONE);
        emptyView.setVisibility(View.VISIBLE);


        mPresenter.getAllPrepareTranOrders();
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void displayOrderTrans(ArrayList<OrderTran> orderTrans) {

        for (Iterator<OrderTran> it = orderTrans.iterator(); it.hasNext();) {
            if (it.next().getDDTrangThaiID() != 31) {
                it.remove();
            }
        }

        try {
            if (orderTrans.size() <= 0) {
                getToolbar().getMenu().getItem(0).setVisible(false);
                recyclerView.setVisibility(View.GONE);
                btnConfirmTransport.setVisibility(View.GONE);
                emptyView.setVisibility(View.VISIBLE);
            }else {
                getToolbar().getMenu().getItem(0).setVisible(true);
                recyclerView.setVisibility(View.VISIBLE);
                btnConfirmTransport.setVisibility(View.VISIBLE);
                emptyView.setVisibility(View.GONE);
            }

        }catch (Exception e) {
            e.printStackTrace();
        }

        tempOrderTrans = orderTrans;

        selectableItemsManager.set(orderTrans);
    }

    public static Intent getPrepareTranIntent(Context context) {
        Intent intent = new Intent(context, PrepareTranActivity.class);
        return intent;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.select_all_receive_from_inventory_menu, menu);
        MenuItem menuItem = menu.findItem(R.id.action_select_all);
        menuItem.setVisible(false);
        return true;
    }

    @SuppressLint("RestrictedApi")
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_select_all) {

            isSelectAll = !isSelectAll;

            if (isSelectAll) {
                getToolbar().getMenu().getItem(0).setIcon(R.drawable.ic_check_all_selected);
                selectableItemsManager.setSelectedItems(tempOrderTrans);
                //selectableItemsManager.set(tempOrderTrans);
            } else {
                getToolbar().getMenu().getItem(0).setIcon(R.drawable.ic_check_all);
                selectableItemsManager.clearSelectedItems();
            }

            return true;
        }

        return super.onOptionsItemSelected(item);
    }



}
