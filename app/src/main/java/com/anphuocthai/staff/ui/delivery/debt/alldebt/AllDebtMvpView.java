package com.anphuocthai.staff.ui.delivery.debt.alldebt;

import com.anphuocthai.staff.ui.base.MvpView;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;

import java.util.List;

public interface AllDebtMvpView extends MvpView {
    void updateOrderTran(List<OrderTran> orderTranArrayList);
}
