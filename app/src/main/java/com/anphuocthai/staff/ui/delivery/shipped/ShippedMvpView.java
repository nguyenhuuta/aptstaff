package com.anphuocthai.staff.ui.delivery.shipped;

import com.anphuocthai.staff.ui.base.MvpView;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;

import java.util.ArrayList;

public interface ShippedMvpView extends MvpView {
    void displayOrderTrans(ArrayList<OrderTran> orderTrans);
    void onShowFinishShipDialog(OrderTran item);
}
