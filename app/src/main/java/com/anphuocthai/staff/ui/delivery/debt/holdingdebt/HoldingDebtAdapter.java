package com.anphuocthai.staff.ui.delivery.debt.holdingdebt;

import android.content.Context;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.model.CongNoModel;
import com.anphuocthai.staff.ui.base.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HoldingDebtAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private List<CongNoModel> orderTranArrayList;
    private Context context;
    private HoldingDebtMvpView callback;

    public HoldingDebtAdapter(ArrayList<CongNoModel> orderTranArrayList) {
        this.orderTranArrayList = orderTranArrayList;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setCallback(HoldingDebtMvpView callback) {
        this.callback = callback;
    }

    public ArrayList<CongNoModel> getItemsChecked() {
        ArrayList<CongNoModel> list = new ArrayList<>();
        for (CongNoModel congNoModel : orderTranArrayList) {
            if (congNoModel.isCheck()) {
                list.add(congNoModel);
            }
        }
        return list;
    }

    public void removeItem(CongNoModel congNoModel) {
        orderTranArrayList.remove(congNoModel);
        notifyDataSetChanged();
    }

    public void removeItemChecked() {
        ArrayList<CongNoModel> list = new ArrayList<>();
        for (CongNoModel congNoModel : orderTranArrayList) {
            if (congNoModel.isCheck()) {
                list.add(congNoModel);
            }
        }
        orderTranArrayList.removeAll(list);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new ViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_congno_damuon, parent, false));
            case VIEW_TYPE_EMPTY:
            default:
                return new EmptyViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_view, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);

    }

    @Override
    public int getItemCount() {
        if (orderTranArrayList != null && orderTranArrayList.size() > 0) {
            return orderTranArrayList.size();
        }
        return 1;

    }

    @Override
    public int getItemViewType(int position) {
        if (orderTranArrayList != null && orderTranArrayList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    public int countItem() {
        if (orderTranArrayList != null) {
            return orderTranArrayList.size();
        }
        return 0;
    }

    public void addItems(List<CongNoModel> orderTrans) {
        orderTranArrayList.clear();
        orderTranArrayList.addAll(orderTrans);
        notifyDataSetChanged();
    }


    public class ViewHolder extends BaseViewHolder {
        TextView txtOrderCode;
        TextView txtCustomerName;
        TextView labelSumaryMoney;
        TextView txtSumaryMoney;
        TextView labelMustPay;
        TextView txtMustPay;
        TextView labelRemain;
        TextView txtRemain;


        TextView txtTimeRemain;
        TextView btnStopDebt;
        TextView txtReturnOrder;
        CheckBox checkBox;

        CountDownTimer countDownTimer;

        public ViewHolder(View itemView) {
            super(itemView);
            initView(itemView);
        }

        private void initView(View itemView) {
            txtOrderCode = itemView.findViewById(R.id.all_debt_txt_order_code);
            txtCustomerName = itemView.findViewById(R.id.all_debt_txt_order_customer_name);
            labelSumaryMoney = itemView.findViewById(R.id.debt_label_sumary_money);
            txtSumaryMoney = itemView.findViewById(R.id.debt_txt_sumary_money);
            labelMustPay = itemView.findViewById(R.id.debt_label_must_pay);
            txtMustPay = itemView.findViewById(R.id.debt_txt_must_pay);
            labelRemain = itemView.findViewById(R.id.debt_label_remain);
            txtRemain = itemView.findViewById(R.id.debt_txt_remain);

            txtTimeRemain = itemView.findViewById(R.id.txt_time_remain);
            btnStopDebt = itemView.findViewById(R.id.btnCollectMoney);
            txtReturnOrder = itemView.findViewById(R.id.txt_return_order);
            checkBox = itemView.findViewById(R.id.checkBox);
        }

        protected void clear() {
        }

        public void onBind(int position) {
            super.onBind(position);
            final CongNoModel orderTran = orderTranArrayList.get(position);
            checkBox.setEnabled(orderTran.getTongTienDaThu() > 0);
            checkBox.setChecked(orderTran.isCheck());
            checkBox.setOnCheckedChangeListener((compoundButton, b) -> {
                orderTran.setCheck(b);
                if (callback != null) {
                    callback.onCheckBoxChange();
                }
            });


            txtOrderCode.setText(orderTran.orderCode());
            txtCustomerName.setText(orderTran.getContentDetail());

            txtSumaryMoney.setText(orderTran.getCongNoDonHangString());
            txtMustPay.setText(orderTran.getTongTienDaThuString());
            txtRemain.setText(orderTran.getSoTienConLaiString());

            int remain = orderTran.getSoTienConLai();
            if (remain == 0) {
                btnStopDebt.setBackground(ContextCompat.getDrawable(context, R.drawable.border_disable_view));
                btnStopDebt.setEnabled(false);
            } else {
                btnStopDebt.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_shape));
                btnStopDebt.setEnabled(true);
            }


            txtTimeRemain.setText("");
            int timeRemain = orderTran.getTimeRemain();
            if (timeRemain == 0) {
                txtTimeRemain.setText(R.string.common_end_time);
            } else {
                tiktok(timeRemain);
            }

            txtReturnOrder.setOnClickListener((View v) -> {
                if (callback != null) {
                    callback.traLaiHoaDon(orderTran);
                }
            });

            btnStopDebt.setOnClickListener((View v) -> {
                if (callback != null) {
                    callback.daThuTienn(orderTran);
                }
            });
        }


        private void tiktok(int time) {
            if (countDownTimer != null) {
                countDownTimer.cancel();
            }
            countDownTimer = new CountDownTimer(time * 1000, 1000) { // adjust the milli seconds here
                public void onTick(long millisUntilFinished) {
                    txtTimeRemain.setText("Còn: " + String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millisUntilFinished),
                            TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                            TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
                }

                public void onFinish() {
                    txtTimeRemain.setText(R.string.common_end_time);
                }
            }.start();
        }
    }

    public class EmptyViewHolder extends BaseViewHolder {

        @BindView(R.id.btn_retry)
        Button retryButton;

        @BindView(R.id.tv_message)
        TextView messageTextView;

        @BindView(R.id.txt_retry)
        TextView txtRetry;

        public EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            retryButton.setVisibility(View.GONE);
            txtRetry.setVisibility(View.GONE);
        }

        @Override
        protected void clear() {

        }

        @OnClick(R.id.btn_retry)
        void onRetryClick() {
        }
    }
}
