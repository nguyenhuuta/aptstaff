package com.anphuocthai.staff.ui.delivery.debt.alldebt;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.di.component.ActivityComponent;
import com.anphuocthai.staff.ui.base.BaseFragment;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;
import com.anphuocthai.staff.ui.searchsimpledata.SearchSimpleActivity;
import com.anphuocthai.staff.ui.searchsimpledata.SearchSimpleItem;
import com.anphuocthai.staff.utils.Enum;
import com.anphuocthai.staff.utils.Utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

public class AllDebtFragment extends BaseFragment implements AllDebtMvpView, AllDebtAdapter.Callback, SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = AllDebtFragment.class.getSimpleName();

    @Inject
    AllDebtMvpPresenter<AllDebtMvpView> mPresenter;

    @Inject
    AllDebtAdapter mAdapter;

    @Inject
    LinearLayoutManager mLayoutManager;

    private RecyclerView recyclerView;

    private TextView labelSumaryCV;

    private TextView txtSumaryCV;


    private TextView labelSumaryMustCollect;

    private TextView txtSumaryMustCollect;

    private TextView labelSumaryAlreadyCollect;

    private TextView txtSumaryOrder;

    private TextView txtSumaryAlreadyCollect;

    private View sumaryContainer;

    private SwipeRefreshLayout mSwipeRefreshLayout;

    private List<OrderTran> mOrderTrans = new ArrayList<>();


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.all_dept_layout, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            mPresenter.onAttach(this);
            mAdapter.setCallback(this);
        }
        return view;
    }

    @Override
    protected void setUp(View view) {

        sumaryContainer = view.findViewById(R.id.layout_sumary);


        labelSumaryCV = view.findViewById(R.id.label_sumary_cv);

        txtSumaryCV = view.findViewById(R.id.txt_sumary_cv);

        labelSumaryCV.setVisibility(View.GONE);

        txtSumaryCV.setVisibility(View.GONE);

        labelSumaryMustCollect = view.findViewById(R.id.label_must_collect);

        txtSumaryMustCollect = view.findViewById(R.id.txt_must_collect);

        labelSumaryMustCollect.setVisibility(View.VISIBLE);
        txtSumaryMustCollect.setVisibility(View.VISIBLE);

        labelSumaryAlreadyCollect = view.findViewById(R.id.label_already_collect);

        txtSumaryAlreadyCollect = view.findViewById(R.id.txt_already_collect);

        labelSumaryAlreadyCollect.setVisibility(View.VISIBLE);
        txtSumaryAlreadyCollect.setVisibility(View.VISIBLE);

        txtSumaryOrder = view.findViewById(R.id.txt_number_order);


        recyclerView = view.findViewById(R.id.all_debt_recycler_view);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        setupPullToRefresh(view);
    }

    public static AllDebtFragment newInstance() {
        Bundle args = new Bundle();
        AllDebtFragment fragment = new AllDebtFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void updateOrderTran(List<OrderTran> orderTran) {
        this.mOrderTrans = orderTran;
        updateOrderTranUI(orderTran);
    }

    private void updateOrderTranUI(List<OrderTran> orderTrans) {
        mAdapter.addItems(orderTrans);

        if (orderTrans.size() <= 0) {
            sumaryContainer.setVisibility(View.GONE);
            mSwipeRefreshLayout.setRefreshing(false);
            return;
        } else {
            sumaryContainer.setVisibility(View.VISIBLE);
        }

        int sumMustCollect = 0;
        int sumAlreadyCollect = 0;

        for (int i = 0; i < orderTrans.size(); i++) {
            sumMustCollect += orderTrans.get(i).getPhaiThanhToan();
            sumAlreadyCollect += orderTrans.get(i).getDaThu();
        }

        txtSumaryMustCollect.setText(Utils.formatValue(sumMustCollect, Enum.FieldValueType.CURRENCY));
        txtSumaryAlreadyCollect.setText(Utils.formatValue(sumAlreadyCollect, Enum.FieldValueType.CURRENCY));
        String text = orderTrans.size() + " " + getString(R.string.common_order);
        txtSumaryOrder.setText(text);

        mSwipeRefreshLayout.setRefreshing(false);
    }


    private boolean isViewShown = false;

    @Override
    public void setUserVisibleHint(boolean visible) {
        super.setUserVisibleHint(visible);
        isViewShown = getView() != null;
    }


    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.setRefreshing(true);
        mPresenter.onViewPrepared();
    }
    @Override
    public void reloadData() {
        if (mPresenter != null && mOrderTrans != null) {
            mPresenter.onViewPrepared();
        }
    }
    private void setupPullToRefresh(View view) {
        // SwipeRefreshLayout
        mSwipeRefreshLayout = view.findViewById(R.id.swipe_container_all_debt);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);
        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
        if (!isViewShown) {
            mSwipeRefreshLayout.post(() -> {
                if (mOrderTrans.size() <= 0) {
                    onRefresh();
                } else {
                    updateOrderTran(mOrderTrans);
                }
            });
        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.filter_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_filter) {
            showFilterScreen();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showFilterScreen() {
        if (mOrderTrans != null && !mOrderTrans.isEmpty()) {
            Set<Integer> searchEmployeeIds = new HashSet<>();
            ArrayList<SearchSimpleItem> searchSimpleItems = new ArrayList<>();
            for (OrderTran orderTran : mOrderTrans) {
                boolean added = searchEmployeeIds.add(orderTran.getTrangthai());
                if (added) {
                    Integer id = orderTran.getTrangthai();
                    String name = orderTran.getTrangThaiText();
                    if (id == null) {
                        id = SearchSimpleActivity.ID_NULL;
                    }
                    searchSimpleItems.add(new SearchSimpleItem(id, name));
                }
            }

            if (!searchSimpleItems.isEmpty()) {
                Intent intent = new Intent(getActivity(), SearchSimpleActivity.class);
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList(SearchSimpleActivity.NAME_PARCELABLE_ARRAYLIST, searchSimpleItems);
                intent.putExtras(bundle);
                startActivityForResult(intent, SearchSimpleActivity.REQUEST_CODE_SELECT_CUS_TAB6);
            } else {
                Toast.makeText(getActivity(), "Danh sách lọc bị trống!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getActivity(), "Danh sách lọc bị trống!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SearchSimpleActivity.REQUEST_CODE_SELECT_CUS_TAB6) {
            if (resultCode == Activity.RESULT_OK) {
                int id = data.getIntExtra(SearchSimpleActivity.KEY_ID, SearchSimpleActivity.ID_NULL);
                ArrayList<OrderTran> searchOrderTran = new ArrayList<>();
                for (OrderTran orderTran : mOrderTrans) {
                    if (SearchSimpleActivity.ID_NULL == id) {
                        if (orderTran.getTrangthai() == null) {
                            searchOrderTran.add(orderTran);
                        }
                    } else if (orderTran.getTrangthai() != null && orderTran.getTrangthai() == id) {
                        searchOrderTran.add(orderTran);
                    }
                }
                updateOrderTranUI(searchOrderTran);

            } else if (resultCode == Activity.RESULT_CANCELED) {
                updateOrderTranUI(mOrderTrans);
            }
        }
    }
}
