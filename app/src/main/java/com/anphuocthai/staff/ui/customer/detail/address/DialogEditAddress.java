package com.anphuocthai.staff.ui.customer.detail.address;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.activities.customer.SpinnerAdapter;
import com.anphuocthai.staff.base.BaseDialogFragment;
import com.anphuocthai.staff.base.Type;
import com.anphuocthai.staff.baserequest.APICallback;
import com.anphuocthai.staff.baserequest.APIResponse;
import com.anphuocthai.staff.baserequest.APIService;
import com.anphuocthai.staff.baserequest.IAppAPI;
import com.anphuocthai.staff.data.address.DatabaseManager;
import com.anphuocthai.staff.entities.Cities;
import com.anphuocthai.staff.entities.District;
import com.anphuocthai.staff.entities.Wards;
import com.anphuocthai.staff.utils.Utils;
import com.google.gson.annotations.SerializedName;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by OpenYourEyes on 11/19/2019
 */
public class DialogEditAddress extends BaseDialogFragment {

    private Spinner mSpinCity, mSpinDistrict, mSpinWard;
    private EditText mNumberHouse;

    private List<Cities> citiesList;
    private List<District> districtList;
    private List<Wards> wardsList;

    DatabaseManager databaseManager = DatabaseManager.getInstance();
    private SpinnerAdapter<Cities> mCitiesArrayAdapter;
    private SpinnerAdapter<District> mDistrictArrayAdapter;
    private SpinnerAdapter<Wards> mWardsArrayAdapter;
    private boolean isDoneCity, isDoneDistrict, isDoneWards;

    private int idThanhVien;

    public static DialogEditAddress newInstance(int idThanhVien) {

        DialogEditAddress fragment = new DialogEditAddress();
        fragment.idThanhVien = idThanhVien;
        fragment.setArguments(new Bundle());
        return fragment;
    }

    @Override
    public int getContentView() {
        return R.layout.dialog_edit_address;
    }

    @Override
    public boolean dismissClickOutside() {
        return false;
    }

    @Override
    public String getTitleDialog() {
        return "Cập nhật địa chỉ";
    }

    @Override
    public void initView(View view) {
        mNumberHouse = view.findViewById(R.id.numberHouse);
        mSpinCity = view.findViewById(R.id.spinnerCity);
        mSpinDistrict = view.findViewById(R.id.spinnerDistrict);
        mSpinWard = view.findViewById(R.id.spinnerWard);
    }

    @Override
    public void initAction() {
    }

    @Override
    public void initData() {
        checkAddressDatabase();
    }

    @Override
    public void onButtonOKClick() {
        Cities cities = (Cities) mSpinCity.getSelectedItem();
        District district = (District) mSpinDistrict.getSelectedItem();
        Wards wards = (Wards) mSpinWard.getSelectedItem();
        String numberHouse = mNumberHouse.getText().toString();

        Param param = new Param(idThanhVien, cities.getTinh_ThanhPho_ID(), district.getQuan_Huyen_ID(), wards.getId(), numberHouse);
        showLoading();
        APIService.getInstance().getAppAPI().updateAddress(param).getAsyncResponse(new APICallback<APIResponse>() {
            @Override
            public void onSuccess(APIResponse apiResponse) {
                dismiss();
                if (mCallbackDialog != null) {
                    mCallbackDialog.onCallback(Type.SUCCESSFULLY, null);
                }
            }

            @Override
            public void onFailure(String errorMessage) {
                hideLoading();
            }
        });
    }


    private void checkAddressDatabase() {
        citiesList = databaseManager.getAllCities();
        districtList = databaseManager.getDistrict();
        wardsList = databaseManager.getWards();
        IAppAPI mApi = APIService.getInstance().getAppAPI();
        isDoneCity = citiesList.size() > 0;
        isDoneDistrict = districtList.size() > 0;
        isDoneWards = wardsList.size() > 0;
        checkDoneAPI(-1);
        if (!isDoneCity) {
            showLoading();
            int id = 1;
            mApi.getCities(new Cities.Params()).getAsyncResponse(new APICallback<List<Cities>>() {
                @Override
                public void onSuccess(List<Cities> cities) {
                    if (Utils.isNull(cities)) {
                        checkDoneAPI(id);
                        return;
                    }
                    citiesList.addAll(cities);
                    MyTask<Cities> task = new MyTask<>(DialogEditAddress.this, id);
                    task.execute(cities);
                }

                @Override
                public void onFailure(String errorMessage) {
                    checkDoneAPI(id);
                }
            });
        }
        if (!isDoneDistrict) {
            showLoading();
            int id = 2;
            mApi.getListDistrict(new District.Params()).getAsyncResponse(new APICallback<List<District>>() {
                @Override
                public void onSuccess(List<District> districts) {
                    if (Utils.isNull(districts)) {
                        checkDoneAPI(id);
                        return;
                    }
                    districtList.addAll(districts);
                    MyTask<District> task = new MyTask<>(DialogEditAddress.this, id);
                    task.execute(districts);
                }

                @Override
                public void onFailure(String errorMessage) {
                    checkDoneAPI(id);
                }
            });
        }
        if (!isDoneWards) {
            showLoading();
            int id = 3;
            mApi.getListWards(new Wards.Params()).getAsyncResponse(new APICallback<List<Wards>>() {
                @Override
                public void onSuccess(List<Wards> wards) {
                    if (Utils.isNull(wards)) {
                        checkDoneAPI(id);
                        return;
                    }
                    wardsList.addAll(wards);
                    MyTask<Wards> task = new MyTask<>(DialogEditAddress.this, id);
                    task.execute(wards);
                }

                @Override
                public void onFailure(String errorMessage) {
                    checkDoneAPI(id);
                }
            });
        }
    }

    private synchronized void checkDoneAPI(int id) {
        switch (id) {
            case 1:
                isDoneCity = true;
                break;
            case 2:
                isDoneDistrict = true;
                break;
            case 3:
                isDoneWards = true;
                break;
        }

        if (isDoneCity && isDoneDistrict && isDoneWards) {
            hideLoading();
            mCitiesArrayAdapter = new SpinnerAdapter<>(getContext(), citiesList);
            mSpinCity.setAdapter(mCitiesArrayAdapter);
            mSpinCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                    Cities cities = (Cities) adapterView.getItemAtPosition(position);
                    districtList.clear();
                    wardsList.clear();
                    List<District> list = databaseManager.getDistrict(cities.getTinh_ThanhPho_ID());
                    if (list.size() > 0) {
                        mSpinDistrict.setEnabled(true);
                        districtList.addAll(list);
                        List<Wards> wards = databaseManager.getWardsByDistrictId(districtList.get(0).getMa_QuanHuyen());
                        if (wards.size() > 0) {
                            mSpinWard.setEnabled(true);
                            mSpinWard.setBackgroundResource(R.drawable.border_spinner_enable);
                            wardsList.addAll(wards);
                        } else {
                            wardsList.add(Wards.newObject());
                            mSpinWard.setEnabled(false);
                            mSpinWard.setBackgroundResource(R.drawable.border_spinner_disable);
                        }
                    } else {
                        districtList.add(District.newObject());
                        mSpinDistrict.setEnabled(false);
                    }
                    mWardsArrayAdapter.notifyDataSetChanged();
                    mDistrictArrayAdapter.notifyDataSetChanged();


                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            mDistrictArrayAdapter = new SpinnerAdapter<>(getContext(), districtList);
            mSpinDistrict.setAdapter(mDistrictArrayAdapter);
            mSpinDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                    District district = (District) adapterView.getItemAtPosition(position);
                    wardsList.clear();
                    List<Wards> list = databaseManager.getWardsByDistrictId(district.getQuan_Huyen_ID());
                    if (list.size() > 0) {
                        mSpinWard.setEnabled(true);
                        mSpinWard.setBackgroundResource(R.drawable.border_spinner_enable);
                        wardsList.addAll(list);
                    } else {
                        mSpinWard.setEnabled(false);
                        mSpinWard.setBackgroundResource(R.drawable.border_spinner_disable);
                        wardsList.add(Wards.newObject());
                    }
                    mWardsArrayAdapter.notifyDataSetChanged();
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });


            mWardsArrayAdapter = new SpinnerAdapter<>(getContext(), wardsList);
            mSpinWard.setAdapter(mWardsArrayAdapter);
            mSpinWard.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                    // Wards wards = (Wards) adapterView.getItemAtPosition(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                }
            });

        }

    }

    private static class MyTask<T> extends AsyncTask<List<T>, Void, Void> {

        private WeakReference<DialogEditAddress> activityReference;
        private int id;

        MyTask(DialogEditAddress context, int id) {
            activityReference = new WeakReference<>(context);
            this.id = id;

        }

        @Override
        protected Void doInBackground(List<T>... lists) {
            if (lists.length == 0) return null;
            List<T> first = lists[0];
            switch (id) {
                case 1:
                    List<Cities> citiesList = (List<Cities>) first;
                    DatabaseManager.getInstance().insertCity(citiesList);
                    break;
                case 2:
                    List<District> districtList = (List<District>) first;
                    DatabaseManager.getInstance().inserDistrict(districtList);
                    break;
                case 3:
                    List<Wards> wardsList = (List<Wards>) first;
                    DatabaseManager.getInstance().insertWard(wardsList);
                    break;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            DialogEditAddress dialogEditAddress = activityReference.get();
            if (dialogEditAddress == null || dialogEditAddress.isDetached()) return;
            dialogEditAddress.checkDoneAPI(id);
        }
    }


    public static class Param {
        @SerializedName("iD_ThanhVien")
        int iD_ThanhVien;
        @SerializedName("tinhId")
        int tinhId;
        @SerializedName("QuanID")
        int QuanID;
        @SerializedName("XaID")
        int XaID;
        @SerializedName("SoNha")
        String SoNha;

        public Param(int iD_ThanhVien, int tinhId, int quanID, int xaID, String soNha) {
            this.iD_ThanhVien = iD_ThanhVien;
            this.tinhId = tinhId;
            QuanID = quanID;
            XaID = xaID;
            SoNha = soNha;
        }
    }

}
