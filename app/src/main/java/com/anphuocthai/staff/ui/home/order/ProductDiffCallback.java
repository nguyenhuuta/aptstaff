package com.anphuocthai.staff.ui.home.order;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DiffUtil;

import com.anphuocthai.staff.model.order.Product;

import java.util.List;

public class ProductDiffCallback extends DiffUtil.Callback {

    private final List<Product> mOldEmployeeList;
    private final List<Product> mNewEmployeeList;

    public ProductDiffCallback(List<Product> oldEmployeeList, List<Product> newEmployeeList) {
        this.mOldEmployeeList = oldEmployeeList;
        this.mNewEmployeeList = newEmployeeList;
    }

    @Override
    public int getOldListSize() {
        return mOldEmployeeList.size();
    }

    @Override
    public int getNewListSize() {
        return mNewEmployeeList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return mOldEmployeeList.get(oldItemPosition).getId() == mNewEmployeeList.get(
                newItemPosition).getId();
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        final Product oldEmployee = mOldEmployeeList.get(oldItemPosition);
        final Product newEmployee = mNewEmployeeList.get(newItemPosition);
        //return oldEmployee.isSelected() == newEmployee.isSelected();
        //return oldEmployee.getName().equals(newEmployee.getName());
        return java.util.Objects.equals(oldEmployee, newEmployee);
    }

    @Nullable
    @Override
    public Object getChangePayload(int oldItemPosition, int newItemPosition) {
        // Implement method if you're going to use ItemAnimator
        return super.getChangePayload(oldItemPosition, newItemPosition);
    }
}
