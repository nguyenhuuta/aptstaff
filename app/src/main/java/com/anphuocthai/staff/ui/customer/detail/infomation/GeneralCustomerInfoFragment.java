package com.anphuocthai.staff.ui.customer.detail.infomation;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.adapters.checkin.CustomerDetailAdapter;
import com.anphuocthai.staff.model.customer.SearchCustomer;
import com.anphuocthai.staff.ui.customer.detail.DetailCustomerActivity;


public class GeneralCustomerInfoFragment extends Fragment {

    SearchCustomer searchCustomer;
    CustomerDetailAdapter adapter;

    public static GeneralCustomerInfoFragment newInstance(SearchCustomer model) {
        GeneralCustomerInfoFragment generalCustomerInfoFragment = new GeneralCustomerInfoFragment();
        generalCustomerInfoFragment.searchCustomer = model;
        return generalCustomerInfoFragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_general_customer_info, container, false);

        RecyclerView detailCustomerRecyclerView = view.findViewById(R.id.recyclerViewDetailGeneralCustomer);
        detailCustomerRecyclerView.setHasFixedSize(true);
        detailCustomerRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new CustomerDetailAdapter(searchCustomer, getActivity());
        detailCustomerRecyclerView.setAdapter(adapter);

        Button btnEditGeneralInfo = view.findViewById(R.id.btn_edit_general_info);
        btnEditGeneralInfo.setOnClickListener(view1 -> {
            if (getActivity() instanceof DetailCustomerActivity) {
                DetailCustomerActivity activity = (DetailCustomerActivity) getActivity();
                activity.editCustomer();
            }
        });
        return view;
    }

}