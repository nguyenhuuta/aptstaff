package com.anphuocthai.staff.ui.home.statistics.viewholder;

import android.view.View;

import com.anphuocthai.staff.ui.base.BaseViewHolder;

public class NormalViewHolder extends BaseViewHolder {

    public NormalViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    protected void clear() {

    }
}
