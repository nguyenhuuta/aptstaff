package com.anphuocthai.staff.ui.home.order;

import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.data.DataManager;
import com.anphuocthai.staff.data.db.model.OrderProduct;
import com.anphuocthai.staff.ui.base.BasePresenter;
import com.anphuocthai.staff.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class OrderMainPresenter<V extends OrderMainMvpView> extends BasePresenter<V> implements OrderMainMvpPresenter<V> {

    ProductOrderAdapter adapter;


    @Inject
    public OrderMainPresenter(NetworkManager networkManager, DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(networkManager, dataManager, schedulerProvider, compositeDisposable);
    }


    @Override
    public boolean getIsShowGrid() {
        return getDataManager().getIsShowGrid();
    }


    @Override
    public void onSaveOrderProduct(OrderProduct orderProduct) {
        getCompositeDisposable().add(getDataManager()
                .insertOrderProduct(orderProduct)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(aBoolean -> getAllOrderProduct(), throwable -> {
                }));
    }

    @Override
    public void onDeleteOrderProduct(OrderProduct orderProduct) {
        getCompositeDisposable().add(getDataManager()
                .deleteOrderProduct(orderProduct)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(aBoolean -> getAllOrderProduct(), throwable -> {
                }));
    }

    @Override
    public void getAllOrderProduct() {
        getCompositeDisposable().add(getDataManager()
                .getAllOrderProducts()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(orderProducts -> {
                    getmMvpView().getAllProductLocal(orderProducts);
                }, throwable -> {

                }));
    }

    @Override
    public void getAllProductLocalWhenOrder() {
        getCompositeDisposable().add(getDataManager()
                .getAllOrderProducts()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(orderProducts -> {
                    getmMvpView().getAllProductLocalWhenOrder(orderProducts);
                }, throwable -> {

                }));
    }
}
