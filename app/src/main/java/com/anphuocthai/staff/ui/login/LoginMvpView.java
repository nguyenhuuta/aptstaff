package com.anphuocthai.staff.ui.login;

import com.anphuocthai.staff.ui.base.MvpView;

public interface LoginMvpView extends MvpView {

    void onLoginFailed(String message);

    void onLoginSuccess();
}
