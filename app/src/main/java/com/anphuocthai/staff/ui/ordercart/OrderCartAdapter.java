package com.anphuocthai.staff.ui.ordercart;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.data.TypeProduct;
import com.anphuocthai.staff.data.db.model.OrderProduct;
import com.anphuocthai.staff.model.ChietKhauModel;
import com.anphuocthai.staff.model.customer.Customer;
import com.anphuocthai.staff.ui.base.BaseActivity;
import com.anphuocthai.staff.ui.base.BaseViewHolder;
import com.anphuocthai.staff.ui.customer.detail.DetailCustomerActivity;
import com.anphuocthai.staff.ui.product.OrderLogic;
import com.anphuocthai.staff.utils.App;
import com.anphuocthai.staff.utils.Enum;
import com.anphuocthai.staff.utils.Utils;
import com.anphuocthai.staff.widget.RoundRectCornerImageView;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.OnClick;


public class OrderCartAdapter extends RecyclerView.Adapter<BaseViewHolder> {


    public static final int VIEW_TYPE_EMPTY = 0;

    public static final int VIEW_TYPE_NORMAL = 1;

    public static final int VIEW_TYPE_FIRST_ITEM = 2;

    public static final int VIEW_TYPE_SECOND_ITEM = 3;

    public static final int EXTEND_VALUE = 2;

    private BaseActivity baseActivity;

    private List<TypeProduct> mListProduct;
    private List<ChietKhauModel> mListChietKhau;

    private boolean isPrintPrice = true;
    private int percenChietKhau;


    OrderCartMvpPresenter<OrderCartMvpView> mPresenter;

    private Callback mCallback;

    private List<OrderProduct> mOrderProductList;


    private Customer customer;


    public void setCustomer(Customer customer) {
        this.customer = customer;
        notifyItemChanged(0);
    }

    private List<OrderLogic> orderLogics = new ArrayList<>();

    public void setPercenChietKhau(int percenChietKhau) {
        this.percenChietKhau = percenChietKhau;
    }

    public OrderCartAdapter(List<OrderProduct> orderProductList) {
        mOrderProductList = orderProductList;
        mListChietKhau = new ArrayList<>();
    }

    public void setmPresenter(OrderCartMvpPresenter<OrderCartMvpView> mPresenter) {
        this.mPresenter = mPresenter;
    }

    public void setBaseActivity(BaseActivity baseActivity) {
        this.baseActivity = baseActivity;
    }

    public void setCallback(Callback callback) {
        mCallback = callback;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (baseActivity != null) {
            baseActivity.getmActivityComponent().inject(this);
        }

        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.order_cart_item_layout, parent, false), baseActivity);
            case VIEW_TYPE_FIRST_ITEM:
                return new FirstItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.order_cart_first_item_layout, parent, false));
            case VIEW_TYPE_SECOND_ITEM:
                return new SecondItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.pay_type_hide_layout, parent, false));
            case VIEW_TYPE_EMPTY:
            default:
                return new EmptyViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_view, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mOrderProductList != null && mOrderProductList.size() > 0 && position != 0 && position != 1) {
            return VIEW_TYPE_NORMAL;
        } else if (mOrderProductList != null && mOrderProductList.size() > 0 && position == 0) {
            return VIEW_TYPE_FIRST_ITEM;
        } else if (mOrderProductList != null && mOrderProductList.size() > 0) {
            return VIEW_TYPE_SECOND_ITEM;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public int getItemCount() {
        if (mOrderProductList != null && mOrderProductList.size() > 0) {
            return mOrderProductList.size() + EXTEND_VALUE;
        } else {
            return 1;
        }
    }

    void changeTypeProduct(TypeProduct current) {
        if (current == null) return;
        if (mOrderProductList != null && mOrderProductList.size() > 0 && mListProduct != null && mListProduct.size() > 0) {
            for (OrderProduct orderProduct : mOrderProductList) {
                orderProduct.setTypeProduct(current);
                notifyDataSetChanged();
            }
        }
    }

    void checkAll(boolean isCheck, TypeProduct current) {
        if (current == null) return;
        if (mOrderProductList != null && mOrderProductList.size() > 0 && mListProduct != null && mListProduct.size() > 0) {
            for (OrderProduct orderProduct : mOrderProductList) {
                orderProduct.setCheck(isCheck);
                orderProduct.setTypeProduct(current);
                notifyDataSetChanged();
            }
        }
    }

    boolean checkAllSelected() {
        if (mOrderProductList != null && mOrderProductList.size() > 0 && mListProduct != null && mListProduct.size() > 0) {
            for (OrderProduct orderProduct : mOrderProductList) {
                if (!orderProduct.isCheck()) {
                    return false;
                }
            }
        }
        return true;
    }


    public void addItems(List<OrderProduct> orderProductList) {
        orderLogics.clear();
        for (OrderProduct orderProduct : orderProductList) {
            orderLogics.add(orderProduct.getOrderLogic());
        }

        updateFeeAndCVAll();
        mOrderProductList.clear();
        mOrderProductList.addAll(orderProductList);
        notifyDataSetChanged();
    }

    void addListTypeProduct(List<TypeProduct> list, List<ChietKhauModel> listChietKhau) {
        mListProduct = list;
        mListChietKhau = listChietKhau;
        mListProduct.get(1).setDoanhSo(false);
        mListProduct.get(1).setCV(true);
        for (OrderProduct orderProduct : mOrderProductList) {
            orderProduct.setTypeProduct(list.get(0));
        }
        notifyDataSetChanged();
    }


    private void updateFeeAndCVAll() {
        float sumFee = 0;
        float sumCV = 0;
        int tongChietKhau = 0;
        for (OrderLogic logic : orderLogics) {
            if (logic.isApplyCV()) {
                sumCV += logic.getCvWithQuantity();
            }
            if (logic.isApplyDoanhSo()) {
                sumFee += logic.getTotalFee();
            }
        }

        for (OrderProduct orderProduct : mOrderProductList) {
            int quantity = (int) orderProduct.getOrderLogic().getQuantity();
            if (orderProduct.isCheckChietKhau()) {
                tongChietKhau += orderProduct.getTongChietKhau(quantity);
            }
        }
        mPresenter.onUpdateSumaryFeeAndCV(sumFee, sumCV, tongChietKhau);
    }

    public interface Callback {
        void onDeleteItem(OrderProduct product, CallbackDelete callbackDelete);

        void onCheckBoxClick(boolean isCheck);

        void showToast();
    }

    public interface CallbackDelete {
        void confirmDelete();
    }

    public class SecondItemViewHolder extends BaseViewHolder {

        private SecondItemViewHolder(View itemView) {
            super(itemView);
        }

        private void initView(View itemView) {
        }

        @Override
        public void onBind(int position) {
            super.onBind(position);
        }

        @Override
        protected void clear() {
        }
    }


    public class FirstItemViewHolder extends BaseViewHolder {
        TextView txtCustomerName;
        Button btnDetailCustomer;
        CheckBox printPrice;

        public FirstItemViewHolder(View itemView) {
            super(itemView);
            initView(itemView);
        }

        private void initView(View itemView) {
            printPrice = itemView.findViewById(R.id.printPrice);
            printPrice.setChecked(isPrintPrice);
            txtCustomerName = itemView.findViewById(R.id.order_cart_first_item_txt_customer_name);
            printPrice.setOnCheckedChangeListener((buttonView, isChecked) -> {
                isPrintPrice = isChecked;
                notifyDataSetChanged();
            });
            txtCustomerName.setOnClickListener((View v) -> {
                mPresenter.openSearchActivity();
            });

            //setupCustomerName();
            btnDetailCustomer = itemView.findViewById(R.id.btn_order_cart_change_customer);
            btnDetailCustomer.setOnClickListener((View v) -> {
                if (customer == null || customer.getTenDayDu().isEmpty()) {
                    mPresenter.showMessage(R.string.order_cart_no_customer);
                    return;
                }
                Intent i = new Intent(baseActivity, DetailCustomerActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("customerguid", customer.getGuid());
                bundle.putString("customername", customer.getTenDayDu());
                i.putExtra("detailcustomeractivity", bundle);
                baseActivity.startActivity(i);
            });
        }

        private void setupCustomerName() {
            if (customer == null || customer.getTenDayDu().isEmpty()) {
                txtCustomerName.setText(R.string.order_cart_no_customer);
            } else {
                txtCustomerName.setText(customer.getTenDayDu());
            }
        }

        @Override
        public void onBind(int position) {
            super.onBind(position);
            setupCustomerName();
            itemView.setOnClickListener((View v) -> {
                mPresenter.openSearchActivity();
            });
        }

        @Override
        protected void clear() {
        }
    }

    public class ViewHolder extends BaseViewHolder {

        TextView txtProductName, txtProductCV, txtProductPrice;

        RoundRectCornerImageView imgProduct;
        ImageView imgSub, imgAdd;

        TextView txtSumFee, txtSumCV, tongChietKhau, txtUnit;

        Button btnBuyBox;

        EditText edtQuantity, edtProductNote;

        ImageView imgDelete, btnBuyBoxIncrease, btnBuyBoxDecrease;

        CheckBox checkFreeProduct;

        Spinner spinnerProduct;

        CheckBox checkBoxChietKhau;
        LinearLayout containerChietKhau;
        ImageView iconCopy;

        BaseActivity baseActivity;

        private int numberBox = 0;

        public ViewHolder(View itemView, BaseActivity baseActivity) {
            super(itemView);
            this.baseActivity = baseActivity;
            txtProductName = itemView.findViewById(R.id.order_cart_txt_name_product);
            txtProductCV = itemView.findViewById(R.id.order_cart_txt_product_cv);
            imgProduct = itemView.findViewById(R.id.order_cart_img_product);
            txtProductPrice = itemView.findViewById(R.id.order_cart_txt_price_product);
            imgSub = itemView.findViewById(R.id.order_cart_img_sub_quantity_product);
            imgAdd = itemView.findViewById(R.id.order_cart_img_increase_quantity_product);
            edtQuantity = itemView.findViewById(R.id.order_cart_edt_quantity_product);
            txtSumFee = itemView.findViewById(R.id.order_cart_txt_sum_fee);
            txtSumCV = itemView.findViewById(R.id.order_cart_txt_sum_cv);
            tongChietKhau = itemView.findViewById(R.id.tongChietKhau);
            txtUnit = itemView.findViewById(R.id.order_cart_txt_unit_detail_product);
            btnBuyBox = itemView.findViewById(R.id.btn_buy_box);
            imgDelete = itemView.findViewById(R.id.order_cart_img_delete);
            edtProductNote = itemView.findViewById(R.id.order_cart_edt_product_note);
            btnBuyBoxIncrease = itemView.findViewById(R.id.img_buy_box_increase);
            btnBuyBoxDecrease = itemView.findViewById(R.id.img_buy_box_decrease);
            checkFreeProduct = itemView.findViewById(R.id.checkFreeProduct);
            spinnerProduct = itemView.findViewById(R.id.spinnerProduct);
            checkBoxChietKhau = itemView.findViewById(R.id.checkBoxChietKhau);
            containerChietKhau = itemView.findViewById(R.id.containerChietKhau);
            iconCopy = itemView.findViewById(R.id.copyItem);
            setupView();
        }

        void setupView() {
            checkFreeProduct.setOnCheckedChangeListener((compoundButton, b) -> {
                OrderProduct orderProduct = mOrderProductList.get(getCurrentPosition() - EXTEND_VALUE);
                spinnerProduct.setEnabled(b);
                orderProduct.setCheck(b);
                if (mCallback != null) {
                    mCallback.onCheckBoxClick(b);
                }
                onUpdatePrice(false);
            });

            spinnerProduct.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    OrderProduct orderProduct = mOrderProductList.get(getCurrentPosition() - EXTEND_VALUE);
                    if (checkFreeProduct.isChecked()) {
                        TypeProduct typeProduct = (TypeProduct) adapterView.getSelectedItem();
                        orderProduct.setTypeProduct(typeProduct);
                        System.out.println("typeProduct " + typeProduct.getName());
                        if ("Khuyến mại".equals(typeProduct.getName())) {
                            iconCopy.setVisibility(View.GONE);
                        } else {
                            iconCopy.setVisibility(View.VISIBLE);
                        }
                    }
                    onUpdatePrice(false);
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            btnBuyBoxIncrease.setOnClickListener((View v) -> {
                resetCV();
                OrderLogic orderLogic = mOrderProductList.get(getCurrentPosition() - EXTEND_VALUE).getOrderLogic();
                orderLogic.increaseQuantity(orderLogic.getQuantityThreshold());
                numberBox++;
                orderLogic.setNumberBox(numberBox);

                if (numberBox > 0) {
                    String text = numberBox + " " + baseActivity.getString(R.string.order_cart_number_box);
                    edtProductNote.setText(text);
                } else {
                    edtProductNote.setText("");
                }
                setQuantity(orderLogic.getQuantity());
                onUpdatePrice(false);

            });
            btnBuyBoxDecrease.setOnClickListener((View v) -> {
                resetCV();
                OrderLogic orderLogic = mOrderProductList.get(getCurrentPosition() - EXTEND_VALUE).getOrderLogic();
                orderLogic.decreaseQuantity(orderLogic.getQuantityThreshold());
                numberBox--;
                if (numberBox < 0) {
                    numberBox = 0;
                }
                // note
                orderLogic.setNumberBox(numberBox);

                if (numberBox > 0) {
                    edtProductNote.setText(numberBox + " " + baseActivity.getString(R.string.order_cart_number_box));
                } else {
                    edtProductNote.setText("");
                }
                setQuantity(orderLogic.getQuantity());
                onUpdatePrice(false);

            });

            edtQuantity.setOnFocusChangeListener((view, b) -> {
                if (!b) {
                    String number = edtQuantity.getText().toString();
                    if (number.isEmpty()) {
                        number = "0";
                        edtQuantity.setText(number);
                    }
                    onChangeQuantity(Float.parseFloat(number));
                }
            });

            edtProductNote.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    mOrderProductList.get(getCurrentPosition() - EXTEND_VALUE).setProductNote(editable.toString());
                }
            });

            imgSub.setOnClickListener((View v) -> {
                onSubQuantityButtonClick();
            });

            imgAdd.setOnClickListener((View v) -> {
                onIncreaseQuantityButtonClick();
            });

            btnBuyBox.setOnClickListener((View v) -> {
                btnBuyBoxIncrease.performClick();
            });

            imgDelete.setOnClickListener((View v) -> {
                if (mCallback != null) {
                    int position = getCurrentPosition() - EXTEND_VALUE;
                    System.out.println("position " + position);
                    mCallback.onDeleteItem(mOrderProductList.get(position), () -> {
                        mOrderProductList.remove(position);
                        if (!mOrderProductList.isEmpty()) {
                            notifyItemRemoved(position);
                            new Handler().postDelayed(OrderCartAdapter.this::notifyDataSetChanged, 500);
                        } else {
                            mPresenter.onGetAllOrderProduct();
                        }
                    });
                }

            });

            txtProductPrice.setOnClickListener((View v) -> {
                resetCV();
                OrderProduct orderProduct = mOrderProductList.get(getCurrentPosition() - EXTEND_VALUE);
                OrderLogic orderLogic = orderProduct.getOrderLogic();
                orderLogic.setQuantity(orderLogic.getQuantity());
                setQuantity(orderLogic.getQuantity());
                onUpdatePrice(false);
                mPresenter.onOpenSaleDialog(orderProduct, ViewHolder.this);
            });


            iconCopy.setOnClickListener(v -> {
                int position = getCurrentPosition() - EXTEND_VALUE;
                OrderProduct orderProduct = mOrderProductList.get(position);
                Gson gson = new Gson();
                String json = gson.toJson(orderProduct);
                OrderProduct cloneObject = gson.fromJson(json, OrderProduct.class);
                int nextPosition = position + 1;
                mOrderProductList.add(nextPosition, cloneObject);
                notifyItemInserted(nextPosition + EXTEND_VALUE);
                new Handler().postDelayed(OrderCartAdapter.this::notifyDataSetChanged, 500);
            });
        }

        void onSubQuantityButtonClick() {
            resetCV();
            OrderLogic orderLogic = mOrderProductList.get(getCurrentPosition() - EXTEND_VALUE).getOrderLogic();
            float currentValue = orderLogic.getQuantity() - 1;
            if (currentValue < 0) {
                currentValue = 0;
            }
            orderLogic.setQuantity(currentValue);
            setQuantity(currentValue);
            onUpdatePrice(false);
        }

        void onIncreaseQuantityButtonClick() {
            resetCV();
            OrderLogic orderLogic = mOrderProductList.get(getCurrentPosition() - EXTEND_VALUE).getOrderLogic();
            orderLogic.setQuantity(orderLogic.getQuantity() + 1);
            setQuantity(orderLogic.getQuantity());
            onUpdatePrice(false);

        }

        private void resetCV() {
            OrderProduct orderProduct = mOrderProductList.get(getCurrentPosition() - EXTEND_VALUE);
            OrderLogic orderLogic = orderProduct.getOrderLogic();
            orderLogic.setCv(orderLogic.getFixCV());
            orderProduct.setCheckChietKhau(false);
            orderProduct.resetChietKhau();
        }

        void onChangeQuantity(float quantity) {
            OrderLogic orderLogic = mOrderProductList.get(getCurrentPosition() - EXTEND_VALUE).getOrderLogic();
            orderLogic.setQuantity(quantity);
            setQuantity(quantity);
            onUpdatePrice(false);
        }

        public void setQuantity(float quantity) {
            try {
                OrderProduct orderProduct = mOrderProductList.get(getCurrentPosition() - EXTEND_VALUE);
                if (quantity != orderProduct.getQuantity()) {
                    orderProduct.setQuantity(quantity);
                    mPresenter.onUpdateOrderCart(orderProduct, false);
                }
                edtQuantity.setText(String.valueOf(quantity));
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        public void onUpdatePrice(boolean isOnBind) {
            onUpdatePrice();
            if (!isOnBind) {
                updateFeeAndCVAll();
            }
            logicChietKhau();
        }

        public void onUpdatePrice() {
            OrderProduct orderProduct = mOrderProductList.get(getCurrentPosition() - EXTEND_VALUE);
            OrderLogic orderLogic = orderProduct.getOrderLogic();
            int minCV = orderLogic.getCv();
            if (orderProduct.getProductCode() != null) {
                String productCV = baseActivity.getString(R.string.order_home_cv) + " " + Utils.formatValue(minCV, Enum.FieldValueType.NORMAL);
                txtProductCV.setText(productCV);
            }

            int tongChietKhau = 0;
            if (orderProduct.isCheckChietKhau()) {
                tongChietKhau = orderProduct.getTongChietKhau(orderLogic.getQuantity());
            }
            txtProductPrice.setText(Utils.formatValue(orderLogic.getCurrentPrice(), Enum.FieldValueType.CURRENCY));
            float fee = orderLogic.getTotalFee();
            String sumFee = baseActivity.getString(R.string.order_sum_fee) + ": " + Utils.formatValue(fee, Enum.FieldValueType.NORMAL);
            if (tongChietKhau > 0) {
                fee = fee - tongChietKhau * 1000;
                sumFee += " | " + Utils.formatValue(fee, Enum.FieldValueType.NORMAL);
            }
            txtSumFee.setText(sumFee);

            int numberCV = orderLogic.getCvWithQuantity();
            String sumCV = baseActivity.getString(R.string.order_sum_cv) + ": " + numberCV;
            if (tongChietKhau > 0) {
                numberCV = numberCV - tongChietKhau;
                sumCV += " | " + numberCV;
            }
            txtSumCV.setText(sumCV);

            if (checkFreeProduct.isChecked()) {
                if (!orderLogic.isApplyDoanhSo()) {
                    String fee0 = baseActivity.getString(R.string.order_sum_fee) + ": 0";
                    txtSumFee.setText(fee0);
                }
                if (!orderLogic.isApplyCV()) {
                    String cv0 = baseActivity.getString(R.string.order_sum_cv) + ": 0";
                    txtSumCV.setText(cv0);
                }
            }
        }


        protected void clear() {
            txtProductName.setText("");
            txtProductCV.setText("");
        }


        public void onBind(int position) {
            super.onBind(position);
            if (position <= 0) {
                return;
            }

            final OrderProduct orderProduct = mOrderProductList.get(position - EXTEND_VALUE);

            TypeProduct typeProduct = orderProduct.getTypeProduct();
            if (typeProduct != null) {
                if ("Khuyến mại".equals(typeProduct.getName())) {
                    iconCopy.setVisibility(View.GONE);
                } else {
                    iconCopy.setVisibility(View.VISIBLE);
                }
            } else {
                iconCopy.setVisibility(View.VISIBLE);
            }

            if (orderProduct.getQuantity() != orderProduct.getOrderLogic().getDefaultQuantity()) {
                setQuantity(orderProduct.getQuantity());
            } else {
                setQuantity(orderProduct.getOrderLogic().getDefaultQuantity());
            }

            numberBox = orderProduct.getOrderLogic().getNumberBox();

            setupProduct(orderProduct, orderProduct.getOrderLogic(), ViewHolder.this, itemView.getContext());
        }

        private void setupProduct(OrderProduct orderProduct, OrderLogic orderLogic, ViewHolder viewHolder, Context context) {
            viewHolder.txtSumFee.setVisibility(isPrintPrice ? View.VISIBLE : View.INVISIBLE);

            viewHolder.onUpdatePrice(true);

            if (orderProduct.getProductName() != null) {
                viewHolder.txtProductName.setText(orderProduct.getProductName());
            }

            int minCV = orderLogic.getCv();
            if (orderProduct.getProductCode() != null) {
                String productCV = baseActivity.getString(R.string.order_home_cv) + " " + Utils.formatValue(minCV, Enum.FieldValueType.NORMAL);
                viewHolder.txtProductCV.setText(productCV);
            }

            if (orderProduct.getMainPrice().getDonVi() != null) {
                viewHolder.txtUnit.setText(orderProduct.getMainPrice().getDonVi());
            }
            if (orderProduct.getMainPrice().getKDBanBuonDieuKienMoTa() != null) {
                viewHolder.btnBuyBox.setText(orderProduct.getMainPrice().getKDBanBuonDieuKienMoTa());
            }

            if (orderLogic.getNumberBox() > 0) {
                viewHolder.edtProductNote.setText(orderLogic.getNumberBox() + " " + baseActivity.getString(R.string.order_cart_number_box));
            } else {
                viewHolder.edtProductNote.setText("");
            }
            if (mListProduct != null && mListProduct.size() > 0) {
                viewHolder.checkFreeProduct.setVisibility(View.VISIBLE);
                viewHolder.spinnerProduct.setVisibility(View.VISIBLE);
                ArrayAdapter<TypeProduct> adapter = new ArrayAdapter<>(context, R.layout.item_check_box, mListProduct);
                viewHolder.spinnerProduct.setAdapter(adapter);
                boolean check = orderProduct.isCheck();
                viewHolder.spinnerProduct.setEnabled(check);
                viewHolder.checkFreeProduct.setChecked(check);
                if (orderProduct.getTypeProduct() != null) {
                    int typeProduct = getPosition(orderProduct.getTypeProduct().getId());
                    viewHolder.spinnerProduct.setSelection(typeProduct);
                }
            } else {
                viewHolder.checkFreeProduct.setVisibility(View.GONE);
                viewHolder.spinnerProduct.setVisibility(View.GONE);
            }
            NetworkManager.getInstance().loadImage(orderProduct.getThumbId(), viewHolder.imgProduct);


        }

        public void logicChietKhau() {
            OrderProduct orderProduct = mOrderProductList.get(getCurrentPosition() - EXTEND_VALUE);
            Context context = itemView.getContext();
            OrderLogic orderLogic = orderProduct.getOrderLogic();

            LinearLayout containerChietKhau = this.containerChietKhau;
            int maxValueCK = orderLogic.getMaxCV(percenChietKhau);
            fillTotalChietKhau(orderProduct);

            if (maxValueCK > 0) {
                boolean isCheckChietKhau = orderProduct.isCheckChietKhau();
                checkBoxChietKhau.setOnCheckedChangeListener(null);
                checkBoxChietKhau.setChecked(isCheckChietKhau);
                checkBoxChietKhau.setOnCheckedChangeListener((compoundButton, b) -> {
                    orderProduct.setCheckChietKhau(b);
                    if (b) {
                        containerChietKhau.setVisibility(View.VISIBLE);
                    } else {
                        containerChietKhau.setVisibility(View.GONE);
                    }
                    onUpdatePrice(false);
                });
                containerChietKhau.setVisibility(isCheckChietKhau ? View.VISIBLE : View.GONE);
                containerChietKhau.removeAllViews();
                LayoutInflater inflater = LayoutInflater.from(context);
                for (ChietKhauModel chietKhauModel : mListChietKhau) {
                    int idChietChau = chietKhauModel.getDM_ChietKhauID();
                    String valueChietKhauLocal = orderProduct.getValueChietKhau(idChietChau);
                    int valueChietKhau = chietKhauModel.getCvChietKhau();

                    View linearLayout = inflater.inflate(R.layout.item_chiet_khau, containerChietKhau, false);

                    EditText editText = linearLayout.findViewById(R.id.inputChietKhau);
                    editText.setEnabled(chietKhauModel.isThayDoi());
                    editText.setImeOptions(EditorInfo.IME_ACTION_DONE);
                    if (chietKhauModel.isThayDoi()) {
                        editText.setText(valueChietKhauLocal);
                        if (valueChietKhauLocal != null) {
                            editText.setFocusableInTouchMode(true);
                        } else {
                            editText.setFocusable(false);
                        }
                    }
                    editText.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {

                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                            try {
                                int value = Integer.parseInt(s.toString());
                                boolean isSuccess = orderProduct.addChietKhauId(idChietChau, value, maxValueCK);
                                if (!isSuccess) {
                                    mCallback.showToast();
                                    onUpdatePrice(false);
                                } else {
                                    onUpdatePrice();
                                }

                                fillTotalChietKhau(orderProduct);
                            } catch (Exception ignore) {

                            }
                        }
                    });
                    CheckBox checkBox = linearLayout.findViewById(R.id.checkBox);
                    String tenChietKhau = chietKhauModel.getTenChietKhau();
                    String fullText = tenChietKhau + " (" + valueChietKhau + " CV)";
                    SpannableString spannableString = new SpannableString(fullText);
                    spannableString.setSpan(new ForegroundColorSpan(Color.GRAY), tenChietKhau.length(), fullText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    checkBox.setText(spannableString);
                    checkBox.setChecked(valueChietKhauLocal != null);
                    checkBox.setOnCheckedChangeListener((compoundButton, b) -> {
                        if (chietKhauModel.isThayDoi()) {
                            if (b) {
                                editText.setFocusableInTouchMode(true);
                            } else {
                                editText.setFocusable(false);
                            }
                        }
                        if (b) {
                            boolean isSuccess = orderProduct.addChietKhauId(idChietChau, valueChietKhau, maxValueCK);
                            if (!isSuccess) {
                                mCallback.showToast();
                            }
                            if (chietKhauModel.isThayDoi()) {
                                editText.setEnabled(true);
                            }
                        } else {
                            orderProduct.removeChietKhauId(idChietChau);
                        }
                        onUpdatePrice(false);
                    });
                    containerChietKhau.addView(linearLayout);
                }
            }
        }

        private void fillTotalChietKhau(OrderProduct orderProduct) {
            OrderLogic orderLogic = orderProduct.getOrderLogic();
            int maxValueCK = orderLogic.getMaxCV(percenChietKhau);
            int countCVSelected = orderProduct.totalCV();

            String text = "Tổng CV chiết khấu: " + orderProduct.getTongChietKhau(orderLogic.getQuantity()) + " CV";
            tongChietKhau.setText(text);

            String current = String.valueOf(maxValueCK);
//            if (countCVSelected > 0) {
//                current = countCVSelected + "/" + current;
//            }
            String textCK = "Chiết khấu";
            String unit = orderProduct.getMainPrice().getDonVi();
            String textCKFull = textCK + " ( Tối đa " + current + " CV/1" + unit + " )";
            SpannableString spanParent = new SpannableString(textCKFull);
            spanParent.setSpan(new ForegroundColorSpan(Color.GRAY), textCK.length(), textCKFull.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            checkBoxChietKhau.setText(spanParent);
            checkBoxChietKhau.setEnabled(maxValueCK > 0);
        }

        private int getPosition(int id) {
            for (TypeProduct typeProduct : mListProduct) {
                if (typeProduct.getId() == id) {
                    return mListProduct.indexOf(typeProduct);
                }
            }
            return 0;
        }
    }


    public boolean checkCustomerAndPayMethodCondition() {
        if (customer == null || customer.getGuid().isEmpty() || customer.getTenDayDu().isEmpty()) {
            mPresenter.showMessage(R.string.order_cart_validate_customer);
            return false;
        }

        return true;
    }

    public void onShowConfigDialogOrder() {
        if (checkCustomerAndPayMethodCondition()) {
            mPresenter.onShowOrderConfigDialog(customer, this);
        }
    }


    public void startOrderFromDialog(int payType, String note, String address) {
        mPresenter.onStartOrder(createBodyObject(payType, note, address));
    }

    private JSONObject createBodyObject(int payType, String note, String address) {

        JSONObject mainObject = new JSONObject();
        JSONArray products = new JSONArray();
        JSONObject addressDelivery = new JSONObject();
        try {
            addressDelivery.put("DiaChi", address);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            for (OrderProduct product : mOrderProductList) {
                JSONObject productObject = new JSONObject();
                productObject.put("dD_HangHoaID", product.getProductId());
                productObject.put("donViID", product.getMainPrice().getDMDonViID());
                String quantity = String.valueOf(product.getOrderLogic().getQuantity());
                productObject.put("soLuong", quantity);
                productObject.put("giaTien", String.valueOf(product.getOrderLogic().getCurrentPrice()));
                productObject.put("cV", product.getOrderLogic().getCv());
                productObject.put("giaHangHoaID", product.getMainPrice().getId());
                productObject.put("duLieu", product.getProductNote());
                productObject.put("DM_HangHoaDungThuID", product.isCheck() ? product.getTypeProduct().getId() : 0);

                Map<Integer, Integer> map = product.getListChietKhauIds();
                if (product.isCheckChietKhau() && map.size() > 0) {
                    JSONArray jsonArray = new JSONArray();
                    for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
                        int chietKhauId = entry.getKey();
                        int chieuKhauValue = entry.getValue();
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("CV_CK", chieuKhauValue);
                        jsonObject.put("SoLuongCK", quantity);
                        jsonObject.put("DM_ChietKhauID", chietKhauId);
                        jsonObject.put("dD_HangHoaID", product.getProductId());
                        jsonArray.put(jsonObject);
                    }
                    productObject.put("cks", jsonArray);
                }


                products.put(productObject);
            }

            mainObject.put("dD_ThanhToanID", payType); // method pay
            mainObject.put("loaiDonDatID", 1); // wholeSale or retail
            mainObject.put("ghiChu", note);
            mainObject.put("guidThanhVien", customer.getGuid());
            mainObject.put("hanghoas", products);
            mainObject.put("diachigiaohang", addressDelivery);
            mainObject.put("BoPhanID", App.getInstance().getDepartmentId());
            mainObject.put("isPrintPrice", isPrintPrice ? 1 : 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mainObject;
    }

    public class EmptyViewHolder extends BaseViewHolder {
        Button retryButton;
        TextView messageTextView;
        TextView txtRetry;

        public EmptyViewHolder(View itemView) {
            super(itemView);
            retryButton = itemView.findViewById(R.id.btn_retry);
            messageTextView = itemView.findViewById(R.id.tv_message);
            txtRetry = itemView.findViewById(R.id.txt_retry);
            txtRetry.setVisibility(View.GONE);
            retryButton.setVisibility(View.GONE);
        }

        @Override
        protected void clear() {
        }

        @OnClick(R.id.btn_retry)
        void onRetryClick() {
//            if (mCallback != null)
//                mCallback.onBlogEmptyViewRetryClick();
        }
    }

}

