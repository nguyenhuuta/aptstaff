package com.anphuocthai.staff.ui.delivery.shipped.finishtransport;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentManager;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.di.component.ActivityComponent;
import com.anphuocthai.staff.ui.base.BaseDialog;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;
import com.anphuocthai.staff.ui.delivery.shipped.ShippedActivity;
import com.anphuocthai.staff.utils.Enum;
import com.anphuocthai.staff.utils.Logger;
import com.anphuocthai.staff.utils.Utils;
import com.anphuocthai.staff.widget.EditTextNumber;

import javax.inject.Inject;

public class FinishTransportDialog extends BaseDialog implements FinishTransportMvpView, RadioGroup.OnCheckedChangeListener {
    final int GIAO_DU_HANG = 6;
    final int CO_TRA_HANG = 61;
    private static final String TAG = FinishTransportDialog.class.getSimpleName();

    private TextView txtDialogTitle;
    private RadioGroup radioGroup;
    private RadioButton debtRadioButton, cashRadioButton;
    private TextView edtCash;
    private CardView cardView;
    private ShippedActivity shippedActivity;

    private RadioGroup typeCompleteRadioGroup;

    private TextView txtNumberMoney;
    private EditTextNumber edtNumberMoney;

    private TextView btnComplete;
    private TextView textError;
    private EditText edtReasonReturnProduct;

    private TextView txtUnitCurrency;


    private int currentPrice = 0;
    /**
     * 6 : Giao đủ hàng
     * 61 : Có trả hàng
     */
    private int DD_TrangThaiID = GIAO_DU_HANG;

    @Inject
    FinishTransportMvpPresenter<FinishTransportMvpView> mPresenter;

    private OrderTran orderTran;

    @Override
    protected void setUp(View view) {

    }

    public static FinishTransportDialog newInstance() {
        FinishTransportDialog fragment = new FinishTransportDialog();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.finish_transport_dialog_layout, container, false);
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            txtDialogTitle = view.findViewById(R.id.shipped_dialog_title);
            radioGroup = view.findViewById(R.id.shipped_dialog_radio_group);
            debtRadioButton = view.findViewById(R.id.shipped_dialog_debt_radio_button);
            cashRadioButton = view.findViewById(R.id.shipped_dialog_cash_radio_button);
            edtCash = view.findViewById(R.id.shipped_dialog_edt_cash);
            cardView = view.findViewById(R.id.finish_transport_card_view);
            btnComplete = view.findViewById(R.id.shipped_dialog_btn_complete);
            typeCompleteRadioGroup = view.findViewById(R.id.type_complete_ship_radiogroup);

            txtNumberMoney = view.findViewById(R.id.lbl_money_number);
            edtNumberMoney = view.findViewById(R.id.edt_money_number);
            txtUnitCurrency = view.findViewById(R.id.txt_unit_currency);
            textError = view.findViewById(R.id.textError);
            edtReasonReturnProduct = view.findViewById(R.id.edt_reason_return_product);

            initEvents();
            mPresenter.onAttach(this);

        }

        return view;
    }

    @Override
    public void hideKeyboard() {
        super.hideKeyboard();
    }

    private void initEvents() {
        if (orderTran == null || orderTran.getTongGiaTri() == null) {
            showMessage(R.string.have_error);
            dismissDialog();
            return;
        }

        currentPrice = orderTran.getTongGiaTri();
        edtNumberMoney.setText("" + currentPrice);
        cardView.setOnClickListener((View v) -> {
            InputMethodManager imm = (InputMethodManager) getBaseActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            edtCash.clearFocus();
            edtNumberMoney.clearFocus();
        });

        edtReasonReturnProduct.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().isEmpty() && textError.getVisibility() == View.VISIBLE) {
                    textError.setVisibility(View.GONE);
                }
            }
        });


        radioGroup.setOnCheckedChangeListener(this);
        typeCompleteRadioGroup.setOnCheckedChangeListener(this);
        if (orderTran.getDDThanhToanID() == Enum.PayMethod.DEBT) {
            debtRadioButton.setChecked(true); //Công nợ
        } else {
            cashRadioButton.setChecked(true); // Trả tiền mặt
        }

        String donHang = getString(R.string.order_tran_order_code) + " " + orderTran.getMaDonDat();
        txtDialogTitle.setText(donHang);
        edtCash.setText(Utils.formatValue(orderTran.getTongGiaTri(), Enum.FieldValueType.CURRENCY_NORMAL));

        btnComplete.setOnClickListener((View v) -> {
            String note = edtReasonReturnProduct.getText().toString().trim();
            if (DD_TrangThaiID == CO_TRA_HANG && note.isEmpty()) {
                textError.setVisibility(View.VISIBLE);
                return;
            }
            float money = 0;
            if (edtNumberMoney.getVisibility() == View.VISIBLE) {
                money = edtNumberMoney.getNumber();
            }
            Logger.d("btnComplete", "money " + money);
            mPresenter.banGiaoChungTu(orderTran.getDDDonDatID(), money, note, DD_TrangThaiID);
        });
    }

    private void visibleNumberMoney(boolean isVisible) {
        if (isVisible) {
            txtNumberMoney.setVisibility(View.VISIBLE);
            edtNumberMoney.setVisibility(View.VISIBLE);
            txtUnitCurrency.setVisibility(View.VISIBLE);
        } else {
            txtNumberMoney.setVisibility(View.GONE);
            edtNumberMoney.setVisibility(View.GONE);
            txtUnitCurrency.setVisibility(View.GONE);
        }
    }

    private void visibleReasonReturnProduct(boolean isVisible) {
        if (isVisible) {
            edtReasonReturnProduct.setVisibility(View.VISIBLE);
        } else {
            edtReasonReturnProduct.setVisibility(View.GONE);
        }

    }

    public void show(FragmentManager fragmentManager, OrderTran orderTran, ShippedActivity shippedActivity) {
        super.show(fragmentManager, TAG);
        this.orderTran = orderTran;
        this.shippedActivity = shippedActivity;
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void dismissDialog() {
        super.dismissDialog(TAG);
        shippedActivity.getAllShipped();
    }


    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int radioButtonId) {
        switch (radioButtonId) {
            case R.id.shipped_dialog_debt_radio_button: //Công nợ
                edtCash.setText(Utils.formatValue(orderTran.getTongGiaTri(), Enum.FieldValueType.CURRENCY_NORMAL));
                visibleNumberMoney(false);
                currentPrice = 0;
                break;
            case R.id.shipped_dialog_cash_radio_button: // Tiền mặt
                edtNumberMoney.clearFocus();
                edtNumberMoney.setText("0");
                visibleNumberMoney(true);
                break;

            case R.id.delivery_radio_button: // Giao đủ hàng
                DD_TrangThaiID = 6;
                if (cashRadioButton.isChecked()) {
                    visibleNumberMoney(true);
                    currentPrice = orderTran.getTongGiaTri();
                } else {
                    visibleNumberMoney(false);
                }
                edtNumberMoney.setText(Utils.formatValue(orderTran.getTongGiaTri(), Enum.FieldValueType.CURRENCY_NORMAL));
                visibleReasonReturnProduct(true);
                edtReasonReturnProduct.setHint(R.string.common_note);
                break;
            case R.id.return_radio_button: // có trả hàng
                DD_TrangThaiID = 61;
                visibleNumberMoney(cashRadioButton.isChecked());
                edtNumberMoney.setText("0");
                visibleReasonReturnProduct(true);
                edtReasonReturnProduct.setHint(R.string.reason_return_product);
                break;
        }
    }
}
