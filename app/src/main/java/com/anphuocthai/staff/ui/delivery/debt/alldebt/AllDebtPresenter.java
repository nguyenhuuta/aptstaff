package com.anphuocthai.staff.ui.delivery.debt.alldebt;

import com.androidnetworking.error.ANError;
import com.anphuocthai.staff.api.ApiURL;
import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.data.DataManager;
import com.anphuocthai.staff.interfaces.IAPICallback;
import com.anphuocthai.staff.ui.base.BasePresenter;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;
import com.anphuocthai.staff.utils.App;
import com.anphuocthai.staff.utils.Logger;
import com.anphuocthai.staff.utils.rx.SchedulerProvider;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.rx2androidnetworking.Rx2AndroidNetworking;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.disposables.CompositeDisposable;

public class AllDebtPresenter<V extends AllDebtMvpView> extends BasePresenter<V> implements AllDebtMvpPresenter<V> {

    @Inject
    public AllDebtPresenter(NetworkManager networkManager, DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(networkManager, dataManager, schedulerProvider, compositeDisposable);
    }

    private JSONObject createJSONObject(final int page) {
        JSONObject notTranObject = new JSONObject();
        try {
            notTranObject.put("phanLoai", 3);
            notTranObject.put("guidThanhVien", "");
            notTranObject.put("sessionID", "");
            notTranObject.put("tuNgay", "");
            notTranObject.put("denNgay", "");
            notTranObject.put("trangThaiIds", "");
            notTranObject.put("pageIndex", page);
            notTranObject.put("pageSize", 10);
            notTranObject.put("BoPhanID", App.getInstance().getDepartmentId());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return notTranObject;
    }

    @Override
    public Flowable<List<OrderTran>> getAllDebtApiCall(JSONObject object) {
        return Rx2AndroidNetworking.post(ApiURL.GET_ORDER_URL)
                //.addHeaders(mApiHeader.getProtectedApiHeader())
                .addHeaders(ApiURL.getBaseHeader())
                .addHeaders(ApiURL.getTokenHeader())
                .addJSONObjectBody(object)
                .build()
                .getObjectListFlowable(OrderTran.class);

    }

    @Override
    public void onViewPrepared() {
        getmNetworkManager().sendPostRequestWithArrayResponse(ApiURL.GET_ORDER_URL, createJSONObject(1), new IAPICallback() {
            @Override
            public void onResponseSuccess(JSONObject response) {
            }

            @Override
            public void onResponseSuccess(JSONArray response) {
                if (response != null) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<ArrayList<OrderTran>>() {
                    }.getType();
                    ArrayList<OrderTran> orderTrans = gson.fromJson(response.toString(), type);
                    Logger.d("onViewPrepared", orderTrans.size() + "");
                    if (getmMvpView() != null) {
                        getmMvpView().updateOrderTran(orderTrans);
                    }
                }
            }

            @Override
            public void onResponseError(ANError anError) {

            }
        });
    }
}
