package com.anphuocthai.staff.ui.delivery.debt.inventorydebt;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.ui.base.BaseViewHolder;
import com.anphuocthai.staff.ui.delivery.model.Hanghoa;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;
import com.anphuocthai.staff.utils.Enum;
import com.anphuocthai.staff.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.anphuocthai.staff.utils.Enum.FieldValueType.NORMAL;

public class InventoryDebtAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private Callback mCallback;
    private List<OrderTran> orderTranArrayList;

    private Context context;
    private List<OrderTran> mListOrigin = new ArrayList<>();

    public InventoryDebtAdapter(ArrayList<OrderTran> orderTranArrayList) {
        this.orderTranArrayList = orderTranArrayList;
    }

    public void setCallback(Callback mCallback) {
        this.mCallback = mCallback;
    }


    public void setContext(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_cong_no_ton, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);

    }

    @Override
    public int getItemCount() {
        if (!Utils.isNull(orderTranArrayList)) {
            return orderTranArrayList.size();
        }
        return 0;
    }

    public interface Callback {
        void onItemSelected();
    }

    public void addItems(List<OrderTran> orderTrans) {
        orderTranArrayList.clear();
        mListOrigin.clear();
        orderTranArrayList.addAll(orderTrans);
        mListOrigin.addAll(orderTrans);
        notifyDataSetChanged();
    }

    public List<OrderTran> getItemSelected() {
        List<OrderTran> result = new ArrayList<>();
        for (OrderTran orderTran : orderTranArrayList) {
            if (orderTran.isSelected()) {
                result.add(orderTran);
            }
        }
        return result;
    }

    public void removeItemsChecked() {
        List<OrderTran> list = getItemSelected();
        orderTranArrayList.removeAll(list);
        notifyDataSetChanged();
    }

    public int countItem() {
        if (orderTranArrayList != null) {
            return orderTranArrayList.size();
        }
        return 0;
    }

    void onFilterMonth(String month) {
        if (Utils.isNull(mListOrigin)) return;
        orderTranArrayList.clear();
        if (month == null) {
            orderTranArrayList.addAll(mListOrigin);
        } else {
            String time;
            for (OrderTran orderTran : mListOrigin) {
                if (orderTran.getMaDonDat() != null) {
                    if (orderTran.getNgayKetThuc() != null) {
                        time = orderTran.getNgayKetThuc().toString();
                    } else {
                        time = orderTran.getNgayBatDau();
                    }
                    if (time.contains(month)) {
                        orderTranArrayList.add(orderTran);
                    }
                }
            }
        }
        notifyDataSetChanged();
    }

    void unSelectedAll() {
        if (Utils.isNull(orderTranArrayList)) return;
        for (OrderTran orderTran : orderTranArrayList) {
            orderTran.setSelected(false);
        }
        notifyDataSetChanged();
    }

    void selectedAll() {
        if (Utils.isNull(orderTranArrayList)) return;
        for (OrderTran orderTran : orderTranArrayList) {
            orderTran.setSelected(true);
        }
        notifyDataSetChanged();
    }

    public List<OrderTran> getOrderTranArrayList() {
        return orderTranArrayList;
    }

    public class ViewHolder extends BaseViewHolder {
        private CheckBox mCheckBox;
        TextView txtOrderCode;
        TextView txtCustomerName;

        TextView labelSumaryMoney;
        TextView txtSumaryMoney;

        TextView labelMustPay;
        TextView txtMustPay;

        TextView labelRemain;
        TextView txtRemain;

        public ViewHolder(View itemView) {
            super(itemView);
            initView(itemView);
            itemView.setOnClickListener(v -> {
                OrderTran orderTran = orderTranArrayList.get(getCurrentPosition());
                orderTran.setSelected(!orderTran.isSelected());
                mCheckBox.setChecked(orderTran.isSelected());
                if (mCallback != null) {
                    mCallback.onItemSelected();
                }

            });
        }

        private void initView(View itemView) {
            mCheckBox = itemView.findViewById(R.id.checkBox);
            txtOrderCode = itemView.findViewById(R.id.all_debt_txt_order_code);
            txtCustomerName = itemView.findViewById(R.id.all_debt_txt_order_customer_name);

            labelSumaryMoney = itemView.findViewById(R.id.debt_label_sumary_money);
            txtSumaryMoney = itemView.findViewById(R.id.debt_txt_sumary_money);
            labelMustPay = itemView.findViewById(R.id.debt_label_must_pay);
            txtMustPay = itemView.findViewById(R.id.debt_txt_must_pay);
            labelRemain = itemView.findViewById(R.id.debt_label_remain);
            txtRemain = itemView.findViewById(R.id.debt_txt_remain);

        }

        protected void clear() {
        }

        public void onBind(int position) {
            super.onBind(position);
            final OrderTran orderTran = orderTranArrayList.get(position);

            mCheckBox.setChecked(orderTran.isSelected());

            if (orderTran.getMaDonDat() != null) {
                String date = orderTran.getNgayKetThuc() != null ? orderTran.getNgayKetThuc() : orderTran.getNgayBatDau();
                String orderId = "ĐH số : " + orderTran.getMaDonDat() + " - " + Utils.formatDate(date, false);
                txtOrderCode.setText(orderId);
            }
            if (Utils.isNull(orderTran.getContentDetail())) {
                String content = "";
                if (orderTran.getTenThanhVien() != null) {
                    content = orderTran.getTenThanhVien();
                }
                String diachiVanChuyen = orderTran.getDiaChiVanChuyen();
                if (!diachiVanChuyen.isEmpty()) {
                    content += "\n" + diachiVanChuyen;
                }

                if (orderTran.getHanghoas() != null) {
                    int size = orderTran.getHanghoas().size();
                    if (size > 0) {
                        String goodsNames = new String();
                        for (int i = 0; i < size; i++) {
                            if (orderTran.getHanghoas().get(i).getHanghoa().getTenHangHoa() != null) {
                                Hanghoa hanghoa = orderTran.getHanghoas().get(i);
                                if ((i == 0 && (size == 1)) || i == size - 1) {
                                    goodsNames += hanghoa.getHanghoa().getTenHangHoa() + "(" + Utils.formatValue(hanghoa.getSoLuong(), NORMAL) + hanghoa.getDonVi() + ")";
                                } else {
                                    goodsNames += hanghoa.getHanghoa().getTenHangHoa() + "(" + Utils.formatValue(hanghoa.getSoLuong(), NORMAL) + hanghoa.getDonVi() + ")" + "\n";
                                }

                            }
                        }
                        content += "\n" + goodsNames;
                    }
                }
                orderTran.setContentDetail(content);
            }


            txtCustomerName.setText(orderTran.getContentDetail());

            txtSumaryMoney.setText("");
            if (orderTran.getTongGiaTri() != null) {
                txtSumaryMoney.setText(Utils.formatValue(orderTran.getTongGiaTri(), Enum.FieldValueType.CURRENCY));
            }

            txtMustPay.setText("");
            if (orderTran.getTongThanhToan() != null) {
                txtMustPay.setText(Utils.formatValue(orderTran.getTongThanhToan(), Enum.FieldValueType.CURRENCY));
            }

            txtRemain.setText("");
            if (orderTran.getPhaiThanhToan() != null) {
                txtRemain.setText(Utils.formatValue(orderTran.getPhaiThanhToan(), Enum.FieldValueType.CURRENCY));
            }
        }
    }

    public class EmptyViewHolder extends BaseViewHolder {

        @BindView(R.id.btn_retry)
        Button retryButton;

        @BindView(R.id.tv_message)
        TextView messageTextView;

        @BindView(R.id.txt_retry)
        TextView txtRetry;

        public EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            retryButton.setVisibility(View.GONE);
            txtRetry.setVisibility(View.GONE);
        }

        @Override
        protected void clear() {

        }

        @OnClick(R.id.btn_retry)
        void onRetryClick() {
//            if (mCallback != null)
//                mCallback.onBlogEmptyViewRetryClick();
        }
    }
}
