package com.anphuocthai.staff.ui.home.statistics.viewholder

import android.view.MotionEvent
import android.view.View
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.ViewPager
import com.anphuocthai.staff.R
import com.anphuocthai.staff.api.ApiURL
import com.anphuocthai.staff.api.addTo
import com.anphuocthai.staff.api.getRequest
import com.anphuocthai.staff.api.runOnMain
import com.anphuocthai.staff.model.BannerResponse
import com.anphuocthai.staff.ui.base.BaseViewHolder
import com.anphuocthai.staff.ui.home.MainActivity
import com.anphuocthai.staff.ui.home.statistics.viewholder.newdailyadapter.BannerAdapter
import com.anphuocthai.staff.utils.Utils
import com.anphuocthai.staff.utils.printLog
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.item_banner.view.*
import java.util.concurrent.TimeUnit

/**
 * Created by OpenYourEyes on 6/15/2020
 */
class BannerViewHolder(val view: View) : BaseViewHolder(view) {

    var compositeDisposable: CompositeDisposable
    var currentPager: Int = 0
    var isAuto = true
    private val publishSubject = PublishSubject.create<Unit>()
    private lateinit var mAdapter: BannerAdapter
    override fun clear() {
    }

    init {
        val context = view.context
        compositeDisposable = if (context is MainActivity) {
            context.compositeDisposable
        } else {
            CompositeDisposable()
        }
        initView()
        initData()
        requestBanner()
        publishSubject.debounce(5, TimeUnit.SECONDS)
                .subscribe {
                    printLog(this, "unblock auto start")
                    isAuto = true
                }.addTo(compositeDisposable)

    }


    private fun initView() {
        view.pageIndicator.apply {
            fillColor = ContextCompat.getColor(itemView.context, R.color.colorPrimary)
            pageColor = ContextCompat.getColor(itemView.context, R.color.colorDisableCoin)
            radius = Utils.convertDpToPixel(itemView.context, 3.0f).toFloat()
            strokeWidth = 0f
        }
    }

    private fun initData() {
        mAdapter = BannerAdapter(itemView.context)
        view.viewPageBanner.apply {
            adapter = mAdapter
            clipToPadding = false
            clipChildren = false
            offscreenPageLimit = 3

//            val padding = Utils.convertDpToPixel(context, 30f)
//            var margin = Utils.convertDpToPixel(context, 12f)
//            setPadding(padding, 0, padding, 0)
//            pageMargin = margin

//            setPageTransformer(true) { page, position ->
//                printLog(this, "Page $position")
//            }
        }
        view.viewPageBanner.setOnTouchListener { _, motionEvent ->
            if (motionEvent.action == MotionEvent.ACTION_UP) {
                isAuto = false
                publishSubject.onNext(Unit)
            }
            false
        }
        view.viewPageBanner.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(p0: Int) {
            }

            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {
            }

            override fun onPageSelected(position: Int) {
                currentPager = position
            }

        })

        view.pageIndicator.setViewPager(view.viewPageBanner)
    }


    private fun requestBanner() {
        ApiURL.BANNER_API.getRequest()
                .build()
                .getObjectObservable(BannerResponse::class.java)
                .runOnMain()
                .subscribe({
                    it?.let {
                        val list = it.data ?: mutableListOf()
                        val adapter = view.viewPageBanner.adapter as BannerAdapter
                        adapter.updateList(list)
                        startAutoSlide()
                    }

                }, {
                    printLog(this, "BANNER_API ${it.message}")
                }).addTo(compositeDisposable)
    }

    private fun startAutoSlide() {
        if (mAdapter.count < 2) {
            return
        }
        Observable.interval(5, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .filter { isAuto }
                .subscribe {
                    if (currentPager == (mAdapter.count - 1)) {
                        view.viewPageBanner.setCurrentItem(0, false)
                    } else {
                        view.viewPageBanner.setCurrentItem(currentPager + 1, true)
                    }
                }.addTo(compositeDisposable)

    }

}
