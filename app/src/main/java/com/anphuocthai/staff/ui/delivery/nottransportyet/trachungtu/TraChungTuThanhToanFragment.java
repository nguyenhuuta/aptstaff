package com.anphuocthai.staff.ui.delivery.nottransportyet.trachungtu;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.di.component.ActivityComponent;
import com.anphuocthai.staff.ui.base.BaseFragment;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;
import com.anphuocthai.staff.widget.LoadMoreRecyclerView2;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by OpenYourEyes on 11/23/2020
 */
public class TraChungTuThanhToanFragment extends BaseFragment implements ITraChungTuView, SwipeRefreshLayout.OnRefreshListener {
    private SwipeRefreshLayout swipeTraChungTu;
    private LoadMoreRecyclerView2 recyclerTraChungTu;
    private TraChungTuAdapter adapter;
    int page = 0; // start page = 1
    int limit = 30;
    @Inject
    ITraChungTuPresenter mPresenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tra_chung_tu_thanh_toan_fragment, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            mPresenter.onAttach(this);
        }
        return view;
    }

    @Override
    protected void setUp(View view) {
        swipeTraChungTu = view.findViewById(R.id.swipeTraChungTu);
        recyclerTraChungTu = view.findViewById(R.id.recyclerTraChungTu);

        swipeTraChungTu.setOnRefreshListener(this);
        recyclerTraChungTu.setHasFixedSize(true);
        recyclerTraChungTu.setLayoutManager(new LinearLayoutManager(getBaseActivity()));
        adapter = new TraChungTuAdapter();
        recyclerTraChungTu.setAdapter(adapter);
        recyclerTraChungTu.addListenerLoadMore(() -> {
            print("addListenerLoadMore");
            requestAPI();
            return null;
        });
    }

    @Override
    public void onRefresh() {
        swipeTraChungTu.setRefreshing(false);
        recyclerTraChungTu.setEndOfPage(false);
        page = 0;
        requestAPI();
    }

    @Override
    public void getDanhSachDonDat(List<OrderTran> list) {
        if (list != null && list.size() < limit) {
            recyclerTraChungTu.setEndOfPage(true);
        }
        recyclerTraChungTu.setLoaded();
        page++;
        adapter.updateList(list, page == 1);
    }

    @Override
    public void showMessage(String message) {
        super.showMessage(message);
        recyclerTraChungTu.setLoaded();
    }

    private void requestAPI() {
        print("requestAPI " + (page + 1));
        mPresenter.layDanhSachDonDat(page + 1, limit);
    }
}
