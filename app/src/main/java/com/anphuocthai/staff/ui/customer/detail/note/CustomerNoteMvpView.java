package com.anphuocthai.staff.ui.customer.detail.note;

import com.anphuocthai.staff.model.customer.Note;
import com.anphuocthai.staff.ui.base.MvpView;

import java.util.ArrayList;

public interface CustomerNoteMvpView extends MvpView {

    void updateNote(ArrayList<Note> noteArrayList);
}
