package com.anphuocthai.staff.ui.delivery.preparetransport;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.ui.base.BaseViewHolder;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;

import java.util.List;

public class PrepareTranAdapter extends RecyclerView.Adapter<BaseViewHolder>{

    private List<OrderTran> mOrderTrans;

    public PrepareTranAdapter(List<OrderTran> mOrderTrans) {
        this.mOrderTrans = mOrderTrans;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }
}
