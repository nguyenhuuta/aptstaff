package com.anphuocthai.staff.ui.delivery.debt.customerdebt;

public class ReportResponse {

    String code;
    String message;
    String idRecord;
    String objectInfo;

    public ReportResponse() {
    }

    public ReportResponse(String code, String message, String idRecord ) {
        this.code = code;
        this.message = message;
        this.idRecord = idRecord;

    }



    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(String idRecord) {
        this.idRecord = idRecord;
    }

    public String getObjectInfo() {
        return objectInfo;
    }

    public void setObjectInfo(String objectInfo) {
        this.objectInfo = objectInfo;
    }
}
