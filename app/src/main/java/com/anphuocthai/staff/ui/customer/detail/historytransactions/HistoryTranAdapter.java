package com.anphuocthai.staff.ui.customer.detail.historytransactions;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.ui.base.BaseViewHolder;
import com.anphuocthai.staff.ui.delivery.model.Hanghoa;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;
import com.anphuocthai.staff.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.anphuocthai.staff.utils.Enum.FieldValueType.CURRENCY;
import static com.anphuocthai.staff.utils.Enum.FieldValueType.NORMAL;

public class HistoryTranAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private Callback mCallback;
    private List<OrderTran> orderTranArrayList;

    public HistoryTranAdapter(List<OrderTran> orderTranArrayList) {
        this.orderTranArrayList = orderTranArrayList;
    }

    public void setCallback(Callback mCallback) {
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_history_transaction, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);

    }

    @Override
    public int getItemCount() {
        if (Utils.isNull(orderTranArrayList)) {
            return 0;
        }
        return orderTranArrayList.size();
    }


    public interface Callback {

    }


    public class ViewHolder extends BaseViewHolder {
        TextView mOrderId, mAddressCustomer, mOrderName, mTotalOrder;


        public ViewHolder(View view) {
            super(view);
            initView(view);
            mOrderId = view.findViewById(R.id.orderId);
            mAddressCustomer = view.findViewById(R.id.addressCustomer);
            mOrderName = view.findViewById(R.id.orderName);
            mTotalOrder = view.findViewById(R.id.totalOrder);
        }

        private void initView(View itemView) {
        }

        protected void clear() {
        }

        public void onBind(int position) {
            super.onBind(position);
            OrderTran orderTran = orderTranArrayList.get(position);
            if (orderTran.getNgayKetThuc() != null) {
                mOrderId.setText(": " + orderTran.getMaDonDat() + " - " + Utils.formatDate(orderTran.getNgayKetThuc().toString(), false));
            } else {
                mOrderId.setText(": " + orderTran.getMaDonDat() + " - " + Utils.formatDate(orderTran.getNgayBatDau(), false));
            }
            String addressList = "";
            if (orderTran.getVanchuyens() != null) {
                int size = orderTran.getVanchuyens().size();
                if (size > 0) {
                    if (orderTran.getVanchuyens().get(0).getDiaChiNhanHang() != null) {
                        addressList += String.format(orderTran.getVanchuyens().get(0).getDiaChiNhanHang());
                    }
                }
            }
            if (Utils.isNull(addressList)) {
                addressList = "Không xác định";
            }
            mAddressCustomer.setText(": " + addressList);
            StringBuilder goodsNames = new StringBuilder();

            List<Hanghoa> listHangHoa = orderTran.getHanghoas();
            int totalPrice = 0;
            if (!Utils.isNull(listHangHoa)) {

                for (Hanghoa hanghoa : listHangHoa) {
                    if (goodsNames.length() > 0) {
                        goodsNames.append("\n");
                    }
                    totalPrice += hanghoa.getSoLuong() * hanghoa.getGiaTien();

                    goodsNames.append("- ")
                            .append(hanghoa.getHanghoa().getTenHangHoa())
                            .append("(")
                            .append(Utils.formatValue(hanghoa.getSoLuong(), NORMAL))
                            .append(hanghoa.getDonVi())
                            .append(")")
                            .append(" - ")
                            .append(Utils.formatValue(hanghoa.getGiaTien(), CURRENCY));
                }
            }
            if (goodsNames.length() == 0) {
                goodsNames.append("Không xác định");
            }
            mOrderName.setText(goodsNames.toString());
            int price = orderTran.getTongThanhToan();// != 0 ? orderTran.getTongThanhToan() : orderTran.getTongGiaTri();
            String convertPrice = ": " + Utils.formatValue(totalPrice, CURRENCY);
            mTotalOrder.setText(convertPrice);

        }
    }

    public class EmptyViewHolder extends BaseViewHolder {

        @BindView(R.id.btn_retry)
        Button retryButton;

        @BindView(R.id.tv_message)
        TextView messageTextView;

        @BindView(R.id.txt_retry)
        TextView txtRetry;

        public EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            retryButton.setVisibility(View.GONE);
            txtRetry.setVisibility(View.GONE);
        }

        @Override
        protected void clear() {

        }

        @OnClick(R.id.btn_retry)
        void onRetryClick() {
        }
    }
}

