package com.anphuocthai.staff.ui.profile.personalinfo;

import com.anphuocthai.staff.di.PerActivity;
import com.anphuocthai.staff.ui.base.MvpPresenter;

@PerActivity
public interface ProfileMvpPresenter<V extends ProfileMvpView> extends MvpPresenter<V> {
    void onChangePass();
}
