package com.anphuocthai.staff.ui.customer.detail.infomation.editinformation.phonetypedialog;

import com.anphuocthai.staff.ui.base.DialogMvpView;

public interface PhoneTypeMvpView extends DialogMvpView {

    void dismissDialog();
    void onStopDebtSuccess();
}
