package com.anphuocthai.staff.ui.ordercart.payconfig;

import com.anphuocthai.staff.model.customer.Customer;
import com.anphuocthai.staff.ui.base.DialogMvpView;

public interface OrderConfigMvpView extends DialogMvpView {

    void dismissDialog();
    void onStopDebtSuccess();
    void getCustomerByGuidSuccess(Customer customer);
}
