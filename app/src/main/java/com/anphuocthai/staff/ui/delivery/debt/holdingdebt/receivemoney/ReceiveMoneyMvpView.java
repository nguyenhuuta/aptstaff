package com.anphuocthai.staff.ui.delivery.debt.holdingdebt.receivemoney;

import com.anphuocthai.staff.ui.base.DialogMvpView;

public interface ReceiveMoneyMvpView extends DialogMvpView {
    void dismissDialog();
}
