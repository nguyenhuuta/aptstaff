package com.anphuocthai.staff.ui.delivery.debt.customerdebt;

import com.anphuocthai.staff.ui.base.MvpPresenter;

public interface CustomerDebtMvpPresenter<V extends CustomerDebtMvpView> extends MvpPresenter<V> {
//    void onViewPrepared();
//
//    Flowable<List<OrderTran>> getAllDebtApiCall(JSONObject object);
//
//    void onSetStartCollectDebt(OrderTran orderTran);
//

    void getCustomerDebt(boolean isFirst);
}
