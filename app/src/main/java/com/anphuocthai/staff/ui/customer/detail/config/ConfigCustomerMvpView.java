package com.anphuocthai.staff.ui.customer.detail.config;

import com.anphuocthai.staff.model.customer.Customer;
import com.anphuocthai.staff.ui.base.MvpView;

public interface ConfigCustomerMvpView extends MvpView {

   void onUpdateView(Customer customer);
}
