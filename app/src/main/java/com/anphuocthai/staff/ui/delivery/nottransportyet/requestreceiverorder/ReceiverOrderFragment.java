package com.anphuocthai.staff.ui.delivery.nottransportyet.requestreceiverorder;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.di.component.ActivityComponent;
import com.anphuocthai.staff.ui.base.BaseFragment;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;
import com.anphuocthai.staff.ui.delivery.model.Vanchuyen;
import com.anphuocthai.staff.ui.searchsimpledata.SearchSimpleActivity;
import com.anphuocthai.staff.ui.searchsimpledata.SearchSimpleItem;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

public class ReceiverOrderFragment extends BaseFragment implements ReceiverOrderMvpView, SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = ReceiverOrderFragment.class.getSimpleName();

    @Inject
    ReceiverOrderMvpPresenter<ReceiverOrderMvpView> mPresenter;

    @Inject
    ReceiverOrderAdapter mAdapter;

    @Inject
    LinearLayoutManager mLayoutManager;

    private RecyclerView recyclerView;

    private TextView labelSumaryCV;

    private TextView txtSumaryCV;


    private TextView lblSumaryOrder;


    private TextView labelSumaryMustCollect;

    private TextView txtSumaryMustCollect;

    private TextView labelSumaryAlreadyCollect;

    private TextView txtSumaryOrder;

    private TextView txtSumaryAlreadyCollect;

    private View sumaryContainer;

    private SwipeRefreshLayout mSwipeRefreshLayout;

    private List<OrderTran> mOrderTrans = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.receiver_order_fragment, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            mPresenter.onAttach(this);
            mAdapter.setmPresenter(mPresenter);
            mAdapter.setBaseActivity(getBaseActivity());
        }
        return view;
    }

    @Override
    protected void setUp(View view) {

        lblSumaryOrder = view.findViewById(R.id.lblSumaryOrder);

        sumaryContainer = view.findViewById(R.id.layout_sumary);

        labelSumaryCV = view.findViewById(R.id.label_sumary_cv);

        txtSumaryCV = view.findViewById(R.id.txt_sumary_cv);

        labelSumaryCV.setVisibility(View.GONE);

        txtSumaryCV.setVisibility(View.GONE);

        labelSumaryMustCollect= view.findViewById(R.id.label_must_collect);

        txtSumaryMustCollect= view.findViewById(R.id.txt_must_collect);

        labelSumaryMustCollect.setVisibility(View.GONE);
        txtSumaryMustCollect.setVisibility(View.GONE);

        labelSumaryAlreadyCollect= view.findViewById(R.id.label_already_collect);

        txtSumaryAlreadyCollect= view.findViewById(R.id.txt_already_collect);

        labelSumaryAlreadyCollect.setVisibility(View.GONE);
        txtSumaryAlreadyCollect.setVisibility(View.GONE);

        txtSumaryOrder = view.findViewById(R.id.txt_number_order);


        recyclerView = view.findViewById(R.id.recycle_view_receiver_order);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        mPresenter.onGetAllNotTranOrder();
        mPresenter.hideRecyclerView(true);
        setupPullToRefresh(view);

    }

    public static ReceiverOrderFragment newInstance() {
        Bundle args = new Bundle();
        ReceiverOrderFragment fragment = new ReceiverOrderFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }



    @Override
    public void displayOrderTrans(ArrayList<OrderTran> orderTrans) {
        mOrderTrans = orderTrans;

        updateOrderTranUI(orderTrans);
    }

    private void updateOrderTranUI(List<OrderTran> orderTrans) {
        mAdapter.addItems(orderTrans);
        mPresenter.hideRecyclerView(false);
        mSwipeRefreshLayout.setRefreshing(false);

        int sumReceived = 0;
        int sumNotReceived = 0;

        for (int i = 0; i < orderTrans.size(); i++) {
            if (orderTrans.get(i).getVanchuyens().get(0).getNguoiChuyenID() == null || orderTrans.get(i).getVanchuyens().get(0).getNguoiChuyenID() <= 0) {
                sumNotReceived++;
            }

            if (orderTrans.get(i).getVanchuyens().get(0).getNguoiChuyenID() != null &&
                    orderTrans.get(i).getVanchuyens().get(0).getNguoiChuyenID() > 0){
                sumReceived++;
            }
        }

        if (orderTrans.size() > 0) {
            sumaryContainer.setVisibility(View.VISIBLE);
            txtSumaryOrder.setText(String.valueOf(sumReceived) + " " + getString(R.string.common_received) + ", " +
                    String.valueOf(sumNotReceived) + " " + getString(R.string.common_not_received));
        }
    }

    @Override
    public void hideRecyclerView(boolean isHide) {
        if (isHide) {
            recyclerView.setVisibility(View.INVISIBLE);
        }else {
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    private boolean isViewShown = false;

    @Override
    public void setUserVisibleHint(boolean visible) {
        super.setUserVisibleHint(visible);
        if (getView() != null) {
            isViewShown = true;
        } else {
            isViewShown = false;
        }
    }

    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.setRefreshing(true);
        mPresenter.onGetAllNotTranOrder();
    }

    private void setupPullToRefresh(View view) {
        // SwipeRefreshLayout
        mSwipeRefreshLayout =  view.findViewById(R.id.swipe_container_receiver_order);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);
        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
        if (!isViewShown) {
            mSwipeRefreshLayout.post(() -> {
                onRefresh();
            });
        }
    }


//
//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        inflater.inflate(R.menu.search_menu, menu);
//
//        MenuItem myActionMenuItem = menu.findItem( R.id.action_search);
//        SearchView searchView = (SearchView) myActionMenuItem.getActionView();
//        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String query) {
//                if(!searchView.isIconified()) {
//                    searchView.setIconified(true);
//                }
//                myActionMenuItem.collapseActionView();
//                return false;
//            }
//            @Override
//            public boolean onQueryTextChange(String s) {
//                showSearchResult(s);
//                return true;
//            }
//        });
//
//        super.onCreateOptionsMenu(menu, inflater);
//    }
//
//    private void showSearchResult(String s) {
//        if (s == null || s.isEmpty()) {
//            mAdapter.addItems(mOrderTrans);
//            return;
//        }
//
//        ArrayList<OrderTran> searchOrderTran = new ArrayList<>();
//        for (OrderTran orderTran : mOrderTrans) {
//            List<Vanchuyen> vanchuyens = orderTran.getVanchuyens();
//            if (vanchuyens != null && !vanchuyens.isEmpty()) {
//                if (vanchuyens.get(0).getName() != null && vanchuyens.get(0).getName().toLowerCase().contains(s.toLowerCase())) {
//                    searchOrderTran.add(orderTran);
//                }
//            }
//        }
//        mAdapter.addItems(searchOrderTran);
//    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.filter_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_filter) {
            showFilterScreen();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showFilterScreen() {
        if (mOrderTrans != null && !mOrderTrans.isEmpty()) {
            Set<Integer> searchEmployeeIds = new HashSet<>();
            ArrayList<SearchSimpleItem> searchSimpleItems = new ArrayList<>();

            for(OrderTran orderTran : mOrderTrans) {
                List<Vanchuyen> vanchuyens = orderTran.getVanchuyens();
                if (vanchuyens != null && !vanchuyens.isEmpty()) {
                    boolean added = searchEmployeeIds.add(vanchuyens.get(0).getNguoiChuyenID());
                    if (added) {
                        Integer id = vanchuyens.get(0).getNguoiChuyenID();
                        String name = vanchuyens.get(0).getTenNguoiChuyen();
                        if (id == null) {
                            id = SearchSimpleActivity.ID_NULL;
                        }
                        if (name == null || name.trim().isEmpty()) {
                            name = getString(R.string.order_tran_select_tran);
                        }
                        searchSimpleItems.add(new SearchSimpleItem(id, name));
                    }
                }
            }

            if (!searchSimpleItems.isEmpty()) {
                Intent intent = new Intent(getActivity(), SearchSimpleActivity.class);
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList(SearchSimpleActivity.NAME_PARCELABLE_ARRAYLIST, searchSimpleItems);
                intent.putExtras(bundle);
                startActivityForResult(intent, SearchSimpleActivity.REQUEST_CODE_SELECT_EMP_TAB1);
            } else {
                Toast.makeText(getActivity(), "Danh sách lọc bị trống!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getActivity(), "Danh sách lọc bị trống!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SearchSimpleActivity.REQUEST_CODE_SELECT_EMP_TAB1) {
            if (resultCode == Activity.RESULT_OK) {
                int id = data.getIntExtra(SearchSimpleActivity.KEY_ID, SearchSimpleActivity.ID_NULL);
                ArrayList<OrderTran> searchOrderTran = new ArrayList<>();
                for(OrderTran orderTran : mOrderTrans) {
                    List<Vanchuyen> vanchuyens = orderTran.getVanchuyens();
                    if (vanchuyens != null && !vanchuyens.isEmpty()) {
                        if (SearchSimpleActivity.ID_NULL == id) {
                            if (vanchuyens.get(0).getNguoiChuyenID() == null) {
                                searchOrderTran.add(orderTran);
                            }
                        } else if (vanchuyens.get(0).getNguoiChuyenID() != null && vanchuyens.get(0).getNguoiChuyenID() == id) {
                            searchOrderTran.add(orderTran);
                        }
                    }
                }
                updateOrderTranUI(searchOrderTran);

            } else if (resultCode == Activity.RESULT_CANCELED) {
                updateOrderTranUI(mOrderTrans);
            }
        }
    }
}
