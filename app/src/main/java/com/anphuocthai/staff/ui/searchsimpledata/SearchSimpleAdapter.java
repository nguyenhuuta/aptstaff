package com.anphuocthai.staff.ui.searchsimpledata;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.ui.base.BaseViewHolder;
import com.anphuocthai.staff.utils.Enum;
import com.anphuocthai.staff.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SearchSimpleAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public interface OnEmployeeSelectedListener {
        void onEmployeeSelected(Integer id);
    }

    public static final int VIEW_TYPE_EMPTY = 0;

    public static final int VIEW_TYPE_NORMAL = 1;

    private List<SearchSimpleItem> mSearchSimpleItems;

    private OnEmployeeSelectedListener mOnEmployeeSelectedListener;

    public SearchSimpleAdapter(List<SearchSimpleItem> searchSimpleItems, OnEmployeeSelectedListener onEmployeeSelectedListener) {
        this.mSearchSimpleItems = searchSimpleItems;
        this.mOnEmployeeSelectedListener = onEmployeeSelectedListener;
    }

    public void updateSearchEmployees(List<SearchSimpleItem> searchSimpleItems) {
        this.mSearchSimpleItems = searchSimpleItems;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new ViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search_customer, parent, false));
            default:
                return new EmptyViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_view, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemCount() {
        return mSearchSimpleItems.size();
    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_NORMAL;
    }

    public class ViewHolder extends BaseViewHolder {

        public TextView txtCustomerSearchName;
        public ImageView imgCustomer;
        private TextView lblDebt;
        private TextView txtDebt;

        public ViewHolder(View itemView) {
            super(itemView);
            initView(itemView);
        }


        private void initView(View itemView) {
            lblDebt = itemView.findViewById(R.id.lbl_debt);
            txtDebt = itemView.findViewById(R.id.txt_debt);
            lblDebt.setVisibility(View.GONE);
            txtDebt.setVisibility(View.GONE);
            txtCustomerSearchName = itemView.findViewById(R.id.txt_customer_search_name);
            imgCustomer = itemView.findViewById(R.id.img_customer_search);
        }

        protected void clear() {
        }

        public void onBind(int position) {
            super.onBind(position);
            SearchSimpleItem se = mSearchSimpleItems.get(position);
            txtCustomerSearchName.setText(se.getName());


            if (se.getTongCongNo() > 0) {
                lblDebt.setVisibility(View.VISIBLE);
                txtDebt.setVisibility(View.VISIBLE);
                txtDebt.setText(Utils.formatValue(se.getTongCongNo(), Enum.FieldValueType.CURRENCY));
            } else {
                lblDebt.setVisibility(View.GONE);
                txtDebt.setVisibility(View.GONE);
            }
            itemView.setOnClickListener((View v) -> {
                onEmployeeSelected(se.getId());
            });
        }
    }

    private void onEmployeeSelected(int id) {
        if (mOnEmployeeSelectedListener != null) {
            mOnEmployeeSelectedListener.onEmployeeSelected(id);
        }
    }

    public class EmptyViewHolder extends BaseViewHolder {

        @BindView(R.id.btn_retry)
        Button retryButton;

        @BindView(R.id.tv_message)
        TextView messageTextView;

        @BindView(R.id.txt_retry)
        TextView txtRetry;

        public EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            retryButton.setVisibility(View.GONE);
            txtRetry.setVisibility(View.GONE);
        }

        @Override
        protected void clear() {

        }

        @OnClick(R.id.btn_retry)
        void onRetryClick() {
        }
    }

}

