package com.anphuocthai.staff.ui.customer.detail.historytransactions;

import com.anphuocthai.staff.ui.base.MvpView;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;

import java.util.List;

public interface HistoryTranMvpView extends MvpView {
    void updateOrderTran(List<OrderTran> orderTranArrayList);
}
