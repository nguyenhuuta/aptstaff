
package com.anphuocthai.staff.ui.home.notifications.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Notification implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("userID")
    @Expose
    private Integer userID;
    @SerializedName("iD_ThanhVien")
    @Expose
    private Object iDThanhVien;
    @SerializedName("ngayTao")
    @Expose
    private String ngayTao;
    @SerializedName("ngayGui")
    @Expose
    private String ngayGui;
    @SerializedName("nhanDe")
    @Expose
    private String nhanDe;
    @SerializedName("noiDung")
    @Expose
    private String noiDung;
    @SerializedName("trangThaiId")
    @Expose
    private Integer trangThaiId;
    @SerializedName("duLieuID")
    @Expose
    private String duLieuID;
    @SerializedName("loaiID")
    @Expose
    private Integer loaiID;
    @SerializedName("chanels")
    @Expose
    private String chanels;
    @SerializedName("chanelGroupId")
    @Expose
    private Object chanelGroupId;
    @SerializedName("isSeen")
    @Expose
    private Boolean isSeen;

    protected Notification(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        if (in.readByte() == 0) {
            userID = null;
        } else {
            userID = in.readInt();
        }
        ngayTao = in.readString();
        ngayGui = in.readString();
        nhanDe = in.readString();
        noiDung = in.readString();
        if (in.readByte() == 0) {
            trangThaiId = null;
        } else {
            trangThaiId = in.readInt();
        }
        duLieuID = in.readString();
        if (in.readByte() == 0) {
            loaiID = null;
        } else {
            loaiID = in.readInt();
        }
        chanels = in.readString();
        byte tmpIsSeen = in.readByte();
        isSeen = tmpIsSeen == 0 ? null : tmpIsSeen == 1;
    }

    public static final Creator<Notification> CREATOR = new Creator<Notification>() {
        @Override
        public Notification createFromParcel(Parcel in) {
            return new Notification(in);
        }

        @Override
        public Notification[] newArray(int size) {
            return new Notification[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public Object getIDThanhVien() {
        return iDThanhVien;
    }

    public void setIDThanhVien(Object iDThanhVien) {
        this.iDThanhVien = iDThanhVien;
    }

    public String getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(String ngayTao) {
        this.ngayTao = ngayTao;
    }

    public String getNgayGui() {
        return ngayGui;
    }

    public void setNgayGui(String ngayGui) {
        this.ngayGui = ngayGui;
    }

    public String getNhanDe() {
        return nhanDe;
    }

    public void setNhanDe(String nhanDe) {
        this.nhanDe = nhanDe;
    }

    public String getNoiDung() {
        return noiDung;
    }

    public void setNoiDung(String noiDung) {
        this.noiDung = noiDung;
    }

    public Integer getTrangThaiId() {
        return trangThaiId;
    }

    public void setTrangThaiId(Integer trangThaiId) {
        this.trangThaiId = trangThaiId;
    }

    public String getDuLieuID() {
        return duLieuID;
    }

    public void setDuLieuID(String duLieuID) {
        this.duLieuID = duLieuID;
    }

    public Integer getLoaiID() {
        return loaiID;
    }

    public void setLoaiID(Integer loaiID) {
        this.loaiID = loaiID;
    }

    public String getChanels() {
        return chanels;
    }

    public void setChanels(String chanels) {
        this.chanels = chanels;
    }

    public Object getChanelGroupId() {
        return chanelGroupId;
    }

    public void setChanelGroupId(Object chanelGroupId) {
        this.chanelGroupId = chanelGroupId;
    }

    public Boolean getIsSeen() {
        return isSeen;
    }

    public void setIsSeen(Boolean isSeen) {
        this.isSeen = isSeen;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        if (id == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(id);
        }
        if (userID == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(userID);
        }
        parcel.writeString(ngayTao);
        parcel.writeString(ngayGui);
        parcel.writeString(nhanDe);
        parcel.writeString(noiDung);
        if (trangThaiId == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(trangThaiId);
        }
        parcel.writeString(duLieuID);
        if (loaiID == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(loaiID);
        }
        parcel.writeString(chanels);
        parcel.writeByte((byte) (isSeen == null ? 0 : isSeen ? 1 : 2));
    }
}
