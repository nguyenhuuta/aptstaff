package com.anphuocthai.staff.ui.delivery.myorder.detailorder.detailtab;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.interfaces.IYNDialogCallback;
import com.anphuocthai.staff.ui.base.BaseViewHolder;
import com.anphuocthai.staff.ui.delivery.model.Hanghoa;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;
import com.anphuocthai.staff.utils.Enum;
import com.anphuocthai.staff.utils.Utils;
import com.anphuocthai.staff.utils.ui.UIUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.anphuocthai.staff.utils.Enum.FieldValueType.NORMAL;

public class DebtTabAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private Callback mCallback;
    private List<OrderTran> orderTranArrayList;

    private Context context;

    private DebtTabMvpPresenter<DebtTabMvpView> mPresenter;

    public DebtTabAdapter(ArrayList<OrderTran> orderTranArrayList) {
        this.orderTranArrayList = orderTranArrayList;
    }

    public void setCallback(Callback mCallback) {
        this.mCallback = mCallback;
    }

    public void setmPresenter(DebtTabMvpPresenter<DebtTabMvpView> mPresenter) {
        this.mPresenter = mPresenter;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new ViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_all_debt_layout, parent, false));
            case VIEW_TYPE_EMPTY:
            default:
                return new EmptyViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_view, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);

    }

    @Override
    public int getItemCount() {
        if (orderTranArrayList != null && orderTranArrayList.size() > 0) {
            return orderTranArrayList.size();
        } else {
            return 1;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (orderTranArrayList != null && orderTranArrayList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    public interface Callback {

    }

    public void addItems(List<OrderTran> orderTrans) {
        orderTranArrayList.clear();
        orderTranArrayList.addAll(orderTrans);
        notifyDataSetChanged();
    }


    public class ViewHolder extends BaseViewHolder {
        TextView txtOrderCode;
        TextView txtCustomerAddress;
        TextView txtCustomerName;
        TextView txtProductInfo;
        TextView btnCollectDebt;

        TextView labelSumaryMoney;
        TextView txtSumaryMoney;

        TextView labelMustPay;
        TextView txtMustPay;

        TextView labelRemain;
        TextView txtRemain;

        TextView labelAlreadyCollect;
        TextView txtAlreadyCollect;

        TextView labelCerti;
        TextView txtCerti;

        private TextView debtLocation;


        public ViewHolder(View itemView) {
            super(itemView);
            initView(itemView);
        }

        private void initView(View itemView){
            txtOrderCode = itemView.findViewById(R.id.all_debt_txt_order_code);
            txtCustomerAddress = itemView.findViewById(R.id.all_debt_txt_order_customer_address);
            txtCustomerName = itemView.findViewById(R.id.all_debt_txt_order_customer_name);
            txtProductInfo = itemView.findViewById(R.id.all_debt_txt_product_info);
            btnCollectDebt = itemView.findViewById(R.id.all_debt_btn_submit);
            btnCollectDebt.setText(R.string.accept_debt);


            labelSumaryMoney = itemView.findViewById(R.id.debt_label_sumary_money);
            txtSumaryMoney = itemView.findViewById(R.id.debt_txt_sumary_money);
            labelMustPay = itemView.findViewById(R.id.debt_label_must_pay);
            txtMustPay = itemView.findViewById(R.id.debt_txt_must_pay);
            labelRemain = itemView.findViewById(R.id.debt_label_remain);
            txtRemain = itemView.findViewById(R.id.debt_txt_remain);
            labelAlreadyCollect = itemView.findViewById(R.id.debt_label_already_collect);
            txtAlreadyCollect = itemView.findViewById(R.id.debt_txt_already_collect);
            labelCerti = itemView.findViewById(R.id.debt_label_certi);
            txtCerti = itemView.findViewById(R.id.debt_txt_certi);

            debtLocation = itemView.findViewById(R.id.txt_debt_location);
            debtLocation.setVisibility(View.GONE);

        }

        protected void clear() {}

        public void onBind(int position) {
            super.onBind(position);
            final OrderTran orderTran = orderTranArrayList.get(position);
            if (orderTran.getMaDonDat() != null) {
                if (orderTran.getNgayKetThuc() != null) {
                    txtOrderCode.setText(orderTran.getMaDonDat() + " - " + Utils.formatDate(orderTran.getNgayKetThuc().toString(), false));
                }else {
                    txtOrderCode.setText(orderTran.getMaDonDat() + " - " + Utils.formatDate(orderTran.getNgayBatDau().toString(), false));
                }
            }
            if (orderTran.getTenThanhVien() != null) {
                txtCustomerName.setText(orderTran.getTenThanhVien());
            }

            txtCustomerAddress.setText("");
            if (orderTran.getVanchuyens() != null) {
                int size = orderTran.getVanchuyens().size();
                if (size > 0) {
                    String addressList = new String();
                    //for (int i = 0; i< size; i++) {
                        if (orderTran.getVanchuyens().get(0).getDiaChiNhanHang() != null){
                            //if (size == 1 || (i == size - 1)) {
                                addressList += String.format(orderTran.getVanchuyens().get(0).getDiaChiNhanHang());
                           // }else {
                               // addressList += String.format(orderTran.getVanchuyens().get(i).getDiaChiNhanHang() + "\n");
                            //}

                        }
                   // }
                    txtCustomerAddress.setText(addressList);
                }
                if (txtCustomerAddress.getText().toString().trim().isEmpty()) {
                    txtCustomerAddress.setVisibility(View.GONE);
                }
            }


            txtProductInfo.setText("");
            if (orderTran.getHanghoas() != null) {
                int size = orderTran.getHanghoas().size();
                if (size > 0) {
                    String goodsNames = new String();
                    for (int i = 0; i < size; i++){
                        if (orderTran.getHanghoas().get(i).getHanghoa().getTenHangHoa() != null) {
                            Hanghoa hanghoa = orderTran.getHanghoas().get(i);
                            if ((i == 0 && (size == 1)) || i == size - 1) {
                                goodsNames += hanghoa.getHanghoa().getTenHangHoa() + "(" + Utils.formatValue(hanghoa.getSoLuong(), NORMAL) +  hanghoa.getDonVi() + ")";
                            }else {
                                goodsNames += hanghoa.getHanghoa().getTenHangHoa() + "(" + Utils.formatValue(hanghoa.getSoLuong(), NORMAL) +  hanghoa.getDonVi() + ")"   +"\n";
                            }

                        }
                    }

                    txtProductInfo.setText(goodsNames);
                }
            }

            txtSumaryMoney.setText("");
            if (orderTran.getTongGiaTri() != null) {
                txtSumaryMoney.setText(Utils.formatValue(orderTran.getTongGiaTri(), Enum.FieldValueType.CURRENCY));
            }

            txtMustPay.setText("");
            if (orderTran.getTongThanhToan() != null) {
                txtMustPay.setText(Utils.formatValue(orderTran.getTongThanhToan(), Enum.FieldValueType.CURRENCY));
            }

            txtRemain.setText("");
            if (orderTran.getPhaiThanhToan() != null){
                txtRemain.setText(Utils.formatValue(orderTran.getPhaiThanhToan(), Enum.FieldValueType.CURRENCY));
            }

            labelAlreadyCollect.setVisibility(View.GONE);
            txtAlreadyCollect.setVisibility(View.GONE);
            txtAlreadyCollect.setText("");
            if (orderTran.getDaThu() != null) {
                txtAlreadyCollect.setText(Utils.formatValue(orderTran.getDaThu(), Enum.FieldValueType.CURRENCY));
            }


            txtCerti.setVisibility(View.GONE);
            labelCerti.setVisibility(View.GONE);
            txtCerti.setText(R.string.debt_info_default);
            if (orderTran.getVanchuyens().size() > 0) {
                if (orderTran.getVanchuyens().get(0).getTenNguoiChuyen() != null) {
                    txtCerti.setText(orderTran.getVanchuyens().get(0).getTenNguoiChuyen());
                }
            }


            itemView.setOnClickListener((View v) -> {

            });

            btnCollectDebt.setOnClickListener((View v)-> {
                UIUtils.showYesNoDialog(context, "", context.getString(R.string.confirm_start_collect_debt), new IYNDialogCallback() {
                    @Override
                    public void accept() {
                        mPresenter.onSetStartCollectDebt(orderTran);
                    }

                    @Override
                    public void cancel() {

                    }
                });
            });
        }
    }

    public class EmptyViewHolder extends BaseViewHolder {

        @BindView(R.id.btn_retry)
        Button retryButton;

        @BindView(R.id.tv_message)
        TextView messageTextView;

        @BindView(R.id.txt_retry)
        TextView txtRetry;

        public EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            retryButton.setVisibility(View.GONE);
            txtRetry.setVisibility(View.GONE);
            messageTextView.setVisibility(View.VISIBLE);
            messageTextView.setText(R.string.common_coming_soon);
        }

        @Override
        protected void clear() {

        }

        @OnClick(R.id.btn_retry)
        void onRetryClick() {
//            if (mCallback != null)
//                mCallback.onBlogEmptyViewRetryClick();
        }
    }
}
