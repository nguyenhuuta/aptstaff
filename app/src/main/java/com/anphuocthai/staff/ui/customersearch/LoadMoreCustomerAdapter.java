package com.anphuocthai.staff.ui.customersearch;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.model.customer.SearchCustomer;
import com.anphuocthai.staff.ui.base.BaseViewHolder;
import com.anphuocthai.staff.utils.Enum;
import com.anphuocthai.staff.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoadMoreCustomerAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_EMPTY = 0;

    public static final int VIEW_TYPE_NORMAL = 1;

    public static final int VIEW_TYPE_HEADER = 2;

    private Callback mCallback;

    private List<SearchCustomer> customerHistory = new ArrayList<>();

    private List<SearchCustomer> customerList;

    public LoadMoreCustomerAdapter(List<SearchCustomer> customerArrayList) {
        this.customerList = customerArrayList;
    }

    public void setCallback(Callback mCallback) {
        this.mCallback = mCallback;
    }

    CSearchMvpPresenter<CSearchMvpView> mPresenter;

    public void setmPresenter(CSearchMvpPresenter<CSearchMvpView> mPresenter) {
        this.mPresenter = mPresenter;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new ViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search_customer, parent, false));
            case VIEW_TYPE_EMPTY:
                return new EmptyViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_view, parent, false));
            case VIEW_TYPE_HEADER:
                return new HeaderViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.header_search_customer, parent, false));
            default:
                return new EmptyViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_view, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);

    }

    @Override
    public int getItemCount() {
        if (customerList != null && customerList.size() > 0) {
            if (customerHistory.size() <= 0) {
                return customerList.size() + 1;
            }
            return customerList.size() + 2; // 2 header
        }
        return 1;
    }

    @Override
    public int getItemViewType(int position) {
        // Nếu danh sách khách hàng gần đây rỗng
        if (customerHistory.size() <= 0) {
            if (customerList.size() > 0 && position != 0) { // Nếu có khách hàng và vị trí khác 0
                return VIEW_TYPE_NORMAL;
            } else {
                return VIEW_TYPE_HEADER;
            }
        } else {
            if (customerList != null &&
                    customerList.size() > 0
                    && position != 0 &&
                    position != (customerHistory.size() + 1)) {
                return VIEW_TYPE_NORMAL;
            } else if (customerList != null && customerList.size() > 0) {
                return VIEW_TYPE_HEADER;
            } else {
                return VIEW_TYPE_EMPTY;
            }
        }


    }

    public interface Callback {
    }

    public void addItems(List<SearchCustomer> customerList) {
        this.customerList.addAll(customerList);
        notifyDataSetChanged();
    }

    public void addHistoryCustomers(List<SearchCustomer> searchCustomers) {
        this.customerHistory.clear();
        this.customerList.clear();
        this.customerHistory.addAll(searchCustomers);
        this.customerList.addAll(customerHistory);
        notifyDataSetChanged();
    }


    public class ViewHolder extends BaseViewHolder {

        public TextView txtCustomerSearchName;
        public ImageView imgCustomer;
        private TextView lblDebt;
        private TextView txtDebt;

        public ViewHolder(View itemView) {
            super(itemView);
            initView(itemView);
        }


        private void initView(View itemView) {
            lblDebt = itemView.findViewById(R.id.lbl_debt);
            txtDebt = itemView.findViewById(R.id.txt_debt);
            txtCustomerSearchName = itemView.findViewById(R.id.txt_customer_search_name);
            imgCustomer = itemView.findViewById(R.id.img_customer_search);
        }

        protected void clear() {
        }

        public void onBind(int position) {
            super.onBind(position);
            if (customerHistory.size() <= 0) {
                if (position == 0 || customerList.size() <= 0) {
                    return;
                }
                txtCustomerSearchName.setText(customerList.get(position - 1).getTenDayDu());
                if (customerList.get(position - 1).getCongNo() > 0) {
                    txtDebt.setText(Utils.formatValue(customerList.get(position - 1).getCongNo(), Enum.FieldValueType.CURRENCY));
                    lblDebt.setVisibility(View.VISIBLE);
                    txtDebt.setVisibility(View.VISIBLE);
                } else {
                    lblDebt.setVisibility(View.GONE);
                    txtDebt.setVisibility(View.GONE);
                }

                itemView.setOnClickListener((View v) -> {
                    mPresenter.onCustomerSelected(customerList.get(position - 1));
                });
            } else {
                if (position == 0 || position == (customerHistory.size() + 1)) {
                    return;
                }
                if (customerList.size() <= 0) {
                    return;
                }
                try {
                    if (position > 0 && position < (customerHistory.size() + 1)) {
                        txtCustomerSearchName.setText(customerList.get(position - 1).getTenDayDu());

                        if (customerList.get(position - 1).getCongNo() > 0) {
                            txtDebt.setText(Utils.formatValue(customerList.get(position - 1).getCongNo(), Enum.FieldValueType.CURRENCY));
                            lblDebt.setVisibility(View.VISIBLE);
                            txtDebt.setVisibility(View.VISIBLE);
                        } else {
                            lblDebt.setVisibility(View.GONE);
                            txtDebt.setVisibility(View.GONE);
                        }

                        itemView.setOnClickListener((View v) -> {

                            mPresenter.onCustomerSelected(customerList.get(position - 1));
                        });
                    } else {
                        txtCustomerSearchName.setText(customerList.get(position - 2).getTenDayDu());
                        if (customerList.get(position - 2).getCongNo() > 0) {
                            txtDebt.setText(Utils.formatValue(customerList.get(position - 2).getCongNo(), Enum.FieldValueType.CURRENCY));
                            lblDebt.setVisibility(View.VISIBLE);
                            txtDebt.setVisibility(View.VISIBLE);
                        } else {
                            lblDebt.setVisibility(View.GONE);
                            txtDebt.setVisibility(View.GONE);
                        }
                        itemView.setOnClickListener((View v) -> {

                            mPresenter.onCustomerSelected(customerList.get(position - 2));
                        });
                    }
                } catch (Exception e) {

                }
            }
        }
    }

    public class HeaderViewHolder extends BaseViewHolder {

        private TextView header;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            header = itemView.findViewById(R.id.txt_customer_header_search);
        }

        @Override
        protected void clear() {

        }

        @Override
        public void onBind(int position) {
            super.onBind(position);
            if (customerHistory.size() <= 0) {
                if (position == 0) {
                    header.setText(R.string.ci_adap_customer_list);
                }

            } else {
                int lastHeaderIndex = customerHistory.size() + 1;
                if (position != 0 && position != (customerHistory.size() + 1)) {
                    return;
                }

                if (position == 0 && customerHistory.size() > 0) {
                    header.setText(R.string.recent_customer);
                } else if (position == lastHeaderIndex) {
                    header.setText(R.string.ci_adap_customer_list);
                }
            }

        }
    }


    public class EmptyViewHolder extends BaseViewHolder {

        @BindView(R.id.btn_retry)
        Button retryButton;

        @BindView(R.id.tv_message)
        TextView messageTextView;

        @BindView(R.id.txt_retry)
        TextView txtRetry;

        public EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            retryButton.setVisibility(View.GONE);
            txtRetry.setVisibility(View.GONE);
        }

        @Override
        protected void clear() {

        }

        @OnClick(R.id.btn_retry)
        void onRetryClick() {
        }
    }

}

