package com.anphuocthai.staff.ui.delivery.debt;

import com.anphuocthai.staff.ui.base.MvpPresenter;

public interface DebtMvpPresenter<V extends DebtMvpView> extends MvpPresenter<V> {
}
