package com.anphuocthai.staff.ui.delivery.shipped;

import com.anphuocthai.staff.ui.base.MvpPresenter;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;

public interface ShippedMvpPresenter<V extends ShippedMvpView> extends MvpPresenter<V> {

    void getAllShippedOrderTrans();
    void onShowFinishShipDialog(OrderTran item);

}
