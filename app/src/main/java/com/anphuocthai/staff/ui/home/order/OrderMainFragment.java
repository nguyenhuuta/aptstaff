package com.anphuocthai.staff.ui.home.order;

import android.util.Log;
import android.view.View;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.androidnetworking.error.ANError;
import com.anphuocthai.staff.R;
import com.anphuocthai.staff.api.ApiURL;
import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.data.ActionRoom;
import com.anphuocthai.staff.data.category.CategoryDatabase;
import com.anphuocthai.staff.data.db.model.OrderProduct;
import com.anphuocthai.staff.data.product.ProductDatabase;
import com.anphuocthai.staff.entities.Category;
import com.anphuocthai.staff.interfaces.IAPICallback;
import com.anphuocthai.staff.model.order.Product;
import com.anphuocthai.staff.model.order.ProductCategory;
import com.anphuocthai.staff.ui.base.BaseActivity;
import com.anphuocthai.staff.ui.home.container.ContainerFragment;
import com.anphuocthai.staff.utils.App;
import com.anphuocthai.staff.utils.Utils;
import com.anphuocthai.staff.utils.extension.GridSpacingItemDecoration;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

public class OrderMainFragment extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener, OrderMainMvpView {

    private final String TAG = OrderMainFragment.class.getSimpleName();

    private RecyclerView recyclerViewCategory;
    private RecyclerView recyclerViewProduct;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ContainerFragment containerFragment;
    private boolean isShowGrid = true;
    private int mCurrentCategoryId;

    private CategoryDatabase mCategoryDatabase;
    private ProductDatabase mProductDatabase;

    private ArrayList<Product> mListProduct = new ArrayList<>();

    private Map<Integer, ArrayList<Product>> mMapCategoryProducts;

    private GridSpacingItemDecoration gridSpacingItemDecoration = new GridSpacingItemDecoration(3, 50, true);

    private List<OrderProduct> mListOrderLocal;

    private ArrayList<ProductCategory> mListCategory;
    private CategoryOrderAdapter mCategoryAdapter;
    private ProductOrderAdapter mProductAdapter;


    @Inject
    OrderMainMvpPresenter<OrderMainMvpView> mPresenter;

    @Override
    public void onRefresh() {
        mPresenter.getAllOrderProduct();
        mMapCategoryProducts.clear();
        mListProduct.clear();
        mListCategory.clear();
        mCategoryAdapter.clearData();
        getAllOrderCategoryFromServer();
    }

    @Override
    public void getAllProductLocal(List<OrderProduct> orderProducts) {
        mListOrderLocal.clear();
        if (!Utils.isNull(orderProducts)) {
            mListOrderLocal.addAll(orderProducts);
        }
    }

    @Override
    public void getAllProductLocalWhenOrder(List<OrderProduct> orderProducts) {
        mListOrderLocal.clear();
        if (!Utils.isNull(orderProducts)) {
            mListOrderLocal.addAll(orderProducts);
            syncNumberProductSelectedInCategory();
            syncOrderProduct();
            mCategoryAdapter.notifyDataSetChanged();
            mProductAdapter.notifyDataSetChanged();

            for (Map.Entry map : mMapCategoryProducts.entrySet()) {
                int key = (int) map.getKey();
                if (key != mCurrentCategoryId) {
                    mMapCategoryProducts.put(key, null);
                }
            }


        } else {
            clearAllCheckProduct();
        }

    }

    public void initOrderView(View view, ContainerFragment containerFragment) {
        this.containerFragment = containerFragment;
        containerFragment.getActivityComponent().inject(this);
        mPresenter.onAttach(this);
        // category
        mListOrderLocal = new ArrayList<>();
        departmentId = App.getInstance().getDepartmentId();
        recyclerViewCategory = view.findViewById(R.id.order_recycler_view_category);
        recyclerViewProduct = view.findViewById(R.id.order_recycler_view_product);
        mSwipeRefreshLayout = view.findViewById(R.id.swipe_container_profile);
//        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setRefreshing(false);
        mSwipeRefreshLayout.setEnabled(false);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);
        mMapCategoryProducts = new HashMap<>();
        mCategoryDatabase = CategoryDatabase.getInstance();
        mProductDatabase = ProductDatabase.getInstance();


        // category
        recyclerViewCategory.setHasFixedSize(true);
        GridLayoutManager gridLayoutManagerCategory = new GridLayoutManager(containerFragment.getActivity(), 1);
        recyclerViewCategory.addItemDecoration(new GridSpacingItemDecoration(1, 0, true));
        recyclerViewCategory.setLayoutManager(gridLayoutManagerCategory);
        mListCategory = new ArrayList<>();
        int itemSize = (int) (Utils.getWidthDevice(containerFragment.getActivity()) * 0.2);
        mCategoryAdapter = new CategoryOrderAdapter(mListCategory, containerFragment.getActivity(), itemSize, new CategoryOrderAdapter.ICategoryCallback() {
            @Override
            public void onItemClick(int categoryId) {
                mCurrentCategoryId = categoryId;
                getAllProductInCategory(false);
            }

            @Override
            public void onUpdateCategoryToOld(int categoryId) {
                Category category = mCategoryDatabase.getCategoryById(categoryId);
                if (category != null && category.isCategoryNew()) {
                    category.setCategoryNew(false);
                    mCategoryDatabase.onAction(ActionRoom.UPDATE, category);
                }
            }
        });
        recyclerViewCategory.setAdapter(mCategoryAdapter);


        // product
        recyclerViewProduct.setHasFixedSize(true);
        int imageSize = (int) (Utils.getWidthDevice(containerFragment.getActivity()) * 0.8) / 3 - 100;
        mProductAdapter = new ProductOrderAdapter(mListProduct, containerFragment.getActivity(), isShowGrid, imageSize, new ProductOrderAdapter.IProductOrderCallback() {
            @Override
            public void onUpdateItemToOld(int productId) {
                com.anphuocthai.staff.entities.Product product = mProductDatabase.getProducts(mCurrentCategoryId, productId);
                if (product != null && !product.getWhatNew().isEmpty()) {
                    boolean isUpdate = product.startCountTimeToOld();
                    if (isUpdate) {
                        mProductDatabase.onAction(ActionRoom.UPDATE, product);
                    }
                }
                Category category = mCategoryDatabase.getCategoryById(mCurrentCategoryId);
                if (category != null && category.getNumberItemNew() > 0) {
                    category.decrementNumberSelected();
                    mCategoryDatabase.onAction(ActionRoom.UPDATE, category);
                }
            }

            @Override
            public void onSaveOrderProduct(OrderProduct orderProduct) {
                mPresenter.onSaveOrderProduct(orderProduct);
                updateNumberSelected(true);

            }

            @Override
            public void onDeleteOrderProduct(OrderProduct orderProduct) {
                mPresenter.onDeleteOrderProduct(orderProduct);
                updateNumberSelected(false);
            }
        });
        recyclerViewProduct.setAdapter(mProductAdapter);
        mPresenter.getAllOrderProduct();
        setShowGrid(mPresenter.getIsShowGrid());

        getAllOrderCategoryFromServer();
    }

    public void setShowGrid(boolean showGrid) {
        isShowGrid = showGrid;
        changeViewType();
    }

    private void changeViewType() {
        recyclerViewProduct.removeItemDecoration(gridSpacingItemDecoration);
        if (isShowGrid) {
            GridLayoutManager gridLayoutManagerProduct = new GridLayoutManager(containerFragment.getActivity(), 3);
            recyclerViewProduct.addItemDecoration(gridSpacingItemDecoration);
            recyclerViewProduct.setLayoutManager(gridLayoutManagerProduct);
        } else {
            recyclerViewProduct.setLayoutManager(new LinearLayoutManager(this));
        }
        mProductAdapter.setShowGrid(isShowGrid);
        recyclerViewProduct.setAdapter(mProductAdapter);
    }

    public void clearAllCheckProduct() {
        print("#clearAllCheckProduct");
        for (ProductCategory category : mListCategory) {
            category.setNumberProductSelected(0);
        }
        mCategoryAdapter.notifyDataSetChanged();
        for (Product product : mListProduct) {
            product.setSelected(false);
        }
        for (Map.Entry<Integer, ArrayList<Product>> entry : mMapCategoryProducts.entrySet()) {
            ArrayList<Product> value = entry.getValue();
            if (!Utils.isNull(value)) {
                for (Product product : value) {
                    product.setSelected(false);
                }
            }
        }
        mProductAdapter.notifyDataSetChanged();
    }

    public void updateCheckProductWhenOrder() {
        print("#updateCheckProductWhenOrder");
        mPresenter.getAllProductLocalWhenOrder();
    }

    private void updateNumberSelected(boolean increment) {
        if (Utils.isNull(mListCategory)) return;
        for (ProductCategory category : mListCategory) {
            if (category.getId() == mCurrentCategoryId) {
                if (increment) {
                    category.incrementNumberProductSelected();
                } else {
                    category.decrementNumberProductSelected();
                }
                int position = mListCategory.indexOf(category);
                mCategoryAdapter.notifyItemChanged(position);
                break;
            }
        }

    }




    /*
     *
     * Get all category and sync local
     *
     * */

    private void getAllOrderCategoryFromServer() {
        mSwipeRefreshLayout.setRefreshing(true);
        NetworkManager.getInstance().sendGetRequestNotAuthen(ApiURL.GET_ALL_CATEGORY_COMMODITY_URL, null, false, new IAPICallback() {
            @Override
            public void onResponseSuccess(JSONObject response) {
            }

            @Override
            public void onResponseSuccess(JSONArray response) {
                mSwipeRefreshLayout.setRefreshing(false);
                if (response != null) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<ArrayList<ProductCategory>>() {
                    }.getType();
                    mListCategory.addAll(gson.fromJson(response.toString(), type));
                    mCategoryAdapter.notifyDataSetChanged();
                    if (mListCategory.size() > 0) {
                        ProductCategory firstItem = mListCategory.get(0);
                        firstItem.setSelected(true);
                        syncNumberProductSelectedInCategory();
                        checkChangeCategoryOrNewCategory();
                        mCurrentCategoryId = firstItem.getId();
                        getAllProductInCategory(false);
                    }
                }
            }

            @Override
            public void onResponseError(ANError anError) {
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

    }

    private void syncNumberProductSelectedInCategory() {
        if (mListOrderLocal.size() == 0) return;
        for (ProductCategory category : mListCategory) {
            category.setNumberProductSelected(0);
            for (OrderProduct orderProduct : mListOrderLocal) {
                if (orderProduct.getProductCategoryId() == category.getId()) {
                    category.incrementNumberProductSelected();
                }
            }
        }
    }

    private void checkChangeCategoryOrNewCategory() {
        List<Category> listCategory = mCategoryDatabase.getCategories();
        if (Utils.isNull(listCategory)) {
            for (ProductCategory productCategory : mListCategory) {
                mCategoryDatabase.onAction(ActionRoom.INSERT, new Category(productCategory));
            }
        } else {
            for (ProductCategory productCategory : mListCategory) {
                Category category = mCategoryDatabase.getCategoryById(productCategory.getId());
                if (category == null) {
                    productCategory.setNewCategory(true);
                    Category categoryNew = new Category(productCategory);
                    categoryNew.setCategoryNew(true);
                    mCategoryDatabase.onAction(ActionRoom.INSERT, categoryNew);
                } else {
                    boolean diff = category.checkDiff(productCategory);
                    if (diff) {
                        productCategory.setNewCategory(true);
                        mCategoryDatabase.onAction(ActionRoom.UPDATE, category);
                    } else {
                        productCategory.setNewCategory(category.isCategoryNew());
                        productCategory.setNumberItemNew(category.getNumberItemNew());
                    }
                }
            }
        }
    }



    /*
     *
     * Get product by category
     *
     * */

    private int departmentId = 0;

    public void changeIdDepartment(int departmentId) {
        this.departmentId = departmentId;
        if (mCurrentCategoryId != 0) {
            getAllProductInCategory(true);
        }
    }

    public void getAllProductInCategory(boolean isRefresh) {
        Log.d(TAG, "departmentId " + departmentId);
        ArrayList<Product> products = mMapCategoryProducts.get(mCurrentCategoryId);
        recyclerViewProduct.getRecycledViewPool().clear();
        if (products != null && !isRefresh) {
            mListProduct.clear();
            mListProduct.addAll(products);
            mProductAdapter.notifyDataSetChanged();
            recyclerViewProduct.post(() -> recyclerViewProduct.smoothScrollToPosition(0));
            return;
        }
        JSONObject productObject = new JSONObject();
        try {
            productObject.put("thongTin", "");
            productObject.put("nhomHangHoaId", mCurrentCategoryId);
            productObject.put("khoHangId", 0);
            productObject.put("isApp", "1");
            productObject.put("BoPhanID", departmentId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.d(TAG, "getAllProductInCategory " + productObject.toString() + " / departmentId " + departmentId);
        mSwipeRefreshLayout.setRefreshing(true);
        NetworkManager.getInstance().sendPostRequestNotAuthen(ApiURL.GET_ALL_COMMODITY_IN_CATEGORY_URL, productObject, false, new IAPICallback() {
            @Override
            public void onResponseSuccess(JSONObject response) {

            }

            @Override
            public void onResponseSuccess(JSONArray response) {
                mSwipeRefreshLayout.setRefreshing(false);
                mListProduct.clear();
                if (response != null) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<ArrayList<Product>>() {
                    }.getType();
                    ArrayList<Product> list = gson.fromJson(response.toString(), type);
                    if (!Utils.isNull(list)) {
                        mMapCategoryProducts.put(mCurrentCategoryId, list);
                        mListProduct.addAll(list);
                        syncOrderProduct();
                        checkChangeProductOrNewProduct();
                    }
                }
                mProductAdapter.notifyDataSetChanged();
                recyclerViewProduct.post(() -> recyclerViewProduct.smoothScrollToPosition(0));
            }

            @Override
            public void onResponseError(ANError anError) {
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private void syncOrderProduct() {
        if (mListOrderLocal.size() == 0) return;
        for (Product product : mListProduct) {
            product.setSelected(false);
            for (OrderProduct orderProduct : mListOrderLocal) {
                int orderId = Integer.parseInt(orderProduct.getProductId());
                if (orderId == product.getId()) {
                    product.setSelected(true);
                }

            }
        }

    }

    private void checkChangeProductOrNewProduct() {
        List<com.anphuocthai.staff.entities.Product> listDatabase = mProductDatabase.getProducts(mCurrentCategoryId);
        if (Utils.isNull(listDatabase)) {
            for (Product product : mListProduct) {
                com.anphuocthai.staff.entities.Product categoryProducts = new com.anphuocthai.staff.entities.Product(product.getProductCategoryId(),
                        product.getId(),
                        product.getName(),
                        product.getMainPrice().getKDBanBuonGia(),
                        product.getMainPrice().getKDCV());
                mProductDatabase.onAction(ActionRoom.INSERT, categoryProducts);
            }
        } else {
            int count = 0;
            long currentTime = System.currentTimeMillis();
            for (Product product : mListProduct) {
                com.anphuocthai.staff.entities.Product productLocal = mProductDatabase.getProducts(product.getProductCategoryId(), product.getId());
                if (productLocal == null) {
                    productLocal = new com.anphuocthai.staff.entities.Product(product.getProductCategoryId(),
                            product.getId(),
                            product.getName(),
                            product.getMainPrice().getKDBanBuonGia(),
                            product.getMainPrice().getKDCV());

                    String whatNew = productLocal.getTextChange(com.anphuocthai.staff.entities.Product.WhatChange.NEW);
                    productLocal.setWhatNew(whatNew);
                    mProductDatabase.onAction(ActionRoom.INSERT, productLocal);
                    product.setWhatNew(whatNew);
                    count++;
                } else {
//                    CheckChangeProduct checkChangeProduct = productLocal.checkDiff(currentTime,product);
//                    if (checkChangeProduct.isNew()) {
//                        productLocal.setNew(true);
//                        mProductDatabase.onAction(ActionRoom.UPDATE, productLocal);
//                    }
//                    product.setCheckChangeProduct(checkChangeProduct);
                    String whatNew = productLocal.checkChange(currentTime, product);
                    if (!whatNew.isEmpty()) {
                        productLocal.setWhatNew(whatNew);
                        product.setWhatNew(whatNew);
                        count++;
                    }
                    mProductDatabase.onAction(ActionRoom.UPDATE, productLocal);
                }
            }
            if (count > 0) {
                for (ProductCategory category : mListCategory) {
                    if (category.getId() == mCurrentCategoryId) {
                        category.setNumberItemNew(count);
                        Category categoryLocal = mCategoryDatabase.getCategoryById(mCurrentCategoryId);
                        categoryLocal.setNumberItemNew(count);
                        mCategoryDatabase.onAction(ActionRoom.UPDATE, categoryLocal);
                        break;
                    }
                }
                mCategoryAdapter.notifyDataSetChanged();
            }
        }
    }


}
