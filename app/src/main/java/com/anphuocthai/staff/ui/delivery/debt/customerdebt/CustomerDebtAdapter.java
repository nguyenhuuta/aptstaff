package com.anphuocthai.staff.ui.delivery.debt.customerdebt;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.entities.Report;
import com.anphuocthai.staff.utils.Enum;
import com.anphuocthai.staff.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class CustomerDebtAdapter extends RecyclerView.Adapter<CustomerDebtAdapter.ViewHolder> {

    public static final int HEADER = 0;
    public static final int BODY = 1;
    public static final int BOTTOM = 2;

    private List<Report> mListReport;
    private ICallback callback;

    public CustomerDebtAdapter(ArrayList<Report> orderTranArrayList) {
        mListReport = orderTranArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int type) {
        View view;
        switch (type) {
            case HEADER:
            default:
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.header_report_item, viewGroup, false);
                break;
            case BODY:
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.body_report_item, viewGroup, false);
                break;
            case BOTTOM:
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.bottom_report_item, viewGroup, false);
                break;
        }
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        viewHolder.bind(position);
    }

    @Override
    public int getItemCount() {
        if (mListReport.size() > 0) {
            return mListReport.size() + 1;
        }
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) return HEADER;
        else if (position == (getItemCount() - 1)) return BOTTOM;
        return BODY;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mNo, mCustomerName, mTotal, mPaid, mAmount;

        public ViewHolder(@NonNull View view) {
            super(view);
            mNo = view.findViewById(R.id.no);
            mCustomerName = view.findViewById(R.id.customerName);
            mTotal = view.findViewById(R.id.total);
            mPaid = view.findViewById(R.id.paid);
            mAmount = view.findViewById(R.id.amount);
        }

        public void bind(final int position) {
            if (position == 0) return;
            Report report = mListReport.get(position - 1);
            if (CustomerDebtAdapter.this.getItemViewType(position) == BODY) {
                mNo.setText(String.valueOf(position));
                mCustomerName.setText(report.getTenThanhVien());
            }
            mTotal.setText(Utils.formatValue(report.getTongGiaTri(), Enum.FieldValueType.NORMAL));
            mPaid.setText(Utils.formatValue(report.getTongThanhToan(), Enum.FieldValueType.NORMAL));
            mAmount.setText(Utils.formatValue(report.getPhaiThanhToan(), Enum.FieldValueType.NORMAL));
            itemView.setOnClickListener(view -> {
                if (callback != null && (position < getItemCount() - 1)) {
                    callback.onItemCLick(mListReport.get(position - 1));
                }
            });
        }
    }

    public void setOnItemClick(ICallback callback) {
        this.callback = callback;
    }

    public interface ICallback {
        void onItemCLick(Report report);
    }


}
