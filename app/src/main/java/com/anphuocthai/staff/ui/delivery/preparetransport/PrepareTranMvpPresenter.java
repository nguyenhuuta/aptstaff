package com.anphuocthai.staff.ui.delivery.preparetransport;

import com.anphuocthai.staff.ui.base.MvpPresenter;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;

import java.util.List;

public interface PrepareTranMvpPresenter<V extends PrepareTranMvpView> extends MvpPresenter<V> {
    void getAllPrepareTranOrders();
    void confirmTransport(List<OrderTran> orderTranListSelected);
    void setSingleSelectionBinder(SingleSelectionBinder singleSelectionBinder);

}
