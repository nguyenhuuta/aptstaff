package com.anphuocthai.staff.ui.home.statistics;

import com.androidnetworking.error.ANError;
import com.anphuocthai.staff.api.ApiURL;
import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.data.DataManager;
import com.anphuocthai.staff.interfaces.IAPICallback;
import com.anphuocthai.staff.ui.base.BasePresenter;
import com.anphuocthai.staff.ui.home.statistics.model.DynamicReport;
import com.anphuocthai.staff.ui.home.statistics.model.updatedynamic.PerformStatistic;
import com.anphuocthai.staff.ui.home.statistics.viewholder.DailyCVViewHolder;
import com.anphuocthai.staff.utils.rx.SchedulerProvider;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class StatisticPresenter<V extends StatisticMvpView> extends BasePresenter<V> implements StatisticMvpPresenter<V> {

    private static final String TAG = StatisticPresenter.class.getSimpleName();

    @Inject
    public StatisticPresenter(NetworkManager networkManager, DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(networkManager, dataManager, schedulerProvider, compositeDisposable);
    }


    @Override
    public void getStatisticPerform(DailyCVViewHolder viewHolder) {
        getmNetworkManager().sendGetRequestObjectResponse(ApiURL.STATISTIC_ORDER_BY_DAY_URL_DYNAMIC, null, new IAPICallback() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                if (response != null) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<PerformStatistic>() {
                    }.getType();
                    PerformStatistic performStatistic = gson.fromJson(response.toString(), type);
                    getmMvpView().onPerformStatisticSuccess(performStatistic, viewHolder);
                }
            }

            @Override
            public void onResponseSuccess(JSONArray response) {


            }

            @Override
            public void onResponseError(ANError anError) {

            }
        });
    }


    @Override
    public void getDynamicReport() {
        getmNetworkManager().sendGetRequest(ApiURL.STATISTIC_REPORT_SELL, null, new IAPICallback() {
            @Override
            public void onResponseSuccess(JSONObject response) {

            }

            @Override
            public void onResponseSuccess(JSONArray response) {
                if (response != null) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<ArrayList<DynamicReport>>() {
                    }.getType();
                    ArrayList<DynamicReport> dynamicReports = gson.fromJson(response.toString(), type);
                    getmMvpView().onGetDynamicReportSuccess(dynamicReports);
                }
            }

            @Override
            public void onResponseError(ANError anError) {

                getmMvpView().onGetDynamicReportError();
            }
        });
    }

    @Override
    public void openMyOrderShortcut() {
        getmMvpView().openMyOrderShortcut();
    }

    @Override
    public void openDebtManagement() {
        getmMvpView().openDebtManagement();
    }

    @Override
    public void openCustomerShortcut() {
        getmMvpView().openCustomerShortcut();
    }

    @Override
    public void openTransportShortcut() {
        getmMvpView().openTransportShortcut();
    }

    @Override
    public void openDailyWork() {
        getmMvpView().openDailyWork();
    }


}
