package com.anphuocthai.staff.ui.delivery.debtlocation;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.ui.base.BaseActivity;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import javax.inject.Inject;

public class DebtLocationActivity extends BaseActivity implements DebtLocationMvpView {


    private MapView mapView;

    LatLng position = new LatLng(37.77493, -122.41942);
    String markerText = "Người giao hàng";

    public static Intent getDebtLocationIntent(Context context) {
        Intent intent = new Intent(context, DebtLocationActivity.class);
        return intent;
    }

    @Inject
    DebtLocationMvpPresenter<DebtLocationMvpView> mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_debt_location);

        handleIntent();

        setupToolbar(getString(R.string.common_my_location) + " " + markerText);

        initView();

        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                Log.i("DEBUG", "onMapReady");
                Marker marker  = googleMap.addMarker(new MarkerOptions().
                        position(position).
                        title(markerText)
                        .visible(true));
                marker.showInfoWindow();


                //zoom to position with level 16
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(position, 16);
                googleMap.animateCamera(cameraUpdate);
            }
        });
    }

    private void handleIntent() {
        try {
            Bundle b = getIntent().getBundleExtra("LatLong");
            position = new LatLng(b.getDouble("latitude"), b.getDouble("longitude"));
            markerText = b.getString("debtname");
        }catch (Exception e) {
            showMessage(e.toString());
        }

    }

    private void initView() {
        mapView = findViewById(R.id.map_my_location);

    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }
}
