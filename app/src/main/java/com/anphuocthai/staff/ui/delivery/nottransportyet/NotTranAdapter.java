package com.anphuocthai.staff.ui.delivery.nottransportyet;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.interfaces.IYNDialogCallback;
import com.anphuocthai.staff.ui.base.BaseActivity;
import com.anphuocthai.staff.ui.base.BaseViewHolder;
import com.anphuocthai.staff.ui.delivery.model.Hanghoa;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;
import com.anphuocthai.staff.utils.Enum;
import com.anphuocthai.staff.utils.Utils;
import com.anphuocthai.staff.utils.ui.UIUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Janisharali on 25-05-2017.
 */

public class NotTranAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private static final String TAG = NotTranAdapter.class.getSimpleName();

    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private Callback mCallback;
    private List<OrderTran> mOrderTrans;

    NotTranMvpPresenter<NotTranMvpView> mPresenter;

    BaseActivity baseActivity;


    public void setmPresenter(NotTranMvpPresenter<NotTranMvpView> mPresenter) {
        this.mPresenter = mPresenter;
    }

    public void setBaseActivity(BaseActivity baseActivity) {
        this.baseActivity = baseActivity;
    }

    public NotTranAdapter(List<OrderTran> mOrderTrans) {
        this.mOrderTrans = mOrderTrans;
    }

    public void setCallback(Callback callback) {
        mCallback = callback;
    }



    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new ViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_not_tran, parent, false));
            case VIEW_TYPE_EMPTY:
            default:
                return new EmptyViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_view, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mOrderTrans != null && mOrderTrans.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }


    }

    @Override
    public int getItemCount() {
        if (mOrderTrans != null && mOrderTrans.size() > 0) {
            return mOrderTrans.size();
        } else {
            return 1;
        }

    }

    public void addItems(List<OrderTran> orderTrans) {
        mOrderTrans.clear();
        mOrderTrans.addAll(orderTrans);
        notifyDataSetChanged();
    }

    public void hideEmptyView(){

    }

    public interface Callback {
        void onBlogEmptyViewRetryClick();
    }

    public class ViewHolder extends BaseViewHolder {
          TextView txtOrderCode;
          TextView txtCustomerAddress;
          TextView txtCustomerName;
          TextView txtProductInfo;
          TextView btnSubmitTran;

        public ViewHolder(View itemView) {
            super(itemView);
            initView(itemView);
        }

        private void initView(View itemView){
            //itemView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
            txtOrderCode = itemView.findViewById(R.id.all_debt_txt_order_code);
            txtCustomerAddress = itemView.findViewById(R.id.all_debt_txt_order_customer_address);
            txtCustomerName = itemView.findViewById(R.id.all_debt_txt_order_customer_name);
            txtProductInfo = itemView.findViewById(R.id.order_tran_txt_product_info);
            btnSubmitTran = itemView.findViewById(R.id.all_debt_btn_submit);
            btnSubmitTran.setOnClickListener((View v)-> {
                UIUtils.showYesNoDialog(baseActivity, "", baseActivity.getString(R.string.order_tran_submit_tran_order), new IYNDialogCallback() {
                    @Override
                    public void accept() {
                        mPresenter.setTransport(mOrderTrans.get(getAdapterPosition()));
                    }

                    @Override
                    public void cancel() {

                    }
                });
            });
        }

        protected void clear() {
        }

        public void onBind(int position) {
            super.onBind(position);
            final OrderTran orderTran = mOrderTrans.get(position);
            if (orderTran.getMaDonDat() != null) {
                txtOrderCode.setText(orderTran.getMaDonDat());
            }
            if (orderTran.getTenThanhVien() != null) {
                txtCustomerName.setText(orderTran.getTenThanhVien());
            }

            txtCustomerAddress.setText("");
            if (orderTran.getVanchuyens() != null) {
                int size = orderTran.getVanchuyens().size();
                if (size > 0) {
                    String addressList = new String();
                    //for (int i = 0; i< size; i++) {
                    if (orderTran.getVanchuyens().get(0).getDiaChiNhanHang() != null){
                        //if (size == 1 || (i == size - 1)) {
                        addressList += String.format(orderTran.getVanchuyens().get(0).getDiaChiNhanHang());
                        // }else {
                        // addressList += String.format(orderTran.getVanchuyens().get(i).getDiaChiNhanHang() + "\n");
                        //}

                    }
                    // }
                    txtCustomerAddress.setText(addressList);
                }
                if (txtCustomerAddress.getText().toString().trim().isEmpty()) {
                    txtCustomerAddress.setVisibility(View.GONE);
                }
            }


            txtProductInfo.setText("");
            if (orderTran.getHanghoas() != null) {
                int size = orderTran.getHanghoas().size();
                if (size > 0) {
                    String goodsNames = new String();
                    for (int i = 0; i < size; i++){
                        if (orderTran.getHanghoas().get(i).getHanghoa().getTenHangHoa() != null) {
                            Hanghoa hanghoa = orderTran.getHanghoas().get(i);
                            if ((i == 0 && (size == 1)) || i == size - 1) {
                                goodsNames += hanghoa.getHanghoa().getTenHangHoa() + "(" + Utils.formatValue(hanghoa.getSoLuong(), Enum.FieldValueType.NORMAL) +  hanghoa.getDonVi() + ")";
                            }else {
                                goodsNames += hanghoa.getHanghoa().getTenHangHoa() + "(" + Utils.formatValue(hanghoa.getSoLuong(), Enum.FieldValueType.NORMAL) +  hanghoa.getDonVi() + ")"   +"\n";
                            }

                        }
                    }

                    txtProductInfo.setText(goodsNames);
                }
            }

            itemView.setOnClickListener((View v) -> {

            });
        }
    }

    public class EmptyViewHolder extends BaseViewHolder {

        @BindView(R.id.btn_retry)
        Button retryButton;

        @BindView(R.id.tv_message)
        TextView messageTextView;

        @BindView(R.id.txt_retry)
        TextView txtRetry;

        public EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            retryButton.setVisibility(View.GONE);
            txtRetry.setVisibility(View.GONE);
        }

        @Override
        protected void clear() {

        }

        @OnClick(R.id.btn_retry)
        void onRetryClick() {
            if (mCallback != null)
                mCallback.onBlogEmptyViewRetryClick();
        }
    }
}
