package com.anphuocthai.staff.ui.ordercart.sale;

import com.anphuocthai.staff.ui.base.DialogMvpView;

public interface SaleDialogMvpView extends DialogMvpView {

    void openPlayStoreForRating();

    void showPlayStoreRatingView();

    void showRatingMessageView();

    void hideSubmitButton();

    void disableRatingStars();

    void dismissDialog();
}
