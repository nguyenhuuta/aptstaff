package com.anphuocthai.staff.ui.delivery.debt.inventorydebt;

import com.anphuocthai.staff.api.ApiURL;
import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.baserequest.APIResponse;
import com.anphuocthai.staff.data.DataManager;
import com.anphuocthai.staff.model.AllCongNoResponse;
import com.anphuocthai.staff.model.CongNoParams;
import com.anphuocthai.staff.ui.base.BasePresenter;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;
import com.anphuocthai.staff.utils.App;
import com.anphuocthai.staff.utils.Logger;
import com.anphuocthai.staff.utils.Utils;
import com.anphuocthai.staff.utils.rx.SchedulerProvider;
import com.rx2androidnetworking.Rx2AndroidNetworking;

import org.json.JSONObject;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class InventoryDebtPresenter<V extends InventoryDebtMvpView> extends BasePresenter<V> implements InventoryDebtMvpPresenter<V> {

    @Inject
    public InventoryDebtPresenter(NetworkManager networkManager, DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(networkManager, dataManager, schedulerProvider, compositeDisposable);
    }

    private JSONObject createJSONObject(final int page) {
        JSONObject notTranObject = new JSONObject();
        try {
            notTranObject.put("phanLoai", 4);
            notTranObject.put("guidThanhVien", "");
            notTranObject.put("sessionID", "");
            notTranObject.put("tuNgay", "");
            notTranObject.put("denNgay", "");
            notTranObject.put("trangThaiIds", "");
            notTranObject.put("pageIndex", page);
            notTranObject.put("pageSize", 10);
            notTranObject.put("BoPhanID", App.getInstance().getDepartmentId());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return notTranObject;
    }

    @Override
    public void onSetStartCollectDebt(List<OrderTran> orderTrans) {

        if (Utils.isNull(orderTrans)) return;


        if (getmMvpView() != null) {
            getmMvpView().showLoading();
        }
        int size = orderTrans.size();
        StringBuilder ids = new StringBuilder();
        for (int position = 0; position < size; position++) {
            OrderTran orderTran = orderTrans.get(position);
            if (position == (size - 1)) {
                ids.append(orderTran.getDDDonDatID());
            } else {
                ids.append(orderTran.getDDDonDatID()).append(";");
            }
        }
        JSONObject setDebtObject = new JSONObject();
        try {
            String fullName = getDataManager().getUserInfo().getFullname();
            setDebtObject.put("UserID", getDataManager().getUserInfoId());
            setDebtObject.put("NguoiThucHien", fullName);
            setDebtObject.put("DD_DonDatIDs", ids);
            setDebtObject.put("BoPhanID", App.getInstance().getDepartmentId());
        } catch (Exception e) {
            e.printStackTrace();
        }
        Disposable disposable = Rx2AndroidNetworking.post(ApiURL.yeuCauMuonCongNo)
                .addHeaders(ApiURL.getBaseHeader())
                .addHeaders(ApiURL.getTokenHeader())
                .addJSONObjectBody(setDebtObject)
                .build()
                .getObjectObservable(APIResponse.class)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(responseMessage -> {
                    if (getmMvpView() == null) return;
                    if (getmMvpView() != null) {
                        getmMvpView().hideLoading();
                    }
                    if (responseMessage.isSuccess()) {
                        getmMvpView().yeuCauMuonThanhCong();
                    } else {
                        getmMvpView().showMessage(responseMessage.getMessage());
                    }
                }, throwable -> {
                    if (getmMvpView() != null) {
                        getmMvpView().hideLoading();
                        getmMvpView().showMessage(throwable.getMessage());
                    }
                });

        getCompositeDisposable().add(disposable);

    }


    @Override
    public void onViewPrepared(String keySearch, int page) {
        System.out.println("onViewPrepared page: " + page);
        if (getmMvpView() != null) {
            getmMvpView().showLoading();
        }
        CongNoParams congNoParams = new CongNoParams(getDataManager().getUserInfoId(), page, 0);
        congNoParams.setKeySearch(keySearch);
        Disposable disposable = getmNetworkManager().postRequest(ApiURL.allCongNo, congNoParams, AllCongNoResponse.class)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(congNoResponse -> {
                    if (getmMvpView() != null) {
                        getmMvpView().hideLoading();
                    }

                    if (congNoResponse != null && congNoResponse.getData() != null) {
                        getmMvpView().updateOrderTran(congNoResponse.getData());
                    }
                }, throwable -> {
                    if (getmMvpView() != null) {
                        getmMvpView().hideLoading();
                    }
                    Logger.d("InventoryDebtPresenter", throwable.getLocalizedMessage());
                    getmMvpView().onError(throwable.getLocalizedMessage());
                });
        getCompositeDisposable().add(disposable);
    }

    public static class InventoryParams {
        private int id;
        private String sessionID;
        private int SoTien;

        InventoryParams(int id, String sessionID, int soTien) {
            this.id = id;
            this.sessionID = sessionID;
            SoTien = soTien;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getSessionID() {
            return sessionID;
        }

        public void setSessionID(String sessionID) {
            this.sessionID = sessionID;
        }

        public int getSoTien() {
            return SoTien;
        }

        public void setSoTien(int soTien) {
            SoTien = soTien;
        }
    }
}
