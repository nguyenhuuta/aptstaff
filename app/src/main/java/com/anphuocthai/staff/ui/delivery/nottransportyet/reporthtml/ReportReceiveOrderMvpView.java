package com.anphuocthai.staff.ui.delivery.nottransportyet.reporthtml;

import com.anphuocthai.staff.ui.base.MvpView;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;

import java.util.List;

public interface ReportReceiveOrderMvpView extends MvpView {
    void updateOrderTran(List<OrderTran> orderTranArrayList);

    void onGetCustomerDebtSuccess(String report);

    void setRefresh(boolean isRefresh);
}
