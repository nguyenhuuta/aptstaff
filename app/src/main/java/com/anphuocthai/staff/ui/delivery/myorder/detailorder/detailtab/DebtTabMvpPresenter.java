package com.anphuocthai.staff.ui.delivery.myorder.detailorder.detailtab;

import com.anphuocthai.staff.ui.base.MvpPresenter;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;

import org.json.JSONObject;

import java.util.List;

import io.reactivex.Flowable;

public interface DebtTabMvpPresenter<V extends DebtTabMvpView> extends MvpPresenter<V> {
    void onViewPrepared();
    Flowable<List<OrderTran>> getAllDebtApiCall(JSONObject object);
    void onSetStartCollectDebt(OrderTran orderTran);


    void getCustomerDebt();
}
