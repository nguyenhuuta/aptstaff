package com.anphuocthai.staff.ui.searchsimpledata;

import android.os.Parcel;

import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;

public class SearchSimpleItem implements SearchSuggestion {

    private int id;

    private String name;

    private int tongCongNo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SearchSimpleItem() {
    }

    public SearchSimpleItem(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public void setTongCongNo(int tongCongNo) {
        this.tongCongNo = tongCongNo;
    }

    public int getTongCongNo() {
        return tongCongNo;
    }

    public static final Creator<SearchSimpleItem> CREATOR = new Creator<SearchSimpleItem>() {

        @Override
        public SearchSimpleItem createFromParcel(Parcel in) {
            return new SearchSimpleItem(in);
        }

        @Override
        public SearchSimpleItem[] newArray(int i) {
            return new SearchSimpleItem[i];
        }
    };

    public SearchSimpleItem(Parcel in) {
        id = in.readInt();
        name = in.readString();
        tongCongNo = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeInt(tongCongNo);
    }

    @Override
    public String getBody() {
        return name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

}
