package com.anphuocthai.staff.ui.home.notifications;

import com.anphuocthai.staff.ui.base.MvpPresenter;
import com.anphuocthai.staff.ui.home.notifications.model.Notification;

import java.util.List;

import io.reactivex.Flowable;

public interface NotificationMvpPresenter<V extends NotificationMvpView> extends MvpPresenter<V> {

//    void getDailyCVStatistic(DailyCVViewHolder viewHolder);
//
//    void getTopCustomerStatistic(TableViewHolder viewHolder);
//
//    void getTopCustomerStatistic(TopCustomerVHTable viewHolder);
//
//    void getDynamicReport();
//
//    void openMyOrderShortcut();
//
//    void openDebtManagement();
//
//    void openCustomerShortcut();
//
//    void openTransportShortcut();

    void onGetAllNotification();

    Flowable<List<Notification>> rxJavaOnGetAllNotification(int page);

    void getUnReadNotification();
}
