/*
 * Copyright 2017 Riyaz Ahamed
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.anphuocthai.staff.ui.delivery.preparetransport;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ahamed.multiviewadapter.BaseViewHolder;
import com.ahamed.multiviewadapter.ItemBinder;
import com.ahamed.multiviewadapter.util.ItemDecorator;
import com.anphuocthai.staff.R;
import com.anphuocthai.staff.ui.delivery.model.Hanghoa;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;
import com.anphuocthai.staff.utils.Enum;
import com.anphuocthai.staff.utils.Utils;

public class SingleSelectionBinder
    extends ItemBinder<OrderTran, SingleSelectionBinder.ViewHolder> {

  PrepareTranMvpPresenter<PrepareTranMvpView> mPresenter;

  public SingleSelectionBinder(ItemDecorator itemDecorator, PrepareTranMvpPresenter<PrepareTranMvpView> mPresenter) {
    super(itemDecorator);
    this.mPresenter = mPresenter;
  }

  @Override
  public ViewHolder create(LayoutInflater layoutInflater, ViewGroup parent) {
    return new ViewHolder(layoutInflater.inflate(R.layout.transport_prepare_item_layout, parent, false));
  }

  @Override
  public void bind(ViewHolder holder, OrderTran item) {
    holder.txtOrderCode.setText(item.getMaDonDat());
    holder.ivIndicator.setImageResource(
        holder.isItemSelected() ? R.drawable.drawable_selection_indicator
            : R.drawable.drawable_circle);
    holder.txtCustomerName.setText(item.getTenThanhVien());
    holder.txtProductName.setText("");
    if (item.getHanghoas() != null) {
      int size = item.getHanghoas().size();
      if (size > 0) {
        String goodsNames = new String();
        for (int i = 0; i < size; i++){
          if (item.getHanghoas().get(i).getHanghoa().getTenHangHoa() != null) {
            Hanghoa hanghoa = item.getHanghoas().get(i);
            if ((i == 0 && (size == 1)) || i == size - 1) {
              goodsNames += hanghoa.getHanghoa().getTenHangHoa() + "(" + Utils.formatValue(hanghoa.getSoLuong(), Enum.FieldValueType.NORMAL) +  hanghoa.getDonVi() + ")";
            }else {
              goodsNames += hanghoa.getHanghoa().getTenHangHoa() + "(" + Utils.formatValue(hanghoa.getSoLuong(), Enum.FieldValueType.NORMAL) +  hanghoa.getDonVi() + ")"   +"\n";
            }
          }
        }
        holder.txtProductName.setText(goodsNames);
      }
    }

  }

  @Override
  public boolean canBindData(Object item) {
    return item instanceof OrderTran;
  }

  @Override
  public int getSpanSize(int maxSpanCount) {
    return maxSpanCount;
  }

  static class ViewHolder extends BaseViewHolder<OrderTran> {

    private TextView txtOrderCode;
    private ImageView ivIndicator;
    private TextView txtCustomerName;
    private TextView txtProductName;

    ViewHolder(View itemView) {
      super(itemView);
      txtOrderCode = (TextView) itemView.findViewById(R.id.all_debt_txt_order_code);
      txtCustomerName = (TextView) itemView.findViewById(R.id.all_debt_txt_order_customer_name);
      txtProductName = (TextView) itemView.findViewById(R.id.order_prepare_txt_product_info);
      ivIndicator = (ImageView) itemView.findViewById(R.id.iv_selection_indicator);

      setItemClickListener(new OnItemClickListener<OrderTran>() {
        @Override
        public void onItemClick(View view, OrderTran item) {
          toggleItemSelection();
        }
      });


    }
  }
}
