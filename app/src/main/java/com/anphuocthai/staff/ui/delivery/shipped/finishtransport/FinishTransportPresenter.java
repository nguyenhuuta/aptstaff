package com.anphuocthai.staff.ui.delivery.shipped.finishtransport;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.data.DataManager;
import com.anphuocthai.staff.model.GiaoHangParam;
import com.anphuocthai.staff.ui.base.BasePresenter;
import com.anphuocthai.staff.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class FinishTransportPresenter<V extends FinishTransportMvpView> extends BasePresenter<V> implements FinishTransportMvpPresenter<V> {

    @Inject
    public FinishTransportPresenter(NetworkManager networkManager, DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(networkManager, dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void banGiaoChungTu(int orderId, float money, String ghiChu, int DD_TrangThaiID) {
        getmMvpView().showLoading();

        GiaoHangParam param = new GiaoHangParam(orderId, money, ghiChu, DD_TrangThaiID, getDataManager().getUserInfo().getId());
        Disposable disposable = getDataManager().banGiaoChungTu(param)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(apiResponse -> {
                    //trigger show notification from service, count times go to company
//                    getDataManager().triggerShowNotification().onNext(true);
                    getmMvpView().hideLoading();
                    getmMvpView().showMessage(R.string.shipped_return_success);
                    getmMvpView().dismissDialog();
                }, throwable -> {
                    getmMvpView().hideLoading();
                    getmMvpView().showMessage(throwable.getLocalizedMessage());
                });
        getCompositeDisposable().add(disposable);
    }
}
