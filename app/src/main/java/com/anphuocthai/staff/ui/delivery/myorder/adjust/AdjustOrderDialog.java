package com.anphuocthai.staff.ui.delivery.myorder.adjust;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.di.component.ActivityComponent;
import com.anphuocthai.staff.ui.base.BaseDialog;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;
import com.anphuocthai.staff.ui.delivery.myorder.MyOrderActivity;
import com.anphuocthai.staff.utils.Enum;
import com.anphuocthai.staff.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class AdjustOrderDialog extends BaseDialog implements AdjustOrderMvpView {

    private static final String TAG = AdjustOrderDialog.class.getSimpleName();

    private TextView txtName;

    private RecyclerView recyclerView;

    private OrderTran orderTran;

    private MyOrderActivity activity;

    @Inject
    AdjustOrderMvpPresenter<AdjustOrderMvpView> mPresenter;

    @Inject
    LinearLayoutManager linearLayoutManager;




    @Override
    protected void setUp(View view) {

    }

    public static AdjustOrderDialog newInstance() {
        AdjustOrderDialog fragment = new AdjustOrderDialog();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.adjust_order_dialog, container, false);

        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            mPresenter.onAttach(this);
            txtName = view.findViewById(R.id.txt_order_name);

            if (orderTran.getMaDonDat() != null) {
                if (orderTran.getNgayKetThuc() != null) {
                    txtName.setText(getString (R.string.order_tran_order_code)+ " " + orderTran.getMaDonDat() + " - " + Utils.formatDate(orderTran.getNgayKetThuc().toString(), false));
                }else {
                    txtName.setText(getString (R.string.order_tran_order_code)+ " " +orderTran.getMaDonDat() + " - " + Utils.formatDate(orderTran.getNgayBatDau().toString(), false));
                }
            }

            if (orderTran.getTrangThaiText() != null) {
                txtName.setText(txtName.getText() + " - " + orderTran.getTrangThaiText());
                if(orderTran.getDDTrangThaiID() == Enum.OrderState.NEW) {
                    txtName.setVisibility(View.VISIBLE);
                }
            }


            recyclerView = view.findViewById(R.id.recycler_view_adjust);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(linearLayoutManager);
            AdjustOrderAdapter adapter = new AdjustOrderAdapter(new ArrayList<>());
            adapter.setBaseActivity(activity);
            adapter.setOrderTran(orderTran);
            adapter.setmPresenter(mPresenter);
            List<String> list = new ArrayList<>();
            list.add(getString(R.string.edit_order));
            list.add(getString(R.string.cancel_order_menu));
            adapter.addItems(list);
            recyclerView.setAdapter(adapter);
            initEvents();
        }

        return view;
    }

    @Override
    public void hideKeyboard() {
        super.hideKeyboard();
        //edtCash.setText(Utils.formatValue(Double.parseDouble(edtCash.getText().toString()), Enum.FieldValueType.CURRENCY_NORMAL));
    }

    private void initEvents() {}

    public void show(FragmentManager fragmentManager, OrderTran orderTran, MyOrderActivity activity) {
        super.show(fragmentManager, TAG);
        this.orderTran = orderTran;
        this.activity = activity;
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void dismissDialog() {
        super.dismissDialog(TAG);
    }

//    @Override
//    public void dismissDialog() {
//        super.dismissDialog(TAG);
//    }
}
