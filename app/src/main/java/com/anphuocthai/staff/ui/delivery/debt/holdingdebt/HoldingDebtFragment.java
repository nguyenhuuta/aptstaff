package com.anphuocthai.staff.ui.delivery.debt.holdingdebt;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.di.component.ActivityComponent;
import com.anphuocthai.staff.model.CongNoModel;
import com.anphuocthai.staff.ui.base.BaseFragment;
import com.anphuocthai.staff.ui.delivery.debt.holdingdebt.receivemoney.ReceiveMoneyDialog;
import com.anphuocthai.staff.ui.delivery.debt.holdingdebt.receivemoney.ReceiveMoneyDialogCallback;
import com.anphuocthai.staff.ui.searchsimpledata.SearchSimpleActivity;
import com.anphuocthai.staff.ui.searchsimpledata.SearchSimpleItem;
import com.anphuocthai.staff.utils.Constants;
import com.anphuocthai.staff.utils.Enum;
import com.anphuocthai.staff.utils.Utils;
import com.anphuocthai.staff.widget.LoadMoreRecycleView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

public class HoldingDebtFragment extends BaseFragment implements HoldingDebtMvpView, SwipeRefreshLayout.OnRefreshListener {

    @Inject
    HoldingDebtMvpPresenter<HoldingDebtMvpView> mPresenter;

    @Inject
    HoldingDebtAdapter mAdapter;

    @Inject
    LinearLayoutManager mLayoutManager;

    private LoadMoreRecycleView recyclerView;

    private TextView txtSumaryMustCollect;

    private TextView txtSumaryOrder;

    private TextView txtSumaryAlreadyCollect;

    private View sumaryContainer;

    private SwipeRefreshLayout mSwipeRefreshLayout;

    private TextView buttonThanhToan;

    private List<CongNoModel> mOrderTrans = new ArrayList<>();
    private int currentPage = Constants.FIRST_PAGE;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.holding_debt_layout, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            mPresenter.onAttach(this);
            mAdapter.setCallback(this);
        }
        return view;
    }

    @Override
    protected void setUp(View view) {

        sumaryContainer = view.findViewById(R.id.layout_sumary);
        txtSumaryMustCollect = view.findViewById(R.id.txt_must_collect);
        txtSumaryMustCollect.setVisibility(View.VISIBLE);
        txtSumaryAlreadyCollect = view.findViewById(R.id.txt_already_collect);
        txtSumaryAlreadyCollect.setVisibility(View.VISIBLE);
        txtSumaryOrder = view.findViewById(R.id.txt_number_order);
        buttonThanhToan = view.findViewById(R.id.buttonThanhToan);

        recyclerView = view.findViewById(R.id.holding_debt_recycler_view);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        mAdapter.setContext(getBaseActivity());
        setupPullToRefresh(view);


        buttonThanhToan.setOnClickListener(view1 -> {
            ArrayList<CongNoModel> itemsChecked = mAdapter.getItemsChecked();
            int size = itemsChecked.size();
            if (size == 0) return;
            StringBuilder ids = new StringBuilder();
            for (int position = 0; position < size; position++) {
                CongNoModel congNoModel = itemsChecked.get(position);
                if (position == (size - 1)) {
                    ids.append(congNoModel.getQlCongNoID());
                } else {
                    ids.append(congNoModel.getQlCongNoID()).append(";");
                }
            }
            mPresenter.chuyenDaMuonSangChoTra(ids.toString());
        });

    }

    @Override
    public void daThuTienn(CongNoModel congNoModel) {
        ReceiveMoneyDialog.newInstance().show(getChildFragmentManager(), congNoModel, new ReceiveMoneyDialogCallback() {
            @Override
            public void thuTien(CongNoModel congNoModel, int soTienDaThu) {
                print("ReceiveMoneyDialog " + soTienDaThu);
                mPresenter.daThuTienn(congNoModel, soTienDaThu);
            }
        });
    }

    @Override
    public void datThuTienThanhCong() {
        showMessage("Thành công");
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onCheckBoxChange() {
        if (mAdapter == null) return;
        ArrayList<CongNoModel> itemsChecked = mAdapter.getItemsChecked();
        buttonThanhToan.setEnabled(itemsChecked.size() > 0);
    }

    @Override
    public void chuyenCongNoSangChoTraThanhCong() {
        mAdapter.removeItemChecked();
        int status = mAdapter.countItem() == 0 ? View.GONE : View.VISIBLE;
        sumaryContainer.setVisibility(status);
    }

    @Override
    public void traLaiHoaDon(CongNoModel congNoModel) {
        showDialogConfirm(getString(R.string.return_order), () -> {
            mPresenter.onReturnOrder(congNoModel);
        });
    }

    @Override
    public void traLaiHoaDonThanhCong(CongNoModel congNoModel) {
        mAdapter.removeItem(congNoModel);
        int status = mAdapter.countItem() == 0 ? View.GONE : View.VISIBLE;
        sumaryContainer.setVisibility(status);

    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void onError(String message) {
        showDialog(message, null);
        recyclerView.setLoading(false);
    }

    @Override
    public void updateOrderTran(List<CongNoModel> orderTrans) {
        recyclerView.setLoading(false);
        recyclerView.setVisibility(View.VISIBLE);
        if (currentPage == Constants.FIRST_PAGE) {
            mOrderTrans.clear();
            buttonThanhToan.setEnabled(false);
        }
        mOrderTrans.addAll(orderTrans);
        if (orderTrans.size() < Constants.LIMIT) {
            recyclerView.setEndOfPage(true);
        }
        updateOrderTranUI(mOrderTrans);
    }

    private void updateOrderTranUI(List<CongNoModel> orderTrans) {
        mAdapter.addItems(orderTrans);
        if (orderTrans.size() == 0) {
            sumaryContainer.setVisibility(View.GONE);
            return;
        }
        sumaryContainer.setVisibility(View.VISIBLE);
        int sumMustCollect = 0;
        int sumAlreadyCollect = 0;
        for (CongNoModel congNoModel : orderTrans) {
            sumMustCollect += congNoModel.getCongNoDonHang();
            sumAlreadyCollect += congNoModel.getTongTienDaThu();
        }
        txtSumaryMustCollect.setText(Utils.formatValue(sumMustCollect, Enum.FieldValueType.CURRENCY));
        txtSumaryAlreadyCollect.setText(Utils.formatValue(sumAlreadyCollect, Enum.FieldValueType.CURRENCY));
        String summaryOrder = orderTrans.size() + " " + getString(R.string.common_order);
        txtSumaryOrder.setText(summaryOrder);
    }

    @Override
    public void onRefresh() {
        currentPage = Constants.FIRST_PAGE;
        recyclerView.setEndOfPage(false);
        mSwipeRefreshLayout.setRefreshing(false);
        mPresenter.onViewPrepared(currentPage);
    }

    @Override
    public void reloadData() {
        if (mPresenter != null && mOrderTrans.isEmpty()) {
            mPresenter.onViewPrepared(currentPage);
        }
    }

    private void setupPullToRefresh(View view) {
        // SwipeRefreshLayout
        mSwipeRefreshLayout = view.findViewById(R.id.swipe_container_holding_debt);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.filter_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_filter) {
            showFilterScreen();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showFilterScreen() {
        if (mOrderTrans != null && !mOrderTrans.isEmpty()) {
            Set<Integer> searchEmployeeIds = new HashSet<>();
            ArrayList<SearchSimpleItem> searchSimpleItems = new ArrayList<>();
            for (CongNoModel orderTran : mOrderTrans) {
                boolean added = searchEmployeeIds.add(orderTran.getIDThanhVien());
                if (added) {
                    int id = orderTran.getIDThanhVien();
                    String name = orderTran.getTenThanhVien();
                    if (id == 0) {
                        id = SearchSimpleActivity.ID_NULL;
                    }
                    searchSimpleItems.add(new SearchSimpleItem(id, name));
                }
            }

            if (!searchSimpleItems.isEmpty()) {
                Intent intent = new Intent(getActivity(), SearchSimpleActivity.class);
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList(SearchSimpleActivity.NAME_PARCELABLE_ARRAYLIST, searchSimpleItems);
                intent.putExtras(bundle);
                startActivityForResult(intent, SearchSimpleActivity.REQUEST_CODE_SELECT_CUS_TAB3);
            } else {
                Toast.makeText(getActivity(), "Danh sách lọc bị trống!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getActivity(), "Danh sách lọc bị trống!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SearchSimpleActivity.REQUEST_CODE_SELECT_CUS_TAB3) {
            if (resultCode == Activity.RESULT_OK) {
                int id = data.getIntExtra(SearchSimpleActivity.KEY_ID, SearchSimpleActivity.ID_NULL);
                ArrayList<CongNoModel> searchOrderTran = new ArrayList<>();
                for (CongNoModel orderTran : mOrderTrans) {
                    if (SearchSimpleActivity.ID_NULL == id) {
                        if (orderTran.getIDThanhVien() == 0) {
                            searchOrderTran.add(orderTran);
                        }
                    } else if (orderTran.getIDThanhVien() == id) {
                        searchOrderTran.add(orderTran);
                    }
                }
                updateOrderTranUI(searchOrderTran);

            } else if (resultCode == Activity.RESULT_CANCELED) {
                updateOrderTranUI(mOrderTrans);
            }
        }
    }


}
