package com.anphuocthai.staff.ui.customer;

import android.util.Log;
import android.view.View;

import com.androidnetworking.error.ANError;
import com.anphuocthai.staff.api.ApiURL;
import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.data.DataManager;
import com.anphuocthai.staff.interfaces.IAPICallback;
import com.anphuocthai.staff.model.customer.SearchCustomer;
import com.anphuocthai.staff.ui.base.BasePresenter;
import com.anphuocthai.staff.utils.rx.SchedulerProvider;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.rx2androidnetworking.Rx2AndroidNetworking;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.anphuocthai.staff.utils.Constants.NUMBER_ROW_LOAD_MORE;

public class CustomerPresenter<V extends CustomerMvpView> extends BasePresenter<V> implements CustomerMvpPresenter<V> {

    private static final String TAG = CustomerPresenter.class.getSimpleName();

    @Inject
    public CustomerPresenter(NetworkManager networkManager, DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(networkManager, dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void getAllCustomer() {
        JSONObject object = new JSONObject();
        try {
            object.put("tenDayDu", "");
            object.put("doiTuongId", 0);
            object.put("pageNumber", 1);
            object.put("pageSize", 30);
        } catch (Exception e) {
            e.printStackTrace();
        }

        getmNetworkManager().sendPostRequestWithArrayResponse(ApiURL.GET_CUSTOMER_BY_CATEGORY_URL, object, new IAPICallback() {
            @Override
            public void onResponseSuccess(JSONObject response) {
            }

            @Override
            public void onResponseSuccess(JSONArray response) {
                if (response != null) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<ArrayList<SearchCustomer>>() {
                    }.getType();
                    ArrayList<SearchCustomer> result = gson.fromJson(response.toString(), type);
                    getmMvpView().updateCustomer(result);
                }
            }

            @Override
            public void onResponseError(ANError anError) {
                //getmMvpView().hideLoading();
            }
        });

    }

    @Override
    public void onQuickActionShow(View view) {
        getmMvpView().showQuickAction(view);
    }

    @Override
    public void onShowBottomSheet(SearchCustomer customer) {
        getmMvpView().showBottomSheet(customer);
    }

    @Override
    public void onCustomerCall(SearchCustomer customer) {
        getmMvpView().onCustomerCall(customer);
    }

    @Override
    public void onCustomerSMS(SearchCustomer customer) {
        getmMvpView().onCustomerSMS(customer);
    }

    @Override
    public void onCustomerDetail(SearchCustomer customer) {
        getmMvpView().onCustomerDetail(customer);
    }

    @Override
    public void loadAllContacts() {

    }

    @Override
    public void hideLoading() {
        getmMvpView().hideLoading();
    }

    @Override
    public void openAddNewWS(SearchCustomer customer) {
        getmMvpView().openAddNewWS(customer);
    }

    @Override
    public void openAddNewQWS(SearchCustomer customer) {
        getmMvpView().openAddNewQWS(customer);
    }

    @Override
    public void updateContacts(JSONArray jsonArray) {
        getmNetworkManager().sendPostRequestWithObject(ApiURL.SYNC_MULTI_CONTACTS, jsonArray, new IAPICallback() {
            @Override
            public void onResponseSuccess(JSONObject response) {

            }

            @Override
            public void onResponseSuccess(JSONArray response) {

            }

            @Override
            public void onResponseError(ANError anError) {

            }
        });
    }

    private JSONObject createJSONObject(final int page) {
        JSONObject object = new JSONObject();
        try {
            object.put("tenDayDu", "");
            object.put("doiTuongId", 0);
            object.put("pageNumber", page);
            object.put("pageSize", NUMBER_ROW_LOAD_MORE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return object;
    }

    @Override
    public Flowable<List<SearchCustomer>> rxJavaOnGetAllCustomer(int page) {
        return Rx2AndroidNetworking.post(ApiURL.GET_CUSTOMER_BY_CATEGORY_URL)
                .addHeaders(ApiURL.getBaseHeader())
                .addHeaders(ApiURL.getTokenHeader())
                .addJSONObjectBody(createJSONObject(page))
                .build()
                .getObjectListFlowable(SearchCustomer.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public void onSearchCustomer(String customerName) {

        if (customerName.isEmpty()) {
            return;
        }
        JSONObject customerObject = new JSONObject();
        try {
            customerObject.put("tenDayDu", customerName);
            customerObject.put("doiTuongId", "");
            customerObject.put("pageIndex", 1);
            customerObject.put("pageSize", 5);

        } catch (Exception e) {
            e.printStackTrace();
        }

        getmNetworkManager().sendPostRequestWithArrayResponse(ApiURL.GET_CUSTOMER_BY_CATEGORY_URL, customerObject, new IAPICallback() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                if (response != null) {

                }
            }

            @Override
            public void onResponseSuccess(JSONArray response) {
                if (response != null) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<List<SearchCustomer>>() {
                    }.getType();
                    List<SearchCustomer> result = gson.fromJson(response.toString(), type);
                    getmMvpView().onSearchCustomerComplele(result);
                }


            }

            @Override
            public void onResponseError(ANError anError) {
                Log.d(TAG, anError.toString());
            }
        });
    }
}
