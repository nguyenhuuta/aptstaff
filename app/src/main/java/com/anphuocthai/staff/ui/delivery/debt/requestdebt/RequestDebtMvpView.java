package com.anphuocthai.staff.ui.delivery.debt.requestdebt;

import com.anphuocthai.staff.model.CongNoModel;
import com.anphuocthai.staff.ui.base.MvpView;

import java.util.List;

public interface RequestDebtMvpView extends MvpView {
    void updateOrderTran(List<CongNoModel> orderTranArrayList);
}
