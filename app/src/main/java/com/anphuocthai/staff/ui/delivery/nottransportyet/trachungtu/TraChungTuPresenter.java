package com.anphuocthai.staff.ui.delivery.nottransportyet.trachungtu;

import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.data.DataManager;
import com.anphuocthai.staff.ui.base.BasePresenter;
import com.anphuocthai.staff.utils.rx.SchedulerProvider;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * Created by OpenYourEyes on 11/23/2020
 */
public class TraChungTuPresenter extends BasePresenter<ITraChungTuView> implements ITraChungTuPresenter {
    @Inject
    public TraChungTuPresenter(NetworkManager networkManager, DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(networkManager, dataManager, schedulerProvider, compositeDisposable);
    }

    @Override
    public void layDanhSachDonDat(int page, int size) {
        getmMvpView().showLoading();
        Disposable disposable = getDataManager().layDanhSachDonDat(getDataManager().getUserInfoId(), page, size)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(giaoHangResponse -> {
                    getmMvpView().hideLoading();
                    if (giaoHangResponse == null || giaoHangResponse.isSuccess() == null) return;
                    getmMvpView().getDanhSachDonDat(giaoHangResponse.getData());

                }, throwable -> {
                    getmMvpView().hideLoading();
                    getmMvpView().showMessage(throwable.getLocalizedMessage());
                });
        getCompositeDisposable().add(disposable);
    }
}
