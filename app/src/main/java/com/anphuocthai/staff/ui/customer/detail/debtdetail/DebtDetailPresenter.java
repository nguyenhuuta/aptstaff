package com.anphuocthai.staff.ui.customer.detail.debtdetail;

import com.androidnetworking.error.ANError;
import com.anphuocthai.staff.api.ApiURL;
import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.data.DataManager;
import com.anphuocthai.staff.interfaces.IAPICallback;
import com.anphuocthai.staff.ui.base.BasePresenter;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;
import com.anphuocthai.staff.utils.App;
import com.anphuocthai.staff.utils.rx.SchedulerProvider;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class DebtDetailPresenter<V extends DebtDetailMvpView> extends BasePresenter<V> implements DebtDetailMvpPresenter<V> {


    @Inject
    public DebtDetailPresenter(NetworkManager networkManager, DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(networkManager, dataManager, schedulerProvider, compositeDisposable);
    }

    private JSONObject createJSONObject(String guid) {
        JSONObject notTranObject = new JSONObject();
        try {
            notTranObject.put("phanLoai", 3);
            notTranObject.put("guidThanhVien", guid);
            notTranObject.put("sessionID", "");
            notTranObject.put("tuNgay", "");
            notTranObject.put("denNgay", "");
            notTranObject.put("trangThaiIds", "");
            notTranObject.put("pageIndex", 1);
            notTranObject.put("pageSize", 100);
            notTranObject.put("BoPhanID", App.getInstance().getDepartmentId());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return notTranObject;
    }

    @Override
    public void getHistoryTrans(String guid) {
        getmNetworkManager().sendPostRequestWithArrayResponse(ApiURL.GET_ORDER_URL, createJSONObject(guid), new IAPICallback() {
            @Override
            public void onResponseSuccess(JSONObject response) {
            }

            @Override
            public void onResponseSuccess(JSONArray response) {
                if (response != null) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<ArrayList<OrderTran>>() {
                    }.getType();
                    ArrayList<OrderTran> orderTrans = gson.fromJson(response.toString(), type);
                    getmMvpView().updateOrderTran(orderTrans);
                }
            }

            @Override
            public void onResponseError(ANError anError) {

            }
        });
    }
}
