package com.anphuocthai.staff.ui.customer.detail.config;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.interfaces.ItemClickListener;
import com.anphuocthai.staff.model.customer.Note;
import com.anphuocthai.staff.ui.base.BaseViewHolder;
import com.anphuocthai.staff.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ConfigCustomerAdapter extends RecyclerView.Adapter<BaseViewHolder>  {

    private List<Note> noteList;

    private ConfigCustomerMvpPresenter<ConfigCustomerMvpView> mPresenter;

    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private Callback mCallback;

    public void setmPresenter(ConfigCustomerMvpPresenter<ConfigCustomerMvpView> mPresenter) {
        this.mPresenter = mPresenter;
    }

    public void setCallback(Callback callback) {
        mCallback = callback;
    }

    public ConfigCustomerAdapter() {
    }
    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_EMPTY:
                return new EmptyViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_view, parent, false));
            case VIEW_TYPE_NORMAL:
                ItemViewHolder vh = new ItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_customer_note, parent, false));
                vh.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                        if (!isLongClick) {

                            //mPresenter.onQuickActionShow(view);


                        }
                    }
                });

                return vh;
            default:
                return new EmptyViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_view, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public int getItemViewType(int position) {
        if (noteList != null && noteList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public int getItemCount() {
        if (noteList != null && noteList.size() > 0) {
            return noteList.size();
        } else {
            return 1;
        }

    }

    public void addItems(List<Note> notes) {
        this.noteList.clear();
        this.noteList.addAll(notes);
        notifyDataSetChanged();
    }



    public class ItemViewHolder extends BaseViewHolder implements View.OnClickListener, View.OnLongClickListener {

        private TextView txtNote;
        private TextView txtDateChange;

        private ItemClickListener itemClickListener;

        public ItemViewHolder(View itemView) {
            super(itemView);
            txtNote = itemView.findViewById(R.id.txt_note);
            txtDateChange = itemView.findViewById(R.id.txt_date_change);

        }

        @Override
        protected void clear() {}

        @Override
        public void onBind(int position) {
            super.onBind(position);
            txtNote.setText("");
            if (noteList.get(position).getDate() != null) {
                txtDateChange.setText(Utils.formatDate(noteList.get(position).getDate(), true));
            }
            if (noteList.get(position).getContent() != null) {
                txtNote.setText(noteList.get(position).getContent());
            }

        }


        @Override
        public void onClick(View v) {
            itemClickListener.onClick(v, getAdapterPosition(),false);
        }
        @Override
        public boolean onLongClick(View v) {
            itemClickListener.onClick(v,getAdapterPosition(),true); // Gọi interface , true là vì đây là onLongClick
            return true;
        }
        public void setItemClickListener(ItemClickListener itemClickListener)
        {
            this.itemClickListener = itemClickListener;
        }
    }

    public interface Callback {
        void onBlogEmptyViewRetryClick();
    }


    public class EmptyViewHolder extends BaseViewHolder {

        @BindView(R.id.btn_retry)
        Button retryButton;

        @BindView(R.id.tv_message)
        TextView messageTextView;

        @BindView(R.id.txt_retry)
        TextView txtRetry;

        public EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            retryButton.setVisibility(View.GONE);
            txtRetry.setVisibility(View.GONE);
        }

        @Override
        protected void clear() {

        }

        @OnClick(R.id.btn_retry)
        void onRetryClick() {
            if (mCallback != null)
                mCallback.onBlogEmptyViewRetryClick();
        }
    }
}
