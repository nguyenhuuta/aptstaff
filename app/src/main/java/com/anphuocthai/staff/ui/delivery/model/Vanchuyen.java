
package com.anphuocthai.staff.ui.delivery.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Vanchuyen implements Serializable{

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("dD_DonDatID")
    @Expose
    private Integer dDDonDatID;
    @SerializedName("ma")
    @Expose
    private String ma;
    @SerializedName("nguoiChuyenID")
    @Expose
    private Integer nguoiChuyenID;
    @SerializedName("tenNguoiChuyen")
    @Expose
    private String tenNguoiChuyen;
    @SerializedName("phi")
    @Expose
    private Integer phi;
    @SerializedName("trangThaiID")
    @Expose
    private Integer trangThaiID;
    @SerializedName("xacNhan")
    @Expose
    private Boolean xacNhan;
    @SerializedName("ngayTao")
    @Expose
    private String ngayTao;
    @SerializedName("batDau")
    @Expose
    private String batDau;
    @SerializedName("ketThuc")
    @Expose
    private Object ketThuc;
    @SerializedName("diaChiNhanHang")
    @Expose
    private String diaChiNhanHang;
    @SerializedName("diaChiGiaoHang")
    @Expose
    private String diaChiGiaoHang;
    @SerializedName("tenNguoiNhan")
    @Expose
    private String tenNguoiNhan;
    @SerializedName("soDienThoai")
    @Expose
    private String soDienThoai;
    @SerializedName("ghiChu")
    @Expose
    private String ghiChu;
    @SerializedName("latitude")
    @Expose
    private Double latitude;
    @SerializedName("longitude")
    @Expose
    private Double longitude;

    @SerializedName("thoiGianConLai")
    @Expose
    private String timeRemain;




    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDDDonDatID() {
        return dDDonDatID;
    }

    public void setDDDonDatID(Integer dDDonDatID) {
        this.dDDonDatID = dDDonDatID;
    }

    public String getMa() {
        return ma;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public Integer getNguoiChuyenID() {
        return nguoiChuyenID;
    }

    public void setNguoiChuyenID(Integer nguoiChuyenID) {
        this.nguoiChuyenID = nguoiChuyenID;
    }

    public String getTenNguoiChuyen() {
        return tenNguoiChuyen;
    }

    public void setTenNguoiChuyen(String tenNguoiChuyen) {
        this.tenNguoiChuyen = tenNguoiChuyen;
    }

    public Integer getPhi() {
        return phi;
    }

    public void setPhi(Integer phi) {
        this.phi = phi;
    }

    public Integer getTrangThaiID() {
        return trangThaiID;
    }

    public void setTrangThaiID(Integer trangThaiID) {
        this.trangThaiID = trangThaiID;
    }

    public Boolean getXacNhan() {
        return xacNhan;
    }

    public void setXacNhan(Boolean xacNhan) {
        this.xacNhan = xacNhan;
    }

    public String getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(String ngayTao) {
        this.ngayTao = ngayTao;
    }

    public String getBatDau() {
        return batDau;
    }

    public void setBatDau(String batDau) {
        this.batDau = batDau;
    }

    public Object getKetThuc() {
        return ketThuc;
    }

    public void setKetThuc(Object ketThuc) {
        this.ketThuc = ketThuc;
    }

    public String getDiaChiNhanHang() {
        return diaChiNhanHang;
    }

    public void setDiaChiNhanHang(String diaChiNhanHang) {
        this.diaChiNhanHang = diaChiNhanHang;
    }

    public String getDiaChiGiaoHang() {
        return diaChiGiaoHang;
    }

    public void setDiaChiGiaoHang(String diaChiGiaoHang) {
        this.diaChiGiaoHang = diaChiGiaoHang;
    }

    public String getTenNguoiNhan() {
        return tenNguoiNhan;
    }

    public void setTenNguoiNhan(String tenNguoiNhan) {
        this.tenNguoiNhan = tenNguoiNhan;
    }

    public String getSoDienThoai() {
        return soDienThoai;
    }

    public void setSoDienThoai(String soDienThoai) {
        this.soDienThoai = soDienThoai;
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getTimeRemain() {
        return timeRemain;
    }

    public void setTimeRemain(String timeRemain) {
        this.timeRemain = timeRemain;
    }

}
