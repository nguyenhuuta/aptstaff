package com.anphuocthai.staff.ui.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.data.db.network.model.request.LoginRequest;
import com.anphuocthai.staff.ui.base.BaseActivity;
import com.anphuocthai.staff.ui.home.MainActivity;

import javax.inject.Inject;

/**
 * Login screen
 */
public class LoginActivity extends BaseActivity implements LoginMvpView, View.OnClickListener {

    private static final String TAG = LoginActivity.class.getSimpleName();

    private EditText emailText;

    private EditText passwordText;
    private TextView mButtonForgetPassword;
    private TextView loginButton, loginFacebook, loginZalo;

    private CheckBox autoLoginCb;

    @Inject
    LoginMvpPresenter<LoginMvpView> mvpPresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getmActivityComponent().inject(this);
        mvpPresenter.onAttach(this);
        addControls();
        addEvents();
    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void addControls() {
        emailText = findViewById(R.id.input_email);
        passwordText = findViewById(R.id.input_password);
        mButtonForgetPassword = findViewById(R.id.buttonForgetPassword);
        loginButton = findViewById(R.id.btn_login);
        loginFacebook = findViewById(R.id.buttonFacebook);
        loginZalo = findViewById(R.id.buttonZalo);
    }

    private void addEvents() {
        loginButton.setOnClickListener(this);
        loginZalo.setOnClickListener(this);
        loginFacebook.setOnClickListener(this);
    }

    public void login(String username, String password) {
        if (!validate()) {
            onLoginFailed(getString(R.string.loginfailure));
            return;
        }
        loginButton.setEnabled(false);
        mvpPresenter.onLogin(new LoginRequest(username, password, "ADMIN"));

    }


    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    @Override
    public void onLoginSuccess() {
        loginButton.setEnabled(true);
        mvpPresenter.onAutoLoginChange(true);
        Intent intent = new Intent(getBaseContext(), MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onLoginFailed(String message) {
        Toast.makeText(getBaseContext(), getString(R.string.have_error) + "\n" + message, Toast.LENGTH_LONG).show();
        loginButton.setEnabled(true);
        hideLoading();
    }

    public boolean validate() {
        boolean valid = true;
        String email = emailText.getText().toString();
        String password = passwordText.getText().toString();
        if (email.isEmpty()) {
            emailText.setError(getString(R.string.empty_login_name));
            valid = false;
        } else {
            emailText.setError(null);
        }
        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            passwordText.setError(getString(R.string.limit_pass_character));
            valid = false;
        } else {
            passwordText.setError(null);
        }
        return valid;
    }

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, LoginActivity.class);
        return intent;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_login:
                String username = emailText.getText().toString();
                String password = passwordText.getText().toString();
                login(username, password);
                break;
            case R.id.buttonFacebook:
            case R.id.buttonZalo:
                showDialog("Tính năng đang được phát triển", null);
                break;
        }
    }
}
