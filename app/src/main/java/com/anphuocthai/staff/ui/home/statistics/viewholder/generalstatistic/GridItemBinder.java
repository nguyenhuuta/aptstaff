/*
 * Copyright 2017 Riyaz Ahamed
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.anphuocthai.staff.ui.home.statistics.viewholder.generalstatistic;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.ItemTouchHelper;

import com.ahamed.multiviewadapter.BaseViewHolder;
import com.ahamed.multiviewadapter.ItemBinder;
import com.anphuocthai.staff.R;
import com.anphuocthai.staff.activities.tong_hop_phat.TongHopPhatActivity;

import java.util.ArrayList;


public class GridItemBinder extends ItemBinder<GridItem, GridItemBinder.ItemViewHolder> {
    private ArrayList<APTIndex> statistics;

    public GridItemBinder(int insetInPixels, ArrayList<APTIndex> statistics) {
        super(new GridInsetDecoration(insetInPixels));
        this.statistics = statistics;
    }

    APTIndex getItem(int position) {
        return statistics.get(position);
    }

    @Override
    public ItemViewHolder create(LayoutInflater layoutInflater, ViewGroup parent) {
        return new ItemViewHolder(layoutInflater.inflate(R.layout.item_grid, parent, false));
    }

    @Override
    public void bind(ItemViewHolder holder, GridItem item) {
        View itemView = holder.itemView;
        itemView.getBackground().setColorFilter(item.getColor(), PorterDuff.Mode.MULTIPLY);
        APTIndex model = getItem(item.getPosition());
        holder.title.setText(model.getTitle());
        holder.txtValue.setText(model.getBody());
        itemView.setOnClickListener(v -> {
            if (!model.isTongPhat()) return;
            Context context = v.getContext();
            Intent intent = new Intent(context, TongHopPhatActivity.class);
            context.startActivity(intent);
        });
    }

    @Override
    public boolean canBindData(Object item) {
        return item instanceof GridItem;
    }

    static class ItemViewHolder extends BaseViewHolder<GridItem> {

        private TextView title;
        private TextView txtValue;

        ItemViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.txt_general_statistic_title);
            txtValue = (TextView) itemView.findViewById(R.id.txt_value_statistic_general);
        }

        @Override
        public int getDragDirections() {
            return ItemTouchHelper.LEFT
                    | ItemTouchHelper.UP
                    | ItemTouchHelper.RIGHT
                    | ItemTouchHelper.DOWN;
        }
    }
}