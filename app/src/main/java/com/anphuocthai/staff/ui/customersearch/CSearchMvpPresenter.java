package com.anphuocthai.staff.ui.customersearch;

import com.anphuocthai.staff.model.customer.SearchCustomer;
import com.anphuocthai.staff.ui.base.MvpPresenter;

import java.util.List;

import io.reactivex.Flowable;

public interface CSearchMvpPresenter<V extends CSearchMvpView> extends MvpPresenter<V> {
    void onGetRecentCustomer();

    void onCustomerSelected(SearchCustomer customer);

    Flowable<List<SearchCustomer>> rxJavaOnGetAllCustomer(final int page);
}
