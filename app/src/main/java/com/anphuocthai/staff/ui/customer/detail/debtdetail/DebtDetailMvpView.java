package com.anphuocthai.staff.ui.customer.detail.debtdetail;

import com.anphuocthai.staff.ui.base.MvpView;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;

import java.util.List;

public interface DebtDetailMvpView extends MvpView {
    void updateOrderTran(List<OrderTran> orderTranArrayList);
}
