package com.anphuocthai.staff.ui.login;

import com.anphuocthai.staff.data.db.local.prefs.PreferencesHelper;
import com.anphuocthai.staff.data.db.network.ApiHelper;
import com.anphuocthai.staff.data.db.network.model.request.LoginRequest;
import com.anphuocthai.staff.data.db.network.model.response.LoginResponse;
import com.anphuocthai.staff.ui.base.BaseInteractor;

import javax.inject.Inject;

import io.reactivex.Observable;

public class LoginInteractor extends BaseInteractor implements LoginMvpInteractor{


    @Inject
    public LoginInteractor(PreferencesHelper preferencesHelper, ApiHelper apiHelper) {
        super(preferencesHelper, apiHelper);
    }

    @Override
    public Observable<LoginResponse> doServerLoginApiCall(LoginRequest request) {
        return null;
    }
}
