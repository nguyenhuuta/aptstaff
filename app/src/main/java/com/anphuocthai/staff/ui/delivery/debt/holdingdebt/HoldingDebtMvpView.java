package com.anphuocthai.staff.ui.delivery.debt.holdingdebt;

import com.anphuocthai.staff.model.CongNoModel;
import com.anphuocthai.staff.ui.base.MvpView;

import java.util.List;

public interface HoldingDebtMvpView extends MvpView {
    void updateOrderTran(List<CongNoModel> orderTranArrayList);

    void traLaiHoaDon(CongNoModel congNoModel);

    void traLaiHoaDonThanhCong(CongNoModel congNoModel);

    void daThuTienn(CongNoModel congNoModel);

    void datThuTienThanhCong();

    void chuyenCongNoSangChoTraThanhCong();

    void onRefresh();

    void onCheckBoxChange();
}
