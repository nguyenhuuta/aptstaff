package com.anphuocthai.staff.ui.home.container;

import com.anphuocthai.staff.di.PerActivity;
import com.anphuocthai.staff.ui.base.MvpPresenter;

@PerActivity
public interface ContainerMvpPresenter<V extends ContainerMvpView> extends MvpPresenter<V> {

}
