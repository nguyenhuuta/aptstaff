
package com.anphuocthai.staff.ui.home.statistics.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.anphuocthai.staff.model.SeeMore;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Item implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("guid")
    @Expose
    private String guid;
    @SerializedName("fullname")
    @Expose
    private String fullname;
    @SerializedName("tenPhongBan")
    @Expose
    private String tenPhongBan;
    @SerializedName("tenChucVu")
    @Expose
    private String tenChucVu;
    @SerializedName("tongGiaTri")
    @Expose
    private Long tongGiaTri;
    @SerializedName("tongCV")
    @Expose
    private Integer tongCV;
    @SerializedName("tongThanhToan")
    @Expose
    private Long tongThanhToan;

    public SeeMore seeMore = null;

    public Item() {
    }

    protected Item(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        guid = in.readString();
        fullname = in.readString();
        tenPhongBan = in.readString();
        tenChucVu = in.readString();
        if (in.readByte() == 0) {
            tongGiaTri = null;
        } else {
            tongGiaTri = in.readLong();
        }
        if (in.readByte() == 0) {
            tongCV = null;
        } else {
            tongCV = in.readInt();
        }
        if (in.readByte() == 0) {
            tongThanhToan = null;
        } else {
            tongThanhToan = in.readLong();
        }
    }

    public static final Creator<Item> CREATOR = new Creator<Item>() {
        @Override
        public Item createFromParcel(Parcel in) {
            return new Item(in);
        }

        @Override
        public Item[] newArray(int size) {
            return new Item[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getTenPhongBan() {
        return tenPhongBan;
    }

    public void setTenPhongBan(String tenPhongBan) {
        this.tenPhongBan = tenPhongBan;
    }

    public String getTenChucVu() {
        return tenChucVu;
    }

    public void setTenChucVu(String tenChucVu) {
        this.tenChucVu = tenChucVu;
    }

    public Long getTongGiaTri() {
        return tongGiaTri;
    }

    public void setTongGiaTri(Long tongGiaTri) {
        this.tongGiaTri = tongGiaTri;
    }

    public Integer getTongCV() {
        return tongCV;
    }

    public void setTongCV(Integer tongCV) {
        this.tongCV = tongCV;
    }

    public Long getTongThanhToan() {
        return tongThanhToan;
    }

    public void setTongThanhToan(Long tongThanhToan) {
        this.tongThanhToan = tongThanhToan;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        dest.writeString(guid);
        dest.writeString(fullname);
        dest.writeString(tenPhongBan);
        dest.writeString(tenChucVu);
        if (tongGiaTri == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(tongGiaTri);
        }
        if (tongCV == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(tongCV);
        }
        if (tongThanhToan == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(tongThanhToan);
        }
    }
}
