package com.anphuocthai.staff.ui.base;

import com.androidnetworking.error.ANError;
import com.anphuocthai.staff.fragments.dialogmessage.DialogMessage;

public interface MvpPresenter<V extends MvpView> {
    void onAttach(V mvpView);

    void onDetach();

    void handleApiError(ANError error);

    String getToken();

    void showDialog(String message, DialogMessage.IOnClickListener callback);

    void showDialogConfirm(String message, DialogMessage.IOnClickListener callback);
}
