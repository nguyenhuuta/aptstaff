package com.anphuocthai.staff.ui.delivery.debt.inventorydebt;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.di.component.ActivityComponent;
import com.anphuocthai.staff.ui.base.BaseFragment;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;
import com.anphuocthai.staff.utils.Constants;
import com.anphuocthai.staff.utils.Enum;
import com.anphuocthai.staff.utils.Utils;
import com.anphuocthai.staff.widget.LoadMoreRecycleView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.PublishSubject;

public class InventoryDebtFragment extends BaseFragment implements InventoryDebtMvpView, InventoryDebtAdapter.Callback, SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = InventoryDebtFragment.class.getSimpleName();
    @Inject
    InventoryDebtMvpPresenter<InventoryDebtMvpView> mPresenter;

    @Inject
    InventoryDebtAdapter mAdapter;

    @Inject
    LinearLayoutManager mLayoutManager;

    private LoadMoreRecycleView recyclerView;

    private TextView txtSumaryOrder;

    private TextView txtSumaryMustCollect;

    private TextView txtSumaryAlreadyCollect;

    private View sumaryContainer;

    private SwipeRefreshLayout mSwipeRefreshLayout;

    private List<OrderTran> mOrderTrans;
    private TextView mButtonCongNo;
    private int currentPage = Constants.FIRST_PAGE;
    String keySearch;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.inventory_debt_layout, container, false);
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);
            mPresenter.onAttach(this);
            mAdapter.setCallback(this);
            mAdapter.setContext(getActivity());
        }
        return view;
    }

    @Override
    protected void setUp(View view) {
        sumaryContainer = view.findViewById(R.id.layout_sumary);
        txtSumaryOrder = view.findViewById(R.id.txt_number_order);
        txtSumaryMustCollect = view.findViewById(R.id.txt_must_collect);
        txtSumaryAlreadyCollect = view.findViewById(R.id.txt_already_collect);
        mButtonCongNo = view.findViewById(R.id.buttonCongNo);
        recyclerView = view.findViewById(R.id.inventory_debt_recycler_view);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(mAdapter);
        mSwipeRefreshLayout = view.findViewById(R.id.swipe_container_inventory_debt);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        recyclerView.addLoadMore(() -> {
            currentPage++;
            mPresenter.onViewPrepared(keySearch, currentPage);
            return null;
        });

        mButtonCongNo.setOnClickListener(v -> showDialog(getString(R.string.confirm_start_collect_debt), () -> {
            List<OrderTran> list = getItemSelected();
            if (list.size() > 0) {
                mPresenter.onSetStartCollectDebt(list);
            }
        }));
        if (mOrderTrans == null) {
            mOrderTrans = new ArrayList<>();
            sumaryContainer.setVisibility(View.GONE);
        } else {
            updateOrderTranUI(mOrderTrans);
        }
        mAdapterAutoComplete = new ArrayAdapter<>(getBaseActivity(), android.R.layout.simple_dropdown_item_1line, new ArrayList<>());
    }

    @Override
    public void yeuCauMuonThanhCong() {
        showMessage(R.string.shipped_start_tran_success);
        mAdapter.removeItemsChecked();

        int status = mAdapter.countItem() == 0 ? View.GONE : View.VISIBLE;
        sumaryContainer.setVisibility(status);
    }

    @Override
    public void onDestroyView() {
        mPresenter.onDetach();
        super.onDestroyView();
    }

    @Override
    public void onError(String message) {
        showDialog(message, null);
        recyclerView.setLoading(false);
    }

    @Override
    public void updateOrderTran(List<OrderTran> orderTrans) {
        recyclerView.setLoading(false);
        recyclerView.setVisibility(View.VISIBLE);
        if (currentPage == Constants.FIRST_PAGE) {
            mOrderTrans.clear();
            mAdapterAutoComplete.clear();
        }
        mOrderTrans.addAll(orderTrans);
        recyclerView.setEndOfPage(orderTrans.size() < Constants.LIMIT);
        if (menuCancel != null && menuCancel.isVisible()) {
            menuCancel.setVisible(false);
        }
        if (checkBox != null && checkBox.isChecked()) {
            checkBox.setOnCheckedChangeListener(null);
            checkBox.setChecked(false);
            checkBox.setOnCheckedChangeListener(onCheckAll);
        }


        Set<Integer> searchEmployeeIds = new HashSet<>();
        for (OrderTran orderTran : orderTrans) {
            boolean isAdd = searchEmployeeIds.add(orderTran.getIDThanhVien());
            if (isAdd) {
                mAdapterAutoComplete.add(orderTran.getTenThanhVien());
            }
        }
        mAdapterAutoComplete.notifyDataSetChanged();
        updateOrderTranUI(mOrderTrans);
    }

    private void updateOrderTranUI(List<OrderTran> orderTrans) {
        mAdapter.addItems(orderTrans);
        if (orderTrans.size() <= 0) {
            sumaryContainer.setVisibility(View.GONE);
            return;
        } else {
            sumaryContainer.setVisibility(View.VISIBLE);
        }
        showTotal(orderTrans);
    }

    private void showTotal(List<OrderTran> orderTrans) {
        int sumMustCollect = 0;
        int sumAlreadyCollect = 0;
        for (OrderTran orderTran : orderTrans) {
            sumMustCollect += orderTran.getPhaiThanhToan();
            sumAlreadyCollect += orderTran.getTongThanhToan();
        }
        txtSumaryOrder.setText(orderTrans.size() + " " + getString(R.string.common_order));
        txtSumaryMustCollect.setText(Utils.formatValue(sumMustCollect, Enum.FieldValueType.CURRENCY));
        txtSumaryAlreadyCollect.setText(Utils.formatValue(sumAlreadyCollect, Enum.FieldValueType.CURRENCY));
    }


    @Override
    public void onRefresh() {
        currentPage = Constants.FIRST_PAGE;
        recyclerView.setEndOfPage(false);
        mSwipeRefreshLayout.setRefreshing(false);
        mPresenter.onViewPrepared(keySearch, currentPage);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.inventory_menu, menu);
        initMenu(menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuSearch:
                return false;
            case R.id.menuCancel:
                clearSelectFromMenu();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void clearSelectFromMenu() {
        if (checkBox != null && checkBox.isChecked()) {
            checkBox.setOnCheckedChangeListener(null);
            checkBox.setChecked(false);
            checkBox.setOnCheckedChangeListener(onCheckAll);
        }
        if (menuCancel != null) {
            menuCancel.setVisible(false);
        }
        mAdapter.unSelectedAll();
        mButtonCongNo.setVisibility(View.GONE);
        calculatorPrice();
    }

    @Override
    public void reloadData() {
        if (mPresenter != null && mOrderTrans.isEmpty()) {
            mPresenter.onViewPrepared(keySearch, currentPage);
        }
    }

    private SearchView searchView = null;
    private ArrayAdapter<String> mAdapterAutoComplete;

    private MenuItem menuCancel;
    private CheckBox checkBox;

    private void initMenu(Menu menu) {
        MenuItem searchItem = menu.findItem(R.id.menuSearch);
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        PublishSubject<String> publishSubject = PublishSubject.create();
        Disposable a = publishSubject
                .debounce(300, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::searchList);
        compositeDisposable.add(a);
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
//            searchItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
//                @Override
//                public boolean onMenuItemActionExpand(MenuItem menuItem) {
//                    recyclerView.setPauseLoadMore(true);
//                    return true;
//                }
//
//                @Override
//                public boolean onMenuItemActionCollapse(MenuItem menuItem) {
//                    recyclerView.setPauseLoadMore(false);
//                    return true;
//                }
//            });
        }
        if (searchView != null) {
            EditText editext = searchView.findViewById(com.google.android.material.R.id.search_src_text);
            editext.setHint("Tìm kiếm");
            editext.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (i != 0 || i1 != 0 || i2 != 0) {
                        publishSubject.onNext(editext.getText().toString());
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {
                }
            });

        }
        menuCancel = menu.findItem(R.id.menuCancel);
        menuCancel.setVisible(false);


        MenuItem menuItem = menu.findItem(R.id.menuCheckBox);
        checkBox = (CheckBox) MenuItemCompat.getActionView(menuItem);
        checkBox.setOnCheckedChangeListener(onCheckAll);
    }

    CompoundButton.OnCheckedChangeListener onCheckAll = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            menuCancel.setVisible(isChecked);
            if (isChecked) {
                mAdapter.selectedAll();
                mButtonCongNo.setVisibility(View.VISIBLE);
            } else {
                mAdapter.unSelectedAll();
                mButtonCongNo.setVisibility(View.GONE);
            }
            calculatorPrice();
        }
    };

    private void searchList(String keyword) {
        keySearch = keyword;
        currentPage = 1;
        print("searchList " + keyword);
        mPresenter.onViewPrepared(keySearch, currentPage);
//        if (Utils.isNull(keyword)) {
//            updateOrderTranUI(mOrderTrans);
//        } else {
//            mPresenter.onViewPrepared(keySearch, currentPage);
//            ArrayList<OrderTran> searchOrderTran = new ArrayList<>();
//            for (OrderTran orderTran : mOrderTrans) {
//                String tenKhachHang = orderTran.getTenThanhVien();
//                String diachi = orderTran.getDiaChiVanChuyen();
//                boolean isMatchName = tenKhachHang.toLowerCase().contains(keyword);
//                boolean isMatchAddress = diachi.toLowerCase().contains(keyword);
//                print("isMatchName " + keyword + "|" + isMatchName + "|" + isMatchAddress);
//                if (isMatchName || isMatchAddress) {
//                    searchOrderTran.add(orderTran);
//                }
//            }
//            updateOrderTranUI(searchOrderTran);
//        }
    }


    @Override
    public void onItemSelected() {
        List<OrderTran> orderTrans = getItemSelected();
        int number = orderTrans.size();
        if (number == mAdapter.getItemCount()) {
            checkBox.setOnCheckedChangeListener(null);
            checkBox.setChecked(true);
            checkBox.setOnCheckedChangeListener(onCheckAll);
        } else {
            if (checkBox.isChecked()) {
                checkBox.setOnCheckedChangeListener(null);
                checkBox.setChecked(false);
                checkBox.setOnCheckedChangeListener(onCheckAll);
            }
        }
        if (number > 0) {
            menuCancel.setVisible(true);
            mButtonCongNo.setVisibility(View.VISIBLE);
        } else {
            menuCancel.setVisible(false);
            mButtonCongNo.setVisibility(View.GONE);
        }
        calculatorPrice();
    }


    private void calculatorPrice() {

        List<OrderTran> orderTrans = getItemSelected();
        if (orderTrans.size() == 0) {
            orderTrans = mOrderTrans;
        }
        showTotal(orderTrans);
    }

    public List<OrderTran> getItemSelected() {
        List<OrderTran> result = new ArrayList<>();
        for (OrderTran orderTran : mOrderTrans) {
            if (orderTran.isSelected()) {
                result.add(orderTran);
            }
        }
        return result;
    }

    class ObjectMonth {
        private String originDate;
        private String filterDate;

        public ObjectMonth(String originDate, String filterDate) {
            this.originDate = originDate;
            this.filterDate = filterDate;
        }

        public String getFilterDate() {
            return filterDate;
        }

        public String getOriginDate() {
            return originDate;
        }

        public void setFilterDate(String filterDate) {
            this.filterDate = filterDate;
        }

        public void setOriginDate(String originDate) {
            this.originDate = originDate;
        }
    }

}
