package com.anphuocthai.staff.ui.delivery.nottransportyet.reporthtml;

import com.androidnetworking.error.ANError;
import com.anphuocthai.staff.R;
import com.anphuocthai.staff.api.ApiURL;
import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.data.DataManager;
import com.anphuocthai.staff.interfaces.IAPICallback;
import com.anphuocthai.staff.model.ResponseMessage;
import com.anphuocthai.staff.ui.base.BasePresenter;
import com.anphuocthai.staff.ui.delivery.debt.customerdebt.ReportResponse;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;
import com.anphuocthai.staff.utils.App;
import com.anphuocthai.staff.utils.rx.SchedulerProvider;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.rx2androidnetworking.Rx2AndroidNetworking;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.anphuocthai.staff.utils.Constants.ERROR_CODE;
import static com.anphuocthai.staff.utils.Constants.NOT_FOUND_CODE;
import static com.anphuocthai.staff.utils.Constants.SUCCESS_CODE;

public class ReportReceiveOrderPresenter<V extends ReportReceiveOrderMvpView> extends BasePresenter<V> implements ReportReceiveOrderMvpPresenter<V> {

    @Inject
    public ReportReceiveOrderPresenter(NetworkManager networkManager, DataManager dataManager, SchedulerProvider schedulerProvider, CompositeDisposable compositeDisposable) {
        super(networkManager, dataManager, schedulerProvider, compositeDisposable);
    }

    private JSONObject createJSONObject(final int page) {
        JSONObject notTranObject = new JSONObject();
        try {
            notTranObject.put("phanLoai", 4);
            notTranObject.put("guidThanhVien", "");
            notTranObject.put("sessionID", "");
            notTranObject.put("tuNgay", "");
            notTranObject.put("denNgay", "");
            notTranObject.put("trangThaiIds", "");
            notTranObject.put("pageIndex", page);
            notTranObject.put("pageSize", 10);
            notTranObject.put("BoPhanID", App.getInstance().getDepartmentId());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return notTranObject;
    }
    @Override
    public Flowable<List<OrderTran>> getAllDebtApiCall(JSONObject object) {
        return Rx2AndroidNetworking.post(ApiURL.GET_ORDER_URL)
                //.addHeaders(mApiHeader.getProtectedApiHeader())
                .addHeaders(ApiURL.getBaseHeader())
                .addHeaders(ApiURL.getTokenHeader())
                .addJSONObjectBody(object)
                .build()
                .getObjectListFlowable(OrderTran.class);

    }

    @Override
    public void onSetStartCollectDebt(OrderTran orderTran) {
        JSONObject setDebtObject = new JSONObject();
        try {
            setDebtObject.put("id", 0);
            setDebtObject.put("sessionID", orderTran.getSessionID());
            setDebtObject.put("SoTien", 0);
            setDebtObject.put("BoPhanID", App.getInstance().getDepartmentId());
        } catch (Exception e) {
            e.printStackTrace();
        }

        Rx2AndroidNetworking.post(ApiURL.SET_DEBT_TRANSPORT_URL)
                .addHeaders(ApiURL.getBaseHeader())
                .addHeaders(ApiURL.getTokenHeader())
                .addJSONObjectBody(setDebtObject)
                .build()
                .getObjectObservable(ResponseMessage.class)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ResponseMessage>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ResponseMessage responseMessage) {
                        if (responseMessage.getCode().equals(SUCCESS_CODE)) {
                            getmMvpView().showMessage(R.string.shipped_start_tran_success);
                            onViewPrepared();
                        } else if (responseMessage.getCode().equals(NOT_FOUND_CODE)) {

                        } else if (responseMessage.getCode().equals(ERROR_CODE)) {

                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void getCustomerDebt() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("code", "BAO_CAO_VAN_CHUYEN");
        }catch (Exception e) {
            e.printStackTrace();
        }

        getmNetworkManager().sendPostRequest(ApiURL.GET_REPORT, jsonObject, new IAPICallback() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                if (response != null) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<ReportResponse>() {
                    }.getType();
                    ReportResponse reportResponse = gson.fromJson(response.toString(), type);
                    if (!isViewAttached()) {
                        return;
                    }
                    if (reportResponse.getObjectInfo() != null)
                    getmMvpView().onGetCustomerDebtSuccess(reportResponse.getObjectInfo());
                }
            }

            @Override
            public void onResponseSuccess(JSONArray response) {

            }

            @Override
            public void onResponseError(ANError anError) {
                getmMvpView().setRefresh(false);
            }
        });
    }


//    @Override
//    public void onViewPrepared() {
//        getmMvpView().showLoading();
//        getCompositeDisposable().add(getAllDebtApiCall(createJSONObject(1))
//                .onBackpressureDrop()
//                .subscribeOn(getSchedulerProvider().io())
//                .observeOn(getSchedulerProvider().ui())
//                .subscribe(new Consumer<List<OrderTran>>() {
//                    @Override
//                    public void accept(@NonNull List<OrderTran> orderTrans)
//                            throws Exception {
//                        if (orderTrans != null) {
//                            getmMvpView().updateOrderTran(orderTrans);
//                        }
//                        getmMvpView().hideLoading();
//                    }
//                }, new Consumer<Throwable>() {
//                    @Override
//                    public void accept(@NonNull Throwable throwable)
//                            throws Exception {
//                        if (!isViewAttached()) {
//                            return;
//                        }
//
//                        getmMvpView().hideLoading();
//                        // handle the error here
//                        if (throwable instanceof ANError) {
//                            ANError anError = (ANError) throwable;
//                            handleApiError(anError);
//                        }
//                    }
//                }));
//    }


    @Override
    public void onViewPrepared() {
       // getmMvpView().showLoading();
        getmNetworkManager().sendPostRequestWithArrayResponse(ApiURL.GET_ORDER_URL, createJSONObject(1), new IAPICallback() {
            @Override
            public void onResponseSuccess(JSONObject response) {}
            @Override
            public void onResponseSuccess(JSONArray response) {
                //Log.d(TAG, response.toString());
                if (response != null) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<ArrayList<OrderTran>>() {
                    }.getType();
                    ArrayList<OrderTran> orderTrans = gson.fromJson(response.toString(), type);
                    if (!isViewAttached()) {
                        return;
                    }
                    getmMvpView().updateOrderTran(orderTrans);
                    //getmMvpView().hideLoading();
                }
            }

            @Override
            public void onResponseError(ANError anError) {
               // getmMvpView().hideLoading();
            }
        });
    }
}
