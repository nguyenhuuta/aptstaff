package com.anphuocthai.staff.ui.base;

import androidx.annotation.StringRes;
import androidx.viewpager.widget.ViewPager;

import com.anphuocthai.staff.fragments.dialogmessage.DialogMessage;

public interface MvpView {

    void showLoading();

    void hideLoading();

    void openActivityOnTokenExpire();

    void onError(@StringRes int resId);

    void onError(String message);

    void showMessage(String message);

    void showMessage(@StringRes int resId);

    void showSnackBar(String message);

    void showDialog(String message, DialogMessage.IOnClickListener callback);

    void showDialogConfirm(String message, DialogMessage.IOnClickListener callback);

    boolean isNetworkConnected();

    void hideKeyboard();

    void setupToolbar(String title);

    void setupToobarWithTab(String title, ViewPager viewPager, boolean isFixed);

}
