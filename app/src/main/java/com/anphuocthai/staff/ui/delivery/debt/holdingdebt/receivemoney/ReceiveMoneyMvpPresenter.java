package com.anphuocthai.staff.ui.delivery.debt.holdingdebt.receivemoney;

import com.anphuocthai.staff.ui.base.MvpPresenter;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;

public interface ReceiveMoneyMvpPresenter<V extends ReceiveMoneyMvpView> extends MvpPresenter<V> {
    void onStopCollectDebt(OrderTran orderTran, int realReceive);
}
