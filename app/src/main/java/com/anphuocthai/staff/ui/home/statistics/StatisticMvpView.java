package com.anphuocthai.staff.ui.home.statistics;

import com.anphuocthai.staff.ui.base.MvpView;
import com.anphuocthai.staff.ui.home.statistics.model.DynamicReport;
import com.anphuocthai.staff.ui.home.statistics.model.updatedynamic.PerformStatistic;
import com.anphuocthai.staff.ui.home.statistics.viewholder.DailyCVViewHolder;

import java.util.ArrayList;

public interface StatisticMvpView extends MvpView {

    void onGetDynamicReportSuccess(ArrayList<DynamicReport> dynamicReports);

    void openMyOrderShortcut();

    void openDebtManagement();

    void openCustomerShortcut();

    void openTransportShortcut();

    void openDailyWork();

    void onGetDynamicReportError();

    void onPerformStatisticSuccess(PerformStatistic performStatistic, DailyCVViewHolder viewHolder);

}
