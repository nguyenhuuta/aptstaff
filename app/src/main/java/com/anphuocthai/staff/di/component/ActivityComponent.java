package com.anphuocthai.staff.di.component;

import com.anphuocthai.staff.activities.TestActivity;
import com.anphuocthai.staff.di.PerActivity;
import com.anphuocthai.staff.di.module.ActivityModule;
import com.anphuocthai.staff.ui.base.BaseRecyclerViewActivity;
import com.anphuocthai.staff.ui.customer.detail.config.ConfigCustomerFM;
import com.anphuocthai.staff.ui.customer.detail.debtdetail.DebtDetailFragment;
import com.anphuocthai.staff.ui.customer.detail.historytransactions.HistoryTranFragment;
import com.anphuocthai.staff.ui.customer.detail.infomation.editinformation.phonetypedialog.PhoneTypeTypeDialog;
import com.anphuocthai.staff.ui.customer.detail.note.CustomerNoteFM;
import com.anphuocthai.staff.ui.customersearch.SearchActivity;
import com.anphuocthai.staff.ui.delivery.debt.DebtManagerActivity;
import com.anphuocthai.staff.ui.delivery.debt.alldebt.AllDebtFragment;
import com.anphuocthai.staff.ui.delivery.debt.customerdebt.CustomerDebtFragment;
import com.anphuocthai.staff.ui.delivery.debt.holdingdebt.HoldingDebtFragment;
import com.anphuocthai.staff.ui.delivery.debt.holdingdebt.receivemoney.ReceiveMoneyDialog;
import com.anphuocthai.staff.ui.delivery.debt.inventorydebt.InventoryDebtFragment;
import com.anphuocthai.staff.ui.delivery.debt.requestdebt.RequestDebtFragment;
import com.anphuocthai.staff.ui.delivery.debt.waitpaydebt.WaitPayDebtFragment;
import com.anphuocthai.staff.ui.delivery.myorder.MyOrderActivity;
import com.anphuocthai.staff.ui.delivery.myorder.adjust.AdjustOrderDialog;
import com.anphuocthai.staff.ui.delivery.myorder.detailorder.DetailMyOrderActivity;
import com.anphuocthai.staff.ui.delivery.myorder.detailorder.detailtab.DebtTabFragment;
import com.anphuocthai.staff.ui.delivery.nottransportyet.NotTranActivity;
import com.anphuocthai.staff.ui.delivery.nottransportyet.reporthtml.ReportReceiveOrderFragment;
import com.anphuocthai.staff.ui.delivery.nottransportyet.requestreceiverorder.ReceiverOrderFragment;
import com.anphuocthai.staff.ui.delivery.nottransportyet.trachungtu.TraChungTuThanhToanFragment;
import com.anphuocthai.staff.ui.delivery.nottransportyet.transportorder.TransportOrderFragment;
import com.anphuocthai.staff.ui.delivery.preparetransport.PrepareTranActivity;
import com.anphuocthai.staff.ui.delivery.shipped.ShippedActivity;
import com.anphuocthai.staff.ui.delivery.shipped.finishtransport.FinishTransportDialog;
import com.anphuocthai.staff.ui.home.MainActivity;
import com.anphuocthai.staff.ui.home.container.ContainerFragment;
import com.anphuocthai.staff.ui.home.notifications.NotificationFragment;
import com.anphuocthai.staff.ui.home.order.OrderMainFragment;
import com.anphuocthai.staff.ui.home.statistics.StatisticFragment;
import com.anphuocthai.staff.ui.login.LoginActivity;
import com.anphuocthai.staff.ui.ordercart.OrderCartActivity;
import com.anphuocthai.staff.ui.ordercart.OrderCartAdapter;
import com.anphuocthai.staff.ui.ordercart.payconfig.OrderConfigDialog;
import com.anphuocthai.staff.ui.ordercart.sale.SaleDialog;
import com.anphuocthai.staff.ui.product.DetailProductActivity;
import com.anphuocthai.staff.ui.profile.personalinfo.ProfileFragment;
import com.anphuocthai.staff.ui.splash.SplashActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = {AppComponent.class}, modules = {ActivityModule.class})
public interface ActivityComponent {
    void inject(DetailProductActivity activity);

    void inject(TestActivity activity);

    void inject(OrderCartActivity activity);

    void inject(SplashActivity activity);

    void inject(OrderCartAdapter adapter);

    void inject(OrderCartAdapter.ViewHolder viewHolder);

    void inject(SaleDialog dialog);

    void inject(SearchActivity searchActivity);

    void inject(NotTranActivity activity);

    void inject(MainActivity mainActivity);

    void inject(ContainerFragment fragment);

    void inject(OrderMainFragment orderMainFragment);

    void inject(PrepareTranActivity activity);

    void inject(FinishTransportDialog dialog);

    void inject(ShippedActivity activity);

    void inject(DebtManagerActivity activity);

    void inject(AllDebtFragment fragment);

    void inject(HoldingDebtFragment fragment);

    void inject(InventoryDebtFragment fragment);

    void inject(RequestDebtFragment fragment);

    void inject(WaitPayDebtFragment fragment);

    void inject(ReceiveMoneyDialog dialog);

    void inject(LoginActivity activity);

    void inject(ProfileFragment fragment);

    void inject(MyOrderActivity activity);

    void inject(BaseRecyclerViewActivity activity);

    void inject(HistoryTranFragment fragment);

    void inject(CustomerNoteFM fragment);

    void inject(AdjustOrderDialog dialog);

    void inject(DebtDetailFragment fragment);

    void inject(ConfigCustomerFM fm);


    void inject(ReceiverOrderFragment fragment);

    void inject(TransportOrderFragment fragment);

    void inject(OrderConfigDialog dialog);

    void inject(StatisticFragment fragment);

    void inject(NotificationFragment fragment);

    void inject(DetailMyOrderActivity activity);

    void inject(CustomerDebtFragment fragment);

    void inject(ReportReceiveOrderFragment fragment);

    void inject(DebtTabFragment fragment);

    void inject(PhoneTypeTypeDialog dialog);

    void inject(TraChungTuThanhToanFragment fragment);

}
