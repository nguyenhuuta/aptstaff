package com.anphuocthai.staff.di.component;

import android.app.Application;
import android.content.Context;

import com.anphuocthai.staff.activities.ViewModelFactory;
import com.anphuocthai.staff.data.DataManager;
import com.anphuocthai.staff.di.ApplicationContext;
import com.anphuocthai.staff.di.module.AppModule;
import com.anphuocthai.staff.utils.App;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = {AppModule.class})
@Singleton
public interface AppComponent {

    void inject(App app);

    @ApplicationContext
    Context context();

    Application application();

    DataManager getDataManager();

    ViewModelFactory viewModelFractory();

}
