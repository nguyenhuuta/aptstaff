package com.anphuocthai.staff.di.module;

import android.app.Application;
import android.content.Context;

import androidx.lifecycle.ViewModelProvider;
import androidx.room.Room;

import com.anphuocthai.staff.activities.ViewModelFactory;
import com.anphuocthai.staff.data.AppDataManager;
import com.anphuocthai.staff.data.DataManager;
import com.anphuocthai.staff.data.db.local.db.AppDatabase;
import com.anphuocthai.staff.data.db.local.db.AppDbHelper;
import com.anphuocthai.staff.data.db.local.db.DbHelper;
import com.anphuocthai.staff.data.db.local.prefs.AppPreferencesHelper;
import com.anphuocthai.staff.data.db.local.prefs.PreferencesHelper;
import com.anphuocthai.staff.data.db.network.ApiHeader;
import com.anphuocthai.staff.data.db.network.ApiHelper;
import com.anphuocthai.staff.data.db.network.AppApiHelper;
import com.anphuocthai.staff.di.ApiInfo;
import com.anphuocthai.staff.di.ApplicationContext;
import com.anphuocthai.staff.di.DatabaseInfo;
import com.anphuocthai.staff.di.PreferenceInfo;
import com.anphuocthai.staff.utils.Constants;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private final Application mApplication;

    public AppModule(Application application) {
        mApplication = application;
    }


    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @DatabaseInfo
    String provideDatabaseName() {
        return Constants.DB_NAME;
    }

    @Provides
    @Singleton
    DataManager provideDataManager(AppDataManager appDataManager) {
        return appDataManager;
    }

    @Provides
    @Singleton
    DbHelper provideDbHelper(AppDbHelper appDbHelper) {
        return appDbHelper;
    }

    @Provides
    @Singleton
    AppDatabase provideAppDatabase(@DatabaseInfo String dbName, Context context) {
        return Room.databaseBuilder(context, AppDatabase.class, dbName).fallbackToDestructiveMigration()
                .build();
    }

    @Provides
    @Singleton
    Context provideDatabaseContext(Application application) {
        return application;
    }


    @Provides
    @Singleton
    PreferencesHelper providePreferencesHelper(AppPreferencesHelper appPreferencesHelper) {
        return appPreferencesHelper;
    }

    @Provides
    @PreferenceInfo
    String providePreferenceName() {
        return Constants.USER_SETTING_FILE;
    }


    @Provides
    @Singleton
    ApiHelper provideApiHelper(AppApiHelper appApiHelper) {
        return appApiHelper;
    }

    @Provides
    @ApiInfo
    String provideApiKey() {
        return "";
    }

    @Provides
    @Singleton
    ApiHeader.ProtectedApiHeader provideProtectedApiHeader(@ApiInfo String apiKey,
                                                           PreferencesHelper preferencesHelper) {
        return new ApiHeader.ProtectedApiHeader(
                apiKey,
                1L,
                "");// preferencesHelper.getAccessToken();
    }

    @Provides
    @Singleton
    ViewModelProvider.Factory viewModelFactory(ViewModelFactory factory) {
        return factory;
    }


}
