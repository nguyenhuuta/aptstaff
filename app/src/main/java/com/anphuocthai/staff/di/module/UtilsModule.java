package com.anphuocthai.staff.di.module;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;

import com.anphuocthai.staff.api.NetworkManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class UtilsModule {

    private final Application mApplication;

    public UtilsModule(Application app){
        mApplication = app;
    }

    @Provides
    @NonNull
    @Singleton
    public NetworkManager provideNetworkManager(){
        return new NetworkManager();
    }

    @Provides
    @NonNull
    @Singleton
    SharedPreferences provideSharedPref(){
        return mApplication.getSharedPreferences("app_setting", Context.MODE_PRIVATE);
    }

}
