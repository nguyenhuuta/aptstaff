package com.anphuocthai.staff.di.module;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.di.PerActivity;
import com.anphuocthai.staff.model.CongNoModel;
import com.anphuocthai.staff.model.customer.Note;
import com.anphuocthai.staff.ui.customer.CustomerMvpPresenter;
import com.anphuocthai.staff.ui.customer.CustomerMvpView;
import com.anphuocthai.staff.ui.customer.CustomerPresenter;
import com.anphuocthai.staff.ui.customer.detail.config.ConfigCustomerAdapter;
import com.anphuocthai.staff.ui.customer.detail.config.ConfigCustomerMvpPresenter;
import com.anphuocthai.staff.ui.customer.detail.config.ConfigCustomerMvpView;
import com.anphuocthai.staff.ui.customer.detail.config.ConfigCustomerPresenter;
import com.anphuocthai.staff.ui.customer.detail.debtdetail.DebtDetailAdapter;
import com.anphuocthai.staff.ui.customer.detail.debtdetail.DebtDetailMvpPresenter;
import com.anphuocthai.staff.ui.customer.detail.debtdetail.DebtDetailMvpView;
import com.anphuocthai.staff.ui.customer.detail.debtdetail.DebtDetailPresenter;
import com.anphuocthai.staff.ui.customer.detail.historytransactions.HistoryTranMvpPresenter;
import com.anphuocthai.staff.ui.customer.detail.historytransactions.HistoryTranMvpView;
import com.anphuocthai.staff.ui.customer.detail.historytransactions.HistoryTranPresenter;
import com.anphuocthai.staff.ui.customer.detail.infomation.editinformation.phonetypedialog.PhoneTypeMvpPresenter;
import com.anphuocthai.staff.ui.customer.detail.infomation.editinformation.phonetypedialog.PhoneTypeMvpView;
import com.anphuocthai.staff.ui.customer.detail.infomation.editinformation.phonetypedialog.PhoneTypePresenter;
import com.anphuocthai.staff.ui.customer.detail.note.CustomerNoteAdapter;
import com.anphuocthai.staff.ui.customer.detail.note.CustomerNoteMvpPresenter;
import com.anphuocthai.staff.ui.customer.detail.note.CustomerNoteMvpView;
import com.anphuocthai.staff.ui.customer.detail.note.CustomerNotePresenter;
import com.anphuocthai.staff.ui.customersearch.CSearchMvpPresenter;
import com.anphuocthai.staff.ui.customersearch.CSearchMvpView;
import com.anphuocthai.staff.ui.customersearch.CustomerSearchPresenter;
import com.anphuocthai.staff.ui.customersearch.LoadMoreCustomerAdapter;
import com.anphuocthai.staff.ui.delivery.debt.DebtMvpPresenter;
import com.anphuocthai.staff.ui.delivery.debt.DebtMvpView;
import com.anphuocthai.staff.ui.delivery.debt.DebtPresenter;
import com.anphuocthai.staff.ui.delivery.debt.alldebt.AllDebtAdapter;
import com.anphuocthai.staff.ui.delivery.debt.alldebt.AllDebtMvpPresenter;
import com.anphuocthai.staff.ui.delivery.debt.alldebt.AllDebtMvpView;
import com.anphuocthai.staff.ui.delivery.debt.alldebt.AllDebtPresenter;
import com.anphuocthai.staff.ui.delivery.debt.customerdebt.CustomerDebtAdapter;
import com.anphuocthai.staff.ui.delivery.debt.customerdebt.CustomerDebtMvpPresenter;
import com.anphuocthai.staff.ui.delivery.debt.customerdebt.CustomerDebtMvpView;
import com.anphuocthai.staff.ui.delivery.debt.customerdebt.CustomerDebtPresenter;
import com.anphuocthai.staff.ui.delivery.debt.holdingdebt.HoldingDebtAdapter;
import com.anphuocthai.staff.ui.delivery.debt.holdingdebt.HoldingDebtMvpPresenter;
import com.anphuocthai.staff.ui.delivery.debt.holdingdebt.HoldingDebtMvpView;
import com.anphuocthai.staff.ui.delivery.debt.holdingdebt.HoldingDebtPresenter;
import com.anphuocthai.staff.ui.delivery.debt.holdingdebt.receivemoney.ReceiveMoneyMvpPresenter;
import com.anphuocthai.staff.ui.delivery.debt.holdingdebt.receivemoney.ReceiveMoneyMvpView;
import com.anphuocthai.staff.ui.delivery.debt.holdingdebt.receivemoney.ReceiveMoneyPresenter;
import com.anphuocthai.staff.ui.delivery.debt.inventorydebt.InventoryDebtAdapter;
import com.anphuocthai.staff.ui.delivery.debt.inventorydebt.InventoryDebtMvpPresenter;
import com.anphuocthai.staff.ui.delivery.debt.inventorydebt.InventoryDebtMvpView;
import com.anphuocthai.staff.ui.delivery.debt.inventorydebt.InventoryDebtPresenter;
import com.anphuocthai.staff.ui.delivery.debt.requestdebt.RequestDebtAdapter;
import com.anphuocthai.staff.ui.delivery.debt.requestdebt.RequestDebtMvpPresenter;
import com.anphuocthai.staff.ui.delivery.debt.requestdebt.RequestDebtMvpView;
import com.anphuocthai.staff.ui.delivery.debt.requestdebt.RequestDebtPresenter;
import com.anphuocthai.staff.ui.delivery.debt.waitpaydebt.WaitPayDebtAdapter;
import com.anphuocthai.staff.ui.delivery.debt.waitpaydebt.WaitPayDebtMvpPresenter;
import com.anphuocthai.staff.ui.delivery.debt.waitpaydebt.WaitPayDebtMvpView;
import com.anphuocthai.staff.ui.delivery.debt.waitpaydebt.WaitPayDebtPresenter;
import com.anphuocthai.staff.ui.delivery.debtlocation.DebtLocationMvpPresenter;
import com.anphuocthai.staff.ui.delivery.debtlocation.DebtLocationMvpView;
import com.anphuocthai.staff.ui.delivery.debtlocation.DebtLocationPresenter;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;
import com.anphuocthai.staff.ui.delivery.myorder.MyOrderAdapter;
import com.anphuocthai.staff.ui.delivery.myorder.MyOrderMvpPresenter;
import com.anphuocthai.staff.ui.delivery.myorder.MyOrderMvpView;
import com.anphuocthai.staff.ui.delivery.myorder.MyOrderPresenter;
import com.anphuocthai.staff.ui.delivery.myorder.adjust.AdjustOrderMvpPresenter;
import com.anphuocthai.staff.ui.delivery.myorder.adjust.AdjustOrderMvpView;
import com.anphuocthai.staff.ui.delivery.myorder.adjust.AdjustOrderPresenter;
import com.anphuocthai.staff.ui.delivery.myorder.detailorder.DetailMyOrderMvpPresenter;
import com.anphuocthai.staff.ui.delivery.myorder.detailorder.DetailMyOrderMvpView;
import com.anphuocthai.staff.ui.delivery.myorder.detailorder.DetailMyOrderPresenter;
import com.anphuocthai.staff.ui.delivery.myorder.detailorder.detailtab.DebtTabAdapter;
import com.anphuocthai.staff.ui.delivery.myorder.detailorder.detailtab.DebtTabMvpPresenter;
import com.anphuocthai.staff.ui.delivery.myorder.detailorder.detailtab.DebtTabMvpView;
import com.anphuocthai.staff.ui.delivery.myorder.detailorder.detailtab.DebtTabPresenter;
import com.anphuocthai.staff.ui.delivery.nottransportyet.NotTranAdapter;
import com.anphuocthai.staff.ui.delivery.nottransportyet.NotTranMvpPresenter;
import com.anphuocthai.staff.ui.delivery.nottransportyet.NotTranMvpView;
import com.anphuocthai.staff.ui.delivery.nottransportyet.NotTranPresenter;
import com.anphuocthai.staff.ui.delivery.nottransportyet.reporthtml.ReportReceiveOrderAdapter;
import com.anphuocthai.staff.ui.delivery.nottransportyet.reporthtml.ReportReceiveOrderMvpPresenter;
import com.anphuocthai.staff.ui.delivery.nottransportyet.reporthtml.ReportReceiveOrderMvpView;
import com.anphuocthai.staff.ui.delivery.nottransportyet.reporthtml.ReportReceiveOrderPresenter;
import com.anphuocthai.staff.ui.delivery.nottransportyet.requestreceiverorder.ReceiverOrderAdapter;
import com.anphuocthai.staff.ui.delivery.nottransportyet.requestreceiverorder.ReceiverOrderMvpPresenter;
import com.anphuocthai.staff.ui.delivery.nottransportyet.requestreceiverorder.ReceiverOrderMvpView;
import com.anphuocthai.staff.ui.delivery.nottransportyet.requestreceiverorder.ReceiverOrderPresenter;
import com.anphuocthai.staff.ui.delivery.nottransportyet.trachungtu.ITraChungTuPresenter;
import com.anphuocthai.staff.ui.delivery.nottransportyet.trachungtu.TraChungTuPresenter;
import com.anphuocthai.staff.ui.delivery.nottransportyet.transportorder.TransportOrderAdapter;
import com.anphuocthai.staff.ui.delivery.nottransportyet.transportorder.TransportOrderMvpPresenter;
import com.anphuocthai.staff.ui.delivery.nottransportyet.transportorder.TransportOrderMvpView;
import com.anphuocthai.staff.ui.delivery.nottransportyet.transportorder.TransportOrderPresenter;
import com.anphuocthai.staff.ui.delivery.preparetransport.PrepareTranAdapter;
import com.anphuocthai.staff.ui.delivery.preparetransport.PrepareTranMvpPresenter;
import com.anphuocthai.staff.ui.delivery.preparetransport.PrepareTranMvpView;
import com.anphuocthai.staff.ui.delivery.preparetransport.PrepareTranPresenter;
import com.anphuocthai.staff.ui.delivery.shipped.ShippedMvpPresenter;
import com.anphuocthai.staff.ui.delivery.shipped.ShippedMvpView;
import com.anphuocthai.staff.ui.delivery.shipped.ShippedPresenter;
import com.anphuocthai.staff.ui.delivery.shipped.finishtransport.FinishTransportMvpPresenter;
import com.anphuocthai.staff.ui.delivery.shipped.finishtransport.FinishTransportMvpView;
import com.anphuocthai.staff.ui.delivery.shipped.finishtransport.FinishTransportPresenter;
import com.anphuocthai.staff.ui.home.MainMvpPresenter;
import com.anphuocthai.staff.ui.home.MainMvpView;
import com.anphuocthai.staff.ui.home.MainPresenter;
import com.anphuocthai.staff.ui.home.container.ContainerMvpPresenter;
import com.anphuocthai.staff.ui.home.container.ContainerMvpView;
import com.anphuocthai.staff.ui.home.container.ContainerPresenter;
import com.anphuocthai.staff.ui.home.notifications.NotificationMvpPresenter;
import com.anphuocthai.staff.ui.home.notifications.NotificationMvpView;
import com.anphuocthai.staff.ui.home.notifications.NotificationPresenter;
import com.anphuocthai.staff.ui.home.order.OrderMainMvpPresenter;
import com.anphuocthai.staff.ui.home.order.OrderMainMvpView;
import com.anphuocthai.staff.ui.home.order.OrderMainPresenter;
import com.anphuocthai.staff.ui.home.statistics.StatisticMvpPresenter;
import com.anphuocthai.staff.ui.home.statistics.StatisticMvpView;
import com.anphuocthai.staff.ui.home.statistics.StatisticPresenter;
import com.anphuocthai.staff.ui.login.LoginMvpPresenter;
import com.anphuocthai.staff.ui.login.LoginMvpView;
import com.anphuocthai.staff.ui.login.LoginPresenter;
import com.anphuocthai.staff.ui.ordercart.OrderCartAdapter;
import com.anphuocthai.staff.ui.ordercart.OrderCartMvpPresenter;
import com.anphuocthai.staff.ui.ordercart.OrderCartMvpView;
import com.anphuocthai.staff.ui.ordercart.OrderCartPresenter;
import com.anphuocthai.staff.ui.ordercart.payconfig.OrderConfigMvpPresenter;
import com.anphuocthai.staff.ui.ordercart.payconfig.OrderConfigMvpView;
import com.anphuocthai.staff.ui.ordercart.payconfig.OrderConfigPresenter;
import com.anphuocthai.staff.ui.ordercart.sale.SaleDialogMvpPresenter;
import com.anphuocthai.staff.ui.ordercart.sale.SaleDialogMvpView;
import com.anphuocthai.staff.ui.ordercart.sale.SaleDialogPresenter;
import com.anphuocthai.staff.ui.product.DetailProductMvpPresenter;
import com.anphuocthai.staff.ui.product.DetailProductMvpView;
import com.anphuocthai.staff.ui.product.DetailProductPresenter;
import com.anphuocthai.staff.ui.product.OrderLogic;
import com.anphuocthai.staff.ui.profile.personalinfo.ProfileMvpPresenter;
import com.anphuocthai.staff.ui.profile.personalinfo.ProfileMvpView;
import com.anphuocthai.staff.ui.profile.personalinfo.ProfilePresenter;
import com.anphuocthai.staff.ui.splash.SplashMvpPresenter;
import com.anphuocthai.staff.ui.splash.SplashMvpView;
import com.anphuocthai.staff.ui.splash.SplashPresenter;
import com.anphuocthai.staff.utils.rx.AppSchedulerProvider;
import com.anphuocthai.staff.utils.rx.SchedulerProvider;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@Module
public class ActivityModule {
    private AppCompatActivity mActivity;

    public ActivityModule(AppCompatActivity appCompatActivity) {
        this.mActivity = appCompatActivity;
    }

    @Provides
    @PerActivity
    SplashMvpPresenter<SplashMvpView> provideSplashPresenter(
            SplashPresenter<SplashMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    LoginMvpPresenter<LoginMvpView> provideLoginPresenter(
            LoginPresenter<LoginMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    ProfileMvpPresenter<ProfileMvpView> provideProfilePresenter(
            ProfilePresenter<ProfileMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    ReportReceiveOrderMvpPresenter<ReportReceiveOrderMvpView> provideReportReceiveOrderPresenter(
            ReportReceiveOrderPresenter<ReportReceiveOrderMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    MyOrderMvpPresenter<MyOrderMvpView> provideMyOrderPresenter(
            MyOrderPresenter<MyOrderMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    DetailProductMvpPresenter<DetailProductMvpView> provideDetailProductPresenter(
            DetailProductPresenter<DetailProductMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    DetailMyOrderMvpPresenter<DetailMyOrderMvpView> provideDetailMyOrderPresenter(
            DetailMyOrderPresenter<DetailMyOrderMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    DebtLocationMvpPresenter<DebtLocationMvpView> provideDebtLocationPresenter(
            DebtLocationPresenter<DebtLocationMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    CustomerDebtMvpPresenter<CustomerDebtMvpView> provideCustomerDebtPresenter(
            CustomerDebtPresenter<CustomerDebtMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    CustomerMvpPresenter<CustomerMvpView> provideCustomerPresenter(
            CustomerPresenter<CustomerMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    NotTranMvpPresenter<NotTranMvpView> provideNotTranPresenter(
            NotTranPresenter<NotTranMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    PrepareTranMvpPresenter<PrepareTranMvpView> providePrepareTranPresenter(
            PrepareTranPresenter<PrepareTranMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    OrderCartMvpPresenter<OrderCartMvpView> provideOrderCartPresenter(OrderCartPresenter<OrderCartMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    MainMvpPresenter<MainMvpView> provideMainPresenter(MainPresenter<MainMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    ContainerMvpPresenter<ContainerMvpView> provideContainerPresenter(ContainerPresenter<ContainerMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    OrderMainMvpPresenter<OrderMainMvpView> provideOrderMainPresenter(OrderMainPresenter<OrderMainMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    DebtMvpPresenter<DebtMvpView> provideDebtPresenter(DebtPresenter<DebtMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    AllDebtMvpPresenter<AllDebtMvpView> provideAllDebtPresenter(AllDebtPresenter<AllDebtMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    PhoneTypeMvpPresenter<PhoneTypeMvpView> providePhoneTypePresenter(PhoneTypePresenter<PhoneTypeMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    HoldingDebtMvpPresenter<HoldingDebtMvpView> provideHoldingDebtPresenter(HoldingDebtPresenter<HoldingDebtMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    InventoryDebtMvpPresenter<InventoryDebtMvpView> provideInventoryDebtPresenter(InventoryDebtPresenter<InventoryDebtMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    RequestDebtMvpPresenter<RequestDebtMvpView> provideRequestDebtPresenter(RequestDebtPresenter<RequestDebtMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    WaitPayDebtMvpPresenter<WaitPayDebtMvpView> provideWaitPayDebtPresenter(WaitPayDebtPresenter<WaitPayDebtMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    CSearchMvpPresenter<CSearchMvpView> provideCSearchPresenter(CustomerSearchPresenter<CSearchMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    HistoryTranMvpPresenter<HistoryTranMvpView> provideHistoryTranPresenter(HistoryTranPresenter<HistoryTranMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    CustomerNoteMvpPresenter<CustomerNoteMvpView> provideCustomerNotePresenter(CustomerNotePresenter<CustomerNoteMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    DebtDetailMvpPresenter<DebtDetailMvpView> provideDebtDetailPresenter(DebtDetailPresenter<DebtDetailMvpView> presenter) {
        return presenter;
    }


    @Provides
    @PerActivity
    ConfigCustomerMvpPresenter<ConfigCustomerMvpView> provideConfigCustomerPresenter(ConfigCustomerPresenter<ConfigCustomerMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    TransportOrderMvpPresenter<TransportOrderMvpView> provideTransportOrderPresenter(TransportOrderPresenter<TransportOrderMvpView> presenter) {
        return presenter;
    }


    @Provides
    @PerActivity
    ReceiverOrderMvpPresenter<ReceiverOrderMvpView> provideReceiverOrderPresenter(ReceiverOrderPresenter<ReceiverOrderMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    OrderConfigMvpPresenter<OrderConfigMvpView> provideOrderConfigPresenter(OrderConfigPresenter<OrderConfigMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    DebtTabMvpPresenter<DebtTabMvpView> provideDebtTabPresenter(DebtTabPresenter<DebtTabMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    StatisticMvpPresenter<StatisticMvpView> provideStatisticPresenter(StatisticPresenter<StatisticMvpView> presenter) {
        return presenter;
    }

    @Provides
    @PerActivity
    NotificationMvpPresenter<NotificationMvpView> provideNotificationPresenter(NotificationPresenter<NotificationMvpView> presenter) {
        return presenter;
    }

    @Provides
    DebtDetailAdapter provideDebtDetailAdapter() {
        return new DebtDetailAdapter(new ArrayList<OrderTran>());
    }

    @Provides
    AllDebtAdapter provideAllDebtAdapter() {
        return new AllDebtAdapter(new ArrayList<OrderTran>());
    }

    @Provides
    DebtTabAdapter provideDebtTabAdapter() {
        return new DebtTabAdapter(new ArrayList<>());
    }


    @Provides
    ConfigCustomerAdapter provideConfigCustomerAdapter() {
        return new ConfigCustomerAdapter();
    }

    @Provides
    HoldingDebtAdapter provideHoldingDebtAdapter() {
        return new HoldingDebtAdapter(new ArrayList<CongNoModel>());
    }

    @Provides
    LoadMoreCustomerAdapter provideLoadMoreCustomerAdapter() {
        return new LoadMoreCustomerAdapter(new ArrayList<>());
    }


    @Provides
    InventoryDebtAdapter provideInventoryDebtAdapter() {
        return new InventoryDebtAdapter(new ArrayList<OrderTran>());
    }

    @Provides
    MyOrderAdapter provideMyOrderAdapter() {
        return new MyOrderAdapter(new ArrayList<OrderTran>());
    }

    @Provides
    RequestDebtAdapter provideRequestDebtAdapter() {
        return new RequestDebtAdapter(new ArrayList<CongNoModel>());
    }

    @Provides
    CustomerDebtAdapter provideCustomerDebtAdapter() {
        return new CustomerDebtAdapter(new ArrayList<>());
    }

    @Provides
    WaitPayDebtAdapter provideWaitPayDebtAdapter() {
        return new WaitPayDebtAdapter(new ArrayList<>());
    }

    @Provides
    OrderCartAdapter provideOrderCartAdapter() {
        return new OrderCartAdapter(new ArrayList<>());
    }

    @Provides
    NotTranAdapter provideNotTranAdapter() {
        return new NotTranAdapter(new ArrayList<OrderTran>());
    }

    @Provides
    PrepareTranAdapter providePrepareTranAdapter() {
        return new PrepareTranAdapter(new ArrayList<OrderTran>());
    }

    @Provides
    CustomerNoteAdapter provideCustomerNoteAdapter() {
        return new CustomerNoteAdapter(new ArrayList<Note>());
    }

    @Provides
    ReportReceiveOrderAdapter provideReportReceiveOrderAdapter() {
        return new ReportReceiveOrderAdapter(new ArrayList<>());
    }

    @Provides
    ReceiverOrderAdapter provideReceiverOrderAdapter() {
        return new ReceiverOrderAdapter(new ArrayList<OrderTran>());
    }

    @Provides
    TransportOrderAdapter provideTransportOrderAdapter() {
        return new TransportOrderAdapter(new ArrayList<OrderTran>());
    }


    @Provides
    SaleDialogMvpPresenter<SaleDialogMvpView> provideSaleDialogPresenter(
            SaleDialogPresenter<SaleDialogMvpView> presenter) {
        return presenter;
    }

    @Provides
    AdjustOrderMvpPresenter<AdjustOrderMvpView> provideAdjustOrderPresenter(
            AdjustOrderPresenter<AdjustOrderMvpView> presenter) {
        return presenter;
    }


    @Provides
    FinishTransportMvpPresenter<FinishTransportMvpView> provideFinishTransportPresenter(
            FinishTransportPresenter<FinishTransportMvpView> presenter) {
        return presenter;
    }

    @Provides
    ReceiveMoneyMvpPresenter<ReceiveMoneyMvpView> provideReceiveMoneyPresenter(
            ReceiveMoneyPresenter<ReceiveMoneyMvpView> presenter) {
        return presenter;
    }


    @Provides
    ShippedMvpPresenter<ShippedMvpView> provideShippedPresenter(
            ShippedPresenter<ShippedMvpView> presenter) {
        return presenter;
    }


    @Provides
    @NonNull
    @PerActivity
    public NetworkManager provideNetworkManager() {
        return new NetworkManager();
    }


    @Provides
    @NonNull
    @PerActivity
    public OrderLogic provideOrderLogic() {
        return new OrderLogic();
    }


    @Provides
    CompositeDisposable provideCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Provides
    SchedulerProvider provideSchedulerProvider() {
        return new AppSchedulerProvider();
    }

    @Provides
    LinearLayoutManager provideLinearLayoutManager(AppCompatActivity activity) {
        return new LinearLayoutManager(activity);
    }

    @Provides
    AppCompatActivity provideActivity() {
        return mActivity;
    }


    @Provides
    @PerActivity
    ITraChungTuPresenter provideTraChungTuPresenter(TraChungTuPresenter presenter) {
        return presenter;
    }

}
