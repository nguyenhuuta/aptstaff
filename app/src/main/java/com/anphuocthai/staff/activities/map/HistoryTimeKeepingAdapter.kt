package com.anphuocthai.staff.activities.map

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.anphuocthai.staff.R
import com.anphuocthai.staff.model.ChamCongModel
import com.anphuocthai.staff.utils.DateType
import com.anphuocthai.staff.utils.convertDate
import com.anphuocthai.staff.utils.extension.loadImage
import kotlinx.android.synthetic.main.item_history_time_keeping.view.*


/**
 * Created by OpenYourEyes on 7/27/2020
 */
class HistoryTimeKeepingAdapter(context: Context) : RecyclerView.Adapter<HistoryTimeKeepingAdapter.ViewHolder>() {
    val list: MutableList<ChamCongModel> = mutableListOf()

    var layoutInflater: LayoutInflater = LayoutInflater.from(context)

    fun insertFirst(model: ChamCongModel) {
        list.add(0, model)
        notifyItemInserted(0)
    }

    fun updateList(updateList: MutableList<ChamCongModel>) {
        list.clear()
        list.addAll(updateList)
        notifyDataSetChanged()
    }


    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = layoutInflater.inflate(R.layout.item_history_time_keeping, viewGroup, false)
        return ViewHolder(view)
    }


    override fun onBindViewHolder(holder: HistoryTimeKeepingAdapter.ViewHolder, position: Int) {
        holder.bind(list[position])
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(model: ChamCongModel) {
            itemView.imageTimeKeeping.loadImage(model.imageBase64)

            val isXacNhan = model.isXacNhan ?: true
            itemView.daDuyet.apply {
                if (isXacNhan) { // isXacNhan = 1
                    itemView.daDuyet.text = "Đã Duyệt"
                    itemView.daDuyet.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
                    itemView.imageTimeKeeping.background = ContextCompat.getDrawable(context, R.drawable.border_primary)
                } else { // isXacNhan = 0
                    itemView.daDuyet.text = "Đợi Duyệt"
                    itemView.daDuyet.setTextColor(ContextCompat.getColor(context, R.color.red))
                    itemView.imageTimeKeeping.background = ContextCompat.getDrawable(context, R.drawable.border_red)
                }
            }


            val time = " : ${model.ngayChamCong?.convertDate(DateType.FUll3, DateType.ONLY_TIME2)}"
            itemView.timeKeeping.text = time
            val machine = model.machineID?.let {
                if (it.isEmpty()) {
                    "Android App"
                } else {
                    it
                }
            } ?: kotlin.run {
                "Android App"
            }

            itemView.machineId.text = " : $machine"
            itemView.placeName.text = " : ${model.placeName ?: ""}"
        }
    }

}