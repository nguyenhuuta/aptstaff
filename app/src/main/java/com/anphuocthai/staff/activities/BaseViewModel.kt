package com.anphuocthai.staff.activities

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.anphuocthai.staff.api.BaseResponse
import com.anphuocthai.staff.api.ErrorMessage
import com.anphuocthai.staff.baserequest.APIResponse
import com.anphuocthai.staff.baserequest.APIService
import com.anphuocthai.staff.baserequest.ApiCoroutines
import com.anphuocthai.staff.data.DataManager
import com.anphuocthai.staff.data.RxProgressBar
import com.anphuocthai.staff.utils.printLog
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch

/**
 * Created by OpenYourEyes on 12/28/20
 */

open class BaseViewModel : ViewModel() {

    val apiCoroutines: ApiCoroutines by lazy {
        APIService.getInstance().apiCoroutines
    }

    lateinit var dataManager: DataManager
    val loading: RxProgressBar by lazy {
        RxProgressBar()
    }

    val showDialog = PublishSubject.create<ErrorMessage>()

    val showLoading = PublishSubject.create<Boolean>()

    val compositeDisposable: CompositeDisposable = CompositeDisposable()
    fun inject(dataManager: DataManager) {
        this.dataManager = dataManager
    }

    fun Disposable.addToDisposable() {
        compositeDisposable.add(this)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    fun <T> executeRequest(request: suspend (ApiCoroutines.() -> APIResponse<T>), response: (T) -> Unit): Job {
        return viewModelScope.launch {
            flow {
                try {
                    showLoading.onNext(true)
                    val resultRequest = request(apiCoroutines)
                    showLoading.onNext(false)
                    if (resultRequest.isSuccess) {
                        emit(resultRequest.data)
                    } else {
                        val messageError = resultRequest.message
                        showDialog.onNext(ErrorMessage(messageError))
                    }
                } catch (ignore: Exception) {
                    val messageError = ignore.message
                    showDialog.onNext(ErrorMessage(messageError))
                }
            }.flowOn(Dispatchers.IO)
                    .collect { r ->
                        response(r)
                    }
        }
    }

    fun <T> executeRequestWithBaseResponse(request: suspend (ApiCoroutines.() -> BaseResponse<T>), response: (T) -> Unit): Job {
        return viewModelScope.launch() {
            flow {
                try {
                    showLoading.onNext(true)
                    val resultRequest = request(apiCoroutines)
                    showLoading.onNext(false)
                    if (resultRequest.isSuccess()) {
                        emit(resultRequest.objectInfo!!)
                    } else {
                        val messageError = resultRequest.message ?: ""
                        showDialog.onNext(ErrorMessage(messageError))
                    }
                } catch (ignore: Exception) {
                    val messageError = ignore.message
                    showDialog.onNext(ErrorMessage(messageError))
                }
            }.flowOn(Dispatchers.IO)
                    .collect { r ->
                        response(r)
                    }
        }
    }

    suspend fun <T> executeRequestFlow(request: suspend (ApiCoroutines) -> APIResponse<T>): Flow<T> {
        return flow {
            try {
                showLoading.onNext(true)
                val resultRequest = request(apiCoroutines)
                showLoading.onNext(false)
                if (resultRequest.isSuccess) {
                    printLog("request success")
                    emit(resultRequest.data)
                } else {
                    val messageError = resultRequest.message
                    showDialog.onNext(ErrorMessage(messageError))
                }
            } catch (ignore: Exception) {
                val messageError = ignore.message
                showDialog.onNext(ErrorMessage(messageError))
            }
        }.flowOn(Dispatchers.IO)
    }

    suspend fun <T> executeRequestBaseResponseFlow(request: suspend (ApiCoroutines.() -> BaseResponse<T>)): Flow<BaseResponse<T>> {
        return flow {
            try {
                showLoading.onNext(true)
                val resultRequest = request(apiCoroutines)
                showLoading.onNext(false)
                if (resultRequest.isSuccess()) {
                    printLog("request success")
                    emit(resultRequest)
                } else {
                    val messageError = resultRequest.message
                    showDialog.onNext(ErrorMessage(messageError))
                }
            } catch (ignore: Exception) {
                val messageError = ignore.message
                showDialog.onNext(ErrorMessage(messageError))
            }
        }.flowOn(Dispatchers.IO)
    }


}