package com.anphuocthai.staff.activities.tonghopluong

import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.anphuocthai.staff.R
import com.anphuocthai.staff.activities.BaseActivities
import com.anphuocthai.staff.activities.tonghopcong.ChiTietCongTemp
import com.anphuocthai.staff.api.*
import com.anphuocthai.staff.base.actionbar.CustomActionBar
import com.anphuocthai.staff.data.trackProgressBar
import com.anphuocthai.staff.entities.TongHopCongModel
import com.anphuocthai.staff.entities.TongHopCongResponse
import com.anphuocthai.staff.model.ChiTietPhuCapModel
import com.anphuocthai.staff.model.ChiTietPhuCapResponse
import com.anphuocthai.staff.utils.App
import com.anphuocthai.staff.utils.DateConvert
import com.anphuocthai.staff.utils.printLog
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import kotlinx.android.synthetic.main.item_tong_hop_cong.view.*
import kotlinx.android.synthetic.main.tonghopluong_activity.*
import java.util.*


/**
 * Created by OpenYourEyes on 9/8/2020
 */
class TongHopLuongActivity : BaseActivities() {
    var listChiTietPhuCapModel: List<ChiTietPhuCapModel> = listOf()
    var ngayCongChuan = 0
    override fun getContentView(): Int {
        return R.layout.tonghopluong_activity
    }

    override fun initAction() {
    }

    override fun initView() {
        customActionBar.setTitle("Tổng hợp lương")
        customActionBar.setCustomActionBar(object : CustomActionBar.ICustomActionBar {
            override fun onButtonRightClick() {
            }

            override fun onButtonLeftClick() {
                finish()
            }
        })

        recyclerView.apply {
            layoutManager = LinearLayoutManager(this@TongHopLuongActivity)
            adapter = TongHopLuongAdapter()
        }

    }

    override fun initData() {
        val calendar: Calendar = Calendar.getInstance()
        countSundayInMonth(calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR))
        val month = DateConvert.convertDate(calendar.timeInMillis, DateConvert.TypeConvert.MONTH_YEAR)
        fun calculatorTime(isNext: Boolean): ChiTietCongTemp {
            if (isNext) {
                calendar.add(Calendar.MONTH, 1)
            } else {
                calendar.add(Calendar.MONTH, -1)
            }
            countSundayInMonth(calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR))
            val time = DateConvert.convertDate(calendar.timeInMillis, DateConvert.TypeConvert.MONTH_YEAR)
            printLog("time $time");
            getTongHopLuong(time)
            val isNow = time == month
            return ChiTietCongTemp(time, isNow)
        }

        icPrev.setOnClickListener {
            icNext.visibility = View.VISIBLE
            val timePrev = calculatorTime(false)
            tongLuongThang.text = "Tháng ${timePrev.time}"

        }
        icNext.setOnClickListener {
            val timeNext = calculatorTime(true)
            if (timeNext.isNow) {
                it.visibility = View.INVISIBLE
            }
            tongLuongThang.text = "Tháng ${timeNext.time}"
        }
        tongLuongThang.text = "Tháng $month"
        val souce1 = _getTongHopLuong(month)
        val souce2 = _getPhuCap()
        Observable.zip(souce1, souce2, BiFunction<TongHopCongResponse, ChiTietPhuCapResponse, Pair<TongHopCongResponse, ChiTietPhuCapResponse>> { t1, t2 ->
            Pair(t1, t2)
        }).trackProgressBar(progressBar)
                .runOnMain()
                .subscribe {
                    val tongHopCongResponse = it.first
                    val chiTietPhuCapResponse = it.second
                    listChiTietPhuCapModel = chiTietPhuCapResponse.data ?: listOf()
                    fillData(tongHopCongResponse.data)
                }.addTo(compositeDisposable)
    }

    private fun _getTongHopLuong(month: String): Observable<TongHopCongResponse> {
        return ApiURL.TonghopLuong.postRequest()
                .addQueryParameter("workMonth", month)
                .addQueryParameter("UserID", dataManager.userInfo?.id?.toString())
                .addQueryParameter("PhongBanId", App.getInstance().departmentId.toString())
                .get(TongHopCongResponse::class.java)
    }

    private fun _getPhuCap(): Observable<ChiTietPhuCapResponse> {
        return ApiURL.ChiTietPhuCap.postRequest()
                .addQueryParameter("UserID", dataManager.userInfo?.id?.toString())
                .get(ChiTietPhuCapResponse::class.java)
                .trackProgressBar(progressBar)
    }

    private fun countSundayInMonth(month: Int, year: Int) {
        val cal = Calendar.getInstance()
        cal.set(year, month, 1)
        ngayCongChuan = cal.getActualMaximum(Calendar.DAY_OF_MONTH)
        do {
            val day = cal.get(Calendar.DAY_OF_WEEK)
            if (day == Calendar.SUNDAY) {
                ngayCongChuan--
            }
            cal.add(Calendar.DAY_OF_YEAR, 1)
        } while ((cal.get(Calendar.MONTH)) == month)
    }

    private fun getTongHopLuong(month: String) {
        _getTongHopLuong(month)
                .runOnMain()
                .trackProgressBar(progressBar)
                .subscribe {
                    it?.let {
                        fillData(it.data)
                    } ?: kotlin.run {
                        showDialog("Không có dữ liệu", null)
                    }
                }.addTo(compositeDisposable)
    }

    private fun fillData(list: List<TongHopCongModel>) {
        val tongHopLuong = list.firstOrNull()
        if (tongHopLuong == null) {
            showDialog("Không có dữ liệu", null)
            return
        }
        val list = tongHopLuong.toList(listChiTietPhuCapModel, ngayCongChuan)
        val adapter = recyclerView.adapter as TongHopLuongAdapter
        adapter.updateList(list = list)
    }

}