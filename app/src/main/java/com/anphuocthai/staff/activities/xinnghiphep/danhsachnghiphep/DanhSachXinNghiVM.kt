package com.anphuocthai.staff.activities.xinnghiphep.danhsachnghiphep

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.anphuocthai.staff.activities.BaseViewModel
import com.anphuocthai.staff.api.ErrorMessage
import com.anphuocthai.staff.model.DanhSachXinNghiModel
import kotlinx.coroutines.launch
import java.util.*


/**
 * Created by OpenYourEyes on 3/8/21
 */
class DanhSachXinNghiVM : BaseViewModel() {
    private val _listDanhSachXinNghi = MutableLiveData<MutableList<DanhSachXinNghiModel>>()
    val listDanhSachXinNghi = _listDanhSachXinNghi


    fun layDanhSachXinNghi() {
        viewModelScope.launch {
            showLoading.onNext(true)
            val date = convertDate()
            val response = apiCoroutines.layDanhSachXinNghi(userId = dataManager.userInfoId, FomDate = date.first, ToDate = date.second)
            showLoading.onNext(false)
            if (response.isSuccess) {
                val list = response.data
                list.reverse()
                _listDanhSachXinNghi.postValue(list)
            } else {
                showDialog.onNext(ErrorMessage(response.message))
            }
        }
    }


    fun convertDate(): Pair<String, String> {
        fun twoNumber(month: Int): String {
            if (month < 10) {
                return "0$month"
            }
            return "$month"
        }

        val cal: Calendar = Calendar.getInstance()
        val startDate = "${cal.get(Calendar.YEAR)}-${twoNumber(cal.get(Calendar.MONTH) + 1)}-01"
        val endDate = "${cal.get(Calendar.YEAR)}-${twoNumber(cal.get(Calendar.MONTH) + 1)}-${cal.getActualMaximum(Calendar.DATE)}"
        return Pair(startDate, endDate)
    }

}