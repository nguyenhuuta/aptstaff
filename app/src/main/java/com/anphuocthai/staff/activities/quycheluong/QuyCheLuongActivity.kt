package com.anphuocthai.staff.activities.quycheluong

import com.anphuocthai.staff.R
import com.anphuocthai.staff.activities.BaseActivities
import com.anphuocthai.staff.api.*
import com.anphuocthai.staff.base.actionbar.CustomActionBar
import com.anphuocthai.staff.data.trackProgressBar
import com.anphuocthai.staff.entities.QuyCheLuongResponse
import com.anphuocthai.staff.utils.printLog
import kotlinx.android.synthetic.main.quy_che_luong_activity.*

/**
 * Created by OpenYourEyes on 9/4/2020
 */
class QuyCheLuongActivity : BaseActivities() {


    override fun getContentView(): Int {
        return R.layout.quy_che_luong_activity
    }

    override fun initAction() {
    }

    override fun initView() {
        toolbar.setTitle("Quy chế lương")
        toolbar.setCustomActionBar(object : CustomActionBar.ICustomActionBar {
            override fun onButtonRightClick() {
            }

            override fun onButtonLeftClick() {
                finish()
            }

        }
        )
    }

    override fun initData() {
        requestApi()
    }


    private fun requestApi() {
        val userInfo = dataManager.userInfo ?: return
        ApiURL.QuyCheLuong.getRequest()
                .addQueryParameter("userId", userInfo.id.toString())
                .get(QuyCheLuongResponse::class.java)
                .trackProgressBar(progressBar)
                .runOnMain()
                .subscribe({
                    showData(it)
                }, {
                    printLog("QuyCheLuong error: ${it.localizedMessage}")
                }).addTo(compositeDisposable)

    }


    private fun showData(response: QuyCheLuongResponse) {
        if (!response.isSuccess) return;

        response.data?.let {
            val html = it.noiDung ?: ""
            webView.loadData(html, "text/html", "UTF-8")
        }
    }
}