package com.anphuocthai.staff.activities.customer;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.viewpager.widget.ViewPager;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.fragments.customer.AgencyCustomerFM;
import com.anphuocthai.staff.fragments.customer.PersonalCustomerFM;
import com.anphuocthai.staff.fragments.customer.RestaurantCustomerFM;
import com.anphuocthai.staff.ui.base.BaseActivity;
import com.anphuocthai.staff.ui.base.ViewPagerAdapter;

/**
 * This class show customer list:
 * Have three type of customer: restaurant, agency, personal
 */
public class ListCustomerActivity extends BaseActivity {

    private ViewPager viewPager;
    private boolean isSelectCustomerToSchedule = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_customer);
        handleIntent();
        setupViewPager();
        customToolbar();
    }

    private void customToolbar() {
        if (isSelectCustomerToSchedule) {
            setupToobarWithTab(getString(R.string.ci_choose_list_customer), viewPager, true);
        }else {
            setupToobarWithTab(getString(R.string.list_customer_name), viewPager, true);
        }

    }

    private void handleIntent(){
        Bundle b = getIntent().getBundleExtra("CheckSelectWorkScheduleExtra");
        isSelectCustomerToSchedule = b.getBoolean("isSelectCustomerToSchedule");
        
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.tool_bar_list_customer_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_add_customer) {
            Toast.makeText(this, getString(R.string.ci_create_new_customer), Toast.LENGTH_LONG).show();
            Intent i = new Intent(this, AddNewCustomerActivity.class);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void setupViewPager() {
        viewPager = findViewById(R.id.viewpagerListCustomer);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(RestaurantCustomerFM.newInstance(this, isSelectCustomerToSchedule), getString(R.string.ci_restaurant_customer));
        adapter.addFragment(AgencyCustomerFM.newInstance(this, isSelectCustomerToSchedule),  getString(R.string.ci_agency_customer));
        adapter.addFragment(PersonalCustomerFM.newInstance(this, isSelectCustomerToSchedule),  getString(R.string.ci_personal_customer));
        viewPager.setAdapter(adapter);
    }


}
