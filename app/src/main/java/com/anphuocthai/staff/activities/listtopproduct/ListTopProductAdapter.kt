package com.anphuocthai.staff.activities.listtopproduct

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.PorterDuff
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityOptionsCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.anphuocthai.staff.R
import com.anphuocthai.staff.model.order.Product
import com.anphuocthai.staff.ui.product.DetailProductActivity
import com.anphuocthai.staff.utils.Utils
import com.anphuocthai.staff.utils.extension.loadImage
import kotlinx.android.synthetic.main.item_list_top_product.view.*

/**
 * Created by OpenYourEyes on 9/1/2020
 */

class ListTopProductAdapter(val context: Context, val isSale: Boolean, private val products: List<Product>, val addToCart: (Product) -> Unit) : RecyclerView.Adapter<ListTopProductAdapter.ListTopViewHolder>() {
    private val layoutInflater = LayoutInflater.from(context)
    override fun getItemCount(): Int {
        return products.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListTopViewHolder {
        val view = layoutInflater.inflate(R.layout.item_list_top_product, parent, false)
        return ListTopViewHolder(view)
    }


    override fun onBindViewHolder(viewHoler: ListTopViewHolder, position: Int) {
        viewHoler.bindData(products[position])
    }


    inner class ListTopViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        @SuppressLint("SetTextI18n")
        fun bindData(product: Product) {
            fun checkStatusImageCart() {
                if (product.isSelected) {
                    itemView.imageCart.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary), PorterDuff.Mode.SRC_IN)
                } else {
                    itemView.imageCart.setColorFilter(ContextCompat.getColor(context, R.color.darkerGray), PorterDuff.Mode.SRC_IN)
                }
            }
            itemView.imageCart.setOnClickListener {
                product.isSelected = !product.isSelected
                addToCart(product)
                checkStatusImageCart()
            }
            itemView.setOnClickListener {
                if (context is AppCompatActivity) {
                    val intent = Intent(context, DetailProductActivity::class.java)
                    val bundle = Bundle()
                    bundle.putInt("productid", product.id)
                    bundle.putInt("imageProduct", product.thumbId)
                    intent.putExtra("productExtra", bundle)
                    val options: ActivityOptionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(context, itemView.imageProduct, "imageProduct")
                    context.startActivity(intent, options.toBundle())
                }

            }
            checkStatusImageCart()
            itemView.productName.text = product.name
            itemView.priceProduct.text = Utils.currency(product.mainPrice?.kDBanBuonGia ?: 0)
            itemView.imageProduct.imageProduct.loadImage(product.thumbId)

            itemView.cvBanBuon.text = "Bán buôn: ${product.mainPrice?.kdcv ?: 0} CV"
            itemView.cvBanLe.text = "Bán lẻ: ${product.mainPrice?.khcv ?: 0} CV"
            itemView.coin.text = "Coin: ${product.mainPrice?.coint ?: 0}"
        }
    }
}