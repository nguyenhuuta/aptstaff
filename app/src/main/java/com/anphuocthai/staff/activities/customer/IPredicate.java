package com.anphuocthai.staff.activities.customer;

/**
 * Created by OpenYourEyes on 5/11/21
 */
interface IPredicate<T> {
    boolean condition(T model);
}
