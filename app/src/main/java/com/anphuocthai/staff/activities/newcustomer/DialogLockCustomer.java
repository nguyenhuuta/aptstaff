package com.anphuocthai.staff.activities.newcustomer;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.base.BaseDialogFragment;

/**
 * Created by DaiKySy on 8/29/19.
 */
public class DialogLockCustomer extends BaseDialogFragment {


    public static final String ARG_CUSTOMER_NAME = "arg_custom_name";
    public static final String ARG_CUSTOMER_ID = "arg_custom_id";

    private EditText mInputReason;
    private IDialogLockCustomer mCallback;
    private int mCustomerId;

    public static DialogLockCustomer newInstance(Bundle args) {
        DialogLockCustomer fragment = new DialogLockCustomer();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getContentView() {
        return R.layout.dialog_delete_user;
    }

    @Override
    public String getTextButtonOK() {
        return "Khoá";
    }

    @Override
    public void initView(View view) {
        mInputReason = view.findViewById(R.id.inputReason);
        mButtonOK.setBackgroundResource(R.drawable.border_button_red);
    }

    @Override
    public void initAction() {
    }

    @Override
    public void initData() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            String customerName = bundle.getString(ARG_CUSTOMER_NAME);
            setTitleDialog(customerName);
            mCustomerId = bundle.getInt(ARG_CUSTOMER_ID);
        }
    }

    @Override
    public void onButtonOKClick() {
        if (mCallback != null) {
            mCallback.onLock(mCustomerId, mInputReason.getText().toString());
        }
        dismiss();
    }

    public void setmCallback(IDialogLockCustomer mCallback) {
        this.mCallback = mCallback;
    }

    public interface IDialogLockCustomer {
        void onLock(int customerId, String reason);
    }
}
