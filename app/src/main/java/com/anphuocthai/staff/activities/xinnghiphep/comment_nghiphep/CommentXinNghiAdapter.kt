package com.anphuocthai.staff.activities.xinnghiphep.comment_nghiphep

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.anphuocthai.staff.BR
import com.anphuocthai.staff.R
import com.anphuocthai.staff.adapters.BaseViewHolder
import com.anphuocthai.staff.model.CommentXinNghiModel

/**
 * Created by OpenYourEyes on 3/17/21
 */
class CommentXinNghiAdapter(
        context: Context
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val TYPE_ME_COMMENT = 1
    private val TYPE_NOT_ME = 2
    private val TYPE_LINE = 3


    private val layoutInflater: LayoutInflater = LayoutInflater.from(context)
    private var mList = mutableListOf<CommentXinNghiModel>()
    private lateinit var recyclerView: RecyclerView


    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    fun updateList(list: MutableList<CommentXinNghiModel>) {
        mList = list
        notifyDataSetChanged()
    }

    fun insertItem(model: CommentXinNghiModel) {
        mList.add(0, model)
        notifyItemInserted(0)
        val layoutManager = recyclerView.layoutManager as? LinearLayoutManager
        layoutManager?.let {
            if (it.findFirstCompletelyVisibleItemPosition() == 0) {
                it.scrollToPosition(0);
            }
        }

    }

    override fun getItemViewType(position: Int): Int {
        val model = getItem(position)
        return model.time?.let {
            val isMeComment = model.isMeComment ?: false
            if (isMeComment) {
                TYPE_ME_COMMENT
            } else {
                TYPE_NOT_ME
            }
        } ?: kotlin.run {
            TYPE_LINE
        }

    }

    private fun getItem(position: Int): CommentXinNghiModel {
        return mList[position]
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == TYPE_ME_COMMENT) {
            val rightView: ViewDataBinding =
                    DataBindingUtil.inflate(layoutInflater, R.layout.item_commnet_xin_nghi_right, parent, false)
            return MeCommentVH(rightView)
        }
        if (viewType == TYPE_LINE) {
            val rightView: ViewDataBinding =
                    DataBindingUtil.inflate(layoutInflater, R.layout.item_comment_line, parent, false)
            return MeCommentVH(rightView)
        }

        val leftView: ViewDataBinding =
                DataBindingUtil.inflate(layoutInflater, R.layout.item_comment_xin_nghi_left, parent, false)

        return BaseViewHolder<CommentXinNghiModel>(leftView, null)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val model = getItem(position)
        if (holder is MeCommentVH) {
            holder.bindData(model)
        } else {
            (holder as BaseViewHolder<CommentXinNghiModel>).bindData(model)
        }
    }

    override fun getItemCount(): Int {
        return mList.size
    }

}


open class MeCommentVH(
        private val rootView: ViewDataBinding) :
        RecyclerView.ViewHolder(rootView.root) {

    fun bindData(model: CommentXinNghiModel) {
        rootView.setVariable(BR.model, model)
        rootView.executePendingBindings()
    }
}
