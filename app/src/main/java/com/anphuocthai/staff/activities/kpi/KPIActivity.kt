package com.anphuocthai.staff.activities.kpi

import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.anphuocthai.staff.R
import com.anphuocthai.staff.activities.BaseActivityBinding
import com.anphuocthai.staff.databinding.KpiActivityBinding
import com.anphuocthai.staff.model.KPIModel
import com.anphuocthai.staff.utils.DateConvert
import java.util.*

/**
 * Created by OpenYourEyes on 3/22/21
 */
class KPIActivity : BaseActivityBinding<KPIViewModel, KpiActivityBinding>() {
    override fun createViewModel(): Class<KPIViewModel> {
        return KPIViewModel::class.java
    }

    override fun layoutId(): Int = R.layout.kpi_activity

    override fun titleToolBar(): String {
        return "KPI"
    }

    override fun initView() {
        initToolbar(dataBinding.toolbar)
        val calendar = viewModel.calendar

        with(dataBinding.headerKPI) {
            model = KPIModel.toModel("Header", KPIModel.positionHeader)
        }
        val now = DateConvert.convertDate(calendar.timeInMillis, DateConvert.TypeConvert.MONTH_YEAR)
        fun calculatorTime(isNext: Boolean): Boolean {
            if (isNext) {
                calendar.add(Calendar.MONTH, 1)
            } else {
                calendar.add(Calendar.MONTH, -1)
            }
            val time = DateConvert.convertDate(calendar.timeInMillis, DateConvert.TypeConvert.MONTH_YEAR)
            dataBinding.dateTitle.text = "Tháng $time"
            viewModel.getKpi()
            return time == now
        }

        dataBinding.buttonPrev.setOnClickListener {
            dataBinding.buttonNext.visibility = View.VISIBLE
            calculatorTime(false)

        }
        dataBinding.buttonNext.setOnClickListener {
            val isNow = calculatorTime(true)
            if (isNow) {
                it.visibility = View.INVISIBLE
            }
        }
        dataBinding.dateTitle.text = "Tháng $now"
        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        dataBinding.recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@KPIActivity)
            adapter = KPIAdapter(this@KPIActivity)
        }
    }

    override fun bindViewModel() {

        viewModel.listKPI.observe(this, androidx.lifecycle.Observer {


            it.getOrNull(1)?.let { model ->
                val text = "${dataBinding.dateTitle.text} (${model.ketqua})"
                dataBinding.dateTitle.text = text
            }

            val adapter = dataBinding.recyclerView.adapter as KPIAdapter
            adapter.updateList(it)
        })

        viewModel.getKpi()
    }
}