package com.anphuocthai.staff.activities.detaildebtactivity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.ui.base.BaseViewHolder;
import com.anphuocthai.staff.ui.delivery.debt.inventorydebt.InventoryDebtAdapter;
import com.anphuocthai.staff.ui.delivery.model.Hanghoa;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;
import com.anphuocthai.staff.utils.Enum;
import com.anphuocthai.staff.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.anphuocthai.staff.utils.Enum.FieldValueType.NORMAL;

public class DetailDebtAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private InventoryDebtAdapter.Callback mCallback;
    private List<OrderTran> orderTranArrayList;


    public DetailDebtAdapter(ArrayList<OrderTran> orderTranArrayList) {
        this.orderTranArrayList = orderTranArrayList;
    }

    public void setCallback(InventoryDebtAdapter.Callback mCallback) {
        this.mCallback = mCallback;
    }


    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new DetailDebtAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cong_no_ton, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);

    }

    @Override
    public int getItemCount() {
        if (!Utils.isNull(orderTranArrayList)) {
            return orderTranArrayList.size();
        }
        return 0;
    }

    public void addItems(List<OrderTran> orderTrans) {
        orderTranArrayList.clear();
        orderTranArrayList.addAll(orderTrans);
        notifyDataSetChanged();
    }


    public List<OrderTran> getOrderTranArrayList() {
        return orderTranArrayList;
    }

    public class ViewHolder extends BaseViewHolder {
        private CheckBox mCheckBox;
        TextView txtOrderCode;
        TextView txtCustomerName;

        TextView labelSumaryMoney;
        TextView txtSumaryMoney;

        TextView labelMustPay;
        TextView txtMustPay;

        TextView labelRemain;
        TextView txtRemain;

        public ViewHolder(View itemView) {
            super(itemView);
            initView(itemView);
        }

        private void initView(View itemView) {
            mCheckBox = itemView.findViewById(R.id.checkBox);
            txtOrderCode = itemView.findViewById(R.id.all_debt_txt_order_code);
            txtCustomerName = itemView.findViewById(R.id.all_debt_txt_order_customer_name);
            labelSumaryMoney = itemView.findViewById(R.id.debt_label_sumary_money);
            txtSumaryMoney = itemView.findViewById(R.id.debt_txt_sumary_money);
            labelMustPay = itemView.findViewById(R.id.debt_label_must_pay);
            txtMustPay = itemView.findViewById(R.id.debt_txt_must_pay);
            labelRemain = itemView.findViewById(R.id.debt_label_remain);
            txtRemain = itemView.findViewById(R.id.debt_txt_remain);
            mCheckBox.setVisibility(View.GONE);

        }

        protected void clear() {
        }

        public void onBind(int position) {
            super.onBind(position);
            final OrderTran orderTran = orderTranArrayList.get(position);

            if (orderTran.getMaDonDat() != null) {
                if (orderTran.getNgayKetThuc() != null) {
                    txtOrderCode.setText("ĐH số : " + orderTran.getMaDonDat() + " - " + Utils.formatDate(orderTran.getNgayKetThuc().toString(), false));
                } else {
                    txtOrderCode.setText("ĐH số : " + orderTran.getMaDonDat() + " - " + Utils.formatDate(orderTran.getNgayBatDau(), false));
                }
            }
            if (Utils.isNull(orderTran.getContentDetail())) {
                String content = "";
                if (orderTran.getTenThanhVien() != null) {
                    content = orderTran.getTenThanhVien();
                }
                if (orderTran.getVanchuyens() != null) {
                    int size = orderTran.getVanchuyens().size();
                    if (size > 0) {
                        if (orderTran.getVanchuyens().get(0).getDiaChiNhanHang() != null) {
                            content += "\n" + String.format(orderTran.getVanchuyens().get(0).getDiaChiNhanHang());
                        }
                    }
                }

                if (orderTran.getHanghoas() != null) {
                    int size = orderTran.getHanghoas().size();
                    if (size > 0) {
                        String goodsNames = new String();
                        for (int i = 0; i < size; i++) {
                            if (orderTran.getHanghoas().get(i).getHanghoa().getTenHangHoa() != null) {
                                Hanghoa hanghoa = orderTran.getHanghoas().get(i);
                                if ((i == 0 && (size == 1)) || i == size - 1) {
                                    goodsNames += hanghoa.getHanghoa().getTenHangHoa() + "(" + Utils.formatValue(hanghoa.getSoLuong(), NORMAL) + hanghoa.getDonVi() + ")";
                                } else {
                                    goodsNames += hanghoa.getHanghoa().getTenHangHoa() + "(" + Utils.formatValue(hanghoa.getSoLuong(), NORMAL) + hanghoa.getDonVi() + ")" + "\n";
                                }

                            }
                        }
                        content += "\n" + goodsNames;
                    }
                }
                orderTran.setContentDetail(content);
            }

            txtCustomerName.setText(orderTran.getContentDetail());

            txtSumaryMoney.setText("");
            if (orderTran.getTongGiaTri() != null) {
                txtSumaryMoney.setText(Utils.formatValue(orderTran.getTongGiaTri(), Enum.FieldValueType.CURRENCY));
            }
            txtMustPay.setText("");
            if (orderTran.getTongThanhToan() != null) {
                txtMustPay.setText(Utils.formatValue(orderTran.getTongThanhToan(), Enum.FieldValueType.CURRENCY));
            }

            txtRemain.setText("");
            if (orderTran.getPhaiThanhToan() != null) {
                txtRemain.setText(Utils.formatValue(orderTran.getPhaiThanhToan(), Enum.FieldValueType.CURRENCY));
            }
        }
    }
}
