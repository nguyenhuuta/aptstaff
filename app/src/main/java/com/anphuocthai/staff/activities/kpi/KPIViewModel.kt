package com.anphuocthai.staff.activities.kpi

import androidx.lifecycle.viewModelScope
import com.anphuocthai.staff.activities.BaseViewModel
import com.anphuocthai.staff.model.KPIModel
import com.anphuocthai.staff.utils.extension.ListLiveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import java.util.*

/**
 * Created by OpenYourEyes on 3/22/21
 */
class KPIViewModel : BaseViewModel() {

    val listKPI = ListLiveData<KPIModel>()

    val calendar: Calendar by lazy {
        Calendar.getInstance()
    }

    fun getKpi() {


        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH) + 1
        viewModelScope.launch {
            executeRequestFlow(request = {
                it.getKPI(dataManager.userInfoId, month, year)
            }).map {
                val map = it.groupBy { model ->
                    model.tenNhomChiTieu
                }
                val listNew = mutableListOf<KPIModel>()
                for ((key, list) in map) {
                    list.mapIndexed { index, kpiModel -> kpiModel.position = index + 1 }
                    listNew.add(KPIModel.toModel(key, KPIModel.positionTitle))
                    listNew.addAll(list)
                }
                listNew
            }.flowOn(Dispatchers.IO).collect {
                listKPI.postValue(it)
            }
        }

    }

}