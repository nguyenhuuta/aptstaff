package com.anphuocthai.staff.activities;

import android.os.Bundle;

import com.anphuocthai.staff.fragments.BaseFragment;
import com.anphuocthai.staff.fragments.FragmentId;

public interface IActivityCallback {
    void displayScreen(FragmentId screenId, boolean... addToBackStack);

    void onFragmentResumed(BaseFragment fragment);

    void setAppBarTitle(String title);

    void onBackPressed(Bundle bundle);

}
