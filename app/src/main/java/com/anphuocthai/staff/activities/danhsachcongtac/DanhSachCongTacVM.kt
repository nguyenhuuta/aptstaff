package com.anphuocthai.staff.activities.danhsachcongtac

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.anphuocthai.staff.R
import com.anphuocthai.staff.activities.BaseViewModel
import com.anphuocthai.staff.api.runOnMain
import com.anphuocthai.staff.data.address.DatabaseManager
import com.anphuocthai.staff.data.trackProgressBar
import com.anphuocthai.staff.model.ChiTietCongTacModel
import com.anphuocthai.staff.model.CongTacModel
import com.anphuocthai.staff.model.InsertCongTacBody
import com.anphuocthai.staff.utils.printLog
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import java.util.*

/**
 * Created by OpenYourEyes on 12/28/20
 */

fun <T : Any?> MutableLiveData<T>.default(initialValue: T) = apply { setValue(initialValue) }
class DanhSachCongTacVM : BaseViewModel() {
    val listDanhSach = MutableLiveData<MutableList<CongTacModel>>()


    var DuLieuChamCongId: Int = 0
    var ThoiGianChamCong: String = ""
    val detailDanhSach = MutableLiveData<MutableList<ChiTietCongTacModel>>()
    var insertItem = MutableLiveData<ChiTietCongTacModel>()
    val colorButton = MutableLiveData<Int>().default(R.color.colorTextDisable)

    val textChange = MutableLiveData<String>()
    val isEnable: LiveData<Boolean> = Transformations.map(textChange) {
        colorButton.value = if (it.isEmpty()) R.color.colorTextDisable else R.color.colorPrimary
        it.isNotEmpty()
    }

    fun getDanhSach() {
        val calendar = Calendar.getInstance()
        val maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH)
        val month = calendar.get(Calendar.MONTH) + 1
        val currentMonth = String.format("%02d", month)
        val year = calendar.get(Calendar.YEAR)
        val time = "$currentMonth/$year"
        val fromDate = "01/${time}"
        val toDate = "$maxDay/${time}"


        val queryLocal = dataManager.queryCongTacByMonth(month).share();
        val fromServer = dataManager.laydanhSachCongTac(fromDate, toDate)
                .trackProgressBar(loading)
                .filter {
                    it.isSuccess
                }.map {
                    val list = it.data ?: mutableListOf()
                    list.forEach { model ->
                        model.month = month
                    }
                    list
                }

        Observable.combineLatest(queryLocal,
                fromServer,
                BiFunction<MutableList<CongTacModel>, MutableList<CongTacModel>, MutableList<CongTacModel>> { t1, t2 ->
                    syncData(t1, t2, month)
                }).runOnMain()
                .subscribe {
                    printLog("runOnMain ${Thread.currentThread().name}")
                    listDanhSach.value = it
                }.addToDisposable()
    }

    private fun syncData(local: MutableList<CongTacModel>, servers: MutableList<CongTacModel>, currentMonth: Int): MutableList<CongTacModel> {
        servers.reverse()
        printLog("syncData ${Thread.currentThread().name}")
        fun insertDatabase(list: MutableList<CongTacModel>) {
            printLog("insertDatabase ${list.size}")
            if (list.size == 0) return
            dataManager.themCongTacs(list)
        }

        if (local.size == 0) {
            insertDatabase(servers)
            return servers
        }

        val listNeedInsert = mutableListOf<CongTacModel>()
        for (modelServer in servers) {
            modelServer.month = currentMonth
            val idServer = modelServer.id ?: 0
            val modelLocal = local.firstOrNull { it.id == idServer }
            if (modelLocal == null) {
                listNeedInsert.add(modelServer)
                continue
            }
            val idLocal = modelLocal.id ?: 0
            if (idServer > 0 && idLocal == idLocal) {
                modelServer.hasNew = modelServer.totalYeuCau != modelLocal.totalYeuCau
            }
        }
        insertDatabase(listNeedInsert)
        return servers
    }

    fun updateCongTac(congTacModel: CongTacModel) {
        DatabaseManager.getInstance().CongTacDAO().capNhatCongTacs(congTacModel)
    }


    fun chiTietDanhSach() {
        dataManager.chiTietCongTac(DuLieuChamCongId)
                .trackProgressBar(loading)
                .runOnMain()
                .subscribe {
                    it.data?.let { list ->
                        list.reverse()
                        detailDanhSach.value = list
                    }
                }.addToDisposable()
    }

    fun buttonSent(view: View) {
        val message = textChange.value ?: return
        val body = InsertCongTacBody(DuLieuChamCongId, message, ThoiGianChamCong)
        dataManager.inserCongTac(body)
                .runOnMain()
                .trackProgressBar(loading)
                .subscribe({
                    if (it.isSuccess()) {
                        DatabaseManager.getInstance().CongTacDAO().updateNumber(DuLieuChamCongId)
                        insertItem.value = it.objectInfo
                        textChange.value = ""
                    }
                }, { t: Throwable? ->

                })
                .addToDisposable()

    }
}