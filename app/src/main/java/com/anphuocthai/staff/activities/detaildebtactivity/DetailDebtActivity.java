package com.anphuocthai.staff.activities.detaildebtactivity;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.activities.BaseActivities;
import com.anphuocthai.staff.base.actionbar.CustomActionBar;
import com.anphuocthai.staff.baserequest.APICallback;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;
import com.anphuocthai.staff.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class DetailDebtActivity extends BaseActivities {
    public static final String ARG_USER_ID = "userId";
    public static final String ARG_USER_NAME = "userName";
    private CustomActionBar mCustomActionBar;
    private RecyclerView mDetailDebtRecycleView;
    private DetailDebtAdapter mAdapter;

    @Override
    public int getContentView() {
        return R.layout.activity_detail_deb;
    }

    @Override
    public void initView() {
        mCustomActionBar = findViewById(R.id.customActionBar);
        mDetailDebtRecycleView = findViewById(R.id.detailDebRecyclerView);


    }

    @Override
    public void initAction() {
        mCustomActionBar.setCustomActionBar(new CustomActionBar.ICustomActionBar() {
            @Override
            public void onButtonLeftClick() {
                finish();
            }

            @Override
            public void onButtonRightClick() {

            }
        });
    }

    @Override
    public void initData() {
        mCustomActionBar.setTitle("Chi tiết công nợ");
        mCustomActionBar.showLeftRightButton(true, false);
        mAdapter = new DetailDebtAdapter(new ArrayList<>());
        mDetailDebtRecycleView.setLayoutManager(new LinearLayoutManager(this));
        mDetailDebtRecycleView.setAdapter(mAdapter);
        if (getIntent() != null && getIntent().getExtras() != null) {
            int userId = getIntent().getExtras().getInt(ARG_USER_ID, 0);
            String tenThanhVien = getIntent().getExtras().getString(ARG_USER_NAME, "");
            mCustomActionBar.setTitle(tenThanhVien);
            if (userId == 0) return;

            showProgress();
            mAPI.getDetailDeb(new DebtParams(userId)).getAsyncResponse(new APICallback<List<OrderTran>>() {
                @Override
                public void onSuccess(List<OrderTran> orderTrans) {
                    hideProgress();
                    if (!Utils.isNull(orderTrans)) {
                        mAdapter.addItems(orderTrans);
                    }
                }

                @Override
                public void onFailure(String errorMessage) {
                    hideProgress();
                }
            });
        }
    }
}
