package com.anphuocthai.staff.activities.customer;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.error.ANError;
import com.anphuocthai.staff.R;
import com.anphuocthai.staff.api.ApiURL;
import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.interfaces.IAPICallback;
import com.anphuocthai.staff.model.ResponseMessage;
import com.anphuocthai.staff.model.customer.CustomerAddress;
import com.anphuocthai.staff.ui.base.BaseActivity;
import com.anphuocthai.staff.utils.App;
import com.anphuocthai.staff.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import static com.anphuocthai.staff.utils.Constants.REQUEST_LAT_LONG_CREATE_NEW_ADDRESS_CODE;

public class AddOrUpdateAddressActivity extends BaseActivity {

    private static final String TAG = AddOrUpdateAddressActivity.class.getSimpleName();
    //private Toolbar toolbar;
    private Button btnOk;
    private EditText edtCustomerAddress;
    private TextView txtSelectInMap;
    private Double latitude;
    private Double longitude;
    private String addressName;
    private String guid;
    private boolean isAddNew = false;
    private String idAddress;

    private CustomerAddress customerAddress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_or_update_address);
        handleIntent();
        customToolBar();
        addControls();
        addEvents();
    }

    private void handleIntent() {
        Bundle b = getIntent().getBundleExtra("addresscustomerfragment");
        this.guid = b.getString("guid");
        this.isAddNew = b.getBoolean("isAddNew");
        if (!isAddNew) {
            idAddress = b.getString("idAddress");
            addressName = b.getString("addressName");
            customerAddress = (CustomerAddress) b.getSerializable("address");
        } else {
            idAddress = "0";
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        edtCustomerAddress.clearFocus();
        Utils.hideKeyboard(this);
        return super.dispatchTouchEvent(ev);
    }

    private void customToolBar() {
        App.setContext(this);
        if (isAddNew) {
            setupToolbar(getString(R.string.ci_add_new_address_customer));

        }else {
            setupToolbar(getString(R.string.ci_update_address_customer));
        }
    }

    private void addControls() {
        btnOk = findViewById(R.id.btn_accept_address_customer);
        edtCustomerAddress = findViewById(R.id.edt_customer_add_new_or_update_address);
        txtSelectInMap = findViewById(R.id.ci_txt_select_in_map);
        if (!isAddNew) {
            edtCustomerAddress.setText(addressName);
        }
    }
    private void addEvents(){
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!validate()) {
                    return;
                }
                if (latitude == null | longitude == null) {
                    new GetLatLongByAdressAsynchTask().execute(edtCustomerAddress.getText().toString());
                }else {
                    sendRequestAddNewAddress();
                }

            }
        });
        txtSelectInMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });

        edtCustomerAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_LAT_LONG_CREATE_NEW_ADDRESS_CODE) {
        }
    }

    private JSONObject createBodyParam(){
        JSONObject addressObject = new JSONObject();
        try {
            addressObject.put("id", idAddress); //them moi
            addressObject.put("guidThanhVien", this.guid);
            addressObject.put("diaChi", edtCustomerAddress.getText().toString());
            addressObject.put("tinhId", "1");
            addressObject.put("latitude", String.valueOf(this.latitude));
            addressObject.put("longitude", String.valueOf(this.longitude));
            addressObject.put("isChinh", "true");
        }catch (Exception e) {
            e.printStackTrace();
        }
        return addressObject;
    }

    private void sendRequestAddNewAddress() {
        showLoading();
        NetworkManager.getInstance().sendPostRequest(ApiURL.UPDATE_ADDRESS_URL, createBodyParam(), new IAPICallback() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                Log.d(TAG, response.toString());
                if (response != null) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<ResponseMessage>() {
                    }.getType();
                    ResponseMessage result = gson.fromJson(response.toString(), type);
//                    Toast.makeText(getBaseContext(), result.getMessage(), Toast.LENGTH_SHORT).show();
                    finish();
                    hideLoading();
                }
            }
            @Override
            public void onResponseSuccess(JSONArray response) {}

            @Override
            public void onResponseError(ANError anError) {
                Toast.makeText(getBaseContext(), R.string.have_error, Toast.LENGTH_SHORT).show();
                hideLoading();
            }
        });
    }

    public boolean validate() {
        boolean valid = true;

        String address = edtCustomerAddress.getText().toString();

        if (address.isEmpty()) {
            edtCustomerAddress.setError(getString(R.string.ci_add_new_address_empty_address));
            valid = false;
        }
        else {
            edtCustomerAddress.setError(null);
        }
        return valid;
    }


    private class GetLatLongByAdressAsynchTask extends AsyncTask<String, Void, String[]> {

        ProgressDialog dialog = new ProgressDialog(AddOrUpdateAddressActivity.this, R.style.AppTheme_Dark_Dialog);
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String[] doInBackground(String... params) {
            String response;
            try {
                response = getLatLongByURL("http://maps.google.com/maps/api/geocode/json?address=mumbai&sensor=false");
                Log.d("response",""+response);
                return new String[]{response};
            } catch (Exception e) {
                return new String[]{"error"};
            }
        }

        @Override
        protected void onPostExecute(String... result) {
            try {
                JSONObject jsonObject = new JSONObject(result[0]);

                double lng = ((JSONArray)jsonObject.get("results")).getJSONObject(0)
                        .getJSONObject("geometry").getJSONObject("location")
                        .getDouble("lng");

                double lat = ((JSONArray)jsonObject.get("results")).getJSONObject(0)
                        .getJSONObject("geometry").getJSONObject("location")
                        .getDouble("lat");

                latitude = lat;
                longitude = lng;

                Log.d("latitude", "" + lat);
                Log.d("longitude", "" + lng);
                sendRequestAddNewAddress();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            //Utils.showProgressDialog(AddNewCustomerActivity.this, false);
//            if (dialog.isShowing()) {
//                dialog.dismiss();
//            }
        }
    }


    public String getLatLongByURL(String requestURL) {
        URL url;
        String response = "";
        try {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");
            conn.setDoOutput(true);
            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

}
