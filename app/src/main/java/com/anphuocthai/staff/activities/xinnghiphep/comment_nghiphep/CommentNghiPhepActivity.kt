package com.anphuocthai.staff.activities.xinnghiphep.comment_nghiphep

import android.widget.ArrayAdapter
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.anphuocthai.staff.R
import com.anphuocthai.staff.activities.BaseActivityBinding
import com.anphuocthai.staff.base.actionbar.CustomActionBar
import com.anphuocthai.staff.databinding.CommentXinNghiActivityBinding
import com.anphuocthai.staff.model.NhanVienPhongBanModel
import kotlinx.android.synthetic.main.custom_toolbar.view.*


/**
 * Created by OpenYourEyes on 3/15/21
 */
class CommentNghiPhepActivity : BaseActivityBinding<CommentNghiPhepVM, CommentXinNghiActivityBinding>() {
    companion object {
        const val arg_phieu_nghi_id = "PhieuNghiId"
    }

    override fun createViewModel(): Class<CommentNghiPhepVM> = CommentNghiPhepVM::class.java

    override fun layoutId(): Int = R.layout.comment_xin_nghi_activity

    override fun titleToolBar(): String = "Chi tiết xin nghỉ"


    override fun initView() {
        initToolbar(dataBinding.toolbar as CustomActionBar)
        dataBinding.recyclerComment.apply {
            setHasFixedSize(true)
            val layoutManager = LinearLayoutManager(this@CommentNghiPhepActivity, LinearLayoutManager.VERTICAL, true)
            this.layoutManager = layoutManager
            adapter = CommentXinNghiAdapter(this@CommentNghiPhepActivity)
        }

        dataBinding.spinner.apply {
            setOnItemClickListener { parent, view, position, id ->
                val model = parent.adapter.getItem(position) as NhanVienPhongBanModel
                viewModel.indexSeletedNhanVien = model
            }
            setOnFocusChangeListener { v, hasFocus ->
                if (hasFocus) {
                    this.showDropDown()
                }
            }

            setOnTouchListener { v, event ->
                this.showDropDown()
                false
            }

        }
    }

    override fun bindViewModel() {

        intent?.let {
            val id = intent.getIntExtra(arg_phieu_nghi_id, 0)
            viewModel.danhSachNhanVienPhongBan()
            viewModel.layNoiDungTraoDoi(phieuNghiId = id)
        }

        viewModel.listComment.observe(this, Observer {
            val adapter = dataBinding.recyclerComment.adapter as CommentXinNghiAdapter
            adapter.updateList(it)
            dataBinding.recyclerComment.post {
                dataBinding.recyclerComment.scrollToPosition(0)
            }
        })


        viewModel.insertComment.observe(this, Observer {
            val adapter = dataBinding.recyclerComment.adapter as CommentXinNghiAdapter
            adapter.insertItem(it)
        })

        viewModel.danhSachNhanVienPhongBan.observe(this, Observer {
            val adapter = ArrayAdapter<NhanVienPhongBanModel>(this@CommentNghiPhepActivity, android.R.layout.simple_list_item_1, it)
            dataBinding.spinner.setAdapter(adapter)
        })
    }
}