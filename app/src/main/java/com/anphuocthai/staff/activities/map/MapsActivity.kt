package com.anphuocthai.staff.activities.map

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.hardware.camera2.CameraCharacteristics
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.os.Looper
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.transition.Slide
import androidx.transition.TransitionManager
import com.androidnetworking.common.Priority
import com.anphuocthai.staff.R
import com.anphuocthai.staff.api.ApiURL
import com.anphuocthai.staff.model.ChamCongModel
import com.anphuocthai.staff.model.ChamCongResponse
import com.anphuocthai.staff.model.DetailTimeKeepingResponse
import com.anphuocthai.staff.ui.base.BaseActivity
import com.anphuocthai.staff.utils.App
import com.anphuocthai.staff.utils.DateConvert
import com.anphuocthai.staff.utils.Utils
import com.anphuocthai.staff.utils.printLog
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.rx2androidnetworking.Rx2AndroidNetworking
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_maps.*
import org.json.JSONException
import org.json.JSONObject
import java.util.*


class MapsActivity : BaseActivity(), OnMapReadyCallback {
    companion object {
        const val MY_PERMISSIONS_REQUEST_LOCATION = 99
        const val MY_CAMERA_PERMISSION_CODE = 100
        const val CAMERA_REQUEST = 101
        const val radiusInMeters = 100.0 //100 met
        const val UPDATE_INTERVAL_IN_MILLISECONDS = 10000L // 10s reload
        val anPhuocThaiLocation: LatLng = LatLng(20.96027085194506, 105.81683300435543)


        /**
         * Tính khoảng cách giữa 2 điểm (met)
         */
        fun isNearCompany(currentMyLocation: LatLng): Boolean {

            val distanceMeter = currentMyLocation.let {
                val distance = FloatArray(2)
                Location.distanceBetween(it.latitude, it.longitude, anPhuocThaiLocation.latitude, anPhuocThaiLocation.longitude, distance)
                val dist = distance[0]
                dist
            }
            printLog("distance $distanceMeter")
            return distanceMeter <= radiusInMeters
        }

    }

    private lateinit var googleMap: GoogleMap
    private lateinit var locationRequest: LocationRequest
    private lateinit var currentMyLocation: LatLng
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    private val cameraZoom: Float = 14.0F
    private var polyline: Polyline? = null

    lateinit var mapFragment: SupportMapFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        setupStandardBottomSheet()
        val params = statusBar.layoutParams.apply {
            height = getStatusBarHeight()
        }
        statusBar.layoutParams = params
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
        }
        initLocation()

        mapFragment = (supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?)!!
        mapFragment.getMapAsync(this)

        initAction()


    }

    private var typeChamCong = 1
    private fun showLayoutChamCong() {

        fun showAnim(isSHow: Boolean) {
            val slide = Slide()
            if (isSHow) {
                slide.slideEdge = Gravity.RIGHT
                TransitionManager.beginDelayedTransition(layoutChamcong, slide)
                chamCongDiaDiemKhac.visibility = View.VISIBLE
                chamCongTaiCongTy.visibility = View.VISIBLE
            } else {
                slide.slideEdge = Gravity.RIGHT
                TransitionManager.beginDelayedTransition(layoutChamcong, slide)
                chamCongDiaDiemKhac.visibility = View.INVISIBLE
                chamCongTaiCongTy.visibility = View.INVISIBLE
            }
        }

        if (chamCongDiaDiemKhac.visibility != View.VISIBLE) {
            showAnim(true)
        } else {
            showAnim(false)
        }
    }

    private fun initAction() {
        fun startCamera() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    checkCameraPermission()
                    return
                }
            }
            val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            cameraIntent.putExtra("android.intent.extras.CAMERA_FACING", 1);
            when {
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1 && Build.VERSION.SDK_INT < Build.VERSION_CODES.O -> {
                    cameraIntent.putExtra("android.intent.extras.CAMERA_FACING", CameraCharacteristics.LENS_FACING_FRONT)  // Tested on API 24 Android version 7.0(Samsung S6)
                }
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.O -> {
                    cameraIntent.putExtra("android.intent.extras.CAMERA_FACING", CameraCharacteristics.LENS_FACING_FRONT) // Tested on API 27 Android version 8.0(Nexus 6P)
                    cameraIntent.putExtra("android.intent.extra.USE_FRONT_CAMERA", true)
                }
                else -> cameraIntent.putExtra("android.intent.extras.CAMERA_FACING", 1)  // Tested API 21 Android version 5.0.1(Samsung S4)
            }
            startActivityForResult(cameraIntent, CAMERA_REQUEST)
        }

        buttonChamCongTaiCongTy.setOnClickListener {
            typeChamCong = 1
            startCamera()
        }

        buttonChamCongDiaDiemKhac.setOnClickListener {
            typeChamCong = 0
            startCamera()
        }

        buttonChamCong.setOnClickListener {
            showLayoutChamCong()
        }
        initRecyclerView()
        buttonBack.setOnClickListener {
            finish()
        }
        buttonMyLocation.setOnClickListener {
            if (::googleMap.isInitialized) {
                googleMap.moveTo()
            }
        }
        detailTimeKeeping.setOnClickListener {
            standardBottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        }
    }

    private fun initLocation() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        locationRequest = LocationRequest()
        locationRequest.interval = UPDATE_INTERVAL_IN_MILLISECONDS
        locationRequest.fastestInterval = UPDATE_INTERVAL_IN_MILLISECONDS
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    private fun getBitmapFromVectorDrawable(drawableId: Int): BitmapDescriptor {
        var drawable = ContextCompat.getDrawable(this, drawableId)
        var bitmap: Bitmap
        try {
            bitmap = drawable?.let {
                val bitmap = Bitmap.createBitmap(it.intrinsicWidth,
                        it.intrinsicHeight, Bitmap.Config.ARGB_8888)
                val canvas = Canvas(bitmap)
                drawable.setBounds(0, 0, canvas.width, canvas.height)
                drawable.draw(canvas)
                bitmap
            } ?: kotlin.run {
                val b = BitmapFactory.decodeResource(resources, R.drawable.ic_apt)
                val smallMarker = Bitmap.createScaledBitmap(b, 500, 500, false)
                smallMarker
            }
        } catch (ignore: Exception) {
            val b = BitmapFactory.decodeResource(resources, R.drawable.ic_apt)
            bitmap = Bitmap.createScaledBitmap(b, 500, 500, false)
        }
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        printLog(this, "onMapReady")
        this.googleMap = googleMap
        googleMap.addMarker(MarkerOptions()
                .position(anPhuocThaiLocation)
                .icon(getBitmapFromVectorDrawable(R.drawable.ic_anphuocthai)))

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        fusedLocationClient.lastLocation
                .addOnSuccessListener { location: Location? ->
                    location?.let {
                        currentMyLocation = LatLng(it.latitude, it.longitude)
                        googleMap.moveTo()
                    }
                }
        requestPermissionLocation()
        googleMap.setInfoWindowAdapter(object : InfoWindowAdapter {
            override fun getInfoContents(p0: Marker?): View {
                return LayoutInflater.from(this@MapsActivity).inflate(R.layout.item_map_cham_cong, null)
            }

            override fun getInfoWindow(p0: Marker?): View? {
                return null
            }
        })
        googleMap.addCircle(CircleOptions()
                .center(anPhuocThaiLocation)
                .radius(radiusInMeters)
                .strokeColor(ContextCompat.getColor(this@MapsActivity, R.color.colorBlue))
                .fillColor(ContextCompat.getColor(this@MapsActivity, R.color.colorBlueTransparent)))
    }

    private fun trackingLocation() {
        googleMap.apply {
            uiSettings.isMyLocationButtonEnabled = false
            isMyLocationEnabled = true
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return
        }
        fusedLocationClient.requestLocationUpdates(locationRequest, mLocationCallback, Looper.myLooper())
    }

    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            val locationList = locationResult.locations
            if (locationList.isNotEmpty()) {
                val location = locationList.last()
                currentMyLocation = LatLng(location.latitude, location.longitude)
                anPhuocThaiLocation()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)
        intent?.let { data ->
            when (requestCode) {
                CAMERA_REQUEST -> if (resultCode == Activity.RESULT_OK && data.hasExtra("data")) {
                    val bitmap = data.extras?.get("data") as Bitmap
                    if (typeChamCong == 0) {
                        showDialogNote(bitmap)
                    } else {
                        requestChamCong(bitmap)
                    }
                }
            }
        }
    }

    private fun showDialogNote(bitmap: Bitmap) {
        dialogNote.visibility = View.VISIBLE
        avatar.setImageBitmap(bitmap)
        buttonCancel.setOnClickListener {
            dialogNote.visibility = View.GONE
            val imm: InputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(inputNote.windowToken, 0)
        }

        inputNote.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(text: Editable?) {
                val value = text?.toString() ?: ""
                if (value.isEmpty()) {
                    buttonXacNhanChamCong.isEnabled = false
                    buttonXacNhanChamCong.setTextColor(ContextCompat.getColor(this@MapsActivity, R.color.colorTextDisable))
                } else {
                    buttonXacNhanChamCong.isEnabled = true
                    buttonXacNhanChamCong.setTextColor(ContextCompat.getColor(this@MapsActivity, R.color.colorPrimary))

                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

        })

        buttonXacNhanChamCong.setOnClickListener {
            val text = inputNote.text.trim().toString()
            requestChamCong(bitmap, text)
            inputNote.setText("")
            val imm: InputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(inputNote.windowToken, 0)
            dialogNote.visibility = View.GONE

        }

    }

    private val compositeDisposable = CompositeDisposable()
    private fun requestChamCong(bitmap: Bitmap, note: String = "") {
        if (!::currentMyLocation.isInitialized) {
            showDialog("Không lấy được vị trí, vui lòng kiểm tra lại") {
                finish()
            }
            return
        }
        val param = JSONObject()
        try {
            val calendar = Calendar.getInstance()
            val timeKeeping = DateConvert.convertDate(calendar.timeInMillis, DateConvert.TypeConvert.ONLY_DATE2)
            val imageName = "chamCong_${System.currentTimeMillis()}.jpg"
            param.put("UserId", App.getUserInfo().id)
            param.put("NgayChamCong", timeKeeping)
            if (typeChamCong == 0) {
                param.put("Lat", currentMyLocation.latitude)
                param.put("Lng", currentMyLocation.longitude)
            } else {
                param.put("Lat", anPhuocThaiLocation.latitude)
                param.put("Lng", anPhuocThaiLocation.longitude)
            }

            param.put("PlaceName", "AnPhuocThai")
            param.put("PlaceDescription", note)
            param.put("ImageBase64", Utils.bitmapToBase64(bitmap))
            param.put("imageName", imageName)
            param.put("IsXacNhan", typeChamCong)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        printLog(this, "IsXacNhan $typeChamCong")
        showLoading()
        val disposable = Rx2AndroidNetworking.post(ApiURL.ChamCongAPI)
                .addHeaders(ApiURL.getBaseHeader())
                .addHeaders(ApiURL.getTokenHeader())
                .addJSONObjectBody(param)
                .setPriority(Priority.MEDIUM)
                .build()
                .getObjectObservable(ChamCongResponse::class.java)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    hideLoading()
                    if (it.isSuccess()) {
                        val chamCongModel = it.objectInfo
                        chamCongModel?.let { model ->
                            val adapter = recyclerView.adapter as HistoryTimeKeepingAdapter
                            adapter.insertFirst(model)
                            standardBottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
                        }
                        showDialogConfirm("Đã chấm công") { finish() }
                    }
                }, {
                    hideLoading()
                    showDialog(it.localizedMessage, null)
                })
        compositeDisposable.add(disposable)
    }

    private fun anPhuocThaiLocation() {

        val isNearCompany = isNearCompany(currentMyLocation)

        fun checkEnable(button: TextView, isEnable: Boolean) {
            if (isEnable) {
                button.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_check_enable_blue, 0)
                button.setTextColor(ContextCompat.getColor(this, R.color.black))
            } else {
                button.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_check_disable, 0)
                button.setTextColor(ContextCompat.getColor(this, R.color.colorTextDisable))
            }
            button.isEnabled = isEnable
        }

        if (isNearCompany) {
            checkEnable(buttonChamCongTaiCongTy, true)
            checkEnable(buttonChamCongDiaDiemKhac, false)
        } else {
            checkEnable(buttonChamCongTaiCongTy, false)
            checkEnable(buttonChamCongDiaDiemKhac, true)
        }
        polyline?.remove()
        val polylineOptions = PolylineOptions()
                .add(anPhuocThaiLocation, currentMyLocation)
                .width(20f).color(ContextCompat.getColor(this@MapsActivity, R.color.colorBlue))
        polyline = googleMap.addPolyline(polylineOptions)

    }


    private fun requestPermissionLocation() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                checkLocationPermission()
                return
            }
        }
        trackingLocation()
    }

    private fun checkLocationPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(
                        this,
                        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        MY_PERMISSIONS_REQUEST_LOCATION
                )
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(
                        this,
                        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        MY_PERMISSIONS_REQUEST_LOCATION
                )
            }
        }
    }

    private fun checkCameraPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(
                        this,
                        arrayOf(Manifest.permission.CAMERA),
                        MY_CAMERA_PERMISSION_CODE
                )
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(
                        this,
                        arrayOf(Manifest.permission.CAMERA),
                        MY_CAMERA_PERMISSION_CODE
                )
            }
        }
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {

        when (requestCode) {
            MY_PERMISSIONS_REQUEST_LOCATION -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        trackingLocation()
                    }
                } else {
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show()
                }
                return
            }
            MY_CAMERA_PERMISSION_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                        startActivityForResult(cameraIntent, CAMERA_REQUEST)
                    }
                } else {
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show()
                }
            }
        }
    }


    private fun GoogleMap.moveTo() {
        if (!::currentMyLocation.isInitialized) {
            return
        }

        printLog(this, "moveTo $currentMyLocation $cameraZoom")
        val cameraFactory = CameraUpdateFactory.newLatLngZoom(currentMyLocation, cameraZoom)
        animateCamera(cameraFactory)
    }

    private fun getStatusBarHeight(): Int {
        var result = 0
        val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
        if (resourceId > 0) {
            result = resources.getDimensionPixelSize(resourceId)
        }
        return result
    }

    override fun onResume() {
        super.onResume()
        printLog(this, "onResume")
        if (::googleMap.isInitialized) {
            trackingLocation()
        }
    }

    override fun onPause() {
        super.onPause()
        fusedLocationClient.removeLocationUpdates(mLocationCallback)
    }

    private lateinit var standardBottomSheetBehavior: BottomSheetBehavior<View>
    private fun setupStandardBottomSheet() {
        standardBottomSheetBehavior = BottomSheetBehavior.from(bottom_sheet)
        val bottomSheetCallback = object : BottomSheetBehavior.BottomSheetCallback() {

            override fun onStateChanged(bottomSheet: View, newState: Int) {
                printLog(this, getStateString(newState))
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                val fraction = (slideOffset + 1f) / 2f
                printLog(this, "$fraction")
            }
        }
        standardBottomSheetBehavior.setBottomSheetCallback(bottomSheetCallback)
    }

    fun getStateString(state: Int): String {
        return when (state) {
            BottomSheetBehavior.STATE_EXPANDED -> {
                detailTimeKeeping.text = "Vuốt xuống để ẩn"
                "STATE_EXPANDED"
            }
            BottomSheetBehavior.STATE_COLLAPSED -> {
                detailTimeKeeping.text = "Vuốt lên để xem chi tiết"
                "STATE_COLLAPSED"
            }
            BottomSheetBehavior.STATE_DRAGGING -> "STATE_DRAGGING"
            BottomSheetBehavior.STATE_HALF_EXPANDED -> "STATE_HALF_EXPANDED"
            BottomSheetBehavior.STATE_HIDDEN -> {
                "STATE_HIDDEN"
            }
            BottomSheetBehavior.STATE_SETTLING -> "STATE_SETTLING"
            else -> "ELSE"
        }
    }

    /**
     *
     *
     * Lịch sử chấm công
     *
     *
     */
    private val calendar: Calendar = Calendar.getInstance()!!
    private var timeNow: String = ""
    private fun initRecyclerView() {
        recyclerView.apply {
            adapter = HistoryTimeKeepingAdapter(this@MapsActivity)
            layoutManager = LinearLayoutManager(this@MapsActivity)
            setHasFixedSize(true)
        }

        imageRight.setOnClickListener {
            calendar.add(Calendar.DAY_OF_MONTH, 1)
            getHistoryChamCong()
        }
        imageLeft.setOnClickListener {
            calendar.add(Calendar.DAY_OF_MONTH, -1)
            getHistoryChamCong()
        }
        goToNow.setOnClickListener {
            val now = Calendar.getInstance()
            calendar.timeInMillis = now.timeInMillis
            getHistoryChamCong()
        }

        getHistoryChamCong()
    }


    private fun getHistoryChamCong() {
        imageRight.isEnabled = false
        imageLeft.isEnabled = false
        val time = DateConvert.convertDate(calendar.timeInMillis, DateConvert.TypeConvert.ONLY_DATE2)
        if (timeNow.isEmpty()) {
            timeNow = time
        }
        if (timeNow == time) {
            imageRight.visibility = View.INVISIBLE
            goToNow.visibility = View.INVISIBLE
        } else {
            imageRight.visibility = View.VISIBLE
            goToNow.visibility = View.VISIBLE
        }
        dayOfMonth.text = time
        val userId = App.getUserInfo().id
        val url = "${ApiURL.HistoryTimeKeepingAPI}?NgayChamCong=$time&userId=$userId"
        showLoading()
        val disposable = Rx2AndroidNetworking.post(url)
                .addHeaders(ApiURL.getBaseHeader())
                .addHeaders(ApiURL.getTokenHeader())
                .setPriority(Priority.MEDIUM)
                .build()
                .getObjectObservable(DetailTimeKeepingResponse::class.java)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    hideLoading()
                    imageRight.isEnabled = true
                    imageLeft.isEnabled = true
                    if (it.isSuccess) {
                        val list = it.data ?: mutableListOf()
                        list.reverse()
                        fillData(list)
                    }

                }, {
                    hideLoading()
                    imageRight.isEnabled = true
                    imageLeft.isEnabled = true
                    showDialog(it.localizedMessage, null)
                })
        compositeDisposable.add(disposable)
    }

    private fun fillData(list: MutableList<ChamCongModel>) {
        if (list.isEmpty()) {
            emptyView.visibility = View.VISIBLE
            recyclerView.visibility = View.INVISIBLE
            return
        }
        emptyView.visibility = View.GONE
        recyclerView.visibility = View.VISIBLE
        val adapter = recyclerView.adapter as HistoryTimeKeepingAdapter
        adapter.updateList(list)
    }


}

