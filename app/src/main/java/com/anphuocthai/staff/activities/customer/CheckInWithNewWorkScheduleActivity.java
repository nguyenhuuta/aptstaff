package com.anphuocthai.staff.activities.customer;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.anphuocthai.staff.R;
import com.anphuocthai.staff.adapters.checkin.CheckedInImageRecyclerViewAdapter;
import com.anphuocthai.staff.api.ApiURL;
import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.interfaces.IAPICallback;
import com.anphuocthai.staff.model.ResponseMessage;
import com.anphuocthai.staff.ui.base.BaseActivity;
import com.anphuocthai.staff.utils.App;
import com.anphuocthai.staff.utils.Utils;
import com.anphuocthai.staff.utils.extension.GridSpacingItemDecoration;
import com.frosquivel.magicalcamera.MagicalCamera;
import com.frosquivel.magicalcamera.MagicalPermissions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import static com.anphuocthai.staff.utils.Constants.RESULT_CODE_TAKE_PICTURE;

public class CheckInWithNewWorkScheduleActivity extends BaseActivity implements CheckedInImageRecyclerViewAdapter.OnItemClickListener {

    private static final String TAG = CheckInWithNewWorkScheduleActivity.class.getSimpleName();
    private static final int STEP_DO_NOTHING = 0;

    private String guid;
    private String customerName;
    private EditText txtNameWorkSchedule;
    private EditText txtDescriptionWorkSchedule;
    private EditText txtLocationWorkSchedule;
    private Button btnCreateNewWorkSchedule;
    private TextView txtDateSchedule;
    private TextView txtTimeSchedule;
    private TextView txtWithCustomer;
    private TextView tv_check_in_warming;
    private ImageView ivAddNewPicture;
    private RecyclerView recyclerView;
    private RadioGroup typeWSRadioGroup;
    private CheckedInImageRecyclerViewAdapter adapter;
    private int currentItemToDelete;

    // Magical camera
    private MagicalCamera magicalCamera;
    private MagicalPermissions magicalPermissions;
    private int RESIZE_PHOTO_PIXELS_PERCENTAGE = 10;
    String[] permissions = new String[]{
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
    };

    Map<String, Boolean> mapPermissions = null;

    private ArrayList<Bitmap> photos = new ArrayList<>();

    private int numberUpload;

    int doneStep = STEP_DO_NOTHING;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_in_with_new_work_schedule);
        handleIntent();
        setupToolbarAndTitle();

        addControls();
        setupRadioGroup();
        setCurrentDayAndTime();
        App.setContext(this);
        initRecyclerView();
        initCamera();
        addEvents();
    }

    private void initCamera() {
        // Magical camera
        magicalPermissions = new MagicalPermissions(this, permissions);
        magicalCamera = new MagicalCamera(this, RESIZE_PHOTO_PIXELS_PERCENTAGE, magicalPermissions);
    }

    @Override
    protected void onResume() {
        super.onResume();
        magicalCamera.setResizePhoto(RESIZE_PHOTO_PIXELS_PERCENTAGE);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        mapPermissions = magicalPermissions.permissionResult(requestCode, permissions, grantResults);

        for (String permission : mapPermissions.keySet()) {
            Log.d("PERMISSIONS", permission + " was: " + mapPermissions.get(permission));
        }
    }

    private void setupToolbarAndTitle() {
        setupToolbar(getString(R.string.ci_btn_check_in_with_new_ws_text));
    }

    private void handleIntent() {
        Bundle b = getIntent().getBundleExtra("checkinwithnewworkextra");
        this.guid = b.getString("guid");
        this.customerName = b.getString("customername");
    }

    private void addControls() {
        txtNameWorkSchedule = findViewById(R.id.full_work_schedule_name);
        txtDescriptionWorkSchedule = findViewById(R.id.work_schedule_description);
        txtLocationWorkSchedule = findViewById(R.id.work_schedule_location);
        btnCreateNewWorkSchedule = findViewById(R.id.btn_check_in_with_new_work_schedule);
        txtDateSchedule = findViewById(R.id.txtDateSchedule);
        txtTimeSchedule = findViewById(R.id.txtTimeSchedule);
        txtWithCustomer = findViewById(R.id.ci_txt_add_new_ws_with_customer);
        tv_check_in_warming = findViewById(R.id.tv_check_in_warming);
        txtWithCustomer.setText(customerName);
        ivAddNewPicture = findViewById(R.id.iv_add_new_picture);

        recyclerView = findViewById(R.id.recyclerView);
        // type ws radio group
        typeWSRadioGroup = findViewById(R.id.ci_ws_type_ws_radio_group);
    }

    private void setupRadioGroup() {
        typeWSRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.ci_ws_debt_radiobutton:
                        txtNameWorkSchedule.setText(getString(R.string.ci_ws_debt_text) + " : " + customerName);
                        txtDescriptionWorkSchedule.setText(R.string.ci_ws_debt_text);
                        break;
                    case R.id.ci_ws_customer_care_radiobutton:
                        txtNameWorkSchedule.setText(getString(R.string.ci_ws_custormer_care) + " : " + customerName);
                        txtDescriptionWorkSchedule.setText(R.string.ci_ws_custormer_care);
                        break;
                    case R.id.ci_ws_offer_radiobutton:
                        txtNameWorkSchedule.setText(getString(R.string.ci_ws_offer_text) + " : " + customerName);
                        txtDescriptionWorkSchedule.setText(R.string.ci_ws_offer_text);
                        break;
                    case R.id.ci_ws_other_radiobutton:
                        txtNameWorkSchedule.setText("");
                        txtDescriptionWorkSchedule.setText("");
                        break;
                }
            }
        });

        typeWSRadioGroup.check(R.id.ci_ws_customer_care_radiobutton);
    }

    private void setCurrentDayAndTime() {
        txtDateSchedule.setText(Utils.formatDateWithFormat("dd-MM-yyyy", new Date()));
        txtTimeSchedule.setText(Utils.formatDateWithFormat("HH:mm", new Date()));
    }

    private void addEvents() {
        btnCreateNewWorkSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createNewWorkSchedule();
            }
        });

        ivAddNewPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dexter.withActivity(CheckInWithNewWorkScheduleActivity.this)
                        .withPermission(Manifest.permission.CAMERA)
                        .withListener(new PermissionListener() {
                            @Override public void onPermissionGranted(PermissionGrantedResponse response) {
                                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(cameraIntent, RESULT_CODE_TAKE_PICTURE);
                            }
                            @Override public void onPermissionDenied(PermissionDeniedResponse response) {

                            }
                            @Override public void onPermissionRationaleShouldBeShown(PermissionRequest permission, final PermissionToken token) {

                                new AlertDialog.Builder(CheckInWithNewWorkScheduleActivity.this, R.style.MyDialogTheme).setTitle(R.string.permission_rationale_title)
                                        .setMessage(R.string.permission_rationale_message)
                                        .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                                            @Override public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                                token.cancelPermissionRequest();
                                            }
                                        })
                                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                            @Override public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                                token.continuePermissionRequest();
                                            }
                                        })
                                        .setOnDismissListener(new DialogInterface.OnDismissListener() {
                                            @Override public void onDismiss(DialogInterface dialog) {
                                                token.cancelPermissionRequest();
                                            }
                                        })
                                        .show();
                            }
                        }).check();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        switch (requestCode) {
            case RESULT_CODE_TAKE_PICTURE:
                if (resultCode == RESULT_OK && intent.hasExtra("data")) {
                    Bitmap bitmap = (Bitmap) intent.getExtras().get("data");
                    if (bitmap != null) {
                        Bitmap resized = Bitmap.createScaledBitmap(bitmap,(int)(bitmap.getWidth()*0.8), (int)(bitmap.getHeight()*0.8), true);
                        photos.add(resized);
                        populateRecyclerView();
                    }
                }
                break;
        }
    }

    private void initRecyclerView() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(3, 3, false));

        adapter = new CheckedInImageRecyclerViewAdapter(this, photos);
        adapter.setOnImageItemClickListener(this);
        recyclerView.setAdapter(adapter);
    }

    private void populateRecyclerView() {
        if (photos == null || photos.isEmpty()) {
            tv_check_in_warming.setVisibility(View.VISIBLE);
        } else {
            tv_check_in_warming.setVisibility(View.GONE);
        }
        adapter.setPhotos(photos);
    }

    @Override
    public void imageItemClick(View view, int position, boolean isLongClick) {
        currentItemToDelete = position;
        view.showContextMenu();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater m = getMenuInflater();
        m.inflate(R.menu.menu_delete_photo_item, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.delete_photo_item:
                photos.remove(currentItemToDelete);
                populateRecyclerView();
                return true;
        }
        return super.onContextItemSelected(item);

    }

    private void createNewWorkSchedule() {
        if (!validate()) {
            btnCreateNewWorkSchedule.setEnabled(true);
            return;
        }

        btnCreateNewWorkSchedule.setEnabled(false);
        showLoading();
        String name = txtNameWorkSchedule.getText().toString();
        String description = txtDescriptionWorkSchedule.getText().toString();
        String location = txtLocationWorkSchedule.getText().toString();

        JSONObject newWSObject = new JSONObject();
        try {
            newWSObject.put("id", "0");
            newWSObject.put("guidThanhVien", this.guid);
            newWSObject.put("ten", name);
            newWSObject.put("moTa", description);
            newWSObject.put("ngayDuKien", Utils.formatDateStringWithFormat("dd-MM-yyyy", "yyyy-MM-dd", txtDateSchedule.getText().toString()) + " " + txtTimeSchedule.getText());
            newWSObject.put("viTri", location);
        } catch (Exception e) {
            e.printStackTrace();
        }

        NetworkManager.getInstance().sendPostRequest(ApiURL.CREATE_OR_UPDATE_WORK_SCHEDULE_URL, newWSObject, new IAPICallback() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                btnCreateNewWorkSchedule.setEnabled(true);
                Log.d(TAG, response.toString());
                if (response != null) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<ResponseMessage>() {}.getType();
                    ResponseMessage message = gson.fromJson(response.toString(), type);
                    if (message.getCode().trim().equals("00")) {
                        numberUpload = photos.size();
                        for (int i = 0; i < photos.size(); i++) {
                            upLoadCheckInImage(photos.get(i), i, message.getIdRecord());
                        }
                    } else {
                        showError(message.getMessage());
                    }
                } else {
                    showError(getString(R.string.server_not_respond_when_make_schedule));
                }
            }

            @Override
            public void onResponseSuccess(JSONArray response) {
                hideLoading();
            }

            @Override
            public void onResponseError(ANError anError) {
                onCreateWorkScheduleFailed();
            }
        });
    }

    private void showError(String message) {
        hideLoading();
        showMessage(message);
    }

    private void upLoadCheckInImage(Bitmap bitmap, int index, String wsId) {
        JSONObject workScheduleObject = new JSONObject();
        try {
            workScheduleObject.put("loaiID", 3);
            workScheduleObject.put("moTa", "Mo ta anh");
            workScheduleObject.put("objectID", wsId);
            workScheduleObject.put("guidThanhVien", guid);
            workScheduleObject.put("tenFile", wsId + guid + index);
            workScheduleObject.put("noidungFile", Utils.bitmapToBase64(bitmap));


        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(ApiURL.UPLOAD_FILE_WORK_SCHEDULE_URL)
                .addHeaders(ApiURL.getBaseHeader())
                .addHeaders(ApiURL.getTokenHeader())
                .addJSONObjectBody(workScheduleObject)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        if (response != null) {
                            numberUpload--;
                            if (numberUpload <= 0) {
                                onCreateWorkScheduleSuccess();
                            }

                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        onCreateWorkScheduleFailed();
                    }
                });
    }

    private void onCreateWorkScheduleFailed() {
        hideLoading();
        int msgId = doneStep == STEP_DO_NOTHING ? R.string.have_error_create_ws : R.string.have_error_check_in;
        Toast.makeText(getBaseContext(), getString(msgId), Toast.LENGTH_SHORT).show();
        btnCreateNewWorkSchedule.setEnabled(true);
    }

    private void onCreateWorkScheduleSuccess() {
        hideLoading();
        photos.clear();
        finish();
        btnCreateNewWorkSchedule.setEnabled(true);
        Toast.makeText(getBaseContext(), getString(R.string.check_in_with_new_work_schedule_success), Toast.LENGTH_SHORT).show();
    }

    private boolean validate() {
        boolean valid = true;
        String errMsg = null;
        String name = txtNameWorkSchedule.getText().toString();
        String location = txtLocationWorkSchedule.getText().toString();

        if (name.isEmpty()) {
            txtNameWorkSchedule.setError(getString(R.string.ci_work_schedule_name_empty));
            errMsg = getString(R.string.ci_work_schedule_name_empty);
            valid = false;
        } else {
            txtNameWorkSchedule.setError(null);
        }

        if (location.isEmpty()) {
            txtLocationWorkSchedule.setError(getString(R.string.ci_work_schedule_address));
            if (errMsg == null) errMsg = getString(R.string.ci_work_schedule_address);
            valid = false;
        } else {
            txtLocationWorkSchedule.setError(null);
        }

        if (photos == null || photos.size() <= 0) {
            if (errMsg == null) errMsg = getString(R.string.ci_work_schedule_no_image_check_in);
            valid = false;
        }
        if (!valid) {
            Toast.makeText(getBaseContext(), errMsg, Toast.LENGTH_SHORT).show();
        }
        return valid;
    }

}
