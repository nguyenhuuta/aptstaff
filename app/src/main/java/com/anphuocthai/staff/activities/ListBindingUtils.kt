package com.anphuocthai.staff.activities

import android.widget.ArrayAdapter
import androidx.appcompat.widget.AppCompatSpinner
import androidx.databinding.BindingAdapter
import com.anphuocthai.staff.R


/**
 * Created by OpenYourEyes on 2/26/21
 */
object ListBindingUtils {
    /**
     * Spinner Adapter
     */
//    @BindingAdapter(value = ["onItemSelected"])
//    fun setProjects(spinner: Spinner, projects: List<*>, selectedProject: Project, listener: InverseBindingListener) {
//        if (projects == null) return
//        spinner.adapter = NameAdapter(spinner.context, android.R.layout.simple_spinner_dropdown_item, projects)
//        setCurrentSelection(spinner, selectedProject)
//        setSpinnerListener(spinner, listener)
//    }
    @JvmStatic
    @BindingAdapter("data")
    fun <T> setDataSpinner(spinner: AppCompatSpinner, items: MutableList<T>?) {
        println("spinner: $spinner $items")
        if (items == null) return
        val spinnerAdapter: ArrayAdapter<T> = ArrayAdapter(spinner.context, R.layout.item_spinner, items)
        spinner.adapter = spinnerAdapter
    }

}