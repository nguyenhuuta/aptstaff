package com.anphuocthai.staff.activities.detailpush;

import android.widget.TextView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.activities.BaseActivities;
import com.anphuocthai.staff.base.actionbar.CustomActionBar;
import com.anphuocthai.staff.ui.home.notifications.model.Notification;
import com.anphuocthai.staff.utils.DateConvert;
import com.anphuocthai.staff.utils.Utils;

/**
 * Created by OpenYourEyes on 11/20/2019
 */
public class DetailPushActivity extends BaseActivities {
    public static final String ARG_PUSH = "arg_push";
    private CustomActionBar mCustomActionBar;
    private TextView mOderOfUser, mTime, mContent;

    @Override
    public int getContentView() {
        return R.layout.detail_push_activity;
    }

    @Override
    public void initView() {
        mCustomActionBar = findViewById(R.id.customActionBar);
        mOderOfUser = findViewById(R.id.orderOfUser);
        mTime = findViewById(R.id.time);
        mContent = findViewById(R.id.content);
        mCustomActionBar.setTitle("Thông báo");
    }

    @Override
    public void initAction() {
        mCustomActionBar.showLeftRightButton(true, false);
        mCustomActionBar.setCustomActionBar(new CustomActionBar.ICustomActionBar() {
            @Override
            public void onButtonLeftClick() {
                finish();
            }

            @Override
            public void onButtonRightClick() {

            }
        });
    }

    @Override
    public void initData() {
        Notification notification = getIntent().getParcelableExtra(ARG_PUSH);
        if (notification != null) {
            mOderOfUser.setText(notification.getNhanDe());
            mContent.setText(notification.getNoiDung());
            long time = Utils.getDateInMillis(notification.getNgayTao());
            mTime.setText(DateConvert.convertDate(time, DateConvert.TypeConvert.FULL));
        }
    }
}
