package com.anphuocthai.staff.activities.tonghopluong

import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.anphuocthai.staff.databinding.ItemBangLuongBinding
import com.anphuocthai.staff.entities.BangLuongModel

/**
 * Created by OpenYourEyes on 4/15/21
 */
class TongHopLuongAdapter : RecyclerView.Adapter<TongHopLuongAdapter.LuongViewHolder>() {

    private var mList = mutableListOf<BangLuongModel>()
    fun updateList(list: MutableList<BangLuongModel>) {
        mList = list
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LuongViewHolder {
        val view = ItemBangLuongBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return LuongViewHolder(view)
    }

    override fun onBindViewHolder(holder: LuongViewHolder, position: Int) {
        holder.bindData(mList[position])
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    inner class LuongViewHolder(val binding: ItemBangLuongBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bindData(model: BangLuongModel) {
            binding.model = model
            if (model.isBold) {
                binding.title.setTypeface(null, Typeface.BOLD)
                binding.value1.setTypeface(null, Typeface.BOLD)
                binding.value2.setTypeface(null, Typeface.BOLD)
            } else {
                binding.title.setTypeface(null, Typeface.NORMAL)
                binding.value1.setTypeface(null, Typeface.NORMAL)
                binding.value2.setTypeface(null, Typeface.NORMAL)
            }
        }
    }
}