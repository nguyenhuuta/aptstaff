package com.anphuocthai.staff.activities.detailcoin;

import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.activities.BaseActivities;
import com.anphuocthai.staff.base.actionbar.CustomActionBar;
import com.anphuocthai.staff.utils.App;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.anphuocthai.staff.api.ApiURL.IMAGE_VIEW_URL;

public class DetailCoinActivity extends BaseActivities implements View.OnClickListener {
    private CustomActionBar mCustomerBar;
    private LinearLayout mParentContainer;

    private HorizontalScrollView mHorizontalScroll;

    private TextView mHowToCalculatorCoin, mMyEndow, mEndow;
    private TextView mButtonMember, mButtonSilver, mButtonGold, mButtonPlatinum, mButtonDiamond;
    private TextView mTextMember, mTextSilver, mTextGold, mTextPlatinum, mTextDiamond;
    private View mLine1, mLine2, mLine3, mLine4, mLine5;


    private CircleImageView mAvatar;
    private TextView mUserName;

    @Override
    public int getContentView() {
        return R.layout.activity_detail_coin;
    }

    @Override
    public void initView() {
        mCustomerBar = findViewById(R.id.customActionBar);
        mParentContainer = findViewById(R.id.parentContainer);
        mAvatar = findViewById(R.id.avatar);
        mUserName = findViewById(R.id.userName);

        mHorizontalScroll = findViewById(R.id.horizontalScroll);

        mHowToCalculatorCoin = findViewById(R.id.calculator_coin);
        mMyEndow = findViewById(R.id.my_endow);
        mEndow = findViewById(R.id.endow);


        mButtonMember = findViewById(R.id.button_member);
        mButtonSilver = findViewById(R.id.button_silver);
        mButtonGold = findViewById(R.id.button_gold);
        mButtonPlatinum = findViewById(R.id.button_platinum);
        mButtonDiamond = findViewById(R.id.button_diamond);

        mTextMember = findViewById(R.id.tv_member);
        mTextSilver = findViewById(R.id.tv_sliver);
        mTextGold = findViewById(R.id.tv_gold);
        mTextPlatinum = findViewById(R.id.tv_platinum);
        mTextDiamond = findViewById(R.id.tv_diamond);


        mLine1 = findViewById(R.id.line1);
        mLine2 = findViewById(R.id.line2);
        mLine3 = findViewById(R.id.line3);
        mLine4 = findViewById(R.id.line4);
        mLine5 = findViewById(R.id.line5);

        mCustomerBar.showLeftRightButton(true, false);
        mCustomerBar.setTitle("Chi tiết");

    }

    @Override
    public void initAction() {
        mCustomerBar.setCustomActionBar(new CustomActionBar.ICustomActionBar() {
            @Override
            public void onButtonLeftClick() {
                finish();
            }

            @Override
            public void onButtonRightClick() {

            }
        });
        setOnClick(mButtonMember, mButtonSilver, mButtonGold, mButtonPlatinum, mButtonDiamond);
        setOnClick(mHowToCalculatorCoin, mMyEndow, mEndow);
    }

    @Override
    public void initData() {
        try {
            Picasso.get()
                    .load(IMAGE_VIEW_URL + App.getUserInfo().getAnhDaiDienId())
                    .placeholder(R.drawable.ic_launch)
                    .into(mAvatar);
            mUserName.setText(App.getUserInfo().getFullname());
        } catch (Exception e) {

        }

//
//        usernameTextView.setText(App.getUserInfo().getFullname());
//        phoneNumberTextView.setText(App.getUserInfo().getMobile());
//        positionTextView.setText(App.getUserInfo().getRoleName());
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.button_member:
            case R.id.button_silver:
            case R.id.button_gold:
            case R.id.button_platinum:
            case R.id.button_diamond:
                setEnableButtonRank(false, mButtonMember, mButtonSilver, mButtonGold, mButtonPlatinum, mButtonDiamond);
                setEnableButtonRank(true, (TextView) view);
                showTextRank(view.getId());
                break;
            default:
                showDialog("Tính năng đang được phát triển", "Đồng ý", null);
                break;
        }
    }


    private void setOnClick(TextView... textViews) {
        for (TextView textView : textViews) {
            textView.setOnClickListener(this);
        }
    }

    private void showTextRank(int buttonId) {
        setEnableTextRank(false, mTextMember, mTextSilver, mTextGold, mTextPlatinum, mTextDiamond);
        showLine(false, mLine1, mLine2, mLine3, mLine4, mLine5);
        switch (buttonId) {
            case R.id.button_member:
                setEnableTextRank(true, mTextMember);
                showLine(true, mLine1);
                break;
            case R.id.button_silver:
                setEnableTextRank(true, mTextMember, mTextSilver);
                showLine(true, mLine2);
                break;
            case R.id.button_gold:
                setEnableTextRank(true, mTextMember, mTextSilver, mTextGold);
                showLine(true, mLine3);
                break;
            case R.id.button_platinum:
                mHorizontalScroll.postDelayed(() -> mHorizontalScroll.fullScroll(HorizontalScrollView.FOCUS_RIGHT), 100L);
                setEnableTextRank(true, mTextMember, mTextSilver, mTextGold, mTextPlatinum);
                showLine(true, mLine4);
                break;
            case R.id.button_diamond:
                mHorizontalScroll.postDelayed(() -> mHorizontalScroll.fullScroll(HorizontalScrollView.FOCUS_RIGHT), 100L);
                setEnableTextRank(true, mTextMember, mTextSilver, mTextGold, mTextPlatinum, mTextDiamond);
                showLine(true, mLine5);
                break;
        }

    }

    private void setEnableTextRank(boolean isEnable, TextView... textRanks) {
        int textColorId, drawableStartId;
        if (isEnable) {
            textColorId = getResources().getColor(R.color.colorTextMain);
            drawableStartId = R.drawable.bg_square_green;
        } else {
            textColorId = getResources().getColor(R.color.colorDisableCoin);
            drawableStartId = R.drawable.bg_square_gray;

        }
        for (TextView textView : textRanks) {
            textView.setTextColor(textColorId);
            textView.setCompoundDrawablesWithIntrinsicBounds(drawableStartId, 0, 0, 0);
        }
    }

    private void setEnableButtonRank(boolean isEnable, TextView... buttonRanks) {
        int textColorId, drawableId;
        if (isEnable) {
            textColorId = getResources().getColor(R.color.colorTextMain);
            drawableId = R.drawable.ic_lock_enable;
        } else {
            textColorId = getResources().getColor(R.color.colorDisableCoin);
            drawableId = R.drawable.ic_lock_disable;

        }
        for (TextView textView : buttonRanks) {
            textView.setTextColor(textColorId);
            if (textView.getId() != R.id.button_member) {
                textView.setCompoundDrawablesWithIntrinsicBounds(drawableId, 0, 0, 0);
            }
        }
    }

    private void showLine(boolean isShow, View... lines) {
        int show = isShow ? View.VISIBLE : View.GONE;
        for (View view : lines) {
            view.setVisibility(show);
        }
    }
}
