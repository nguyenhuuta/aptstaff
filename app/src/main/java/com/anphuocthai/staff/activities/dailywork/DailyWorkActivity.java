package com.anphuocthai.staff.activities.dailywork;

import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.error.ANError;
import com.anphuocthai.staff.R;
import com.anphuocthai.staff.activities.BaseActivities;
import com.anphuocthai.staff.activities.newcustomer.CustomerActivities;
import com.anphuocthai.staff.api.ApiURL;
import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.base.actionbar.CustomActionBar;
import com.anphuocthai.staff.interfaces.IAPICallback;
import com.anphuocthai.staff.model.customer.Schedule;
import com.anphuocthai.staff.ui.home.statistics.viewholder.DailyWorkAdapter;
import com.anphuocthai.staff.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;

public class DailyWorkActivity extends BaseActivities {
    public static final String TAG = "DailyWorkActivity";
    private CustomActionBar mCustomActionBar;
    private RecyclerView recyclerView;

    private TextView txtEmpty, mQuickCheckIn;

    private DailyWorkAdapter mAdapter;

    @Override
    public int getContentView() {
        return R.layout.daily_work_view_holder_layout;
    }

    @Override
    public void initView() {
        mCustomActionBar = findViewById(R.id.customActionBar);
        mCustomActionBar.showLeftRightButton(true, false);
        mCustomActionBar.setTitle(getString(R.string.common_daily_work));
        txtEmpty = findViewById(R.id.txt_empty);
        recyclerView = findViewById(R.id.daily_work_recycler_view);
        mQuickCheckIn = findViewById(R.id.cv_quick_check_in);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void initAction() {
        mCustomActionBar.setCustomActionBar(new CustomActionBar.ICustomActionBar() {
            @Override
            public void onButtonLeftClick() {
                finish();
            }

            @Override
            public void onButtonRightClick() {

            }
        });
        mQuickCheckIn.setOnClickListener(view -> {
            Intent intent = new Intent(this, CustomerActivities.class);
            intent.putExtra(CustomerActivities.ARG_FROM_WHERE, TAG);
            startActivity(intent);
        });
    }

    @Override
    public void initData() {
        mAdapter = new DailyWorkAdapter(new ArrayList<>(), this, true, true, true);
        recyclerView.setAdapter(mAdapter);
        getIndayWorkSchedule();
    }

    private void getIndayWorkSchedule() {
        JSONObject workScheduleObject = new JSONObject();
        try {
            // lúc hiển thị danh sách không cần guid
            workScheduleObject.put("guidThanhVien", "");
            workScheduleObject.put("tuNgay", Utils.formatDateWithFormat("yyyy-MM-dd", new Date()));
            workScheduleObject.put("denNgay", Utils.formatDateWithFormat("yyyy-MM-dd", new Date()));
            workScheduleObject.put("trangThaiId", 0);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        NetworkManager.getInstance().sendPostRequestWithArrayResponse(ApiURL.SEARCH_WORK_SCHEDULE_URL, workScheduleObject, new IAPICallback() {
            @Override
            public void onResponseSuccess(JSONObject response) {

            }

            @Override
            public void onResponseSuccess(JSONArray response) {

                if (response != null) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<ArrayList<Schedule>>() {
                    }.getType();
                    ArrayList<Schedule> result = gson.fromJson(response.toString(), type);
                    if (result.isEmpty()) {
                        recyclerView.setVisibility(View.GONE);
                        txtEmpty.setVisibility(View.VISIBLE);
                        mAdapter.updateList(result);
                    } else {
                        recyclerView.setVisibility(View.VISIBLE);
                        txtEmpty.setVisibility(View.GONE);
                    }

                }
            }

            @Override
            public void onResponseError(ANError anError) {

            }
        });
    }
}
