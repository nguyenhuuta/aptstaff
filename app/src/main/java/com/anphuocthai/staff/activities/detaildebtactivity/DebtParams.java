package com.anphuocthai.staff.activities.detaildebtactivity;

public class DebtParams {


    private int phanLoai;
    private int pageSize;
    private int idThanhVien;

//    {
////        "phanLoai":4, // Công nợ
////            "guidThanhVien":"",
////            "sessionID":"",
////            "tuNgay":"",
////            "denNgay":"",
////            "trangThaiIds":"",
////            "pageIndex":"",
////            "pageSize":100,
////            "idThanhVien":4301 // ID khách hàng
////    }


    public DebtParams(int idThanhVien) {
        this.phanLoai = 4;
        this.pageSize = 100;
        this.idThanhVien = idThanhVien;
    }

    public int getPhanLoai() {
        return phanLoai;
    }

    public void setPhanLoai(int phanLoai) {
        this.phanLoai = phanLoai;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getIdThanhVien() {
        return idThanhVien;
    }

    public void setIdThanhVien(int idThanhVien) {
        this.idThanhVien = idThanhVien;
    }
}
