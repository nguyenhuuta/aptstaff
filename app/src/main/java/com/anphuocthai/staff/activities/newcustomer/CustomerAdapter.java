package com.anphuocthai.staff.activities.newcustomer;

import android.graphics.drawable.GradientDrawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.activities.dailywork.DailyWorkActivity;
import com.anphuocthai.staff.fragments.customer.CustomerFragment;
import com.anphuocthai.staff.fragments.customer.CustomerType;
import com.anphuocthai.staff.model.customer.SearchCustomer;
import com.anphuocthai.staff.ui.customersearch.SearchActivity;
import com.anphuocthai.staff.utils.ColorUtils;
import com.anphuocthai.staff.utils.Enum;
import com.anphuocthai.staff.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by DaiKySy on 8/27/19.
 */
public class CustomerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private static final int TYPE_DATA = 1;
    private static final int TYPE_LOADING = 2;

    private final List<SearchCustomer> mOriginalData;
    private final List<SearchCustomer> mListCustomer;

    private int itemSelected = -1;
    private final ICustomerCallback mCallback;

    private final CustomerType customerType;
    private String fromWhere = "";

    public CustomerAdapter(CustomerType customerType, List<SearchCustomer> list, ICustomerCallback callback) {
        this.mCallback = callback;
        this.customerType = customerType;
        mListCustomer = list;
        mOriginalData = new ArrayList<>();
        mOriginalData.addAll(mListCustomer);
    }

    public void updateList(List<SearchCustomer> listCustomer) {
        mOriginalData.clear();
        mOriginalData.addAll(listCustomer);
        notifyDataSetChanged();
    }

    public void setFromWhere(String fromWhere) {
        this.fromWhere = fromWhere;
    }

    public void setItemSelected(int itemSelected) {
        this.itemSelected = itemSelected;
    }

    public void removeItem(SearchCustomer searchCustomer) {
        int index = mListCustomer.indexOf(searchCustomer);
        if (index > -1) {
            mListCustomer.remove(index);
            mOriginalData.remove(index);
            itemSelected = -1;
            notifyItemRemoved(index);
            notifyItemRangeChanged(index, mListCustomer.size());
        }


    }

    @Override
    public int getItemViewType(int position) {
        return mListCustomer.get(position).isLoadMore() ? TYPE_LOADING : TYPE_DATA;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int type) {
        View view;
        switch (type) {
            case TYPE_LOADING:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
                return new ViewHolderLoading(view);
            case TYPE_DATA:
            default:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_customer, parent, false);
                return new ViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof ViewHolder) {
            ((ViewHolder) viewHolder).bind(position);
        }
    }

    @Override
    public int getItemCount() {
        return mListCustomer.size();
    }


    public void filter(CustomerFragment.TypeFilter filterBy, String key) {
        if (itemSelected != -1 && itemSelected < mListCustomer.size()) {
            mListCustomer.get(itemSelected).setSelected(false);
            notifyItemChanged(itemSelected);
            itemSelected = -1;
        }
        mListCustomer.clear();
        switch (filterBy) {
            case TYPE_NAME:
                if (key.isEmpty()) {
                    mListCustomer.addAll(mOriginalData);
                    return;
                }
                for (SearchCustomer searchCustomer : mOriginalData) {
                    String name = searchCustomer.getTenDayDu();
                    if (!TextUtils.isEmpty(name)) {
                        if (name.toLowerCase().contains(key.toLowerCase())) {
                            mListCustomer.add(searchCustomer);
                        }
                    }
                }
                break;
            case TYPE_CONG_NO_ASC:
                for (SearchCustomer searchCustomer : mOriginalData) {
                    Integer congNo = searchCustomer.getCongNo();
                    if (congNo != null && congNo > 0) {
                        mListCustomer.add(searchCustomer);
                    }
                }

                Collections.sort(mListCustomer, (obj1, obj2) -> obj1.getCongNo().compareTo(obj2.getCongNo()));
                break;
            case TYPE_CONG_NO_DESC:
                for (SearchCustomer searchCustomer : mOriginalData) {
                    Integer congNo = searchCustomer.getCongNo();
                    if (congNo != null && congNo > 0) {
                        mListCustomer.add(searchCustomer);
                    }
                }
                Collections.sort(mListCustomer, (obj1, obj2) -> obj2.getCongNo().compareTo(obj1.getCongNo()));
                break;

            case TYPE_CV:
                mListCustomer.addAll(mOriginalData);
                break;
        }
        notifyDataSetChanged();

    }

    static class ViewHolderLoading extends RecyclerView.ViewHolder {

        ViewHolderLoading(@NonNull View itemView) {
            super(itemView);
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView txtCustomerName;

        private TextView txtCustomerCongNo;

        private TextView vRoundLetterView;

        private TextView txtCustomerPhone;

        private LinearLayout customerActionContainer;

        private TextView txtCustomerCall;

        private TextView txtCustomerSms;

        private TextView txtCustomerDetail;

        private TextView buttonDeleteCustomer;

        private TextView buttonChangeTypeUser;

        private ImageView mButtonMap;
        private int position;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtCustomerName = itemView.findViewById(R.id.item_customer_name);
            txtCustomerCongNo = itemView.findViewById(R.id.item_customer_cong_no);
            vRoundLetterView = itemView.findViewById(R.id.vRoundLetterView);
            GradientDrawable draw = new GradientDrawable();
            draw.setShape(GradientDrawable.OVAL);
            draw.setColor(ColorUtils.getRandomMaterialColor());
            vRoundLetterView.setBackground(draw);


            txtCustomerPhone = itemView.findViewById(R.id.item_customer_phone);
            customerActionContainer = itemView.findViewById(R.id.customer_action_container);
            txtCustomerCall = itemView.findViewById(R.id.txt_customer_call);
            txtCustomerSms = itemView.findViewById(R.id.txt_customer_sms);
            txtCustomerDetail = itemView.findViewById(R.id.txt_customer_detail);
            buttonDeleteCustomer = itemView.findViewById(R.id.deleteUser);
            buttonChangeTypeUser = itemView.findViewById(R.id.txt_change_type_user);
            if (customerType == CustomerType.POTENTIAL) {
                buttonChangeTypeUser.setOnClickListener(this);
                buttonChangeTypeUser.setVisibility(View.VISIBLE);
            } else {
                buttonChangeTypeUser.setVisibility(View.GONE);
            }

            mButtonMap = itemView.findViewById(R.id.buttonMap);
            itemView.setOnClickListener(this);
            txtCustomerCall.setOnClickListener(this);
            txtCustomerSms.setOnClickListener(this);
            txtCustomerDetail.setOnClickListener(this);
            buttonDeleteCustomer.setOnClickListener(this);
            mButtonMap.setOnClickListener(this);
        }

        public void bind(int position) {
            this.position = position;
            SearchCustomer searchCustomer = mListCustomer.get(position);
            txtCustomerName.setText(searchCustomer.getTenDayDu());
            Integer congNo = searchCustomer.getCongNo();
            boolean check = congNo != null && congNo > 0;
            if (check) {
                String congNoText = "Nợ : " + Utils.formatValue(congNo, Enum.FieldValueType.CURRENCY);
                txtCustomerCongNo.setText(congNoText);
            }
            txtCustomerCongNo.setVisibility(check ? View.VISIBLE : View.GONE);
            String alpha = "";
            if (!TextUtils.isEmpty(searchCustomer.getTenDayDu())) {
                alpha = String.valueOf(searchCustomer.getTenDayDu().charAt(0)).toUpperCase();
            }
            vRoundLetterView.setText(alpha);
            txtCustomerPhone.setText(searchCustomer.getPhone());

            if (searchCustomer.getSelected()) {
                customerActionContainer.setVisibility(View.VISIBLE);
            } else {
                customerActionContainer.setVisibility(View.GONE);
            }
        }

        @Override
        public void onClick(View v) {
            if (!fromWhere.isEmpty()) {
                switch (fromWhere) {
                    case DailyWorkActivity.TAG:
                        mCallback.goToCheckInWithNewWorkScheduleActivity(mListCustomer.get(position));
                        break;
                    case SearchActivity.TAG:
                        mCallback.onFromSearchActivity(mListCustomer.get(position));
                        break;
                }
                return;
            }

            SearchCustomer searchCustomer = mListCustomer.get(position);
            switch (v.getId()) {
                case R.id.txt_customer_call:
                    mCallback.onActionCall(searchCustomer);
                    break;
                case R.id.txt_customer_sms:
                    mCallback.onActionSms(searchCustomer);
                    break;
                case R.id.txt_customer_detail:
                    mCallback.onActionDetail(searchCustomer);
                    break;
                case R.id.deleteUser:
                    mCallback.onActionLockerUser(searchCustomer);
                    break;
                case R.id.buttonMap:
                    mCallback.onActionGoToMap(searchCustomer);
                    break;
                case R.id.txt_change_type_user:
                    mCallback.onChangeTypeUser(searchCustomer);
                    break;
                default:
                    if (itemSelected == -1) {
                        searchCustomer.setSelected(true);
                        notifyItemChanged(position);
                        itemSelected = position;
                    } else if (itemSelected != position) {
                        mListCustomer.get(itemSelected).setSelected(false);
                        notifyItemChanged(itemSelected);
                        searchCustomer.setSelected(true);
                        notifyItemChanged(position);
                        itemSelected = position;
                    } else {
                        mListCustomer.get(itemSelected).setSelected(false);
                        notifyItemChanged(itemSelected);
                        itemSelected = -1;
                    }
            }

        }
    }

    public interface ICustomerCallback {
        void onActionCall(SearchCustomer searchCustomer);

        void onActionSms(SearchCustomer searchCustomer);

        void onActionDetail(SearchCustomer searchCustomer);

        void onActionLockerUser(SearchCustomer searchCustomer);

        void onActionGoToMap(SearchCustomer searchCustomer);

        void onChangeTypeUser(SearchCustomer searchCustomer);

        void onFromSearchActivity(SearchCustomer searchCustomer);

        void goToCheckInWithNewWorkScheduleActivity(SearchCustomer searchCustomer);
    }


}
