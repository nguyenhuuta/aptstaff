package com.anphuocthai.staff.activities

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Criteria
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import androidx.core.app.ActivityCompat
import com.google.android.gms.maps.LocationSource

/**
 * Created by OpenYourEyes on 7/18/2020
 */
class FollowMeLocationSource constructor(val context: Context) : LocationSource, LocationListener {
    private var mListener: LocationSource.OnLocationChangedListener? = null
    private var locationManager: LocationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    private val criteria = Criteria()
    private var bestAvailableProvider: String? = null

    /* Updates are restricted to one every 10 seconds, and only when
     * movement of more than 10 meters has been detected.*/
    private val minTime = 10000L // minimum time interval between location updates, in milliseconds
    private val minDistance = 10f // minimum distance between location updates, in meters
    private fun getBestAvailableProvider() {
        /* The preffered way of specifying the location provider (e.g. GPS, NETWORK) to use
         * is to ask the Location Manager for the one that best satisfies our criteria.
         * By passing the 'true' boolean we ask for the best available (enabled) provider. */
        bestAvailableProvider = locationManager.getBestProvider(criteria, true)
    }

    /* Activates this provider. This provider will notify the supplied listener
     * periodically, until you call deactivate().
     * This method is automatically invoked by enabling my-location layer. */
    override fun activate(listener: LocationSource.OnLocationChangedListener) {
        // We need to keep a reference to my-location layer's listener so we can push forward
        // location updates to it when we receive them from Location Manager.
        mListener = listener

        // Request location updates from Location Manager
        if (bestAvailableProvider != null) {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return
            }
            locationManager.requestLocationUpdates(bestAvailableProvider!!, minTime, minDistance, this)
        } else {
            // (Display a message/dialog) No Location Providers currently available.
        }
    }

    /* Deactivates this provider.
     * This method is automatically invoked by disabling my-location layer. */
    override fun deactivate() {
        // Remove location updates from Location Manager
        locationManager.removeUpdates(this)
//            mListener = null
    }

    override fun onLocationChanged(location: Location) {
        /* Push location updates to the registered listener..
         * (this ensures that my-location layer will set the blue dot at the new/received location) */
        if (mListener != null) {
            mListener!!.onLocationChanged(location)
        }

        /* ..and Animate camera to center on that location !
         * (the reason for we created this custom Location Source !) */
    }

    override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {
    }

    override fun onProviderEnabled(provider: String) {
    }

    override fun onProviderDisabled(provider: String) {
    }


    init {
        criteria.accuracy = Criteria.ACCURACY_FINE
        criteria.powerRequirement = Criteria.POWER_LOW
        criteria.isAltitudeRequired = true
        criteria.isBearingRequired = true
        criteria.isSpeedRequired = true
        criteria.isCostAllowed = true
    }
}