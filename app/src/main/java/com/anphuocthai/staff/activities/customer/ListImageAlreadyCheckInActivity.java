package com.anphuocthai.staff.activities.customer;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.anphuocthai.staff.R;
import com.anphuocthai.staff.adapters.checkin.RecyclerViewType;
import com.anphuocthai.staff.adapters.checkin.SectionRecyclerViewAdapter;
import com.anphuocthai.staff.api.ApiURL;
import com.anphuocthai.staff.model.SectionModel;
import com.anphuocthai.staff.model.customer.ImageCheckIn;
import com.anphuocthai.staff.model.customer.Schedule;
import com.anphuocthai.staff.ui.base.BaseActivity;
import com.anphuocthai.staff.utils.Utils;
import com.frosquivel.magicalcamera.MagicalCamera;
import com.frosquivel.magicalcamera.MagicalPermissions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import pl.tajchert.nammu.Nammu;

import static com.anphuocthai.staff.utils.Constants.RESULT_CODE_TAKE_PICTURE;

//https://github.com/jkwiecien/EasyImage
//https://github.com/fabian7593/MagicalCamera
public class ListImageAlreadyCheckInActivity extends BaseActivity implements SectionRecyclerViewAdapter.OnItemClickListener {

    private static final String TAG = ListImageAlreadyCheckInActivity.class.getSimpleName();

    private Button btnTakePhoto;

    protected RecyclerView recyclerView;

    private Schedule schedule;

    private int numberUpload;

    private ArrayList<Bitmap> photos = new ArrayList<>();
    private ArrayList<Bitmap> photoAlreadyCheckin = new ArrayList<>();
    private ArrayList<ImageCheckIn> arrImageCheckIn = new ArrayList<>();
    private RecyclerViewType recyclerViewType;

    private int currentItemToDelete = 0;
    private SectionRecyclerViewAdapter adapter;

    // Magical camera
    private MagicalCamera magicalCamera;
    private MagicalPermissions magicalPermissions;
    private int RESIZE_PHOTO_PIXELS_PERCENTAGE = 10;
    String[] permissions = new String[]{
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    Map<String, Boolean> mapPermissions = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_image_already_check_in);
        handleIntent();
        recyclerViewType = RecyclerViewType.GRID;
        Nammu.init(this);
        setupToolbar(getString(R.string.ci_work_schedule_list_image));
        addControls();
        addEvents();
        populateRecyclerView();

        // Magical camera
        magicalPermissions = new MagicalPermissions(this, permissions);
        magicalCamera = new MagicalCamera(this, RESIZE_PHOTO_PIXELS_PERCENTAGE, magicalPermissions);

        // Test
        getAllImageAlreadyCheckin();

    }

    private void handleIntent() {
        Bundle b = getIntent().getBundleExtra("scheduledata");
        schedule = (Schedule) b.getSerializable("schedule");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_check_in, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_take_photo) {

            Dexter.withActivity(ListImageAlreadyCheckInActivity.this)
                    .withPermission(Manifest.permission.CAMERA)
                    .withListener(new PermissionListener() {
                        @Override public void onPermissionGranted(PermissionGrantedResponse response) {/* ... */
                            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraIntent, RESULT_CODE_TAKE_PICTURE);
                        }
                        @Override public void onPermissionDenied(PermissionDeniedResponse response) {

                        }
                        @Override public void onPermissionRationaleShouldBeShown(PermissionRequest permission, final PermissionToken token) {

                            new AlertDialog.Builder(ListImageAlreadyCheckInActivity.this, R.style.MyDialogTheme).setTitle(R.string.permission_rationale_title)
                                    .setMessage(R.string.permission_rationale_message)
                                    .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                                        @Override public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                            token.cancelPermissionRequest();
                                        }
                                    })
                                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                        @Override public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                            token.continuePermissionRequest();
                                        }
                                    })
                                    .setOnDismissListener(new DialogInterface.OnDismissListener() {
                                        @Override public void onDismiss(DialogInterface dialog) {
                                            token.cancelPermissionRequest();
                                        }
                                    })
                                    .show();
                        }
                    }).check();


            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //populate recycler view
    private void populateRecyclerView() {
        ArrayList<SectionModel> sectionModelArrayList = new ArrayList<>();
        ArrayList<String> arrSectionLabel = new ArrayList<>();
        arrSectionLabel.add(getString(R.string.ci_work_schedule_list_image_recent_take_photo));
        arrSectionLabel.add(getString(R.string.ci_work_schedule_list_image_already_check_in));
        ArrayList<String> itemArrayList = new ArrayList<>();
        ArrayList<String> itemPhotoAlreadyCheckinArrayList = new ArrayList<>();
        for (int i = 0; i < photos.size(); i++) {
            itemArrayList.add("Ảnh " + (i + 1));
        }
        for (int i = 0; i < photoAlreadyCheckin.size(); i++) {
            itemPhotoAlreadyCheckinArrayList.add("Ảnh " + (i + 1));
        }
        sectionModelArrayList.add(new SectionModel(arrSectionLabel.get(0), itemArrayList, photos));
        sectionModelArrayList.add(new SectionModel(arrSectionLabel.get(1), itemPhotoAlreadyCheckinArrayList, photoAlreadyCheckin));
        adapter = new SectionRecyclerViewAdapter(this, this, recyclerViewType, sectionModelArrayList);
        adapter.setOnImageItemClickListener(this);
        recyclerView.setAdapter(adapter);
        //Utils.showProgressDialog(this,false);
    }

    private void addControls() {
        btnTakePhoto = findViewById(R.id.btn_check_in);
        recyclerView = findViewById(R.id.ci_photo_result_recycler_view);
        //imagesAdapter = new ImagesAdapter(this, photos);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        //recyclerView.setAdapter(imagesAdapter);


    }

    private void addEvents() {
        btnTakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (photos.size() > 0) {
                    numberUpload = photos.size();
                    showLoading();
                    for (int i = 0; i < photos.size(); i++) {
                        upLoadCheckInImage(i);
                    }
                } else {
                    Toast.makeText(getBaseContext(), getString(R.string.ci_work_schedule_no_image_check_in), Toast.LENGTH_SHORT).show();
                }

            }
        });
    }


    private void getAllImageAlreadyCheckin() {

        //Utils.showProgressDialog(this,true);
        Map object = new HashMap();
        object.put("loaiID", "3");
        object.put("objectID", String.valueOf(schedule.getId()));
        object.put("guidThanhVien", schedule.getGuidThanhVien());
        AndroidNetworking.get(ApiURL.GET_FILES_WORK_SCHEDULE_URL)
                .addHeaders(ApiURL.getBaseHeader())
                .addHeaders(ApiURL.getTokenHeader())
                .addQueryParameter(object)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        //Log.d(TAG, response.toString());
                        Gson gson = new Gson();
                        Type type = new TypeToken<ArrayList<ImageCheckIn>>() {
                        }.getType();
                        arrImageCheckIn.clear();
                        photoAlreadyCheckin.clear();
                        arrImageCheckIn = gson.fromJson(response.toString(), type);
                        for (int i = 0; i < arrImageCheckIn.size(); i++) {
                            photoAlreadyCheckin.add(Utils.decodeBase64(arrImageCheckIn.get(i).getNoiDung()));
                        }
                        populateRecyclerView();
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });

    }

    private void deleteImageFile(String id) {

        //JSONObject object = new JSONObject();

        showLoading();
        AndroidNetworking.post(ApiURL.DELETE_FILE_WORK_SCHEDULE_URL)
                .addHeaders(ApiURL.getBaseHeader())
                .addHeaders(ApiURL.getTokenHeader())
                .addQueryParameter("id", id)

                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        arrImageCheckIn.remove(currentItemToDelete);
                        photoAlreadyCheckin.remove(currentItemToDelete);
                        populateRecyclerView();
                        hideLoading();

                    }

                    @Override
                    public void onError(ANError anError) {
                       hideLoading();
                    }

                });


    }

    private void upLoadCheckInImage(int position) {
        JSONObject workScheduleObject = new JSONObject();
        try {
            workScheduleObject.put("loaiID", 3);
            workScheduleObject.put("moTa", "Mo ta anh");
            workScheduleObject.put("objectID", schedule.getId());
            workScheduleObject.put("guidThanhVien", schedule.getGuidThanhVien());
            workScheduleObject.put("tenFile", "test3");
            workScheduleObject.put("noidungFile", Utils.bitmapToBase64(photos.get(position)));


        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(ApiURL.UPLOAD_FILE_WORK_SCHEDULE_URL)
                .addHeaders(ApiURL.getBaseHeader())
                .addHeaders(ApiURL.getTokenHeader())
                .addJSONObjectBody(workScheduleObject)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        if (response != null) {
                            numberUpload--;
                            if (numberUpload <= 0) {
                                numberUpload = 0;
                                photos.clear();
                                getAllImageAlreadyCheckin();
                                hideLoading();
                                finish();
                            }

                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        hideLoading();
                    }
                });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.clear();
    }

    @Override
    protected void onResume() {
        super.onResume();
        magicalCamera.setResizePhoto(RESIZE_PHOTO_PIXELS_PERCENTAGE);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        mapPermissions = magicalPermissions.permissionResult(requestCode, permissions, grantResults);

        for (String permission : mapPermissions.keySet()) {
            Log.d("PERMISSIONS", permission + " was: " + mapPermissions.get(permission));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        switch (requestCode) {
            case RESULT_CODE_TAKE_PICTURE:
                if (resultCode == RESULT_OK && intent.hasExtra("data")) {
                    Bitmap bitmap = (Bitmap) intent.getExtras().get("data");
                    if (bitmap != null) {
                        Bitmap resized = Bitmap.createScaledBitmap(bitmap,(int)(bitmap.getWidth()*0.8), (int)(bitmap.getHeight()*0.8), true);
                        photos.add(resized);
                        populateRecyclerView();
                    }
                }
                break;
        }
    }

    /**
     * when item long click, it call
     *
     * @param position
     */
    @Override
    public void imageItemClick(View view, int position, boolean isLongClick) {

        currentItemToDelete = position;
        view.showContextMenu();

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        // TODO Auto-generated method stub
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater m = getMenuInflater();
        m.inflate(R.menu.menu_delete_photo_item, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.delete_photo_item:
                deleteImageFile(String.valueOf(arrImageCheckIn.get(currentItemToDelete).getId()));
                return true;
        }
        return super.onContextItemSelected(item);

    }
}
