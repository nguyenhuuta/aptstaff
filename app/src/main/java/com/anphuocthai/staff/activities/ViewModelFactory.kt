package com.anphuocthai.staff.activities

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.anphuocthai.staff.activities.danhsachcongtac.DanhSachCongTacVM
import com.anphuocthai.staff.activities.kpi.KPIViewModel
import com.anphuocthai.staff.activities.tong_hop_phat.TongHopPhatViewModel
import com.anphuocthai.staff.activities.xinnghiphep.comment_nghiphep.CommentNghiPhepVM
import com.anphuocthai.staff.activities.xinnghiphep.danhsachnghiphep.DanhSachXinNghiVM
import com.anphuocthai.staff.activities.xinnghiphep.yeucaunghi.YeuCauNghiVM
import com.anphuocthai.staff.data.DataManager
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by OpenYourEyes on 12/28/20
 */

@Singleton
class ViewModelFactory @Inject constructor(val dataManager: DataManager) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        val viewModel = when (modelClass) {
            DanhSachCongTacVM::class.java -> DanhSachCongTacVM() as T
            YeuCauNghiVM::class.java -> YeuCauNghiVM() as T
            DanhSachXinNghiVM::class.java -> DanhSachXinNghiVM() as T
            CommentNghiPhepVM::class.java -> CommentNghiPhepVM() as T
            KPIViewModel::class.java -> KPIViewModel() as T
            TongHopPhatViewModel::class.java -> TongHopPhatViewModel() as T
            else -> {
                throw IllegalArgumentException("Unknown ViewModel class ${modelClass.simpleName}")
            }
        }
        (viewModel as? BaseViewModel)?.inject(dataManager = dataManager)
        return viewModel
    }

}