package com.anphuocthai.staff.activities.newcustomer;

import android.os.Bundle;
import android.util.SparseArray;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.anphuocthai.staff.fragments.customer.CustomerFragment;
import com.anphuocthai.staff.fragments.customer.CustomerType;

/**
 * Created by DaiKySy on 9/3/19.
 */
public class ViewPageCustomerAdapter extends FragmentPagerAdapter {

    private final SparseArray<CustomerFragment> mListFragmentCustomer;

    public ViewPageCustomerAdapter(FragmentManager fm) {
        super(fm);
        mListFragmentCustomer = new SparseArray<>();
        mListFragmentCustomer.put(0, createFragment(CustomerType.SELL));
        mListFragmentCustomer.put(1, createFragment(CustomerType.POTENTIAL));
    }

    private CustomerFragment createFragment(CustomerType customerType) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(CustomerFragment.ARG_TYPE_CUSTOMER, customerType);
        return CustomerFragment.newInstance(bundle);
    }

    @Override
    public Fragment getItem(int position) {
        return mListFragmentCustomer.get(position);
    }

    @Override
    public int getCount() {
        return mListFragmentCustomer.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
            default:
                return "Khách hàng đang bán";
            case 1:
                return "Khách hàng tiềm năng";
        }
    }

    public CustomerFragment getCustomerFragmentAt(int position) {
        return mListFragmentCustomer.get(position);
    }
}
