package com.anphuocthai.staff.activities.customer;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.anphuocthai.staff.R;
import com.anphuocthai.staff.adapters.checkin.DetailWSAdapter;
import com.anphuocthai.staff.api.ApiURL;
import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.interfaces.IAPICallback;
import com.anphuocthai.staff.interfaces.IYNDialogCallback;
import com.anphuocthai.staff.model.ResponseMessage;
import com.anphuocthai.staff.model.customer.Schedule;
import com.anphuocthai.staff.ui.base.BaseActivity;
import com.anphuocthai.staff.utils.Enum;
import com.anphuocthai.staff.utils.ui.UIUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;

public class DetailWorkScheduleActivity extends BaseActivity {

    private static final String TAG = DetailWorkScheduleActivity.class.getSimpleName();
    private RecyclerView recyclerView;
    private Button btnEdit;
    private Button btnCancelWS;
    private RecyclerView.LayoutManager aLayout;
    private Schedule schedule;
    private Boolean isHistory;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_work_schedule);
        handleIntent();
        setupToolbar(getString(R.string.ci_work_schedule_detail_title));
        setupViewPager();
        setupEdit();
        getWorkShedule();
    }

    private void handleIntent(){
        Bundle b = getIntent().getBundleExtra("extraschedule");
        schedule = (Schedule) b.getSerializable("schedule");
        isHistory = b.getBoolean("isHistory");
    }


    private void setupViewPager() {
        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewDetailWorkSchedule);
        recyclerView.setHasFixedSize(true);
        aLayout = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(aLayout);
    }

    private void setupEdit() {
        btnEdit = findViewById(R.id.btn_edit_work_schedule_detail);
        btnCancelWS = findViewById(R.id.btn_cancel_work_schedule);

        if (isHistory) {
            btnEdit.setVisibility(View.GONE);
            btnCancelWS.setVisibility(View.GONE);
        }
        if (schedule.getTrangThaiId() == Enum.WorkScheduleState.FINISH) {
            btnCancelWS.setVisibility(View.GONE);
        }
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(DetailWorkScheduleActivity.this, AddNewWorkScheduleActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("schedule", schedule);
                bundle.putBoolean("isEdit", true);
                i.putExtra("schedulenewworkextra", bundle);
                startActivity(i);
            }
        });
        btnCancelWS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UIUtils.showYesNoDialog(DetailWorkScheduleActivity.this, "", getString(R.string.ci_ws_alert_cancel_content), new IYNDialogCallback() {
                    @Override
                    public void accept() {
                        cancelWorkSchedule();
                    }

                    @Override
                    public void cancel() {

                    }
                });

            }
        });
    }

    private void cancelWorkSchedule() {
        HashMap hashMap = new HashMap();
        try {
            hashMap.put("id", String.valueOf(schedule.getId()));
        }catch (Exception e) {
            e.printStackTrace();
        }
        NetworkManager.getInstance().sendPostRequestWithQueryParam(ApiURL.DELETE_WORK_SCHEDULE_URL, hashMap, new IAPICallback() {
            @Override
            public void onResponseSuccess(JSONObject response) {

                if (response != null) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<ResponseMessage>() {}.getType();
                    ResponseMessage message =   gson.fromJson(response.toString(), type);
                    if (message.getCode().trim().equals("00")) {
                    }
                    showMessage(message.getMessage());
                    finish();
                }
               // hideLoading();
                //finish();
            }

            @Override
            public void onResponseSuccess(JSONArray response) {
                if (response != null) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<ResponseMessage>() {}.getType();
                    ResponseMessage message =   gson.fromJson(response.toString(), type);
                    if (message.getCode().trim().equals("00")) {
                    }
                    showMessage(message.getMessage());
                    finish();
                }
                //finish();
            }

            @Override
            public void onResponseError(ANError anError) {

            }
        });
    }

    private void getWorkShedule() {
        AndroidNetworking.get(ApiURL.GET_WORK_SCHEDULE_URL)
                .addHeaders(ApiURL.getBaseHeader())
                .addHeaders(ApiURL.getTokenHeader())
                .addQueryParameter("id", String.valueOf(schedule.getId()))
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        if (response != null) {
                            Gson gson = new Gson();
                            Type type = new TypeToken<Schedule>() {}.getType();
                            Schedule schedule =   gson.fromJson(response.toString(), type);
                            DetailWSAdapter detailWSAdapter = new DetailWSAdapter(schedule, DetailWorkScheduleActivity.this, isHistory);
                            recyclerView.setAdapter(detailWSAdapter);
                        }
                    }
                    @Override
                    public void onError(ANError anError) {

                    }
                });

    }

    @Override
    protected void onResume() {
        super.onResume();
        getWorkShedule();
    }
}
