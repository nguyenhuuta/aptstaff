package com.anphuocthai.staff.activities.listtopproduct

import android.content.Intent
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.anphuocthai.staff.R
import com.anphuocthai.staff.activities.BaseActivities
import com.anphuocthai.staff.api.addTo
import com.anphuocthai.staff.data.db.model.OrderProduct
import com.anphuocthai.staff.model.order.Product
import com.anphuocthai.staff.model.order.ProductSaleModel
import com.anphuocthai.staff.ui.delivery.model.GiaChinh
import com.anphuocthai.staff.ui.home.MainActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_list_top_product.*
import kotlinx.android.synthetic.main.item_cart_badget.view.*

/**
 * Created by OpenYourEyes on 8/28/2020
 */
class ListTopProductActivity : BaseActivities() {


    companion object {
        const val ARG_PRODUCT = "product"
        const val ARG_TITLE = "title"
        const val ARG_SALE = "isSale"
    }

    private var products = listOf<Product>()
    private var listOrder = mutableListOf<OrderProduct>()

    private var numberBadge = 0
    override fun getContentView(): Int {
        return R.layout.activity_list_top_product
    }

    override fun initAction() {
        buttonLeft.setOnClickListener {
            finish()
        }
        layoutCart.setOnClickListener {

        }
    }

    override fun initView() {
        shimmerView.startShimmerAnimation()
    }

    override fun initData() {
        getAllOderDatabase {
            listOrder.addAll(it)
            getData()
        }
    }

    private fun checkSelectedItem() {
        products.forEach { product ->
            listOrder.forEach { order ->
                try {
                    val orderId: Int = order.productId.toInt()
                    if (orderId == product.id) {
                        product.isSelected = true
                    }
                } catch (ignore: Exception) {

                }
            }
        }
        numberBadge = products.filter { it.isSelected }.count()
        if (numberBadge > 0) {
            layoutCart.badgeCart.text = "$numberBadge"
            layoutCart.badgeCart.visibility = View.VISIBLE
        }
    }


    fun getData() {
        val title = intent.getStringExtra(ARG_TITLE)
        val isSale = intent.getBooleanExtra(ARG_SALE, false)
        if (isSale) {
            val list = intent.getSerializableExtra(ARG_PRODUCT) as List<ProductSaleModel>
            list.map {
                val mainPrice = GiaChinh()
                mainPrice.kDBanBuonGia = it.giaKM
                it.mainPrice = mainPrice
            }
            products = list
        } else {
            products = intent.getSerializableExtra(ARG_PRODUCT) as List<Product>
        }
        titleActionBar.text = title

        checkSelectedItem()

        recycleCustomer.postDelayed({
            shimmerView.stopShimmerAnimation()
            shimmerView.visibility = View.GONE
            recycleCustomer.visibility = View.VISIBLE
            setupRecyclerView(products, isSale)
        }, 1000)
    }

    override fun onResume() {
        super.onResume()
        val adapter = recycleCustomer.adapter as? ListTopProductAdapter
        adapter?.let {
            getAllOderDatabase {
                listOrder.clear()
                listOrder.addAll(it)
                checkSelectedItem()
                adapter.notifyDataSetChanged()
            }
        }
    }

    private fun getAllOderDatabase(callback: (MutableList<OrderProduct>) -> Unit) {
        dataManager
                .allOrderProducts
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturnItem(listOf())
                .subscribe {
                    callback(it)

                }.addTo(compositeDisposable)
    }

    private fun setupRecyclerView(products: List<Product>, isSale: Boolean) {
        recycleCustomer.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@ListTopProductActivity, LinearLayoutManager.VERTICAL, false)
            adapter = ListTopProductAdapter(this@ListTopProductActivity, isSale, products) { product ->

                val productSave = product.toOrderProduct("", "")
                if (product.isSelected) {
                    dataManager.insertOrderProduct(productSave)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe().addTo(compositeDisposable = compositeDisposable)
                    numberBadge++
                } else {
                    dataManager.deleteOrderProduct(productSave)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe().addTo(compositeDisposable = compositeDisposable)
                    numberBadge--
                }
                layoutCart.badgeCart.text = "$numberBadge"
                layoutCart.badgeCart.visibility = if (numberBadge == 0) View.GONE else View.VISIBLE

                val intent = Intent()
                intent.action = MainActivity.BADGE_CHANGE
                sendBroadcast(intent)

            }
        }
    }
}