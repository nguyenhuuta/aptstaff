package com.anphuocthai.staff.activities.tong_hop_phat

import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.anphuocthai.staff.R
import com.anphuocthai.staff.activities.BaseActivityBinding
import com.anphuocthai.staff.adapters.BaseAdapterBinding
import com.anphuocthai.staff.databinding.TongHopPhatActivityBinding
import com.anphuocthai.staff.model.TongHopPhatModel

/**
 * Created by OpenYourEyes on 4/1/21
 */
class TongHopPhatActivity : BaseActivityBinding<TongHopPhatViewModel, TongHopPhatActivityBinding>() {
    override fun createViewModel(): Class<TongHopPhatViewModel> = TongHopPhatViewModel::class.java
    override fun layoutId(): Int = R.layout.tong_hop_phat_activity

    override fun titleToolBar(): String = "Tổng hợp phạt"

    override fun initView() {
        initToolbar(dataBinding.toolbar)

        dataBinding.recyclerView.apply {
            layoutManager = LinearLayoutManager(this@TongHopPhatActivity)
            setHasFixedSize(true)
            adapter = BaseAdapterBinding<TongHopPhatModel>(this@TongHopPhatActivity, R.layout.tong_hop_phat_item)
        }
    }

    override fun bindViewModel() {
        viewModel.listTongHopPhat.observe(this, Observer {
            val adapter = dataBinding.recyclerView.adapter as BaseAdapterBinding<TongHopPhatModel>
            adapter.updateList(it)
        })
        viewModel.getListTongHopPhat()
    }
}