package com.anphuocthai.staff.activities.customer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.entities.BaseColumn;
import com.anphuocthai.staff.utils.Utils;

import java.util.List;

/**
 * Created by OpenYourEyes on 11/16/2019
 */
public class SpinnerAdapter<T extends BaseColumn> extends ArrayAdapter {

    private List<T> mListData;
    private final LayoutInflater mInflater;

    public SpinnerAdapter(@NonNull Context context, List<T> list) {
        super(context, 0);
        mInflater = LayoutInflater.from(context);
        this.mListData = list;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }


    @Nullable
    @Override
    public T getItem(int position) {
        return mListData.get(position);
    }


    @Override
    public int getCount() {
        if (Utils.isNull(mListData)) return 0;
        return mListData.size();
    }

    private View createItemView(int position, View viewCell, ViewGroup parent) {
        ViewHolder viewHolder;
        if (viewCell == null) {
            viewCell = mInflater.inflate(R.layout.item_spinner, parent, false);
            viewHolder = new ViewHolder(viewCell);
            viewCell.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) viewCell.getTag();
        }
        viewHolder.bind(getItem(position));
        return viewCell;
    }


    private class ViewHolder {
        TextView txtTitle;

        public ViewHolder(View view) {
            txtTitle = view.findViewById(android.R.id.text1);
        }

        void bind(T data) {
            if (data == null) {
                txtTitle.setText("");
                return;
            }
            txtTitle.setText(data.toString());
            if (data.isDisable()){
                txtTitle.setTextColor(getContext().getResources().getColor(R.color.colorTextDisable));
            }else {
                txtTitle.setTextColor(getContext().getResources().getColor(R.color.colorTextMain));
            }
        }
    }
}
