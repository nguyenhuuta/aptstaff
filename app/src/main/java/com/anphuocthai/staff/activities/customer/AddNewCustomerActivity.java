package com.anphuocthai.staff.activities.customer;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.anphuocthai.staff.R;
import com.anphuocthai.staff.api.ApiURL;
import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.baserequest.APICallback;
import com.anphuocthai.staff.baserequest.APIService;
import com.anphuocthai.staff.baserequest.IAppAPI;
import com.anphuocthai.staff.data.address.DatabaseManager;
import com.anphuocthai.staff.entities.Cities;
import com.anphuocthai.staff.entities.District;
import com.anphuocthai.staff.entities.Wards;
import com.anphuocthai.staff.fragments.customer.CustomerType;
import com.anphuocthai.staff.fragments.customer.LoaiKinhDoanh;
import com.anphuocthai.staff.interfaces.IAPICallback;
import com.anphuocthai.staff.model.DanhMucLoaiKhachHangModel;
import com.anphuocthai.staff.model.DanhMucLoaiKhachHangParam;
import com.anphuocthai.staff.model.PhoneModel;
import com.anphuocthai.staff.model.ResponseMessage;
import com.anphuocthai.staff.model.customer.CustomerAddress;
import com.anphuocthai.staff.model.customer.SearchCustomer;
import com.anphuocthai.staff.ui.base.BaseActivity;
import com.anphuocthai.staff.ui.customer.detail.infomation.editinformation.phonetypedialog.PhoneTypeTypeDialog;
import com.anphuocthai.staff.utils.App;
import com.anphuocthai.staff.utils.Logger;
import com.anphuocthai.staff.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class AddNewCustomerActivity extends BaseActivity {

    private static final String TAG = AddNewCustomerActivity.class.getSimpleName();
    public static final String ARG_USER = "arg_user";
    public static final int RESTAURANT = 1;
    public static final int AGENCY = 2;
    public static final int PERSONAL = 3;

    /**
     * UI
     */
    private EditText txtCustomerFullName;
    private EditText txtCustomerEmail;
    private EditText txtCustomerCMT, mNumberHouse;

    private RadioButton radioRestaurant;
    private TextView btnCreateCustomer;
    private RadioGroup customerTypeRadioGroup;

    private RadioGroup mRadioGroupTypeUser;

    private ImageView contact_numbers_add_new;


    private LinearLayout contact_numbers_holder;

    private Spinner mSpinCity, mSpinDistrict, mSpinWard;

    private List<DanhMucLoaiKhachHangModel> radioButtons;
    private List<Cities> citiesList;
    private List<District> districtList;
    private List<Wards> wardsList;

    private boolean isDoneCity, isDoneDistrict, isDoneWards, isDoneDanhMuc;

    SearchCustomer customerModel;
    boolean isFromUpdate = false;
    DatabaseManager databaseManager = DatabaseManager.getInstance();
    private SpinnerAdapter<Cities> mCitiesArrayAdapter;
    private SpinnerAdapter<District> mDistrictArrayAdapter;
    private SpinnerAdapter<Wards> mWardsArrayAdapter;

    List<PhoneModel> listPhone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_customer);
        customerModel = getIntent().getParcelableExtra(ARG_USER);
        isFromUpdate = customerModel != null;
        App.setContext(this);
        addControls();
        customization();
        addEvents();
        checkAddressDatabase();
        if (isFromUpdate) {
            fillDataUpdateUser();
            btnCreateCustomer.setText("Sửa thông tin");
        }

        initPhoneViews();
    }


    private void initPhoneViews() {
        if (isFromUpdate && listPhone != null) {
            for (PhoneModel phoneModel : listPhone) {
                addNewPhoneNumberField(phoneModel);
            }
        } else {
            addNewPhoneNumberField(null);
        }
        contact_numbers_add_new.setOnClickListener((View v) -> {
            addNewPhoneNumberField(null);
        });
    }

    private void addControls() {
        txtCustomerFullName = findViewById(R.id.full_customer_name);
        txtCustomerEmail = findViewById(R.id.email_customer);
        txtCustomerCMT = findViewById(R.id.cmt_customer);
        mNumberHouse = findViewById(R.id.numberHouse);
        radioRestaurant = findViewById(R.id.radioRestaurant);
        btnCreateCustomer = findViewById(R.id.btn_create_customer);
        customerTypeRadioGroup = findViewById(R.id.customerTypeRadioGroup);
        contact_numbers_holder = findViewById(R.id.contact_numbers_holder);
        contact_numbers_add_new = findViewById(R.id.contact_numbers_add_new);
        mRadioGroupTypeUser = findViewById(R.id.radio_type_user);

        mSpinCity = findViewById(R.id.spinnerCity);
        mSpinDistrict = findViewById(R.id.spinnerDistrict);
        mSpinWard = findViewById(R.id.spinnerWard);
    }

    private void checkAddressDatabase() {
        citiesList = databaseManager.getAllCities();
        districtList = databaseManager.getDistrict();
        wardsList = databaseManager.getWards();
        IAppAPI mApi = APIService.getInstance().getAppAPI();
        isDoneCity = citiesList.size() > 0;
        isDoneDistrict = districtList.size() > 0;
        isDoneWards = wardsList.size() > 0;
        checkDoneAPI(-1);
        if (!isDoneDanhMuc) {
            getListDanhMuc();
        }
        if (!isDoneCity) {
            showLoading();
            int id = 1;
            mApi.getCities(new Cities.Params()).getAsyncResponse(new APICallback<List<Cities>>() {
                @Override
                public void onSuccess(List<Cities> cities) {
                    if (Utils.isNull(cities)) {
                        checkDoneAPI(id);
                        return;
                    }
                    citiesList.addAll(cities);
                    MyTask<Cities> task = new MyTask<>(AddNewCustomerActivity.this, id);
                    task.execute(cities);
                }

                @Override
                public void onFailure(String errorMessage) {
                    checkDoneAPI(id);
                }
            });
        }
        if (!isDoneDistrict) {
            showLoading();
            int id = 2;
            mApi.getListDistrict(new District.Params()).getAsyncResponse(new APICallback<List<District>>() {
                @Override
                public void onSuccess(List<District> districts) {
                    if (Utils.isNull(districts)) {
                        checkDoneAPI(id);
                        return;
                    }
                    districtList.addAll(districts);
                    MyTask<District> task = new MyTask<>(AddNewCustomerActivity.this, id);
                    task.execute(districts);
                }

                @Override
                public void onFailure(String errorMessage) {
                    checkDoneAPI(id);
                }
            });
        }
        if (!isDoneWards) {
            showLoading();
            int id = 3;
            mApi.getListWards(new Wards.Params()).getAsyncResponse(new APICallback<List<Wards>>() {
                @Override
                public void onSuccess(List<Wards> wards) {
                    if (Utils.isNull(wards)) {
                        checkDoneAPI(id);
                        return;
                    }
                    wardsList.addAll(wards);
                    MyTask<Wards> task = new MyTask<>(AddNewCustomerActivity.this, id);
                    task.execute(wards);
                }

                @Override
                public void onFailure(String errorMessage) {
                    checkDoneAPI(id);
                }
            });
        }
    }

    private void getListDanhMuc() {
        IAppAPI mApi = APIService.getInstance().getAppAPI();
        int id = 4;
        mApi.layDanhSachLoaiKhachHang(new DanhMucLoaiKhachHangParam()).getAsyncResponse(new APICallback<List<DanhMucLoaiKhachHangModel>>() {
            @Override
            public void onSuccess(List<DanhMucLoaiKhachHangModel> khachHangs) {
                radioButtons = khachHangs;
                checkDoneAPI(id);
            }

            @Override
            public void onFailure(String errorMessage) {
                checkDoneAPI(id);
            }
        });
    }

    private synchronized void checkDoneAPI(int id) {
        switch (id) {
            case 1:
                isDoneCity = true;
                break;
            case 2:
                isDoneDistrict = true;
                break;
            case 3:
                isDoneWards = true;
                break;
            case 4:
                isDoneDanhMuc = true;
                break;
        }

        if (isDoneCity && isDoneDistrict && isDoneWards && isDoneDanhMuc) {
            print("All done");
            hideLoading();
            mCitiesArrayAdapter = new SpinnerAdapter<>(this, citiesList);
            mSpinCity.setAdapter(mCitiesArrayAdapter);
            mSpinCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                    print("mSpinCity " + position);
                    Cities cities = (Cities) adapterView.getItemAtPosition(position);
                    districtList.clear();
                    wardsList.clear();
                    int thanhPhoId = cities.getTinh_ThanhPho_ID();
                    List<District> list = databaseManager.getDistrict(thanhPhoId);
                    if (list.size() > 0) {
                        mSpinDistrict.setEnabled(true);
                        districtList.addAll(list);
                        int quanHuyenId = districtList.get(0).getQuan_Huyen_ID();
                        List<Wards> wards = databaseManager.getWardsByDistrictId(quanHuyenId);
                        if (wards.size() > 0) {
                            mSpinWard.setEnabled(true);
                            mSpinWard.setBackgroundResource(R.drawable.border_spinner_enable);
                            wardsList.addAll(wards);
                        } else {
                            wardsList.add(Wards.newObject());
                            mSpinWard.setEnabled(false);
                            mSpinWard.setBackgroundResource(R.drawable.border_spinner_disable);
                        }
                    } else {
                        districtList.add(District.newObject());
                        mSpinDistrict.setEnabled(false);
                    }
                    mWardsArrayAdapter.notifyDataSetChanged();
                    mDistrictArrayAdapter.notifyDataSetChanged();
                    if (huyenId > 0) {
                        District district = findItem(list, model -> model.getQuan_Huyen_ID() == huyenId);
                        int index = list.indexOf(district);
                        mSpinDistrict.setSelection(index);
                        huyenId = 0;
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            mDistrictArrayAdapter = new SpinnerAdapter<>(this, districtList);
            mSpinDistrict.setAdapter(mDistrictArrayAdapter);
            mSpinDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                    print("mSpinDistrict " + position);
                    District district = (District) adapterView.getItemAtPosition(position);
                    wardsList.clear();
                    List<Wards> list = databaseManager.getWardsByDistrictId(district.getQuan_Huyen_ID());
                    if (list.size() > 0) {
                        mSpinWard.setEnabled(true);
                        mSpinWard.setBackgroundResource(R.drawable.border_spinner_enable);
                        wardsList.addAll(list);
                    } else {
                        mSpinWard.setEnabled(false);
                        mSpinWard.setBackgroundResource(R.drawable.border_spinner_disable);
                        wardsList.add(Wards.newObject());
                    }
                    mWardsArrayAdapter.notifyDataSetChanged();
                    if (xaID > 0) {
                        Wards wards = findItem(list, model -> model.getId() == xaID);
                        int index = list.indexOf(wards);
                        mSpinWard.setSelection(index);
                        xaID = 0;
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });


            mWardsArrayAdapter = new SpinnerAdapter<>(this, wardsList);
            mSpinWard.setAdapter(mWardsArrayAdapter);
            mSpinWard.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                    print("mSpinWard " + position);
                    // Wards wards = (Wards) adapterView.getItemAtPosition(position);

                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                    print("mSpinWard ");
                }
            });


            if (isFromUpdate) {
                showLoading();
                new Handler().postDelayed(this::getAddressById, 700);
            }
        }

    }

    private static class MyTask<T> extends AsyncTask<List<T>, Void, Void> {

        private WeakReference<AddNewCustomerActivity> activityReference;
        private int id;

        MyTask(AddNewCustomerActivity context, int id) {
            activityReference = new WeakReference<>(context);
            this.id = id;

        }

        @Override
        protected Void doInBackground(List<T>... lists) {
            if (lists.length == 0) return null;
            List<T> first = lists[0];
            switch (id) {
                case 1:
                    List<Cities> citiesList = (List<Cities>) first;
                    DatabaseManager.getInstance().insertCity(citiesList);
                    break;
                case 2:
                    List<District> districtList = (List<District>) first;
                    DatabaseManager.getInstance().inserDistrict(districtList);
                    break;
                case 3:
                    List<Wards> wardsList = (List<Wards>) first;
                    DatabaseManager.getInstance().insertWard(wardsList);
                    break;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            AddNewCustomerActivity activity = activityReference.get();
            if (activity == null || activity.isFinishing()) return;
            activity.checkDoneAPI(id);
        }
    }


    private void customization() {
        // set default check restaurant
        radioRestaurant.setChecked(true);
        txtCustomerCMT.setHint(getString(R.string.ci_update_general_info_business_code));
        setupToolbar(getString(R.string.ci_create_new_customer));
    }


    private void addEvents() {
        btnCreateCustomer.setOnClickListener((View v) -> {
            createCustomer();
        });

        customerTypeRadioGroup.setOnCheckedChangeListener((radioGroup, i) -> {
            switch (i) {
                case R.id.radioRestaurant:
                    txtCustomerCMT.setHint(R.string.ci_update_general_info_business_code);
                    break;
                default:
                    txtCustomerCMT.setHint(R.string.ci_update_general_info_identification);
                    break;
            }

        });
    }


    private void addNewPhoneNumberField(PhoneModel phoneModel) {
        View numberHolder = LayoutInflater.from(this).inflate(R.layout.item_edit_phone_number, contact_numbers_holder, false);
        TextView contact_number_type = numberHolder.findViewById(R.id.contact_number_type);
        ImageView imgRemovePhone = numberHolder.findViewById(R.id.img_remove_phone_number);
        if (phoneModel != null) {
            EditText customerName = numberHolder.findViewById(R.id.customerName);
            EditText contactName = numberHolder.findViewById(R.id.contact_number);
            customerName.setText(phoneModel.getTenNguoiSoHuu());
            contactName.setText(phoneModel.getSodienThoai());
            contact_number_type.setText(phoneModel.getTenDanhMuc());
            imgRemovePhone.setVisibility(View.GONE);
        } else {
            imgRemovePhone.setOnClickListener((View v) -> {
                showDialogConfirm(getString(R.string.common_remove_phone), () -> contact_numbers_holder.removeView(numberHolder));
            });
        }
        contact_number_type.setOnClickListener((View v) -> {
            PhoneTypeTypeDialog.newInstance().show(getSupportFragmentManager(), radioButtons, new PhoneTypeTypeDialog.PhoneTypeCallBack() {
                @Override
                public void onSetPhoneType(int i) {
                    DanhMucLoaiKhachHangModel model = radioButtons.get(i);
                    System.out.println("onSetPhoneType " + model.getTen() + "/" + model.getDanhMucID());
                    contact_number_type.setText(model.getTen());
                    contact_number_type.setTag(model.getDanhMucID());
                }

                @Override
                public void onSetOtherText(String otherText) {
                    contact_number_type.setText(otherText);
                }
            });
        });
        contact_numbers_holder.addView(numberHolder);
    }


    private boolean isEnterPhoneNumber() {
        if (listPhone == null) {
            listPhone = new ArrayList<>();
        }
        listPhone.clear();
        int numbersCount = contact_numbers_holder.getChildCount();
        for (int i = 0; i < numbersCount; i++) {
            View numberHolder = contact_numbers_holder.getChildAt(i);
            EditText inputName = numberHolder.findViewById(R.id.customerName);
            EditText edt_number = numberHolder.findViewById(R.id.contact_number);
            TextView typeCustomer = numberHolder.findViewById(R.id.contact_number_type);
            String name = inputName.getText().toString().trim();
            String phone = edt_number.getText().toString().trim();
            Object type = typeCustomer.getTag();
            int danhMucId = 0;
            if (type == null && radioButtons != null && radioButtons.size() > 0) {
                danhMucId = radioButtons.get(0).getDanhMucID();
            } else {
                danhMucId = Integer.parseInt(type.toString());
            }
            if (phone.isEmpty()) {
                edt_number.setError("Số điện thoại không được trống");
                return false;
            } else {
                listPhone.add(new PhoneModel(name, phone, danhMucId));
            }
        }
        return numbersCount > 0;
    }


    private void createCustomer() {
        if (!validate()) {
            return;
        }

        btnCreateCustomer.setEnabled(false);

        Cities cities = (Cities) mSpinCity.getSelectedItem();
        District district = (District) mSpinDistrict.getSelectedItem();
        Wards wards = (Wards) mSpinWard.getSelectedItem();

        showLoading();
        String fullName = txtCustomerFullName.getText().toString();
        String email = txtCustomerEmail.getText().toString();

        StringBuilder address = new StringBuilder();
        if (!wards.isDisable()) {
            address.append(wards.getTenXa());
        }
        if (!district.isDisable()) {
            address.append(" - ").append(district.getTen_Quan_Huyen());
        }

        if (!cities.isDisable()) {
            address.append(" - ").append(cities.getTen_Tinh_ThanhPho());
        }
        print(address.toString());

        String cmt = txtCustomerCMT.getText().toString();
        int idChecked = customerTypeRadioGroup.getCheckedRadioButtonId();
        int customerType = getCustomerType(idChecked);

        int id = mRadioGroupTypeUser.getCheckedRadioButtonId();
        CustomerType typeUser;
        switch (id) {
            case R.id.radio_on_sell:
            default:
                typeUser = CustomerType.SELL;
                break;
            case R.id.radio_potential:
                typeUser = CustomerType.POTENTIAL;
                break;
        }
        JSONObject customerObject = new JSONObject();
        try {
            if (isFromUpdate) {
                customerObject.put("guid", customerModel.getGuid());
                customerObject.put("iD_ThanhVien", customerModel.getiD_ThanhVien());
            }
            customerObject.put("tenDayDu", fullName);
            customerObject.put("email", email);
            customerObject.put("dienthoais", getPhones());//getFormPhoneNumbers
            customerObject.put("diaChi", address.toString());
            customerObject.put("cmt", cmt);
            customerObject.put("gioiTinh", "1");
            customerObject.put("maGioiThieu", "");
            customerObject.put("doiTuongId", String.valueOf(customerType));
            customerObject.put("ngaySinh", "");
            customerObject.put("tinhId", cities.getTinh_ThanhPho_ID());
            customerObject.put("QuanID", district.getQuan_Huyen_ID());
            customerObject.put("XaID", wards.getId());
            customerObject.put("SoNha", mNumberHouse.getText().toString());
            customerObject.put("latitude", 0);
            customerObject.put("PhanLoaiKhachHang", typeUser.getId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Logger.d(TAG, customerObject.toString());
        if (isFromUpdate) {
            NetworkManager.getInstance().sendPostRequest(ApiURL.UPDATE_CUSTOMER_URL, customerObject, new IAPICallback() {
                @Override
                public void onResponseSuccess(JSONObject response) {
                    btnCreateCustomer.setEnabled(true);
                    Log.d(TAG, response.toString());
                    if (response != null) {
                        Gson gson = new Gson();
                        Type type = new TypeToken<ResponseMessage>() {
                        }.getType();
                        ResponseMessage result = gson.fromJson(response.toString(), type);
                        if (result.getCode().equals("00")) {
                            customerModel.setTenDayDu(fullName);
                            customerModel.setEmail(email);
                            customerModel.setDiaChi(address.toString());
                            customerModel.setCmt(cmt);
                            customerModel.setDoiTuongText(getKinhDoanhString(customerType));

                            onUpdateCustomerSuccess();
                        } else {
                            onCreateCustomerFailed();
                        }
                        hideLoading();
                    }
                }

                @Override
                public void onResponseSuccess(JSONArray response) {

                }

                @Override
                public void onResponseError(ANError anError) {
                    onCreateCustomerFailed();
                    hideLoading();
                }
            });

        } else {
            NetworkManager.getInstance().sendPostRequest(ApiURL.CREATE_CUSTOMER_URL, customerObject, new IAPICallback() {
                @Override
                public void onResponseSuccess(JSONObject response) {
                    btnCreateCustomer.setEnabled(true);
                    if (response != null) {
                        Gson gson = new Gson();
                        Type type = new TypeToken<ResponseMessage>() {
                        }.getType();
                        ResponseMessage result = gson.fromJson(response.toString(), type);
                        if (result.getCode().equals("00")) {
                            onCreateCustomerSuccess();
                        } else {
                            onCreateCustomerFailed();
                        }
                        hideLoading();
                    }
                }

                @Override
                public void onResponseSuccess(JSONArray response) {

                }

                @Override
                public void onResponseError(ANError anError) {
                    onCreateCustomerFailed();
                    hideLoading();
                }
            });
        }

    }


    private JSONArray getPhones() {
        JSONArray array = new JSONArray();
        if (listPhone == null) return array;

        if (isFromUpdate) {
            List<PhoneModel> list = customerModel.toPhoneModel();
            for (int index = 0; index < listPhone.size(); index++) {
                PhoneModel phoneModel = listPhone.get(index);
                if (index < list.size()) {
                    PhoneModel server = list.get(index);
                    server.syncPhone(phoneModel);
                } else {
                    list.add(phoneModel);
                }
            }
            listPhone = list;
        }

        for (PhoneModel model : listPhone) {
            array.put(model.toJSONObject());
        }
        return array;
    }

    private void onCreateCustomerSuccess() {
        btnCreateCustomer.setEnabled(true);
        Toast.makeText(getBaseContext(), R.string.ci_create_customer_success, Toast.LENGTH_SHORT).show();
        finish();
    }

    private void onUpdateCustomerSuccess() {
        btnCreateCustomer.setEnabled(true);
        Toast.makeText(getBaseContext(), "Cập nhật thành công", Toast.LENGTH_SHORT).show();
        EventBus.getDefault().post(customerModel);
        finish();
    }

    private void onCreateCustomerFailed() {
        Toast.makeText(getBaseContext(), getString(R.string.have_error), Toast.LENGTH_SHORT).show();
        btnCreateCustomer.setEnabled(true);
    }


    private boolean validate() {
        boolean valid = true;
        String fullName = txtCustomerFullName.getText().toString();
        String email = txtCustomerEmail.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            txtCustomerEmail.setError(getString(R.string.ci_email_format_wrong));
            valid = false;
        } else {
            txtCustomerEmail.setError(null);
        }

        if (fullName.isEmpty()) {
            txtCustomerFullName.setError(getString(R.string.ci_customer_name_empty));
            valid = false;
        } else {
            txtCustomerFullName.setError(null);
        }

        if (!isEnterPhoneNumber()) {
            showMessage(getString(R.string.ci_customer_phone_empty));
            valid = false;
        }

        return valid;
    }


    /**
     * Update user
     */


    private void fillDataUpdateUser() {
        listPhone = customerModel.toPhoneModel();
        txtCustomerFullName.setText(customerModel.getTenDayDu());
        txtCustomerEmail.setText(customerModel.getEmail());
        txtCustomerCMT.setText(customerModel.getCmt());

        int doiTuongId = customerModel.getDoiTuongId();
        int id = LoaiKinhDoanh.Companion.getRadioByCustomType(doiTuongId);
        customerTypeRadioGroup.check(id);
        int phanLoaiKHTiemNang = customerModel.getPhanLoaiKHTiemNang();
        CustomerType type = CustomerType.Companion.getTypeById(phanLoaiKHTiemNang);
        switch (type) {
            case SELL:
                mRadioGroupTypeUser.check(R.id.radio_on_sell);
            case POTENTIAL:
                mRadioGroupTypeUser.check(R.id.radio_potential);
                break;
        }
    }

    private int huyenId, xaID;

    private void getAddressById() {
        String guid = customerModel.getGuid();
        AndroidNetworking.get(ApiURL.GET_ADDRESS_BY_MEMBER_URL)
                .addHeaders(ApiURL.getBaseHeader())
                .addHeaders(ApiURL.getTokenHeader())
                .addQueryParameter("guid", guid)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        hideLoading();
                        if (response != null) {
                            Log.d(TAG, response.toString());
                            Gson gson = new Gson();
                            Type type = new TypeToken<ArrayList<CustomerAddress>>() {
                            }.getType();
                            ArrayList<CustomerAddress> arrayCustomer = gson.fromJson(response.toString(), type);
                            if (arrayCustomer.size() > 0) {
                                CustomerAddress address = arrayCustomer.get(0);
                                mNumberHouse.setText(address.getSoNha());
                                int tinhID = address.getTinhID();
                                Cities cities = findItem(citiesList, model -> model.getTinh_ThanhPho_ID() == tinhID);
                                mSpinCity.setSelection(citiesList.indexOf(cities));
                                huyenId = address.getQuanID();
                                xaID = address.getXaID();
                            }
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        hideLoading();
                        showDialog(anError.getErrorDetail(), null);
                    }
                });
    }

    private <T> T findItem(List<T> list, IPredicate<T> predicate) {
        for (T model : list) {
            boolean result = predicate.condition(model);
            if (result) {
                return model;
            }
        }
        return null;
    }


    int getCustomerType(int idChecked) {
        if (idChecked == R.id.radioPersonal) {
            return PERSONAL;
        } else if (idChecked == R.id.radioAgency) {
            return AGENCY;
        }
        return RESTAURANT;
    }

    int getRadioByCustomType(int id) {
        if (id == PERSONAL) {
            return R.id.radioPersonal;
        } else if (id == AGENCY) {
            return R.id.radioAgency;
        }
        return R.id.radioRestaurant;
    }

    String getKinhDoanhString(int id) {
        if (id == PERSONAL) {
            return "Cá nhân";
        } else if (id == AGENCY) {
            return "Đại lý";
        }
        return "Nhà hàng";
    }

}
