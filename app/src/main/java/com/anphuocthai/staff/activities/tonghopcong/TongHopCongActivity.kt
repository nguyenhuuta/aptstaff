package com.anphuocthai.staff.activities.tonghopcong

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import com.anphuocthai.staff.R
import com.anphuocthai.staff.activities.BaseActivities
import com.anphuocthai.staff.api.*
import com.anphuocthai.staff.base.actionbar.CustomActionBar
import com.anphuocthai.staff.data.trackProgressBar
import com.anphuocthai.staff.entities.ChiTietCongResponse
import com.anphuocthai.staff.entities.TongHopCongModel
import com.anphuocthai.staff.entities.TongHopCongResponse
import com.anphuocthai.staff.utils.*
import com.anphuocthai.staff.utils.Enum
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import kotlinx.android.synthetic.main.item_chi_tiet_cong.view.*
import kotlinx.android.synthetic.main.item_tong_hop_cong.view.*
import kotlinx.android.synthetic.main.tong_hop_cong_activity.*
import java.util.*


/**
 * Created by OpenYourEyes on 8/5/2020
 */
class TongHopCongActivity : BaseActivities() {

    override fun getContentView(): Int {
        return R.layout.tong_hop_cong_activity
    }

    override fun initAction() {
        customActionBar.setTitle("Tổng hợp công")
        customActionBar.setCustomActionBar(object : CustomActionBar.ICustomActionBar {
            override fun onButtonRightClick() {
            }

            override fun onButtonLeftClick() {
                finish()
            }

        })
    }

    override fun initView() {
    }

    private val calendar: Calendar = Calendar.getInstance()
    override fun initData() {
        val month = DateConvert.convertDate(calendar.timeInMillis, DateConvert.TypeConvert.MONTH_YEAR)
        recyclerView.apply {
            adapter = ChiTietCongAdapter()
            layoutManager = LinearLayoutManager(this@TongHopCongActivity)
            setHasFixedSize(true)
        }

        fun calculatorTime(isNext: Boolean): ChiTietCongTemp {
            if (isNext) {
                calendar.add(Calendar.MONTH, 1)
            } else {
                calendar.add(Calendar.MONTH, -1)
            }
            val time = DateConvert.convertDate(calendar.timeInMillis, DateConvert.TypeConvert.MONTH_YEAR)
            requestAPI(month = time)
            val isNow = time == month
            return ChiTietCongTemp(time, isNow)
        }

        icPrev.setOnClickListener {
            icNext.visibility = View.VISIBLE
            val timePrev = calculatorTime(false)
            tongCongThang.text = "Tháng ${timePrev.time}"

        }
        icNext.setOnClickListener {
            val timeNext = calculatorTime(true)
            if (timeNext.isNow) {
                it.visibility = View.INVISIBLE
            }
            tongCongThang.text = "Tháng ${timeNext.time}"
        }
        tongCongThang.text = "Tháng ${month}"
        requestAPI(month = month)
    }

    private fun requestAPI(month: String) {

        val userId = dataManager.userInfo?.id?.toString()
        val source1 = ApiURL.tongHopCong.postRequest()
                .addQueryParameter("workMonth", month)
                .addQueryParameter("userId", userId)
                .addQueryParameter("PhongBanId", App.getInstance().departmentId.toString())
                .get(TongHopCongResponse::class.java)
                .doOnNext { printLog("source1 ${Thread.currentThread().name}") }

        val source2 = ApiURL.thongTinChamgCong.postRequest()
                .addQueryParameter("monthOfWork", month)
                .addQueryParameter("userId", userId)
                .addQueryParameter("PhongBanId", App.getInstance().departmentId.toString())
                .get(ChiTietCongResponse::class.java)
                .doOnNext { printLog("source2 ${Thread.currentThread().name}") }

        Observable.combineLatest(source1, source2,
                BiFunction<TongHopCongResponse, ChiTietCongResponse, Pair<TongHopCongResponse, ChiTietCongResponse>> { r1, r2 ->
                    Pair(r1, r2)
                }).runOnMain()
                .trackProgressBar(progressBar)
                .subscribe {
                    val tongHopCongResponse = it.first
                    val chiTietCongResponse = it.second
                    val tongHOpCongModel = tongHopCongResponse.data.firstOrNull()
                    val listChiTietCong = chiTietCongResponse.data ?: listOf()
                    val adapter = recyclerView.adapter as ChiTietCongAdapter
                    adapter.updateData(listChiTietCong)
                    fillTotal(tongHOpCongModel)
                }.addTo(compositeDisposable)
    }

    private fun fillTotal(tongHopCongModel: TongHopCongModel?) {
        fun fillItem(title: TextView, content: TextView, vararg list: String?) {
            title.text = list[0]
            content.text = " : ${list[1]}"
        }

        val value = Utils.formatValue((tongHopCongModel?.tongPhat?.toDouble()
                ?: 0.0), Enum.FieldValueType.CURRENCY)
        fillItem(tongCong.title, tongCong.content, "Tổng công", tongHopCongModel?.tongCong?.toString())
        fillItem(tongPhat.title, tongPhat.content, "Tổng phạt", value)
        fillItem(gioLamViec.title, gioLamViec.content, "Giờ làm việc", tongHopCongModel?.gioLamViec?.toString())
        fillItem(gioLamThem.title, gioLamThem.content, "Giờ làm thêm", tongHopCongModel?.gioLamThem?.toString())

    }


}