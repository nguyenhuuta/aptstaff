package com.anphuocthai.staff.activities.detailtable;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.activities.BaseActivities;
import com.anphuocthai.staff.base.actionbar.CustomActionBar;

/**
 * Created by DaiKySy on 9/6/19.
 */
public class DetailTableActivity extends BaseActivities {
    public static final String ARG_NAME = "customName";
    private CustomActionBar mCustomActionBar;

    @Override
    public int getContentView() {
        return R.layout.activities_detail_table;
    }

    @Override
    public void initView() {
        mCustomActionBar = findViewById(R.id.customActionBar);
        String customName = getIntent().getStringExtra(ARG_NAME);
        mCustomActionBar.setTitle(customName);
    }

    @Override
    public void initAction() {
        mCustomActionBar.setCustomActionBar(new CustomActionBar.ICustomActionBar() {
            @Override
            public void onButtonLeftClick() {
                finish();
            }

            @Override
            public void onButtonRightClick() {

            }
        });
        mCustomActionBar.showLeftRightButton(true, false);
    }

    @Override
    public void initData() {

    }
}
