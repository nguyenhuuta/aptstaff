package com.anphuocthai.staff.activities.tong_hop_phat

import android.view.View
import androidx.lifecycle.MutableLiveData
import com.anphuocthai.staff.R
import com.anphuocthai.staff.activities.BaseViewModel
import com.anphuocthai.staff.model.TongHopPhatModel
import com.anphuocthai.staff.utils.DateType
import com.anphuocthai.staff.utils.convertDate
import com.anphuocthai.staff.utils.extension.ListLiveData
import java.util.*

/**
 * Created by OpenYourEyes on 4/1/21
 */
class TongHopPhatViewModel : BaseViewModel() {
    var currentDate = ""
    val calendar: Calendar by lazy {
        val calendar = Calendar.getInstance()
        currentDate = calendar.convertDate(DateType.ONLY_DATE3)
        calendar
    }

    val timeBinding = MutableLiveData<String>()

    val buttonNextVisible = MutableLiveData<Int>()

    val listTongHopPhat = ListLiveData<TongHopPhatModel>()

    init {
        buttonNextVisible.postValue(View.INVISIBLE)
    }

    fun getListTongHopPhat() {
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        val month = calendar.get(Calendar.MONTH) + 1
        val year = calendar.get(Calendar.YEAR)
        val userId = dataManager.userInfoId
        val dateString = calendar.convertDate(DateType.ONLY_DATE3)
        timeBinding.postValue(dateString)
        if (dateString == currentDate) {
            buttonNextVisible.postValue(View.INVISIBLE)
        } else {
            buttonNextVisible.postValue(View.VISIBLE)
        }

        executeRequest(request = {
            getTongPhat(userId, day, month, year)
        }, response = {
            listTongHopPhat.postValue(it)
        })

    }


    fun onClick(view: View) {
        when (view.id) {
            R.id.buttonPrev -> calendar.add(Calendar.DAY_OF_MONTH, -1)
            R.id.buttonNext -> calendar.add(Calendar.DAY_OF_MONTH, 1)
        }
        getListTongHopPhat()
    }

}