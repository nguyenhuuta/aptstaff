package com.anphuocthai.staff.activities.chitietcongtac

import android.app.ProgressDialog
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.anphuocthai.staff.BR
import com.anphuocthai.staff.R
import com.anphuocthai.staff.activities.ViewModelFactory
import com.anphuocthai.staff.activities.danhsachcongtac.DanhSachCongTacVM
import com.anphuocthai.staff.api.addTo
import com.anphuocthai.staff.databinding.ChitietCongtacActivityBinding
import com.anphuocthai.staff.utils.App
import com.anphuocthai.staff.utils.Utils
import com.anphuocthai.staff.utils.printLog
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Consumer
import kotlinx.android.synthetic.main.chitiet_congtac_activity.*
import kotlinx.android.synthetic.main.danhsach_congtac_activity.toolbar

/**
 * Created by OpenYourEyes on 12/30/2020
 */
class ChiTietCongTacActivity : AppCompatActivity() {
    companion object {
        const val ARG_ID = "arg_id"
        const val ARG_TIME = "arg_time"
    }

    var compositeDisposable = CompositeDisposable()
    lateinit var factory: ViewModelFactory
    lateinit var viewModel: DanhSachCongTacVM
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ChitietCongtacActivityBinding = DataBindingUtil.setContentView(this, R.layout.chitiet_congtac_activity)
        binding.lifecycleOwner = this
        factory = App.getComponent().viewModelFractory()
        viewModel = ViewModelProvider(this, factory).get(DanhSachCongTacVM::class.java)
        binding.setVariable(BR.viewModel, viewModel)
        initView()
        bindViewModel()
    }


    fun initView() {
        toolbar.setTitle("Chi tiết")
        toolbar.setCustomActionBar {
            finish()
        }
    }

    private fun bindViewModel() {


        viewModel.loading
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(Consumer { aBoolean: Boolean ->
                    if (aBoolean) {
                        showProgress()
                    } else {
                        hideProgress()
                    }
                }).addTo(compositeDisposable)

        recycleChiTietCongTac.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@ChiTietCongTacActivity, LinearLayoutManager.VERTICAL, true)
        }
        val id = intent.getIntExtra(ARG_ID, 0)
        val time = intent.getStringExtra(ARG_TIME) ?: ""
        printLog("id $id $time")
        viewModel.DuLieuChamCongId = id
        viewModel.ThoiGianChamCong = time
        viewModel.chiTietDanhSach()
        viewModel.detailDanhSach.observe(this, Observer {
            val adapter = ChiTietCongTacAdapter(this, it)
            recycleChiTietCongTac.adapter = adapter
        })

        viewModel.insertItem.observe(this, Observer {
            val adapter = recycleChiTietCongTac.adapter as ChiTietCongTacAdapter
            adapter.insertItem(it)
            recycleChiTietCongTac.post {
                recycleChiTietCongTac.smoothScrollToPosition(0)
            }
        })
    }

    private var mProgressDialog: ProgressDialog? = null
    private fun showProgress() {
        if (mProgressDialog == null) {
            mProgressDialog = Utils.showLoadingDialog(this)
        }
        if (!isDestroyed && !isFinishing) {
            mProgressDialog!!.show()
        }
    }

    private fun hideProgress() {
        if (mProgressDialog != null && mProgressDialog!!.isShowing && !isDestroyed && !isFinishing) {
            mProgressDialog!!.dismiss()
        }
    }


}