package com.anphuocthai.staff.activities.listquychehanhchinh

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.anphuocthai.staff.R
import com.anphuocthai.staff.model.QuyCheHanhChinhNhanhSuModel
import com.anphuocthai.staff.utils.extension.loadImage
import kotlinx.android.synthetic.main.item_quy_che_hanhchinh_nhansu.view.*

/**
 * Created by OpenYourEyes on 10/14/2020
 */
class QuyCheHanhChinhAdapter(val onItemClick: (QuyCheHanhChinhNhanhSuModel) -> Unit) : RecyclerView.Adapter<QuyCheHanhChinhAdapter.QuyCheHanhChinhVH>() {
    val list: MutableList<QuyCheHanhChinhNhanhSuModel> = mutableListOf()
    fun updateList(new: MutableList<QuyCheHanhChinhNhanhSuModel>) {
        list.clear()
        list.addAll(new)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): QuyCheHanhChinhVH {
        val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_quy_che_hanhchinh_nhansu, viewGroup, false)
        return QuyCheHanhChinhVH(view)
    }

    override fun onBindViewHolder(viewHolder: QuyCheHanhChinhVH, position: Int) {
        viewHolder.bindData(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }


    inner class QuyCheHanhChinhVH(val view: View) : RecyclerView.ViewHolder(view) {
        fun bindData(model: QuyCheHanhChinhNhanhSuModel) {
            val imageId = model.url?.toInt() ?: 0
            view.imageQuyChe.loadImage(imageId)
            view.tenQuyChe.text = model.ten
            view.tenBoPhan.text = "Tên bộ phận: ${model.tenBoPhan}"
            view.setOnClickListener {
                onItemClick(model)
            }
        }
    }
}