package com.anphuocthai.staff.activities.listquychehanhchinh

import android.content.Intent
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.anphuocthai.staff.R
import com.anphuocthai.staff.activities.BaseActivities
import com.anphuocthai.staff.activities.webview.WebViewActivity
import com.anphuocthai.staff.activities.webview.WebViewType
import com.anphuocthai.staff.api.*
import com.anphuocthai.staff.base.actionbar.CustomActionBar
import com.anphuocthai.staff.data.trackProgressBar
import com.anphuocthai.staff.model.QuyCheHanhChinhNhanhSuModel
import com.anphuocthai.staff.model.QuyCheHanhChinhNhanhSuResponse
import kotlinx.android.synthetic.main.list_quy_che_hanh_chinh_nhansu_activity.*

/**
 * Created by OpenYourEyes on 10/13/2020
 */
class ListQuyCheHanhChinhNhanSuActivity : BaseActivities(), SwipeRefreshLayout.OnRefreshListener {
    override fun getContentView(): Int {
        return R.layout.list_quy_che_hanh_chinh_nhansu_activity
    }

    override fun initView() {
        toolbar.setTitle("Quy chế hành chính nhân sự")
        toolbar.setCustomActionBar(object : CustomActionBar.ICustomActionBar {
            override fun onButtonLeftClick() {
                finish()
            }

            override fun onButtonRightClick() {
            }

        })
        recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@ListQuyCheHanhChinhNhanSuActivity)
            adapter = QuyCheHanhChinhAdapter {
                val intent = Intent(this@ListQuyCheHanhChinhNhanSuActivity, WebViewActivity::class.java)
                intent.putExtra(WebViewActivity.WEB_VIEW_TYPE, WebViewType.QuyCheHanhChinhNhanSu)
                intent.putExtra(WebViewActivity.URL_ARG, it.getLink())
                this@ListQuyCheHanhChinhNhanSuActivity.startActivity(intent)
            }
        }
    }

    override fun initAction() {
        swipeRefresh.setOnRefreshListener(this)

    }

    override fun initData() {
        ApiURL.QuyCheHanhChinhNhanSu.getRequest()
                .get(QuyCheHanhChinhNhanhSuResponse::class.java)
                .trackProgressBar(progressBar)
                .runOnMain()
                .subscribe({
                    if (it != null && it.data != null) {
                        fillData(it.data)
                    }
                }, {
                    showDialog(it.localizedMessage, null)
                }).addTo(compositeDisposable)

    }

    private fun fillData(list: MutableList<QuyCheHanhChinhNhanhSuModel>) {
        if (list.size == 0) {
            noData.visibility = View.VISIBLE
            return
        }
        noData.visibility = View.GONE
        val adapter = recyclerView.adapter as QuyCheHanhChinhAdapter
        adapter.updateList(list)
    }


    override fun onRefresh() {
        swipeRefresh.isRefreshing = false
        initData()
    }
}