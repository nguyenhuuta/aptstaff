package com.anphuocthai.staff.activities.customer;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.androidnetworking.error.ANError;
import com.anphuocthai.staff.R;
import com.anphuocthai.staff.api.ApiURL;
import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.interfaces.IAPICallback;
import com.anphuocthai.staff.model.ResponseMessage;
import com.anphuocthai.staff.model.customer.Schedule;
import com.anphuocthai.staff.ui.base.BaseActivity;
import com.anphuocthai.staff.utils.App;
import com.anphuocthai.staff.utils.DateUtils;
import com.anphuocthai.staff.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.Calendar;
import java.util.Date;

public class AddNewWorkScheduleActivity extends BaseActivity {

    /**
     * Variables
     */
    private static final String TAG = AddNewWorkScheduleActivity.class.getSimpleName();
    Calendar myCalendar = Calendar.getInstance();
    Calendar timeCalendar = Calendar.getInstance();
    private String guid;
    private String customerName;
    // sent to UPDATE
    private Schedule schedule;
    private boolean isEdit = false;

    /**
     * UI
     */
    //private Toolbar toolbar;
    private EditText txtNameWorkSchedule;
    private EditText txtDescriptionWorkSchedule;
    private EditText txtLocationWorkSchedule;
    private Button btnCreateNewWorkSchedule;
    private ImageView imgDateAddWorkSchedule;
    private TextView txtDateSchedule;
    private ImageView imgTimeAddWorkSchedule;
    private TextView txtTimeSchedule;
    private TextView txtWithCustomer;

    // radio button
    private RadioGroup typeWSRadioGroup;
    private RadioButton debtRadioButton;
    private RadioButton customerCareRadioButton;
    private RadioButton offerRadioButton;
    private RadioButton otherRadioButton;


    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateDateLabel();
        }

    };

    TimePickerDialog.OnTimeSetListener timeSetListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

            timeCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
            timeCalendar.set(Calendar.MINUTE, minute);
            updateTimeLabel();
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_work_schedule);
        handleIntent();
        setupToolbarAndTitle();

        addControls();
        setupRadioGroup();
        setCurrentDayAndTime();
        App.setContext(this);
        addEvents();
        placeTextIfEdit();
    }

    private void placeTextIfEdit() {
        if (isEdit) {
            txtWithCustomer.setText(schedule.getTenThanhVien());
            txtNameWorkSchedule.setText(schedule.getTen());
            txtDescriptionWorkSchedule.setText(schedule.getMoTa());
            txtLocationWorkSchedule.setText(schedule.getViTri());
            txtDateSchedule.setText(Utils.formatDateStringWithFormat("yyyy-MM-dd HH:mm:ss", "dd-MM-yyyy", schedule.getNgayBatDau()));
            txtTimeSchedule.setText(Utils.formatDateStringWithFormat("yyyy-MM-dd HH:mm:ss", "HH:mm", schedule.getNgayBatDau()));
        }
    }

    private void setupToolbarAndTitle() {
        if (isEdit) {
            setupToolbar(getString(R.string.edit_work_schedule_name));
        } else {
            setupToolbar(getString(R.string.add_work_schedule_name));
        }
    }

    private void handleIntent() {
        Bundle b = getIntent().getBundleExtra("schedulenewworkextra");
        isEdit = b.getBoolean("isEdit");
        if (isEdit) {
            schedule = (Schedule) b.getSerializable("schedule");
            this.guid = schedule.getGuidThanhVien();
            this.customerName = schedule.getTenThanhVien();
        } else {
            this.guid = b.getString("guid");
            this.customerName = b.getString("customername");
        }

    }

    private void addControls() {
        txtNameWorkSchedule = findViewById(R.id.full_work_schedule_name);
        txtDescriptionWorkSchedule = findViewById(R.id.work_schedule_description);
        txtLocationWorkSchedule = findViewById(R.id.work_schedule_location);
        btnCreateNewWorkSchedule = findViewById(R.id.btn_create_work_schedule);
        if (isEdit) {
            btnCreateNewWorkSchedule.setText(getString(R.string.common_update));
        } else {
            btnCreateNewWorkSchedule.setText(getString(R.string.ci_btn_add_new_ws_text));
        }

        imgDateAddWorkSchedule = findViewById(R.id.imgDateAddWorkSchedule);
        txtDateSchedule = findViewById(R.id.txtDateSchedule);
        imgTimeAddWorkSchedule = findViewById(R.id.imgTimeAddWorkSchedule);
        txtTimeSchedule = findViewById(R.id.txtTimeSchedule);
        txtWithCustomer = findViewById(R.id.ci_txt_add_new_ws_with_customer);
        txtWithCustomer.setText(customerName);

        // type ws radio group
        typeWSRadioGroup = findViewById(R.id.ci_ws_type_ws_radio_group);
        debtRadioButton = findViewById(R.id.ci_ws_debt_radiobutton);
        customerCareRadioButton = findViewById(R.id.ci_ws_customer_care_radiobutton);
        offerRadioButton = findViewById(R.id.ci_ws_offer_radiobutton);
        otherRadioButton = findViewById(R.id.ci_ws_other_radiobutton);
    }

    private void setupRadioGroup() {
        typeWSRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.ci_ws_debt_radiobutton:
                        txtNameWorkSchedule.setText(getString(R.string.ci_ws_debt_text) + " : " + customerName);
                        txtDescriptionWorkSchedule.setText(R.string.ci_ws_debt_text);
                        break;
                    case R.id.ci_ws_customer_care_radiobutton:
                        txtNameWorkSchedule.setText(getString(R.string.ci_ws_custormer_care) + " : " + customerName);
                        txtDescriptionWorkSchedule.setText(R.string.ci_ws_custormer_care);
                        break;
                    case R.id.ci_ws_offer_radiobutton:
                        txtNameWorkSchedule.setText(getString(R.string.ci_ws_offer_text) + " : " + customerName);
                        txtDescriptionWorkSchedule.setText(R.string.ci_ws_offer_text);
                        break;
                    case R.id.ci_ws_other_radiobutton:
                        txtNameWorkSchedule.setText("");
                        txtDescriptionWorkSchedule.setText("");
                        break;
                }
            }
        });

        typeWSRadioGroup.check(R.id.ci_ws_customer_care_radiobutton);
    }

    private void setCurrentDayAndTime() {
        txtDateSchedule.setText(Utils.formatDateWithFormat("dd-MM-yyyy", new Date()));
        txtTimeSchedule.setText(Utils.formatDateWithFormat("HH:mm", new Date()));
    }

    private void addEvents() {
        btnCreateNewWorkSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createNewWorkSchedule();
            }
        });

        imgDateAddWorkSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog dialog =
                        new DatePickerDialog(AddNewWorkScheduleActivity.this,
                                R.style.MaterialDatePickerTheme,
                                date,
                                myCalendar.get(Calendar.YEAR),
                                myCalendar.get(Calendar.MONTH),
                                myCalendar.get(Calendar.DAY_OF_MONTH));
                dialog.getDatePicker().setMinDate(new Date().getTime());
                dialog.getDatePicker().setMaxDate(DateUtils.getNextDayDate(14).getTime());
                dialog.show();
            }
        });

        imgTimeAddWorkSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new TimePickerDialog(AddNewWorkScheduleActivity.this,
                        R.style.MaterialDatePickerTheme,
                        timeSetListener,
                        timeCalendar.get(Calendar.HOUR_OF_DAY),
                        timeCalendar.get(Calendar.MINUTE),
                        false)
                        .show();

            }
        });

        txtDateSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(AddNewWorkScheduleActivity.this,
                        R.style.MaterialDatePickerTheme,
                        date,
                        myCalendar.get(Calendar.YEAR),
                        myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH))
                        .show();
            }
        });
        txtTimeSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new TimePickerDialog(AddNewWorkScheduleActivity.this,
                        R.style.MaterialDatePickerTheme,
                        timeSetListener,
                        timeCalendar.get(Calendar.HOUR_OF_DAY),
                        timeCalendar.get(Calendar.MINUTE),
                        false)
                        .show();
            }
        });
    }

    private void updateDateLabel() {
        txtDateSchedule.setText(Utils.formatDateWithFormat("dd-MM-yyyy", myCalendar.getTime()));
    }

    private void updateTimeLabel() {
        txtTimeSchedule.setText(Utils.formatDateWithFormat("HH:mm", timeCalendar.getTime()));
    }


    private void createNewWorkSchedule() {
        if (!validate()) {
            onCreateWorkScheduleFailed();
            return;
        }
        btnCreateNewWorkSchedule.setEnabled(false);
        showLoading();
        String name = txtNameWorkSchedule.getText().toString();
        String description = txtDescriptionWorkSchedule.getText().toString();
        String location = txtLocationWorkSchedule.getText().toString();

        String id = new String();
        if (isEdit) {
            id = String.valueOf(schedule.getId());
        } else {
            id = "0";
        }

        JSONObject newWSObject = new JSONObject();
        try {
            newWSObject.put("id", id);
            newWSObject.put("guidThanhVien", this.guid);
            newWSObject.put("ten", name);
            newWSObject.put("moTa", description);
            newWSObject.put("ngayDuKien", Utils.formatDateStringWithFormat("dd-MM-yyyy", "yyyy-MM-dd", txtDateSchedule.getText().toString()) +
                    " "
                    +
                    txtTimeSchedule.getText());
            newWSObject.put("viTri", location);
        } catch (Exception e) {
            e.printStackTrace();
        }

        NetworkManager.getInstance().sendPostRequest(ApiURL.CREATE_OR_UPDATE_WORK_SCHEDULE_URL, newWSObject, new IAPICallback() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                btnCreateNewWorkSchedule.setEnabled(true);
                Log.d(TAG, response.toString());
//                if (response != null) {
//                    onCreateWorkScheduleSuccess();
//                    finish();
//                }

                if (response != null) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<ResponseMessage>() {}.getType();
                    ResponseMessage message =   gson.fromJson(response.toString(), type);
                    if (message.getCode().trim().equals("00")) {
                        onCreateWorkScheduleSuccess();
                    }
                    showMessage(message.getMessage());
                    finish();
                }
                hideLoading();
            }

            @Override
            public void onResponseSuccess(JSONArray response) {
                hideLoading();
            }

            @Override
            public void onResponseError(ANError anError) {
                onCreateWorkScheduleFailed();
            }
        });
    }


    private void onCreateWorkScheduleFailed() {
        Toast.makeText(getBaseContext(), getString(R.string.have_error), Toast.LENGTH_SHORT).show();
        btnCreateNewWorkSchedule.setEnabled(true);
    }

    private void onCreateWorkScheduleSuccess() {
        btnCreateNewWorkSchedule.setEnabled(true);
        if (isEdit) {
            Toast.makeText(getBaseContext(), getString(R.string.ci_update_work_schedule_success), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getBaseContext(), getString(R.string.ci_create_new_work_schedule_success), Toast.LENGTH_SHORT).show();
        }


    }

    private boolean validate() {
        boolean valid = true;
        String name = txtNameWorkSchedule.getText().toString();
        String description = txtDescriptionWorkSchedule.getText().toString();
        //String date = txtDateWorkSchedule.getText().toString();
        String location = txtLocationWorkSchedule.getText().toString();

        if (name.isEmpty()) {
            txtNameWorkSchedule.setError(getString(R.string.ci_work_schedule_name_empty));
            valid = false;
        } else {
            txtNameWorkSchedule.setError(null);
        }

        if (location.isEmpty()) {
            txtLocationWorkSchedule.setError(getString(R.string.ci_work_schedule_address));
            valid = false;
        } else {
            txtLocationWorkSchedule.setError(null);
        }

        return valid;
    }
}
