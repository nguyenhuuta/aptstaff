package com.anphuocthai.staff.activities.kpi

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.anphuocthai.staff.BR
import com.anphuocthai.staff.R
import com.anphuocthai.staff.model.KPIModel

/**
 * Created by OpenYourEyes on 3/25/21
 */
class KPIAdapter(context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val layoutInflater: LayoutInflater = LayoutInflater.from(context)

    private val typeTitle = 1
    private val typeNormal = 2

    private val mList = mutableListOf<KPIModel>()
    fun updateList(list: MutableList<KPIModel>) {
        mList.clear()
        mList.addAll(list)
        notifyDataSetChanged()
    }

    private fun getItem(position: Int): KPIModel {
        return mList[position]
    }

    override fun getItemViewType(position: Int): Int {
        val model = getItem(position)
        if (model.position == -1) {
            return typeTitle
        }
        return typeNormal
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == typeTitle) {
            val view: ViewDataBinding =
                    DataBindingUtil.inflate(layoutInflater, R.layout.item_kpi_title, parent, false)
            return KPITitleVH(view)
        }
        val view: ViewDataBinding =
                DataBindingUtil.inflate(layoutInflater, R.layout.item_kpi, parent, false)

        return KPIVH(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val model = getItem(position)
        if (holder is KPITitleVH) {
            holder.bindData(model.tenNhomChiTieu ?: "")
        }
        if (holder is KPIVH) {
            holder.bindData(model)
        }

    }

    override fun getItemCount(): Int {
        return mList.size
    }

    class KPIVH(private val viewBinding: ViewDataBinding) : RecyclerView.ViewHolder(viewBinding.root) {
        fun bindData(model: KPIModel) {
            viewBinding.setVariable(BR.model, model)
            viewBinding.executePendingBindings()
        }
    }

    class KPITitleVH(private val viewBinding: ViewDataBinding) : RecyclerView.ViewHolder(viewBinding.root) {
        fun bindData(model: String) {
            viewBinding.setVariable(BR.model, model)
            viewBinding.executePendingBindings()
        }
    }


}