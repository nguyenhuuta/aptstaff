package com.anphuocthai.staff.activities.danhsachcongtac

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.anphuocthai.staff.BR
import com.anphuocthai.staff.R
import com.anphuocthai.staff.activities.ViewModelFactory
import com.anphuocthai.staff.activities.chitietcongtac.ChiTietCongTacActivity
import com.anphuocthai.staff.api.*
import com.anphuocthai.staff.databinding.DanhsachCongtacActivityBinding
import com.anphuocthai.staff.utils.App
import kotlinx.android.synthetic.main.danhsach_congtac_activity.*

/**
 * Created by OpenYourEyes on 12/28/20
 */
class DanhSachCongTacActivity : AppCompatActivity() {
    lateinit var factory: ViewModelFactory
    lateinit var viewModel: DanhSachCongTacVM
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: DanhsachCongtacActivityBinding = DataBindingUtil.setContentView(this, R.layout.danhsach_congtac_activity)
        factory = App.getComponent().viewModelFractory()
        viewModel = ViewModelProvider(this, factory).get(DanhSachCongTacVM::class.java)
        binding.setVariable(BR.viewModel, viewModel)
        initView()
        bindViewModel()
    }

    fun initView() {
        toolbar.setTitle("Danh sách công tác")
        toolbar.setCustomActionBar {
            finish()
        }
    }

    private fun bindViewModel() {
        recycleDanhSachCongTac.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@DanhSachCongTacActivity)
            addItemDecoration(DividerItemDecoration(this@DanhSachCongTacActivity, DividerItemDecoration.VERTICAL))
        }

        viewModel.getDanhSach()
        viewModel.listDanhSach.observe(this, Observer {
            val adapter = DanhSachCongTacAdapter(this, it) { position, model ->
                if (model.hasNew) {
                    model.hasNew = false
                    recycleDanhSachCongTac.adapter?.notifyItemChanged(position)
                    viewModel.updateCongTac(model)
                }

                val intent = Intent(this, ChiTietCongTacActivity::class.java)
                intent.putExtra(ChiTietCongTacActivity.ARG_ID, model.id)
                intent.putExtra(ChiTietCongTacActivity.ARG_TIME, model.ngayChamCong)
                startActivity(intent)
            }
            recycleDanhSachCongTac.adapter = adapter
        })
    }


}