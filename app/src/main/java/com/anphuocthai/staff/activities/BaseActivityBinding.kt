package com.anphuocthai.staff.activities

import android.app.ProgressDialog
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProvider
import com.anphuocthai.staff.BR
import com.anphuocthai.staff.R
import com.anphuocthai.staff.api.ErrorMessage
import com.anphuocthai.staff.base.actionbar.CustomActionBar
import com.anphuocthai.staff.data.DataManager
import com.anphuocthai.staff.fragments.dialogmessage.DialogMessage
import com.anphuocthai.staff.fragments.dialogmessage.DialogMessage.IOnClickListener
import com.anphuocthai.staff.utils.App
import com.anphuocthai.staff.utils.Utils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * Created by OpenYourEyes on 2/25/21
 */

abstract class BaseActivityBinding<VM : BaseViewModel, DB : ViewDataBinding> : AppCompatActivity() {


    lateinit var factory: ViewModelFactory

    val dataManager: DataManager by lazy {
        App.getComponent().dataManager
    }

    private val mDisposable: CompositeDisposable by lazy {
        CompositeDisposable()
    }

    private val progressDialog: ProgressDialog by lazy {
        Utils.showLoadingDialog(this)
    }

    abstract fun createViewModel(): Class<VM>

    abstract fun layoutId(): Int

    abstract fun titleToolBar(): String

    abstract fun initView()

    abstract fun bindViewModel()


    lateinit var viewModel: VM
    lateinit var dataBinding: DB

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        factory = App.getComponent().viewModelFractory()
        dataBinding = DataBindingUtil.setContentView(this, layoutId())
        viewModel = ViewModelProvider(this, factory).get(createViewModel())
        dataBinding.setVariable(BR.viewModel, viewModel)
        dataBinding.lifecycleOwner = this
        defaultBinding()
        initView()
        bindViewModel()
    }


    fun initToolbar(customActionBar: CustomActionBar) {
        customActionBar.setTitle(titleToolBar())
        customActionBar.setCustomActionBar(object : CustomActionBar.ICustomActionBar {
            override fun onButtonRightClick() {
            }

            override fun onButtonLeftClick() {
                finish()
            }

        })
    }

    private fun defaultBinding() {
        viewModel.showDialog
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    handleError(it)
                }.addDisposable()

        viewModel.showLoading
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    if (it) {
                        showProgress()
                    } else {
                        hideProgress()
                    }
                }.addDisposable()

    }

    open fun handleError(errorMessage: ErrorMessage) {
        showDialog(errorMessage.message())
    }

    open fun showDialog(message: String?, buttonOk: String = getString(R.string.OK), callback: IOnClickListener? = null) {
        val dialogMessage = DialogMessage.newInstance(message!!, buttonOk!!)
        dialogMessage.setCallback(callback)
        dialogMessage.show(supportFragmentManager, null)
    }

    open fun showProgress() {
        if (!isDestroyed && !isFinishing && !progressDialog.isShowing) {
            progressDialog.show()
        }
    }

    open fun hideProgress() {
        if (!isDestroyed && !isFinishing) {
            progressDialog.dismiss()
        }
    }


    fun Disposable.addDisposable() {
        mDisposable.add(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        mDisposable.dispose()
    }

}