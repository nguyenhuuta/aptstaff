package com.anphuocthai.staff.activities.xinnghiphep.yeucaunghi

import android.content.Intent
import androidx.lifecycle.Observer
import com.anphuocthai.staff.R
import com.anphuocthai.staff.activities.BaseActivityBinding
import com.anphuocthai.staff.activities.xinnghiphep.danhsachnghiphep.DanhSachXinNghiActivity
import com.anphuocthai.staff.databinding.YeucaunghiActivityBinding
import com.anphuocthai.staff.fragments.dialogmessage.DialogMessage
import com.anphuocthai.staff.utils.printLog
import kotlinx.android.synthetic.main.yeucaunghi_activity.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


/**
 * Created by OpenYourEyes on 2/25/21
 */
class YeuCauNghiActivity : BaseActivityBinding<YeuCauNghiVM, YeucaunghiActivityBinding>() {
    override fun createViewModel(): Class<YeuCauNghiVM> {
        return YeuCauNghiVM::class.java
    }

    override fun layoutId(): Int {
        return R.layout.yeucaunghi_activity
    }

    override fun titleToolBar(): String {
        return "Yêu cầu nghỉ"
    }

    override fun initView() {
        initToolbar(toolbar)
    }

    override fun bindViewModel() {
        viewModel.selectedTypeTime.observe(this, Observer {
            inputHour.isEnabled = it != 0
            if (it == 0) {
                viewModel.valueHour.postValue(viewModel.totalTime().toString())
            } else {
                inputHour.setText("4.0")
            }
        })

        viewModel.xinNghiThanhCong.observe(this, Observer {
            showDialog("Xin nghỉ thành công", callback = DialogMessage.IOnClickListener {
                val returnIntent = Intent()
                returnIntent.putExtra(DanhSachXinNghiActivity.ThemYeuCauModel, it)
                setResult(RESULT_OK, returnIntent)
                finish()
            })
        })
        CoroutineScope(Dispatchers.Main).launch {
            showProgress()
            try {
                viewModel.getCauHinhLamViec()
                val response = viewModel.getLoaiNghiPhep()
                if (response.isSuccess) {
                    val list = response.data
                    viewModel.listTypeTakeLeave.postValue(list)
                }
            } catch (e: Exception) {
                printLog("Exception $e")
            }
            hideProgress()
        }


    }
}