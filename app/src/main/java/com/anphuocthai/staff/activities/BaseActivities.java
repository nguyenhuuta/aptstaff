package com.anphuocthai.staff.activities;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.baserequest.APIService;
import com.anphuocthai.staff.baserequest.IAppAPI;
import com.anphuocthai.staff.data.DataManager;
import com.anphuocthai.staff.data.RxProgressBar;
import com.anphuocthai.staff.fragments.dialogmessage.DialogMessage;
import com.anphuocthai.staff.utils.App;
import com.anphuocthai.staff.utils.Logger;
import com.anphuocthai.staff.utils.Utils;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * Created by DaiKySy on 8/26/19.
 */
public abstract class BaseActivities extends AppCompatActivity {

    public IAppAPI mAPI;

    private ProgressDialog mProgressDialog;
    public DataManager dataManager;
    public CompositeDisposable compositeDisposable = new CompositeDisposable();
    public RxProgressBar progressBar = new RxProgressBar();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dataManager = App.getComponent().getDataManager();
        print("===================================#onCreate=====================================");
        setContentView(getContentView());
        mAPI = APIService.getInstance().getAppAPI();
        initView();
        initAction();
        initData();
        Disposable disposable = progressBar
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aBoolean -> {
                    if (aBoolean) {
                        showProgress();
                    } else {
                        hideProgress();
                    }
                });
        compositeDisposable.add(disposable);
    }

//    @Override
//    protected void onStart() {
//        super.onStart();
//        final ViewGroup viewGroup = (ViewGroup) ((ViewGroup) this
//                .findViewById(android.R.id.content)).getChildAt(0);
//        viewGroup.setFitsSystemWindows(true);
//    }

    public abstract int getContentView();

    public abstract void initView();

    public abstract void initAction();

    public abstract void initData();

    public void print(String message) {
        Logger.d(this.getClass().getSimpleName(), message);
    }

    public void showProgress() {
        if (mProgressDialog == null) {
            mProgressDialog = Utils.showLoadingDialog(this);
        }

        if (!isDestroyed() && !isFinishing()) {
            mProgressDialog.show();
        }
    }

    public void hideProgress() {
        if (mProgressDialog != null && mProgressDialog.isShowing() && !isDestroyed() && !isFinishing()) {
            mProgressDialog.dismiss();
        }
    }

    public void showDialog(String message, String buttonOk, DialogMessage.IOnClickListener callback) {
        DialogMessage dialogMessage = DialogMessage.newInstance(message, buttonOk);
        dialogMessage.setCallback(callback);
        dialogMessage.show(getSupportFragmentManager(), null);
    }

    public void showDialog(String content, DialogMessage.IOnClickListener callback) {
        DialogMessage dialogMessage = DialogMessage.newInstance(content, getString(R.string.OK));
        dialogMessage.setCallback(callback);
        dialogMessage.show(getSupportFragmentManager(), null);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeDisposable.dispose();
    }
}
