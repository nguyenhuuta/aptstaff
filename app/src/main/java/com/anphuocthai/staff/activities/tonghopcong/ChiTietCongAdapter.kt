package com.anphuocthai.staff.activities.tonghopcong

import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.anphuocthai.staff.R
import com.anphuocthai.staff.entities.ChiTietCongModel
import com.anphuocthai.staff.utils.DateType
import com.anphuocthai.staff.utils.Utils
import com.anphuocthai.staff.utils.convertDate
import com.anphuocthai.staff.utils.printLog
import kotlinx.android.synthetic.main.item_chi_tiet_cong.view.*
import kotlinx.android.synthetic.main.item_tong_hop_cong.view.*

/**
 * Created by OpenYourEyes on 8/6/2020
 */
data class ChiTietCongTemp(
        val time: String,
        val isNow: Boolean
)

class ChiTietCongAdapter : RecyclerView.Adapter<ChiTietCongAdapter.ViewHolder>() {
    private val listChiTiet: MutableList<ChiTietCongModel> = mutableListOf()
    fun updateData(list: List<ChiTietCongModel>) {
        listChiTiet.clear()
        listChiTiet.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ChiTietCongAdapter.ViewHolder {
        val layoutInflater: LayoutInflater = LayoutInflater.from(viewGroup.context)
        val view = layoutInflater.inflate(R.layout.item_chi_tiet_cong, viewGroup, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listChiTiet.size
    }

    override fun onBindViewHolder(viewHolder: ChiTietCongAdapter.ViewHolder, position: Int) {
        viewHolder.bindData(listChiTiet[position])
    }


    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        init {
            view.timeIn.title.text = "Sáng"
            view.timeIn2.title.text = "Chiều"
            view.timeWorking.title.text = "Giờ làm việc"
            view.tongCong.title.text = "Tổng công"
            view.tongPhat.title.text = "Tổng phạt"
        }

        fun bindData(model: ChiTietCongModel) {
            view.chiTietPhat.removeAllViews()
            view.dayOfMonth.text = model.ngayThang
            val time1 = " : ${model.gioVao?.convertDate(DateType.FUll3, DateType.ONLY_TIME2) ?: ""}"
            val timeOut1 = "${model.raSang?.convertDate(DateType.FUll3, DateType.ONLY_TIME2) ?: ""}"
            view.timeIn.content.text = "$time1 - $timeOut1"

            val time2 = " : ${model.vaoChieu?.convertDate(DateType.FUll3, DateType.ONLY_TIME2) ?: ""}"
            val timeOut2 = "${model.gioRa?.convertDate(DateType.FUll3, DateType.ONLY_TIME2) ?: ""}"
            view.timeIn2.content.text = "$time2 - $timeOut2"

            view.timeWorking.content.text = " : ${model.gioLamViec ?: ""}"
            view.tongCong.content.text = " : ${model.tongCong ?: ""}"
            view.tongPhat.content.text = " : ${model.tongPhat ?: ""}"

            val listGiaoDich = model.giaodichphats ?: mutableListOf()
            val visible = if (listGiaoDich.isEmpty()) View.GONE else View.VISIBLE
            view.tongPhat.seeMore.visibility = visible
            view.tongPhat.seeMore.setOnClickListener {
                it.visibility = View.GONE
                if (listGiaoDich.isNotEmpty()) {
                    for ((index, giaoDichPhat) in listGiaoDich.withIndex()) {
                        val textView = TextView(itemView.context)
                        textView.textSize = 14f
                        textView.setTypeface(textView.typeface, Typeface.ITALIC);
                        val padding = Utils.convertDpToPixel(itemView.context, 10f)
                        textView.setPadding(padding, padding, padding, padding)
                        textView.text = "${index + 1}. ${giaoDichPhat.moTa}"
                        view.chiTietPhat.addView(textView)
                    }
                } else {
                    printLog(this, "No Data")
                }
            }
        }
    }


}