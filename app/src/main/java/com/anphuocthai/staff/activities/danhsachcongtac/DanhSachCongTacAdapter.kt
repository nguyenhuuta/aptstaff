package com.anphuocthai.staff.activities.danhsachcongtac

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.anphuocthai.staff.R
import com.anphuocthai.staff.model.CongTacModel
import com.anphuocthai.staff.utils.extension.loadImage
import com.anphuocthai.staff.utils.visibility
import kotlinx.android.synthetic.main.item_danh_sach_cong_tac.view.*

/**
 * Created by OpenYourEyes on 12/30/2020
 */

class DanhSachCongTacAdapter(val context: Context, val list: MutableList<CongTacModel>, val onItemClick: (Int, CongTacModel) -> Unit) : RecyclerView.Adapter<DanhSachCongTacAdapter.DanhSachCongTacVH>() {

    private val layoutInflater: LayoutInflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DanhSachCongTacVH {
        val view = layoutInflater.inflate(R.layout.item_danh_sach_cong_tac, parent, false)
        return DanhSachCongTacVH(view)
    }

    override fun onBindViewHolder(holder: DanhSachCongTacVH, position: Int) {
        holder.bind(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }


    inner class DanhSachCongTacVH(rootView: View) : RecyclerView.ViewHolder(rootView) {

        fun bind(model: CongTacModel) {
            itemView.imageView.loadImage(model.imageBase64)
            itemView.content.text = model.placeDescription
            itemView.time.text = model.ngayChamCong
            val color = if (model.hasNew) R.color.colorGray else R.color.white
            itemView.setBackgroundColor(ContextCompat.getColor(context, color))
            itemView.hasNew.visibility = model.hasNew.visibility()
            if (model.hasNew) {
                itemView.content.setTypeface(itemView.content.typeface, Typeface.BOLD)
                itemView.time.setTypeface(itemView.time.typeface, Typeface.BOLD)
            } else {
                itemView.content.setTypeface(itemView.content.typeface, Typeface.NORMAL)
                itemView.time.setTypeface(itemView.time.typeface, Typeface.NORMAL)
            }
            itemView.setOnClickListener {
                onItemClick(adapterPosition, model)
            }
        }
    }
}