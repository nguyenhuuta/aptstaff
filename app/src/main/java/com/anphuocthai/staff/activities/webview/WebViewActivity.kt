package com.anphuocthai.staff.activities.webview

import android.webkit.WebChromeClient
import android.webkit.WebViewClient
import com.anphuocthai.staff.R
import com.anphuocthai.staff.activities.BaseActivities
import com.anphuocthai.staff.api.*
import com.anphuocthai.staff.base.actionbar.CustomActionBar
import com.anphuocthai.staff.data.trackProgressBar
import com.anphuocthai.staff.model.BannerModel
import com.anphuocthai.staff.model.DetailBannerResponse
import kotlinx.android.synthetic.main.webview_activity.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

/**
 * Created by OpenYourEyes on 9/9/2020
 */

enum class WebViewType {
    Banner, QuyCheHanhChinhNhanSu
}

class WebViewActivity : BaseActivities() {
    companion object {
        const val WEB_VIEW_TYPE = "webview_type"
        const val URL_ARG = "webview_data"
    }

    override fun getContentView(): Int {
        return R.layout.webview_activity
    }

    override fun initAction() {
    }

    override fun initView() {
        toolbar.setTitle("An Phước Thái")
        toolbar.setCustomActionBar(object : CustomActionBar.ICustomActionBar {
            override fun onButtonRightClick() {
            }

            override fun onButtonLeftClick() {
                finish()
            }

        })
    }

    override fun initData() {
        webView.settings.apply {
            javaScriptEnabled = true
        }
        webView.webChromeClient = WebChromeClient()
        webView.webViewClient = WebViewClient()

        intent?.let {
            when (it.getSerializableExtra(WEB_VIEW_TYPE) as WebViewType) {
                WebViewType.Banner -> {
                    val data = it.getParcelableExtra<BannerModel>(URL_ARG)
                    val id = data?.id ?: 0
                    val module = data?.moduleId ?: 0
                    getBannerDetail(id, module)
                }
                WebViewType.QuyCheHanhChinhNhanSu -> {
                    CoroutineScope(Dispatchers.Main).launch {
                        showProgress()
                        delay(2500)
                        hideProgress()
                    }

                    val html = it.getStringExtra(URL_ARG)
                    webView.loadUrl(html)
                }
            }
        }
    }

    private fun getBannerDetail(id: Int, module: Int) {
        ApiURL.DetailBanner.getRequest()
                .addQueryParameter("Id", id.toString())
                .addQueryParameter("Module", module.toString())
                .get(DetailBannerResponse::class.java)
                .trackProgressBar(progressBar)
                .runOnMain()
                .subscribe { detailResponse ->
                    detailResponse?.let {
                        val bannerModel = it.data
                        loadHtml(bannerModel.noiDungBaiViet)
                    }
                }.addTo(compositeDisposable)
    }


    private fun loadHtml(html: String?) {
        webView.loadData(html ?: "", "text/html", "UTF-8")
    }
}