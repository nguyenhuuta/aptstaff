package com.anphuocthai.staff.activities.customer;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.viewpager.widget.ViewPager;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.activities.newcustomer.CustomerActivities;
import com.anphuocthai.staff.fragments.customer.HistoryWSTabFragment;
import com.anphuocthai.staff.fragments.customer.NewWScheduleTabFragment;
import com.anphuocthai.staff.ui.base.BaseActivity;
import com.anphuocthai.staff.ui.base.ViewPagerAdapter;

/**
 * This is main class of work schedule
 */

public class WorkScheduleActivity extends BaseActivity {

    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_work_schedule);
        setupToolBarAndViewPager();
    }

    private void setupToolBarAndViewPager() {
        setupViewPager();
        setupToobarWithTab(getString(R.string.work_schedule_name), viewPager, true);
    }

    private void setupViewPager() {
        viewPager = findViewById(R.id.viewpagerDetailWorkSchedule);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(NewWScheduleTabFragment.newInstance(this), getString(R.string.ci_work_schedule_title_new_tab));
        adapter.addFragment(HistoryWSTabFragment.newInstance(this), getString(R.string.ci_work_schedule_title_history_tab));
        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.tool_bar_list_customer_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_add_customer) {
            Intent intent = new Intent(this, CustomerActivities.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
