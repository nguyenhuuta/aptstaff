package com.anphuocthai.staff.activities.xinnghiphep.comment_nghiphep

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.anphuocthai.staff.R
import com.anphuocthai.staff.activities.BaseViewModel
import com.anphuocthai.staff.activities.danhsachcongtac.default
import com.anphuocthai.staff.api.ErrorMessage
import com.anphuocthai.staff.model.CommentXinNghiBody
import com.anphuocthai.staff.model.CommentXinNghiModel
import com.anphuocthai.staff.model.NhanVienPhongBanModel
import com.anphuocthai.staff.utils.DateConvert
import com.anphuocthai.staff.utils.DateType
import com.anphuocthai.staff.utils.convertDate
import com.anphuocthai.staff.utils.extension.ListLiveData
import java.util.*

/**
 * Created by OpenYourEyes on 3/15/21
 */
class CommentNghiPhepVM : BaseViewModel() {
    var phieuNghiId: Int = 0


    val listComment = ListLiveData<CommentXinNghiModel>().apply { postValue(mutableListOf()) }
    val insertComment = MutableLiveData<CommentXinNghiModel>()

    val colorButton = MutableLiveData<Int>().default(R.color.colorTextDisable)


    /**
     * value editext change
     */
    val textChange = MutableLiveData<String>()

    /**
     * state disable button sent
     */
    val isEnable: LiveData<Boolean> = Transformations.map(textChange) {
        colorButton.value = if (it.isEmpty()) R.color.colorTextDisable else R.color.colorPrimary
        it.isNotEmpty()
    }

    /**
     * danh sách nhan viên duyệt xin nghỉ
     */
    val listPhongBan = mutableListOf<NhanVienPhongBanModel>()
    val danhSachNhanVienPhongBan = ListLiveData<NhanVienPhongBanModel>()

    /**
     * index selected danh sách nhân viên
     */
    lateinit var indexSeletedNhanVien: NhanVienPhongBanModel

    /**
     * trạng thái ẩn hiện spinner danh sách nhân viên
     */
    val isHideSpinnerDanhSachNhanVien = MutableLiveData<Boolean>()

    init {
        isHideSpinnerDanhSachNhanVien.postValue(false)
    }

    fun buttonSent(view: View) {
        if (::indexSeletedNhanVien.isInitialized) {
            showDialog.onNext(ErrorMessage("Người duyệt trống"))
            return
        }
        if (listPhongBan.isNotEmpty()) {
            val nguoiDuyet = indexSeletedNhanVien
            val nguoiDuyetId = nguoiDuyet.id ?: 0
            val tenNguoiDuyet = nguoiDuyet.fullname
            val nguoiGui = dataManager.userInfoId
            val tenNguoiGui = dataManager.userInfo.fullname
            val comment = textChange.value ?: ""
            val body = CommentXinNghiBody(nguoiNhanId = nguoiDuyetId,
                    phieuNghiId = phieuNghiId,
                    nguoiGui = nguoiGui,
                    comment = comment)

            executeRequestWithBaseResponse(request = {
                themCommentXinNghi(phieuNghiId = phieuNghiId, body = body)
            }, response = {
                textChange.postValue("")
                val calendar = Calendar.getInstance()
                val time = DateConvert.convertDate(calendar.timeInMillis, DateConvert.TypeConvert.ONLY_TIME)
                val model = CommentXinNghiModel(tenNguoiNhan = tenNguoiDuyet,
                        comment = comment,
                        tenNguoiGui = tenNguoiGui,
                        createDate = "",
                        isMeComment = true,
                        time = time)
                insertComment.postValue(model)
            })

        } else {
            showDialog.onNext(ErrorMessage("Không có người duyệt, vui lòng thử lại sau"))
        }

    }

    fun layNoiDungTraoDoi(phieuNghiId: Int) {
        this.phieuNghiId = phieuNghiId
        executeRequest(request = {
            layCommentDanhSachXinNghi(phieuNghiId)
        }, response = { list ->
            val listNew = mutableListOf<CommentXinNghiModel>()
            val myName = dataManager.userInfo.fullname
            var date: String? = ""
            list.forEach {
                val newDate = it.createDate?.convertDate(DateType.FUll3, DateType.ONLY_DATE2)
                if (date != newDate) {
                    date = newDate
                    listNew.add(CommentXinNghiModel.toTempObject(newDate))
                }
                it.toTime()
                listNew.add(it)
                it.isMeComment = it.tenNguoiGui == myName
            }
            listNew.reverse()
            listComment.postValue(listNew)
        })
    }


    fun danhSachNhanVienPhongBan() {
        executeRequest(request = {
            danhSachNhanVienPhongBan()
        }, response = { list ->
            listPhongBan.addAll(list)
            val firstItem = list.firstOrNull()
            firstItem?.let {
                danhSachNhanVienPhongBan.postValue(listPhongBan)
            } ?: kotlin.run {
                showDialog.onNext(ErrorMessage("Không có người duyệt, vui lòng thử lại sau"))
            }
        })
    }


}