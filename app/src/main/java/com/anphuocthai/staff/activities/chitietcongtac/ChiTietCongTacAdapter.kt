package com.anphuocthai.staff.activities.chitietcongtac

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.anphuocthai.staff.R
import com.anphuocthai.staff.model.ChiTietCongTacModel
import kotlinx.android.synthetic.main.item_chi_tiet_cong_tac.view.*

/**
 * Created by OpenYourEyes on 12/30/2020
 */

class ChiTietCongTacAdapter(context: Context, val list: MutableList<ChiTietCongTacModel>) : RecyclerView.Adapter<ChiTietCongTacAdapter.ChiTietCongTacVH>() {

    private val layoutInflater: LayoutInflater = LayoutInflater.from(context)
    fun insertItem(model: ChiTietCongTacModel) {
        list.add(0, model)
        notifyItemInserted(0)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChiTietCongTacVH {
        val view = layoutInflater.inflate(R.layout.item_chi_tiet_cong_tac, parent, false)
        return ChiTietCongTacVH(view)
    }

    override fun onBindViewHolder(holder: ChiTietCongTacVH, position: Int) {
        holder.bind(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }


    inner class ChiTietCongTacVH(rootView: View) : RecyclerView.ViewHolder(rootView) {
        fun bind(model: ChiTietCongTacModel) {
            itemView.message.text = model.noiDung
        }
    }
}