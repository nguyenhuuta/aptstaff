package com.anphuocthai.staff.activities.xinnghiphep.danhsachnghiphep

import android.content.Intent
import android.os.Handler
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.anphuocthai.staff.R
import com.anphuocthai.staff.activities.BaseActivityBinding
import com.anphuocthai.staff.activities.xinnghiphep.comment_nghiphep.CommentNghiPhepActivity
import com.anphuocthai.staff.activities.xinnghiphep.yeucaunghi.YeuCauNghiActivity
import com.anphuocthai.staff.adapters.BaseAdapterBinding
import com.anphuocthai.staff.databinding.DanhsachxinnghiActivityBinding
import com.anphuocthai.staff.model.DanhSachXinNghiModel
import kotlinx.android.synthetic.main.danhsachxinnghi_activity.*


/**
 * Created by OpenYourEyes on 3/8/21
 */
class DanhSachXinNghiActivity : BaseActivityBinding<DanhSachXinNghiVM, DanhsachxinnghiActivityBinding>(), SwipeRefreshLayout.OnRefreshListener {

    companion object {
        const val ThemYeuCauModel = "them_model"
        const val RequestCode = 1
    }

    override fun createViewModel(): Class<DanhSachXinNghiVM> {
        return DanhSachXinNghiVM::class.java
    }

    override fun layoutId(): Int = R.layout.danhsachxinnghi_activity

    override fun titleToolBar(): String = "Danh sách xin nghỉ"

    override fun initView() {
        initToolbar(toolbar)
        buttonYeuCauNghi.setOnClickListener {
            val intent = Intent(this, YeuCauNghiActivity::class.java)
            startActivityForResult(intent, RequestCode)
        }
        swipeRefresh.setOnRefreshListener(this)
    }

    override fun bindViewModel() {
        val adapter = BaseAdapterBinding<DanhSachXinNghiModel>(this, R.layout.item_danh_sach_xin_nghi) {
            val id = it.id ?: 0
            val intent = Intent(this, CommentNghiPhepActivity::class.java)
            intent.putExtra(CommentNghiPhepActivity.arg_phieu_nghi_id, id)
            startActivity(intent)
        }
        recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@DanhSachXinNghiActivity, LinearLayoutManager.VERTICAL, false)
            val dividerItemDecoration = DividerItemDecoration(this.context,
                    LinearLayoutManager.VERTICAL)
            addItemDecoration(dividerItemDecoration)
            this.adapter = adapter
        }
        viewModel.listDanhSachXinNghi.observe(this, Observer {
            val currentAdapter = recyclerView.adapter as BaseAdapterBinding<DanhSachXinNghiModel>
            currentAdapter.updateList(it)
        })

        viewModel.layDanhSachXinNghi()
    }

    override fun onRefresh() {
        swipeRefresh.isRefreshing = false
        viewModel.layDanhSachXinNghi()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Handler().postDelayed({
            if (requestCode == RequestCode && resultCode == RESULT_OK) {
                data?.let {
                    val model = data.getParcelableExtra<DanhSachXinNghiModel>(ThemYeuCauModel)
                    val currentAdapter = recyclerView.adapter as BaseAdapterBinding<DanhSachXinNghiModel>
                    currentAdapter.insertItem(model)
                }

            }
        }, 300)

    }
}