package com.anphuocthai.staff.activities.newcustomer;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.activities.BaseActivities;
import com.anphuocthai.staff.activities.IActivityCallback;
import com.anphuocthai.staff.base.actionbar.CustomActionBar;
import com.anphuocthai.staff.fragments.BaseFragment;
import com.anphuocthai.staff.fragments.FragmentId;
import com.anphuocthai.staff.model.customer.SearchCustomer;
import com.google.android.material.tabs.TabLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Created by DaiKySy on 8/26/19.
 */
public class CustomerActivities extends BaseActivities implements IActivityCallback {
    public static final String ARG_FROM_WHERE = "FromWhere";
    private CustomActionBar mCustomActionBar;

    private EditText mInputSearch;
    private ImageView mButtonHideSearch, mButtonFilter;


    private TabLayout mTabLayout;
    private ViewPager mViewPageCustomer;

    private ViewPageCustomerAdapter mAdapter;
    String fromWhere;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    public int getContentView() {
        return R.layout.activities_customer;
    }

    @Override
    public void initView() {
        mCustomActionBar = findViewById(R.id.customActionBar);
        mTabLayout = findViewById(R.id.tabLayout);
        mViewPageCustomer = findViewById(R.id.viewPage);
        mButtonHideSearch = findViewById(R.id.buttonHideSearch);
        mButtonFilter = findViewById(R.id.buttonFilter);
        mInputSearch = findViewById(R.id.inputSearch);
        mInputSearch.postDelayed(() -> mInputSearch.setVisibility(View.VISIBLE), 300);
    }

    @Override
    public void initAction() {
        mButtonHideSearch.setOnClickListener(v -> finish());
        mButtonFilter.setOnClickListener(v -> mAdapter.getCustomerFragmentAt(mViewPageCustomer.getCurrentItem()).onShowSheetFilter());
        mInputSearch.addTextChangedListener(textWatcher);

        mViewPageCustomer.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {
            }

            @Override
            public void onPageSelected(int position) {
                mInputSearch.removeTextChangedListener(textWatcher);
                mInputSearch.addTextChangedListener(textWatcher);
                if (getCurrentFocus() != null) {
                    InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {
            }
        });
    }


    @Override
    public void initData() {
        mCustomActionBar.setTitle("Danh sách khách hàng");
        fromWhere = getIntent().getStringExtra(ARG_FROM_WHERE);
        if (fromWhere == null) {
            fromWhere = "";
        }
        mAdapter = new ViewPageCustomerAdapter(getSupportFragmentManager());
        mViewPageCustomer.setAdapter(mAdapter);
        mViewPageCustomer.setOffscreenPageLimit(3);
        mTabLayout.setupWithViewPager(mViewPageCustomer);
    }


    private final TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            String keySearch = s.toString();
            int index = mViewPageCustomer.getCurrentItem();
            mAdapter.getCustomerFragmentAt(index).onFilter(keySearch);
        }
    };

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void receiverEvent(SearchCustomer event) {
        if (!isDestroyed()) {
            mAdapter.getCustomerFragmentAt(mViewPageCustomer.getCurrentItem()).updateCustomer(event.getiD_ThanhVien());
        }
    }


    @Override
    public void displayScreen(FragmentId screenId, boolean... addToBackStack) {

    }

    @Override
    public void onFragmentResumed(BaseFragment fragment) {

    }

    @Override
    public void setAppBarTitle(String title) {

    }

    @Override
    public void onBackPressed(Bundle bundle) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    public String getFromWhere() {
        return fromWhere;
    }
}
