package com.anphuocthai.staff.activities.xinnghiphep.yeucaunghi

import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.content.Context
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.anphuocthai.staff.activities.BaseViewModel
import com.anphuocthai.staff.api.ErrorMessage
import com.anphuocthai.staff.model.*
import com.anphuocthai.staff.utils.DateConvert
import com.anphuocthai.staff.utils.printLog
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import java.util.*


/**
 * Created by OpenYourEyes on 2/25/21
 */
class YeuCauNghiVM : BaseViewModel() {
    /**
     * Các loại type trong màn hình
     */
    val radioTrongNgay = 1
    val radioNhieuNgay = 2
    val startDate = 3
    val endDate = 4
    val chooseDate = 5
    val buttonSend = 6

    /**
     * mặc định check Radio trong ngày
     */
    val checkRadio = MutableLiveData<Boolean>()

    /**
     * mặc định hiển thị radio trong ngày
     */
    val isHideNghiTrongNgay = MutableLiveData<Boolean>()

    /**
     * list loại thời gian
     * @see selectedTypeTime
     */
    val listTypeTime = MutableLiveData<MutableList<String>>()


    /**
     * Biến lưu trữ index loại thời gian bao gồm:
     *  Cả ngày (0)
     *  Ca Sáng (1)
     *  Ca Chiều (2)
     */
    var selectedTypeTime = MutableLiveData<Int>()


    /**
     * Biến lưu trữ index giờ
     * 1 -> 8 giờ
     */
    var valueHour = MutableLiveData<String>()


    /**
     * Biến lưu trữ thời gian xin nghỉ trong ngày
     */
    var valueTimeInDay = MutableLiveData<String>()

    /**
     * Biến lưu trữ thời gian bắt đầu nghỉ
     */
    var valueStarDate = MutableLiveData<String>()
    var longStartDate: Long = 0

    /**
     * Biến lưu trữ thời gian kết thúc nghỉ
     */
    var valueEndDate = MutableLiveData<String>()
    var longEndDate: Long = 0


    /**
     * List loại nghỉ phép lấy từ server
     */
    val listTypeTakeLeave = MutableLiveData<MutableList<LoaiNghiPhepModel>>()

    /**
     * Biến lưu trữ index loại nghỉ phép
     */
    var selectedTypeTakeLeave = MutableLiveData<Int>()

    /**
     * Biến lưu trữ ghi chú
     */
    val noteValue = MutableLiveData<String>()


    var cauHinhLamViecModel: CauHinhLamViecModel? = null

    /**
     * Kết quả gọi api xin phép nhỉ
     * Boolean: request thành công hay không
     * String: Hiển thị lên dialog
     */
    val xinNghiThanhCong = MutableLiveData<DanhSachXinNghiModel>()

    val calendar: Calendar = Calendar.getInstance()


    /**
     * Biến lưu trữ type đang được click bao gồm
     * @see chooseDate : Chọn thời gian nghỉ trong ngày (hôm nay hoặc 1 ngày khác)
     * @see startDate : Chọn thời gian bắt đầu nghỉ
     * @see endDate : Chọn thời gian kết thúc nghỉ
     *
     */
    private var currentType: Int = -1

    private lateinit var datePicker: DatePickerDialog

    init {
        checkRadio.postValue(true)
        isHideNghiTrongNgay.postValue(false)
        listTypeTime.postValue(mutableListOf("Cả ngày", "Sáng", "Chiều"))
        selectedTypeTime.postValue(0)

        val currentDate = DateConvert.convertDate(calendar.timeInMillis, DateConvert.TypeConvert.ONLY_DATE2)
        valueTimeInDay.postValue(currentDate)

    }

    private fun showDatePicker(context: Context) {
        if (!::datePicker.isInitialized) {
            val myCalendar = Calendar.getInstance()
            datePicker = DatePickerDialog(context, android.R.style.Theme_Material_Light_Dialog, date, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH))
            datePicker.datePicker.minDate = System.currentTimeMillis()
        }
        datePicker.datePicker.tag = currentType
        datePicker.show()
    }


    var date = OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        printLog("${view.tag} $year $monthOfYear $dayOfMonth")
        calendar.set(Calendar.YEAR, year)
        calendar.set(Calendar.MONTH, monthOfYear)
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        printLog("timeSet: ${calendar.timeInMillis}")
        val time = DateConvert.convertDate(calendar.timeInMillis, DateConvert.TypeConvert.ONLY_DATE2)
        when (currentType) {
            chooseDate -> valueTimeInDay.postValue(time)
            startDate -> {
                longStartDate = calendar.timeInMillis
                valueStarDate.postValue(time)
            }
            endDate -> {
                longEndDate = calendar.timeInMillis
                valueEndDate.postValue(time)
            }

        }
    }

    fun onClick(view: View) {
        currentType = (view.tag ?: 0) as Int
        when (currentType) {
            radioTrongNgay -> {
                isHideNghiTrongNgay.postValue(false)
            }
            radioNhieuNgay -> {
                isHideNghiTrongNgay.postValue(true)
            }
            chooseDate, startDate, endDate -> {
                showDatePicker(view.context)
            }
            buttonSend -> {
                sendRequestTakeLeave()
            }
        }
    }

    suspend fun getLoaiNghiPhep() = apiCoroutines.getLoaiNghi()

    fun getCauHinhLamViec() {

        executeRequest(request = {
            getCauHinhLamViec(dataManager.userInfoId)
        }, response = {
            printLog(it.toString())
            cauHinhLamViecModel = it
            valueHour.postValue(totalTime().toString())
        })

    }


    private fun sendRequestTakeLeave() {
        if (cauHinhLamViecModel == null) {
            showDialog.onNext(ErrorMessage("Không lấy được cấu hình làm việc, vui lòng liên hệ với quản trị"))
            return
        }
        val position = selectedTypeTakeLeave.value ?: 0
        val list = listTypeTakeLeave.value ?: mutableListOf()

        val userId = dataManager.userInfoId
        var loaiNghiPhepId = 0
        var listThoiGian = mutableListOf<ThoiGianNghiModel>()

        if (position < list.size) {
            val model = list[position]
            loaiNghiPhepId = model.id ?: 0
        }
        val isNghiTrongNgay = isHideNghiTrongNgay.value ?: false

        fun taoThoiGianNghiModel(date: String, hour: Float, typeTimeId: Int = 0): ThoiGianNghiModel {
            return ThoiGianNghiModel.toTimeInDay(date = date,
                    loaiNghiId = loaiNghiPhepId,
                    hour = hour,
                    typeTimeId = typeTimeId,
                    cauHinhLamViecModel = cauHinhLamViecModel!!)
        }
        if (!isNghiTrongNgay) { // Xin nghỉ một ngày
            val hour = (valueHour.value ?: "0").toFloat()
            if (hour <= 0f) {
                showDialog.onNext(ErrorMessage("Số giờ không hợp lệ"))
                return
            }
            val date = valueTimeInDay.value ?: ""
            val typeTime = selectedTypeTime.value ?: 0
            val thoiGianNghiModel = taoThoiGianNghiModel(date = date, hour = hour, typeTimeId = typeTime)
            listThoiGian.add(thoiGianNghiModel)

        } else {
            val startDate = valueStarDate.value ?: ""
            val endDate = valueEndDate.value ?: ""
            var messageError = ""
            if (startDate.isEmpty() || endDate.isEmpty()) {
                messageError = "Vui lòng chọn đủ thời gian"
            } else if (startDate == endDate) {
                messageError = "Vui lòng chọn thời gian khác nhau"
            } else if (longEndDate <= longStartDate) {
                messageError = "Thời gian kết thúc không được phép nhỏ hơn thời gian bắt đầu"
            }
            if (messageError.isNotEmpty()) {
                showDialog.onNext(ErrorMessage(messageError))
            } else {

                val calendar = Calendar.getInstance()
                calendar.timeInMillis = longStartDate
                var nextLongTimeZone = longStartDate
                val listDate = mutableListOf(startDate)
                while (nextLongTimeZone != longEndDate) {
                    calendar.add(Calendar.DATE, 1)
                    nextLongTimeZone = calendar.timeInMillis
                    val midTime = DateConvert.convertDate(nextLongTimeZone, DateConvert.TypeConvert.ONLY_DATE2)
                    listDate.add(midTime)
                }
                val fullHourWoking = totalTime()
                val listModel = listDate.map { date ->
                    taoThoiGianNghiModel(date = date, hour = fullHourWoking)
                }
                listThoiGian.addAll(listModel)
            }

        }
        val lyDo = noteValue.value ?: ""
        val yeuCauNghiBody = YeuCauNghiBody(userId, lyDo, listThoiGian)
        viewModelScope.launch {
            executeRequestBaseResponseFlow(request = {
                xinNghiPhep(body = yeuCauNghiBody)
            }).collect { response ->
                val startDate = listThoiGian.first().thoiGianBatDau
                val endDate = listThoiGian.last().thoiGianKetThuc
                val model = DanhSachXinNghiModel(
                        id = response.idRecord?.toInt() ?: 0,
                        userId = dataManager.userInfoId,
                        tenNhanVien = "",
                        lyDo = lyDo,
                        soPhepNam = null,
                        trangThaiId = null,
                        trangThaiText = "Mới",
                        thoiGianBatDau = startDate,
                        thoiGianKetThuc = endDate,
                        phieuDangKyNghi_TheoNgay = null
                )

                xinNghiThanhCong.postValue(model)
            }
        }
    }

    fun totalTime() = cauHinhLamViecModel?.tongSoGioLamViec() ?: 0f
}