package com.anphuocthai.staff.baserequest;

import androidx.annotation.NonNull;

import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.CallAdapter;

/**
 * Created by DaiKySy on 6/6/19.
 */
public class APICallAdapter<Data> implements CallAdapter<APIResponse<Data>,APICall<Data>> {
    private Type type;

    public APICallAdapter(Type type) {
        this.type = type;
    }

    @Override
    public Type responseType() {
        return type;
    }

    @Override
    public APICall<Data> adapt(@NonNull Call<APIResponse<Data>> call) {
        return new APICall<>(call);
    }
}
