package com.anphuocthai.staff.baserequest;

import com.google.gson.annotations.SerializedName;

public class APIResponse<Data> {
    @SerializedName("isSuccess")
    private boolean isSuccess;

    @SerializedName("data")
    private Data data;

    @SerializedName("errors")
    private String message;


    public String getMessage() {
        return message;
    }


    public Data getData() {
        return data;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public APIResponse() {
    }
}
