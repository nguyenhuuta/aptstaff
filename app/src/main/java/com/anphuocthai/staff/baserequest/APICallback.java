package com.anphuocthai.staff.baserequest;

public interface APICallback<Data> {

    void onSuccess(Data data);

    void onFailure(String errorMessage);
}
