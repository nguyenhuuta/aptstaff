package com.anphuocthai.staff.baserequest

import com.anphuocthai.staff.api.BaseResponse
import com.anphuocthai.staff.model.*
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by OpenYourEyes on 3/1/21
 */
interface ApiCoroutines {

    @POST("BackendSvc/Get_LoaiNghis")
    suspend fun getLoaiNghi(): APIResponse<MutableList<LoaiNghiPhepModel>>

    @POST("BackendSvc/CauHinhLichLamViec_Users_getCauHinhLichLamViec")
    suspend fun getCauHinhLamViec(@Query("UserId") userId: Int): APIResponse<CauHinhLamViecModel>

    @POST("BackendSvc/Insert_PhieuDangKyNghi")
    suspend fun xinNghiPhep(@Body body: YeuCauNghiBody): BaseResponse<Any>

    /**
     * userId: Id of user
     * FomDate: start date query // yyyy-MM-dd
     * ToDate: End date query // yyyy-MM-dd
     */
    @POST("BackendSvc/GetPhieuDangKyNghi")
    suspend fun layDanhSachXinNghi(@Query("UserId") userId: Int, @Query("FomDate") FomDate: String, @Query("ToDate") ToDate: String): APIResponse<MutableList<DanhSachXinNghiModel>>

    @POST("BackendSvc/XemNoiDungTraoDoi")
    suspend fun layCommentDanhSachXinNghi(@Query("PhieuNghiId") id: Int): APIResponse<MutableList<CommentXinNghiModel>>

    @POST("BackendSvc/UserTimKiem_Ext")
    suspend fun danhSachNhanVienPhongBan(@Body body: NhanVienPhongBanBody = NhanVienPhongBanBody()): APIListResponse<NhanVienPhongBanModel>

    @POST("BackendSvc/ThemNoiDungTraoDoi/{phieuNghiId}")
    suspend fun themCommentXinNghi(@Path("phieuNghiId") phieuNghiId: Int, @Body body: CommentXinNghiBody): BaseResponse<CommentXinNghiModel>

    @POST("BackendSvc/KPI_Evaluate_Getkq")
    suspend fun getKPI(@Query("UserId") userId: Int, @Query("Thang") month: Int, @Query("Nam") year: Int): APIListResponse<KPIModel>

    @POST("BackendSvc/GetGiaoDichPhatV1")
    suspend fun getTongPhat(@Query("UserId") userId: Int, @Query("ngay") day: Int, @Query("thang") month: Int, @Query("nam") year: Int): APIListResponse<TongHopPhatModel>
}
