package com.anphuocthai.staff.baserequest;


import android.os.Handler;
import android.os.Looper;

import com.anphuocthai.staff.utils.Logger;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class APICall<Data> extends APIResponse<Data> {

    public static final String CANNOT_CONNECT = "Có lỗi xảy ra";

    private Call<APIResponse<Data>> call;


    public APICall(Call<APIResponse<Data>> call) {
        this.call = call;

    }

    public void getAsyncResponseOnlyCustomer(final APICallback<Data> callback) {
        call.enqueue(new Callback<APIResponse<Data>>() {

            @Override
            public void onResponse(Call<APIResponse<Data>> call, final Response<APIResponse<Data>> response) {
                new Handler(Looper.getMainLooper()).post(() -> {
                    if (response.isSuccessful()) {
                        APIResponse<Data> apiResponse = response.body();
                        if (apiResponse != null) {
                            callback.onSuccess(apiResponse.getData());
                        } else {
                            callback.onFailure(apiResponse.getMessage());
                        }
                    } else {
                        int code = response.raw().code();
                        String message = null;
                        try {
                            String json = response.errorBody().string();
                            Gson gson = new Gson();
                            APIResponse apiResponse = gson.fromJson(json, APIResponse.class);
                            message = apiResponse.getMessage();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (code == 404) {
                            message = CANNOT_CONNECT;
                        }
                        Logger.d("APICallArrays", message);
                        callback.onFailure(message);
                    }
                });
            }

            @Override
            public void onFailure(Call<APIResponse<Data>> call, Throwable t) {
                Logger.d("APICallArrays", "onFailure" + t.getMessage());
                String message = t.getMessage();
                new Handler(Looper.getMainLooper()).post(() -> {
                    if (message != null) {
                        callback.onFailure(message);
                    } else {
                        callback.onFailure(CANNOT_CONNECT);
                    }

                });
            }
        });
    }

    public void getAsyncResponse(final APICallback<Data> callback) {
        Logger.d("DKS", " RequestAPI " + (call.request().url()));
        call.enqueue(new Callback<APIResponse<Data>>() {

            @Override
            public void onResponse(Call<APIResponse<Data>> call, final Response<APIResponse<Data>> response) {
                new Handler(Looper.getMainLooper()).post(() -> {
                    if (response.isSuccessful()) {
                        APIResponse<Data> apiResponse = response.body();
                        if (apiResponse != null && apiResponse.isSuccess()) {
                            callback.onSuccess(apiResponse.getData());
                        } else {
                            callback.onFailure(apiResponse.getMessage());
                        }
                    } else {
                        int code = response.raw().code();
                        String message = "";
                        try {
                            String json = response.errorBody().string();
                            Gson gson = new Gson();
                            APIResponse apiResponse = gson.fromJson(json, APIResponse.class);
                            message = apiResponse.getMessage();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (code != 200) {
                            message = "ERROR " + code;
                        }
                        Logger.d("DKS APICallArrays onResponse", message);
                        callback.onFailure(message);
                    }
                });
            }

            @Override
            public void onFailure(Call<APIResponse<Data>> call, Throwable t) {
                Logger.d("getAsyncResponse ", "onFailure" + t.getMessage());
                System.out.println("onFailure " + t.getMessage());
                String message = t.getMessage();
                new Handler(Looper.getMainLooper()).post(() -> {
                    if (message != null) {
                        callback.onFailure(message);
                    } else {
                        callback.onFailure(CANNOT_CONNECT);
                    }

                });
            }
        });
    }

    public void getJson() {

    }


}
