package com.anphuocthai.staff.baserequest;


import com.anphuocthai.staff.BuildConfig;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIService {

    private static APIService apiService;
    private Retrofit retrofitCoroutines;
    private final HttpLoggingInterceptor logging;

    public static APIService getInstance() {
        if (apiService == null) {
            apiService = new APIService();
        }
        return apiService;
    }

    private APIService() {
        logging = new HttpLoggingInterceptor();
        HttpLoggingInterceptor.Level LEVEL = HttpLoggingInterceptor.Level.BODY;
        logging.setLevel(LEVEL);
    }

    private Map<String, Retrofit> clients = new HashMap<>();


    private static final int REQUEST_TIMEOUT = 20;


    private Retrofit getClient() {
        String baseUrl = BuildConfig.HOST;
        Retrofit retrofit = clients.get(baseUrl);
        if (retrofit == null) {
            OkHttpClient.Builder httpClient = createOkHttp();
            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .client(httpClient.build())
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(new APICallAdapterFactory())
                    .build();
            clients.put(baseUrl, retrofit);
        }
        return retrofit;
    }


    private OkHttpClient.Builder createOkHttp() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                .readTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
                .connectTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS);
        httpClient.addInterceptor(chain -> {
            okhttp3.Request request = chain.request().newBuilder()
                    .addHeader("Content-Type", "application/json")
                    .build();
            return chain.proceed(request);
        }).addInterceptor(logging);
        return httpClient;
    }

    private OkHttpClient.Builder createOkHttp(final String token) {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                .readTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
                .connectTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS);
        httpClient.addInterceptor(chain -> {
            okhttp3.Request request = chain.request().newBuilder()
                    .addHeader("Content-Type", "application/json")
                    .addHeader("Authorization", "Bearer " + token)
                    .build();
            return chain.proceed(request);
        }).addInterceptor(logging);
        return httpClient;
    }

    public void addAuthorizationHeader(String token) {
        createApiCoroutines(token);
        getRetrofit(token);
        String baseUrl = BuildConfig.HOST;
        OkHttpClient.Builder httpClient = createOkHttp(token);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(new APICallAdapterFactory())
                .build();
        clients.put(baseUrl, retrofit);

    }

    public IAppAPI getAppAPI() {
        return getClient().create(IAppAPI.class);
    }

    private void createApiCoroutines(String token) {

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                .readTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
                .connectTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
                .addInterceptor(logging)
                .addInterceptor(chain -> {
                    okhttp3.Request request = chain.request().newBuilder()
                            .addHeader("Content-Type", "application/json")
                            .addHeader("Authorization", "Bearer " + token)
                            .build();
                    return chain.proceed(request);
                });
        retrofitCoroutines = new Retrofit.Builder()
                .baseUrl(BuildConfig.HOST)
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public ApiCoroutines getApiCoroutines() {
        return retrofitCoroutines.create(ApiCoroutines.class);
    }


    private Retrofit retrofitJson;

    private Retrofit getRetrofit(String token) {
        if (retrofitJson == null) {
            String baseUrl = BuildConfig.HOST;
            OkHttpClient.Builder httpClient = createOkHttp(token);
            retrofitJson = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .client(httpClient.build())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofitJson;
    }

    public IAppAPI getAppAPIJson() {
        return retrofitJson.create(IAppAPI.class);
    }

}