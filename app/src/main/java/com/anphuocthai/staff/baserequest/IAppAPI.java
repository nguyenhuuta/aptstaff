package com.anphuocthai.staff.baserequest;


import com.anphuocthai.staff.activities.detaildebtactivity.DebtParams;
import com.anphuocthai.staff.data.db.network.model.response.UserInfo;
import com.anphuocthai.staff.entities.Cities;
import com.anphuocthai.staff.entities.DepartmentModel;
import com.anphuocthai.staff.entities.District;
import com.anphuocthai.staff.entities.ListReportResponse;
import com.anphuocthai.staff.entities.LockCustomerParam;
import com.anphuocthai.staff.entities.Report;
import com.anphuocthai.staff.entities.TopSellBody;
import com.anphuocthai.staff.entities.Wards;
import com.anphuocthai.staff.fragments.customer.CustomerParams;
import com.anphuocthai.staff.model.DanhMucLoaiKhachHangModel;
import com.anphuocthai.staff.model.DanhMucLoaiKhachHangParam;
import com.anphuocthai.staff.model.customer.SearchCustomer;
import com.anphuocthai.staff.model.order.Product;
import com.anphuocthai.staff.model.order.ProductSaleResponse;
import com.anphuocthai.staff.ui.customer.detail.address.DialogEditAddress;
import com.anphuocthai.staff.ui.delivery.debt.inventorydebt.InventoryDebtPresenter;
import com.anphuocthai.staff.ui.delivery.model.OrderTran;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by DaiKySy on 6/6/19.
 */
public interface IAppAPI {
    @FormUrlEncoded
    @POST("AuthenSvc/Login")
    APICall<UserInfo> login(@Field("userName") String userName, @Field("password") String password, @Field("APP") String app);

    @POST("MemberSvc/searchCustomer")
    APICall<List<SearchCustomer>> getCustomer(@Body CustomerParams param);

    @POST("BackendSvc/khoaKhachHang")
    APICall<APIResponse> lockCustomer(@Body LockCustomerParam param);


    @POST("AdminSvc/getBaoCaoList")
    APICall<ListReportResponse> getListReport(@Body Report.ReportBody param);

    @POST("orderSvc/searchDonDatTheoIdThanhVien")
    APICall<List<OrderTran>> getDetailDeb(@Body DebtParams params);

    @POST("BackendSvc/callListTinh")
    APICall<List<Cities>> getCities(@Body Cities.Params params);

    @POST("BackendSvc/CallListQuanHuyen")
    APICall<List<District>> getListDistrict(@Body District.Params params);

    @POST("BackendSvc/CallListPhuongXa")
    APICall<List<Wards>> getListWards(@Body Wards.Params params);

    @POST("MemberSvc/UpdateDiaChiThanhVien")
    APICall<APIResponse> updateAddress(@Body DialogEditAddress.Param params);

    @POST("DanhMuc/GetTopSanPhamBanChay")
    APICall<List<Product>> getTopSell(@Body TopSellBody body);

    @POST("DanhMuc/GetTopSanPhamKhuyenmai")
    APICall<ProductSaleResponse> getProductSale(@Body TopSellBody body);

    @POST("DanhMuc/GetTopSanPhamMoi")
    APICall<List<Product>> getProductNew(@Body TopSellBody body);

    @GET("DanhMuc/getAllDM_BoPhan_GetAllByUserId")
    APICall<List<DepartmentModel>> getDepartment(@Query("ID") int id);

    @POST("DanhMuc/getAllDanhMuc")
    APICall<List<DanhMucLoaiKhachHangModel>> layDanhSachLoaiKhachHang(@Body DanhMucLoaiKhachHangParam body);

    /*
     *
     * get json
     *
     * */

    @POST("orderSvc/searchDonDatTheoIdThanhVien")
    Call<ResponseBody> getJson(@Body List<InventoryDebtPresenter.InventoryParams> jsonArray);


}
