package com.anphuocthai.staff.fragments;

import android.content.Context;
import android.os.Bundle;

/**
 * Created by DaiKySy on 6/6/19.
 */
public interface IBaseFragment {
    Context getViewContext();

    void showDialog(String message);

    void showProgress();

    void hideProgress();

    void showToast(String message);

    void nextScreen(FragmentId fragmentId, boolean... addToBackStack);

    void onBackScreen();

    void onBackScreen(Bundle bundle);

}
