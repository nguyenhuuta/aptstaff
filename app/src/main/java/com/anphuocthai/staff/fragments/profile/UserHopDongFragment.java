package com.anphuocthai.staff.fragments.profile;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.anphuocthai.staff.R;
import com.anphuocthai.staff.adapters.profile.UserHopDongAdapter;
import com.anphuocthai.staff.api.ApiURL;
import com.anphuocthai.staff.model.UserHopDong;
import com.anphuocthai.staff.utils.App;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.lang.reflect.Type;


public class UserHopDongFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = UserHopDongFragment.class.getSimpleName();
    private RecyclerView hopDongRecyclerView;
    private RecyclerView.Adapter aAdapter;
    private RecyclerView.LayoutManager aLayout;
    SwipeRefreshLayout mSwipeRefreshLayout;

    private Context context;

    public UserHopDongFragment() {
        // Required empty public constructor

        this.context = App.getAppContext();

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_user_hop_dong, container, false);
        // Inflate the layout for this fragment
        hopDongRecyclerView = view.findViewById(R.id.recycler_hop_dong);
        hopDongRecyclerView.setHasFixedSize(true);
        aLayout = new LinearLayoutManager(context);
        hopDongRecyclerView.setLayoutManager(aLayout);

        setupPullToRefresh(view);

        fetchUserHopDong();

        return view;
    }

    private void fetchUserHopDong() {
        mSwipeRefreshLayout.setRefreshing(true);
        AndroidNetworking.get(ApiURL.USER_HOP_DONG_URL)
                .addHeaders(ApiURL.getBaseHeader())
                .addHeaders(ApiURL.getTokenHeader())
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //Log.d(TAG, response.toString());
                        if (response != null) {
                            Gson gson = new Gson();
                            Type type = new TypeToken<UserHopDong>() {
                            }.getType();
                            UserHopDong result = gson.fromJson(response.toString(), type);
                            UserHopDongAdapter adapter = new UserHopDongAdapter(result, context);
                            hopDongRecyclerView.setAdapter(adapter);
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                });
    }

    /**
     * This method is called when swipe refresh is pulled down
     */
    @Override
    public void onRefresh() {
        // Fetching data from server
        fetchUserHopDong();
    }

    private void setupPullToRefresh(View view) {
        // SwipeRefreshLayout
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container_hop_dong);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);
        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
    }

}