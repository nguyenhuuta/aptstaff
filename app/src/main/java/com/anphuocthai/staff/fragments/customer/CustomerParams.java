package com.anphuocthai.staff.fragments.customer;

import com.google.gson.annotations.SerializedName;

public class CustomerParams {
    @SerializedName("tenDayDu")
    private String tenDayDu;
    @SerializedName("doiTuongId")
    private int doiTuongId;
    @SerializedName("phanLoai")
    private int phanLoai;
    @SerializedName("pageNumber")
    private int pageNumber;
    @SerializedName("pageSize")
    private int pageSize;

    public CustomerParams(int phanLoai, int pageNumber, int pageSize, String tenDayDu) {
        this.tenDayDu = tenDayDu;
        this.doiTuongId = 0;
        this.phanLoai = phanLoai;
        this.pageNumber = pageNumber;
        this.pageSize = pageSize;
    }



    public String getTenDayDu() {
        return tenDayDu;
    }

    public void setTenDayDu(String tenDayDu) {
        this.tenDayDu = tenDayDu;
    }

    public int getDoiTuongId() {
        return doiTuongId;
    }

    public void setDoiTuongId(int doiTuongId) {
        this.doiTuongId = doiTuongId;
    }

    public int getPhanLoai() {
        return phanLoai;
    }

    public void setPhanLoai(int phanLoai) {
        this.phanLoai = phanLoai;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

}
