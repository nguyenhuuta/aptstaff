package com.anphuocthai.staff.fragments.customer;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.androidnetworking.error.ANError;
import com.anphuocthai.staff.R;
import com.anphuocthai.staff.activities.customer.AddNewCustomerActivity;
import com.anphuocthai.staff.activities.customer.CheckInWithNewWorkScheduleActivity;
import com.anphuocthai.staff.activities.newcustomer.CustomerActivities;
import com.anphuocthai.staff.activities.newcustomer.CustomerAdapter;
import com.anphuocthai.staff.activities.newcustomer.DialogLockCustomer;
import com.anphuocthai.staff.api.ApiURL;
import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.fragments.BaseFragment;
import com.anphuocthai.staff.fragments.FragmentId;
import com.anphuocthai.staff.interfaces.IAPICallback;
import com.anphuocthai.staff.model.DienThoaiModel;
import com.anphuocthai.staff.model.ResponseMessage;
import com.anphuocthai.staff.model.customer.SearchCustomer;
import com.anphuocthai.staff.ui.customer.detail.DetailCustomerActivity;
import com.anphuocthai.staff.ui.customersearch.SearchActivity;
import com.anphuocthai.staff.utils.Utils;
import com.anphuocthai.staff.widget.LoadMoreRecyclerView2;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.PublishSubject;

import static com.anphuocthai.staff.utils.Constants.REQUEST_PHONE_CALL;

/**
 * Created by DaiKySy on 9/3/19.
 */
public class CustomerFragment extends BaseFragment<CustomerViewModel> implements SwipeRefreshLayout.OnRefreshListener, ICustomerView {
    public static final String ARG_TYPE_CUSTOMER = "customerType";


    private SwipeRefreshLayout mRefreshLayout;
    private LoadMoreRecyclerView2 mRecycleCustomer;
    private LinearLayout mEmptyView;
    private FloatingActionButton mButtonAddUser;
    private CustomerAdapter mAdapter;
    private String phoneChoose;

    private BottomSheetDialog mDialogFilter;
    private BottomSheetDialog bottomSheetDialogPhone;
    private PublishSubject<String> triggerSearch = PublishSubject.create();
    boolean isFromSearchActivity = false;
    String fromWhere = "";

    public static CustomerFragment newInstance(Bundle args) {

        CustomerFragment fragment = new CustomerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public String getLogTag() {
        return "CustomerFragment";
    }

    @Override
    public FragmentId getFragmentId() {
        return FragmentId.CUSTOMER;
    }

    @Override
    protected int getResourceLayout() {
        return R.layout.fragment_customer;
    }

    @Override
    protected CustomerViewModel onCreateViewModel() {
        return new CustomerViewModel(this);
    }

    @Override
    public String getTitleActionBar() {
        return "";
    }

    @Override
    protected void initViews(View view) {
        mRefreshLayout = view.findViewById(R.id.refreshLayout);
        mRecycleCustomer = view.findViewById(R.id.recycleCustomer);
        mEmptyView = view.findViewById(R.id.emptyView);
        mButtonAddUser = view.findViewById(R.id.add_new_customer_fab);
        isFromSearchActivity = getActivity() instanceof SearchActivity;
        if (isFromSearchActivity) {
            fromWhere = SearchActivity.TAG;
            mButtonAddUser.hide();
        }

    }

    @Override
    protected void initAction() {
        mRefreshLayout.setOnRefreshListener(this);
        mButtonAddUser.setOnClickListener(v -> {
            Intent i = new Intent(getViewContext(), AddNewCustomerActivity.class);
            startActivity(i);
        });
    }

    Disposable disposable;

    @Override
    protected void initData() {
        if (getArguments() != null) {
            viewModel.customerType = (CustomerType) getArguments().getSerializable(ARG_TYPE_CUSTOMER);
        }
        mRecycleCustomer.addListenerLoadMore(() -> {
            viewModel.getListCustomer(true);
            return null;
        });
        boolean isFromCustomerActivities = getActivity() instanceof CustomerActivities;
        if (isFromCustomerActivities) {
            CustomerActivities activities = (CustomerActivities) getActivity();
            fromWhere = activities.getFromWhere();
        }
        mRecycleCustomer.setLayoutManager(new LinearLayoutManager(getViewContext()));
        mRecycleCustomer.setHasFixedSize(true);
        mAdapter = new CustomerAdapter(viewModel.customerType, viewModel.customers, new CustomerAdapter.ICustomerCallback() {
            @Override
            public void onFromSearchActivity(SearchCustomer searchCustomer) {
                if (getActivity() instanceof SearchActivity) {
                    SearchActivity activity = (SearchActivity) getActivity();
                    activity.onCustomerSelected(searchCustomer);
                }
            }

            @Override
            public void onActionCall(SearchCustomer searchCustomer) {
                showBottomSheetPhone(searchCustomer.getDienthoais(), "");
            }

            @Override
            public void onActionSms(SearchCustomer searchCustomer) {
                showBottomSheetPhone(searchCustomer.getDienthoais(), searchCustomer.getTenDayDu());
            }

            @Override
            public void onActionDetail(SearchCustomer searchCustomer) {
                Intent i = new Intent(getViewContext(), DetailCustomerActivity.class);
                Bundle bundle = new Bundle();
                bundle.putParcelable(DetailCustomerActivity.ARG_CUSTOMER, searchCustomer);
                bundle.putString("customerguid", searchCustomer.getGuid());
                bundle.putString("customername", searchCustomer.getTenDayDu());
                i.putExtra("detailcustomeractivity", bundle);
                startActivity(i);
            }

            @Override
            public void onActionLockerUser(SearchCustomer searchCustomer) {
                String customerName = searchCustomer.getTenDayDu();
                Bundle bundle = new Bundle();
                bundle.putString(DialogLockCustomer.ARG_CUSTOMER_NAME, customerName);
                bundle.putInt(DialogLockCustomer.ARG_CUSTOMER_ID, searchCustomer.getiD_ThanhVien());
                DialogLockCustomer dialogLockCustomer = DialogLockCustomer.newInstance(bundle);
                dialogLockCustomer.setmCallback((customerId, reason) -> {
                    if (Utils.isNull(reason)) return;
                    viewModel.lockCustomer(customerId, reason);
                });
                dialogLockCustomer.show(getChildFragmentManager(), null);
            }

            @Override
            public void onActionGoToMap(SearchCustomer searchCustomer) {

                Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + searchCustomer.getDiaChi());
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
            }

            @Override
            public void onChangeTypeUser(SearchCustomer searchCustomer) {
                showDialogConfirm(getString(R.string.confirm_change_type_user), () -> requestChangeTypeUser(searchCustomer));
            }

            @Override
            public void goToCheckInWithNewWorkScheduleActivity(SearchCustomer customer) {
                Intent i = new Intent(getActivity(), CheckInWithNewWorkScheduleActivity.class);
                Bundle b = new Bundle();
                b.putString("guid", customer.getGuid());
                b.putString("customername", customer.getTenDayDu());
                i.putExtra("checkinwithnewworkextra", b);
                startActivity(i);
                getActivity().finish();
            }
        });
        mAdapter.setFromWhere(fromWhere);
        mRecycleCustomer.setAdapter(mAdapter);
        if (viewModel.customers.size() == 0) {
            viewModel.getListCustomer(viewModel.customerType == CustomerType.SELL);
        }
        disposable = triggerSearch.debounce(300, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
                .subscribe(s -> {
                    viewModel.mKeySearch = s;
                    resetRecyclerView();
                    viewModel.getListCustomer(true);
                });


    }

    private ListView listViewPhone;
    private ArrayAdapter<DienThoaiModel> phoneAdapter;

    private void showBottomSheetPhone(List<DienThoaiModel> list, String tenDaydu) {
        if (bottomSheetDialogPhone == null) {
            View view = getLayoutInflater().inflate(R.layout.dialog_botton_sheet_phone, null);
            listViewPhone = view.findViewById(R.id.listPhone);
            bottomSheetDialogPhone = new BottomSheetDialog(getViewContext());
            bottomSheetDialogPhone.setContentView(view);
            phoneAdapter = new ArrayAdapter<DienThoaiModel>(getViewContext(), android.R.layout.simple_list_item_1);
            listViewPhone.setAdapter(phoneAdapter);
        }
        listViewPhone.setOnItemClickListener((parent, view, position, id) -> {
            bottomSheetDialogPhone.dismiss();
            DienThoaiModel model = (DienThoaiModel) parent.getItemAtPosition(position);
            phoneChoose = model.getSoDienThoai();
            if (tenDaydu.isEmpty()) {
                if (isPermissionGranted()) {
                    callAction();
                }
            } else {
                Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                smsIntent.setType("vnd.android-dir/mms-sms");
                smsIntent.putExtra("address", phoneChoose);
                smsIntent.putExtra("sms_body", "Hi " + tenDaydu + "!");
                startActivity(smsIntent);
            }

        });

        phoneAdapter.clear();
        phoneAdapter.addAll(list);
        phoneAdapter.notifyDataSetChanged();
        bottomSheetDialogPhone.show();
    }

    private void resetRecyclerView() {
        mRefreshLayout.setRefreshing(false);
        mRecycleCustomer.setEndOfPage(false);
        viewModel.page = 0;
    }

    boolean showLoading = false;

    public void updateCustomer(int customId) {
        showLoading = true;
        viewModel.updateCustomer(customId);
    }

    @Override
    public void onRefresh() {
        resetRecyclerView();
        if (mAdapter != null) {
            mAdapter.setItemSelected(-1);
        }
        viewModel.getListCustomer(true);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PHONE_CALL: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    callAction();
                } else {
                    showToast("Permission denied");
                }
            }
        }
    }


    private boolean isPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(getViewContext(), android.Manifest.permission.CALL_PHONE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("TAG", "Permission is granted");
                return true;
            } else {

                Log.v("TAG", "Permission is revoked");
                requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, REQUEST_PHONE_CALL);
                return false;
            }
        } else {
            Log.v("TAG", "Permission is granted");
            return true;
        }
    }

    private void callAction() {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + phoneChoose));
        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(getViewContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
        }
        startActivity(callIntent);
    }

    @Override
    public void showListCustomer(boolean isEndOf) {
        mRecycleCustomer.setLoaded();
        mRecycleCustomer.setEndOfPage(isEndOf);
        mAdapter.updateList(viewModel.customers);
        mEmptyView.setVisibility(mAdapter.getItemCount() == 0 ? View.VISIBLE : View.GONE);
    }

    @Override
    public void updateItem(int position) {
        printLog("updateItem " + position);
        showLoading = false;
        mAdapter.setItemSelected(-1);
        mAdapter.notifyItemChanged(position);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (showLoading) {
            showProgress();
        }
    }

    class ChangeTypeParam {

        ChangeTypeParam(String guid, int phanLoaiKhachHang) {
            this.guid = guid;
            PhanLoaiKhachHang = phanLoaiKhachHang;
        }

        @SerializedName("guid")
        private String guid;
        @SerializedName("PhanLoaiKhachHang")
        private int PhanLoaiKhachHang;

        public String getGuid() {
            return guid;
        }

        public int getPhanLoaiKhachHang() {
            return PhanLoaiKhachHang;
        }
    }

    private void requestChangeTypeUser(SearchCustomer searchCustomer) {
        ChangeTypeParam changeType = new ChangeTypeParam(searchCustomer.getGuid(), CustomerType.SELL.getId());
        showProgress();
        NetworkManager.getInstance().sendPostJsonRequest(ApiURL.CHANGE_CUSTOMER_TYPE, changeType, new IAPICallback() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                if (response != null) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<ResponseMessage>() {
                    }.getType();
                    ResponseMessage result = gson.fromJson(response.toString(), type);
                    if (result.getCode().equals("00")) {
                        showDialog(getString(R.string.change_type_user_success));
                        mAdapter.removeItem(searchCustomer);
                    } else {
                        showDialog(getString(R.string.have_error));
                    }
                    hideProgress();
                }
            }

            @Override
            public void onResponseSuccess(JSONArray response) {

            }

            @Override
            public void onResponseError(ANError anError) {
                hideProgress();
                showDialog(getString(R.string.have_error));
            }
        });
    }

    public void onFilter(String keySearch) {
        if (mAdapter == null) return;
        triggerSearch.onNext(keySearch);
    }

    public void onShowSheetFilter() {
        if (mDialogFilter == null) {
            View view = getLayoutInflater().inflate(R.layout.dialog_filter, null);
            initFilter(view);
            mDialogFilter = new BottomSheetDialog(getViewContext());
            mDialogFilter.setContentView(view);
        }
        mDialogFilter.show();
    }

    private RadioGroup mRadioGroup;
    private TextView mTextCongNo;
    private RadioButton mRadioName, mRadioCongNoAsc, mRadioCongNoDesc, mRadioCV;
    private TypeFilter mCurrentFilter = TypeFilter.TYPE_NAME;

    private void initFilter(View view) {
        mRadioGroup = view.findViewById(R.id.radioGroup);
        mTextCongNo = view.findViewById(R.id.textCongNo);
        mRadioName = view.findViewById(R.id.radioName);
        mRadioCongNoAsc = view.findViewById(R.id.radioCongNoAsc);
        mRadioCongNoDesc = view.findViewById(R.id.radioCongNoDesc);
        mRadioCV = view.findViewById(R.id.radioCV);
        mRadioGroup.setTag(mRadioName);
        mTextCongNo.setOnClickListener(v -> mRadioCongNoAsc.performClick());
        mRadioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            RadioButton radioUnselected = (RadioButton) mRadioGroup.getTag();
            RadioButton radioSelected = group.findViewById(checkedId);
            changeTextColorRadio(radioUnselected, radioSelected);
            group.setTag(radioSelected);
            switch (radioSelected.getId()) {
                case R.id.radioName:
                    mCurrentFilter = TypeFilter.TYPE_NAME;
                    break;
                case R.id.radioCongNoAsc:
                    mCurrentFilter = TypeFilter.TYPE_CONG_NO_ASC;
                    break;
                case R.id.radioCongNoDesc:
                    mCurrentFilter = TypeFilter.TYPE_CONG_NO_DESC;
                    break;
                default:
                    mCurrentFilter = TypeFilter.TYPE_CV;
            }
            mAdapter.filter(mCurrentFilter, viewModel.mKeySearch);
            new Handler().postDelayed(() -> mDialogFilter.dismiss(), 200);

        });
    }

    public void changeTextColorRadio(RadioButton radioUnselected, RadioButton radioSelected) {
        radioUnselected.setTextColor(getViewContext().getResources().getColor(R.color.black));
        radioSelected.setTypeface(radioSelected.getTypeface(), Typeface.NORMAL);
        radioSelected.setTextColor(getViewContext().getResources().getColor(R.color.colorPrimary));
        radioSelected.setTypeface(radioSelected.getTypeface(), Typeface.BOLD);
    }

    public enum TypeFilter {
        TYPE_NAME, TYPE_CONG_NO_ASC, TYPE_CONG_NO_DESC, TYPE_CV
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        disposable.dispose();
    }
}
