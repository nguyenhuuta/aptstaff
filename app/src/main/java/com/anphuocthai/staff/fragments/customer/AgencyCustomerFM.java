package com.anphuocthai.staff.fragments.customer;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.anphuocthai.staff.R;
import com.anphuocthai.staff.activities.customer.ListCustomerActivity;
import com.anphuocthai.staff.adapters.checkin.CustomerListAdapter;
import com.anphuocthai.staff.api.ApiURL;
import com.anphuocthai.staff.model.customer.Customer;
import com.anphuocthai.staff.model.customer.ResponseListCustomer;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

import static com.anphuocthai.staff.utils.Constants.REQUEST_PHONE_CALL;


public class AgencyCustomerFM extends Fragment implements SwipeRefreshLayout.OnRefreshListener, CustomerListAdapter.OnPhoneClickListener{

    private static final String TAG = AgencyCustomerFM.class.getSimpleName();

    private RecyclerView agencyCustomerRecyclerView;
    SwipeRefreshLayout mSwipeRefreshLayout;
    private ListCustomerActivity context;
    private boolean isSelectCustomerToSchedule = false;
    private String phone;
    public AgencyCustomerFM() {
        // Required empty public constructor
        //this.context = App.getAppContext();
    }

    public static AgencyCustomerFM newInstance(ListCustomerActivity context, boolean isSelectCustomerToSchedule ) {
        AgencyCustomerFM fragment = new AgencyCustomerFM();
        fragment.context = context;
//        addressCustomerInfoFragment.guid = guid;
        fragment.isSelectCustomerToSchedule = isSelectCustomerToSchedule;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_agency_customer_fm, container, false);
        setupPullToRefresh(view);
        addControls(view);
        //initPersonalCustomerRecyclerView();
        // Inflate the layout for this fragment
        return view;


    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getAllAgencyCustomer();
    }

    private void addControls(View view) {
        agencyCustomerRecyclerView = view.findViewById(R.id.agencyCustomerRecyclerView);
        agencyCustomerRecyclerView.setHasFixedSize(true);
        //LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        //personalCustomerRecyclerView.setLayoutManager(linearLayoutManager);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 2);
        agencyCustomerRecyclerView.setLayoutManager(gridLayoutManager);
    }

    private void initPersonalCustomerRecyclerView(ResponseListCustomer result) {
        ArrayList<Customer> nameOfCustomer = new ArrayList<>();
        //for loop for sections
        for (int i = 0; i < result.getCustomers().size(); i++) {
            //add the section and items to array list
            nameOfCustomer.add(result.getCustomers().get(i).toCustomer());
        }
        CustomerListAdapter adapter = new CustomerListAdapter(context, nameOfCustomer, isSelectCustomerToSchedule);
        adapter.setOnPhoneClickListener(this);
        agencyCustomerRecyclerView.setAdapter(adapter);

    }

    private void getAllAgencyCustomer(){
        mSwipeRefreshLayout.setRefreshing(true);
        AndroidNetworking.post(ApiURL.GET_CUSTOMER_BY_CATEGORY_URL)
                .addHeaders(ApiURL.getBaseHeader())
                .addHeaders(ApiURL.getTokenHeader())
                .addBodyParameter("tenDayDu", "")
                .addBodyParameter("doiTuongId", "2")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //Log.d(TAG, response.toString());
                        if (response != null) {
                            Gson gson = new Gson();
                            Type type = new TypeToken<ResponseListCustomer>() {
                            }.getType();
                            ResponseListCustomer result = gson.fromJson(response.toString(), type);
                            initPersonalCustomerRecyclerView(result);
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                });
    }
    /**
     * This method is called when swipe refresh is pulled down
     */
    @Override
    public void onRefresh() {
        // Fetching data from server
        getAllAgencyCustomer();
    }

    private void setupPullToRefresh( View view) {
        // SwipeRefreshLayout
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container_list_customer_agency);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);
        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(true);
                // Fetching data from server
                getAllAgencyCustomer();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        getAllAgencyCustomer();
    }

    @Override
    public void phoneClick(String phone) {
        this.phone = phone;
        if(isPermissionGranted()){
            call_action(phone);
        }
    }

    private  boolean isPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.CALL_PHONE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v("TAG","Permission is granted");
                return true;
            } else {

                Log.v("TAG","Permission is revoked");
                //ActivityCompat.requestPermissions(context, new String[]{Manifest.permission.CALL_PHONE}, REQUEST_PHONE_CALL);
                requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, REQUEST_PHONE_CALL);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v("TAG","Permission is granted");
            return true;
        }
    }


    private void call_action(String phone){
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + phone));
        startActivity(callIntent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {

            case REQUEST_PHONE_CALL: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(context, "Permission granted", Toast.LENGTH_SHORT).show();
                    call_action(phone);
                } else {
                    Toast.makeText(context, "Permission denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}