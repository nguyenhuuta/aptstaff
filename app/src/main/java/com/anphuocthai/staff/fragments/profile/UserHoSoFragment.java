package com.anphuocthai.staff.fragments.profile;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.anphuocthai.staff.R;
import com.anphuocthai.staff.adapters.profile.UserHoSoAdapter;
import com.anphuocthai.staff.api.ApiURL;
import com.anphuocthai.staff.model.UserHoSo;
import com.anphuocthai.staff.utils.App;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.lang.reflect.Type;


public class UserHoSoFragment extends Fragment  implements SwipeRefreshLayout.OnRefreshListener{

    private static final String TAG = UserHoSoFragment.class.getSimpleName();
    private RecyclerView hoSoRecyclerView;
    private RecyclerView.Adapter aAdapter;
    private RecyclerView.LayoutManager aLayout;
    SwipeRefreshLayout mSwipeRefreshLayout;

    private Context context;

    public UserHoSoFragment() {
        // Required empty public constructor
        this.context = App.getAppContext();

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_user_ho_so, container, false);
        // Inflate the layout for this fragment
        hoSoRecyclerView = view.findViewById(R.id.recycler_hoso);
        hoSoRecyclerView.setHasFixedSize(true);
        aLayout = new LinearLayoutManager(context);
        hoSoRecyclerView.setLayoutManager(aLayout);

        setupPullToRefresh(view);
        fetchUserHoSo();

        return view;
    }

    private void fetchUserHoSo() {
        // Showing refresh animation before making http call
        mSwipeRefreshLayout.setRefreshing(true);
        AndroidNetworking.get(ApiURL.USER_HO_SO_URL)
                .addHeaders(ApiURL.getBaseHeader())
                .addHeaders(ApiURL.getTokenHeader())
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        if (response != null) {
                            Gson gson = new Gson();
                            Type type = new TypeToken<UserHoSo>() {
                            }.getType();
                            UserHoSo result = gson.fromJson(response.toString(), type);
                            //Log.d(TAG, result.getDiaChi());
                            UserHoSoAdapter adapter = new UserHoSoAdapter(result, context);
                            hoSoRecyclerView.setAdapter(adapter);
                            // Showing refresh animation before making http call
                            mSwipeRefreshLayout.setRefreshing(false);
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        // Showing refresh animation before making http call
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                });
    }

    /**
     * This method is called when swipe refresh is pulled down
     */
    @Override
    public void onRefresh() {
        // Fetching data from server
        fetchUserHoSo();
    }

    private void setupPullToRefresh( View view) {
        // SwipeRefreshLayout
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container_hoso);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);
        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(true);
                // Fetching data from server
                fetchUserHoSo();
            }
        });
    }


}