package com.anphuocthai.staff.fragments.customer;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.anphuocthai.staff.R;
import com.anphuocthai.staff.adapters.checkin.HistoryWSAdapter;
import com.anphuocthai.staff.api.ApiURL;
import com.anphuocthai.staff.model.customer.Schedule;
import com.anphuocthai.staff.utils.DateUtils;
import com.anphuocthai.staff.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;


public class HistoryWSTabFragment extends Fragment{

    private static final String TAG = HistoryWSTabFragment.class.getSimpleName();
    private Context context;
    private RecyclerView historyWSRecyclerView;
    private RecyclerView.LayoutManager aLayout;
    private TextView emptyView;
    public  HistoryWSTabFragment() {
        // Required empty public constructor
    }

    public static HistoryWSTabFragment newInstance(Context context) {
        HistoryWSTabFragment fragment = new HistoryWSTabFragment();
        fragment.context = context;
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_history_wstab, container, false);
        historyWSRecyclerView = view.findViewById(R.id.history_ws_recycler_view);
        aLayout = new LinearLayoutManager(context);
        historyWSRecyclerView.setLayoutManager(aLayout);
        emptyView = view.findViewById(R.id.history_ws_txt_empty);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getHistoryWorkSchedule();
    }

    @Override
    public void onResume() {
        super.onResume();
        getHistoryWorkSchedule();
    }


    private void getHistoryWorkSchedule() {
        JSONObject workScheduleObject = new JSONObject();
        try {
            workScheduleObject.put("guidThanhVien", "");
            workScheduleObject.put("tuNgay", Utils.formatDateWithFormat("yyyy-MM-dd", DateUtils.getNextDayDate(-7)));
            workScheduleObject.put("denNgay", Utils.formatDateWithFormat("yyyy-MM-dd", new Date()));
            workScheduleObject.put("trangThaiId", 0);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String date = Utils.formatDateWithFormat("yyyy-MM-dd", DateUtils.getNextDayDate(-7));
        AndroidNetworking.post(ApiURL.SEARCH_WORK_SCHEDULE_URL)
                .addHeaders(ApiURL.getBaseHeader())
                .addHeaders(ApiURL.getTokenHeader())
                .addJSONObjectBody(workScheduleObject)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        if (response != null) {
                            Gson gson = new Gson();
                            Type type = new TypeToken<ArrayList<Schedule>>() {
                            }.getType();
                            ArrayList<Schedule> result = gson.fromJson(response.toString(), type);

                            HistoryWSAdapter adapter = new HistoryWSAdapter(result, context);
                            historyWSRecyclerView.setAdapter(adapter);
                            if (result.isEmpty()) {
                                historyWSRecyclerView.setVisibility(View.GONE);
                                emptyView.setVisibility(View.VISIBLE);
                            }
                            else {
                                historyWSRecyclerView.setVisibility(View.VISIBLE);
                                emptyView.setVisibility(View.GONE);
                            }
                        }

                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });

    }


}