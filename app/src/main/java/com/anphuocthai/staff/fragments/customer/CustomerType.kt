package com.anphuocthai.staff.fragments.customer

import com.anphuocthai.staff.R

/**
 * Created by OpenYourEyes on 6/18/21
 */
enum class CustomerType(val id: Int) {
    SELL(1), POTENTIAL(3), RECENT(2);//tat ca, tiem nang, khach hang gan day

    companion object {
        //tat ca, tiem nang
        fun getTypeById(id: Int): CustomerType {
            return when (id) {
                1 -> SELL
                3 -> POTENTIAL
                else -> SELL
            }
        }
    }
}


enum class LoaiKinhDoanh(val id: Int) {
    RESTAURANT(1),
    AGENCY(2),
    PERSONAL(3);

    companion object {
        fun getCustomerType(idChecked: Int): Int {
            return when (idChecked) {
                R.id.radioPersonal -> PERSONAL.id
                R.id.radioAgency -> AGENCY.id
                R.id.radioRestaurant -> RESTAURANT.id
                else -> RESTAURANT.id
            }
        }

        fun getRadioByCustomType(customType: Int): Int {
            return when (customType) {
                PERSONAL.id -> R.id.radioPersonal
                AGENCY.id -> R.id.radioAgency
                RESTAURANT.id -> R.id.radioRestaurant
                else -> R.id.radioRestaurant
            }
        }

        fun getString(customType: Int): String {
            return when (customType) {
                PERSONAL.id -> "Cá nhân"
                AGENCY.id -> "Cá nhân"
                RESTAURANT.id -> "Nhà hàng"
                else -> "Nhà hàng"
            }
        }

    }
}