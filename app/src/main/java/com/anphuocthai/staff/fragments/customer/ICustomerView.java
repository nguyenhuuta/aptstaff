package com.anphuocthai.staff.fragments.customer;

import com.anphuocthai.staff.fragments.IBaseFragment;

/**
 * Created by DaiKySy on 9/4/19.
 */
public interface ICustomerView extends IBaseFragment {
    void showListCustomer(boolean isEndOf);

    void updateItem(int position);
}
