package com.anphuocthai.staff.fragments.dialogmessage;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.anphuocthai.staff.R;

/**
 * Created by DaiKySy on 6/6/19.
 */
public class DialogMessage extends DialogFragment implements View.OnClickListener {
    public static final String ARG_CONTENT = "arg_content";
    public static final String ARG_BUTTON_YES = "arg_yes";
    public static final String ARG_BUTTON_CANCEL = "arg_no";

    private View mRootView;

    private TextView mTextTitle, mTextContent;

    private TextView mYes, mCancel;

    private View midLine;

    private IOnClickListener mCallback;

    public static DialogMessage newInstance(@NonNull String content, @NonNull String yes) {
        return newInstance(content, yes, null);
    }

    public static DialogMessage newInstance(String content, String yes, String cancel) {
        DialogMessage dialogMessage = new DialogMessage();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_CONTENT, content);
        bundle.putString(ARG_BUTTON_YES, yes);
        bundle.putString(ARG_BUTTON_CANCEL, cancel);
        dialogMessage.setArguments(bundle);
        return dialogMessage;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.dialog_message, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setCancelable(false);
        initView();
        initAction();
        initData();
        return mRootView;
    }


    private void initView() {
        mTextTitle = mRootView.findViewById(R.id.tvTitle);
        mTextContent = mRootView.findViewById(R.id.tvContent);
        mYes = mRootView.findViewById(R.id.tvYes);
        midLine = mRootView.findViewById(R.id.mid_line);
        mCancel = mRootView.findViewById(R.id.tvCancel);
    }

    public void initAction() {
        mYes.setOnClickListener(this);
        mCancel.setOnClickListener(this);
    }

    public void initData() {
        Bundle bundle = getArguments();
        if (bundle == null) return;
        String content = bundle.getString(ARG_CONTENT);
        String yes = bundle.getString(ARG_BUTTON_YES);
        String cancel = bundle.getString(ARG_BUTTON_CANCEL);
        mTextContent.setText(content);
        mYes.setText(yes);
        if (cancel == null || cancel.isEmpty()) {
            mCancel.setVisibility(View.GONE);
            midLine.setVisibility(View.GONE);
        } else {
            mCancel.setText(cancel);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvYes:
                if (mCallback != null) {
                    mCallback.buttonYesClick();
                }
                break;
            case R.id.tvCancel:
                if (mCallback != null) {
                    mCallback.buttonCancelClick();
                }
                break;
        }
        dismissAllowingStateLoss();
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        FragmentTransaction ft = manager.beginTransaction();
        ft.add(this, tag);
        ft.commitAllowingStateLoss();
    }

    public void setCallback(IOnClickListener mCallback) {
        this.mCallback = mCallback;
    }

    public interface IOnClickListener {

        void buttonYesClick();

        default void buttonCancelClick() {

        }
    }
}