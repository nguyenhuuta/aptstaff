package com.anphuocthai.staff.fragments.profile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.error.ANError;
import com.anphuocthai.staff.R;
import com.anphuocthai.staff.api.ApiURL;
import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.interfaces.IAPICallback;
import com.anphuocthai.staff.model.ChangePass;
import com.anphuocthai.staff.ui.base.BaseFragment;
import com.anphuocthai.staff.ui.login.LoginActivity;
import com.anphuocthai.staff.utils.App;
import com.anphuocthai.staff.utils.Singleton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;

public class ChangePassFragment extends BaseFragment{

    private static final String TAG = ChangePassFragment.class.getSimpleName();
    private Context context;
    private EditText newPassText;
    private EditText newPassAgain;
    private EditText oldPassText;
    private Button changePassButton;


    public ChangePassFragment() {
        // Required empty public constructor
        this.context = App.getAppContext();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected void setUp(View view) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_change_pass, container, false);
        // Inflate the layout for this fragment
        addControls(view);
        changePassButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changePassWord();
            }
        });
        return view;
    }

    private void addControls(View view) {
        newPassText = (EditText) view.findViewById(R.id.new_pass);
        newPassAgain = view.findViewById(R.id.new_pass_again);
        oldPassText = view.findViewById(R.id.old_pass);
        changePassButton = view.findViewById(R.id.btn_confirm_transport);
    }

    private void changePassWord(){

        if (!validate()) {
            onChangePassFailed(context.getString(R.string.ci_change_pass_failure));
            return;
        }

        changePassButton.setEnabled(false);

        String oldPass = oldPassText.getText().toString();
        String newPass = newPassText.getText().toString();


        // TODO: Implement your own authentication logic here.


        JSONObject changePassObjet = new JSONObject();
        try {
            changePassObjet.put("passwordNew", newPass);
            changePassObjet.put("passwordOld", oldPass);
        }catch(Exception e) {
            e.printStackTrace();
        }
        NetworkManager.getInstance().sendPostRequest(ApiURL.CHANGE_PASS_URL, changePassObjet, new IAPICallback() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                Log.d(TAG, response.toString());
                if (response != null) {
                    Gson gson = new Gson();
                    Type type = new TypeToken<ChangePass>() {
                    }.getType();
                    ChangePass result = gson.fromJson(response.toString(), type);
                    Singleton.getInstance().changePass = result;

                    if (Singleton.getInstance().changePass.getCode().equals("00")){
                        onChangePassSuccess();
                    }
                    else {
                        onChangePassFailed(Singleton.getInstance().changePass.getMessage());
                    }

                    hideLoading();

                }
            }

            @Override
            public void onResponseSuccess(JSONArray response) {

            }

            @Override
            public void onResponseError(ANError anError) {
                Log.d(TAG, anError.toString());
                onChangePassFailed(getString(R.string.have_error));
                hideLoading();
            }
        });

    }

    public boolean validate() {
        boolean valid = true;

        String oldPass = oldPassText.getText().toString();
        String newPass = newPassText.getText().toString();
        String newPassRepeat = newPassAgain.getText().toString();

        if (oldPass.isEmpty() || oldPass.length() < 4 || oldPass.length() > 10 ) {
            oldPassText.setError(getString(R.string.limit_pass_character));

            valid = false;
        }

        else {
            oldPassText.setError(null);
        }

        if (newPass.isEmpty() || newPass.length() < 4 || newPass.length() > 10) {
            newPassText.setError(getString(R.string.limit_pass_character));
            valid = false;
        } else {
            newPassText.setError(null);
        }

        if (newPassRepeat.isEmpty() || newPassRepeat.length() < 4 || newPassRepeat.length() > 10) {
            newPassAgain.setError(getString(R.string.limit_pass_character));
            valid = false;
        } else {
            newPassAgain.setError(null);
        }

        if (!newPass.equals(newPassRepeat)){
            onChangePassFailed(getString(R.string.not_same_pass));
            valid = false;
        }else {
            newPassAgain.setError(null);
            newPassText.setError(null);
        }

        return valid;
    }

    public void onChangePassFailed(String message) {
        Toast.makeText(context, getString(R.string.have_error) + "\n" + message, Toast.LENGTH_SHORT).show();
        changePassButton.setEnabled(true);
    }

    public void onChangePassSuccess() {
        Toast.makeText(context, getString(R.string.change_pass_success), Toast.LENGTH_SHORT).show();
        changePassButton.setEnabled(true);
        Intent i = new Intent(context, LoginActivity.class);
        startActivity(i);
        //finish();
    }


}