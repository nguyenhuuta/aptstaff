package com.anphuocthai.staff.fragments;

import android.content.Context;

import com.anphuocthai.staff.baserequest.APIService;
import com.anphuocthai.staff.baserequest.IAppAPI;


public class BaseViewModel<CallbackFragment extends IBaseFragment> {
    private APIService service;
    private CallbackFragment callbackFragment;

    public BaseViewModel(CallbackFragment callback) {
        this.callbackFragment = callback;
        service = APIService.getInstance();
    }

    public CallbackFragment getView() {
        return callbackFragment;
    }

//    public MainActivity getMainActity̰() {
//        Context context = getContext();
//        if (context instanceof MainActivity) {
//            return (MainActivity) context;
//        }
//        return null;
//    }

    private Context getContext() {
        return callbackFragment.getViewContext();
    }

    protected IAppAPI getApi() {
        return service.getAppAPI();
    }

//    public SharedPreferencesModel getSharedModel() {
//        return SharedPreferencesModel.getInstance();
//    }

    public String getString(int stringId) {
        Context context = getContext();
        return context.getString(stringId);
    }
}
