package com.anphuocthai.staff.fragments.customer;

import com.anphuocthai.staff.baserequest.APICallback;
import com.anphuocthai.staff.baserequest.APIResponse;
import com.anphuocthai.staff.entities.LockCustomerParam;
import com.anphuocthai.staff.fragments.BaseViewModel;
import com.anphuocthai.staff.model.customer.SearchCustomer;
import com.anphuocthai.staff.utils.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DaiKySy on 9/3/19.
 */
public class CustomerViewModel extends BaseViewModel<ICustomerView> {
    public CustomerViewModel(ICustomerView callback) {
        super(callback);
    }

    List<SearchCustomer> customers = new ArrayList<>();
    String mKeySearch = Constants.EMPTY;
    int page = 0; // start page = 1
    int limit = 30;
    CustomerType customerType;


    public void updateCustomer(int customerId) {
        _requestGetCustomer(customerId);
    }


    public void getListCustomer(boolean isLoading) {
        if (isLoading) {
            getView().showProgress();
        }
        page++;
        _requestGetCustomer(-1);
    }

    private void _requestGetCustomer(int customId) {
        getApi().getCustomer(new CustomerParams(customerType.getId(), page, limit, mKeySearch))
                .getAsyncResponseOnlyCustomer(new APICallback<List<SearchCustomer>>() {
                    @Override
                    public void onSuccess(List<SearchCustomer> searchCustomers) {
                        getView().hideProgress();
                        if (customId != -1) {
                            SearchCustomer findItem = null;
                            for (SearchCustomer searchCustomer : searchCustomers) {
                                if (searchCustomer.getiD_ThanhVien() == customId) {
                                    findItem = searchCustomer;
                                    break;
                                }
                            }
                            if (findItem != null) {
                                for (SearchCustomer searchCustomer : customers) {
                                    if (searchCustomer.getiD_ThanhVien() == customId) {
                                        int index = customers.indexOf(searchCustomer);
                                        customers.set(index, findItem);
                                        getView().updateItem(index);
                                        break;
                                    }
                                }
                            }

                        } else {
                            if (page == 1) {
                                customers.clear();
                            }
                            customers.addAll(searchCustomers);
                            boolean isEndOf = searchCustomers.size() < limit;
                            getView().showListCustomer(isEndOf);
                        }

                    }

                    @Override
                    public void onFailure(String errorMessage) {
                        getView().hideProgress();
                        getView().showDialog(errorMessage);
                    }
                });
    }

    public void lockCustomer(int customId, String reason) {
        getView().showProgress();
        getApi().lockCustomer(new LockCustomerParam(customId, reason)).getAsyncResponse(new APICallback<APIResponse>() {
            @Override
            public void onSuccess(APIResponse apiResponse) {
                getView().hideProgress();
                getView().showDialog("Yêu cầu khoá thành công");
            }

            @Override
            public void onFailure(String errorMessage) {
                getView().hideProgress();
                getView().showDialog(errorMessage);

            }
        });
    }
}
