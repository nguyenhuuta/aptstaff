package com.anphuocthai.staff.fragments.customer;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.anphuocthai.staff.R;
import com.anphuocthai.staff.adapters.checkin.InDayWSAdapter;
import com.anphuocthai.staff.api.ApiURL;
import com.anphuocthai.staff.model.customer.Schedule;
import com.anphuocthai.staff.utils.DateUtils;
import com.anphuocthai.staff.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;


public class NewWScheduleTabFragment extends Fragment{

    private static final String TAG = NewWScheduleTabFragment.class.getSimpleName();
    private Context context;
    private RecyclerView newWSTabRecyclerView;
    private RecyclerView.LayoutManager aLayout;
    private RecyclerView newWSTabOtherRecyclerView;
    private TextView emptyView;
    private TextView otherEmptyView;

    private TextView allEmptyView;


    private ConstraintLayout constraintLayoutToday;
    private ConstraintLayout constraintLayoutNextDay;

    private String startCurrentDay;
    private String endCurrentDay;


    ArrayList<Schedule> currentDay = new ArrayList<>();

    public  NewWScheduleTabFragment() {
        // Required empty public constructor
    }

    public static NewWScheduleTabFragment newInstance(Context context) {
        NewWScheduleTabFragment fragment = new NewWScheduleTabFragment();
        fragment.context = context;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_wschedule_tab, container, false);
        newWSTabRecyclerView = view.findViewById(R.id.ci_new_work_schedule_tab_recyclerview);
        newWSTabRecyclerView.setHasFixedSize(true);
        aLayout = new LinearLayoutManager(context);
        newWSTabRecyclerView.setLayoutManager(aLayout);
        emptyView = view.findViewById(R.id.ci_new_wschedule_empty_view);

        newWSTabOtherRecyclerView = view.findViewById(R.id.ci_new_work_schedule_tab_other_recyclerview);
        newWSTabOtherRecyclerView.setHasFixedSize(true);
        newWSTabOtherRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        otherEmptyView = view.findViewById(R.id.ci_new_wschedule_other_empty_view);

        allEmptyView = view.findViewById(R.id.ws_new_empty_view);
        constraintLayoutToday = view.findViewById(R.id.ws_today_container);
        constraintLayoutNextDay = view.findViewById(R.id.ws_next_day_container);

        constraintLayoutToday.setVisibility(View.GONE);
        constraintLayoutNextDay.setVisibility(View.GONE);
        allEmptyView.setVisibility(View.GONE);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        startCurrentDay = Utils.formatDateWithFormat("yyyy-MM-dd", new Date());
        endCurrentDay   = Utils.formatDateWithFormat("yyyy-MM-dd", new Date());
        getIndayWorkSchedule();
    }

    @Override
    public void onResume() {
        super.onResume();
        getIndayWorkSchedule();
    }

    private void getIndayWorkSchedule() {
        JSONObject workScheduleObject = new JSONObject();
        try {
            // lúc hiển thị danh sách không cần guid
            workScheduleObject.put("guidThanhVien", "");
            workScheduleObject.put("tuNgay", startCurrentDay);
            workScheduleObject.put("denNgay", endCurrentDay);
            workScheduleObject.put("trangThaiId", 0);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post(ApiURL.SEARCH_WORK_SCHEDULE_URL)
                .addHeaders(ApiURL.getBaseHeader())
                .addHeaders(ApiURL.getTokenHeader())
                .addJSONObjectBody(workScheduleObject)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        if (response != null) {
                            Gson gson = new Gson();
                            Type type = new TypeToken<ArrayList<Schedule>>() {
                            }.getType();
                            currentDay.clear();
                            currentDay = gson.fromJson(response.toString(), type);
                            InDayWSAdapter adapter = new InDayWSAdapter(currentDay, context, true, false, true);
                            newWSTabRecyclerView.setAdapter(adapter);
                            if (currentDay.isEmpty()) {
                                newWSTabRecyclerView.setVisibility(View.GONE);
                                emptyView.setVisibility(View.VISIBLE);
                            }
                            else {
                                newWSTabRecyclerView.setVisibility(View.VISIBLE);
                                emptyView.setVisibility(View.GONE);
                            }

                            getOtherWorkSchedule();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });

    }

    private void getOtherWorkSchedule() {
        JSONObject workScheduleObject = new JSONObject();
        try {
            workScheduleObject.put("guidThanhVien", "");
            workScheduleObject.put("tuNgay", Utils.formatDateWithFormat("yyyy-MM-dd", DateUtils.getNextDayDate(1)));
            workScheduleObject.put("denNgay", "");
            workScheduleObject.put("trangThaiId", 1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post(ApiURL.SEARCH_WORK_SCHEDULE_URL)
                .addHeaders(ApiURL.getBaseHeader())
                .addHeaders(ApiURL.getTokenHeader())
                .addJSONObjectBody(workScheduleObject)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        if (response != null) {
                            Gson gson = new Gson();
                            Type type = new TypeToken<ArrayList<Schedule>>() {
                            }.getType();
                            ArrayList<Schedule> result = gson.fromJson(response.toString(), type);
                            InDayWSAdapter adapter = new InDayWSAdapter(result, context, false, false, false );
                            newWSTabOtherRecyclerView.setAdapter(adapter);
                            if (result.isEmpty()) {
                                newWSTabOtherRecyclerView.setVisibility(View.GONE);
                                otherEmptyView.setVisibility(View.VISIBLE);
                            }
                            else {
                                newWSTabOtherRecyclerView.setVisibility(View.VISIBLE);
                                otherEmptyView.setVisibility(View.GONE);
                            }
                            if (currentDay.isEmpty() && result.isEmpty()) {
                                constraintLayoutNextDay.setVisibility(View.GONE);
                                constraintLayoutToday.setVisibility(View.GONE);
                                allEmptyView.setVisibility(View.VISIBLE);
                            }else {
                                constraintLayoutNextDay.setVisibility(View.VISIBLE);
                                constraintLayoutToday.setVisibility(View.VISIBLE);
                                allEmptyView.setVisibility(View.GONE);
                            }
                        }

                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }

}