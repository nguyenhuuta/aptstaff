package com.anphuocthai.staff.fragments.profile;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.anphuocthai.staff.R;
import com.anphuocthai.staff.adapters.profile.TaiKhoanNganHangAdapter;
import com.anphuocthai.staff.api.ApiURL;
import com.anphuocthai.staff.model.TaiKhoanNganHang;
import com.anphuocthai.staff.utils.App;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.lang.reflect.Type;


public class TaiKhoanNganHangFragment extends Fragment  implements SwipeRefreshLayout.OnRefreshListener{

    private static final String TAG = TaiKhoanNganHangFragment.class.getSimpleName();
    private RecyclerView taiKhoanNganHangRecyclerView;
    private RecyclerView.Adapter aAdapter;
    private RecyclerView.LayoutManager aLayout;
    SwipeRefreshLayout mSwipeRefreshLayout;

    private Context context;

    public TaiKhoanNganHangFragment() {
        // Required empty public constructor

        this.context = App.getAppContext();

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_tai_khoan_ngan_hang, container, false);
        // Inflate the layout for this fragment
        taiKhoanNganHangRecyclerView = view.findViewById(R.id.recycler_tai_khoan_ngan_hang);
        taiKhoanNganHangRecyclerView.setHasFixedSize(true);
        aLayout = new LinearLayoutManager(context);
        taiKhoanNganHangRecyclerView.setLayoutManager(aLayout);
        setupPullToRefresh(view);
        fetchUserTaiKhoanNganHang();
        return view;
    }

    private void fetchUserTaiKhoanNganHang() {
        mSwipeRefreshLayout.setRefreshing(true);
        AndroidNetworking.get(ApiURL.TAI_KHOAN_NGAN_HANG_URL)
                .addHeaders(ApiURL.getBaseHeader())
                .addHeaders(ApiURL.getTokenHeader())
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        if (response != null) {
                            Gson gson = new Gson();
                            Type type = new TypeToken<TaiKhoanNganHang>() {
                            }.getType();
                            TaiKhoanNganHang result = gson.fromJson(response.toString(), type);
//                            Log.d(TAG, result.getChiNhanh());
                            TaiKhoanNganHangAdapter adapter = new TaiKhoanNganHangAdapter(result, context);
                            taiKhoanNganHangRecyclerView.setAdapter(adapter);
                            mSwipeRefreshLayout.setRefreshing(false);
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                });
    }


    /**
     * This method is called when swipe refresh is pulled down
     */
    @Override
    public void onRefresh() {
        // Fetching data from server
        fetchUserTaiKhoanNganHang();
    }

    private void setupPullToRefresh( View view) {
        // SwipeRefreshLayout
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container_tai_khoan_ngan_hang);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);
        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(true);
                // Fetching data from server
                fetchUserTaiKhoanNganHang();
            }
        });
    }



}