package com.anphuocthai.staff.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.activities.IActivityCallback;
import com.anphuocthai.staff.fragments.dialogmessage.DialogMessage;
import com.anphuocthai.staff.utils.Logger;

public abstract class BaseFragment<ViewModel extends BaseViewModel> extends Fragment implements IBaseFragment {
    private String TAG = "BaseFragment";

    private Activity mActivity;

    private IActivityCallback mActivityCallback;

    private boolean mIsAttached;

    private View mRootView;

    private ProgressDialog mProgressDialog;

    public ViewModel viewModel;

    public abstract String getLogTag();

    public abstract FragmentId getFragmentId();

    protected abstract int getResourceLayout();

    protected abstract ViewModel onCreateViewModel();

    public abstract String getTitleActionBar();

    protected abstract void initViews(View view);

    protected abstract void initAction();

    protected abstract void initData();

    protected boolean isRetainState() {
        return true;
    }

    public FragmentId getParentFragmentId() {
        return FragmentId.NONE;
    }

    public boolean isShowBottomNavigationView() {
        return true;
    }

    protected int getBottomNavigationId() {
        return 0;
    }

    /*
     *
     * Button Left Action Bar Click
     *
     * */
    public void onLeftActionBarClick() {

    }

    /*
     *
     * Button Right Action Bar Click
     *
     * */
    public void onRightActionBarClick() {

    }

    public boolean isShowActionbar() {
        return true;
    }

    public boolean isShowLeftButton() {
        return true;
    }

    public boolean isShowRightButton() {
        return true;
    }

    @Override
    public void onAttachFragment(Fragment childFragment) {
        super.onAttachFragment(childFragment);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Logger.i(TAG, "#onCreate");
    }

    @Override
    public void onStart() {
        super.onStart();
        Logger.i(TAG, "#onStart");
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        TAG = getLogTag();
        Logger.i(TAG, "#onAttach");
        if (activity instanceof IActivityCallback) {
            mActivity = activity;
            mActivityCallback = (IActivityCallback) activity;
            viewModel = onCreateViewModel();
            mIsAttached = true;
        } else {
            throw new RuntimeException("Activity must implement IActivityCallback");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Logger.i(TAG, "#onCreateView");
        mActivityCallback.onFragmentResumed(this);
        if (isRetainState()) {
            if (mRootView == null) {
                mRootView = onCreateViewInternal(inflater, container);
            }
        } else {
            mRootView = onCreateViewInternal(inflater, container);
        }
        return mRootView;
    }


    private View onCreateViewInternal(LayoutInflater inflater, @Nullable ViewGroup container) {
        View rootView = null;
        int resId = getResourceLayout();
        if (resId > 0) {
            rootView = inflater.inflate(resId, container, false);
            initViews(rootView);
            initAction();
        }
        initData();
        return rootView;
    }


    @Override
    public Context getViewContext() {
        return mActivity;
    }

    @Override
    public void showDialog(String message) {
        if (!isAttached()) {
            return;
        }
        DialogMessage dialogMessage = DialogMessage.newInstance(message, getString(R.string.OK));
        dialogMessage.show(getChildFragmentManager(), getLogTag());
    }

    public void showDialogConfirm(String message, DialogMessage.IOnClickListener callback) {
        DialogMessage dialogMessage = DialogMessage.newInstance(message, getString(R.string.OK), getString(R.string.cancel));
        dialogMessage.setCallback(callback);
        dialogMessage.show(getChildFragmentManager(), getLogTag());
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void nextScreen(FragmentId fragmentId, boolean... addToBackStack) {
        if (mActivityCallback != null) {
            mActivityCallback.displayScreen(fragmentId, true, addToBackStack.length == 0);
        }

    }

    @Override
    public void onBackScreen() {
        if (mActivity != null) {
            mActivity.onBackPressed();
        }
    }


    @Override
    public void onBackScreen(Bundle bundle) {
        if (mActivityCallback != null) {
            mActivityCallback.onBackPressed(bundle);
        }
    }

    @Override
    public void showProgress() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(mActivity);
            mProgressDialog.setCancelable(false);
        }
        if (mIsAttached) {
            if (getUserVisibleHint()) {
                mProgressDialog.show();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Logger.i(TAG, "#onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Logger.i(TAG, "#onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Logger.i(TAG, "#onStop");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mIsAttached = false;
        Logger.i(TAG, "#onDetach");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Logger.i(TAG, "#onDestroyView");
    }

    @Override
    public void hideProgress() {
        if (mProgressDialog != null && mProgressDialog.isShowing() && mIsAttached) {
            mProgressDialog.dismiss();
        }
    }


    private static final class ProgressDialog extends Dialog {

        private ProgressDialog(Context context) {
            super(context);
            setCancelable(false);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.dialog_progress);
            Window window = getWindow();
            if (window != null) {
                getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            }
        }
    }


    public void printLog(String contents) {
        if (contents == null) {
            return;
        }
        Logger.d(getLogTag(), contents);
    }

    public void showDialog(String message, String buttonOk, DialogMessage.IOnClickListener callback) {
        DialogMessage dialogMessage = DialogMessage.newInstance(message, buttonOk);
        dialogMessage.setCallback(callback);
        dialogMessage.show(getChildFragmentManager(), getLogTag());
    }

//    public MainActivity getMainActivity() {
//        if (mActivity instanceof MainActivity) {
//            return (MainActivity) mActivity;
//        }
//        return null;
//    }

    public boolean isAttached() {
        return mIsAttached;
    }

}
