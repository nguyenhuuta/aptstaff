package com.anphuocthai.staff.adapters.checkin;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.activities.customer.CheckInWithNewWorkScheduleActivity;
import com.anphuocthai.staff.interfaces.ItemClickListener;

import java.util.ArrayList;

/**
 * Created by sonu on 24/07/17.
 */

public class CheckedInImageRecyclerViewAdapter extends RecyclerView.Adapter<CheckedInImageRecyclerViewAdapter.ViewHolder> implements ItemClickListener {

    public interface OnItemClickListener {
        void imageItemClick(View view, int position, boolean isLongClick);
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        private ImageView ivImageItem;
        private ItemClickListener itemClickListener;

        public void setItemClickListener(ItemClickListener itemClickListener) {
            this.itemClickListener = itemClickListener;
        }

        public ViewHolder(View itemView) {
            super(itemView);
            ivImageItem = itemView.findViewById(R.id.iv_image_item);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            itemClickListener.onClick(v, getAdapterPosition(),false);
        }

        @Override
        public boolean onLongClick(View v) {
            itemClickListener.onClick(v, getAdapterPosition(),true);
            return true;
        }
    }

    private ArrayList<Bitmap> arrBitmap = new ArrayList<>();

    public void setPhotos(ArrayList<Bitmap> arrBitmap) {
        this.arrBitmap.clear();
        if (arrBitmap != null) {
            this.arrBitmap.addAll(arrBitmap);
        }
        notifyDataSetChanged();
    }

    private OnItemClickListener callback;
    private CheckInWithNewWorkScheduleActivity activity;

    public void setOnImageItemClickListener(OnItemClickListener callback) {
        this.callback = callback;
    }

    @Override
    public void onClick(View view, int position, boolean isLongClick) {
        callback.imageItemClick(view, position, isLongClick);
    }

    public CheckedInImageRecyclerViewAdapter(CheckInWithNewWorkScheduleActivity activity,
                                             ArrayList<Bitmap> arrBitmap) {
        this.arrBitmap.clear();
        if (arrBitmap != null) {
            this.arrBitmap.addAll(arrBitmap);
        }
        this.activity = activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.checked_in_image_item, parent, false);
        activity.registerForContextMenu(view);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.ivImageItem.setImageBitmap(arrBitmap.get(position));
        holder.setItemClickListener(this);
    }

    @Override
    public int getItemCount() {
        return arrBitmap.size();
    }

}

