package com.anphuocthai.staff.adapters.profile;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.model.UserHoSo;
import com.anphuocthai.staff.utils.Utils;

import java.util.ArrayList;

public class UserHoSoAdapter extends RecyclerView.Adapter<UserHoSoAdapter.ViewHolder> {

    private UserHoSo mDataset = new UserHoSo();

    private Context context;
    private ArrayList<String> arrTitle = new ArrayList<>();
    private ArrayList<String> arrValue = new ArrayList<>();

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView titleProfile;
        public TextView valueProfile;

        public ViewHolder(View v) {
            super(v);
            titleProfile = (TextView) v.findViewById(R.id.title_profile);
            valueProfile = (TextView) v.findViewById(R.id.value_profile);
        }
    }

    public UserHoSoAdapter(UserHoSo dataset, Context context) {
        if (context == null) {
            return;
        }
        mDataset = new UserHoSo();
        mDataset = dataset;
        this.context = context;
        arrTitle.clear();

        arrTitle.add(context.getString(R.string.pf_user_resume_code));
        arrTitle.add(context.getString(R.string.pf_user_resume_birthday));
        arrTitle.add(context.getString(R.string.pf_user_resume_identification));
        arrTitle.add(context.getString(R.string.pf_user_resume_provide_day));
        arrTitle.add(context.getString(R.string.pf_user_resume_provide_place));
        arrTitle.add(context.getString(R.string.pf_user_resume_old_country));
        arrTitle.add(context.getString(R.string.pf_user_resume_address));
//        arrTitle.add(context.getString(R.string.pf_user_resume_create_day));
//        arrTitle.add(context.getString(R.string.pf_user_resume_person_create));
        arrTitle.add(context.getString(R.string.pf_user_resume_tax_code));
        arrTitle.add(context.getString(R.string.pf_user_resume_state));
        arrTitle.add(context.getString(R.string.pf_user_resume_city));
        arrTitle.add(context.getString(R.string.pf_user_resume_gender));


        arrValue.clear();
        arrValue.add(dataset.getMa());
        arrValue.add(Utils.formatDate(dataset.getNgaySinh(), false));
        arrValue.add(dataset.getCmt());
        arrValue.add(Utils.formatDate(dataset.getNgayCap(), false));
        arrValue.add(dataset.getNoiCap());
        arrValue.add(dataset.getNguyenQuan());
        arrValue.add(dataset.getDiaChi());
//        arrValue.add(Utils.formatDate(dataset.getNgayTao(), false));
//        arrValue.add(dataset.getNguoiTao());
        arrValue.add(dataset.getMaSoThue());
        arrValue.add(dataset.getTrangThaiText());
        arrValue.add(dataset.getTen_Tinh_ThanhPho());
        arrValue.add(dataset.getGioiTinhText());

    }

    @Override
    public UserHoSoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_profile_list, parent, false);
        UserHoSoAdapter.ViewHolder vh = new UserHoSoAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(UserHoSoAdapter.ViewHolder holder, int position) {
        //holder.titleProfile.setText(mDataset.get(position));
        holder.titleProfile.setText(arrTitle.get(position));
        holder.valueProfile.setText(arrValue.get(position));

    }

    @Override
    public int getItemCount() {
        //return mDataset.size();
        return arrTitle.size();
    }

}
