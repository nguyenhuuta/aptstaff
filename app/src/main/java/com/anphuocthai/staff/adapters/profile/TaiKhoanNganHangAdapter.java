package com.anphuocthai.staff.adapters.profile;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.model.TaiKhoanNganHang;

import java.util.ArrayList;

public class TaiKhoanNganHangAdapter  extends RecyclerView.Adapter<TaiKhoanNganHangAdapter.ViewHolder>  {

    private TaiKhoanNganHang mDataset = new TaiKhoanNganHang();
    private Context context;
    private ArrayList<String> arrTitle = new ArrayList<>();
    private ArrayList<String> arrValue = new ArrayList<>();

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView titleProfile;
        public TextView valueProfile;
        public ViewHolder(View v) {
            super(v);
            titleProfile = (TextView) v.findViewById(R.id.title_profile);
            valueProfile = (TextView) v.findViewById(R.id.value_profile);
        }
    }

    public TaiKhoanNganHangAdapter(TaiKhoanNganHang dataset, Context context) {
        mDataset = new TaiKhoanNganHang();
        mDataset = dataset;

        this.context = context;
        arrTitle.clear();


        arrTitle.add(context.getString(R.string.pf_user_bank_account_number));
        arrTitle.add(context.getString(R.string.pf_user_bank_account_name));
        arrTitle.add(context.getString(R.string.pf_user_bank_name));
        arrTitle.add(context.getString(R.string.pf_user_bank_account_branch));
//        arrTitle.add(context.getString(R.string.pf_user_bank_account_state));
        arrTitle.add(context.getString(R.string.pf_user_bank_account_city));



        arrValue.clear();
        arrValue.add(dataset.getSoTaiKhoan());
        arrValue.add(dataset.getTenTaiKhoan());
        arrValue.add(dataset.getTenNganHang());
        arrValue.add(dataset.getChiNhanh());
//        arrValue.add(dataset.getInUseText());
        arrValue.add(dataset.getTen_Tinh_ThanhPho());


    }

    @Override
    public TaiKhoanNganHangAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_profile_list, parent, false);
        TaiKhoanNganHangAdapter.ViewHolder vh = new TaiKhoanNganHangAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(TaiKhoanNganHangAdapter.ViewHolder holder, int position) {
        //holder.titleProfile.setText(mDataset.get(position));
        holder.titleProfile.setText(arrTitle.get(position));
        holder.valueProfile.setText(arrValue.get(position));

    }

    @Override
    public int getItemCount() {
        //return mDataset.size();
        return arrTitle.size();
    }

}
