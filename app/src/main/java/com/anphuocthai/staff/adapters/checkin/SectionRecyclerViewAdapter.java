package com.anphuocthai.staff.adapters.checkin;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.activities.customer.ListImageAlreadyCheckInActivity;
import com.anphuocthai.staff.interfaces.ItemClickListener;
import com.anphuocthai.staff.model.SectionModel;
import com.anphuocthai.staff.utils.extension.GridSpacingItemDecoration;

import java.util.ArrayList;

/**
 * Created by sonu on 24/07/17.
 */

public class SectionRecyclerViewAdapter extends RecyclerView.Adapter<SectionRecyclerViewAdapter.SectionViewHolder> implements ItemClickListener {


    private static final String TAG = SectionRecyclerViewAdapter.class.getSimpleName();


    public interface OnItemClickListener {
        void imageItemClick(View view, int position, boolean isLongClick);
    }

    class SectionViewHolder extends RecyclerView.ViewHolder {
        private TextView sectionLabel, showAllButton;
        private RecyclerView itemRecyclerView;
        private TextView txtNoImage;

        public SectionViewHolder(View itemView) {
            super(itemView);
            sectionLabel = (TextView) itemView.findViewById(R.id.section_label);
            showAllButton = (TextView) itemView.findViewById(R.id.section_show_all_button);
            itemRecyclerView = (RecyclerView) itemView.findViewById(R.id.item_recycler_view);
            txtNoImage = itemView.findViewById(R.id.txt_list_image_no_image);
        }
    }

    private Context context;
    private RecyclerViewType recyclerViewType;
    private ArrayList<SectionModel> sectionModelArrayList;
    private OnItemClickListener callback;
    private ListImageAlreadyCheckInActivity listImageAlreadyCheckInActivity;

    public void setOnImageItemClickListener(OnItemClickListener callback) {
        this.callback = callback;
    }

    @Override
    public void onClick(View view, int position, boolean isLongClick) {
        callback.imageItemClick(view, position, isLongClick);
    }

    public SectionRecyclerViewAdapter(ListImageAlreadyCheckInActivity listImageAlreadyCheckInActivity,
                                      Context context,
                                      RecyclerViewType recyclerViewType,
                                      ArrayList<SectionModel> sectionModelArrayList) {
        this.context = context;
        this.recyclerViewType = recyclerViewType;
        this.sectionModelArrayList = sectionModelArrayList;
        this.listImageAlreadyCheckInActivity = listImageAlreadyCheckInActivity;
    }

    @Override
    public SectionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.section_custom_row_layout, parent, false);
        return new SectionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SectionViewHolder holder, int position) {
        final SectionModel sectionModel = sectionModelArrayList.get(position);
        holder.sectionLabel.setText(sectionModel.getSectionLabel());

        // if no image, show no image text view
        if (sectionModel.getImageBitmapArrayList().size() <= 0) {
            holder.txtNoImage.setVisibility(View.VISIBLE);
            holder.itemRecyclerView.setVisibility(View.GONE);
            return;
        }else {
            holder.txtNoImage.setVisibility(View.GONE);
            holder.itemRecyclerView.setVisibility(View.VISIBLE);

        }

        //recycler view for items
        holder.itemRecyclerView.setHasFixedSize(true);
        holder.itemRecyclerView.setNestedScrollingEnabled(false);

        /* set layout manager on basis of recyclerview enum type */
        switch (recyclerViewType) {
            case LINEAR_VERTICAL:
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
                holder.itemRecyclerView.setLayoutManager(linearLayoutManager);
                break;
            case LINEAR_HORIZONTAL:
                LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
                holder.itemRecyclerView.setLayoutManager(linearLayoutManager1);
                break;
            case GRID:
                GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 3);
                int spanCount = 3; // 3 columns
                int spacing = 3; // 50px
                boolean includeEdge = true;
                holder.itemRecyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));
                holder.itemRecyclerView.setLayoutManager(gridLayoutManager);

                break;
        }


        ItemRecyclerViewAdapter adapter = new ItemRecyclerViewAdapter(listImageAlreadyCheckInActivity, context, sectionModel.getItemArrayList(), sectionModel.getImageBitmapArrayList());
        adapter.setItemClickListener(this);
        holder.itemRecyclerView.setAdapter(adapter);

        //show toast on click of show all button
        holder.showAllButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }

    @Override
    public int getItemCount() {
        return sectionModelArrayList.size();
    }

}

