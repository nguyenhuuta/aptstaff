package com.anphuocthai.staff.adapters.checkin;


import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.interfaces.ItemClickListener;

import java.util.ArrayList;

/**
 * Created by sonu on 24/07/17.
 */

public class ItemImageAlreadyCheckInAdapter extends RecyclerView.Adapter<ItemImageAlreadyCheckInAdapter.ItemViewHolder> {

    class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        private TextView itemLabel;
        private ImageView imageView;
        private ItemClickListener itemClickListener;
        public ItemViewHolder(View itemView) {
            super(itemView);
            itemLabel = (TextView) itemView.findViewById(R.id.item_label);
            imageView = itemView.findViewById(R.id.img_item_take_photo);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);

        }

        @Override
        public void onClick(View v) {
            itemClickListener.onClick(v, getAdapterPosition(),false);
        }
        @Override
        public boolean onLongClick(View v) {
            itemClickListener.onClick(v,getAdapterPosition(),true); // Gọi interface , true là vì đây là onLongClick
            return true;
        }
        public void setItemClickListener(ItemClickListener itemClickListener)
        {
            this.itemClickListener = itemClickListener;
        }


    }

    private Context context;
    private ArrayList<String> arrayList;
    private ArrayList<Bitmap> arrayListBitmap;
    private ItemClickListener itemClickListener;


    public void setItemClickListener(ItemClickListener itemClickListener)
    {
        this.itemClickListener = itemClickListener;
    }

    public ItemImageAlreadyCheckInAdapter(Context context, ArrayList<String> arrayList, ArrayList<Bitmap> arrayListBitmap) {
        this.context = context;
        this.arrayList = arrayList;
        this.arrayListBitmap = arrayListBitmap;

    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_custom_row_layout, parent, false);
        ItemViewHolder vh = new ItemViewHolder(view);
        vh.setItemClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position, boolean isLongClick) {
                if (isLongClick) {
                    itemClickListener.onClick(view, position, isLongClick);
                }
            }
        });
        return vh;
    }


    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        holder.itemLabel.setText(arrayList.get(position));
        holder.imageView.setImageBitmap(arrayListBitmap.get(position));


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

}
