package com.anphuocthai.staff.adapters.checkin;

import android.content.Context;
import android.media.Image;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.fragments.customer.LoaiKinhDoanh;
import com.anphuocthai.staff.model.DienThoaiModel;
import com.anphuocthai.staff.model.customer.SearchCustomer;
import com.anphuocthai.staff.utils.DateConvert;

import java.util.ArrayList;
import java.util.List;

public class CustomerDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    static final int TYPE_PHONE = 1;


    private final List<Pair<String, String>> listInfo = new ArrayList<>();
    List<DienThoaiModel> listDienThoai;

    private Pair<String, String> convertToPair(int resId, String value, Context context) {
        String key = context.getString(resId);
        return new Pair<String, String>(key, value);
    }

    public CustomerDetailAdapter(SearchCustomer dataset, Context context) {
        getData(dataset, context, false);
    }

    public void getData(SearchCustomer dataset, Context context, boolean isRefresh) {
        listInfo.clear();
        Pair<String, String> ma = convertToPair(R.string.ci_adap_customer_member_code, dataset.getMaThanhVien(), context);
        listInfo.add(ma);
        Pair<String, String> tenDayDu = convertToPair(R.string.common_name, dataset.getTenDayDu(), context);
        listInfo.add(tenDayDu);

        Pair<String, String> email = convertToPair(R.string.ci_adap_customer_email, dataset.getEmail(), context);
        listInfo.add(email);
        listDienThoai = dataset.getDienthoais();
        Pair<String, String> phone = convertToPair(R.string.ci_adap_customer_phone, dataset.getFirstPhone(), context);
        listInfo.add(phone);

        Pair<String, String> dangKyKinhDoanh;
        int customerType = dataset.getDoiTuongId();
        if (customerType == LoaiKinhDoanh.RESTAURANT.getId()) {
            dangKyKinhDoanh = convertToPair(R.string.ci_adap_customer_business_code, dataset.getCmt(), context);
        } else {
            dangKyKinhDoanh = convertToPair(R.string.ci_adap_customer_identification, dataset.getCmt(), context);
        }
        listInfo.add(dangKyKinhDoanh);
        Pair<String, String> doiTuong = convertToPair(R.string.ci_adap_customer_object, dataset.getDoiTuongText(), context);
        listInfo.add(doiTuong);

        String _ngayDangKy = DateConvert.formatDate(dataset.getNgayDangKy(), DateConvert.TypeConvert.FUll3, DateConvert.TypeConvert.ONLY_DATE);
        Pair<String, String> ngayDangKy = convertToPair(R.string.ci_adap_customer_register_day, _ngayDangKy, context);
        listInfo.add(ngayDangKy);

        Pair<String, String> address = convertToPair(R.string.pf_user_resume_address, dataset.getDiaChi(), context);
        listInfo.add(address);
        if (isRefresh) {
            notifyDataSetChanged();
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_PHONE) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_profile_list_type_phone, parent, false);
            return new ViewHolderExpand(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_profile_list, parent, false);
            return new CustomerDetailAdapter.ViewHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ViewHolder viewHolder = (ViewHolder) holder;
            viewHolder.bindData(listInfo.get(position));
        } else if (holder instanceof ViewHolderExpand) {
            ViewHolderExpand viewHolder = (ViewHolderExpand) holder;
            viewHolder.bindData(listInfo.get(position), listDienThoai);
        }

    }

    @Override
    public int getItemCount() {
        return listInfo.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 3) {
            return TYPE_PHONE;
        }
        return super.getItemViewType(position);
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView titleProfile;
        public TextView valueProfile;

        public ViewHolder(View v) {
            super(v);
            titleProfile = v.findViewById(R.id.title_profile);
            valueProfile = v.findViewById(R.id.value_profile);
        }

        void bindData(Pair<String, String> data) {
            titleProfile.setText(data.first);
            valueProfile.setText(data.second);
        }
    }

    public static class ViewHolderExpand extends RecyclerView.ViewHolder {
        public TextView titleProfile;
        public TextView valueProfile;
        LinearLayout linearLayout, layoutRoot;
        ImageView dropdown;

        public ViewHolderExpand(View v) {
            super(v);
            titleProfile = v.findViewById(R.id.title_profile);
            valueProfile = v.findViewById(R.id.value_profile);
            linearLayout = v.findViewById(R.id.layoutPhone);
            layoutRoot = v.findViewById(R.id.layoutRoot);
            dropdown = v.findViewById(R.id.dropdown);
        }

        void bindData(Pair<String, String> data, List<DienThoaiModel> listDienThoai) {
            titleProfile.setText(data.first);
            valueProfile.setText(data.second);
            if (listDienThoai != null && listDienThoai.size() > 1) {
                for (int position = 1; position < listDienThoai.size(); position++) {
                    DienThoaiModel model = listDienThoai.get(position);
                    View v = LayoutInflater.from(itemView.getContext()).inflate(R.layout.item_profile_list, null, false);
                    View viewLine = v.findViewById(R.id.viewLine);
                    viewLine.setVisibility(View.VISIBLE);
                    TextView titleProfile = v.findViewById(R.id.title_profile);
                    TextView valueProfile = v.findViewById(R.id.value_profile);
                    titleProfile.setVisibility(View.INVISIBLE);
                    valueProfile.setText(model.getFullPhone());
                    linearLayout.addView(v);
                }
            }
            layoutRoot.setOnClickListener(v -> {
                if (linearLayout.getVisibility() == View.GONE) {
                    linearLayout.setVisibility(View.VISIBLE);
                    dropdown.setImageDrawable(ContextCompat.getDrawable(v.getContext(), R.drawable.ic_arrow_up));
                } else {
                    linearLayout.setVisibility(View.GONE);
                    dropdown.setImageDrawable(ContextCompat.getDrawable(v.getContext(), R.drawable.ic_arrow_down));
                }
            });
        }
    }
}
