package com.anphuocthai.staff.adapters.checkin;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.error.ANError;
import com.anphuocthai.staff.R;
import com.anphuocthai.staff.api.ApiURL;
import com.anphuocthai.staff.api.NetworkManager;
import com.anphuocthai.staff.interfaces.IAPICallback;
import com.anphuocthai.staff.model.customer.ImageCheckIn;
import com.anphuocthai.staff.model.customer.Schedule;
import com.anphuocthai.staff.utils.Utils;
import com.anphuocthai.staff.utils.extension.GridSpacingItemDecoration;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

public class DetailWSAdapter extends RecyclerView.Adapter<DetailWSAdapter.ViewHolder>  {

    private Schedule mDataset = new Schedule();
    private Context context;
    private ArrayList<String> arrTitle = new ArrayList<>();
    private ArrayList<String> arrValue = new ArrayList<>();
    private ArrayList<ImageCheckIn> arrImageCheckIn = new ArrayList<>();
    private ArrayList<Bitmap> photoAlreadyCheckin = new ArrayList<>();
    private Boolean isHistory;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView titleProfile;
        private TextView valueProfile;
        private RecyclerView recyclerView;

        public ViewHolder(View v) {
            super(v);
            titleProfile =  v.findViewById(R.id.title_profile);
            valueProfile = v.findViewById(R.id.value_profile);
            recyclerView = v.findViewById(R.id.ci_ws_list_image_history_check_in);
        }
    }

    public DetailWSAdapter(Schedule dataset, Context context, boolean isHistory) {
        mDataset = new Schedule();
        mDataset = dataset;
        this.context = context;
        this.isHistory = isHistory;
        arrTitle.clear();
        arrTitle.add(context.getString(R.string.ci_work_schedule_detail_name_customer));
        arrTitle.add(context.getString(R.string.ci_work_schedule_detail_name_schedule));
        arrTitle.add(context.getString(R.string.ci_work_schedule_detail_description));
        arrTitle.add(context.getString(R.string.ci_work_schedule_detail_day_start));
        arrTitle.add(context.getString(R.string.ci_work_schedule_detail_location));
        arrValue.clear();
        arrValue.add(dataset.getTenThanhVien());
        arrValue.add(dataset.getTen());
        arrValue.add(dataset.getMoTa());
        arrValue.add(Utils.formatDate(dataset.getNgayBatDau(), true));
        arrValue.add(dataset.getViTri());
    }

    @Override
    public DetailWSAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        DetailWSAdapter.ViewHolder vh;

        if (viewType == arrValue.size())
        {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_history_checkin_recycler, parent, false);
            if (!isHistory) {
                v.setVisibility(View.GONE);
            }
        }else {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_profile_list, parent, false);
        }

        vh = new DetailWSAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(DetailWSAdapter.ViewHolder holder, int position) {
        if (position == arrValue.size()) {

            GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 3);
            int spanCount = 3; // 3 columns
            int spacing = 3; // 50px
            boolean includeEdge = true;
            holder.recyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));
            holder.recyclerView.setLayoutManager(gridLayoutManager);
            loadAlreadyCheckInImages(holder);
        }else {
            holder.titleProfile.setText(arrTitle.get(position));
            holder.valueProfile.setText(arrValue.get(position));
        }

    }

    @Override
    public int getItemCount() {
        //return mDataset.size();
        return arrTitle.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    private void loadAlreadyCheckInImages(DetailWSAdapter.ViewHolder holder) {
        HashMap object = new HashMap();
        object.put("loaiID", "3");
        object.put("objectID", String.valueOf(mDataset.getId()));
        object.put("guidThanhVien", mDataset.getGuidThanhVien());

        NetworkManager.getInstance().sendGetRequest(ApiURL.GET_FILES_WORK_SCHEDULE_URL, object, new IAPICallback() {
            @Override
            public void onResponseSuccess(JSONObject response) {

            }

            @Override
            public void onResponseSuccess(JSONArray response) {
                Gson gson = new Gson();
                Type type = new TypeToken<ArrayList<ImageCheckIn>>() {}.getType();
                arrImageCheckIn.clear();
                photoAlreadyCheckin.clear();
                arrImageCheckIn = gson.fromJson(response.toString(), type);
                for (int i = 0; i < arrImageCheckIn.size(); i ++) {
                    photoAlreadyCheckin.add(Utils.decodeBase64(arrImageCheckIn.get(i).getNoiDung()));

                }

                ArrayList<String> itemPhotoAlreadyCheckinArrayList = new ArrayList<>();
                for (int i = 0; i < photoAlreadyCheckin.size(); i++) {
                    itemPhotoAlreadyCheckinArrayList.add("Ảnh " + (i + 1));
                }
                ItemImageAlreadyCheckInAdapter adapter = new ItemImageAlreadyCheckInAdapter(context,itemPhotoAlreadyCheckinArrayList, photoAlreadyCheckin);
                //adapter.setItemClickListener(this);
                holder.recyclerView.setAdapter(adapter);
            }

            @Override
            public void onResponseError(ANError anError) {

            }
        });
    }


}

