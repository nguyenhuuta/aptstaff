package com.anphuocthai.staff.adapters.common;

import android.content.Context;
import android.view.ViewGroup;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.anphuocthai.staff.ui.home.container.ContainerFragment;
import com.anphuocthai.staff.ui.home.notifications.NotificationFragment;
import com.anphuocthai.staff.ui.home.statistics.StatisticFragment;

import java.util.ArrayList;

/**
 *
 */
public class MainViewPagerAdapter extends FragmentPagerAdapter {

    private ArrayList<ContainerFragment> fragments = new ArrayList<>();
    private ContainerFragment currentFragment;

    public MainViewPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        fragments.clear();
        fragments.add(StatisticFragment.newInstance(0, context));
        fragments.add(ContainerFragment.newInstance(1, context));
        fragments.add(ContainerFragment.newInstance(2, context));
        fragments.add(NotificationFragment.newInstance(3, context));
        fragments.add(ContainerFragment.newInstance(4, context));
    }


    public ContainerFragment getFragment(int position) {
        if (position >= 0 && position <= fragments.size() - 1) {
            return fragments.get(position);
        }
        return null;
    }


    @Override
    public ContainerFragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        if (getCurrentFragment() != object) {
            currentFragment = ((ContainerFragment) object);
        }
        super.setPrimaryItem(container, position, object);
    }

    /**
     * Get the current fragment
     */
    public ContainerFragment getCurrentFragment() {
        return currentFragment;
    }
}