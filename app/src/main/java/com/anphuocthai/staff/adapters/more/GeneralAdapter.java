package com.anphuocthai.staff.adapters.more;

import android.content.Context;
import android.content.Intent;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.activities.detailcoin.DetailCoinActivity;
import com.anphuocthai.staff.activities.kpi.KPIActivity;
import com.anphuocthai.staff.activities.listquychehanhchinh.ListQuyCheHanhChinhNhanSuActivity;
import com.anphuocthai.staff.activities.newcustomer.CustomerActivities;
import com.anphuocthai.staff.activities.xinnghiphep.danhsachnghiphep.DanhSachXinNghiActivity;
import com.anphuocthai.staff.entities.SubModel;
import com.anphuocthai.staff.entities.SubUserModel;
import com.anphuocthai.staff.interfaces.ItemClickListener;
import com.anphuocthai.staff.ui.base.BaseViewHolder;
import com.anphuocthai.staff.ui.profile.ProfileActivity;
import com.anphuocthai.staff.utils.App;
import com.anphuocthai.staff.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.anphuocthai.staff.api.ApiURL.IMAGE_VIEW_URL;

public class GeneralAdapter extends RecyclerView.Adapter<BaseViewHolder> {


    private static final int VIEW_TYPE_PROFILE = 0;
    private static final int VIEW_TYPE_NORMAL = 1;


    private ArrayList<SubUserModel> mDataset = new ArrayList<>();
    private Context context;

    public GeneralAdapter(ArrayList<SubUserModel> dataset, Context context) {
        mDataset.clear();
        mDataset.addAll(dataset);
        this.context = context;
    }


    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_PROFILE:
                ViewHolder viewHolder = new GeneralAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_more_fragment, parent, false));
                viewHolder.setItemClickListener((view, position, isLongClick) -> {
                    if (isLongClick) {
                    } else {
                        Intent i = new Intent(context, ProfileActivity.class);
                        context.startActivity(i);
                    }

                });
                return viewHolder;
            case VIEW_TYPE_NORMAL:
            default:
                return new GeneralAdapter.NormalViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.sub_item_user_info, parent, false));
        }

    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(position);
    }


    @Override
    public int getItemCount() {
        return mDataset.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return VIEW_TYPE_PROFILE;
        }
        return VIEW_TYPE_NORMAL;
    }


    public class ViewHolder extends BaseViewHolder implements View.OnClickListener, View.OnLongClickListener {
        TextView usernameTextView;
        TextView phoneNumberTextView;
        TextView positionTextView;
        private CircleImageView imgUser;
        private ItemClickListener itemClickListener;

        public ViewHolder(View v) {
            super(v);
            usernameTextView = v.findViewById(R.id.usernameTV);
            phoneNumberTextView = v.findViewById(R.id.phoneNumberTV);
            positionTextView = v.findViewById(R.id.positionTV);

            imgUser = v.findViewById(R.id.pf_img_user_avatar);

            try {
                Picasso.get()
                        .load(IMAGE_VIEW_URL + App.getUserInfo().getAnhDaiDienId())
                        .placeholder(R.drawable.ic_launch)
                        .into(imgUser);

                usernameTextView.setText(App.getUserInfo().getFullname());
                phoneNumberTextView.setText(App.getUserInfo().getMobile());
                positionTextView.setText(App.getUserInfo().getRoleName());
            } catch (Exception e) {
            }


            v.setOnClickListener(this);
            v.setOnLongClickListener(this);
        }

        @Override
        protected void clear() {

        }

        @Override
        public void onBind(int position) {
            super.onBind(position);
        }

        @Override
        public void onClick(View v) {
            itemClickListener.onClick(v, getAdapterPosition(), false);
        }

        @Override
        public boolean onLongClick(View v) {
            itemClickListener.onClick(v, getAdapterPosition(), true); // Gọi interface , true là vì đây là onLongClick
            return true;
        }

        public void setItemClickListener(ItemClickListener itemClickListener) {
            this.itemClickListener = itemClickListener;
        }
    }


    public class NormalViewHolder extends BaseViewHolder {
        private TextView txtTitle;
        private ImageView imageView;
        private ImageView imageRight;
        private LinearLayout subItem;

        NormalViewHolder(View v) {
            super(v);
            txtTitle = v.findViewById(R.id.list_check_in_name);
            imageView = v.findViewById(R.id.imgListCheckInIcon);
            imageRight = v.findViewById(R.id.imageRight);
            subItem = v.findViewById(R.id.subItem);
            imageRight.setVisibility(View.GONE);
        }

        @Override
        protected void clear() {
        }

        @Override
        public void onBind(int position) {
            super.onBind(position);
            SubUserModel model = mDataset.get(position - 1);
            txtTitle.setText(model.getName());
            subItem.removeAllViews();
            List<SubModel> listSub = model.getListSub();
            boolean listNotNull = listSub != null;
            imageRight.setVisibility(listNotNull ? View.VISIBLE : View.GONE);
            if (listNotNull && model.isExpand()) {
                imageRight.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_arrow_up));
                subItem.setVisibility(View.VISIBLE);
                int padding = Utils.convertDpToPixel(context, 16);
                int lineHeight = Utils.convertDpToPixel(context, 1);
                for (SubModel subModel : listSub) {
                    TextView textView = new TextView(context);
                    textView.setPadding(padding, padding, padding, padding);
                    textView.setText(subModel.getName());
                    textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
                    textView.setOnClickListener(v -> {
                        Intent intent = new Intent(context, subModel.getClazz());
                        context.startActivity(intent);
                    });
                    subItem.addView(textView);
                    View view = new View(context);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, lineHeight);
                    params.leftMargin = padding;
                    view.setLayoutParams(params);
                    view.setBackgroundColor(ContextCompat.getColor(context, R.color.color_line));
                    subItem.addView(view);
                }
            } else {
                imageRight.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_arrow_down));
                subItem.setVisibility(View.GONE);
            }
            switch (position) {
                case 1:
                    imageView.setImageResource(R.drawable.ic_list_customer);
                    break;
                case 2:
                    imageView.setImageResource(R.drawable.ic_fingerprint);
                    break;
                case 3:
                    imageView.setImageResource(R.drawable.ic_regulation);
                    break;
                case 4:
                    imageView.setImageResource(R.drawable.salary_policy);
                    imageRight.setVisibility(View.VISIBLE);
                    break;
                case 5:
                case 6:
                    imageView.setImageResource(R.drawable.all_report);
                    break;
                default:
                    imageView.setImageBitmap(null);
            }
            itemView.setOnClickListener(view -> {
                Intent intent;
                switch (position) {
                    case 1://Danh sách khách hàng
                        intent = new Intent(context, CustomerActivities.class);
                        context.startActivity(intent);
                        break;
                    case 2:// Chấm công
                    case 4:// Luơng thưởng
                        model.setExpand(!model.isExpand());
                        notifyItemChanged(position);
                        break;
                    case 3:// Quy chế hành chính nhân sự
                        intent = new Intent(context, ListQuyCheHanhChinhNhanSuActivity.class);
                        context.startActivity(intent);
                        break;
                    case 5:// ưu đãi
                        intent = new Intent(context, DetailCoinActivity.class);
                        context.startActivity(intent);
                        break;
                    case 6: // Danh Sách xin nghỉ
                        intent = new Intent(context, DanhSachXinNghiActivity.class);
                        context.startActivity(intent);
                        break;
                    case 7: // KPI hiện tại
                        intent = new Intent(context, KPIActivity.class);
                        context.startActivity(intent);
                    default:
//                    Toast toast = Toast.makeText(context, R.string.common_coming_soon, Toast.LENGTH_SHORT);
//                    toast.show();
                        break;
                }
            });

        }
    }
}
