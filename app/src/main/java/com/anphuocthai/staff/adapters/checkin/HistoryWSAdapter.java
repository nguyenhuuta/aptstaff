package com.anphuocthai.staff.adapters.checkin;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.activities.customer.DetailWorkScheduleActivity;
import com.anphuocthai.staff.interfaces.ItemClickListener;
import com.anphuocthai.staff.model.customer.Schedule;
import com.anphuocthai.staff.utils.Utils;

import java.util.ArrayList;

public class HistoryWSAdapter extends RecyclerView.Adapter<HistoryWSAdapter.ViewHolder> {

    private ArrayList<Schedule> mDataset = new ArrayList<>();
    private Context context;


    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        public TextView workScheduleName;
        public TextView workScheduleCustomerName;
        public TextView workScheduleTime;
        public TextView txtWorkScheduleCheckin;
        private TextView txtStateSchedule;
        //public ImageView imgIcon;
        private ItemClickListener itemClickListener;
        public ViewHolder(View v) {
            super(v);
            workScheduleName = (TextView) v.findViewById(R.id.ci_txt_work_schedule_name);
            workScheduleCustomerName = v.findViewById(R.id.ci_txt_work_schedule_customer_name);
            workScheduleTime = v.findViewById(R.id.ci_txt_work_schedule_time);
            txtWorkScheduleCheckin = v.findViewById(R.id.ci_txt_check_in_work_schedule);
            txtStateSchedule = v.findViewById(R.id.txtStateSchedule);
            v.setOnClickListener(this);
            v.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            itemClickListener.onClick(v, getAdapterPosition(),false);
        }
        @Override
        public boolean onLongClick(View v) {
            itemClickListener.onClick(v,getAdapterPosition(),true); // Gọi interface , true là vì đây là onLongClick
            return true;
        }
        public void setItemClickListener(ItemClickListener itemClickListener)
        {
            this.itemClickListener = itemClickListener;
        }
    }

    public HistoryWSAdapter(ArrayList<Schedule> dataset, Context context) {
        mDataset.clear();
        mDataset.addAll(dataset);
        this.context = context;

    }

    @Override
    public HistoryWSAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_work_schedule, parent, false);
        HistoryWSAdapter.ViewHolder vh = new HistoryWSAdapter.ViewHolder(v);

        vh.setItemClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position, boolean isLongClick) {
                if(isLongClick) {

                }
                else{
                    Intent i = new Intent(context, DetailWorkScheduleActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("schedule", mDataset.get(position));
                    bundle.putBoolean("isHistory", true);
                    i.putExtra("extraschedule", bundle);
                    context.startActivity(i);

                }

            }
        });


        return vh;
    }

    @Override
    public void onBindViewHolder(HistoryWSAdapter.ViewHolder holder, int position) {
        holder.workScheduleName.setText(mDataset.get(position).getTen());
        holder.workScheduleCustomerName.setText(mDataset.get(position).getTenThanhVien());
        holder.workScheduleTime.setText(Utils.formatDateStringWithFormat("yyyy-MM-dd HH:mm:ss", "dd-MM\nHH:mm", mDataset.get(position).getNgayBatDau()));
        holder.txtWorkScheduleCheckin.setVisibility(View.INVISIBLE);

        if (mDataset.get(position).getTrangThaiText() != null){
            holder.txtStateSchedule.setText(mDataset.get(position).getTrangThaiText());
        }
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

}
