package com.anphuocthai.staff.adapters.profile;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.model.UserHopDong;
import com.anphuocthai.staff.utils.Enum;
import com.anphuocthai.staff.utils.Utils;

import java.util.ArrayList;

public class UserHopDongAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    final String fileUpload = "fileUpload";
    final int typeLink = 1;
    final int typeNormal = 0;
    private UserHopDong mDataset = new UserHopDong();
    private Context context;
    private ArrayList<String> arrTitle = new ArrayList<>();
    private ArrayList<String> arrValue = new ArrayList<>();


    public UserHopDongAdapter(UserHopDong dataset, Context context) {
        mDataset = new UserHopDong();
        mDataset = dataset;
        this.context = context;
        arrTitle.clear();

        arrTitle.add(context.getString(R.string.pf_user_contract_code));
        arrTitle.add("");//context.getString(R.string.pf_user_contract_name)
        arrTitle.add(context.getString(R.string.pf_user_contract_start_day));
        arrTitle.add(context.getString(R.string.pf_user_contract_end_day));
        arrTitle.add(context.getString(R.string.pf_user_contract_state));
        arrTitle.add(context.getString(R.string.pf_user_contract_net_salary));
        arrTitle.add(context.getString(R.string.pf_user_contract_allowance));
        arrTitle.add(context.getString(R.string.pf_user_contract_allowance_other));
        arrTitle.add(context.getString(R.string.pf_user_contract_ratio_net_salary));
        arrTitle.add(context.getString(R.string.pf_user_contract_position));
        arrTitle.add(context.getString(R.string.pf_user_contract_type));

        arrValue.clear();
        arrValue.add(dataset.getMaHopDong());
        arrValue.add(dataset.getTenHopDong());
        arrValue.add(Utils.formatDate(dataset.getNgayBatDau(), false));
        arrValue.add(Utils.formatDate(dataset.getNgayKetThuc(), false));
        arrValue.add(dataset.getIsInUseText());
        arrValue.add(Utils.formatValue(dataset.getLuongCung(), Enum.FieldValueType.CURRENCY));
        arrValue.add(Utils.formatValue(dataset.getPhuCapCongViec(), Enum.FieldValueType.CURRENCY));
        arrValue.add(Utils.formatValue(dataset.getPhuCapKhac(), Enum.FieldValueType.CURRENCY));
        arrValue.add(Utils.formatValue(dataset.getTiLeLuongCung(), Enum.FieldValueType.NORMAL) + "%");
        arrValue.add(dataset.getTenChucVu());
        arrValue.add(dataset.getLoaihopdongtext());
        String fileUpload1 = dataset.fileUpload1;
        if (fileUpload1 != null && !fileUpload1.isEmpty()) {
            arrTitle.add(fileUpload + "1");
            arrValue.add(fileUpload1);
        }

        String fileUpload2 = dataset.fileUpload2;
        if (fileUpload2 != null && !fileUpload2.isEmpty()) {
            arrTitle.add(fileUpload + "2");
            arrValue.add(fileUpload2);
        }
        String fileUpload3 = dataset.fileUpload3;
        if (fileUpload3 != null && !fileUpload3.isEmpty()) {
            arrTitle.add(fileUpload + "3");
            arrValue.add(fileUpload3);
        }


    }

    @Override
    public int getItemViewType(int position) {
        String title = arrTitle.get(position);
        if (title.contains(fileUpload)) {
            return typeLink;
        }
        return typeNormal;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == typeNormal) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_profile_list, parent, false);
            return new UserHopDongAdapter.ViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_link, parent, false);
            return new UserHopDongAdapter.LinkViewHolder(v);
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ViewHolder viewHolder = (ViewHolder) holder;
            viewHolder.binData(arrTitle.get(position), arrValue.get(position));
        } else if (holder instanceof LinkViewHolder) {
            LinkViewHolder viewHolder = (LinkViewHolder) holder;
            viewHolder.bindData(arrValue.get(position));
        }


    }

    @Override
    public int getItemCount() {
        //return mDataset.size();
        return arrTitle.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView titleProfile;
        TextView valueProfile;

        public ViewHolder(View v) {
            super(v);
            titleProfile = v.findViewById(R.id.title_profile);
            valueProfile = v.findViewById(R.id.value_profile);
        }

        void binData(String title, String value) {
            titleProfile.setText(title);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                valueProfile.setText(Html.fromHtml(value, Html.FROM_HTML_MODE_COMPACT));
            } else {
                valueProfile.setText(Html.fromHtml(value));
            }
        }
    }

    public class LinkViewHolder extends RecyclerView.ViewHolder {
        TextView link;

        LinkViewHolder(View v) {
            super(v);
            link = v.findViewById(R.id.link);
            link.setPaintFlags(link.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        }

        void bindData(String data) {
            link.setText(data);
            link.setOnClickListener(view -> {
                String path = Uri.encode("http://res.anphuocthai.vn/images/Employee/" + data);
                String urlString = "http://docs.google.com/gview?embedded=true&url=" + path;
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlString));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setPackage("com.android.chrome");
                try {
                    context.startActivity(intent);
                } catch (ActivityNotFoundException ignored) {
                }

            });
        }
    }


}
