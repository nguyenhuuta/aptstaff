package com.anphuocthai.staff.adapters.checkin;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.activities.customer.DetailWorkScheduleActivity;
import com.anphuocthai.staff.activities.customer.ListImageAlreadyCheckInActivity;
import com.anphuocthai.staff.interfaces.ItemClickListener;
import com.anphuocthai.staff.model.customer.Schedule;
import com.anphuocthai.staff.utils.ColorUtils;
import com.anphuocthai.staff.utils.Utils;

import java.util.ArrayList;

public class InDayWSAdapter extends RecyclerView.Adapter<InDayWSAdapter.ViewHolder> {

    private ArrayList<Schedule> mDataset = new ArrayList<>();
    private Context context;
    private boolean isCheckin;
    private boolean isShowInHome;

    private boolean isToday;

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        public TextView workScheduleName;
        public TextView workScheduleCustomerName;
        public TextView workScheduleTime;
        public TextView txtWorkScheduleCheckin;

        private TextView txtStateSchedule;
        //public ImageView imgIcon;
        private ItemClickListener itemClickListener;
        public ViewHolder(View v) {
            super(v);
            workScheduleName = (TextView) v.findViewById(R.id.ci_txt_work_schedule_name);
            workScheduleCustomerName = v.findViewById(R.id.ci_txt_work_schedule_customer_name);
            workScheduleTime = v.findViewById(R.id.ci_txt_work_schedule_time);
            txtWorkScheduleCheckin = v.findViewById(R.id.ci_txt_check_in_work_schedule);
            txtStateSchedule = v.findViewById(R.id.txtStateSchedule);
            v.setOnClickListener(this);
            v.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            itemClickListener.onClick(v, getAdapterPosition(),false);
        }
        @Override
        public boolean onLongClick(View v) {
            itemClickListener.onClick(v,getAdapterPosition(),true); // Gọi interface , true là vì đây là onLongClick
            return true;
        }
        public void setItemClickListener(ItemClickListener itemClickListener)
        {
            this.itemClickListener = itemClickListener;
        }
    }

    public InDayWSAdapter(ArrayList<Schedule> dataset, Context context, boolean isCheckin, boolean isShowInHome, boolean isToday) {
        mDataset.clear();
        mDataset.addAll(dataset);
        this.context = context;
        this.isCheckin = isCheckin;
        this.isShowInHome = isShowInHome;
        this.isToday = isToday;
    }

    @Override
    public InDayWSAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_work_schedule, parent, false);
        InDayWSAdapter.ViewHolder vh = new InDayWSAdapter.ViewHolder(v);

        vh.setItemClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position, boolean isLongClick) {
                if(isLongClick) {

                }
                else{
                    Intent i = new Intent(context, DetailWorkScheduleActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("schedule", mDataset.get(position));
                    bundle.putBoolean("isHistory", false);
                    i.putExtra("extraschedule", bundle);
                    context.startActivity(i);
                }
            }
        });


        return vh;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(InDayWSAdapter.ViewHolder holder, int position) {
        holder.workScheduleName.setText(mDataset.get(position).getTen());
        holder.workScheduleCustomerName.setText(mDataset.get(position).getTenThanhVien());
        if (isToday) {
            holder.workScheduleTime.setText(Utils.formatDateStringWithFormat("yyyy-MM-dd HH:mm:ss", "HH:mm", mDataset.get(position).getNgayBatDau()));
        }else {
            holder.workScheduleTime.setText(Utils.formatDateStringWithFormat("yyyy-MM-dd HH:mm:ss", "dd-MM\nHH:mm", mDataset.get(position).getNgayBatDau()));
        }
        if (isCheckin) {
            holder.txtWorkScheduleCheckin.setVisibility(View.VISIBLE);
        }else {
            holder.txtWorkScheduleCheckin.setVisibility(View.INVISIBLE);
        }

        try {
            if (mDataset.get(position).getTrangThaiId() != 1) {
               // holder.txtWorkScheduleCheckin.setTextColor(R.color.red);
                //holder.txtWorkScheduleCheckin.setTextColor(context.getResources().getColor(R.color.tran_person_color));
                holder.txtWorkScheduleCheckin.setTextColor(R.color.tran_person_color);
                ColorUtils.setTextViewDrawableColor(holder.txtWorkScheduleCheckin, R.color.tran_person_color);
                holder.txtWorkScheduleCheckin.setText(R.string.ci_already_check_in);
            }else {
                //holder.txtWorkScheduleCheckin.setTextColor(context.getResources().getColor(R.color.pink));
                holder.txtWorkScheduleCheckin.setTextColor(R.color.pink);
                ColorUtils.setTextViewDrawableColor(holder.txtWorkScheduleCheckin, R.color.pink);
                holder.txtWorkScheduleCheckin.setText(R.string.ci_work_schedule_check_in);
            }

        }catch (Exception e) {
            e.printStackTrace();
        }


        if (mDataset.get(position).getTrangThaiText() != null){
            holder.txtStateSchedule.setText(mDataset.get(position).getTrangThaiText());
        }


        holder.txtWorkScheduleCheckin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, ListImageAlreadyCheckInActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("schedule", mDataset.get(position));
                i.putExtra("scheduledata", bundle);
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }


}
