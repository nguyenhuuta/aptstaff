package com.anphuocthai.staff.adapters.profile;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.data.db.network.model.response.UserInfo;
import com.anphuocthai.staff.utils.Utils;

import java.util.ArrayList;

public class ProfileAdapter extends RecyclerView.Adapter<ProfileAdapter.ViewHolder> {

    private UserInfo mDataset = new UserInfo();
    private Context context;
    private ArrayList<String> arrTitle = new ArrayList<>();
    private ArrayList<String> arrValue = new ArrayList<>();

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView titleProfile;
        public TextView valueProfile;

        public ViewHolder(View v) {
            super(v);
            titleProfile = (TextView) v.findViewById(R.id.title_profile);
            valueProfile = (TextView) v.findViewById(R.id.value_profile);
        }
    }

    public ProfileAdapter(UserInfo dataset, Context context) {
        if (dataset == null) {
            return;
        }
        mDataset = new UserInfo();
        mDataset = dataset;

        this.context = context;

        arrTitle.clear();
        if (context != null) {
            arrTitle.add(context.getString(R.string.pf_user_name));
            arrTitle.add(context.getString(R.string.pf_user_phone));
            arrTitle.add(context.getString(R.string.pf_user_email));
            arrTitle.add(context.getString(R.string.pf_user_online_day));
        }


        arrValue.clear();
        arrValue.add(dataset.getFullname());
        arrValue.add(dataset.getMobile());
        arrValue.add(dataset.getEmail());
        arrValue.add(Utils.formatDate(dataset.getNgayTruyCap(), true));


    }

    @Override
    public ProfileAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_profile_list, parent, false);
        ProfileAdapter.ViewHolder vh = new ProfileAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ProfileAdapter.ViewHolder holder, int position) {
        //holder.titleProfile.setText(mDataset.get(position));
        holder.titleProfile.setText(arrTitle.get(position));
        holder.valueProfile.setText(arrValue.get(position));

    }

    @Override
    public int getItemCount() {
        //return mDataset.size();
        return arrTitle.size();
    }

}

