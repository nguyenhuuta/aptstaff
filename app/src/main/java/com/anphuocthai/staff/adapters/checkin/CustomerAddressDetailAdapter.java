package com.anphuocthai.staff.adapters.checkin;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.base.IBaseCallback;
import com.anphuocthai.staff.interfaces.ItemClickListener;
import com.anphuocthai.staff.model.customer.CustomerAddress;

import java.util.ArrayList;
import java.util.List;

public class CustomerAddressDetailAdapter extends RecyclerView.Adapter<CustomerAddressDetailAdapter.ViewHolder> {
    private ArrayList<CustomerAddress> mDataset = new ArrayList<>();
    private Context context;
    private List<String> arrAdress = new ArrayList<>();

    private String guid;


    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        public TextView txtAdressName;
        private TextView txtAddressTitle;
        public TextView txtAddressEdit;
        private ItemClickListener itemClickListener;

        public ViewHolder(View v, int position) {
            super(v);
            int newPos = position;
            txtAdressName = (TextView) v.findViewById(R.id.ci_txt_address_cus);
            txtAddressEdit = (TextView) v.findViewById(R.id.all_debt_btn_submit);
            txtAddressTitle = v.findViewById(R.id.all_debt_txt_order_code);
            v.setOnClickListener(this);
            v.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            itemClickListener.onClick(v, getAdapterPosition(), false);
        }

        @Override
        public boolean onLongClick(View v) {
            itemClickListener.onClick(v, getAdapterPosition(), true); // Gọi interface , true là vì đây là onLongClick
            return true;
        }

        public void setItemClickListener(ItemClickListener itemClickListener) {
            this.itemClickListener = itemClickListener;
        }
    }

    private IBaseCallback<Integer> mCallback;

    public CustomerAddressDetailAdapter(ArrayList<CustomerAddress> dataset, Context context, IBaseCallback<Integer> callback) {
        mDataset = new ArrayList<>();
        mDataset = dataset;
        mCallback = callback;
        this.context = context;

    }

    @Override
    public CustomerAddressDetailAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        CustomerAddressDetailAdapter.ViewHolder vh;
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_address_customer, parent, false);
        vh = new CustomerAddressDetailAdapter.ViewHolder(v, viewType);

        vh.setItemClickListener((view, position, isLongClick) -> {
            if (isLongClick) {
            } else {
                goToEditAddress(position);
            }
        });

        return vh;
    }

    public void goToEditAddress(int position) {
        mCallback.onCallback(position);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.txtAdressName.setText(mDataset.get(position).getDiaChi());
        holder.txtAddressTitle.setText(context.getString(R.string.common_address) + " " + (position + 1) + ":");

        holder.txtAddressEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToEditAddress(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

}
