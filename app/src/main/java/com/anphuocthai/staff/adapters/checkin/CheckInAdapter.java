package com.anphuocthai.staff.adapters.checkin;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.activities.customer.WorkScheduleActivity;
import com.anphuocthai.staff.interfaces.ItemClickListener;
import com.anphuocthai.staff.ui.delivery.debt.DebtManagerActivity;
import com.anphuocthai.staff.ui.delivery.myorder.MyOrderActivity;
import com.anphuocthai.staff.ui.delivery.nottransportyet.NotTranActivity;
import com.anphuocthai.staff.ui.delivery.preparetransport.PrepareTranActivity;
import com.anphuocthai.staff.ui.delivery.shipped.ShippedActivity;

import java.util.ArrayList;

/*
 * This adapter for recyclerview in checkin tab at home.
 * Danh sách khách hàng và lịch chăm sóc khách hàng
 * */
public class CheckInAdapter extends RecyclerView.Adapter<CheckInAdapter.ViewHolder> {

    private ArrayList<String> mDataset = new ArrayList<>();

    private ArrayList<String> arrayItemNumber = new ArrayList<>();

    public void setArrayItemNumber(ArrayList<String> arrayItemNumber) {
        this.arrayItemNumber = arrayItemNumber;
        notifyDataSetChanged();
    }


    private Context context;

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        TextView mTextView;
        ImageView imgIcon;
        private TextView txtNumber;
        private TextView txtNumberItemNew;

        private ItemClickListener itemClickListener;

        public ViewHolder(View v) {
            super(v);
            mTextView = v.findViewById(R.id.list_check_in_name);
            imgIcon = v.findViewById(R.id.imgListCheckInIcon);
            txtNumber = v.findViewById(R.id.txt_number_item);
            txtNumberItemNew = v.findViewById(R.id.txt_number_item_new);
            txtNumberItemNew.setVisibility(View.GONE);
            v.setOnClickListener(this);
            v.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            itemClickListener.onClick(v, getAdapterPosition(), false);
        }

        @Override
        public boolean onLongClick(View v) {
            itemClickListener.onClick(v, getAdapterPosition(), true); // Gọi interface , true là vì đây là onLongClick
            return true;
        }

        public void setItemClickListener(ItemClickListener itemClickListener) {
            this.itemClickListener = itemClickListener;
        }
    }

    public CheckInAdapter(ArrayList<String> dataset, Context context) {
        mDataset.clear();
        mDataset.addAll(dataset);
        this.context = context;
    }

    @Override
    public CheckInAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_check_in_item, parent, false);
        CheckInAdapter.ViewHolder vh = new CheckInAdapter.ViewHolder(v);

        vh.setItemClickListener((view, position, isLongClick) -> {
            if (isLongClick) {
                //Toast.makeText(context, "Long click", Toast.LENGTH_SHORT).show();
            } else {

                if (position == 0) {
                    Toast.makeText(context, R.string.ci_adap_schedule_customer_care, Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(context, WorkScheduleActivity.class);
                    context.startActivity(i);
                } else if (position == 1) {
                    Intent i = MyOrderActivity.getMyOrderIntent(context);
                    context.startActivity(i);
                } else if (position == 2) {
                    Intent i = NotTranActivity.getNotTranIntent(context);
                    context.startActivity(i);
                } else if (position == 3) {
                    Intent i = PrepareTranActivity.getPrepareTranIntent(context);
                    context.startActivity(i);
                } else if (position == 4) {
                    Intent i = ShippedActivity.getShippedIntent(context);
                    context.startActivity(i);
                } else if (position == 5) {
                    Intent i = DebtManagerActivity.getDebtIntent(context);
                    context.startActivity(i);
                }
            }

        });


        return vh;
    }

    @Override
    public void onBindViewHolder(CheckInAdapter.ViewHolder holder, int position) {
        holder.mTextView.setText(mDataset.get(position));
        if (position == 0) {
            holder.imgIcon.setImageResource(R.drawable.ic_customer_schedule);
            holder.txtNumber.setVisibility(View.GONE);
            holder.txtNumberItemNew.setVisibility(View.GONE);
        }
        if (position == 1) {
            holder.imgIcon.setImageResource(R.drawable.ic_my_order_menu);
            if (arrayItemNumber.size() > 0 && arrayItemNumber.get(0) != null) {
                holder.txtNumber.setVisibility(View.INVISIBLE);

                if (Integer.parseInt(arrayItemNumber.get(0)) > 0) {
                    holder.txtNumberItemNew.setVisibility(View.VISIBLE);
                    holder.txtNumberItemNew.setText(arrayItemNumber.get(0) + " " + context.getString(R.string.common_new));
                }
            }

        }
        if (position == 2) {
            holder.imgIcon.setImageResource(R.drawable.ic_not_tran);
            if (arrayItemNumber.size() > 1 && arrayItemNumber.get(1) != null) {
                holder.txtNumber.setText(arrayItemNumber.get(1));
            }
        }
        if (position == 3) {
            holder.imgIcon.setImageResource(R.drawable.ic_near_tran);
            if (arrayItemNumber.size() > 2 && arrayItemNumber.get(2) != null) {
                holder.txtNumber.setText(arrayItemNumber.get(2));
            }
        }
        if (position == 4) {
            holder.imgIcon.setImageResource(R.drawable.ic_already_tran);
            if (arrayItemNumber.size() > 3 && arrayItemNumber.get(3) != null) {
                holder.txtNumber.setText(arrayItemNumber.get(3));
            }
        }
        if (position == 5) {
            holder.imgIcon.setImageResource(R.drawable.ic_debt);
            if (arrayItemNumber.size() > 4 && arrayItemNumber.get(4) != null) {
                holder.txtNumber.setText(arrayItemNumber.get(4));
            }
        }
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

}
