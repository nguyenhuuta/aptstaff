package com.anphuocthai.staff.adapters.checkin;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.anphuocthai.staff.R;
import com.anphuocthai.staff.activities.customer.AddNewWorkScheduleActivity;
import com.anphuocthai.staff.activities.customer.ListCustomerActivity;
import com.anphuocthai.staff.interfaces.ItemClickListener;
import com.anphuocthai.staff.model.customer.Customer;
import com.anphuocthai.staff.ui.customer.detail.DetailCustomerActivity;

import java.util.ArrayList;

public class CustomerListAdapter extends RecyclerView.Adapter<CustomerListAdapter.ItemCustomerViewHolder> {


    public interface OnPhoneClickListener{
        void phoneClick(String phone);
    }

    class ItemCustomerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener{
        private TextView customerName;
        private TextView customerPhone;
        private TextView customerAddress;
        private ItemClickListener itemClickListener;


        public ItemCustomerViewHolder(View itemView)  {
            super(itemView);
            customerName = (TextView) itemView.findViewById(R.id.txtCustomerItemName);
            customerPhone = itemView.findViewById(R.id.txtCustomerItemPhone);
            customerAddress = itemView.findViewById(R.id.txtAddressCustomerItem);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            itemClickListener.onClick(v, getAdapterPosition(),false);
        }
        @Override
        public boolean onLongClick(View v) {
            itemClickListener.onClick(v,getAdapterPosition(),true); // Gọi interface , true là vì đây là onLongClick
            return true;
        }
        public void setItemClickListener(ItemClickListener itemClickListener)
        {
            this.itemClickListener = itemClickListener;
        }
    }

    private ListCustomerActivity context;
    private ArrayList<Customer> arrayList;
    private boolean isSelectCustomerToSchedule = false;
    private OnPhoneClickListener callback;

    public void setOnPhoneClickListener(OnPhoneClickListener callback) {
        this.callback = callback;
    }

    public CustomerListAdapter(ListCustomerActivity context, ArrayList<Customer> arrayList, boolean isSelectCustomerToSchedule) {
        this.context = context;
        this.arrayList = arrayList;
        this.isSelectCustomerToSchedule = isSelectCustomerToSchedule;
    }

    @Override
    public ItemCustomerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_customer_row_layout, parent, false);
        ItemCustomerViewHolder vh = new ItemCustomerViewHolder(view);
        vh.setItemClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position, boolean isLongClick) {
                if(isLongClick) {
                }
                else{
                    if (isSelectCustomerToSchedule) {
                        Intent i = new Intent(context, AddNewWorkScheduleActivity.class);
                        Bundle b = new Bundle();
                        b.putString("guid", arrayList.get(position).getGuid());
                        b.putString("customername", arrayList.get(position).getTenDayDu());
                        b.putBoolean("isEdit", false);
                        i.putExtra("schedulenewworkextra", b);
                        context.startActivity(i);
                        context.finish();
                    }else {
                        Intent i = new Intent(context, DetailCustomerActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("customerguid", arrayList.get(position).getGuid());
                        bundle.putString("customername", arrayList.get(position).getTenDayDu());
                        i.putExtra("detailcustomeractivity", bundle);
                        context.startActivity(i);
                    }

                }

            }
        });
        return vh;
    }

    @Override
    public void onBindViewHolder(ItemCustomerViewHolder holder, int position) {
        holder.customerName.setText(arrayList.get(position).getTenDayDu());
        holder.customerPhone.setText(arrayList.get(position).getDienThoai());
        holder.customerAddress.setText(arrayList.get(position).getDiaChi());
        holder.customerPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.phoneClick(arrayList.get(position).getDienThoai());
            }
        });
    }



    @Override
    public int getItemCount() {
        return arrayList.size();
    }


}
