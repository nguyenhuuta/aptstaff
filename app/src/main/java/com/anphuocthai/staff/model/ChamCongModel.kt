package com.anphuocthai.staff.model

import com.anphuocthai.staff.api.BaseResponse
import com.anphuocthai.staff.baserequest.APIResponse

/**
 * Created by OpenYourEyes on 7/21/2020
 */
open class ChamCongModel {
    var id: Int? = 0
    var userID: Int? = 0
    var ngayChamCong: String? = ""
    var ngayTao: String? = ""
    var machineID: String? = ""
    var userDeviceID: String? = ""
    var lat: String? = ""
    var lng: String? = ""
    var placeName: String? = ""
    var placeDescription: String? = ""
    var imageBase64: String? = ""
    var isXacNhan: Boolean? = false
}

data class ChiTietCongTacModel(
        val id: Int?,
        val userId: Int?,
        val fullname: String?,
        val nguoiGui: Int?,
        val tenNguoiGui: String?,
        val thoiGianChamCong: String?,
        val noiDung: String?,
        val createDate: String?,
        val duLieuChamCongId: Int?,
        val isView: Int?
)

class ChamCongResponse : BaseResponse<ChamCongModel>()
class DanhSachCongTacResponse : APIResponse<MutableList<CongTacModel>>()
class DetailTimeKeepingResponse : APIResponse<MutableList<ChamCongModel>?>()

class ChiTietCongTacResponse : APIResponse<MutableList<ChiTietCongTacModel>>()


data class InsertCongTacBody(val DuLieuChamCongId: Int, val NoiDung: String, val ThoiGianChamCong: String)
class InsertCongTacResponse : BaseResponse<ChiTietCongTacModel>()
