package com.anphuocthai.staff.model.customer;

//[
//        {
//        "id": 13,
//        "iD_ThanhVien": 19,
//        "diaChi": "85/22 Hạ Đình, Thanh Xuân Trung, Thanh Xuân, Hà Nội, Việt Nam, Thanh Xuân Trung",
//        "tinhID": 1,
//        "latitude": 20.9911,
//        "longitude": 105.80992,
//        "moTa": "",
//        "isChinh": true,
//        "dienThoai": "",
//        "guidThanhVien": null
//        }
//        ]

import java.io.Serializable;

public class CustomerAddress implements Serializable {
    int id;
    int iD_ThanhVien;
    String diaChi;
    int tinhID;
    int quanID;
    int xaID;
    Double latitude;
    Double longitude;
    String moTa;
    boolean isChinh;
    String dienThoai;
    String guidThanhVien;

    String soNha;

    public CustomerAddress(int id, int iD_ThanhVien, String diaChi, int tinhID, Double latitude, Double longitude, String moTa, boolean isChinh, String dienThoai, String guidThanhVien) {
        this.id = id;
        this.iD_ThanhVien = iD_ThanhVien;
        this.diaChi = diaChi;
        this.tinhID = tinhID;
        this.latitude = latitude;
        this.longitude = longitude;
        this.moTa = moTa;
        this.isChinh = isChinh;
        this.dienThoai = dienThoai;
        this.guidThanhVien = guidThanhVien;
    }

    public CustomerAddress() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getiD_ThanhVien() {
        return iD_ThanhVien;
    }

    public void setiD_ThanhVien(int iD_ThanhVien) {
        this.iD_ThanhVien = iD_ThanhVien;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public int getTinhID() {
        return tinhID;
    }

    public void setTinhID(int tinhID) {
        this.tinhID = tinhID;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public boolean isChinh() {
        return isChinh;
    }

    public void setChinh(boolean chinh) {
        isChinh = chinh;
    }

    public String getDienThoai() {
        return dienThoai;
    }

    public void setDienThoai(String dienThoai) {
        this.dienThoai = dienThoai;
    }

    public String getGuidThanhVien() {
        return guidThanhVien;
    }

    public void setGuidThanhVien(String guidThanhVien) {
        this.guidThanhVien = guidThanhVien;
    }

    public int getQuanID() {
        return quanID;
    }

    public void setQuanID(int quanID) {
        this.quanID = quanID;
    }

    public int getXaID() {
        return xaID;
    }

    public void setXaID(int xaID) {
        this.xaID = xaID;
    }

    public String getSoNha() {
        return soNha;
    }

    public void setSoNha(String soNha) {
        this.soNha = soNha;
    }
}
