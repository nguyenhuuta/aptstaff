package com.anphuocthai.staff.model

/**
 * Created by OpenYourEyes on 1/19/21
 */
data class CauHinhModel(
        val cauHinhHeThongID: Int?,
        val qlDonDat_CheDoChuyenCN: Boolean?,
        val chietKhauToiDa: Int?,
        val cheDoPhatDonHang: Boolean?,
        val cheDoPhatQLCongNo: Boolean?,
        val sO_TIEN_TAM_UNG_LON_NHAT: Int?,
        val sO_NGAY_TRA_HANG: Int?,
        val quyUocCV: Int?,
        val phaT_VUOT_MUC_CONG_NO: Int?,
        val phaT_NOP_MUON_CONG_NO: Int?,
        val ngaY_TINH_PHAT_CONG_NO: Int?
)
