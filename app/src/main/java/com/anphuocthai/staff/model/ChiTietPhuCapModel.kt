package com.anphuocthai.staff.model

import com.anphuocthai.staff.baserequest.APIResponse

/**
 * Created by OpenYourEyes on 9/8/2020
 */

data class ChiTietPhuCapModel(
        var ten: String?,
        var luongPhuCap: Float?
)

class ChiTietPhuCapResponse : APIResponse<List<ChiTietPhuCapModel>>()
