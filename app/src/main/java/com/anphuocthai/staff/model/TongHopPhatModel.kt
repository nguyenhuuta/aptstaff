package com.anphuocthai.staff.model

/**
 * Created by OpenYourEyes on 4/1/21
 */
data class TongHopPhatModel(
        val id: String?,
        val ngayTao: String?,
        val soTien: Long?,
        val loaiId: Int?,
        val moTa: String?,
        val tenLoaiPhat: String?,
        val tenNhomPhat: String?
) {
}