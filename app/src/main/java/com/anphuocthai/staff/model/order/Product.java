package com.anphuocthai.staff.model.order;

import com.anphuocthai.staff.api.ApiURL;
import com.anphuocthai.staff.data.db.model.OrderProduct;
import com.anphuocthai.staff.ui.delivery.model.GiaChinh;
import com.anphuocthai.staff.ui.delivery.model.Thuoctinhs;
import com.anphuocthai.staff.ui.product.OrderLogic;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Product implements Serializable {

    private static final long serialVersionUID = 1L;

    @SerializedName("dD_HangHoaID")
    int id;

    @SerializedName("maHangHoa")
    String commodityCode;

    @SerializedName("tenHangHoa")
    String name;

    @SerializedName("moTa")
    String description;

    @SerializedName("chiTiet")
    String detail;

    @SerializedName("duLieu")
    String data;

    @SerializedName("thuoctinhs")

    Thuoctinhs properties;

    @SerializedName("giaChinh")
    GiaChinh mainPrice;

    @SerializedName("tenCongTy")
    String companyName;

    @SerializedName("tenDangSanPham")
    String typeName;

    @SerializedName("xuatXu")
    String source;

    @SerializedName("thuongHieu")
    String tradeMark;

    @SerializedName("nhomSanPhamId")
    int productCategoryId;

    @SerializedName("tenNhomSanPham")
    String productCategoryName;

    @SerializedName("moTaNgan")
    String shortDescription;

    @SerializedName("anhDaiDienId")
    int thumbId;
    @SerializedName("isHetHang")
    private boolean isHetHang;

    @SerializedName("tonKho")
    private float tonKho;

    @SerializedName("ddhh_imgs")
    private List<ImageProductModel> images;

    public List<ImageProductModel> getImages() {
        if (images == null || images.size() == 0) {
            if (images == null) {
                images = new ArrayList<>();
            }
            images.add(new ImageProductModel(ApiURL.IMAGE_VIEW_URL + thumbId));
        } else {
            images.add(0, new ImageProductModel(ApiURL.IMAGE_VIEW_URL + thumbId));
        }
        return images;
    }

    private boolean isSelected = false;

    public float getTonKho() {
        return tonKho;
    }
    //    private CheckChangeProduct checkChangeProduct;

    private String whatNew;

    OrderLogic orderLogic = new OrderLogic();


    public OrderProduct toOrderProduct(String customerName, String customerGuid) {
        OrderProduct orderProduct = new OrderProduct();

        orderProduct.setCustomerName(customerName);
        orderProduct.setCustomerGuid(customerGuid);
        orderProduct.setProductId(String.valueOf(id));
        orderProduct.setQuantity(OrderLogic.DEFAULT_QUANTITY);

        // additional infomation

        orderProduct.setProductCode(commodityCode);
        orderProduct.setProductName(name);
        orderProduct.setDescription(description);
        orderProduct.setDetail(detail);
        orderProduct.setData(data);
        orderProduct.setProperty(properties);
        orderProduct.setMainPrice(mainPrice);
        orderProduct.setCompanyName(companyName);
        orderProduct.setTypeName(typeName);
        orderProduct.setSource(source);
        orderProduct.setTradeMark(tradeMark);
        orderProduct.setProductCategoryName(productCategoryName);
        orderProduct.setProductCategoryId(productCategoryId);
        orderProduct.setShortDescription(shortDescription);
        orderProduct.setThumbId(thumbId);

        // order logic setup
        orderLogic.setCv(this.getMainPrice().getKDCV());

        orderLogic.setLowerThreshold(this.getMainPrice().getKDBanBuonSoLuong2());
        orderLogic.setQuantityThreshold(this.getMainPrice().getKDBanBuonSoLuong1());
        orderLogic.setPriceRetail(this.getMainPrice().getKDBanLeGia());
        orderLogic.setPriceWholeSale(this.getMainPrice().getKDBanBuonGia());
        orderLogic.setFixCV(this.getMainPrice().getKDCV());
        orderProduct.setOrderLogic(orderLogic);
        orderProduct.setProductNote("");
        return orderProduct;
    }

    public Product() {
    }


    public GiaChinh getMainPrice() {
        return mainPrice;
    }

    public void setMainPrice(GiaChinh mainPrice) {
        this.mainPrice = mainPrice;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCommodityCode() {
        return commodityCode;
    }

    public void setCommodityCode(String commodityCode) {
        this.commodityCode = commodityCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Thuoctinhs getProperties() {
        return properties;
    }

    public void setProperties(Thuoctinhs properties) {
        this.properties = properties;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTradeMark() {
        return tradeMark;
    }

    public void setTradeMark(String tradeMark) {
        this.tradeMark = tradeMark;
    }

    public String getProductCategoryName() {
        return productCategoryName;
    }

    public void setProductCategoryName(String productCategoryName) {
        this.productCategoryName = productCategoryName;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public int getThumbId() {
        return thumbId;
    }

    public void setThumbId(int thumbId) {
        this.thumbId = thumbId;
    }

    public int getProductCategoryId() {
        return productCategoryId;
    }

    public void setProductCategoryId(int productCategoryId) {
        this.productCategoryId = productCategoryId;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", commodityCode='" + commodityCode + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", detail='" + detail + '\'' +
                ", data='" + data + '\'' +
                ", properties=" + properties.toString() +
                ", companyName='" + companyName + '\'' +
                ", typeName='" + typeName + '\'' +
                ", source='" + source + '\'' +
                ", tradeMark='" + tradeMark + '\'' +
                ", productCategoryName='" + productCategoryName + '\'' +
                ", shortDescription='" + shortDescription + '\'' +
                ", thumbId=" + thumbId +
                '}';
    }

    public OrderLogic getOrderLogic() {
        return orderLogic;
    }

    public void setOrderLogic(OrderLogic orderLogic) {
        this.orderLogic = orderLogic;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }


//    public void setCheckChangeProduct(CheckChangeProduct checkChangeProduct) {
//        this.checkChangeProduct = checkChangeProduct;
//    }
//
//    public CheckChangeProduct getCheckChangeProduct() {
//        return checkChangeProduct;
//    }


    public void setHetHang(boolean hetHang) {
        isHetHang = hetHang;
    }

    public boolean isHetHang() {
        return isHetHang;
    }


    public String getWhatNew() {
        return whatNew;
    }

    public void setWhatNew(String whatNew) {
        this.whatNew = whatNew;
    }
}
