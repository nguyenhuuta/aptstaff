package com.anphuocthai.staff.model;

public class ChangePass {
    String code;
    String message;
    String idRecord;

    public ChangePass(String code, String message, String idRecord) {
        this.code = code;
        this.message = message;
        this.idRecord = idRecord;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(String idRecord) {
        this.idRecord = idRecord;
    }
}
