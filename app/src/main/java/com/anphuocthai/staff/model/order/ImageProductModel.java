package com.anphuocthai.staff.model.order;

import java.io.Serializable;

/**
 * Created by OpenYourEyes on 8/3/2020
 */
public class ImageProductModel implements Serializable {
    int id;
    String name;
    int dD_HangHoaID;
    String url;
    int orderImage;

    public ImageProductModel() {
    }

    public ImageProductModel(String url) {
        this.id = 0;
        this.name = "name";
        this.dD_HangHoaID = 0;
        this.url = url;
        this.orderImage = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getdD_HangHoaID() {
        return dD_HangHoaID;
    }

    public void setdD_HangHoaID(int dD_HangHoaID) {
        this.dD_HangHoaID = dD_HangHoaID;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getOrderImage() {
        return orderImage;
    }

    public void setOrderImage(int orderImage) {
        this.orderImage = orderImage;
    }
}
