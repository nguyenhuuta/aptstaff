package com.anphuocthai.staff.model

import android.os.Parcel
import android.os.Parcelable
import com.anphuocthai.staff.baserequest.APIResponse

/**
 * Created by OpenYourEyes on 8/13/2020
 */
class BannerModel(
        var id: Int?,
        var ten: String?,
        var url: String?,
        var baiVietId: Int?,
        var moduleId: Int?,
        var moTa: String?,
        var noiDungBaiViet: String?,
        var tenBaiViet: String?
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readValue(Int::class.java.classLoader) as? Int,
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id)
        parcel.writeString(ten)
        parcel.writeString(url)
        parcel.writeValue(baiVietId)
        parcel.writeValue(moduleId)
        parcel.writeString(moTa)
        parcel.writeString(noiDungBaiViet)
        parcel.writeString(tenBaiViet)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<BannerModel> {
        override fun createFromParcel(parcel: Parcel): BannerModel {
            return BannerModel(parcel)
        }

        override fun newArray(size: Int): Array<BannerModel?> {
            return arrayOfNulls(size)
        }
    }
}

class BannerResponse : APIResponse<MutableList<BannerModel>>()
class DetailBannerResponse : APIResponse<BannerModel>()