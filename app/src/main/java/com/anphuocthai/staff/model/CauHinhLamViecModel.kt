package com.anphuocthai.staff.model

/**
 * Created by OpenYourEyes on 3/2/21
 */
data class CauHinhLamViecModel(val vaoSang: String?,
                               val raSang: String?,
                               val vaoChieu: String?,
                               val raChieu: String?,
                               val gioLamCaChieu: Float?,
                               val gioLamCaSang: Float?
) {
    fun tongSoGioLamViec(): Float {
        return (gioLamCaChieu ?: 0f) + (gioLamCaSang ?: 0f)
    }

    /**
     * Giờ vào sáng - ra sáng
     * Giờ vào chiều - ra chiều
     */
    fun toHours(): MutableList<String> {
        fun timeToHour(time: String?): String {
            //time 10-01-2021 15:00:00
            return time?.let {
                val array = it.split(" ")
                if (array.size == 2) {
                    return@let array[1]
                }
                return@let ""
            } ?: kotlin.run { "" }
        }
        return mutableListOf(timeToHour(vaoSang), timeToHour(raSang), timeToHour(vaoChieu), timeToHour(raChieu))

    }
}
