package com.anphuocthai.staff.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

/**
 * Created by OpenYourEyes on 1/5/2021
 */
@Entity(tableName = "CongTacModel")
class CongTacModel {
    @PrimaryKey
    @ColumnInfo(name = "id")
    var id: Int? = 0

    @ColumnInfo(name = "ngayChamCong")
    var ngayChamCong: String? = ""

    @ColumnInfo(name = "placeDescription")
    var placeDescription: String? = ""

    @ColumnInfo(name = "imageBase64")
    var imageBase64: String? = ""

    @ColumnInfo(name = "totalYeuCau")
    var totalYeuCau: Int? = 0

    @ColumnInfo(name = "month")
    var month: Int = 0

    @Ignore
    var hasNew: Boolean = false
}

//{
//    "id": 151089,
//    "userID": 7,
//    "ngayChamCong": "2021-01-05 21:35:44",
//    "ngayTao": "2021-01-05 21:35:44",
//    "machineID": "",
//    "userDeviceID": "",
//    "lat": "21.0221307",
//    "lng": "105.7722057",
//    "placeName": "AnPhuocThai",
//    "placeDescription": "test cham cong",
//    "imageBase64": "http://res.anphuocthai.vn/images/thumb/chamCong_1609857345979.jpg",
//    "imageName": null,
//    "isXacNhan": false,
//    "username": "hoapt",
//    "fullname": "Phạm Thanh Hoa",
//    "chucVuID": 24,
//    "tenChucVu": "TP Thu mua",
//    "totalYeuCau": 1
//}