package com.anphuocthai.staff.model;

public class UserHopDong {

    int id;
    int userID;
    String maHopDong;
    String tenHopDong;
    String ngayBatDau;
    String ngayKetThuc;
    String ngayTao;
    String nguoiTao;
    int loaiHopDongId;
    Boolean isInUse;
    String isInUseText;
    int chucVuID;
    double luongCung;
    double phuCapCongViec;
    double phuCapKhac;
    float tiLeLuongCung;
    String tenChucVu;
    int loaihopdong;
    String loaihopdongtext;
    public String fileUpload1;
    public String fileUpload2;
    public String fileUpload3;

    public UserHopDong() {

    }

    public UserHopDong(int id, int userID, String maHopDong, String tenHopDong, String ngayBatDau, String ngayKetThuc, String ngayTao, String nguoiTao, int loaiHopDongId, Boolean isInUse, String isInUseText, int chucVuID, double luongCung, double phuCapCongViec, double phuCapKhac, float tiLeLuongCung, String tenChucVu, int loaihopdong, String loaihopdongtext) {
        this.id = id;
        this.userID = userID;
        this.maHopDong = maHopDong;
        this.tenHopDong = tenHopDong;
        this.ngayBatDau = ngayBatDau;
        this.ngayKetThuc = ngayKetThuc;
        this.ngayTao = ngayTao;
        this.nguoiTao = nguoiTao;
        this.loaiHopDongId = loaiHopDongId;
        this.isInUse = isInUse;
        this.isInUseText = isInUseText;
        this.chucVuID = chucVuID;
        this.luongCung = luongCung;
        this.phuCapCongViec = phuCapCongViec;
        this.phuCapKhac = phuCapKhac;
        this.tiLeLuongCung = tiLeLuongCung;
        this.tenChucVu = tenChucVu;
        this.loaihopdong = loaihopdong;
        this.loaihopdongtext = loaihopdongtext;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getMaHopDong() {
        return maHopDong;
    }

    public void setMaHopDong(String maHopDong) {
        this.maHopDong = maHopDong;
    }

    public String getTenHopDong() {
        return tenHopDong;
    }

    public void setTenHopDong(String tenHopDong) {
        this.tenHopDong = tenHopDong;
    }

    public String getNgayBatDau() {
        return ngayBatDau;
    }

    public void setNgayBatDau(String ngayBatDau) {
        this.ngayBatDau = ngayBatDau;
    }

    public String getNgayKetThuc() {
        return ngayKetThuc;
    }

    public void setNgayKetThuc(String ngayKetThuc) {
        this.ngayKetThuc = ngayKetThuc;
    }

    public String getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(String ngayTao) {
        this.ngayTao = ngayTao;
    }

    public String getNguoiTao() {
        return nguoiTao;
    }

    public void setNguoiTao(String nguoiTao) {
        this.nguoiTao = nguoiTao;
    }

    public int getLoaiHopDongId() {
        return loaiHopDongId;
    }

    public void setLoaiHopDongId(int loaiHopDongId) {
        this.loaiHopDongId = loaiHopDongId;
    }

    public Boolean getInUse() {
        return isInUse;
    }

    public void setInUse(Boolean inUse) {
        isInUse = inUse;
    }

    public String getIsInUseText() {
        return isInUseText;
    }

    public void setIsInUseText(String isInUseText) {
        this.isInUseText = isInUseText;
    }

    public int getChucVuID() {
        return chucVuID;
    }

    public void setChucVuID(int chucVuID) {
        this.chucVuID = chucVuID;
    }

    public double getLuongCung() {
        return luongCung;
    }

    public void setLuongCung(double luongCung) {
        this.luongCung = luongCung;
    }

    public double getPhuCapCongViec() {
        return phuCapCongViec;
    }

    public void setPhuCapCongViec(double phuCapCongViec) {
        this.phuCapCongViec = phuCapCongViec;
    }

    public double getPhuCapKhac() {
        return phuCapKhac;
    }

    public void setPhuCapKhac(double phuCapKhac) {
        this.phuCapKhac = phuCapKhac;
    }

    public float getTiLeLuongCung() {
        return tiLeLuongCung;
    }

    public void setTiLeLuongCung(float tiLeLuongCung) {
        this.tiLeLuongCung = tiLeLuongCung;
    }

    public String getTenChucVu() {
        return tenChucVu;
    }

    public void setTenChucVu(String tenChucVu) {
        this.tenChucVu = tenChucVu;
    }

    public int getLoaihopdong() {
        return loaihopdong;
    }

    public void setLoaihopdong(int loaihopdong) {
        this.loaihopdong = loaihopdong;
    }

    public String getLoaihopdongtext() {
        return loaihopdongtext;
    }

    public void setLoaihopdongtext(String loaihopdongtext) {
        this.loaihopdongtext = loaihopdongtext;
    }
}
