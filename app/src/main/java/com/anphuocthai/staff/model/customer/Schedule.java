package com.anphuocthai.staff.model.customer;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Schedule implements Serializable {
    int id;
    int userId;
    String guidUser;
    @SerializedName("iD_ThanhVien")
    int idThanhVien;
    String guidThanhVien;
    String tenThanhVien;
    String ten;
    String moTa;
    String ngayTao;
    String ngayBatDau;
    String ngayKetThuc;
    int trangThaiId;
    int loaiId;
    boolean isDinhKy;
    String viTri;
    String trangThaiText;

    public Schedule(int id, int userId, String guidUser, int idThanhVien, String guidThanhVien, String tenThanhVien, String ten, String moTa, String ngayTao, String ngayBatDau, String ngayKetThuc, int trangThaiId, int loaiId, boolean isDinhKy, String viTri) {
        this.id = id;
        this.userId = userId;
        this.guidUser = guidUser;
        this.idThanhVien = idThanhVien;
        this.guidThanhVien = guidThanhVien;
        this.tenThanhVien = tenThanhVien;
        this.ten = ten;
        this.moTa = moTa;
        this.ngayTao = ngayTao;
        this.ngayBatDau = ngayBatDau;
        this.ngayKetThuc = ngayKetThuc;
        this.trangThaiId = trangThaiId;
        this.loaiId = loaiId;
        this.isDinhKy = isDinhKy;
        this.viTri = viTri;
    }

    public Schedule() {
    }

    public String getTenThanhVien() {
        return tenThanhVien;
    }

    public void setTenThanhVien(String tenThanhVien) {
        this.tenThanhVien = tenThanhVien;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getGuidUser() {
        return guidUser;
    }

    public void setGuidUser(String guidUser) {
        this.guidUser = guidUser;
    }

    public int getIdThanhVien() {
        return idThanhVien;
    }

    public void setIdThanhVien(int idThanhVien) {
        this.idThanhVien = idThanhVien;
    }

    public String getGuidThanhVien() {
        return guidThanhVien;
    }

    public void setGuidThanhVien(String guidThanhVien) {
        this.guidThanhVien = guidThanhVien;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public String getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(String ngayTao) {
        this.ngayTao = ngayTao;
    }

    public String getNgayBatDau() {
        return ngayBatDau;
    }

    public void setNgayBatDau(String ngayBatDau) {
        this.ngayBatDau = ngayBatDau;
    }

    public String getNgayKetThuc() {
        return ngayKetThuc;
    }

    public void setNgayKetThuc(String ngayKetThuc) {
        this.ngayKetThuc = ngayKetThuc;
    }

    public int getTrangThaiId() {
        return trangThaiId;
    }

    public void setTrangThaiId(int trangThaiId) {
        this.trangThaiId = trangThaiId;
    }

    public int getLoaiId() {
        return loaiId;
    }

    public void setLoaiId(int loaiId) {
        this.loaiId = loaiId;
    }

    public boolean isDinhKy() {
        return isDinhKy;
    }

    public void setDinhKy(boolean dinhKy) {
        isDinhKy = dinhKy;
    }

    public String getViTri() {
        return viTri;
    }

    public void setViTri(String viTri) {
        this.viTri = viTri;
    }

    public String getTrangThaiText() {
        return trangThaiText;
    }

    public void setTrangThaiText(String trangThaiText) {
        this.trangThaiText = trangThaiText;
    }
}
