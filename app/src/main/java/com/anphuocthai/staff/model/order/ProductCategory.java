package com.anphuocthai.staff.model.order;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ProductCategory implements Serializable {

    int id;
    @SerializedName("ten")
    String name;
    @SerializedName("iconId")
    int categoryThumb;


    private int numberItemNew;
    private boolean isNewCategory;
    private int numberProductSelected = 0;

    private boolean isSelected;

    public ProductCategory(int id, String name, int categoryThumb) {
        this.id = id;
        this.name = name;
        this.categoryThumb = categoryThumb;
        isNewCategory = true;
    }

    public ProductCategory() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCategoryThumb() {
        return categoryThumb;
    }

    public void setCategoryThumb(int categoryThumb) {
        this.categoryThumb = categoryThumb;
    }

    public void setNumberItemNew(int numberItemNew) {
        this.numberItemNew = numberItemNew;
    }

    public int getNumberItemNew() {
        return numberItemNew;
    }

    public void setNewCategory(boolean newCategory) {
        isNewCategory = newCategory;
    }

    public boolean isNewCategory() {
        return isNewCategory;
    }

    public void setNumberProductSelected(int numberProductSelected) {
        this.numberProductSelected = numberProductSelected;
    }

    public void incrementNumberProductSelected() {
        this.numberProductSelected++;
    }

    public void decrementNumberProductSelected() {
        this.numberProductSelected--;
    }

    public int getNumberProductSelected() {
        return numberProductSelected;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
