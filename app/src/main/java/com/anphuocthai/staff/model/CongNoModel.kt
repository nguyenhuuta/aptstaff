package com.anphuocthai.staff.model

import com.anphuocthai.staff.baserequest.APIResponse
import com.anphuocthai.staff.ui.delivery.model.Hanghoa
import com.anphuocthai.staff.ui.delivery.model.OrderTran
import com.anphuocthai.staff.ui.delivery.model.Vanchuyen
import com.anphuocthai.staff.utils.*
import com.anphuocthai.staff.utils.Enum

/**
 * Created by OpenYourEyes on 10/21/2020
 */


data class CongNoModel(
        val qlCongNoID: Int?,
        val qlCongNoIDs: Int?,
        val dDDonDATID: Int?,
        val maDonDat: String?,
        val tongTienPhaiThu: Long?,
        val kinhDoanhID: Long?,
        val ngayBatDau: String?,
        val ngayKetThuc: String?,
        val ngayBatDauF: String?,
        val ngayKetThucF: String?,
        val tVThanhVienID: Long?,
        val nguoiVanChuyenID: Long?,
        val hinhThucThanhToanID: Int?,
        val tkNganHangID: Any?,
        val nguoiMuonID: Long?,
        val sessionDonDAT: String?,
        val tenNganHang: String?,
        val tenKhachHang: String?,
        val tenKinhDoanh: String?,
        val nguoiVanChuyen: String?,
        val tenNguoiMuon: String?,
        val tenTrangThai: String?,
        val ddDonDat: DDDonDAT?,
        val giaTriDonHang: Int,
        val congNoDonHang: Int,
        var tongTienDaThu: Int
) {


    var isCheck: Boolean = false
    fun getIDThanhVien(): Int {
        return ddDonDat?.iDThanhVien?.toInt() ?: 0
    }

    fun getThanhToanId(): Int {
        return hinhThucThanhToanID ?: 0
    }

    fun orderCode(): String {
        return maDonDat?.let {
            val date = (ddDonDat?.ngayKetThuc
                    ?: ddDonDat?.ngayBatDau)?.convertDate(from = DateType.FUll3, to = DateType.ONLY_DATE)
            "ĐH số : $it - ${date ?: ""}"
        } ?: ""
    }

    fun getTenThanhVien(): String {
        return ddDonDat?.tenThanhVien ?: ""
    }

    fun getDiaChiNhanHang(): String {
        return ddDonDat?.vanchuyens?.firstOrNull()?.diaChiNhanHang ?: ""
    }

    fun getTenNguoiChuyen(): String {
        return ddDonDat?.vanchuyens?.firstOrNull()?.tenNguoiChuyen ?: ""
    }

    private var contentDetail: String? = null
    fun getContentDetail(): String? {
        if (contentDetail == null) {
            contentDetail = ""
            val hangHoas = ddDonDat?.hanghoas ?: mutableListOf()
            contentDetail += hangHoas.joinToString("\n") { acc ->
                "${acc.hanghoa?.tenHangHoa} ( ${Utils.formatValue((acc.soLuong ?: 0f).toDouble(), Enum.FieldValueType.NORMAL)}${acc.donVi} )"
            }
        }
        return contentDetail
    }

    fun getTongGiaTriDonHang(): String {
        return Utils.formatValue(giaTriDonHang.toDouble(), Enum.FieldValueType.CURRENCY)
    }

    fun getCongNoDonHangString(): String {
        return Utils.formatValue(congNoDonHang.toDouble(), Enum.FieldValueType.CURRENCY)
    }

    fun getTongTienDaThuString(): String {
        return Utils.formatValue(tongTienDaThu.toDouble(), Enum.FieldValueType.CURRENCY)
    }

    fun getSoTienConLaiString(): String {
        return Utils.formatValue(getSoTienConLai().toDouble(), Enum.FieldValueType.CURRENCY)
    }

    fun getSoTienConLai(): Int {
        return congNoDonHang - tongTienDaThu
    }


    fun getTimeRemain(): Int {
        return (ddDonDat?.vanchuyens?.firstOrNull()?.timeRemain ?: "0").toInt()
    }
}


data class DDDonDAT(
        val dDDonDATID: Long,
        val sessionID: String,
        val maDonDAT: String,
        val tenDonDAT: String,
        val ngayBatDau: String,
        val ngayHoaDon: String,
        val ngayKetThuc: String,
        val dDTrangThaiID: Long,
        val iDThanhVien: Long,
        val dDThanhToanID: Long,
        val dDKhoHangID: Long,
        val tongGiaTri: Long,
        val tongCV: Long,
        val tongCoin: Long,
        val tongThanhToan: Long,   //đã thanh toán
        val loaiDonDATID: Long,
        val maBarCode: String,
        val nguoiTAOID: Long,
        val dDDonDATChaID: Any? = null,
        val soHoaDon: String,
        val ghiChu: String,
        val doiTuongID: Long,
        val nhanVienMUAID: Any? = null,
        val nguoiTiepNhanID: Any? = null,
        val isNo: Boolean,
        val kinhDoanhID: Long,
        val tenKinhDoanh: Any? = null,
        val daThu: Long,
        val isPrint: Long,
        val isMisa: Boolean,
        val duLieuThanhVien: Any? = null,
        val lyDoHuy: String,
        val phaiThanhToan: Long,
        val trangThaiText: Any? = null,
        val maKhoHang: String?,
        val tenKhoHang: String?,
        val tenThanhVien: String?,
        val guidThanhVien: Any? = null,
        val tenNhanVien: Any? = null,
        val nguoiThucHien: Any? = null,
        val diaChiNguoiMUA: Any? = null,
        val phuongThucThanhToan: String,
        val isDuyetCapCao: Boolean,
        val tenDuyetCapCao: String,
        val tenXacNhanDuyet: String,
        val isXacNhanDuyet: Boolean,
        val hanghoas: List<Hanghoa>,
        val nhatkys: Any? = null,
        val vanchuyens: List<Vanchuyen>?,
        val hanghoaxuats: Any? = null,
        val hanghoatras: Any? = null,
        val chiPhis: Any? = null,
        val isVanChuyen: Boolean,
        val tongNo: Long,
        val tongCVCK: Long,
        val tongTienCK: Long,
        val thanhtoans: Any? = null,
        val diachigiaohang: Any? = null,
        val loaiDonDATText: String,
        val loaidondat: Long,
        val trangthai: Long,
        val doituong: Long,
        val isEdit: Boolean,
        val ngayThucHien: Any? = null,
        val thuocTinhThanhVien: ThuocTinhThanhVien,
        val tienThanhToanThua: Long,
        val cVTruocQuyDoi: Long,
        val boPhanID: Long,
        val totalrow: Long
)

data class ThuocTinhThanhVien(
        val nguoiDATDon: Any? = null,
        val thanhToanCongNoID: Long,
        val thanhToanCongNo: Any? = null
)
//
//class CongNoModel {
//    val IDThanhVien: Int? = null
//    val sessionID: String? = null
//    val DDDonDatID: Int? = null
//    var maDonDat: String? = null
//    var ngayBatDau: String? = null
//    var ngayKetThuc: String? = null
//    var tenThanhVien: String? = null
//    var vanchuyens: MutableList<VanChuyenModel>? = null
//    var hanghoas: MutableList<HangHoaModel>? = null
//    var tongGiaTri: Int? = 0
//
//    @SerializedName("tongThanhToan")
//    var daThanhToan: Int? = 0
//    var phaiThanhToan: Int? = 0
//
//
//    var selected = false
//    private var contentDetail: String? = null
//    fun getContentDetail(): String? {
//        if (contentDetail == null) {
//            contentDetail += (tenThanhVien ?: "")
//
//            val vanchuyenModel = vanchuyens?.firstOrNull()
//            vanchuyenModel?.let {
//                contentDetail += " ${it.diaChiNhanHang ?: ""}"
//            }
//
//            val hangHoas = hanghoas ?: mutableListOf()
//            contentDetail += hangHoas.joinToString("\n") { acc ->
//                "${acc.hanghoa?.tenHangHoa} ( ${Utils.formatValue((acc.soLuong ?: 0f).toDouble(), Enum.FieldValueType.NORMAL)}${acc.donVi} )"
//            }
//        }
//        return contentDetail
//    }
//}


class AllCongNoResponse : APIResponse<MutableList<OrderTran>>()
class CongNoResponse : APIResponse<MutableList<CongNoModel>>()
class CongNoParams {
    companion object {
        const val TypeChoDuyet = 2
        const val TypeDaMuon = 3
        const val TypeChoTraVaThanhToan = 4
    }

    var KinhDoanhID: Int? = null
    var isGetHangHoa: Boolean = true
    var Page: Int = 1
    var Size: Int = Constants.LIMIT
    var TrangThaiID: Int = 0
    var keySearch: String? = ""

    constructor(KinhDoanhID: Int, Page: Int, TrangThaiID: Int = 0) {
        this.KinhDoanhID = KinhDoanhID
        isGetHangHoa = true
        this.Page = Page
        Size = Constants.LIMIT
        this.TrangThaiID = TrangThaiID
    }
}