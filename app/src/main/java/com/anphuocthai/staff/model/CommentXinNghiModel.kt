package com.anphuocthai.staff.model

import com.anphuocthai.staff.utils.DateType
import com.anphuocthai.staff.utils.convertDate

/**
 * Created by OpenYourEyes on 3/15/21
 */

data class CommentXinNghiModel(
        val tenNguoiNhan: String?,
        val comment: String?,
        val tenNguoiGui: String?,//Quản trị hệ thống
        val createDate: String?,
        var isMeComment: Boolean?,
        var time: String?
) {
    companion object {
        fun toTempObject(date: String?): CommentXinNghiModel {
            return CommentXinNghiModel(null, null, null, date, null, null)
        }
    }

    fun toTime() {
        time = createDate?.convertDate(DateType.FUll3, DateType.ONLY_TIME)
    }
}

data class CommentXinNghiBody(val nguoiNhanId: Int,
                              val phieuNghiId: Int,
                              val nguoiGui: Int,
                              val comment: String?)

