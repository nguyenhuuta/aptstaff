package com.anphuocthai.staff.model

import com.anphuocthai.staff.baserequest.APIResponse
import com.anphuocthai.staff.data.TypeProduct

data class ChietKhauModel(
        val dM_ChietKhauID: Int,
        val maChietKhau: String,
        val tenChietKhau: String,
        val isXoa: Boolean,
        val dateCreated: String,
        val dateUpdated: String,
        val userIDUpdate: Int,
        val userIDCreated: Int,
        val userID: Int,
        val page: Int,
        val size: Int,
        val isCV: Boolean,
        val isTien: Boolean,
        val totalrow: Int,
        val cvChietKhau: Int,
        val isThayDoi: Boolean
)

class ChietKhauResponse : APIResponse<MutableList<ChietKhauModel>>()

class DanhMucDungThuResponse : APIResponse<MutableList<TypeProduct>>()
