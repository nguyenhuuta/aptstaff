package com.anphuocthai.staff.model;

public class                Login {
    String code;
    String message;
    String idRecord;
    LoginObject objectInfo;

    public Login(String code, String message, String idRecord, LoginObject objectInfo) {
        this.code = code;
        this.message = message;
        this.idRecord = idRecord;
        this.objectInfo = objectInfo;
    }
    public Login(){

    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(String idRecord) {
        this.idRecord = idRecord;
    }

    public LoginObject getObjectInfo() {
        return objectInfo;
    }

    public void setObjectInfo(LoginObject objectInfo) {
        this.objectInfo = objectInfo;
    }
}
