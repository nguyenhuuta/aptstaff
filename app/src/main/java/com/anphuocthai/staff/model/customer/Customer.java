package com.anphuocthai.staff.model.customer;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.anphuocthai.staff.ui.customer.model.CustomerData;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

@Entity(tableName = "currentsearchcustomer")
public class Customer implements Serializable {
    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "customer_id")
    @SerializedName("iD_ThanhVien")
    private int idMember;

    @ColumnInfo(name = "user_name")
    String tenDangNhap;

    @ColumnInfo(name = "password")
    String matKhau;

    @ColumnInfo(name = "full_name")
    String tenDayDu;

    @ColumnInfo(name = "email")
    String email;

    @ColumnInfo(name = "state")
    int trangThai;

    @ColumnInfo(name = "identifi")
    String cmt;

    @ColumnInfo(name = "avatar_id")
    String anhDaiDienID;

    @ColumnInfo(name = "sex")
    int gioiTinh;

    @ColumnInfo(name = "day_resgister")
    String ngayDangKy;

    @ColumnInfo(name = "member_code")
    String maThanhVien;

    @ColumnInfo(name = "approval_day")
    String ngayDuyet;

    @ColumnInfo(name = "key")
    boolean khoa;

    @ColumnInfo(name = "id_member_introduce")
    @SerializedName("iD_ThanhVienGioiThieu")
    int idMemberIntroduce;

    @ColumnInfo(name = "object_id")
    int doiTuongId;

    @ColumnInfo(name = "object")
    int doituong;

    @ColumnInfo(name = "object_text")
    String doiTuongText;

    @ColumnInfo(name = "register_pass")
    String matKhauDangKy;

    @ColumnInfo(name = "phone")
    String dienThoai;

    @ColumnInfo(name = "date_of_birth")
    String ngaySinh;

    @ColumnInfo(name = "address")
    String diaChi;

    @ColumnInfo(name = "guid")
    String guid;

    @ColumnInfo(name = "user_guid")
    String userGuid;

    @ColumnInfo(name = "introduce_code")
    String maGioiThieu;

    @ColumnInfo(name = "debt_value")
    Integer congNo;

    @ColumnInfo(name = "previous_note")
    String ghiChuDonHang;

    @Embedded
    ArrayList<CustomerAddress> customerAddresses;

    @Embedded
    @SerializedName("duLieuModel")
    CustomerData customerData;


    @Ignore
    public Customer() {

    }

    public Customer(@NonNull int idMember, String tenDangNhap, String matKhau, String tenDayDu, String email, int trangThai, String cmt, String anhDaiDienID, int gioiTinh, String ngayDangKy, String maThanhVien, String ngayDuyet, boolean khoa, int idMemberIntroduce, int doiTuongId, int doituong, String doiTuongText, String matKhauDangKy, String dienThoai, String ngaySinh, String diaChi, String guid, String userGuid, String maGioiThieu, ArrayList<CustomerAddress> customerAddresses) {
        this.idMember = idMember;
        this.tenDangNhap = tenDangNhap;
        this.matKhau = matKhau;
        this.tenDayDu = tenDayDu;
        this.email = email;
        this.trangThai = trangThai;
        this.cmt = cmt;
        this.anhDaiDienID = anhDaiDienID;
        this.gioiTinh = gioiTinh;
        this.ngayDangKy = ngayDangKy;
        this.maThanhVien = maThanhVien;
        this.ngayDuyet = ngayDuyet;
        this.khoa = khoa;
        this.idMemberIntroduce = idMemberIntroduce;
        this.doiTuongId = doiTuongId;
        this.doituong = doituong;
        this.doiTuongText = doiTuongText;
        this.matKhauDangKy = matKhauDangKy;
        this.dienThoai = dienThoai;
        this.ngaySinh = ngaySinh;
        this.diaChi = diaChi;
        this.guid = guid;
        this.userGuid = userGuid;
        this.maGioiThieu = maGioiThieu;
        this.customerAddresses = customerAddresses;
    }

    public ArrayList<CustomerAddress> getCustomerAddresses() {
        return customerAddresses;
    }

    public void setCustomerAddresses(ArrayList<CustomerAddress> customerAddresses) {
        this.customerAddresses = customerAddresses;
    }

    @NonNull
    public int getIdMember() {
        return idMember;
    }

    public void setIdMember(@NonNull int idMember) {
        this.idMember = idMember;
    }

    public String getTenDangNhap() {
        return tenDangNhap;
    }

    public void setTenDangNhap(String tenDangNhap) {
        this.tenDangNhap = tenDangNhap;
    }

    public String getMatKhau() {
        return matKhau;
    }

    public void setMatKhau(String matKhau) {
        this.matKhau = matKhau;
    }

    public String getTenDayDu() {
        return tenDayDu;
    }

    public void setTenDayDu(String tenDayDu) {
        this.tenDayDu = tenDayDu;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(int trangThai) {
        this.trangThai = trangThai;
    }

    public String getCmt() {
        return cmt;
    }

    public void setCmt(String cmt) {
        this.cmt = cmt;
    }

    public String getAnhDaiDienID() {
        return anhDaiDienID;
    }

    public void setAnhDaiDienID(String anhDaiDienID) {
        this.anhDaiDienID = anhDaiDienID;
    }

    public int getGioiTinh() {
        return gioiTinh;
    }

    public void setGioiTinh(int gioiTinh) {
        this.gioiTinh = gioiTinh;
    }

    public String getNgayDangKy() {
        return ngayDangKy;
    }

    public void setNgayDangKy(String ngayDangKy) {
        this.ngayDangKy = ngayDangKy;
    }

    public String getMaThanhVien() {
        return maThanhVien;
    }

    public void setMaThanhVien(String maThanhVien) {
        this.maThanhVien = maThanhVien;
    }

    public String getNgayDuyet() {
        return ngayDuyet;
    }

    public void setNgayDuyet(String ngayDuyet) {
        this.ngayDuyet = ngayDuyet;
    }

    public boolean isKhoa() {
        return khoa;
    }

    public void setKhoa(boolean khoa) {
        this.khoa = khoa;
    }

    public int getIdMemberIntroduce() {
        return idMemberIntroduce;
    }

    public void setIdMemberIntroduce(int idMemberIntroduce) {
        this.idMemberIntroduce = idMemberIntroduce;
    }

    public int getDoiTuongId() {
        return doiTuongId;
    }

    public void setDoiTuongId(int doiTuongId) {
        this.doiTuongId = doiTuongId;
    }

    public int getDoituong() {
        return doituong;
    }

    public void setDoituong(int doituong) {
        this.doituong = doituong;
    }

    public String getDoiTuongText() {
        return doiTuongText;
    }

    public void setDoiTuongText(String doiTuongText) {
        this.doiTuongText = doiTuongText;
    }

    public String getMatKhauDangKy() {
        return matKhauDangKy;
    }

    public void setMatKhauDangKy(String matKhauDangKy) {
        this.matKhauDangKy = matKhauDangKy;
    }

    public String getDienThoai() {
        return dienThoai;
    }

    public void setDienThoai(String dienThoai) {
        this.dienThoai = dienThoai;
    }

    public String getNgaySinh() {
        return ngaySinh;
    }

    public void setNgaySinh(String ngaySinh) {
        this.ngaySinh = ngaySinh;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getUserGuid() {
        return userGuid;
    }

    public void setUserGuid(String userGuid) {
        this.userGuid = userGuid;
    }

    public String getMaGioiThieu() {
        return maGioiThieu;
    }

    public void setMaGioiThieu(String maGioiThieu) {
        this.maGioiThieu = maGioiThieu;
    }

    public CustomerData getCustomerData() {
        return customerData;
    }

    public void setCustomerData(CustomerData customerData) {
        this.customerData = customerData;
    }

    public Integer getCongNo() {
        return congNo;
    }

    public void setCongNo(Integer congNo) {
        this.congNo = congNo;
    }

    public String getGhiChuDonHang() {
        return ghiChuDonHang;
    }

    public void setGhiChuDonHang(String ghiChuDonHang) {
        this.ghiChuDonHang = ghiChuDonHang;
    }

    //    public Customer(Parcel source) {
//
//    }
//
//    public static final Creator<Customer> CREATOR = new Creator<Customer>() {
//
//        @Override
//        public Customer createFromParcel(Parcel parcel) {
//            return new Customer(parcel);
//        }
//
//        @Override
//        public Customer[] newArray(int i) {
//            return new Customer[i];
//        }
//    };
//
//    @Override
//    public String getBody() {
//        return getTenDayDu();
//    }
//
//    @Override
//    public int describeContents() {
//        return 0;
//    }
//
//    @Override
//    public void writeToParcel(Parcel parcel, int i) {
//
//    }
}
