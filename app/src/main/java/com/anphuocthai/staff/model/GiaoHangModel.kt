package com.anphuocthai.staff.model

import com.anphuocthai.staff.ui.delivery.model.OrderTran

/**
 * Created by OpenYourEyes on 11/20/2020
 */
data class GiaoHangParam(
        val DD_DonDatID: Int,
//        val IsBanGiaoChungTu: Int = 0,
        val TienVanChuyenThu: Float,
        val GhiChuGiaoHang: String,
        val DD_TrangThaiID: Int,
        val UserID: Int
)


class GiaoHangResponse : Response<MutableList<OrderTran>>()