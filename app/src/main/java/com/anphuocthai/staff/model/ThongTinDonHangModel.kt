package com.anphuocthai.staff.model

/**
 * Created by OpenYourEyes on 11/23/2020
 */
data class ThongTinDonHangModel(
        val tenCacMatHang: String,
        val tongSoLuong: Int,
        val tongCV: Int
)