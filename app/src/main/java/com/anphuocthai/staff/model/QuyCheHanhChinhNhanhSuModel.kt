package com.anphuocthai.staff.model

import com.anphuocthai.staff.baserequest.APIResponse

/**
 * Created by OpenYourEyes on 10/14/2020
 */
data class QuyCheHanhChinhNhanhSuModel(
        val id: Int?,
        val ten: String?,
        val url: String?,
        val boPhan: Int?,
        val tenBoPhan: String?,
        val noiDung: String?,
        val file: String?
) {
    fun getLink(): String {
        return "https://drive.google.com/viewerng/viewer?embedded=true&url=http://res.anphuocthai.vn/images/Employee/$file"
    }
}

class QuyCheHanhChinhNhanhSuResponse : APIResponse<MutableList<QuyCheHanhChinhNhanhSuModel>>()