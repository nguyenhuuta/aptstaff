package com.anphuocthai.staff.model

/**
 * Created by OpenYourEyes on 3/1/21
 */
data class LoaiNghiPhepModel(
        val id: Int?,
        val ten: String?,
        val ma: String?,
        val isXoa: Int?
) {
    override fun toString(): String {
        return ten ?: ""
    }
}

data class YeuCauNghiBody(
        val userId: Int,
        val lyDo: String,
        val phieuDangKyNghi_TheoNgay: MutableList<ThoiGianNghiModel>
)

data class ThoiGianNghiModel(
        val thoiGian: String,
        val thoiGianBatDau: String,
        val thoiGianKetThuc: String,
        val loaiNghiId: Int,
        val soGio: Float
) {
    companion object {
        /**
         * date: Ngày nghỉ phép
         * hour: Số giờ nghỉ
         * typeTimeId: Kiểu nghỉ: cả ngày(0), ca sáng(1) hoặc ca chiều(2)
         * cauHinhLamViecModel: Thời gian làm việc mỗi ca và tổng số giờ làm việc trong ngày
         */
        fun toTimeInDay(date: String, loaiNghiId: Int, hour: Float, typeTimeId: Int, cauHinhLamViecModel: CauHinhLamViecModel): ThoiGianNghiModel {
            val listHour = cauHinhLamViecModel.toHours()
            val startTime = when (typeTimeId) {
                0, 1 -> { // cả ngày hoặc đầu ca sáng
                    "$date ${listHour[0]}"
                }
                2 -> { // đầu ca chiều
                    "$date ${listHour[2]}"
                }
                else -> ""
            }
            val endTime = when (typeTimeId) {
                0, 2 -> {  // cả ngày hoặc cuối ca chiều
                    "$date ${listHour[3]}"
                }
                1 -> { // cuối ca sáng
                    "$date ${listHour[1]}"
                }
                else -> ""

            }
            return ThoiGianNghiModel(date, startTime, endTime, loaiNghiId, hour)
        }
    }

}