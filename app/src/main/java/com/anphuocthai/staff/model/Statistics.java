package com.anphuocthai.staff.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Statistics implements Serializable {

    @SerializedName("ma")
    @Expose
    private String ma;
    @SerializedName("moTa")
    @Expose
    private String moTa;
    @SerializedName("giaTri")
    @Expose
    private Long giaTri;
    @SerializedName("giaTriF")
    @Expose
    private Long giaTriF;

    public String getMa() {
        return ma;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public Long getGiaTri() {
        return giaTri;
    }

    public void setGiaTri(Long giaTri) {
        this.giaTri = giaTri;
    }

    public Long getGiaTriF() {
        return giaTriF;
    }

    public void setGiaTriF(Long giaTriF) {
        this.giaTriF = giaTriF;
    }


    public boolean containId(String id) {
        return this.ma.equals(id);
    }
}


