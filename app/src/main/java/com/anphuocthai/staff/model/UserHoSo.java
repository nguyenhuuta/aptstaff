package com.anphuocthai.staff.model;

import com.google.gson.annotations.SerializedName;

public class UserHoSo {
    int userID;
    String ma;
    String ngaySinh;
    String cmt;
    String ngayCap;
    String noiCap;
    @SerializedName("CustomName")
    int dM_TinhID;
    String nguyenQuan;
    String diaChi;
    String ngayTao;
    String nguoiTao;
    String maSoThue;
    String trangThaiID;
    String trangThaiText;
    String ten_Tinh_ThanhPho;
    int gioiTinhID;
    String gioiTinhText;
    int anhDaiDienID;

    public UserHoSo(){

    }

    public UserHoSo(int userID, String ma, String ngaySinh, String cmt, String ngayCap, String noiCap, int dM_TinhID, String nguyenQuan, String diaChi, String ngayTao, String nguoiTao, String maSoThue, String trangThaiID, String trangThaiText, String ten_Tinh_ThanhPho, int gioiTinhID, String gioiTinhText, int anhDaiDienID) {
        this.userID = userID;
        this.ma = ma;
        this.ngaySinh = ngaySinh;
        this.cmt = cmt;
        this.ngayCap = ngayCap;
        this.noiCap = noiCap;
        this.dM_TinhID = dM_TinhID;
        this.nguyenQuan = nguyenQuan;
        this.diaChi = diaChi;
        this.ngayTao = ngayTao;
        this.nguoiTao = nguoiTao;
        this.maSoThue = maSoThue;
        this.trangThaiID = trangThaiID;
        this.trangThaiText = trangThaiText;
        this.ten_Tinh_ThanhPho = ten_Tinh_ThanhPho;
        this.gioiTinhID = gioiTinhID;
        this.gioiTinhText = gioiTinhText;
        this.anhDaiDienID = anhDaiDienID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getMa() {
        return ma;
    }

    public void setMa(String ma) {
        this.ma = ma;
    }

    public String getNgaySinh() {
        return ngaySinh;
    }

    public void setNgaySinh(String ngaySinh) {
        this.ngaySinh = ngaySinh;
    }

    public String getCmt() {
        return cmt;
    }

    public void setCmt(String cmt) {
        this.cmt = cmt;
    }

    public String getNgayCap() {
        return ngayCap;
    }

    public void setNgayCap(String ngayCap) {
        this.ngayCap = ngayCap;
    }

    public String getNoiCap() {
        return noiCap;
    }

    public void setNoiCap(String noiCap) {
        this.noiCap = noiCap;
    }

    public int getdM_TinhID() {
        return dM_TinhID;
    }

    public void setdM_TinhID(int dM_TinhID) {
        this.dM_TinhID = dM_TinhID;
    }

    public String getNguyenQuan() {
        return nguyenQuan;
    }

    public void setNguyenQuan(String nguyenQuan) {
        this.nguyenQuan = nguyenQuan;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public String getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(String ngayTao) {
        this.ngayTao = ngayTao;
    }

    public String getNguoiTao() {
        return nguoiTao;
    }

    public void setNguoiTao(String nguoiTao) {
        this.nguoiTao = nguoiTao;
    }

    public String getMaSoThue() {
        return maSoThue;
    }

    public void setMaSoThue(String maSoThue) {
        this.maSoThue = maSoThue;
    }

    public String getTrangThaiID() {
        return trangThaiID;
    }

    public void setTrangThaiID(String trangThaiID) {
        this.trangThaiID = trangThaiID;
    }

    public String getTrangThaiText() {
        return trangThaiText;
    }

    public void setTrangThaiText(String trangThaiText) {
        this.trangThaiText = trangThaiText;
    }

    public String getTen_Tinh_ThanhPho() {
        return ten_Tinh_ThanhPho;
    }

    public void setTen_Tinh_ThanhPho(String ten_Tinh_ThanhPho) {
        this.ten_Tinh_ThanhPho = ten_Tinh_ThanhPho;
    }

    public int getGioiTinhID() {
        return gioiTinhID;
    }

    public void setGioiTinhID(int gioiTinhID) {
        this.gioiTinhID = gioiTinhID;
    }

    public String getGioiTinhText() {
        return gioiTinhText;
    }

    public void setGioiTinhText(String gioiTinhText) {
        this.gioiTinhText = gioiTinhText;
    }

    public int getAnhDaiDienID() {
        return anhDaiDienID;
    }

    public void setAnhDaiDienID(int anhDaiDienID) {
        this.anhDaiDienID = anhDaiDienID;
    }
}
