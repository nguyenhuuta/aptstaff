package com.anphuocthai.staff.model.order

import kotlin.math.max

/**
 * Created by OpenYourEyes on 1/4/2021
 */
class ProductSaleModel : Product() {
    var giaGoc: Int? = 0
    var giaKM: Int? = 0

    fun isPriceUp() = (giaGoc ?: 0) < (giaKM ?: 0)
}

class ProductSaleResponse {
    private var listUp: MutableList<ProductSaleModel>? = null
    private var listDown: MutableList<ProductSaleModel>? = null
    var list: MutableList<ProductSaleModel> = mutableListOf()
    fun mergerList() {
        val sizeU = listUp?.size ?: 0
        val sizeD = listDown?.size ?: 0

        val size = max(sizeU, sizeD)
        var count = 0
        while (count < size) {
            val up = listUp?.getOrNull(count)
            val down = listDown?.getOrNull(count)

            if (up != null) {
                list.add(up)
            }
            if (down != null) {
                list.add(down)
            }
            count++
        }
    }

}

