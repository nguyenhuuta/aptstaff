package com.anphuocthai.staff.model.customer;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

//    {
//        "code": "01",
//            "message": "Có lỗi đăng ký thành viên mới",
//            "idRecord": null,
//            "objectInfo": null
//    }


public class ResponseListCustomer {

    String code;
    String message;
    String idRecord;

    @SerializedName("objectInfo")
    ArrayList<SearchCustomer> customers;

    public ResponseListCustomer() {
    }

    public ResponseListCustomer(String code, String message, String idRecord, ArrayList<SearchCustomer> customers) {
        this.code = code;
        this.message = message;
        this.idRecord = idRecord;
        this.customers = customers;
    }

    public ArrayList<SearchCustomer> getCustomers() {
        return customers;
    }

    public void setCustomers(ArrayList<SearchCustomer> customers) {
        this.customers = customers;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(String idRecord) {
        this.idRecord = idRecord;
    }
}
