package com.anphuocthai.staff.model.customer;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.anphuocthai.staff.model.DienThoaiModel;
import com.anphuocthai.staff.model.PhoneModel;
import com.anphuocthai.staff.ui.customer.model.CustomerData;
import com.anphuocthai.staff.utils.ColorUtils;
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class SearchCustomer implements SearchSuggestion, Parcelable {

    private int iD_ThanhVien;
    private String tenDangNhap;

    private String matKhau;

    private String tenDayDu;

    private String email;

    private int trangThai;

    private String cmt;

    private String anhDaiDienID;

    private int gioiTinh;

    private String ngayDangKy;

    private String maThanhVien;

    private String ngayDuyet;

    private boolean khoa;

    private int iD_ThanhVienGioiThieu;

    private int doiTuongId;

    private int doituong;

    private String doiTuongText;

    private String matKhauDangKy;

//    private String dienThoai;

    private List<DienThoaiModel> dienthoais;

    private String ngaySinh;

    private String diaChi;

    private String guid;

    private String userGuid;

    private String maGioiThieu;

    private Integer congNo;

    private ArrayList<CustomerAddress> customerAddresses;

    @SerializedName("duLieuModel")
    private CustomerData customerData;

    private int backgroundColor;
    private int phanLoaiKHTiemNang;

    private boolean isSelected = false;

    private boolean isHistory = false;

    public Customer toCustomer() {
        Customer customer = new Customer();

        customer.setIdMember(this.iD_ThanhVien);


        customer.setTenDangNhap(this.tenDangNhap);


        customer.setMatKhau(this.matKhau);


        customer.setTenDayDu(this.tenDayDu);


        customer.setEmail(this.email);


        customer.setTrangThai(this.trangThai);


        customer.setCmt(this.cmt);


        customer.setAnhDaiDienID(this.anhDaiDienID);


        customer.setGioiTinh(this.gioiTinh);


        customer.setNgayDangKy(this.ngayDangKy);


        customer.setMaThanhVien(this.maThanhVien);


        customer.setNgayDuyet(this.ngayDuyet);


        customer.setKhoa(this.khoa);


        customer.setIdMemberIntroduce(this.iD_ThanhVienGioiThieu);


        customer.setDoiTuongId(this.doiTuongId);


        customer.setDoituong(this.doituong);


        customer.setDoiTuongText(this.doiTuongText);


        customer.setMatKhauDangKy(this.matKhauDangKy);


        customer.setNgaySinh(this.ngaySinh);


        customer.setDiaChi(this.diaChi);


        customer.setGuid(this.guid);


        customer.setUserGuid(this.userGuid);


        customer.setMaGioiThieu(this.maGioiThieu);

        customer.setCustomerAddresses(this.customerAddresses);

        customer.setCustomerData(this.customerData);

        customer.setCongNo(this.congNo);

        return customer;
    }

    public int getPhanLoaiKHTiemNang() {
        return phanLoaiKHTiemNang;
    }

    public int getiD_ThanhVien() {
        return iD_ThanhVien;
    }

    public void setiD_ThanhVien(int iD_ThanhVien) {
        this.iD_ThanhVien = iD_ThanhVien;
    }

    public String getTenDangNhap() {
        return tenDangNhap;
    }

    public void setTenDangNhap(String tenDangNhap) {
        this.tenDangNhap = tenDangNhap;
    }

    public String getMatKhau() {
        return matKhau;
    }

    public void setMatKhau(String matKhau) {
        this.matKhau = matKhau;
    }

    public String getTenDayDu() {
        return tenDayDu;
    }

    public void setTenDayDu(String tenDayDu) {
        this.tenDayDu = tenDayDu;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(int trangThai) {
        this.trangThai = trangThai;
    }

    public String getCmt() {
        return cmt;
    }

    public void setCmt(String cmt) {
        this.cmt = cmt;
    }

    public String getAnhDaiDienID() {
        return anhDaiDienID;
    }

    public void setAnhDaiDienID(String anhDaiDienID) {
        this.anhDaiDienID = anhDaiDienID;
    }

    public int getGioiTinh() {
        return gioiTinh;
    }

    public void setGioiTinh(int gioiTinh) {
        this.gioiTinh = gioiTinh;
    }

    public String getNgayDangKy() {
        return ngayDangKy;
    }

    public void setNgayDangKy(String ngayDangKy) {
        this.ngayDangKy = ngayDangKy;
    }

    public String getMaThanhVien() {
        return maThanhVien;
    }

    public void setMaThanhVien(String maThanhVien) {
        this.maThanhVien = maThanhVien;
    }

    public String getNgayDuyet() {
        return ngayDuyet;
    }

    public void setNgayDuyet(String ngayDuyet) {
        this.ngayDuyet = ngayDuyet;
    }

    public boolean isKhoa() {
        return khoa;
    }

    public void setKhoa(boolean khoa) {
        this.khoa = khoa;
    }

    public int getiD_ThanhVienGioiThieu() {
        return iD_ThanhVienGioiThieu;
    }

    public void setiD_ThanhVienGioiThieu(int iD_ThanhVienGioiThieu) {
        this.iD_ThanhVienGioiThieu = iD_ThanhVienGioiThieu;
    }

    public int getDoiTuongId() {
        return doiTuongId;
    }

    public void setDoiTuongId(int doiTuongId) {
        this.doiTuongId = doiTuongId;
    }

    public int getDoituong() {
        return doituong;
    }

    public void setDoituong(int doituong) {
        this.doituong = doituong;
    }

    public String getDoiTuongText() {
        return doiTuongText;
    }

    public void setDoiTuongText(String doiTuongText) {
        this.doiTuongText = doiTuongText;
    }

    public String getMatKhauDangKy() {
        return matKhauDangKy;
    }

    public void setMatKhauDangKy(String matKhauDangKy) {
        this.matKhauDangKy = matKhauDangKy;
    }

    String phone;

    public String getPhone() {
        if (TextUtils.isEmpty(phone) && dienthoais != null && dienthoais.size() > 0) {
            DienThoaiModel dienThoaiModel = dienthoais.get(0);
            phone = dienThoaiModel.getDanhMucTen_IsChinh() + ": " + dienThoaiModel.getSoDienThoai();
        }
        return phone;
    }


    public List<DienThoaiModel> getDienthoais() {
        return dienthoais;
    }

    public List<PhoneModel> toPhoneModel() {
        List<PhoneModel> list = new ArrayList<>();
        for (DienThoaiModel dienThoaiModel : dienthoais) {
            list.add(dienThoaiModel.toPhoneModel());
        }
        return list;
    }

    public String getFirstPhone() {
        for (DienThoaiModel model : dienthoais) {
            return model.getFullPhone();
        }
        return "";
    }


    public String getNgaySinh() {
        return ngaySinh;
    }

    public void setNgaySinh(String ngaySinh) {
        this.ngaySinh = ngaySinh;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getUserGuid() {
        return userGuid;
    }

    public void setUserGuid(String userGuid) {
        this.userGuid = userGuid;
    }

    public String getMaGioiThieu() {
        return maGioiThieu;
    }

    public void setMaGioiThieu(String maGioiThieu) {
        this.maGioiThieu = maGioiThieu;
    }

    public int getBackgroundColor() {
        if (backgroundColor == 0) {
            backgroundColor = ColorUtils.getRandomMaterialColor();
        }
        return backgroundColor;
    }

    public void setBackgroundColor(int backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public boolean getSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }


    public boolean isHistory() {
        return isHistory;
    }

    public void setHistory(boolean history) {
        isHistory = history;
    }

    public Integer getCongNo() {
        return congNo;
    }

    public void setCongNo(Integer congNo) {
        this.congNo = congNo;
    }

    private boolean isLoadMore;

    public boolean isLoadMore() {
        return isLoadMore;
    }

    public void setLoadMore(boolean loadMore) {
        isLoadMore = loadMore;
    }

    @Override
    public String getBody() {
        return getTenDayDu();
    }

    @Override
    public int describeContents() {
        return 0;
    }


    protected SearchCustomer(Parcel in) {
        iD_ThanhVien = in.readInt();
        tenDangNhap = in.readString();
        matKhau = in.readString();
        tenDayDu = in.readString();
        email = in.readString();
        trangThai = in.readInt();
        cmt = in.readString();
        anhDaiDienID = in.readString();
        gioiTinh = in.readInt();
        ngayDangKy = in.readString();
        maThanhVien = in.readString();
        ngayDuyet = in.readString();
        khoa = in.readByte() != 0;
        iD_ThanhVienGioiThieu = in.readInt();
        doiTuongId = in.readInt();
        doituong = in.readInt();
        doiTuongText = in.readString();
        matKhauDangKy = in.readString();
        dienthoais = new ArrayList<>();
        in.readList(dienthoais, DienThoaiModel.class.getClassLoader());
        ngaySinh = in.readString();
        diaChi = in.readString();
        guid = in.readString();
        userGuid = in.readString();
        maGioiThieu = in.readString();
        if (in.readByte() == 0) {
            congNo = null;
        } else {
            congNo = in.readInt();
        }
        backgroundColor = in.readInt();
        isSelected = in.readByte() != 0;
        isHistory = in.readByte() != 0;
        isLoadMore = in.readByte() != 0;
    }

    public static final Creator<SearchCustomer> CREATOR = new Creator<SearchCustomer>() {
        @Override
        public SearchCustomer createFromParcel(Parcel in) {
            return new SearchCustomer(in);
        }

        @Override
        public SearchCustomer[] newArray(int size) {
            return new SearchCustomer[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(iD_ThanhVien);
        dest.writeString(tenDangNhap);
        dest.writeString(matKhau);
        dest.writeString(tenDayDu);
        dest.writeString(email);
        dest.writeInt(trangThai);
        dest.writeString(cmt);
        dest.writeString(anhDaiDienID);
        dest.writeInt(gioiTinh);
        dest.writeString(ngayDangKy);
        dest.writeString(maThanhVien);
        dest.writeString(ngayDuyet);
        dest.writeByte((byte) (khoa ? 1 : 0));
        dest.writeInt(iD_ThanhVienGioiThieu);
        dest.writeInt(doiTuongId);
        dest.writeInt(doituong);
        dest.writeString(doiTuongText);
        dest.writeString(matKhauDangKy);
        dest.writeList(dienthoais);
        dest.writeString(ngaySinh);
        dest.writeString(diaChi);
        dest.writeString(guid);
        dest.writeString(userGuid);
        dest.writeString(maGioiThieu);
        if (congNo == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(congNo);
        }
        dest.writeInt(backgroundColor);
        dest.writeByte((byte) (isSelected ? 1 : 0));
        dest.writeByte((byte) (isHistory ? 1 : 0));
        dest.writeByte((byte) (isLoadMore ? 1 : 0));
    }
}
