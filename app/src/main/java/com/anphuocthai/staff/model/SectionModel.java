package com.anphuocthai.staff.model;

import android.graphics.Bitmap;

import java.util.ArrayList;

/**
 * Created by sonu on 24/07/17.
 */

public class SectionModel {
    private String sectionLabel;
    private ArrayList<String> itemArrayList;
    private ArrayList<Bitmap> imageBitmapArrayList;

    public SectionModel(String sectionLabel, ArrayList<String> itemArrayList, ArrayList<Bitmap> imageBitmapArrayList) {
        this.sectionLabel = sectionLabel;
        this.itemArrayList = itemArrayList;
        this.imageBitmapArrayList = imageBitmapArrayList;
    }

    public String getSectionLabel() {
        return sectionLabel;
    }

    public ArrayList<String> getItemArrayList() {
        return itemArrayList;
    }

    public ArrayList<Bitmap> getImageBitmapArrayList() {
        return imageBitmapArrayList;
    }

    public void setImageBitmapArrayList(ArrayList<Bitmap> imageBitmapArrayList) {
        this.imageBitmapArrayList = imageBitmapArrayList;
    }
}
