//package com.anphuocthai.vn.model.order;
//
//import android.arch.persistence.room.ColumnInfo;
//
//import com.google.gson.annotations.SerializedName;
//
//import java.io.Serializable;
//
//// "id": 2,
////         "dD_HangHoaID": 1,
////         "ngayTao": "2018-07-15 12:51:18",
////         "nguoiTao": "Quản trị hệ thống(Admin)",
////         "ngayKetThuc": null,
////         "isInUse": true,
////         "dM_DonViID": 1,
////         "donVi": "Kg",
////         "giaNhap": 150000,
////         "kD_BanBuonGia": 180000,
////         "kD_BanBuonDieuKien": "=17",
////         "loaidieukien": 1,
////         "kD_BanBuonSoLuong1": 17,
////         "kD_BanBuonSoLuong2": 0,
////         "kD_BanBuonDieuKienMoTa": "Mua nguyên thùng 17Kg",
////         "kD_BanLeGia": 185000,
////         "kD_CV": 15,
////         "kH_BanLeGia": 255000,
////         "kH_CV": 44,
////         "kD_BanBuonLN": 30000,
////         "kD_BanBuonTiLe": 0.2,
////         "kD_BanLeLN": 35000,
////         "kD_BanLeTiLe": 0.23333333333333334,
////         "kH_BanLeLN": 105000,
////         "kH_BanLeTiLe": 0.7
//
//public class MainPrice implements Serializable {
//
//    @ColumnInfo(name = "main_price_id")
//    private int id;
//
//    @SerializedName("kD_BanBuonDieuKien")
//    @ColumnInfo(name = "whole_sale_condition")
//    private String wholeSaleCondition;
//
//    @SerializedName("loaidieukien")
//    @ColumnInfo(name = "type_condition")
//    private int typeCondition;
//
//    @SerializedName("kD_BanBuonDieuKienMoTa")
//    @ColumnInfo(name = "description_condition")
//    private String descriptionCondition;
//
//    @SerializedName("kD_BanBuonSoLuong1")
//    @ColumnInfo(name = "whole_sale_quantity_1")
//    private float wholeSaleQuantity1;
//
//    @SerializedName("kD_BanBuonSoLuong2")
//    @ColumnInfo(name = "whole_sale_quantity_2")
//    private float wholeSaleQuantity2;
//
//    @SerializedName("kD_BanBuonGia")
//    @ColumnInfo(name = "whole_sale_price")
//    private float wholeSalePrice;
//
//    @SerializedName("kD_BanLeGia")
//    @ColumnInfo(name = "retail_price")
//    private float retailPrice;
//
//
//    @SerializedName("kH_BanLeGia")
//    @ColumnInfo(name = "customer_retail_price")
//    private float customerRetailPrice;
//
//    @SerializedName("donVi")
//    @ColumnInfo(name = "unit_text")
//    private String unitText;
//
//    @SerializedName("dM_DonViID")
//    @ColumnInfo(name = "unit_id")
//    private int unitId;
//
//    @SerializedName("kD_CV")
//    @ColumnInfo(name = "bussiness_cv")
//    private int bussinessCV;
//
//    @SerializedName("kH_CV")
//    @ColumnInfo(name = "customer_cv")
//    private int customerCV;
//
//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }
//
//    public String getWholeSaleCondition() {
//        return wholeSaleCondition;
//    }
//
//    public void setWholeSaleCondition(String wholeSaleCondition) {
//        this.wholeSaleCondition = wholeSaleCondition;
//    }
//
//    public int getTypeCondition() {
//        return typeCondition;
//    }
//
//    public void setTypeCondition(int typeCondition) {
//        this.typeCondition = typeCondition;
//    }
//
//    public String getDescriptionCondition() {
//        return descriptionCondition;
//    }
//
//    public void setDescriptionCondition(String descriptionCondition) {
//        this.descriptionCondition = descriptionCondition;
//    }
//
//    public float getWholeSaleQuantity1() {
//        return wholeSaleQuantity1;
//    }
//
//    public void setWholeSaleQuantity1(float wholeSaleQuantity1) {
//        this.wholeSaleQuantity1 = wholeSaleQuantity1;
//    }
//
//    public float getWholeSaleQuantity2() {
//        return wholeSaleQuantity2;
//    }
//
//    public void setWholeSaleQuantity2(float wholeSaleQuantity2) {
//        this.wholeSaleQuantity2 = wholeSaleQuantity2;
//    }
//
//    public float getWholeSalePrice() {
//        return wholeSalePrice;
//    }
//
//    public void setWholeSalePrice(float wholeSalePrice) {
//        this.wholeSalePrice = wholeSalePrice;
//    }
//
//    public float getRetailPrice() {
//        return retailPrice;
//    }
//
//    public void setRetailPrice(float retailPrice) {
//        this.retailPrice = retailPrice;
//    }
//
//    public String getUnitText() {
//        return unitText;
//    }
//
//    public void setUnitText(String unitText) {
//        this.unitText = unitText;
//    }
//
//    public int getBussinessCV() {
//        return bussinessCV;
//    }
//
//    public void setBussinessCV(int bussinessCV) {
//        this.bussinessCV = bussinessCV;
//    }
//
//    public int getUnitId() {
//        return unitId;
//    }
//
//    public void setUnitId(int unitId) {
//        this.unitId = unitId;
//    }
//
//    public int getCustomerCV() {
//        return customerCV;
//    }
//
//    public void setCustomerCV(int customerCV) {
//        this.customerCV = customerCV;
//    }
//
//    public float getCustomerRetailPrice() {
//        return customerRetailPrice;
//    }
//
//    public void setCustomerRetailPrice(float customerRetailPrice) {
//        this.customerRetailPrice = customerRetailPrice;
//    }
//}
