package com.anphuocthai.staff.model

/**
 * Created by OpenYourEyes on 5/4/21
 */
data class DanhMucLoaiKhachHangModel(val danhMucID: Int, var ten: String) {
    override fun toString(): String {
        return ten;
    }
}

data class DanhMucLoaiKhachHangParam(val PhanLoaiID: Int = 4)