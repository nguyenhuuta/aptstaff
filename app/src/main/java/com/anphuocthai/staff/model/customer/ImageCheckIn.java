package com.anphuocthai.staff.model.customer;

import java.io.Serializable;

public class ImageCheckIn implements Serializable{
    int id;
    String noiDung;
    String loai;
    String tenFile;
    String moTa;


    public ImageCheckIn(int id, String noiDung, String loai, String tenFile, String moTa) {
        this.id = id;
        this.noiDung = noiDung;
        this.loai = loai;
        this.tenFile = tenFile;
        this.moTa = moTa;
    }

    public ImageCheckIn() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNoiDung() {
        return noiDung;
    }

    public void setNoiDung(String noiDung) {
        this.noiDung = noiDung;
    }

    public String getLoai() {
        return loai;
    }

    public void setLoai(String loai) {
        this.loai = loai;
    }

    public String getTenFile() {
        return tenFile;
    }

    public void setTenFile(String tenFile) {
        this.tenFile = tenFile;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }
}
