package com.anphuocthai.staff.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by OpenYourEyes on 3/8/21
 */
//"id": 15,
//"userId": 7,
//"tenNhanVien": "Phạm Thanh Hoa",
//"lyDo": "Herllo",
//"soPhepNam": 0,
//"trangThaiId": 1,
//"trangThaiText": "Mới",
//"thoiGianBatDau": "2021-03-08 07:30:00",
//"thoiGianKetThuc": "2021-03-08 17:30:00",
//"phieuDangKyNghi_TheoNgay": null
@Parcelize
class DanhSachXinNghiModel(val id: Int?,
                           val userId: Int?,
                           val tenNhanVien: String?,
                           val lyDo: String?,
                           val soPhepNam: Int?,
                           val trangThaiId: Int?,
                           val trangThaiText: String?,
                           val thoiGianBatDau: String?,
                           val thoiGianKetThuc: String?,
                           val phieuDangKyNghi_TheoNgay: String?

) : Parcelable {
    fun time() = "$thoiGianBatDau\n$thoiGianKetThuc"
}


