//package com.anphuocthai.vn.model.order;
//
//import android.arch.persistence.room.ColumnInfo;
//import android.arch.persistence.room.Ignore;
//
//import com.google.gson.annotations.SerializedName;
//
//
//
//import java.io.Serializable;
//
//public class ProductProperties implements Serializable {
//
//    private static final long serialVersionUID = 1L;
//
//    @SerializedName("dongGoi")
//    @ColumnInfo(name = "pack")
//    String pack;
//
//    @SerializedName("baoQuan")
//    @ColumnInfo(name = "preservation")
//    String preservation;
//
//
//    @Ignore
//    public ProductProperties(String pack, String preservation) {
//        this.pack = pack;
//        this.preservation = preservation;
//    }
//
//    public String getPack() {
//        return pack;
//    }
//
//    public void setPack(String pack) {
//        this.pack = pack;
//    }
//
//    public String getPreservation() {
//        return preservation;
//    }
//
//    public void setPreservation(String preservation) {
//        this.preservation = preservation;
//    }
//
//    public ProductProperties() {
//    }
//
//    @Override
//    public String toString() {
//        return "ProductProperties{" +
//                "pack='" + pack + '\'' +
//                ", preservation='" + preservation + '\'' +
//                '}';
//    }
//}
