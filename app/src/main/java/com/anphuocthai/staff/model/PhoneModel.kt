package com.anphuocthai.staff.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import org.json.JSONObject

/**
 * Created by OpenYourEyes on 4/29/21
 */
data class PhoneModel(
    var TenNguoiSoHuu: String,
    var SodienThoai: String,
    var DanhMucID_IsChinh: Int
) {
    fun toJSONObject(): JSONObject {
        return JSONObject().apply {
            put("TenNguoiSoHuu", TenNguoiSoHuu)
            put("SodienThoai", SodienThoai)
            put("DanhMucID_IsChinh", DanhMucID_IsChinh)
            put("IsDeleted", false)
            put("iD_ThanhVien", tV_ThanhVienID);
            TV_ThanhVien_DienThoaiID?.let {
                put("TV_ThanhVien_DienThoaiID", it)
            }
        }
    }

    fun syncPhone(model: PhoneModel) {
        TenNguoiSoHuu = model.TenNguoiSoHuu
        SodienThoai = model.SodienThoai
        DanhMucID_IsChinh = model.DanhMucID_IsChinh
        isSync = model.isSync
    }

    var tV_ThanhVienID: Int? = null;
    var TV_ThanhVien_DienThoaiID: Int? = null
    var tenDanhMuc: String? = null
    var isSync: Boolean = false
}

@Parcelize
data class DienThoaiModel(
    val tV_ThanhVien_DienThoaiID: Int,
    val tV_ThanhVienID: Int,
    val soDienThoai: String,
//    val tenDayDu: String,
    val tenNguoiSoHuu: String,
    val danhMucTen_IsChinh: String,
    val danhMucID_IsChinh: Int

) : Parcelable {
    fun toPhoneModel(): PhoneModel {
        val model = PhoneModel(tenNguoiSoHuu, soDienThoai, danhMucID_IsChinh)
        model.TV_ThanhVien_DienThoaiID = tV_ThanhVien_DienThoaiID
        model.tenDanhMuc = danhMucTen_IsChinh
        model.tV_ThanhVienID = tV_ThanhVienID
        return model
    }
    fun getFullPhone(): String{
        return "$tenNguoiSoHuu : $soDienThoai ($danhMucTen_IsChinh)"
    }

    override fun toString(): String {
        return "$tenNguoiSoHuu : $soDienThoai";
    }



}