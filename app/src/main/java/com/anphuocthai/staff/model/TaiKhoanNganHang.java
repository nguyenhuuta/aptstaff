package com.anphuocthai.staff.model;

public class TaiKhoanNganHang {
    int id;
    int userID;
    String soTaiKhoan;
    String tenTaiKhoan;
    int nganHangID;
    String chiNhanh;
    int tinhID;
    Boolean inUse;
    String inUseText;
    String ten_Tinh_ThanhPho;
    String tenNganHang;

    public TaiKhoanNganHang(int id, int userID, String soTaiKhoan, String tenTaiKhoan, int nganHangID, String chiNhanh, int tinhID, Boolean inUse, String inUseText, String ten_Tinh_ThanhPho, String tenNganHang) {
        this.id = id;
        this.userID = userID;
        this.soTaiKhoan = soTaiKhoan;
        this.tenTaiKhoan = tenTaiKhoan;
        this.nganHangID = nganHangID;
        this.chiNhanh = chiNhanh;
        this.tinhID = tinhID;
        this.inUse = inUse;
        this.inUseText = inUseText;
        this.ten_Tinh_ThanhPho = ten_Tinh_ThanhPho;
        this.tenNganHang = tenNganHang;
    }

    public TaiKhoanNganHang() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getSoTaiKhoan() {
        return soTaiKhoan;
    }

    public void setSoTaiKhoan(String soTaiKhoan) {
        this.soTaiKhoan = soTaiKhoan;
    }

    public String getTenTaiKhoan() {
        return tenTaiKhoan;
    }

    public void setTenTaiKhoan(String tenTaiKhoan) {
        this.tenTaiKhoan = tenTaiKhoan;
    }

    public int getNganHangID() {
        return nganHangID;
    }

    public void setNganHangID(int nganHangID) {
        this.nganHangID = nganHangID;
    }

    public String getChiNhanh() {
        return chiNhanh;
    }

    public void setChiNhanh(String chiNhanh) {
        this.chiNhanh = chiNhanh;
    }

    public int getTinhID() {
        return tinhID;
    }

    public void setTinhID(int tinhID) {
        this.tinhID = tinhID;
    }

    public Boolean getInUse() {
        return inUse;
    }

    public void setInUse(Boolean inUse) {
        this.inUse = inUse;
    }

    public String getInUseText() {
        return inUseText;
    }

    public void setInUseText(String inUseText) {
        this.inUseText = inUseText;
    }

    public String getTen_Tinh_ThanhPho() {
        return ten_Tinh_ThanhPho;
    }

    public void setTen_Tinh_ThanhPho(String ten_Tinh_ThanhPho) {
        this.ten_Tinh_ThanhPho = ten_Tinh_ThanhPho;
    }

    public String getTenNganHang() {
        return tenNganHang;
    }

    public void setTenNganHang(String tenNganHang) {
        this.tenNganHang = tenNganHang;
    }
}
