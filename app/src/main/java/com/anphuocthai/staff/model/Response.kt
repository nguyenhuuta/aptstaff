package com.anphuocthai.staff.model

import com.google.gson.annotations.SerializedName

/**
 * Created by OpenYourEyes on 11/23/2020
 */
open class Response<T> {
    @SerializedName("isSuccess")
    var isSuccess: Boolean? = false

    @SerializedName("data")
    var data: T? = null

    @SerializedName("errors")
    var message: String? = null

}