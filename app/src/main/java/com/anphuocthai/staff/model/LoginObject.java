package com.anphuocthai.staff.model;

import java.io.Serializable;

public class LoginObject implements Serializable{
    String username;
    String guid;
    String password;
    String fullname;
    String email;
    String mobile;
    Boolean status;
    boolean isAdmin;
    String token;
    String appName;
    String ipTruyCap;
    String ngayTruyCap;
    String anhDaiDienId;


    public LoginObject() {
    }

    public LoginObject(String username, String guid, String password, String fullname, String email, String mobile, Boolean status, boolean isAdmin, String token, String appName, String ipTruyCap, String ngayTruyCap,
                       String anhDaiDienId) {
        this.username = username;
        this.guid = guid;
        this.password = password;
        this.fullname = fullname;
        this.email = email;
        this.mobile = mobile;
        this.status = status;
        this.isAdmin = isAdmin;
        this.token = token;
        this.appName = appName;
        this.ipTruyCap = ipTruyCap;
        this.ngayTruyCap = ngayTruyCap;
        this.anhDaiDienId = anhDaiDienId;
    }

    public String getAnhDaiDienId() {
        return anhDaiDienId;
    }

    public void setAnhDaiDienId(String anhDaiDienId) {
        this.anhDaiDienId = anhDaiDienId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getIpTruyCap() {
        return ipTruyCap;
    }

    public void setIpTruyCap(String ipTruyCap) {
        this.ipTruyCap = ipTruyCap;
    }

    public String getNgayTruyCap() {
        return ngayTruyCap;
    }

    public void setNgayTruyCap(String ngayTruyCap) {
        this.ngayTruyCap = ngayTruyCap;
    }
}
