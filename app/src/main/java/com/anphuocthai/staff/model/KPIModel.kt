package com.anphuocthai.staff.model

/**
 * Created by OpenYourEyes on 3/22/21
 */
data class KPIModel(
        val tenChiTieu: String?,
        val donViTinhID: Int?,
        val tenDonViTinh: String?,
        val tenNhomChiTieu: String?,
        val ketqua: String?,
        val mucTieu: Int?,
        val kpI_KetQua: Int?,
        val kpI_chs: Int?,
        val kpI_hs: Int?,
        var position: Int = 0
) {
    fun mucTieu(): String? {
        if (position == positionHeader) {
            return "Mục tiêu"
        }
        return mucTieu?.toString()
    }

    fun kpI_KetQua(): String? {
        if (position == positionHeader) {
            return "Kết quả"
        }
        return defaultValue(kpI_KetQua)
    }

    fun kpI_chs(): String? {
        if (position == positionHeader) {
            return "Điểm chs"
        }
        return defaultValue(kpI_chs)
    }

    fun kpI_hs(): String? {
        if (position == positionHeader) {
            return "Điểm hs"
        }
        return defaultValue(kpI_hs)
    }

    private fun defaultValue(number: Int?): String {
        val value = number ?: 0
        if (value == -1) {
            return "-"
        }
        return value.toString()

    }

    fun position(): String {
        if (position == positionHeader) {
            return "STT"
        }
        return position.toString()
    }

    companion object {
        const val positionTitle = -1
        const val positionHeader = -2

        fun toModel(key: String?, position: Int): KPIModel {
            if (position == positionTitle) {
                return KPIModel("", 0, "", key, "", 0, 0, 0, 0, position)
            } else {
                return KPIModel("Chi tiêu", 0, "Đơn vị", "", "", 0, 0, 0, 0, position)
            }
        }
    }
}