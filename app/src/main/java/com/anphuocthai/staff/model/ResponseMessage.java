package com.anphuocthai.staff.model;

//    {
//        "code": "01",
//            "message": "Có lỗi đăng ký thành viên mới",
//            "idRecord": null,
//            "objectInfo": null
//    }


public class ResponseMessage {

    String code;
    String message;
    String idRecord;

    public ResponseMessage() {
    }

    public ResponseMessage(String code, String message, String idRecord ) {
        this.code = code;
        this.message = message;
        this.idRecord = idRecord;

    }



    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getIdRecord() {
        return idRecord;
    }

    public void setIdRecord(String idRecord) {
        this.idRecord = idRecord;
    }
}
