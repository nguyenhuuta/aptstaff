package com.anphuocthai.staff.model

/**
 * Created by OpenYourEyes on 3/16/21
 */

data class NhanVienPhongBanModel(val id: Int?,
                                 val fullname: String) {
    override fun toString(): String {
        return fullname
    }
}


//{
//    "maNhanVien": "",
//    "vanPhongID": 0,
//    "tenDayDu": "",
//    "dienThoai": "",
//    "cmt": "",
//    "trangThaiID": 1,
//    "chucVuID": 0,
//    "phongBanId": 0
//}
data class NhanVienPhongBanBody(val maNhanVien: String = "",
                                val vanPhongID: Int = 0,
                                val tenDayDu: String = "",
                                val dienThoai: String = "",
                                val cmt: String = "",
                                val trangThaiID: Int = 1,
                                val chucVuID: Int = 0,
                                val phongBanId: Int = 0)