package com.anphuocthai.staff.entities;

import com.google.gson.annotations.SerializedName;

/**
 * Created by DaiKySy on 10/7/19.
 */
public class Report {

    @SerializedName("tenThanhVien")
    private String tenThanhVien;
    @SerializedName("phaiThanhToan")
    private long phaiThanhToan;
    @SerializedName("tongGiaTri")
    private long tongGiaTri;
    @SerializedName("tongThanhToan")
    private long tongThanhToan;
    @SerializedName("tienThanhToanThua")
    private long tienThanhToanThua;
    @SerializedName("iD_ThanhVien")
    private int iD_ThanhVien;



    public static class ReportBody {
        public ReportBody() {
            this.code = "CONG_NO_KHACH_HANG";
        }

        @SerializedName("code")
        String code;

        public void setCode(String code) {
            this.code = code;
        }
    }


    public String getTenThanhVien() {
        return tenThanhVien;
    }

    public void setTenThanhVien(String tenThanhVien) {
        this.tenThanhVien = tenThanhVien;
    }

    public long getPhaiThanhToan() {
        return phaiThanhToan;
    }

    public void setPhaiThanhToan(long phaiThanhToan) {
        this.phaiThanhToan = phaiThanhToan;
    }

    public long getTongGiaTri() {
        return tongGiaTri;
    }

    public void setTongGiaTri(long tongGiaTri) {
        this.tongGiaTri = tongGiaTri;
    }

    public long getTongThanhToan() {
        return tongThanhToan;
    }

    public void setTongThanhToan(long tongThanhToan) {
        this.tongThanhToan = tongThanhToan;
    }

    public int getiD_ThanhVien() {
        return iD_ThanhVien;
    }
}
