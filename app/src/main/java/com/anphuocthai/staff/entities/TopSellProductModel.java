package com.anphuocthai.staff.entities;

/**
 * Created by DaiKySy on 2020-06-09.
 */
public class TopSellProductModel {
    int tongTienMua;
    int cv;
    int soLuong;
    String tenHangHoa;
    HH hh;

    public class HH {
        int anhDaiDienId;

        public int getAnhDaiDienId() {
            return anhDaiDienId;
        }
    }

    public int getTongTienMua() {
        return tongTienMua;
    }

    public int getCv() {
        return cv;
    }

    public int getSoLuong() {
        return soLuong;
    }

    public String getTenHangHoa() {
        return tenHangHoa;
    }

    public HH getHh() {
        return hh;
    }
}
