package com.anphuocthai.staff.entities

import com.anphuocthai.staff.baserequest.APIResponse
import com.anphuocthai.staff.model.ChiTietPhuCapModel
import com.anphuocthai.staff.utils.extension.toCurrency
import com.anphuocthai.staff.utils.printLog

/**
 * Created by OpenYourEyes on 8/5/2020
 */
data class TongHopCongModel(
        var id: Int?,
        var fullname: String?,
        var mobile: String?,
        var ma: String?,
        var tenChucVu: String?,
        var tongCong: Float?,
        var tongPhat: Float?,
        var gioLamViec: Float?,
        var gioLamThem: Float?,
        var tongCVKinhDoanh: Float?,
        var luongKinhDoanh: Float?,
        var thuongKinhDoanh: Float?,
        var tongCVVanChuyen: Float?,
        var luongVanChuyen: Float?,
        var thuongVanChuyen: Float?,
        var tongCVKhoHang: Float?,
        var luongKhoHang: Float?,
        var thuongKhoHang: Float?,
        var luongCoBan: Float?,
        var luongCoBanTheoSoCong: Float?,
        var luongTrinhDoChuyenMon: Float?,
        var tongLuongPhuCap: Float?,
        val tongLuongPhuCapTheoSoCong: Float? = null,
        val luongKPITheoCong: Float? = null,
        val luongKPI: Float? = null,
        val soTienBHXHCty: Float? = null,
        val soTienBHXHNhanVien: Float? = null,
        val dienThoai: Float? = null,
        val tamUng: Float? = null,
        val bhxh: Float? = null,
        val truTienHang: Float? = null,
//        val phatckquatkcanhan: Float? = null,
//        val cackhoangiamtru_TongPhat: Float? = null,
        val phongBanId: Float? = null,
        val solanduyet: Float? = null,
        val soLanSuaPhat: Float? = null,
        val soLanSua: Float? = null,
        val soCVThuongBocHang: Float? = null,
        val luongThuongBocHang: Float? = null

) {
    fun toList(listPhuCap: List<ChiTietPhuCapModel>, ngayCongChuan: Int): MutableList<BangLuongModel> {
        val list = arrayListOf(BangLuongModel("Chi tiết", "Lương theo chính sách", "Lương thực tế theo ngày công thực", isBold = true, isPrice = false))
        fun generateValue(title: String, number1: Float? = 0f, number2: Float? = 0f, isBold: Boolean = false, isPrice: Boolean = true) {
            val value1NN = number1 ?: 0f
            val value2NN = number2 ?: 0f
            if (value1NN > 0 && value2NN > 0) {
                list.add(BangLuongModel(title, value1NN.toCurrency(false), value2NN.toCurrency(false), isBold, isPrice))
                return
            }

            if (value1NN > 0) {
                val tongCong = tongCong ?: 0f
                val valueFormat = value1NN.toCurrency(false)
                val price = ((value1NN * tongCong) * 1.0 / ngayCongChuan).toFloat()
                list.add(BangLuongModel(title, valueFormat, price.toCurrency(false), isBold, isPrice))
                return
            }

            if (value2NN > 0) {
                val valueFormat = value2NN.toCurrency(false)
                list.add(BangLuongModel(title, "", valueFormat, isBold, isPrice))
                return
            }
            list.add(BangLuongModel(title, "", "", isBold, isPrice))
        }
        generateValue(title = "Ngày công chuẩn", number1 = ngayCongChuan.toFloat(), number2 = tongCong, isPrice = false)
        generateValue(title = "Lương cơ bản", number1 = luongCoBan)
        generateValue(title = "Tổng phụ cấp", number1 = tongLuongPhuCap, number2 = tongLuongPhuCapTheoSoCong, isBold = true, isPrice = true)
        listPhuCap.forEach {
            generateValue(title = " - ${it.ten ?: ""}}", number1 = it.luongPhuCap, isPrice = false)
        }
        generateValue(title = "KPI", number1 = luongKPI)
        generateValue(title = "Lương chuyên môn", number1 = luongTrinhDoChuyenMon)
        generateValue(title = "Lương thâm niên", number1 = 0f)

        generateValue(title = "Thưởng bốc hàng", number2 = luongThuongBocHang)
        generateValue(title = "Thưởng kho hàng", number2 = thuongKhoHang)
        generateValue(title = "Thưởng vận chuyển", number2 = thuongVanChuyen)
        generateValue(title = "Thưởng kinh doanh", number2 = thuongKinhDoanh)
        val total = list.filter { it.isPrice }.map { it.numberValue2() }.sum()
        printLog("total $total")
        generateValue(title = "Tổng lương", number2 = total, isBold = true)

//        generateValue(title = "Các khoản giảm trừ", number2 = cackhoangiamtru_TongPhat)
        generateValue(title = "Tiền điện thoại", number2 = dienThoai)
        generateValue(title = "BHXH người lao động đóng", number2 = bhxh)
        generateValue(title = "Tạm ứng", number2 = tamUng)
        generateValue(title = "Phạt", number2 = tongPhat)
        val congDoan: Float = (total * 0.01).toFloat()
        generateValue(title = "Kinh phí công đoàn", number2 = congDoan)
        generateValue(title = "Trừ tiền hàng", number2 = truTienHang)
        val reduceValue = list.filter { it.isPrice }.map { it.numberValue2() }.sum() - total * 2
        generateValue(title = "Lương còn chưa thanh toán", number2 = (total - reduceValue), isBold = true)
        return list
    }
}

/**
 * value1: Lương theo chính sách
 * value2: Lương thực tế theo ngày công
 */
data class BangLuongModel(var title: String, var value1: String, var value2: String, var isBold: Boolean = false, var isPrice: Boolean = true) {
    fun numberValue2(): Float {
        return try {
            value2.replace(Regex("""[.,]"""), "").toFloat()
        } catch (ignore: Exception) {
            0f
        }
    }
}

class TongHopCongResponse : APIResponse<List<TongHopCongModel>>() {
    var list: List<ChiTietPhuCapModel>? = null
}
