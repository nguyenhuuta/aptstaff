package com.anphuocthai.staff.entities

import com.anphuocthai.staff.baserequest.APIResponse

/**
 * Created by OpenYourEyes on 9/4/2020
 */
data class QuyCheLuongModel(val id: Int?,
                            val ten: String?,
                            val url: String?,
                            val boPhan: Int?,
                            val tenBoPhan: String?,
                            val noiDung: String?)


class QuyCheLuongResponse : APIResponse<QuyCheLuongModel>()