package com.anphuocthai.staff.entities;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "Wards")
public class Wards extends BaseColumn {
    @ColumnInfo(name = "id")
    @SerializedName("id")
    private int id;

    @ColumnInfo(name = "tenXa")
    @SerializedName("tenXa")
    private String tenXa;

    @ColumnInfo(name = "maXa")
    @SerializedName("maXa")
    private String maXa;

    @ColumnInfo(name = "quanHuyenID")
    @SerializedName("quanHuyenID")
    private int quanHuyenID;

    @ColumnInfo(name = "tinhID")
    @SerializedName("tinhID")
    private int tinhID;

    @ColumnInfo(name = "ghiChu")
    @SerializedName("ghiChu")
    private String ghiChu;


    public static Wards newObject() {
        Wards wards = new Wards();
        wards.setId(0);
        wards.setTenXa("Không có dữ liệu");
        wards.setDisable(true);
        return wards;
    }

    public static class Params {


        public Params() {
            QuanHuyenID = 0;
        }

        @SerializedName("QuanHuyenID")
        private int QuanHuyenID;

        public int getQuanHuyenID() {
            return QuanHuyenID;
        }

        public void setQuanHuyenID(int quanHuyenID) {
            QuanHuyenID = quanHuyenID;
        }
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTenXa() {
        return tenXa;
    }

    public void setTenXa(String tenXa) {
        this.tenXa = tenXa;
    }

    public String getMaXa() {
        return maXa;
    }

    public void setMaXa(String maXa) {
        this.maXa = maXa;
    }

    public int getQuanHuyenID() {
        return quanHuyenID;
    }

    public void setQuanHuyenID(int quanHuyenID) {
        this.quanHuyenID = quanHuyenID;
    }

    public int getTinhID() {
        return tinhID;
    }

    public void setTinhID(int tinhID) {
        this.tinhID = tinhID;
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    @NonNull
    @Override
    public String toString() {
        return tenXa;
    }
}
