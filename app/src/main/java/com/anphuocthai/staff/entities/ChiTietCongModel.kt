package com.anphuocthai.staff.entities

import com.anphuocthai.staff.baserequest.APIResponse

/**
 * Created by OpenYourEyes on 8/6/2020
 */
data class ChiTietCongModel(
        var ngayThang: String?,
        var ngay: Int?,
        var ngayChamCong: String?,
        var timeStamp: Long?,
        var isVisible: Boolean?,
        var stt: Int?,
        var id: Int?,
        var guid: String?,
        var username: String?,
        var fullname: String?,
        var gioVao: String?,
        var raSang: String?,
        var vaoChieu: String?,
        var gioRa: String?,
        var diMuon: Int?,
        var veSom: Int?,
        var daCham: String?,
        var gioLamViec: String?,
        var gioLamThem: String?,
        var gioLamViecHeThong: String?,
        var gioLamThemHeThong: String?,
        var tongCong: Float?,
        var tongCongDuTinh: Float?,
        var tongPhat: Float?,
        var timeString: String?,
        var ghiChu: String?,
        var chucVuID: Int?,
        var giaodichphats: List<Giaodichphat>?,
        var cauHinhLichLamViecID: Int?
)

data class Giaodichphat(
        var id: Int?,
        var userID: Int?,
        var ngayTAO: String?,
        var soTien: Float?,
        var loaiID: Int?,
        var objectID: String?,
        var moTa: String?,
        var duLieu: String?,
        var chuKy: Int?,
        var trangThaiID: Int?,
        var nguoiTAOID: Int?,
        var ngayCapNhat: String?,
        var ngayGiaoDich: String?,
        var loai: Int?,
        var loaiText: String?
)

class ChiTietCongResponse : APIResponse<List<ChiTietCongModel>>()
