package com.anphuocthai.staff.entities;

public class CheckChangeProduct {
    private boolean isNew;

    /*
     * down : -1
     * up : 1
     *
     * */
    private ChangePrice changePrice;


    public CheckChangeProduct(boolean iNew, ChangePrice changePrice) {
        this.isNew = iNew;
        this.changePrice = changePrice;
    }

    public ChangePrice isChangePrice() {
        return changePrice;
    }

    public void setChangePrice(ChangePrice changePrice) {
        this.changePrice = changePrice;
    }

    public void setNew(boolean aNew) {
        isNew = aNew;
    }

    public boolean isNew() {
        return isNew;
    }

    public enum ChangePrice {
        NORMAL, UP, DOWN
    }

}
