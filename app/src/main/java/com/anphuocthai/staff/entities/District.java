package com.anphuocthai.staff.entities;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "District")
public class District extends BaseColumn {
    @ColumnInfo(name = "quan_Huyen_ID")
    @SerializedName("quan_Huyen_ID")
    private int quan_Huyen_ID;

    @ColumnInfo(name = "ma_QuanHuyen")
    @SerializedName("ma_QuanHuyen")
    private int ma_QuanHuyen;

    @ColumnInfo(name = "ten_Quan_Huyen")
    @SerializedName("ten_Quan_Huyen")
    private String ten_Quan_Huyen;

    @ColumnInfo(name = "ghiChu")
    @SerializedName("ghiChu")
    private String ghiChu;

    @ColumnInfo(name = "tinh_ThanhPho_ID")
    @SerializedName("tinh_ThanhPho_ID")
    private int tinh_ThanhPho_ID;
    public static District newObject(){
        District district = new District();
        district.setQuan_Huyen_ID(0);
        district.setTen_Quan_Huyen("Không có dữ liệu");
        district.setDisable(true);
        return district;
    }
    public static class Params {
        public Params() {
            Tinh_ThanhPho_ID = 0;
        }

        @SerializedName("Tinh_ThanhPho_ID")
        private int Tinh_ThanhPho_ID;

        public int getQuanHuyenID() {
            return Tinh_ThanhPho_ID;
        }

        public void setQuanHuyenID(int Tinh_ThanhPho_ID) {
            Tinh_ThanhPho_ID = Tinh_ThanhPho_ID;
        }
    }

    public int getQuan_Huyen_ID() {
        return quan_Huyen_ID;
    }

    public void setQuan_Huyen_ID(int quan_Huyen_ID) {
        this.quan_Huyen_ID = quan_Huyen_ID;
    }

    public int getMa_QuanHuyen() {
        return ma_QuanHuyen;
    }

    public void setMa_QuanHuyen(int ma_QuanHuyen) {
        this.ma_QuanHuyen = ma_QuanHuyen;
    }

    public String getTen_Quan_Huyen() {
        return ten_Quan_Huyen;
    }

    public void setTen_Quan_Huyen(String ten_Quan_Huyen) {
        this.ten_Quan_Huyen = ten_Quan_Huyen;
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    public int getTinh_ThanhPho_ID() {
        return tinh_ThanhPho_ID;
    }

    public void setTinh_ThanhPho_ID(int tinh_ThanhPho_ID) {
        this.tinh_ThanhPho_ID = tinh_ThanhPho_ID;
    }

    @NonNull
    @Override
    public String toString() {
        return ten_Quan_Huyen;
    }
}
