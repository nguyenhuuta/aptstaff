package com.anphuocthai.staff.entities;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by DaiKySy on 10/7/19.
 */
public class ListReportResponse {
    @SerializedName("dD_DonDat_List")
    private List<Report> listReport;
    @SerializedName("tonG_SO_TIEN")
    private long tongSoTien;
    @SerializedName("tonG_SO_TIEN_DA_TT")
    private long tongSoTienDaThanhToan;
    @SerializedName("tonG_SO_TIEN_CON")
    private long soTienConLai;

    public List<Report> getListReport() {
        return listReport;
    }

    public void setListReport(List<Report> listReport) {
        this.listReport = listReport;
    }

    public long getTongSoTien() {
        return tongSoTien;
    }

    public void setTongSoTien(long tongSoTien) {
        this.tongSoTien = tongSoTien;
    }

    public long getTongSoTienDaThanhToan() {
        return tongSoTienDaThanhToan;
    }

    public void setTongSoTienDaThanhToan(long tongSoTienDaThanhToan) {
        this.tongSoTienDaThanhToan = tongSoTienDaThanhToan;
    }

    public long getSoTienConLai() {
        return soTienConLai;
    }

    public void setSoTienConLai(long soTienConLai) {
        this.soTienConLai = soTienConLai;
    }
}
