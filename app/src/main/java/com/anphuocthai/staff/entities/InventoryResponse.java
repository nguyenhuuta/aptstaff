package com.anphuocthai.staff.entities;

import com.google.gson.annotations.SerializedName;

/**
 * Created by DaiKySy on 10/1/19.
 */
public class InventoryResponse {
    @SerializedName("message")
    private String message;
    @SerializedName("sessionID")
    private String sessionID;
    @SerializedName("isSuccess")
    private boolean isSuccess;

    @Override
    public String toString() {
        return "\n InventoryResponse{" +
                "message='" + message + '\'' +
                ", sessionID='" + sessionID + '\'' +
                ", isSuccess=" + isSuccess +
                '}';
    }
}
