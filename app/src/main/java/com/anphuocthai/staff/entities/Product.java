package com.anphuocthai.staff.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.anphuocthai.staff.utils.Constants;
import com.anphuocthai.staff.utils.Logger;
import com.anphuocthai.staff.utils.Utils;

/**
 * Created by DaiKySy on 9/10/19.
 */
@Entity(tableName = "Product")
public class Product {

    private static final long TIME_TO_OLD = 10 * 60 * 1000;

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "catId")
    private int catId;

    @ColumnInfo(name = "productId")
    private int productId;

    @ColumnInfo(name = "productName")
    private String productName;

    @ColumnInfo(name = "price")
    private int price;

    @ColumnInfo(name = "cv")
    private int cv;

//    @ColumnInfo(name = "isNew")
//    private boolean isNew;

    @ColumnInfo(name = "whatNew")
    private String whatNew;

    @ColumnInfo(name = "timeToOld")
    private long timeToOld;

    @Ignore
    public Product() {
    }

    public Product(int catId, int productId, String productName, int price, int cv) {
        this.catId = catId;
        this.productId = productId;
        this.productName = productName;
        this.price = price;
        this.cv = cv;
        this.whatNew = Constants.EMPTY;
        this.timeToOld = 0;
    }

    public String checkChange(long currentTime, com.anphuocthai.staff.model.order.Product product) {
        String checkChangeProduct;

        if (Utils.isNull(whatNew)) {
            checkChangeProduct = checkDiff(product);
        } else { // current is new
            Logger.d("checkChange", timeToOld + "");
            checkChangeProduct = whatNew;
            if (timeToOld == 0) {
                checkChangeProduct = whatNew;
            } else if (currentTime < timeToOld) {
                String checkDiff = checkDiff(product);
                if (!checkDiff.isEmpty()) {
                    timeToOld = currentTime + TIME_TO_OLD;
                    whatNew = checkDiff;
                    checkChangeProduct = checkDiff;
                }
            } else { // currentTime > timeToOld
                String checkDiff = checkDiff(product);
                if (!checkDiff.isEmpty()) {
                    timeToOld = currentTime + TIME_TO_OLD;
                    whatNew = checkDiff;
                    checkChangeProduct = checkDiff;
                } else {
                    whatNew = Constants.EMPTY;
                    checkChangeProduct = Constants.EMPTY;
                    timeToOld = 0;
                }
            }
        }
        return checkChangeProduct;

    }

    private String checkDiff(com.anphuocthai.staff.model.order.Product product) {
        String checkChangeProduct = Constants.EMPTY;
        if (!productName.equals(product.getName())) {
            productName = product.getName();
            checkChangeProduct = getTextChange(WhatChange.NEW);
        }
        if (cv != product.getMainPrice().getKDCV()) {
            cv = product.getMainPrice().getKDCV();
            checkChangeProduct = getTextChange(WhatChange.NEW);
        }
        int priceFromAPI = product.getMainPrice().getKDBanBuonGia();
        if (price != priceFromAPI) {
            if (price < priceFromAPI) {
                checkChangeProduct = getTextChange(WhatChange.UP);
            } else {
                checkChangeProduct = getTextChange(WhatChange.DOWN);
            }
            price = priceFromAPI;

        }

        return checkChangeProduct;
    }


//    public CheckChangeProduct checkDiff(long currentTime, com.anphuocthai.vn.model.order.Product product) {
//        CheckChangeProduct checkChangeProduct = new CheckChangeProduct(isNew, CheckChangeProduct.ChangePrice.NORMAL);
//        if (isNew) {
//            if (timeToOld < currentTime) {
//                isNew = false;
//                checkChangeProduct.setNew(false);
//            } else { // timeToOld = 0
//                checkChangeProduct.setNew(true);
//            }
////
//
//            if (timeToOld == 0) {
//                checkChangeProduct.setNew(true);
//            } else {
//                if (timeToOld > currentTime) {
//                    checkChangeProduct.setNew(true);
//                } else {
//                    isNew = false;
//                    checkChangeProduct.setNew(false);
//                }
//            }
//        } else {
//            if (!productName.equals(product.getName())) {
//                productName = product.getName();
//                checkChangeProduct.setNew(true);
//            }
//
//            int priceFromAPI = product.getMainPrice().getKDBanBuonGia();
//            if (price != priceFromAPI) {
//                if (price < priceFromAPI) {
//                    checkChangeProduct.setChangePrice(CheckChangeProduct.ChangePrice.UP);
//                } else {
//                    checkChangeProduct.setChangePrice(CheckChangeProduct.ChangePrice.DOWN);
//                }
//                price = priceFromAPI;
//                checkChangeProduct.setNew(true);
//
//            }
//            if (cv != product.getMainPrice().getKDCV()) {
//                cv = product.getMainPrice().getKDCV();
//                checkChangeProduct.setNew(true);
//            }
//        }
//        return checkChangeProduct;
//    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCatId() {
        return catId;
    }

    public void setCatId(int catId) {
        this.catId = catId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getCv() {
        return cv;
    }

    public void setCv(int cv) {
        this.cv = cv;
    }
//
//    public void setNew(boolean aNew) {
//        isNew = aNew;
//    }
//
//
//    public boolean isNew() {
//        return isNew;
//    }

    public boolean startCountTimeToOld() {
        long currentTime = System.currentTimeMillis();
        if (timeToOld == 0) {
            timeToOld = currentTime + TIME_TO_OLD;
            return true;
        }
        return false;
    }

    public void setWhatNew(String whatNew) {
        this.whatNew = whatNew;
    }

    public String getWhatNew() {
        return whatNew;
    }

    public long getTimeToOld() {
        return timeToOld;
    }

    public void setTimeToOld(long timeToOld) {
        this.timeToOld = timeToOld;
    }


    public String getTextChange(WhatChange change) {
        switch (change) {
            case UP:
                return "Tăng";
            case DOWN:
                return "Giảm";
            case NEW:
                return "Mới";
            default:
                return "";
        }
    }

    public enum WhatChange {
        NONE, NEW, UP, DOWN
    }
}
