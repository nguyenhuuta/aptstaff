package com.anphuocthai.staff.entities

/**
 * Created by OpenYourEyes on 9/15/2020
 */


/**
 * type = 1 : Top bán hàng xuất sắc trong ngày
 * type = 2 : Top bán hàng năng động trong tháng
 * type = 3 : Top vận chuyển năng động trong tháng
 * type = 4 : Top xuất hàng năng động trong tháng
 * type = 5 : GPS
 */
enum class DynamicReportType(val value: String) {
    None(""),
    TopSellInDay("Top bán hàng xuất sắc trong ngày"),
    TopSellInMonth("Top bán hàng năng động trong tháng"),
    TopTransportIntMonth("Top vận chuyển năng động trong tháng"),
    TopStockInMonth("Top xuất hàng năng động trong tháng"),
    GPS("GPS");

    companion object {
        fun compareValue(newValue: String): DynamicReportType {
            return when (newValue) {
                TopSellInDay.value -> TopSellInDay
                TopSellInMonth.value -> TopSellInMonth
                TopTransportIntMonth.value -> TopTransportIntMonth
                TopStockInMonth.value -> TopStockInMonth
                else -> {
                    if (newValue.contains(GPS.value)) {
                        GPS
                    } else {
                        None
                    }
                }
            }
        }
    }


}