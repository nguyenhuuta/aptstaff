package com.anphuocthai.staff.entities

/**
 * Created by OpenYourEyes on 6/19/2020
 */
data class DepartmentModel(
        var id: Int?,
        var ma: String?,
        var ten: String?,
        var xoa: Boolean?,
        var chucVuID: Int?,
        var tenChucVu: String,
        var isChecked: Int?,
        var isDefault: Int?,
        var dM_BoPhan_UserId: Int?
)


