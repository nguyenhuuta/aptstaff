package com.anphuocthai.staff.entities;

import com.anphuocthai.staff.utils.App;

/**
 * Created by DaiKySy on 2020-06-09.
 */
public class TopSellBody {
    int ngay;
    int thang;
    int nam;
    int LoaiBaoCao = 2;
    boolean getAnhHangHoa = true;
    int BoPhanId = 0;

    public TopSellBody(int ngay, int thang, int nam, int loaiBaoCao, boolean getAnhHangHoa) {
        this.ngay = ngay;
        this.thang = thang;
        this.nam = nam;
        LoaiBaoCao = loaiBaoCao;
        this.getAnhHangHoa = getAnhHangHoa;
        BoPhanId = App.getInstance().getDepartmentId();
    }
}
