package com.anphuocthai.staff.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.anphuocthai.staff.model.order.ProductCategory;

/**
 * Created by DaiKySy on 8/19/19.
 */


/*
 *
 *
 * Dung de check san pham nao la moi
 *
 * */
@Entity(tableName = "Category")
public class Category {
    @PrimaryKey(autoGenerate = true)
    private int id;
    @ColumnInfo(name = "catId")
    private int catId;
    @ColumnInfo(name = "catName")
    private String catName;
    @ColumnInfo(name = "imageId")
    private int imageId;

    @ColumnInfo(name = "numberItemNew")
    private int numberItemNew;

    @ColumnInfo(name = "isCategoryNew")
    private boolean isCategoryNew;

    public Category() {

    }

    public Category(ProductCategory productCategory) {
        this.catId = productCategory.getId();
        this.catName = productCategory.getName();
        this.imageId = productCategory.getCategoryThumb();
        this.numberItemNew = 0;
    }

    public boolean checkDiff(ProductCategory productCategory) {
        boolean isDiff = false;
        if (!catName.equals(productCategory.getName())) {
            catName = productCategory.getName();
            isDiff = true;
        }
        if (imageId != productCategory.getCategoryThumb()) {
            imageId = productCategory.getCategoryThumb();
            isDiff = true;
        }
        return isDiff;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCatId() {
        return catId;
    }

    public void setCatId(int catId) {
        this.catId = catId;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public int getNumberItemNew() {
        return numberItemNew;
    }

    public void setNumberItemNew(int numberItemNew) {
        this.numberItemNew = numberItemNew;
    }

    public void decrementNumberSelected() {
        this.numberItemNew--;
    }

    public boolean isCategoryNew() {
        return isCategoryNew;
    }

    public void setCategoryNew(boolean categoryNew) {
        isCategoryNew = categoryNew;
    }
}

