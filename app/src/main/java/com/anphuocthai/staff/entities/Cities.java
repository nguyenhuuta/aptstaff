package com.anphuocthai.staff.entities;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "Cities")
public class Cities extends BaseColumn {
    @ColumnInfo(name = "tinh_ThanhPho_ID")
    @SerializedName("tinh_ThanhPho_ID")
    private int tinh_ThanhPho_ID;

    @ColumnInfo(name = "ma_Tinh_ThanhPho")
    @SerializedName("ma_Tinh_ThanhPho")
    private String ma_Tinh_ThanhPho;

    @ColumnInfo(name = "ten_Tinh_ThanhPho")
    @SerializedName("ten_Tinh_ThanhPho")
    private String ten_Tinh_ThanhPho;

    @ColumnInfo(name = "ghiChu")
    @SerializedName("ghiChu")
    private String ghiChu;

    @ColumnInfo(name = "quocTich_ID")
    @SerializedName("quocTich_ID")
    private int quocTich_ID;


    public static class Params {
        public Params() {
            tinh_ThanhPho_ID = 0;
        }

        @SerializedName("tinh_ThanhPho_ID")
        private int tinh_ThanhPho_ID;

        public int getTinh_ThanhPho_ID() {
            return tinh_ThanhPho_ID;
        }

        public void setTinh_ThanhPho_ID(int tinh_ThanhPho_ID) {
            this.tinh_ThanhPho_ID = tinh_ThanhPho_ID;
        }
    }
    public int getTinh_ThanhPho_ID() {
        return tinh_ThanhPho_ID;
    }

    public void setTinh_ThanhPho_ID(int tinh_ThanhPho_ID) {
        this.tinh_ThanhPho_ID = tinh_ThanhPho_ID;
    }

    public String getMa_Tinh_ThanhPho() {
        return ma_Tinh_ThanhPho;
    }

    public void setMa_Tinh_ThanhPho(String ma_Tinh_ThanhPho) {
        this.ma_Tinh_ThanhPho = ma_Tinh_ThanhPho;
    }

    public String getTen_Tinh_ThanhPho() {
        return ten_Tinh_ThanhPho;
    }

    public void setTen_Tinh_ThanhPho(String ten_Tinh_ThanhPho) {
        this.ten_Tinh_ThanhPho = ten_Tinh_ThanhPho;
    }

    public String getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    public int getQuocTich_ID() {
        return quocTich_ID;
    }

    public void setQuocTich_ID(int quocTich_ID) {
        this.quocTich_ID = quocTich_ID;
    }

    @NonNull
    @Override
    public String toString() {
        return ten_Tinh_ThanhPho;
    }
}
