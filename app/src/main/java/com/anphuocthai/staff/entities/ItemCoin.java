package com.anphuocthai.staff.entities;

import com.anphuocthai.staff.R;

import java.util.ArrayList;
import java.util.List;

public class ItemCoin {

    public ItemCoin(int level) {
        listDetail = new ArrayList<>();
        switch (level) {
            case 1:
                rankName = "Bạc";
                isEnable = true;
                listDetail.add("Bạc");
                break;
            case 2:
                rankName = "Vàng";
                isEnable = true;
                listDetail.add("Vàng");
                listDetail.add("Vàng");
                break;
            case 3:
                rankName = "Bạch Kim";
                isEnable = true;
                listDetail.add("Bạch Kim");
                listDetail.add("Bạch Kim");
                listDetail.add("Bạch Kim");
                listDetail.add("Bạch Kim");
                break;
            case 4:
                rankName = "Kim Cương";
                isEnable = false;
                listDetail.add("Kim Cương");
                listDetail.add("Kim Cương");
                listDetail.add("Kim Cương");
                listDetail.add("Kim Cương");
                listDetail.add("Kim Cương");
                listDetail.add("Kim Cương");
                listDetail.add("Kim Cương");
                listDetail.add("Kim Cương");
                listDetail.add("Kim Cương");
                listDetail.add("Kim Cương");
                listDetail.add("Kim Cương");
                listDetail.add("Kim Cương");
                listDetail.add("Kim Cương");
                listDetail.add("Kim Cương");
                listDetail.add("Kim Cương");
                listDetail.add("Kim Cương");
                listDetail.add("Kim Cương");
                listDetail.add("Kim Cương");
                listDetail.add("Kim Cương");
                listDetail.add("Kim Cương");
                listDetail.add("Kim Cương");
                listDetail.add("Kim Cương");
                listDetail.add("Kim Cương");
                listDetail.add("Kim Cương");
                break;
            case 5:
                rankName = "Cao thủ";
                isEnable = false;
                listDetail.add("Cao thủ");
                listDetail.add("Cao thủ");
                listDetail.add("Cao thủ");
                listDetail.add("Cao thủ");
                listDetail.add("Cao thủ");
                listDetail.add("Cao thủ");
                listDetail.add("Cao thủ");
                listDetail.add("Cao thủ");
                listDetail.add("Cao thủ");
                listDetail.add("Cao thủ");
                listDetail.add("Cao thủ");
                listDetail.add("Cao thủ");
                listDetail.add("Cao thủ");
                listDetail.add("Cao thủ");
                listDetail.add("Cao thủ");
                listDetail.add("Cao thủ");
                listDetail.add("Cao thủ");
                listDetail.add("Cao thủ");
                break;
        }
    }

    private String rankName;
    private List<String> listDetail;
    private boolean isEnable;

    public int getIcon() {
        if (isEnable) {
            return R.drawable.ic_coin_enable;
        }
        return R.drawable.ic_coin_disable;
    }


}
