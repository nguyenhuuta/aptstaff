package com.anphuocthai.staff.entities

/**
 * Created by OpenYourEyes on 9/4/2020
 */
class SubUserModel {
    var name: String = ""
    var isExpand: Boolean = false
    var listSub: List<SubModel>? = null

    constructor(name: String, listSub: List<SubModel>? = null) {
        this.name = name
        this.listSub = listSub
    }
}

data class SubModel(val name: String, val clazz: Class<*>)