package com.anphuocthai.staff.entities;

import androidx.room.PrimaryKey;

public class BaseColumn {
    @PrimaryKey(autoGenerate = true)
    private int idColumn;

    public int getIdColumn() {
        return idColumn;
    }

    public void setIdColumn(int idColumn) {
        this.idColumn = idColumn;
    }

    private boolean isDisable;

    public void setDisable(boolean disable) {
        isDisable = disable;
    }

    public boolean isDisable() {
        return isDisable;
    }
}
