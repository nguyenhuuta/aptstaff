package com.anphuocthai.staff.entities;

import com.google.gson.annotations.SerializedName;

/**
 * Created by DaiKySy on 10/1/19.
 */
public class LockCustomerParam {

    @SerializedName("ID_ThanhVien")
    private int ID_ThanhVien;
    @SerializedName("LyDoKhoa")
    private String reason;
    @SerializedName("Khoa")
    private int khoa;


    public LockCustomerParam(int ID_ThanhVien, String reason) {
        this.ID_ThanhVien = ID_ThanhVien;
        this.reason = reason;
        this.khoa = 1;
    }

    public int getID_ThanhVien() {
        return ID_ThanhVien;
    }

    public void setID_ThanhVien(int ID_ThanhVien) {
        this.ID_ThanhVien = ID_ThanhVien;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public int getKhoa() {
        return khoa;
    }

    public void setKhoa(int khoa) {
        this.khoa = khoa;
    }
}
