package com.anphuocthai.staff.interfaces;

import android.widget.ImageView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

import io.reactivex.Observable;

public interface IAPIInterface {
    void sendGetRequestNotAuthen(String url, HashMap hashMap, boolean isResponseObject, IAPICallback iapiCallback);

    void sendPostRequest(String url, JSONObject jsonObject, IAPICallback iapiCallback);

    void sendPostJsonRequest(String url, Object jsonObject, IAPICallback iapiCallback);

    void sendPostRequestWithArrayResponse(String url, JSONObject jsonObject, IAPICallback iapiCallback);

    void sendPostRequestWithQueryParam(String url, HashMap hashMap, IAPICallback iapiCallback);


    void sendPostRequestNotAuthen(String url, JSONObject jsonObject, boolean isObjectResponse, IAPICallback iapiCallback);

    void sendGetRequest(String url, HashMap hashMap, IAPICallback iapiCallback);

    void loadImage(int imgId, ImageView imageView);

    void sendGetRequestObjectResponse(String url, HashMap hashMap, IAPICallback iapiCallback);

    void sendPostRequestWithObject(String url, JSONArray jsonArray, IAPICallback iapiCallback);

    <T> Observable<T> postRequest(String url, Object object, Class<T> tClass);

}
