package com.anphuocthai.staff.interfaces.login;

import org.json.JSONObject;

public interface ILoginParam {
    JSONObject makeLoginParam(String username, String password, String app);
}
