package com.anphuocthai.staff.interfaces;

// Callback when show dialog
public interface IYNDialogCallback {
    void accept();
    void cancel();
}
