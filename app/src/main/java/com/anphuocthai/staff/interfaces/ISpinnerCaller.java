package com.anphuocthai.staff.interfaces;

public interface ISpinnerCaller {
    void setCurrentItemSpinner(int currentItem);
}
