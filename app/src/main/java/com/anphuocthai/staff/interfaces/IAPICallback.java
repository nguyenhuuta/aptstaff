package com.anphuocthai.staff.interfaces;

import com.androidnetworking.error.ANError;

import org.json.JSONArray;
import org.json.JSONObject;

public interface IAPICallback {
    void onResponseSuccess(JSONObject response);
    void onResponseSuccess(JSONArray response);
    void onResponseError(ANError anError);
}

